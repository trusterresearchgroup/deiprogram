/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * unique.c
 *
 * Code generation for function 'unique'
 *
 */

/* Include files */
#include "unique.h"
#include "DEIPFunc2_data.h"
#include "DEIPFunc2_emxutil.h"
#include "DEIPFunc2_types.h"
#include "eml_int_forloop_overflow_check.h"
#include "indexShapeCheck.h"
#include "rt_nonfinite.h"
#include "sortLE.h"
#include "mwmathutil.h"
#include <math.h>

/* Variable Definitions */
static emlrtRSInfo xe_emlrtRSI = { 297,/* lineNo */
  "unique_rows",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pathName */
};

static emlrtRSInfo ye_emlrtRSI = { 306,/* lineNo */
  "unique_rows",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pathName */
};

static emlrtRSInfo af_emlrtRSI = { 315,/* lineNo */
  "unique_rows",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pathName */
};

static emlrtRSInfo bf_emlrtRSI = { 328,/* lineNo */
  "unique_rows",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pathName */
};

static emlrtRSInfo cf_emlrtRSI = { 27, /* lineNo */
  "sortrows",                          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pathName */
};

static emlrtRSInfo df_emlrtRSI = { 28, /* lineNo */
  "sortrows",                          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pathName */
};

static emlrtRSInfo ef_emlrtRSI = { 86, /* lineNo */
  "sortIdx",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pathName */
};

static emlrtRSInfo gf_emlrtRSI = { 57, /* lineNo */
  "mergesort",                         /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\mergesort.m"/* pathName */
};

static emlrtRSInfo hf_emlrtRSI = { 113,/* lineNo */
  "mergesort",                         /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\mergesort.m"/* pathName */
};

static emlrtRSInfo if_emlrtRSI = { 40, /* lineNo */
  "apply_row_permutation",             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pathName */
};

static emlrtRSInfo jf_emlrtRSI = { 43, /* lineNo */
  "apply_row_permutation",             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pathName */
};

static emlrtRSInfo kf_emlrtRSI = { 350,/* lineNo */
  "rows_differ",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pathName */
};

static emlrtRSInfo lf_emlrtRSI = { 40, /* lineNo */
  "safeEq",                            /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\safeEq.m"/* pathName */
};

static emlrtRSInfo mf_emlrtRSI = { 46, /* lineNo */
  "eps",                               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\eps.m"/* pathName */
};

static emlrtRSInfo bh_emlrtRSI = { 158,/* lineNo */
  "unique_vector",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pathName */
};

static emlrtRSInfo ch_emlrtRSI = { 160,/* lineNo */
  "unique_vector",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pathName */
};

static emlrtRSInfo dh_emlrtRSI = { 177,/* lineNo */
  "unique_vector",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pathName */
};

static emlrtRSInfo eh_emlrtRSI = { 195,/* lineNo */
  "unique_vector",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pathName */
};

static emlrtRSInfo fh_emlrtRSI = { 202,/* lineNo */
  "unique_vector",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pathName */
};

static emlrtRSInfo gh_emlrtRSI = { 215,/* lineNo */
  "unique_vector",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pathName */
};

static emlrtRSInfo hh_emlrtRSI = { 226,/* lineNo */
  "unique_vector",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pathName */
};

static emlrtRSInfo ih_emlrtRSI = { 234,/* lineNo */
  "unique_vector",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pathName */
};

static emlrtRSInfo jh_emlrtRSI = { 240,/* lineNo */
  "unique_vector",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pathName */
};

static emlrtRSInfo kh_emlrtRSI = { 145,/* lineNo */
  "sortIdx",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pathName */
};

static emlrtRTEInfo d_emlrtRTEI = { 325,/* lineNo */
  1,                                   /* colNo */
  "unique_rows",                       /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pName */
};

static emlrtRTEInfo h_emlrtRTEI = { 233,/* lineNo */
  1,                                   /* colNo */
  "unique_vector",                     /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pName */
};

static emlrtRTEInfo kc_emlrtRTEI = { 287,/* lineNo */
  5,                                   /* colNo */
  "unique",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pName */
};

static emlrtRTEInfo lc_emlrtRTEI = { 1,/* lineNo */
  29,                                  /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

static emlrtRTEInfo mc_emlrtRTEI = { 27,/* lineNo */
  1,                                   /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

static emlrtRTEInfo nc_emlrtRTEI = { 52,/* lineNo */
  9,                                   /* colNo */
  "mergesort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\mergesort.m"/* pName */
};

static emlrtRTEInfo oc_emlrtRTEI = { 38,/* lineNo */
  23,                                  /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

static emlrtRTEInfo pc_emlrtRTEI = { 29,/* lineNo */
  1,                                   /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

static emlrtRTEInfo qc_emlrtRTEI = { 326,/* lineNo */
  5,                                   /* colNo */
  "unique",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pName */
};

static emlrtRTEInfo rc_emlrtRTEI = { 326,/* lineNo */
  1,                                   /* colNo */
  "unique",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pName */
};

static emlrtRTEInfo sc_emlrtRTEI = { 327,/* lineNo */
  22,                                  /* colNo */
  "unique",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pName */
};

static emlrtRTEInfo tc_emlrtRTEI = { 38,/* lineNo */
  1,                                   /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

static emlrtRTEInfo uc_emlrtRTEI = { 52,/* lineNo */
  1,                                   /* colNo */
  "mergesort",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\mergesort.m"/* pName */
};

static emlrtRTEInfo xc_emlrtRTEI = { 158,/* lineNo */
  1,                                   /* colNo */
  "unique",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pName */
};

static emlrtRTEInfo yc_emlrtRTEI = { 159,/* lineNo */
  20,                                  /* colNo */
  "unique",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pName */
};

static emlrtRTEInfo ad_emlrtRTEI = { 234,/* lineNo */
  1,                                   /* colNo */
  "unique",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pName */
};

/* Function Definitions */
void unique_rows(const emlrtStack *sp, const emxArray_real_T *a, emxArray_real_T
                 *b, emxArray_int32_T *ndx)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack st;
  emxArray_int32_T *idx;
  emxArray_int32_T *iwork;
  emxArray_real_T *b_b;
  emxArray_real_T *ycol;
  real_T absx;
  real_T b_a;
  real_T c_b;
  int32_T b_i;
  int32_T exitg1;
  int32_T exponent;
  int32_T i;
  int32_T i1;
  int32_T j;
  int32_T k;
  int32_T k0;
  int32_T kEnd;
  int32_T m;
  int32_T n;
  int32_T nb;
  int32_T q;
  int32_T qEnd;
  boolean_T b_overflow;
  boolean_T exitg2;
  boolean_T overflow;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  if (a->size[0] == 0) {
    i = b->size[0] * b->size[1];
    b->size[0] = a->size[0];
    b->size[1] = 2;
    emxEnsureCapacity_real_T(sp, b, i, &kc_emlrtRTEI);
    m = a->size[0] * a->size[1];
    for (i = 0; i < m; i++) {
      b->data[i] = a->data[i];
    }

    ndx->size[0] = 0;
  } else {
    st.site = &xe_emlrtRSI;
    i = b->size[0] * b->size[1];
    b->size[0] = a->size[0];
    b->size[1] = 2;
    emxEnsureCapacity_real_T(&st, b, i, &lc_emlrtRTEI);
    m = a->size[0] * a->size[1];
    for (i = 0; i < m; i++) {
      b->data[i] = a->data[i];
    }

    emxInit_int32_T(&st, &idx, 1, &mc_emlrtRTEI, true);
    b_st.site = &cf_emlrtRSI;
    n = a->size[0] + 1;
    i = idx->size[0];
    idx->size[0] = a->size[0];
    emxEnsureCapacity_int32_T(&b_st, idx, i, &mc_emlrtRTEI);
    m = a->size[0];
    for (i = 0; i < m; i++) {
      idx->data[i] = 0;
    }

    emxInit_int32_T(&b_st, &iwork, 1, &uc_emlrtRTEI, true);
    c_st.site = &ef_emlrtRSI;
    i = iwork->size[0];
    iwork->size[0] = a->size[0];
    emxEnsureCapacity_int32_T(&c_st, iwork, i, &nc_emlrtRTEI);
    m = a->size[0] - 1;
    d_st.site = &gf_emlrtRSI;
    if ((1 <= a->size[0] - 1) && (a->size[0] - 1 > 2147483645)) {
      e_st.site = &ff_emlrtRSI;
      check_forloop_overflow_error(&e_st);
    }

    for (k = 1; k <= m; k += 2) {
      if (sortLE(a, k, k + 1)) {
        idx->data[k - 1] = k;
        idx->data[k] = k + 1;
      } else {
        idx->data[k - 1] = k + 1;
        idx->data[k] = k;
      }
    }

    if ((a->size[0] & 1) != 0) {
      idx->data[a->size[0] - 1] = a->size[0];
    }

    b_i = 2;
    while (b_i < n - 1) {
      m = b_i << 1;
      j = 1;
      for (k0 = b_i + 1; k0 < n; k0 = qEnd + b_i) {
        nb = j;
        q = k0;
        qEnd = j + m;
        if (qEnd > n) {
          qEnd = n;
        }

        k = 0;
        kEnd = qEnd - j;
        while (k + 1 <= kEnd) {
          i = idx->data[q - 1];
          i1 = idx->data[nb - 1];
          if (sortLE(a, i1, i)) {
            iwork->data[k] = i1;
            nb++;
            if (nb == k0) {
              while (q < qEnd) {
                k++;
                iwork->data[k] = idx->data[q - 1];
                q++;
              }
            }
          } else {
            iwork->data[k] = i;
            q++;
            if (q == qEnd) {
              while (nb < k0) {
                k++;
                iwork->data[k] = idx->data[nb - 1];
                nb++;
              }
            }
          }

          k++;
        }

        d_st.site = &hf_emlrtRSI;
        for (k = 0; k < kEnd; k++) {
          idx->data[(j + k) - 1] = iwork->data[k];
        }

        j = qEnd;
      }

      b_i = m;
    }

    emxFree_int32_T(&iwork);
    emxInit_real_T(&c_st, &ycol, 1, &tc_emlrtRTEI, true);
    b_st.site = &df_emlrtRSI;
    m = a->size[0];
    i = ycol->size[0];
    ycol->size[0] = a->size[0];
    emxEnsureCapacity_real_T(&b_st, ycol, i, &oc_emlrtRTEI);
    overflow = (a->size[0] > 2147483646);
    b_overflow = (a->size[0] > 2147483646);
    c_st.site = &if_emlrtRSI;
    if (overflow) {
      d_st.site = &ff_emlrtRSI;
      check_forloop_overflow_error(&d_st);
    }

    for (b_i = 0; b_i < m; b_i++) {
      ycol->data[b_i] = b->data[idx->data[b_i] - 1];
    }

    c_st.site = &jf_emlrtRSI;
    if (b_overflow) {
      d_st.site = &ff_emlrtRSI;
      check_forloop_overflow_error(&d_st);
    }

    for (b_i = 0; b_i < m; b_i++) {
      b->data[b_i] = ycol->data[b_i];
    }

    c_st.site = &if_emlrtRSI;
    for (b_i = 0; b_i < m; b_i++) {
      ycol->data[b_i] = b->data[(idx->data[b_i] + b->size[0]) - 1];
    }

    c_st.site = &jf_emlrtRSI;
    for (b_i = 0; b_i < m; b_i++) {
      b->data[b_i + b->size[0]] = ycol->data[b_i];
    }

    i = ycol->size[0];
    ycol->size[0] = idx->size[0];
    emxEnsureCapacity_real_T(&st, ycol, i, &pc_emlrtRTEI);
    m = idx->size[0];
    for (i = 0; i < m; i++) {
      ycol->data[i] = idx->data[i];
    }

    emxFree_int32_T(&idx);
    nb = 0;
    m = a->size[0];
    k = 0;
    while (k + 1 <= m) {
      k0 = k;
      do {
        exitg1 = 0;
        k++;
        if (k + 1 > m) {
          exitg1 = 1;
        } else {
          st.site = &ye_emlrtRSI;
          overflow = false;
          j = 0;
          exitg2 = false;
          while ((!exitg2) && (j < 2)) {
            b_st.site = &kf_emlrtRSI;
            b_a = b->data[k0 + b->size[0] * j];
            c_b = b->data[k + b->size[0] * j];
            c_st.site = &lf_emlrtRSI;
            d_st.site = &mf_emlrtRSI;
            absx = muDoubleScalarAbs(c_b / 2.0);
            if ((!muDoubleScalarIsInf(absx)) && (!muDoubleScalarIsNaN(absx))) {
              if (absx <= 2.2250738585072014E-308) {
                absx = 4.94065645841247E-324;
              } else {
                frexp(absx, &exponent);
                absx = ldexp(1.0, exponent - 53);
              }
            } else {
              absx = rtNaN;
            }

            if ((muDoubleScalarAbs(c_b - b_a) < absx) || (muDoubleScalarIsInf
                 (b_a) && muDoubleScalarIsInf(c_b) && ((b_a > 0.0) == (c_b > 0.0))))
            {
              b_overflow = true;
            } else {
              b_overflow = false;
            }

            if (!b_overflow) {
              overflow = true;
              exitg2 = true;
            } else {
              j++;
            }
          }

          if (overflow) {
            exitg1 = 1;
          }
        }
      } while (exitg1 == 0);

      nb++;
      b->data[nb - 1] = b->data[k0];
      b->data[(nb + b->size[0]) - 1] = b->data[k0 + b->size[0]];
      st.site = &af_emlrtRSI;
      if ((k0 + 1 <= k) && (k > 2147483646)) {
        b_st.site = &ff_emlrtRSI;
        check_forloop_overflow_error(&b_st);
      }

      ycol->data[nb - 1] = ycol->data[k0];
    }

    if (nb > a->size[0]) {
      emlrtErrorWithMessageIdR2018a(sp, &d_emlrtRTEI,
        "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
    }

    emxInit_real_T(sp, &b_b, 2, &qc_emlrtRTEI, true);
    if (1 > nb) {
      m = 0;
    } else {
      m = nb;
    }

    i = b_b->size[0] * b_b->size[1];
    b_b->size[0] = m;
    b_b->size[1] = 2;
    emxEnsureCapacity_real_T(sp, b_b, i, &qc_emlrtRTEI);
    for (i = 0; i < m; i++) {
      b_b->data[i] = b->data[i];
    }

    for (i = 0; i < m; i++) {
      b_b->data[i + b_b->size[0]] = b->data[i + b->size[0]];
    }

    i = b->size[0] * b->size[1];
    b->size[0] = b_b->size[0];
    b->size[1] = 2;
    emxEnsureCapacity_real_T(sp, b, i, &rc_emlrtRTEI);
    m = b_b->size[0] * b_b->size[1];
    for (i = 0; i < m; i++) {
      b->data[i] = b_b->data[i];
    }

    emxFree_real_T(&b_b);
    i = ndx->size[0];
    ndx->size[0] = nb;
    emxEnsureCapacity_int32_T(sp, ndx, i, &sc_emlrtRTEI);
    st.site = &bf_emlrtRSI;
    if ((1 <= nb) && (nb > 2147483646)) {
      b_st.site = &ff_emlrtRSI;
      check_forloop_overflow_error(&b_st);
    }

    for (k = 0; k < nb; k++) {
      ndx->data[k] = (int32_T)ycol->data[k];
    }

    emxFree_real_T(&ycol);
  }

  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

void unique_vector(const emlrtStack *sp, const emxArray_real_T *a,
                   emxArray_real_T *b)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack st;
  emxArray_int32_T *idx;
  emxArray_int32_T *iwork;
  real_T absx;
  real_T x;
  int32_T iv[2];
  int32_T b_i;
  int32_T exitg2;
  int32_T exponent;
  int32_T i;
  int32_T i2;
  int32_T j;
  int32_T k;
  int32_T kEnd;
  int32_T n;
  int32_T na;
  int32_T p;
  int32_T pEnd;
  int32_T q;
  int32_T qEnd;
  boolean_T b_p;
  boolean_T exitg1;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  emxInit_int32_T(sp, &idx, 1, &xc_emlrtRTEI, true);
  na = a->size[0];
  st.site = &bh_emlrtRSI;
  n = a->size[0] + 1;
  i = idx->size[0];
  idx->size[0] = a->size[0];
  emxEnsureCapacity_int32_T(&st, idx, i, &xc_emlrtRTEI);
  b_i = a->size[0];
  for (i = 0; i < b_i; i++) {
    idx->data[i] = 0;
  }

  if (a->size[0] != 0) {
    emxInit_int32_T(&st, &iwork, 1, &uc_emlrtRTEI, true);
    b_st.site = &kh_emlrtRSI;
    i = iwork->size[0];
    iwork->size[0] = a->size[0];
    emxEnsureCapacity_int32_T(&b_st, iwork, i, &nc_emlrtRTEI);
    b_i = a->size[0] - 1;
    c_st.site = &gf_emlrtRSI;
    if ((1 <= a->size[0] - 1) && (a->size[0] - 1 > 2147483645)) {
      d_st.site = &ff_emlrtRSI;
      check_forloop_overflow_error(&d_st);
    }

    for (k = 1; k <= b_i; k += 2) {
      if ((a->data[k - 1] <= a->data[k]) || muDoubleScalarIsNaN(a->data[k])) {
        idx->data[k - 1] = k;
        idx->data[k] = k + 1;
      } else {
        idx->data[k - 1] = k + 1;
        idx->data[k] = k;
      }
    }

    if ((a->size[0] & 1) != 0) {
      idx->data[a->size[0] - 1] = a->size[0];
    }

    b_i = 2;
    while (b_i < n - 1) {
      i2 = b_i << 1;
      j = 1;
      for (pEnd = b_i + 1; pEnd < n; pEnd = qEnd + b_i) {
        p = j;
        q = pEnd - 1;
        qEnd = j + i2;
        if (qEnd > n) {
          qEnd = n;
        }

        k = 0;
        kEnd = qEnd - j;
        while (k + 1 <= kEnd) {
          absx = a->data[idx->data[q] - 1];
          i = idx->data[p - 1];
          if ((a->data[i - 1] <= absx) || muDoubleScalarIsNaN(absx)) {
            iwork->data[k] = i;
            p++;
            if (p == pEnd) {
              while (q + 1 < qEnd) {
                k++;
                iwork->data[k] = idx->data[q];
                q++;
              }
            }
          } else {
            iwork->data[k] = idx->data[q];
            q++;
            if (q + 1 == qEnd) {
              while (p < pEnd) {
                k++;
                iwork->data[k] = idx->data[p - 1];
                p++;
              }
            }
          }

          k++;
        }

        c_st.site = &hf_emlrtRSI;
        for (k = 0; k < kEnd; k++) {
          idx->data[(j + k) - 1] = iwork->data[k];
        }

        j = qEnd;
      }

      b_i = i2;
    }

    emxFree_int32_T(&iwork);
  }

  i = b->size[0];
  b->size[0] = a->size[0];
  emxEnsureCapacity_real_T(sp, b, i, &yc_emlrtRTEI);
  st.site = &ch_emlrtRSI;
  if ((1 <= a->size[0]) && (a->size[0] > 2147483646)) {
    b_st.site = &ff_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  for (k = 0; k < na; k++) {
    b->data[k] = a->data[idx->data[k] - 1];
  }

  emxFree_int32_T(&idx);
  k = 0;
  while ((k + 1 <= na) && muDoubleScalarIsInf(b->data[k]) && (b->data[k] < 0.0))
  {
    k++;
  }

  pEnd = k;
  k = a->size[0];
  while ((k >= 1) && muDoubleScalarIsNaN(b->data[k - 1])) {
    k--;
  }

  p = a->size[0] - k;
  exitg1 = false;
  while ((!exitg1) && (k >= 1)) {
    absx = b->data[k - 1];
    if (muDoubleScalarIsInf(absx) && (absx > 0.0)) {
      k--;
    } else {
      exitg1 = true;
    }
  }

  b_i = (a->size[0] - k) - p;
  q = 0;
  if (pEnd > 0) {
    q = 1;
    st.site = &dh_emlrtRSI;
    if (pEnd > 2147483646) {
      b_st.site = &ff_emlrtRSI;
      check_forloop_overflow_error(&b_st);
    }
  }

  while (pEnd + 1 <= k) {
    x = b->data[pEnd];
    i2 = pEnd;
    do {
      exitg2 = 0;
      pEnd++;
      if (pEnd + 1 > k) {
        exitg2 = 1;
      } else {
        st.site = &eh_emlrtRSI;
        b_st.site = &lf_emlrtRSI;
        c_st.site = &mf_emlrtRSI;
        absx = muDoubleScalarAbs(x / 2.0);
        if ((!muDoubleScalarIsInf(absx)) && (!muDoubleScalarIsNaN(absx))) {
          if (absx <= 2.2250738585072014E-308) {
            absx = 4.94065645841247E-324;
          } else {
            frexp(absx, &exponent);
            absx = ldexp(1.0, exponent - 53);
          }
        } else {
          absx = rtNaN;
        }

        if ((muDoubleScalarAbs(x - b->data[pEnd]) < absx) ||
            (muDoubleScalarIsInf(b->data[pEnd]) && muDoubleScalarIsInf(x) &&
             ((b->data[pEnd] > 0.0) == (x > 0.0)))) {
          b_p = true;
        } else {
          b_p = false;
        }

        if (!b_p) {
          exitg2 = 1;
        }
      }
    } while (exitg2 == 0);

    q++;
    b->data[q - 1] = x;
    st.site = &fh_emlrtRSI;
    if ((i2 + 1 <= pEnd) && (pEnd > 2147483646)) {
      b_st.site = &ff_emlrtRSI;
      check_forloop_overflow_error(&b_st);
    }
  }

  if (b_i > 0) {
    q++;
    b->data[q - 1] = b->data[k];
    st.site = &gh_emlrtRSI;
    if (b_i > 2147483646) {
      b_st.site = &ff_emlrtRSI;
      check_forloop_overflow_error(&b_st);
    }
  }

  pEnd = k + b_i;
  st.site = &hh_emlrtRSI;
  if ((1 <= p) && (p > 2147483646)) {
    b_st.site = &ff_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  for (j = 0; j < p; j++) {
    q++;
    b->data[q - 1] = b->data[pEnd + j];
  }

  if (q > a->size[0]) {
    emlrtErrorWithMessageIdR2018a(sp, &h_emlrtRTEI,
      "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
  }

  if (1 > q) {
    i = 0;
  } else {
    i = q;
  }

  iv[0] = 1;
  iv[1] = i;
  st.site = &ih_emlrtRSI;
  indexShapeCheck(&st, b->size[0], iv);
  b_i = b->size[0];
  b->size[0] = i;
  emxEnsureCapacity_real_T(sp, b, b_i, &ad_emlrtRTEI);
  st.site = &jh_emlrtRSI;
  if ((1 <= q) && (q > 2147483646)) {
    b_st.site = &ff_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (unique.c) */
