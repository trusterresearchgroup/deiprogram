#!/bin/bash

neper -T -n 30 -id $1 -periodicity 1 -dim 2
neper -M -elttype tri -order 1 -format msh n30-id$1.tess
