/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_DEIPFunc3_api.h
 *
 * Code generation for function '_coder_DEIPFunc3_api'
 *
 */

#pragma once

/* Include files */
#include "rtwtypes.h"
#include "emlrt.h"
#include "mex.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Function Declarations */
void DEIPFunc3_api(const mxArray * const prhs[12], int32_T nlhs, const mxArray
                   *plhs[41]);

/* End of code generation (_coder_DEIPFunc3_api.h) */
