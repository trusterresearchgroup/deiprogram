% Demonstration of Abaqus reader/writer for Explicit input file.
% Domain: Cube-shaped unit cell containing two materials.
%   CZM couplers are inserted between them. Input file is constructed with
%   artificial loading conditions but able to execute with Abaqus Explicit.
%
% Last revision: 1/19/2022 TJT
clear
clc
close all
%%
Abaqfile = 'OneAgg.inp';
ndm = 3;
AbaqusInputReader

% Insert CZM elements
numnpCG = numnp;
numelCG = numel;
nen_bulk = 4;
InterTypes = tril(ones(nummat),-1); % put CZM between all material interfaces
[NodesOnElement,RegionOnElement,Coordinates,numnp,Output_data] ...
    = DEIPFunction(InterTypes,NodesOnElement,RegionOnElement,Coordinates,numnp,numel,nummat,nen,ndm);


% Insert CZ couplers
% CZ element stiffness
CZprop = [100000 3 0.2];

[NodesOnElement,RegionOnElement,Coordinates,numnp,nen,numel,nummat,RegionsOnInterface,MateT,MatTypeTable...
] = InterFunction(1,InterTypes,NodesOnElement,RegionOnElement,Coordinates,numnp,numel,nummat,nen,ndm,Output_data,7,CZprop,MateT,MatTypeTable);

ProbType = [numnp numel nummat 3 3 nen];

% Output quantity flags
DHist = 1;
FHist = 1;
SHist = 1;
SEHist = 1;
%%
% Write output file
AbaqOut = 'OneAggCZM.inp';
DGCZMflag = 1; % turn on addition of CZM stuff
Data{2}(1:numelCG,2:nen_bulk+1) = NodesOnElement(1:numelCG,1:nen_bulk); % overwrite the connectivity with the separated materials
NodesCZM = [(numnpCG+1:numnp)' Coordinates(numnpCG+1:numnp,:)]; % new duplicated nodes
ElsetCZM{1} = (numelCG+1:numel)'; % new element set for CZM
NodesOnElementCZM{1} = [ElsetCZM{1} NodesOnElement(ElsetCZM{1},1:nen)]; % new CZM elements
eltypeCZM{1,1} = 'COH3D6'; % element type
eltypeCZM{2,1} = 3; % Material number; can also be an ACTIVE, existing material name
Mateczmstr{1} = '*Damage Initiation, criterion=MAXS';
Mateczmstr{2} = '3.,3.,3.';
Mateczmstr{3} = '*Damage Evolution, type=DISPLACEMENT';
Mateczmstr{4} = ' 0.2,';
Mateczmstr{5} = '*Elastic, type=TRACTION';
Mateczmstr{6} = '100000.,100000.,100000.';
Mateczmstr{7} = '*Density';
Mateczmstr{8} = '240,'; % made-up value of density, like elastic stiffness; user needs to choose a reasonable value
MateCZM{1} = Mateczmstr; % this is where all the text for the material stuff goes
%%
AbaqusInputWriter