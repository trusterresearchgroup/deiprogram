function VTKOutputWriter(stepFE,stepPV,nodeelemcoup,nenPV,otherflag,flagout,fileprop, ...
            Coordinates,NodesOnElement,RegionOnElement,DispList,StreList, ...
            StreListE) %#ok<*INUSL,INUSD>
% Tim Truster
% 05/31/2020
% 
% Wrapping function to output VTK files for Paraview
%
% Default usage (for copy/paste):
% VTKOutputWriter([1 1 1],[1 1 1],[1 numnp 1 numel 0 0],0,[3 0 0],[1 1 0 0 0 0 0 0],[],Coordinates,NodesOnElement,RegionOnElement)
%
% Inputs:
%  stepFE = Steps from FE to output: [starting step, increment to step, and last step]
%  stepPV = Steps for VTK file: [starting step, increment to step, and last step]
%  nodeelemcoup = identifiers for nodes and elements and couplers to output
%      nodestart = (1) default starting node
%      nodeend = (size(Coordinates,1)) default ending node
%      elemstart = (1) default starting element
%      elemend = (size(NodesOnElements,1)) default ending element
%      coupstart = (0) default starting coupler
%      coupend = (0) default ending coupler
%  nenPV = Number of nodes per solid element; either a constant, or 0 to
%  calculate
%  otherflag = [ndm ielNL(1)], defaults [3 7]
%      ndm = number of spatial dimensions
%      ielNL(1) = 7 for CZ couplers, 8 for DG couplers
%  flagout = flags for output to print to file, 0 for no, 1 for yes
%      mflag = (1); % flag for printing region colors
%      disflag = (0); % flag for displacement
%      strflag = (0); % flag for stress postprocessing
%      st2flag = (0); % flag for stress vectors/scalars
%      serflag = (0); % flag for elemental stress postprocessing
%      se2flag = (0); % flag for elemental stress vectors/scalars
%      cflag = (0); % flag for couplers to be printed
%      cseflag = (0); % flag for coupler element stresses
%  fileprop = 
%      dfoldername = 'ParaOut'; %default name of folder for files to be placed
%      filename = 'out'; %prefNodesOnElement filename (e.g. 'out' -> out01.vtk)
%      header = 'Elasticity'; %text at top of VTK files
%      foldername = [] default for interactive path to folder to print files
%  Coordinates = nodal coordinates [x y (z)]
%  NodesOnElement = element connectivity [n1 n2 ... nnen]
%  {DispList} = displacements(1:ndf,1:numnpe,1:stepmax)
%  {StreList} = stresses(1:npstr,1:numnpe,1:stepmax)
%  {StreListE} = stresses(1:nestr,1:numnpe,1:stepmax)

%% Get the parameters
% Steps to output: starting step, increment to step, and last step
if isempty(stepFE)
    stepFE = [1 1 1];
end
stepstart = stepFE(1); %#ok<*NASGU>
stepinc = stepFE(2);
stepstop = stepFE(3);


if isempty(stepPV)
    stepPV = [1 1 1];
end
startp = stepPV(1); % first step # for VTK file
incp = stepPV(2); % increment for VTK file
stopp = stepPV(3); % last step # for VTK file

if isempty(nodeelemcoup)
    nodestart = 1;
    nodeend = size(Coordinates,1);
    elemstart = 1;
    elemend = size(NodesOnElement,1);
    coupstart = 0;
    coupend = 0;
else
    % Nodes to output
    nodestart = nodeelemcoup(1);
    nodeend = nodeelemcoup(2);
    % Elements to output
    elemstart = nodeelemcoup(3);
    elemend = nodeelemcoup(4);
    % Couplers to output
    coupstart = nodeelemcoup(5);
    coupend = nodeelemcoup(6);
end

% Number of nodes per element; assumed constant for all elements elemstart 
% to elemend
if isempty(nenPV)
    nenPV = 0; % nen to use for Paraview file
end

% flags to determine size of outputs
if isempty(otherflag)
    otherflag= [3 7];
end
ndm = otherflag(1);
ielNL = otherflag(2);

if isempty(flagout)
    flagout= [1 0 0 0 0 0 0 0];
end
mflag = flagout(1); % flag for printing region colors
disflag = flagout(2); % flag for displacement
strflag = flagout(3); % flag for stress postprocessing
st2flag = flagout(4); % flag for stress vectors/scalars
serflag = flagout(5); % flag for elemental stress postprocessing
se2flag = flagout(6); % flag for elemental stress vectors/scalars
cflag = flagout(7); % flag for couplers
cseflag = flagout(8); % flag for coupler element stresses

if isempty(fileprop)
    dfoldername = 'ParaOut'; %default name of folder for files to be placed
    filename = 'out'; %prefNodesOnElement filename (e.g. 'out' -> out01.vtk)
    header = 'Elasticity'; %text at top of VTK files
    userdefaultdir = pwd; %list your default output root directory here
    foldername = uigetdir(userdefaultdir,'Select folder to write VTK files');
else
    dfoldername = fileprop.dfoldername; %default name of folder for files to be placed
    filename = fileprop.filename; %prefNodesOnElement filename (e.g. 'out' -> out01.vtk)
    header = fileprop.header; %text at top of VTK files
    if isfield(fileprop, 'foldername')
    foldername = fileprop.foldername;
    else
    userdefaultdir = pwd; %list your default output root directory here
    foldername = uigetdir(userdefaultdir,'Select folder to write VTK files');
    end
end

%% Actually call the writer
PostParaview

end