/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc3_initialize.c
 *
 * Code generation for function 'DEIPFunc3_initialize'
 *
 */

/* Include files */
#include "DEIPFunc3_initialize.h"
#include "DEIPFunc3_data.h"
#include "_coder_DEIPFunc3_mex.h"
#include "rt_nonfinite.h"

/* Function Definitions */
void DEIPFunc3_initialize(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mex_InitInfAndNan();
  mexFunctionCreateRootTLS();
  emlrtBreakCheckR2012bFlagVar = emlrtGetBreakCheckFlagAddressR2012b();
  st.tls = emlrtRootTLSGlobal;
  emlrtClearAllocCountR2012b(&st, false, 0U, 0);
  emlrtEnterRtStackR2012b(&st);
  emlrtFirstTimeR2012b(emlrtRootTLSGlobal);
}

/* End of code generation (DEIPFunc3_initialize.c) */
