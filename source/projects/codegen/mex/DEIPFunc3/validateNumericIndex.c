/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * validateNumericIndex.c
 *
 * Code generation for function 'validateNumericIndex'
 *
 */

/* Include files */
#include "validateNumericIndex.h"
#include "DEIPFunc3_data.h"
#include "DEIPFunc3_types.h"
#include "rt_nonfinite.h"
#include "mwmathutil.h"

/* Variable Definitions */
static emlrtRTEInfo bb_emlrtRTEI = { 11,/* lineNo */
  27,                                  /* colNo */
  "sparse/sparse_validateNumericIndex",/* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\private\\validateNumericIndex.m"/* pName */
};

/* Function Definitions */
void b_sparse_validateNumericIndex(const emlrtStack *sp, int32_T upperBound,
  real_T idx)
{
  if ((!(muDoubleScalarFloor(idx) == idx)) || muDoubleScalarIsInf(idx) || (!(idx
        > 0.0))) {
    emlrtErrorWithMessageIdR2018a(sp, &bb_emlrtRTEI, "Coder:MATLAB:badsubscript",
      "Coder:MATLAB:badsubscript", 0);
  }

  if (!(idx <= upperBound)) {
    emlrtErrorWithMessageIdR2018a(sp, &cb_emlrtRTEI,
      "Coder:builtins:IndexOutOfBounds", "Coder:builtins:IndexOutOfBounds", 6, 6,
      idx, 12, 1, 12, upperBound);
  }
}

void c_sparse_validateNumericIndex(const emlrtStack *sp, int32_T upperBound,
  const emxArray_real_T *idx)
{
  real_T d;
  int32_T i;
  int32_T k;
  i = idx->size[1];
  for (k = 0; k < i; k++) {
    d = idx->data[k];
    if ((!(muDoubleScalarFloor(d) == d)) || muDoubleScalarIsInf(d) || (!(d > 0.0)))
    {
      emlrtErrorWithMessageIdR2018a(sp, &bb_emlrtRTEI,
        "Coder:MATLAB:badsubscript", "Coder:MATLAB:badsubscript", 0);
    }

    if (!(d <= upperBound)) {
      emlrtErrorWithMessageIdR2018a(sp, &cb_emlrtRTEI,
        "Coder:builtins:IndexOutOfBounds", "Coder:builtins:IndexOutOfBounds", 6,
        6, d, 12, 1, 12, upperBound);
    }
  }
}

void sparse_validateNumericIndex(const emlrtStack *sp, int32_T upperBound, const
  emxArray_real_T *idx)
{
  int32_T i;
  int32_T k;
  i = idx->size[0];
  for (k = 0; k < i; k++) {
    if ((!(muDoubleScalarFloor(idx->data[k]) == idx->data[k])) ||
        muDoubleScalarIsInf(idx->data[k]) || (!(idx->data[k] > 0.0))) {
      emlrtErrorWithMessageIdR2018a(sp, &bb_emlrtRTEI,
        "Coder:MATLAB:badsubscript", "Coder:MATLAB:badsubscript", 0);
    }

    if (!(idx->data[k] <= upperBound)) {
      emlrtErrorWithMessageIdR2018a(sp, &cb_emlrtRTEI,
        "Coder:builtins:IndexOutOfBounds", "Coder:builtins:IndexOutOfBounds", 6,
        6, idx->data[k], 12, 1, 12, upperBound);
    }
  }
}

/* End of code generation (validateNumericIndex.c) */
