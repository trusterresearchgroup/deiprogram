% Patch test for rectangular domain of Q4 elements with DG couplers along
% interfaces between regions. User can modify the elements belonging to
% each region.
% Domain: 2x1 rectangle
% Loading: Prescribed displacement of 0.1 on right edge.
%
% Last revision: 10/28/2017 TJT

clear
% clc

nen = 4;
nel = 4;
% Mesh with 6x6 tiling
nu = 6;
nv = 6;

L_x = 4.000000;
L_y = L_x;
xl = [1 0   0
      2 L_x 0
      4 0   L_y
      3 L_x L_y];
type = 'cart';
rinc = nu;
sinc = nv;
node1 = 1;
elmt1 = 1;
mat = 1;
rskip = 0;
btype = 0;
[Coordinates,NodesOnElement,RegionOnElement,numnp,numel] = block2d(type,rinc,sinc,node1,elmt1,mat,rskip,btype,xl,nen);
Coordinates = Coordinates';
NodesOnElement = NodesOnElement';

rsinc=rinc*sinc;
% Create periodic boundary conditions
[MPCList,numMPC,NodeBC] = block2dMPC(rinc,sinc);
NodeBC = NodeBC(1:2,:);
numBC = size(NodeBC,1);

RegionOnElement(1:2) = 3;
RegionOnElement(3:4) = 2;
RegionOnElement(7:8) = 2;
nummat = 3;
MatTypeTable = [1 2 3; 1 1 1];
MateT = [1 1 1]'*[100 0.25 1];
% Output quantity flags
DHist = 1;
FHist = 1;
SHist = 1;
SEHist = 1;

% Generate CZM interface: pairs of elements and faces, duplicate the nodes,
% update the connectivities
numnpCG = numnp;
usePBC = 2; % flag to turn on keeping PBC
ndm = 2;
InterTypes = tril(ones(nummat),-1); % put DG between all material interfaces
[NodesOnElement,RegionOnElement,Coordinates,numnp,Output_data,MPCList,numMPC] = ...
    DEIPFunction(InterTypes,NodesOnElement,RegionOnElement,Coordinates,numnp,numel,nummat,nen,ndm,usePBC,numMPC,MPCList);

% Insert couplers; use extended DG mesh with ghost elements so that
% couplers appear properly on the domain boundary
pencoeff = 1e9;
CornerXYZ = [L_x 0.000000
             0.000000 L_y];

[NodesOnElement,RegionOnElement,Coordinates,numnp,nen,numel,nummat, ...
    RegionsOnInterface,MateT,MatTypeTable,NodeTypeNum,numMPC,MPCList] ...
  = InterFunction(5,InterTypes,NodesOnElement,RegionOnElement,Coordinates,...
  numnp,numel,nummat,nen,ndm,Output_data,8,0,MateT,MatTypeTable,usePBC,numMPC,MPCList,...
  pencoeff,CornerXYZ);
     
%% % apply macrostress
NodeLoad = [NodeTypeNum(2)   1 0
            NodeTypeNum(2)   2 10
            NodeTypeNum(2)+1 1 10
            NodeTypeNum(2)+1 2 0];
numNodalF = length(NodeLoad);
NodeBC = [NodeBC; [rinc+1 2 0]];
numBC = numBC+1;     

%% % apply macrostrain
% NodeLoad2 = [NodeTypeNum(2)   1 0
%              NodeTypeNum(2)   2 0.01
%              NodeTypeNum(2)+1 1 0.01
%              NodeTypeNum(2)+1 2 0];
% NodeBC = [NodeBC; NodeLoad2];
% numBC = length(NodeBC);

ProbType = [numnp numel nummat ndm ndm nen];


%% Plots for after FEA_Program is called

% % Plot the u_x displacement
% plotNodeCont2(Coordinates+Node_U_V*10, Node_U_V(:,1), NodesOnElement, 2, 1:(rsinc), [1 1 1], 0,[3 4 6 9 0]) 
% % Extract the macroscale strain and stress*volume
