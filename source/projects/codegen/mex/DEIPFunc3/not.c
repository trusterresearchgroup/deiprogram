/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * not.c
 *
 * Code generation for function 'not'
 *
 */

/* Include files */
#include "not.h"
#include "DEIPFunc3_data.h"
#include "DEIPFunc3_types.h"
#include "eml_int_forloop_overflow_check.h"
#include "rt_nonfinite.h"
#include "sparse1.h"
#include "mwmathutil.h"

/* Variable Definitions */
static emlrtRSInfo jq_emlrtRSI = { 7,  /* lineNo */
  "sparse/not",                        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\not.m"/* pathName */
};

static emlrtRSInfo kq_emlrtRSI = { 8,  /* lineNo */
  "sparse/not",                        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\not.m"/* pathName */
};

static emlrtRSInfo lq_emlrtRSI = { 20, /* lineNo */
  "sparse/not",                        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\not.m"/* pathName */
};

static emlrtRSInfo mq_emlrtRSI = { 30, /* lineNo */
  "sparse/not",                        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\not.m"/* pathName */
};

static emlrtRTEInfo mb_emlrtRTEI = { 36,/* lineNo */
  9,                                   /* colNo */
  "sparse/not",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\not.m"/* pName */
};

static emlrtRTEInfo nb_emlrtRTEI = { 27,/* lineNo */
  13,                                  /* colNo */
  "sparse/not",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\not.m"/* pName */
};

/* Function Definitions */
void sparse_not(const emlrtStack *sp, const emxArray_real_T *S_d, const
                emxArray_int32_T *S_colidx, const emxArray_int32_T *S_rowidx,
                emxArray_boolean_T *out_d, emxArray_int32_T *out_colidx,
                emxArray_int32_T *out_rowidx)
{
  emlrtStack b_st;
  emlrtStack st;
  int32_T curPosition;
  int32_T nnzInThisRowOfS;
  int32_T outNNZ;
  int32_T writeEnd;
  int32_T writeRow;
  int32_T writeStart;
  boolean_T b_overflow;
  boolean_T overflow;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &jq_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  if (2 - S_colidx->data[S_colidx->size[0] - 1] == 0) {
    outNNZ = 0;
  } else {
    outNNZ = 2 - S_colidx->data[S_colidx->size[0] - 1];
  }

  st.site = &kq_emlrtRSI;
  c_sparse_spallocLike(&st, outNNZ, out_d, out_colidx, out_rowidx);
  if (1 > outNNZ) {
    outNNZ = 0;
  }

  for (nnzInThisRowOfS = 0; nnzInThisRowOfS < outNNZ; nnzInThisRowOfS++) {
    out_d->data[nnzInThisRowOfS] = true;
  }

  curPosition = 0;
  out_colidx->data[0] = 1;
  if (S_colidx->data[0] == S_colidx->data[1]) {
    out_rowidx->data[0] = 1;
    curPosition = 1;
  } else {
    outNNZ = S_rowidx->data[S_colidx->data[0] - 1];
    st.site = &lq_emlrtRSI;
    if (1 > outNNZ - 1) {
      overflow = false;
    } else {
      overflow = (S_rowidx->data[S_colidx->data[0] - 1] - 1 > 2147483646);
    }

    if (overflow) {
      b_st.site = &fg_emlrtRSI;
      check_forloop_overflow_error(&b_st);
    }

    for (writeRow = 0; writeRow <= outNNZ - 2; writeRow++) {
      out_rowidx->data[curPosition] = writeRow + 1;
      curPosition++;
    }

    nnzInThisRowOfS = (S_colidx->data[1] - S_colidx->data[0]) - 2;
    if (0 <= nnzInThisRowOfS) {
      writeStart = outNNZ + 1;
      writeEnd = S_rowidx->data[S_colidx->data[0]] - 1;
      if (S_rowidx->data[S_colidx->data[0] - 1] + 1 > writeEnd) {
        b_overflow = false;
      } else {
        b_overflow = (writeEnd > 2147483646);
      }
    }

    for (outNNZ = 0; outNNZ <= nnzInThisRowOfS; outNNZ++) {
      if (muDoubleScalarIsNaN(S_d->data[S_colidx->data[0] - 1])) {
        emlrtErrorWithMessageIdR2018a(sp, &nb_emlrtRTEI, "MATLAB:nologicalnan",
          "MATLAB:nologicalnan", 0);
      }

      st.site = &mq_emlrtRSI;
      if (b_overflow) {
        b_st.site = &fg_emlrtRSI;
        check_forloop_overflow_error(&b_st);
      }

      for (writeRow = writeStart; writeRow <= writeEnd; writeRow++) {
        out_rowidx->data[curPosition] = writeRow;
        curPosition++;
      }
    }

    nnzInThisRowOfS += S_colidx->data[0];
    if (muDoubleScalarIsNaN(S_d->data[nnzInThisRowOfS])) {
      emlrtErrorWithMessageIdR2018a(sp, &mb_emlrtRTEI, "MATLAB:nologicalnan",
        "MATLAB:nologicalnan", 0);
    }

    writeStart = S_rowidx->data[nnzInThisRowOfS] + 1;
    for (writeRow = writeStart; writeRow < 2; writeRow++) {
      out_rowidx->data[curPosition] = 1;
      curPosition++;
    }
  }

  out_colidx->data[1] = curPosition + 1;
}

/* End of code generation (not.c) */
