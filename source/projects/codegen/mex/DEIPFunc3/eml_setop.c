/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * eml_setop.c
 *
 * Code generation for function 'eml_setop'
 *
 */

/* Include files */
#include "eml_setop.h"
#include "DEIPFunc3_emxutil.h"
#include "DEIPFunc3_types.h"
#include "issorted.h"
#include "rt_nonfinite.h"
#include "mwmathutil.h"
#include <math.h>

/* Variable Definitions */
static emlrtRSInfo ni_emlrtRSI = { 212,/* lineNo */
  "do_vectors",                        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pathName */
};

static emlrtRSInfo ij_emlrtRSI = { 615,/* lineNo */
  "do_rows",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pathName */
};

static emlrtRSInfo jj_emlrtRSI = { 618,/* lineNo */
  "do_rows",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pathName */
};

static emlrtRSInfo po_emlrtRSI = { 215,/* lineNo */
  "do_vectors",                        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pathName */
};

static emlrtRTEInfo i_emlrtRTEI = { 213,/* lineNo */
  13,                                  /* colNo */
  "do_vectors",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pName */
};

static emlrtRTEInfo j_emlrtRTEI = { 380,/* lineNo */
  5,                                   /* colNo */
  "do_vectors",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pName */
};

static emlrtRTEInfo k_emlrtRTEI = { 418,/* lineNo */
  5,                                   /* colNo */
  "do_vectors",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pName */
};

static emlrtRTEInfo l_emlrtRTEI = { 616,/* lineNo */
  13,                                  /* colNo */
  "do_rows",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pName */
};

static emlrtRTEInfo m_emlrtRTEI = { 619,/* lineNo */
  13,                                  /* colNo */
  "do_rows",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pName */
};

static emlrtRTEInfo n_emlrtRTEI = { 806,/* lineNo */
  1,                                   /* colNo */
  "do_rows",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pName */
};

static emlrtRTEInfo o_emlrtRTEI = { 811,/* lineNo */
  1,                                   /* colNo */
  "do_rows",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pName */
};

static emlrtRTEInfo ib_emlrtRTEI = { 216,/* lineNo */
  13,                                  /* colNo */
  "do_vectors",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pName */
};

static emlrtRTEInfo lb_emlrtRTEI = { 391,/* lineNo */
  9,                                   /* colNo */
  "do_vectors",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pName */
};

static emlrtRTEInfo fd_emlrtRTEI = { 194,/* lineNo */
  24,                                  /* colNo */
  "eml_setop",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pName */
};

static emlrtRTEInfo gd_emlrtRTEI = { 195,/* lineNo */
  25,                                  /* colNo */
  "eml_setop",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pName */
};

static emlrtRTEInfo hd_emlrtRTEI = { 386,/* lineNo */
  9,                                   /* colNo */
  "eml_setop",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pName */
};

static emlrtRTEInfo id_emlrtRTEI = { 422,/* lineNo */
  9,                                   /* colNo */
  "eml_setop",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pName */
};

static emlrtRTEInfo od_emlrtRTEI = { 584,/* lineNo */
  20,                                  /* colNo */
  "eml_setop",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pName */
};

static emlrtRTEInfo pd_emlrtRTEI = { 589,/* lineNo */
  21,                                  /* colNo */
  "eml_setop",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pName */
};

static emlrtRTEInfo qd_emlrtRTEI = { 812,/* lineNo */
  1,                                   /* colNo */
  "eml_setop",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pName */
};

static emlrtRTEInfo rd_emlrtRTEI = { 850,/* lineNo */
  9,                                   /* colNo */
  "eml_setop",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pName */
};

static emlrtRTEInfo sd_emlrtRTEI = { 850,/* lineNo */
  5,                                   /* colNo */
  "eml_setop",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pName */
};

static emlrtRTEInfo df_emlrtRTEI = { 196,/* lineNo */
  25,                                  /* colNo */
  "eml_setop",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pName */
};

static emlrtRTEInfo ef_emlrtRTEI = { 397,/* lineNo */
  13,                                  /* colNo */
  "eml_setop",                         /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pName */
};

/* Function Declarations */
static uint8_T relop_rows(const emxArray_real_T *a, int32_T arow, const
  emxArray_real_T *b, int32_T brow);
static real_T skip_to_last_equal_value(int32_T *k, const emxArray_real_T *x);

/* Function Definitions */
static uint8_T relop_rows(const emxArray_real_T *a, int32_T arow, const
  emxArray_real_T *b, int32_T brow)
{
  real_T a_tmp;
  real_T absx;
  real_T b_tmp;
  int32_T exitg1;
  int32_T exponent;
  int32_T k;
  uint8_T p;
  boolean_T CAN_BE_EQUAL;
  boolean_T b_p;
  CAN_BE_EQUAL = true;
  k = 0;
  do {
    exitg1 = 0;
    if (k < 2) {
      a_tmp = a->data[(arow + a->size[0] * k) - 1];
      b_tmp = b->data[(brow + b->size[0] * k) - 1];
      absx = muDoubleScalarAbs(b_tmp / 2.0);
      if ((!muDoubleScalarIsInf(absx)) && (!muDoubleScalarIsNaN(absx))) {
        if (absx <= 2.2250738585072014E-308) {
          absx = 4.94065645841247E-324;
        } else {
          frexp(absx, &exponent);
          absx = ldexp(1.0, exponent - 53);
        }
      } else {
        absx = rtNaN;
      }

      if ((muDoubleScalarAbs(b_tmp - a_tmp) < absx) || (muDoubleScalarIsInf
           (a_tmp) && muDoubleScalarIsInf(b_tmp) && ((a_tmp > 0.0) == (b_tmp >
             0.0)))) {
        b_p = true;
      } else {
        b_p = false;
      }

      if (b_p) {
        k++;
      } else {
        if (muDoubleScalarIsNaN(b_tmp)) {
          CAN_BE_EQUAL = !muDoubleScalarIsNaN(a_tmp);
        } else {
          CAN_BE_EQUAL = ((!muDoubleScalarIsNaN(a_tmp)) && (a_tmp < b_tmp));
        }

        if (CAN_BE_EQUAL) {
          p = 1U;
          exitg1 = 1;
        } else if (muDoubleScalarIsNaN(a_tmp) && muDoubleScalarIsNaN(b_tmp)) {
          CAN_BE_EQUAL = false;
          k++;
        } else {
          p = 2U;
          exitg1 = 1;
        }
      }
    } else {
      p = (uint8_T)!CAN_BE_EQUAL;
      exitg1 = 1;
    }
  } while (exitg1 == 0);

  return p;
}

static real_T skip_to_last_equal_value(int32_T *k, const emxArray_real_T *x)
{
  real_T absx;
  real_T xk;
  int32_T exponent;
  boolean_T exitg1;
  boolean_T p;
  xk = x->data[*k - 1];
  exitg1 = false;
  while ((!exitg1) && (*k < x->size[0])) {
    absx = muDoubleScalarAbs(xk / 2.0);
    if ((!muDoubleScalarIsInf(absx)) && (!muDoubleScalarIsNaN(absx))) {
      if (absx <= 2.2250738585072014E-308) {
        absx = 4.94065645841247E-324;
      } else {
        frexp(absx, &exponent);
        absx = ldexp(1.0, exponent - 53);
      }
    } else {
      absx = rtNaN;
    }

    if ((muDoubleScalarAbs(xk - x->data[*k]) < absx) || (muDoubleScalarIsInf
         (x->data[*k]) && muDoubleScalarIsInf(xk) && ((x->data[*k] > 0.0) == (xk
           > 0.0)))) {
      p = true;
    } else {
      p = false;
    }

    if (p) {
      (*k)++;
    } else {
      exitg1 = true;
    }
  }

  return xk;
}

void b_do_vectors(const emlrtStack *sp, const emxArray_real_T *a, const
                  emxArray_real_T *b, emxArray_real_T *c, emxArray_int32_T *ia,
                  int32_T ib_size[1])
{
  emlrtStack st;
  real_T absx;
  real_T ak;
  real_T bk;
  int32_T b_ialast;
  int32_T exponent;
  int32_T iafirst;
  int32_T ialast;
  int32_T iblast;
  int32_T na;
  int32_T nc;
  int32_T nia;
  boolean_T p;
  st.prev = sp;
  st.tls = sp->tls;
  na = a->size[0];
  iblast = c->size[0];
  c->size[0] = a->size[0];
  emxEnsureCapacity_real_T(sp, c, iblast, &fd_emlrtRTEI);
  iblast = ia->size[0];
  ia->size[0] = a->size[0];
  emxEnsureCapacity_int32_T(sp, ia, iblast, &gd_emlrtRTEI);
  ib_size[0] = 0;
  st.site = &ni_emlrtRSI;
  if (!issorted(&st, a)) {
    emlrtErrorWithMessageIdR2018a(sp, &i_emlrtRTEI,
      "Coder:toolbox:eml_setop_unsortedA", "Coder:toolbox:eml_setop_unsortedA",
      0);
  }

  st.site = &po_emlrtRSI;
  if (!issorted(&st, b)) {
    emlrtErrorWithMessageIdR2018a(sp, &ib_emlrtRTEI,
      "Coder:toolbox:eml_setop_unsortedB", "Coder:toolbox:eml_setop_unsortedB",
      0);
  }

  nc = 0;
  nia = 0;
  iafirst = 0;
  ialast = 1;
  iblast = 1;
  while ((ialast <= na) && (iblast <= b->size[0])) {
    b_ialast = ialast;
    ak = skip_to_last_equal_value(&b_ialast, a);
    ialast = b_ialast;
    bk = skip_to_last_equal_value(&iblast, b);
    absx = muDoubleScalarAbs(bk / 2.0);
    if ((!muDoubleScalarIsInf(absx)) && (!muDoubleScalarIsNaN(absx))) {
      if (absx <= 2.2250738585072014E-308) {
        absx = 4.94065645841247E-324;
      } else {
        frexp(absx, &exponent);
        absx = ldexp(1.0, exponent - 53);
      }
    } else {
      absx = rtNaN;
    }

    if ((muDoubleScalarAbs(bk - ak) < absx) || (muDoubleScalarIsInf(ak) &&
         muDoubleScalarIsInf(bk) && ((ak > 0.0) == (bk > 0.0)))) {
      p = true;
    } else {
      p = false;
    }

    if (p) {
      ialast = b_ialast + 1;
      iafirst = b_ialast;
      iblast++;
    } else {
      if (muDoubleScalarIsNaN(bk)) {
        p = !muDoubleScalarIsNaN(ak);
      } else {
        p = ((!muDoubleScalarIsNaN(ak)) && (ak < bk));
      }

      if (p) {
        nc++;
        nia++;
        c->data[nc - 1] = ak;
        ia->data[nia - 1] = iafirst + 1;
        ialast = b_ialast + 1;
        iafirst = b_ialast;
      } else {
        iblast++;
      }
    }
  }

  while (ialast <= na) {
    iblast = ialast;
    ak = skip_to_last_equal_value(&iblast, a);
    nc++;
    nia++;
    c->data[nc - 1] = ak;
    ia->data[nia - 1] = iafirst + 1;
    ialast = iblast + 1;
    iafirst = iblast;
  }

  if (a->size[0] > 0) {
    if (nia > a->size[0]) {
      emlrtErrorWithMessageIdR2018a(sp, &j_emlrtRTEI,
        "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
    }

    iblast = ia->size[0];
    if (1 > nia) {
      ia->size[0] = 0;
    } else {
      ia->size[0] = nia;
    }

    emxEnsureCapacity_int32_T(sp, ia, iblast, &hd_emlrtRTEI);
    if (nc > a->size[0]) {
      emlrtErrorWithMessageIdR2018a(sp, &k_emlrtRTEI,
        "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
    }

    iblast = c->size[0];
    if (1 > nc) {
      c->size[0] = 0;
    } else {
      c->size[0] = nc;
    }

    emxEnsureCapacity_real_T(sp, c, iblast, &id_emlrtRTEI);
  }
}

void c_do_vectors(const emlrtStack *sp, const emxArray_real_T *a, const
                  emxArray_real_T *b, emxArray_real_T *c, emxArray_int32_T *ia,
                  emxArray_int32_T *ib)
{
  emlrtStack st;
  real_T absx;
  real_T ak;
  real_T bk;
  int32_T b_ialast;
  int32_T b_iblast;
  int32_T exponent;
  int32_T iafirst;
  int32_T ialast;
  int32_T ibfirst;
  int32_T iblast;
  int32_T na;
  int32_T nb;
  int32_T nc;
  int32_T ncmax;
  boolean_T p;
  st.prev = sp;
  st.tls = sp->tls;
  na = a->size[0];
  nb = b->size[0];
  ncmax = muIntScalarMin_sint32(na, nb);
  iafirst = c->size[0];
  c->size[0] = ncmax;
  emxEnsureCapacity_real_T(sp, c, iafirst, &fd_emlrtRTEI);
  iafirst = ia->size[0];
  ia->size[0] = ncmax;
  emxEnsureCapacity_int32_T(sp, ia, iafirst, &gd_emlrtRTEI);
  iafirst = ib->size[0];
  ib->size[0] = ncmax;
  emxEnsureCapacity_int32_T(sp, ib, iafirst, &df_emlrtRTEI);
  st.site = &ni_emlrtRSI;
  if (!issorted(&st, a)) {
    emlrtErrorWithMessageIdR2018a(sp, &i_emlrtRTEI,
      "Coder:toolbox:eml_setop_unsortedA", "Coder:toolbox:eml_setop_unsortedA",
      0);
  }

  st.site = &po_emlrtRSI;
  if (!issorted(&st, b)) {
    emlrtErrorWithMessageIdR2018a(sp, &ib_emlrtRTEI,
      "Coder:toolbox:eml_setop_unsortedB", "Coder:toolbox:eml_setop_unsortedB",
      0);
  }

  nc = 0;
  iafirst = 0;
  ialast = 1;
  ibfirst = 0;
  iblast = 1;
  while ((ialast <= na) && (iblast <= nb)) {
    b_ialast = ialast;
    ak = skip_to_last_equal_value(&b_ialast, a);
    ialast = b_ialast;
    b_iblast = iblast;
    bk = skip_to_last_equal_value(&b_iblast, b);
    iblast = b_iblast;
    absx = muDoubleScalarAbs(bk / 2.0);
    if ((!muDoubleScalarIsInf(absx)) && (!muDoubleScalarIsNaN(absx))) {
      if (absx <= 2.2250738585072014E-308) {
        absx = 4.94065645841247E-324;
      } else {
        frexp(absx, &exponent);
        absx = ldexp(1.0, exponent - 53);
      }
    } else {
      absx = rtNaN;
    }

    if ((muDoubleScalarAbs(bk - ak) < absx) || (muDoubleScalarIsInf(ak) &&
         muDoubleScalarIsInf(bk) && ((ak > 0.0) == (bk > 0.0)))) {
      p = true;
    } else {
      p = false;
    }

    if (p) {
      nc++;
      c->data[nc - 1] = ak;
      ia->data[nc - 1] = iafirst + 1;
      ib->data[nc - 1] = ibfirst + 1;
      ialast = b_ialast + 1;
      iafirst = b_ialast;
      iblast = b_iblast + 1;
      ibfirst = b_iblast;
    } else {
      if (muDoubleScalarIsNaN(bk)) {
        p = !muDoubleScalarIsNaN(ak);
      } else {
        p = ((!muDoubleScalarIsNaN(ak)) && (ak < bk));
      }

      if (p) {
        ialast = b_ialast + 1;
        iafirst = b_ialast;
      } else {
        iblast = b_iblast + 1;
        ibfirst = b_iblast;
      }
    }
  }

  if (ncmax > 0) {
    if (nc > ncmax) {
      emlrtErrorWithMessageIdR2018a(sp, &j_emlrtRTEI,
        "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
    }

    iafirst = ia->size[0];
    if (1 > nc) {
      ia->size[0] = 0;
    } else {
      ia->size[0] = nc;
    }

    emxEnsureCapacity_int32_T(sp, ia, iafirst, &hd_emlrtRTEI);
    if (nc > ncmax) {
      emlrtErrorWithMessageIdR2018a(sp, &lb_emlrtRTEI,
        "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
    }

    iafirst = ib->size[0];
    if (1 > nc) {
      ib->size[0] = 0;
    } else {
      ib->size[0] = nc;
    }

    emxEnsureCapacity_int32_T(sp, ib, iafirst, &ef_emlrtRTEI);
    if (nc > ncmax) {
      emlrtErrorWithMessageIdR2018a(sp, &k_emlrtRTEI,
        "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
    }

    iafirst = c->size[0];
    if (1 > nc) {
      c->size[0] = 0;
    } else {
      c->size[0] = nc;
    }

    emxEnsureCapacity_real_T(sp, c, iafirst, &id_emlrtRTEI);
  }
}

void do_rows(const emlrtStack *sp, const emxArray_real_T *a, const real_T
             b_data[], const int32_T b_size[2], emxArray_real_T *c,
             emxArray_int32_T *ia, int32_T ib_size[1])
{
  emlrtStack st;
  emxArray_real_T b_b_data;
  emxArray_real_T c_b_data;
  emxArray_real_T d_b_data;
  emxArray_real_T e_b_data;
  emxArray_real_T *b_c;
  int32_T iafirst;
  int32_T ialast;
  int32_T ibfirst;
  int32_T iblast;
  int32_T j;
  int32_T na;
  int32_T nc;
  int32_T nia;
  uint8_T r;
  boolean_T exitg1;
  st.prev = sp;
  st.tls = sp->tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  na = a->size[0];
  ibfirst = c->size[0] * c->size[1];
  c->size[0] = a->size[0];
  c->size[1] = 2;
  emxEnsureCapacity_real_T(sp, c, ibfirst, &od_emlrtRTEI);
  nc = -1;
  ibfirst = ia->size[0];
  ia->size[0] = a->size[0];
  emxEnsureCapacity_int32_T(sp, ia, ibfirst, &pd_emlrtRTEI);
  nia = 0;
  ib_size[0] = 0;
  st.site = &ij_emlrtRSI;
  if (!b_issorted(&st, a)) {
    emlrtErrorWithMessageIdR2018a(sp, &l_emlrtRTEI,
      "Coder:toolbox:eml_setop_unsortedRowsA",
      "Coder:toolbox:eml_setop_unsortedRowsA", 0);
  }

  b_b_data.data = (real_T *)&b_data[0];
  b_b_data.size = (int32_T *)&b_size[0];
  b_b_data.allocatedSize = -1;
  b_b_data.numDimensions = 2;
  b_b_data.canFreeData = false;
  st.site = &jj_emlrtRSI;
  if (!b_issorted(&st, &b_b_data)) {
    emlrtErrorWithMessageIdR2018a(sp, &m_emlrtRTEI,
      "Coder:toolbox:eml_setop_unsortedRowsB",
      "Coder:toolbox:eml_setop_unsortedRowsB", 0);
  }

  iafirst = 1;
  ibfirst = 1;
  ialast = 1;
  iblast = 1;
  while ((iafirst <= na) && (ibfirst <= b_size[0])) {
    j = ialast;
    while ((ialast < a->size[0]) && (relop_rows(a, j, a, ialast + 1) == 0)) {
      ialast++;
    }

    j = iblast;
    exitg1 = false;
    while ((!exitg1) && (iblast < b_size[0])) {
      c_b_data.data = (real_T *)&b_data[0];
      c_b_data.size = (int32_T *)&b_size[0];
      c_b_data.allocatedSize = -1;
      c_b_data.numDimensions = 2;
      c_b_data.canFreeData = false;
      e_b_data.data = (real_T *)&b_data[0];
      e_b_data.size = (int32_T *)&b_size[0];
      e_b_data.allocatedSize = -1;
      e_b_data.numDimensions = 2;
      e_b_data.canFreeData = false;
      if (relop_rows(&c_b_data, j, &e_b_data, iblast + 1) == 0) {
        iblast++;
      } else {
        exitg1 = true;
      }
    }

    d_b_data.data = (real_T *)&b_data[0];
    d_b_data.size = (int32_T *)&b_size[0];
    d_b_data.allocatedSize = -1;
    d_b_data.numDimensions = 2;
    d_b_data.canFreeData = false;
    r = relop_rows(a, iafirst, &d_b_data, ibfirst);
    if (r == 0) {
      ialast++;
      iafirst = ialast;
      iblast++;
      ibfirst = iblast;
    } else if (r == 1) {
      nc++;
      nia++;
      c->data[nc] = a->data[iafirst - 1];
      c->data[nc + c->size[0]] = a->data[(iafirst + a->size[0]) - 1];
      ia->data[nia - 1] = iafirst;
      ialast++;
      iafirst = ialast;
    } else {
      iblast++;
      ibfirst = iblast;
    }
  }

  while (ialast <= na) {
    j = ialast;
    while ((ialast < a->size[0]) && (relop_rows(a, j, a, ialast + 1) == 0)) {
      ialast++;
    }

    nc++;
    nia++;
    c->data[nc] = a->data[iafirst - 1];
    c->data[nc + c->size[0]] = a->data[(iafirst + a->size[0]) - 1];
    ia->data[nia - 1] = iafirst;
    ialast++;
    iafirst = ialast;
  }

  if (nc + 1 > a->size[0]) {
    emlrtErrorWithMessageIdR2018a(sp, &n_emlrtRTEI,
      "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
  }

  if (nia > a->size[0]) {
    emlrtErrorWithMessageIdR2018a(sp, &o_emlrtRTEI,
      "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
  }

  emxInit_real_T(sp, &b_c, 2, &rd_emlrtRTEI, true);
  ibfirst = ia->size[0];
  if (1 > nia) {
    ia->size[0] = 0;
  } else {
    ia->size[0] = nia;
  }

  emxEnsureCapacity_int32_T(sp, ia, ibfirst, &qd_emlrtRTEI);
  if (1 > nc + 1) {
    iafirst = -1;
  } else {
    iafirst = nc;
  }

  ibfirst = b_c->size[0] * b_c->size[1];
  b_c->size[0] = iafirst + 1;
  b_c->size[1] = 2;
  emxEnsureCapacity_real_T(sp, b_c, ibfirst, &rd_emlrtRTEI);
  for (ibfirst = 0; ibfirst <= iafirst; ibfirst++) {
    b_c->data[ibfirst] = c->data[ibfirst];
  }

  for (ibfirst = 0; ibfirst <= iafirst; ibfirst++) {
    b_c->data[ibfirst + b_c->size[0]] = c->data[ibfirst + c->size[0]];
  }

  ibfirst = c->size[0] * c->size[1];
  c->size[0] = b_c->size[0];
  c->size[1] = 2;
  emxEnsureCapacity_real_T(sp, c, ibfirst, &sd_emlrtRTEI);
  iafirst = b_c->size[0] * b_c->size[1];
  for (ibfirst = 0; ibfirst < iafirst; ibfirst++) {
    c->data[ibfirst] = b_c->data[ibfirst];
  }

  emxFree_real_T(&b_c);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

void do_vectors(const emlrtStack *sp, const real_T a_data[], const int32_T
                a_size[1], real_T b, real_T c_data[], int32_T c_size[1], int32_T
                ia_data[], int32_T ia_size[1], int32_T ib_size[1])
{
  emlrtStack st;
  emxArray_real_T b_a_data;
  emxArray_real_T c_a_data;
  emxArray_real_T d_a_data;
  real_T absx;
  real_T ak;
  int32_T b_ialast;
  int32_T exponent;
  int32_T iafirst;
  int32_T ialast;
  int32_T iblast;
  int32_T na;
  int32_T nc;
  int32_T nia;
  boolean_T p;
  st.prev = sp;
  st.tls = sp->tls;
  na = a_size[0];
  c_size[0] = a_size[0];
  ia_size[0] = a_size[0];
  ib_size[0] = 0;
  b_a_data.data = (real_T *)&a_data[0];
  b_a_data.size = (int32_T *)&a_size[0];
  b_a_data.allocatedSize = -1;
  b_a_data.numDimensions = 1;
  b_a_data.canFreeData = false;
  st.site = &ni_emlrtRSI;
  if (!issorted(&st, &b_a_data)) {
    emlrtErrorWithMessageIdR2018a(sp, &i_emlrtRTEI,
      "Coder:toolbox:eml_setop_unsortedA", "Coder:toolbox:eml_setop_unsortedA",
      0);
  }

  nc = 0;
  nia = 0;
  iafirst = 0;
  ialast = 1;
  iblast = 1;
  while ((ialast <= na) && (iblast <= 1)) {
    b_ialast = ialast;
    c_a_data.data = (real_T *)&a_data[0];
    c_a_data.size = (int32_T *)&a_size[0];
    c_a_data.allocatedSize = -1;
    c_a_data.numDimensions = 1;
    c_a_data.canFreeData = false;
    ak = skip_to_last_equal_value(&b_ialast, &c_a_data);
    ialast = b_ialast;
    absx = muDoubleScalarAbs(b / 2.0);
    if ((!muDoubleScalarIsInf(absx)) && (!muDoubleScalarIsNaN(absx))) {
      if (absx <= 2.2250738585072014E-308) {
        absx = 4.94065645841247E-324;
      } else {
        frexp(absx, &exponent);
        absx = ldexp(1.0, exponent - 53);
      }
    } else {
      absx = rtNaN;
    }

    if ((muDoubleScalarAbs(b - ak) < absx) || (muDoubleScalarIsInf(ak) &&
         muDoubleScalarIsInf(b) && ((ak > 0.0) == (b > 0.0)))) {
      p = true;
    } else {
      p = false;
    }

    if (p) {
      ialast = b_ialast + 1;
      iafirst = b_ialast;
      iblast = 2;
    } else {
      if (muDoubleScalarIsNaN(b)) {
        p = !muDoubleScalarIsNaN(ak);
      } else {
        p = ((!muDoubleScalarIsNaN(ak)) && (ak < b));
      }

      if (p) {
        nc++;
        nia++;
        c_data[nc - 1] = ak;
        ia_data[nia - 1] = iafirst + 1;
        ialast = b_ialast + 1;
        iafirst = b_ialast;
      } else {
        iblast = 2;
      }
    }
  }

  while (ialast <= na) {
    iblast = ialast;
    d_a_data.data = (real_T *)&a_data[0];
    d_a_data.size = (int32_T *)&a_size[0];
    d_a_data.allocatedSize = -1;
    d_a_data.numDimensions = 1;
    d_a_data.canFreeData = false;
    ak = skip_to_last_equal_value(&iblast, &d_a_data);
    nc++;
    nia++;
    c_data[nc - 1] = ak;
    ia_data[nia - 1] = iafirst + 1;
    ialast = iblast + 1;
    iafirst = iblast;
  }

  if (a_size[0] > 0) {
    if (nia > a_size[0]) {
      emlrtErrorWithMessageIdR2018a(sp, &j_emlrtRTEI,
        "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
    }

    if (1 > nia) {
      ia_size[0] = 0;
    } else {
      ia_size[0] = nia;
    }

    if (nc > a_size[0]) {
      emlrtErrorWithMessageIdR2018a(sp, &k_emlrtRTEI,
        "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
    }

    if (1 > nc) {
      c_size[0] = 0;
    } else {
      c_size[0] = nc;
    }
  }
}

/* End of code generation (eml_setop.c) */
