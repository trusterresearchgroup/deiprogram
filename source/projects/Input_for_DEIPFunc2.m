% 03/28/2021 TJT
% A self periodic microstructure containing 30 grains, 2D
% PBC imposed through DG elements
% Loading: Prescribed macro strain of 0.01 


clear
% clc

%% Load Neper .msh file
perplan = 1;
innamePBC = 'n30-id1';
Gmshfile = [innamePBC '.msh'];
GmshInputReader

nen = 3;
% read periodic file
Perfile = [innamePBC '.per'];
fid = fopen(Perfile);
FormatString=repmat('%f',1,5);  % Create format string based on parameter
InputText=textscan(fid,FormatString); % Read data block
NeperPBC=cell2mat(InputText); % Convert to numerical array from cell
fclose(fid);
nummat=max(RegionOnElement);

MateT = ones(nummat,1)*[100 .25 1];
MatTypeTable = [(1:nummat); ones(1,nummat)];
     
MPCList = NeperPBC;
% [~,inds] = unique(MPCList(:,2));
% MPCList = MPCList(inds,:);
numMPC = length(MPCList);
MPCList = MPCList(:,1:4);

% Set rigid body modes
FIX = 1; % just pick one; do the strain BC...
NodeBC = [FIX 1 0
          FIX 2 0
          ];
      
% Output quantity flags
DHist = 1;
FHist = 1;
SHist = 1;
SEHist = 1;


%% Generate DG interface: pairs of elements and faces, duplicate the nodes,
% update the connectivities
numnpCG = numnp;
usePBC = 2; % flag to turn on keeping PBC
InterTypes = zeros(nummat,nummat); % generate DG elements on surface of domain
