/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sum.c
 *
 * Code generation for function 'sum'
 *
 */

/* Include files */
#include "sum.h"
#include "DEIPFunc2_emxutil.h"
#include "DEIPFunc2_types.h"
#include "introsort.h"
#include "rt_nonfinite.h"

/* Function Definitions */
void sum(const emxArray_real_T *x_d, const emxArray_int32_T *x_colidx, const
         emxArray_int32_T *x_rowidx, int x_m, int x_n, emxArray_real_T *y_d,
         emxArray_int32_T *y_colidx, emxArray_int32_T *y_rowidx, int *y_m)
{
  cell_wrap_4 this_tunableEnvironment[1];
  emxArray_int32_T *xrowidxPerm;
  emxArray_int32_T *y;
  emxArray_real_T *yt;
  int currentRow;
  int k;
  int n;
  int nzx;
  int yk;
  boolean_T p;
  if ((x_m == 0) || (x_n == 0)) {
    p = true;
  } else {
    p = false;
  }

  emxInit_real_T(&yt, 1);
  emxInit_int32_T(&xrowidxPerm, 1);
  emxInit_int32_T(&y, 2);
  emxInitMatrix_cell_wrap_41(this_tunableEnvironment);
  if (p || (x_n == 0)) {
    k = y_colidx->size[0];
    y_colidx->size[0] = 2;
    emxEnsureCapacity_int32_T(y_colidx, k);
    y_colidx->data[0] = 1;
    y_colidx->data[1] = 1;
    k = y_d->size[0];
    y_d->size[0] = 1;
    emxEnsureCapacity_real_T(y_d, k);
    y_d->data[0] = 0.0;
    k = y_rowidx->size[0];
    y_rowidx->size[0] = 1;
    emxEnsureCapacity_int32_T(y_rowidx, k);
    y_rowidx->data[0] = 1;
  } else if ((x_n != 0) && ((x_m <= x_colidx->data[x_colidx->size[0] - 1] - 1) ||
              (x_m <= x_n + 1))) {
    k = yt->size[0];
    yt->size[0] = x_m;
    emxEnsureCapacity_real_T(yt, k);
    for (k = 0; k < x_m; k++) {
      yt->data[k] = 0.0;
    }

    nzx = x_colidx->data[x_colidx->size[0] - 1];
    for (currentRow = 0; currentRow <= nzx - 2; currentRow++) {
      yt->data[x_rowidx->data[currentRow] - 1] += x_d->data[currentRow];
    }

    n = 0;
    for (k = 0; k < x_m; k++) {
      if (yt->data[k] != 0.0) {
        n++;
      }
    }

    if (n >= 1) {
      yk = n;
    } else {
      yk = 1;
    }

    k = y_d->size[0];
    y_d->size[0] = yk;
    emxEnsureCapacity_real_T(y_d, k);
    for (k = 0; k < yk; k++) {
      y_d->data[k] = 0.0;
    }

    k = y_colidx->size[0];
    y_colidx->size[0] = 2;
    emxEnsureCapacity_int32_T(y_colidx, k);
    k = y_rowidx->size[0];
    y_rowidx->size[0] = yk;
    emxEnsureCapacity_int32_T(y_rowidx, k);
    for (k = 0; k < yk; k++) {
      y_rowidx->data[k] = 0;
    }

    y_colidx->data[0] = 1;
    y_colidx->data[1] = n + 1;
    yk = 0;
    for (k = 0; k < x_m; k++) {
      if (yt->data[k] != 0.0) {
        y_rowidx->data[yk] = k + 1;
        y_d->data[yk] = yt->data[k];
        yk++;
      }
    }
  } else {
    nzx = x_colidx->data[x_colidx->size[0] - 1] - 1;
    if (1 > x_colidx->data[x_colidx->size[0] - 1] - 1) {
      currentRow = 0;
    } else {
      currentRow = x_colidx->data[x_colidx->size[0] - 1] - 1;
    }

    if (currentRow < 1) {
      n = 0;
    } else {
      n = currentRow;
    }

    k = y->size[0] * y->size[1];
    y->size[0] = 1;
    y->size[1] = n;
    emxEnsureCapacity_int32_T(y, k);
    if (n > 0) {
      y->data[0] = 1;
      yk = 1;
      for (k = 2; k <= n; k++) {
        yk++;
        y->data[k - 1] = yk;
      }
    }

    k = this_tunableEnvironment[0].f1->size[0];
    this_tunableEnvironment[0].f1->size[0] = currentRow;
    emxEnsureCapacity_int32_T(this_tunableEnvironment[0].f1, k);
    for (k = 0; k < currentRow; k++) {
      this_tunableEnvironment[0].f1->data[k] = x_rowidx->data[k];
    }

    k = xrowidxPerm->size[0];
    xrowidxPerm->size[0] = y->size[1];
    emxEnsureCapacity_int32_T(xrowidxPerm, k);
    yk = y->size[1];
    for (k = 0; k < yk; k++) {
      xrowidxPerm->data[k] = y->data[k];
    }

    b_introsort(xrowidxPerm, currentRow, this_tunableEnvironment);
    if (x_colidx->data[x_colidx->size[0] - 1] - 1 >= 1) {
      yk = x_colidx->data[x_colidx->size[0] - 1] - 2;
    } else {
      yk = 0;
    }

    k = y_d->size[0];
    y_d->size[0] = yk + 1;
    emxEnsureCapacity_real_T(y_d, k);
    for (k = 0; k <= yk; k++) {
      y_d->data[k] = 0.0;
    }

    k = y_colidx->size[0];
    y_colidx->size[0] = 2;
    emxEnsureCapacity_int32_T(y_colidx, k);
    k = y_rowidx->size[0];
    y_rowidx->size[0] = yk + 1;
    emxEnsureCapacity_int32_T(y_rowidx, k);
    for (k = 0; k <= yk; k++) {
      y_rowidx->data[k] = 0;
    }

    y_colidx->data[0] = 1;
    yk = 0;
    n = 0;
    while (yk + 1 <= nzx) {
      currentRow = x_rowidx->data[xrowidxPerm->data[yk] - 1];
      y_d->data[n] = x_d->data[xrowidxPerm->data[yk] - 1];
      yk++;
      while ((yk + 1 <= nzx) && (x_rowidx->data[xrowidxPerm->data[yk] - 1] ==
              currentRow)) {
        y_d->data[n] += x_d->data[xrowidxPerm->data[yk] - 1];
        yk++;
      }

      if (y_d->data[n] != 0.0) {
        y_rowidx->data[n] = currentRow;
        n++;
      }
    }

    y_colidx->data[1] = n + 1;
  }

  emxFreeMatrix_cell_wrap_41(this_tunableEnvironment);
  emxFree_int32_T(&y);
  emxFree_int32_T(&xrowidxPerm);
  emxFree_real_T(&yt);
  *y_m = x_m;
}

/* End of code generation (sum.c) */
