/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sparse.c
 *
 * Code generation for function 'sparse'
 *
 */

/* Include files */
#include "sparse.h"
#include "DEIPFunc2_emxutil.h"
#include "DEIPFunc2_types.h"
#include "fillIn.h"
#include "introsort.h"
#include "rt_nonfinite.h"

/* Function Definitions */
void b_sparse(const emxArray_real_T *varargin_1, const emxArray_real_T
              *varargin_2, const emxArray_real_T *varargin_3, d_sparse *y)
{
  cell_wrap_4 this_tunableEnvironment[2];
  emxArray_int32_T *sortedIndices;
  emxArray_int32_T *t;
  int i;
  int k;
  int nc;
  int ns;
  int ny;
  emxInitMatrix_cell_wrap_4(this_tunableEnvironment);
  nc = varargin_2->size[1];
  ns = varargin_1->size[1];
  i = this_tunableEnvironment[1].f1->size[0];
  this_tunableEnvironment[1].f1->size[0] = varargin_1->size[1];
  emxEnsureCapacity_int32_T(this_tunableEnvironment[1].f1, i);
  for (k = 0; k < ns; k++) {
    this_tunableEnvironment[1].f1->data[k] = (int)varargin_1->data[k];
  }

  ns = varargin_2->size[1];
  i = this_tunableEnvironment[0].f1->size[0];
  this_tunableEnvironment[0].f1->size[0] = varargin_2->size[1];
  emxEnsureCapacity_int32_T(this_tunableEnvironment[0].f1, i);
  for (k = 0; k < ns; k++) {
    this_tunableEnvironment[0].f1->data[k] = (int)varargin_2->data[k];
  }

  emxInit_int32_T(&sortedIndices, 1);
  i = sortedIndices->size[0];
  sortedIndices->size[0] = varargin_2->size[1];
  emxEnsureCapacity_int32_T(sortedIndices, i);
  for (k = 0; k < nc; k++) {
    sortedIndices->data[k] = k + 1;
  }

  emxInit_int32_T(&t, 1);
  introsort(sortedIndices, this_tunableEnvironment[0].f1->size[0],
            this_tunableEnvironment);
  ny = this_tunableEnvironment[0].f1->size[0];
  i = t->size[0];
  t->size[0] = this_tunableEnvironment[0].f1->size[0];
  emxEnsureCapacity_int32_T(t, i);
  ns = this_tunableEnvironment[0].f1->size[0];
  for (i = 0; i < ns; i++) {
    t->data[i] = this_tunableEnvironment[0].f1->data[i];
  }

  for (k = 0; k < ny; k++) {
    this_tunableEnvironment[0].f1->data[k] = t->data[sortedIndices->data[k] - 1];
  }

  ny = this_tunableEnvironment[1].f1->size[0];
  i = t->size[0];
  t->size[0] = this_tunableEnvironment[1].f1->size[0];
  emxEnsureCapacity_int32_T(t, i);
  ns = this_tunableEnvironment[1].f1->size[0];
  for (i = 0; i < ns; i++) {
    t->data[i] = this_tunableEnvironment[1].f1->data[i];
  }

  for (k = 0; k < ny; k++) {
    this_tunableEnvironment[1].f1->data[k] = t->data[sortedIndices->data[k] - 1];
  }

  emxFree_int32_T(&t);
  if ((this_tunableEnvironment[1].f1->size[0] == 0) || (this_tunableEnvironment
       [0].f1->size[0] == 0)) {
    ny = 0;
    y->n = 0;
  } else {
    ns = this_tunableEnvironment[1].f1->size[0];
    ny = this_tunableEnvironment[1].f1->data[0];
    for (k = 2; k <= ns; k++) {
      i = this_tunableEnvironment[1].f1->data[k - 1];
      if (ny < i) {
        ny = i;
      }
    }

    y->n = this_tunableEnvironment[0].f1->data[this_tunableEnvironment[0]
      .f1->size[0] - 1];
  }

  y->m = ny;
  if (varargin_2->size[1] >= 1) {
    ns = varargin_2->size[1];
  } else {
    ns = 1;
  }

  i = y->d->size[0];
  y->d->size[0] = ns;
  emxEnsureCapacity_real_T(y->d, i);
  for (i = 0; i < ns; i++) {
    y->d->data[i] = 0.0;
  }

  y->maxnz = ns;
  i = y->colidx->size[0];
  y->colidx->size[0] = y->n + 1;
  emxEnsureCapacity_int32_T(y->colidx, i);
  y->colidx->data[0] = 1;
  i = y->rowidx->size[0];
  y->rowidx->size[0] = ns;
  emxEnsureCapacity_int32_T(y->rowidx, i);
  for (i = 0; i < ns; i++) {
    y->rowidx->data[i] = 0;
  }

  ns = 0;
  i = y->n;
  for (ny = 0; ny < i; ny++) {
    while ((ns + 1 <= nc) && (this_tunableEnvironment[0].f1->data[ns] == ny + 1))
    {
      y->rowidx->data[ns] = this_tunableEnvironment[1].f1->data[ns];
      ns++;
    }

    y->colidx->data[ny + 1] = ns + 1;
  }

  emxFreeMatrix_cell_wrap_4(this_tunableEnvironment);
  for (k = 0; k < nc; k++) {
    y->d->data[k] = varargin_3->data[sortedIndices->data[k] - 1];
  }

  emxFree_int32_T(&sortedIndices);
  sparse_fillIn(y);
}

void c_sparse(const emxArray_real_T *varargin_1, emxArray_real_T *y_d,
              emxArray_int32_T *y_colidx, emxArray_int32_T *y_rowidx, int *y_m,
              int *y_n, int *y_maxnz)
{
  int i;
  int loop_ub;
  int nInt;
  nInt = varargin_1->size[1];
  *y_m = varargin_1->size[0];
  *y_n = varargin_1->size[1];
  i = y_d->size[0];
  y_d->size[0] = 1;
  emxEnsureCapacity_real_T(y_d, i);
  y_d->data[0] = 0.0;
  i = y_colidx->size[0];
  y_colidx->size[0] = varargin_1->size[1] + 1;
  emxEnsureCapacity_int32_T(y_colidx, i);
  loop_ub = varargin_1->size[1];
  for (i = 0; i <= loop_ub; i++) {
    y_colidx->data[i] = 0;
  }

  y_colidx->data[0] = 1;
  i = y_rowidx->size[0];
  y_rowidx->size[0] = 1;
  emxEnsureCapacity_int32_T(y_rowidx, i);
  y_rowidx->data[0] = 1;
  for (loop_ub = 0; loop_ub < nInt; loop_ub++) {
    y_colidx->data[loop_ub + 1] = 1;
  }

  *y_maxnz = 1;
}

void sparse(const emxArray_real_T *varargin_1, const emxArray_real_T *varargin_2,
            const emxArray_real_T *varargin_3, d_sparse *y)
{
  cell_wrap_4 this_tunableEnvironment[2];
  emxArray_int32_T *sortedIndices;
  emxArray_int32_T *t;
  int i;
  int k;
  int nc;
  int ns;
  int ny;
  emxInitMatrix_cell_wrap_4(this_tunableEnvironment);
  nc = varargin_2->size[0] * varargin_2->size[1];
  ns = varargin_1->size[0] * varargin_1->size[1];
  i = this_tunableEnvironment[1].f1->size[0];
  this_tunableEnvironment[1].f1->size[0] = ns;
  emxEnsureCapacity_int32_T(this_tunableEnvironment[1].f1, i);
  for (k = 0; k < ns; k++) {
    this_tunableEnvironment[1].f1->data[k] = (int)varargin_1->data[k];
  }

  ns = varargin_2->size[0] * varargin_2->size[1];
  i = this_tunableEnvironment[0].f1->size[0];
  this_tunableEnvironment[0].f1->size[0] = ns;
  emxEnsureCapacity_int32_T(this_tunableEnvironment[0].f1, i);
  for (k = 0; k < ns; k++) {
    this_tunableEnvironment[0].f1->data[k] = (int)varargin_2->data[k];
  }

  emxInit_int32_T(&sortedIndices, 1);
  i = sortedIndices->size[0];
  sortedIndices->size[0] = nc;
  emxEnsureCapacity_int32_T(sortedIndices, i);
  for (k = 0; k < nc; k++) {
    sortedIndices->data[k] = k + 1;
  }

  emxInit_int32_T(&t, 1);
  introsort(sortedIndices, this_tunableEnvironment[0].f1->size[0],
            this_tunableEnvironment);
  ny = this_tunableEnvironment[0].f1->size[0];
  i = t->size[0];
  t->size[0] = this_tunableEnvironment[0].f1->size[0];
  emxEnsureCapacity_int32_T(t, i);
  ns = this_tunableEnvironment[0].f1->size[0];
  for (i = 0; i < ns; i++) {
    t->data[i] = this_tunableEnvironment[0].f1->data[i];
  }

  for (k = 0; k < ny; k++) {
    this_tunableEnvironment[0].f1->data[k] = t->data[sortedIndices->data[k] - 1];
  }

  ny = this_tunableEnvironment[1].f1->size[0];
  i = t->size[0];
  t->size[0] = this_tunableEnvironment[1].f1->size[0];
  emxEnsureCapacity_int32_T(t, i);
  ns = this_tunableEnvironment[1].f1->size[0];
  for (i = 0; i < ns; i++) {
    t->data[i] = this_tunableEnvironment[1].f1->data[i];
  }

  for (k = 0; k < ny; k++) {
    this_tunableEnvironment[1].f1->data[k] = t->data[sortedIndices->data[k] - 1];
  }

  emxFree_int32_T(&t);
  if ((this_tunableEnvironment[1].f1->size[0] == 0) || (this_tunableEnvironment
       [0].f1->size[0] == 0)) {
    ny = 0;
    y->n = 0;
  } else {
    ns = this_tunableEnvironment[1].f1->size[0];
    ny = this_tunableEnvironment[1].f1->data[0];
    for (k = 2; k <= ns; k++) {
      i = this_tunableEnvironment[1].f1->data[k - 1];
      if (ny < i) {
        ny = i;
      }
    }

    y->n = this_tunableEnvironment[0].f1->data[this_tunableEnvironment[0]
      .f1->size[0] - 1];
  }

  y->m = ny;
  if (nc >= 1) {
    ns = nc;
  } else {
    ns = 1;
  }

  i = y->d->size[0];
  y->d->size[0] = ns;
  emxEnsureCapacity_real_T(y->d, i);
  for (i = 0; i < ns; i++) {
    y->d->data[i] = 0.0;
  }

  y->maxnz = ns;
  i = y->colidx->size[0];
  y->colidx->size[0] = y->n + 1;
  emxEnsureCapacity_int32_T(y->colidx, i);
  y->colidx->data[0] = 1;
  i = y->rowidx->size[0];
  y->rowidx->size[0] = ns;
  emxEnsureCapacity_int32_T(y->rowidx, i);
  for (i = 0; i < ns; i++) {
    y->rowidx->data[i] = 0;
  }

  ns = 0;
  i = y->n;
  for (ny = 0; ny < i; ny++) {
    while ((ns + 1 <= nc) && (this_tunableEnvironment[0].f1->data[ns] == ny + 1))
    {
      y->rowidx->data[ns] = this_tunableEnvironment[1].f1->data[ns];
      ns++;
    }

    y->colidx->data[ny + 1] = ns + 1;
  }

  emxFreeMatrix_cell_wrap_4(this_tunableEnvironment);
  for (k = 0; k < nc; k++) {
    y->d->data[k] = varargin_3->data[sortedIndices->data[k] - 1];
  }

  emxFree_int32_T(&sortedIndices);
  sparse_fillIn(y);
}

/* End of code generation (sparse.c) */
