% Interactive example that plots solutions from tutorial 
% MPC_2d_DG_Q4_mat_refinement to show % the deformation of an RVE
% containing DG couplers
%
% Domain: 4x4 rectangle block with inclusion
% Loading: Prescribed shear macro strain of 0.01.
%                  OR shear macro stress of 10/4^2
%
% Last revision: 12/19/2019 TJT

batchinter = 'batch';
batchname = 'MPC_2d_DG_Q4_mat_refinement.m';
fprintf('starting file %s\n',batchname)
FEA_Program

disp(' Plot the shear stress after a 2% strain; press any key to continue')
pause
plotElemCont2(Coordinates+20*Node_U_V,StreListE(3,:,:)',NodesOnElement(1:rinc*rinc,1:4),1,(1:rinc*rinc),[1 0 0],0,[3 4 6 9 0],{'cb'},{'FontSize',20})

disp(' Extract the macroscale strain and stress*volume')
[RVE_E,RVE_F] = GetMacroSS(DispList,ForcList,NodeTypeNum,ndm)