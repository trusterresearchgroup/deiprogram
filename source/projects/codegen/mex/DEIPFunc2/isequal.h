/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * isequal.h
 *
 * Code generation for function 'isequal'
 *
 */

#pragma once

/* Include files */
#include "rtwtypes.h"
#include "emlrt.h"
#include "mex.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Function Declarations */
boolean_T isequal(real_T varargin_1, const real_T varargin_2_data[], const
                  int32_T varargin_2_size[2]);

/* End of code generation (isequal.h) */
