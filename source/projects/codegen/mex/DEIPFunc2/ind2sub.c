/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * ind2sub.c
 *
 * Code generation for function 'ind2sub'
 *
 */

/* Include files */
#include "ind2sub.h"
#include "rt_nonfinite.h"

/* Variable Definitions */
static emlrtRTEInfo y_emlrtRTEI = { 38,/* lineNo */
  15,                                  /* colNo */
  "ind2sub_indexClass",                /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\ind2sub.m"/* pName */
};

static emlrtRSInfo jp_emlrtRSI = { 18, /* lineNo */
  "indexDivide",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\indexDivide.m"/* pathName */
};

/* Function Declarations */
static int32_T div_s32(const emlrtStack *sp, int32_T numerator, int32_T
  denominator);

/* Function Definitions */
static int32_T div_s32(const emlrtStack *sp, int32_T numerator, int32_T
  denominator)
{
  int32_T quotient;
  uint32_T b_denominator;
  uint32_T b_numerator;
  if (denominator == 0) {
    emlrtDivisionByZeroErrorR2012b(NULL, sp);
  } else {
    if (numerator < 0) {
      b_numerator = ~(uint32_T)numerator + 1U;
    } else {
      b_numerator = (uint32_T)numerator;
    }

    if (denominator < 0) {
      b_denominator = ~(uint32_T)denominator + 1U;
    } else {
      b_denominator = (uint32_T)denominator;
    }

    b_numerator /= b_denominator;
    if ((numerator < 0) != (denominator < 0)) {
      quotient = -(int32_T)b_numerator;
    } else {
      quotient = (int32_T)b_numerator;
    }
  }

  return quotient;
}

void b_ind2sub_indexClass(const emlrtStack *sp, const real_T siz[2], real_T ndx,
  int32_T *varargout_1, int32_T *varargout_2)
{
  emlrtStack st;
  int32_T vk;
  st.prev = sp;
  st.tls = sp->tls;
  if (((int32_T)ndx < 1) || ((int32_T)ndx > (int32_T)siz[0] * (int32_T)siz[1]))
  {
    emlrtErrorWithMessageIdR2018a(sp, &y_emlrtRTEI,
      "Coder:MATLAB:ind2sub_IndexOutOfRange",
      "Coder:MATLAB:ind2sub_IndexOutOfRange", 0);
  }

  st.site = &jp_emlrtRSI;
  vk = div_s32(&st, (int32_T)ndx - 1, (int32_T)siz[0]);
  *varargout_2 = vk + 1;
  *varargout_1 = (int32_T)ndx - vk * (int32_T)siz[0];
}

void ind2sub_indexClass(const emlrtStack *sp, const real_T siz[2], int32_T ndx,
  int32_T *varargout_1, int32_T *varargout_2)
{
  emlrtStack st;
  int32_T vk;
  st.prev = sp;
  st.tls = sp->tls;
  if ((ndx < 1) || (ndx > (int32_T)siz[0] * (int32_T)siz[1])) {
    emlrtErrorWithMessageIdR2018a(sp, &y_emlrtRTEI,
      "Coder:MATLAB:ind2sub_IndexOutOfRange",
      "Coder:MATLAB:ind2sub_IndexOutOfRange", 0);
  }

  st.site = &jp_emlrtRSI;
  vk = div_s32(&st, ndx - 1, (int32_T)siz[0]);
  *varargout_2 = vk + 1;
  *varargout_1 = ndx - vk * (int32_T)siz[0];
}

/* End of code generation (ind2sub.c) */
