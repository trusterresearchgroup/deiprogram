/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc3.h
 *
 * Code generation for function 'DEIPFunc3'
 *
 */

#pragma once

/* Include files */
#include "DEIPFunc3_types.h"
#include "rtwtypes.h"
#include "emlrt.h"
#include "mex.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Function Declarations */
void DEIPFunc3(const emlrtStack *sp, emxArray_real_T *InterTypes,
               emxArray_real_T *NodesOnElement, const emxArray_real_T
               *RegionOnElement, emxArray_real_T *Coordinates, real_T *numnp,
               real_T numel, real_T nummat, real_T nen, real_T ndm, real_T
               usePBC, const real_T *numMPC, emxArray_real_T *MPCList, real_T
               *numEonB, emxArray_real_T *numEonF, emxArray_real_T
               *ElementsOnBoundary, real_T *numSI, emxArray_real_T
               *ElementsOnFacet, e_sparse *ElementsOnNode, e_sparse
               *ElementsOnNodeDup, emxArray_real_T *ElementsOnNodeNum, real_T
               *numfac, emxArray_real_T *ElementsOnNodeNum2, real_T *numinttype,
               emxArray_real_T *FacetsOnElement, emxArray_real_T
               *FacetsOnElementInt, emxArray_real_T *FacetsOnInterface,
               emxArray_real_T *FacetsOnInterfaceNum, e_sparse *FacetsOnNode,
               e_sparse *FacetsOnNodeCut, e_sparse *FacetsOnNodeInt,
               emxArray_real_T *FacetsOnNodeNum, emxArray_real_T *NodeCGDG,
               e_sparse *NodeReg, emxArray_real_T *NodesOnElementCG,
               emxArray_real_T *NodesOnElementDG, emxArray_real_T
               *NodesOnInterface, real_T *NodesOnInterfaceNum, real_T *numCL,
               emxArray_real_T *NodesOnPBC, emxArray_real_T *NodesOnPBCnum,
               emxArray_real_T *NodesOnLink, emxArray_real_T *NodesOnLinknum,
               emxArray_real_T *numEonPBC, emxArray_real_T *FacetsOnPBC,
               emxArray_real_T *FacetsOnPBCNum, emxArray_real_T
               *FacetsOnIntMinusPBC, emxArray_real_T *FacetsOnIntMinusPBCNum);

/* End of code generation (DEIPFunc3.h) */
