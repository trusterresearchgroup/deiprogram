% Shear test of MP Periodic Boundary Conditions with CZ couplers
% Domain: Truly periodic hexagonal domain in 1.667 X 1.667 square block
% Loading: Prescribed shear displacement
% 
% Created: 09/25/2018 % Sunday Aduloju
% Last revision: 11/05/2018 TJT

clear
% clc
nen = 3;
nel = 3;
L=1.1667;
H=L;

m1=14;
n1=14;
numc = 4;2;1;
xl = [1 0 0
      2 L 0
      4 0 H
      3 L H];
type = 'cart';
rinc = m1;
sinc = n1;
node1 = 1;
elmt1 = 1;
mat = 1;
rskip = 0;
btype = 5;
[Coordinates,NodesOnElement,~,numnp,~] = block2d(type,rinc,sinc,node1,elmt1,mat,rskip,btype,xl,nen);
Coordinates = Coordinates';
NodesOnElement = NodesOnElement';

%% Process mesh from rectangle domain to complex shaped domain
RowToDelete = [1 2 3 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 ...
               29 44 45 46 47 48 49 50 51 52 53 54 55 56 57 82 83 ...
               84 85 86 87 112 113 114 115 140 141 166 167 168 169 ... 
               194 195 196 197 198 199 224 225 226 227 252 253 278 ...
               279 280 281 306 307 308 309 310 311 336 337 338 339 ...
               340 341 342 343 344 345 346 347 348 349 364 365 366 ...
               367 368 369 370 371 372 373 374 375 376 377 378 379 ...
               390 391 392];
NodesOnElement(RowToDelete,:) = [];
% Remove nodes not attached to retained elements
NodesOnElement_needed = unique(NodesOnElement(:,1:3));
TotalNodalVector = (1:numnp)';
NodesToRemove = setdiff(TotalNodalVector,NodesOnElement_needed);
Coordinates(NodesToRemove,:) = [];
% Renumber nodes starting from 1
numnp1 = size(Coordinates,1);
numnp = numnp1;
TotalNodalVector1 = (1:numnp1)';
[temp1,temp2] = ismember(NodesOnElement,NodesOnElement_needed);
NodesOnElement(temp1) = TotalNodalVector1(temp2(temp1));
numel = size(NodesOnElement,1); 

% Move the RVE1 to the location in the larger RVE
Coordinates(:,1) = Coordinates(:,1) + 0.833333333333333;
Coordinates(:,2) = Coordinates(:,2) + 0.75;

% Apply the bc corresponding to the location of the RVE in the large one
NodeBC = [ 1 1 0.00846896679589914
           1 2 0.00863206636557072];     	


%% Create regions
nummat = 6;
a=1; b=2; c=3; d=4; e=5; f=6;
RegionOnElement = ones(numel,1);
RegionOnElement(1:38) = a*ones(38,1);
RegionOnElement(39:48) = b*ones((48+1-39),1);
RegionOnElement(49:58) = a*ones((58+1-49),1);
RegionOnElement(59:72) = b*ones((72+1-59),1);
RegionOnElement(73:82) = c*ones((82+1-73),1);
RegionOnElement(83:96) = b*ones((96+1-83),1);
RegionOnElement(97:110) = c*ones((110+1-97),1);
RegionOnElement(111:120) = b*ones((120+1-111),1);
RegionOnElement(121:134) = c*ones((134+1-121),1);
RegionOnElement(135:144) = d*ones((144+1-135),1);
RegionOnElement(145:154) = c*ones((154+1-145),1);
RegionOnElement(155:168) = d*ones((168+1-155),1);
RegionOnElement(169:178) = e*ones((178+1-169),1);
RegionOnElement(179:192) = d*ones((192+1-179),1);
RegionOnElement(193:206) = e*ones((206+1-193),1);
RegionOnElement(207:216) = d*ones((216+1-207),1);
RegionOnElement(217:230) = e*ones((230+1-217),1);
RegionOnElement(231:240) = f*ones((240+1-231),1);
RegionOnElement(241:250) = e*ones((250+1-241),1);
RegionOnElement(251:264) = f*ones((264+1-251),1);
RegionOnElement(265:278) = f*ones((278+1-265),1);
RegionOnElement(279:288) = f*ones((288+1-279),1);

%%  Material properties        
MatTypeTable = [1 2 3 4 5 6 
                1 1 1 1 1 1 ];     

%% All grains set to same properties 
MateT = ones(nummat,1)*[100 0.25 1 ];

%% Create periodic boundary conditions
n = 12;
MPCList= [ 162 6 n n
            25 13 n 0
           38 26 n 0
           51 39 n 0
           64 52 n 0
           77 65 n 0
           90 78 n 0
           103 91 n 0
           116 104 n 0
           129 117  n 0
           142 130 n 0
           155 143 n 0
           155 1 n n
           144 2 0 n
           145 3 0 n
           146 4 0 n
           147 5 0 n
           156 12 0 n
           163 21 0 n
           164 22 0 n
           165 23 0 n
           166 24 0 n
           143 1 0 n
           167 13 n n];
%%       
% Output quantity flags
DHist = 1;
FHist = 1;
SHist = 1;
SEHist = 1;


% Generate CZM interface: pairs of elements and faces, duplicate the nodes,
% update the connectivities
numnpCG = numnp;
usePBC = 2; % flag to turn on keeping PBC
numMPC = length(MPCList);
InterTypes = [ 0 0 0 0 0 0
               1 0 0 0 0 0
               1 1 0 0 0 0
               0 1 1 0 0 0
               0 0 1 1 0 0
               0 0 0 1 1 0];
ndm = 2;
[NodesOnElement,RegionOnElement,Coordinates,numnp,Output_data,MPCList,numMPC]...
= DEIPFunction(InterTypes,NodesOnElement,RegionOnElement,Coordinates,numnp,...
               numel,nummat,nen,ndm,usePBC,numMPC,MPCList);


%% Insert couplers
CZprop = 500;500e10;
% MPC spring element properties
pencoeff = 1e9;
CornerXYZ = [0.000000 0.000000
             1.000000 0.000000];

[NodesOnElement,RegionOnElement,Coordinates,numnp,nen,numel,nummat,...
    RegionsOnInterface,MateT,MatTypeTable,NodeTypeNum,numMPC,MPCList...
] = InterFunction(3,InterTypes,NodesOnElement,RegionOnElement,Coordinates,...
numnp,numel,nummat,nen,ndm,Output_data,7,CZprop,MateT,MatTypeTable,usePBC,...
numMPC,MPCList,pencoeff,CornerXYZ);


% Applied displacements instead of forces
NodeLoad2 = [NodeTypeNum(2)+ndm 1 0
            NodeTypeNum(2)+ndm 2 0.01
            NodeTypeNum(2)+ndm+1 1 0.01
            NodeTypeNum(2)+ndm+1 2 0];

NodeBC = [NodeBC; NodeLoad2];
numBC = length(NodeBC);

ProbType = [numnp numel nummat ndm ndm nen];
