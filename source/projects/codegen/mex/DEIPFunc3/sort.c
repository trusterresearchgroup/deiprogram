/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sort.c
 *
 * Code generation for function 'sort'
 *
 */

/* Include files */
#include "sort.h"
#include "DEIPFunc3_data.h"
#include "DEIPFunc3_emxutil.h"
#include "DEIPFunc3_types.h"
#include "eml_int_forloop_overflow_check.h"
#include "rt_nonfinite.h"
#include "sortIdx.h"
#include "mwmathutil.h"
#include <string.h>

/* Variable Definitions */
static emlrtRSInfo ug_emlrtRSI = { 76, /* lineNo */
  "sort",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pathName */
};

static emlrtRSInfo vg_emlrtRSI = { 79, /* lineNo */
  "sort",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pathName */
};

static emlrtRSInfo wg_emlrtRSI = { 81, /* lineNo */
  "sort",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pathName */
};

static emlrtRSInfo xg_emlrtRSI = { 84, /* lineNo */
  "sort",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pathName */
};

static emlrtRSInfo yg_emlrtRSI = { 87, /* lineNo */
  "sort",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pathName */
};

static emlrtRSInfo ah_emlrtRSI = { 90, /* lineNo */
  "sort",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pathName */
};

static emlrtRSInfo ph_emlrtRSI = { 506,/* lineNo */
  "merge_block",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pathName */
};

static emlrtRSInfo dj_emlrtRSI = { 72, /* lineNo */
  "sort",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pathName */
};

static emlrtRTEInfo ul_emlrtRTEI = { 56,/* lineNo */
  24,                                  /* colNo */
  "sort",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pName */
};

static emlrtRTEInfo vl_emlrtRTEI = { 75,/* lineNo */
  26,                                  /* colNo */
  "sort",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pName */
};

static emlrtRTEInfo wl_emlrtRTEI = { 56,/* lineNo */
  1,                                   /* colNo */
  "sort",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pName */
};

static emlrtRTEInfo xl_emlrtRTEI = { 1,/* lineNo */
  20,                                  /* colNo */
  "sort",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pName */
};

/* Function Definitions */
void b_sort(const emlrtStack *sp, real_T x_data[], const int32_T x_size[2])
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack st;
  real_T xwork_data[101];
  real_T x4[4];
  real_T d;
  real_T d1;
  int32_T idx_data[101];
  int32_T iwork_data[101];
  int32_T i2;
  int32_T i3;
  int32_T i4;
  int32_T ib;
  int32_T k;
  int32_T n;
  int32_T nNaNs;
  int32_T quartetOffset;
  int8_T idx4[4];
  int8_T perm[4];
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &dj_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  ib = x_size[1];
  if (0 <= ib - 1) {
    memset(&idx_data[0], 0, ib * sizeof(int32_T));
  }

  b_st.site = &ch_emlrtRSI;
  c_st.site = &dh_emlrtRSI;
  n = x_size[1];
  x4[0] = 0.0;
  idx4[0] = 0;
  x4[1] = 0.0;
  idx4[1] = 0;
  x4[2] = 0.0;
  idx4[2] = 0;
  x4[3] = 0.0;
  idx4[3] = 0;
  ib = x_size[1];
  if (0 <= ib - 1) {
    memset(&iwork_data[0], 0, ib * sizeof(int32_T));
  }

  ib = x_size[1];
  if (0 <= ib - 1) {
    memset(&xwork_data[0], 0, ib * sizeof(real_T));
  }

  nNaNs = 0;
  ib = -1;
  d_st.site = &ih_emlrtRSI;
  for (k = 0; k < n; k++) {
    if (muDoubleScalarIsNaN(x_data[k])) {
      i3 = (n - nNaNs) - 1;
      idx_data[i3] = k + 1;
      xwork_data[i3] = x_data[k];
      nNaNs++;
    } else {
      ib++;
      idx4[ib] = (int8_T)(k + 1);
      x4[ib] = x_data[k];
      if (ib + 1 == 4) {
        quartetOffset = k - nNaNs;
        if (x4[0] <= x4[1]) {
          ib = 1;
          i2 = 2;
        } else {
          ib = 2;
          i2 = 1;
        }

        if (x4[2] <= x4[3]) {
          i3 = 3;
          i4 = 4;
        } else {
          i3 = 4;
          i4 = 3;
        }

        d = x4[ib - 1];
        d1 = x4[i3 - 1];
        if (d <= d1) {
          d = x4[i2 - 1];
          if (d <= d1) {
            perm[0] = (int8_T)ib;
            perm[1] = (int8_T)i2;
            perm[2] = (int8_T)i3;
            perm[3] = (int8_T)i4;
          } else if (d <= x4[i4 - 1]) {
            perm[0] = (int8_T)ib;
            perm[1] = (int8_T)i3;
            perm[2] = (int8_T)i2;
            perm[3] = (int8_T)i4;
          } else {
            perm[0] = (int8_T)ib;
            perm[1] = (int8_T)i3;
            perm[2] = (int8_T)i4;
            perm[3] = (int8_T)i2;
          }
        } else {
          d1 = x4[i4 - 1];
          if (d <= d1) {
            if (x4[i2 - 1] <= d1) {
              perm[0] = (int8_T)i3;
              perm[1] = (int8_T)ib;
              perm[2] = (int8_T)i2;
              perm[3] = (int8_T)i4;
            } else {
              perm[0] = (int8_T)i3;
              perm[1] = (int8_T)ib;
              perm[2] = (int8_T)i4;
              perm[3] = (int8_T)i2;
            }
          } else {
            perm[0] = (int8_T)i3;
            perm[1] = (int8_T)i4;
            perm[2] = (int8_T)ib;
            perm[3] = (int8_T)i2;
          }
        }

        idx_data[quartetOffset - 3] = idx4[perm[0] - 1];
        idx_data[quartetOffset - 2] = idx4[perm[1] - 1];
        idx_data[quartetOffset - 1] = idx4[perm[2] - 1];
        idx_data[quartetOffset] = idx4[perm[3] - 1];
        x_data[quartetOffset - 3] = x4[perm[0] - 1];
        x_data[quartetOffset - 2] = x4[perm[1] - 1];
        x_data[quartetOffset - 1] = x4[perm[2] - 1];
        x_data[quartetOffset] = x4[perm[3] - 1];
        ib = -1;
      }
    }
  }

  i4 = (n - nNaNs) - 1;
  if (ib + 1 > 0) {
    perm[1] = 0;
    perm[2] = 0;
    perm[3] = 0;
    if (ib + 1 == 1) {
      perm[0] = 1;
    } else if (ib + 1 == 2) {
      if (x4[0] <= x4[1]) {
        perm[0] = 1;
        perm[1] = 2;
      } else {
        perm[0] = 2;
        perm[1] = 1;
      }
    } else if (x4[0] <= x4[1]) {
      if (x4[1] <= x4[2]) {
        perm[0] = 1;
        perm[1] = 2;
        perm[2] = 3;
      } else if (x4[0] <= x4[2]) {
        perm[0] = 1;
        perm[1] = 3;
        perm[2] = 2;
      } else {
        perm[0] = 3;
        perm[1] = 1;
        perm[2] = 2;
      }
    } else if (x4[0] <= x4[2]) {
      perm[0] = 2;
      perm[1] = 1;
      perm[2] = 3;
    } else if (x4[1] <= x4[2]) {
      perm[0] = 2;
      perm[1] = 3;
      perm[2] = 1;
    } else {
      perm[0] = 3;
      perm[1] = 2;
      perm[2] = 1;
    }

    d_st.site = &jh_emlrtRSI;
    if (ib + 1 > 2147483646) {
      e_st.site = &fg_emlrtRSI;
      check_forloop_overflow_error(&e_st);
    }

    for (k = 0; k <= ib; k++) {
      i3 = perm[k] - 1;
      quartetOffset = (i4 - ib) + k;
      idx_data[quartetOffset] = idx4[i3];
      x_data[quartetOffset] = x4[i3];
    }
  }

  ib = (nNaNs >> 1) + 1;
  d_st.site = &kh_emlrtRSI;
  for (k = 0; k <= ib - 2; k++) {
    quartetOffset = (i4 + k) + 1;
    i2 = (int8_T)idx_data[quartetOffset];
    i3 = (n - k) - 1;
    idx_data[quartetOffset] = (int8_T)idx_data[i3];
    idx_data[i3] = i2;
    x_data[quartetOffset] = xwork_data[i3];
    x_data[i3] = xwork_data[quartetOffset];
  }

  if ((nNaNs & 1) != 0) {
    ib += i4;
    x_data[ib] = xwork_data[ib];
  }

  i2 = x_size[1] - nNaNs;
  if (i2 > 1) {
    c_st.site = &hh_emlrtRSI;
    i4 = i2 >> 2;
    i3 = 4;
    while (i4 > 1) {
      if ((i4 & 1) != 0) {
        i4--;
        ib = i3 * i4;
        quartetOffset = i2 - ib;
        if (quartetOffset > i3) {
          d_st.site = &oh_emlrtRSI;
          b_merge(&d_st, idx_data, x_data, ib, i3, quartetOffset - i3,
                  iwork_data, xwork_data);
        }
      }

      ib = i3 << 1;
      i4 >>= 1;
      d_st.site = &ph_emlrtRSI;
      for (k = 0; k < i4; k++) {
        d_st.site = &qh_emlrtRSI;
        b_merge(&d_st, idx_data, x_data, k * ib, i3, i3, iwork_data, xwork_data);
      }

      i3 = ib;
    }

    if (i2 > i3) {
      d_st.site = &rh_emlrtRSI;
      b_merge(&d_st, idx_data, x_data, 0, i3, i2 - i3, iwork_data, xwork_data);
    }
  }
}

void c_sort(const emlrtStack *sp, emxArray_real_T *x)
{
  emlrtStack b_st;
  emlrtStack st;
  emxArray_int32_T *dr_emlrtRSI;
  emxArray_real_T *vwork;
  int32_T dim;
  int32_T i;
  int32_T k;
  int32_T vlen;
  int32_T vstride;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  dim = 0;
  if (x->size[0] != 1) {
    dim = -1;
  }

  emxInit_real_T(sp, &vwork, 1, &wl_emlrtRTEI, true);
  if (dim + 2 <= 1) {
    i = x->size[0];
  } else {
    i = 1;
  }

  vlen = i - 1;
  vstride = vwork->size[0];
  vwork->size[0] = i;
  emxEnsureCapacity_real_T(sp, vwork, vstride, &ul_emlrtRTEI);
  st.site = &ug_emlrtRSI;
  vstride = 1;
  for (k = 0; k <= dim; k++) {
    vstride *= x->size[0];
  }

  st.site = &vg_emlrtRSI;
  st.site = &wg_emlrtRSI;
  if ((1 <= vstride) && (vstride > 2147483646)) {
    b_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  emxInit_int32_T(sp, &dr_emlrtRSI, 1, &xl_emlrtRTEI, true);
  for (dim = 0; dim < vstride; dim++) {
    st.site = &xg_emlrtRSI;
    if ((1 <= i) && (i > 2147483646)) {
      b_st.site = &fg_emlrtRSI;
      check_forloop_overflow_error(&b_st);
    }

    for (k = 0; k <= vlen; k++) {
      vwork->data[k] = x->data[dim + k * vstride];
    }

    st.site = &yg_emlrtRSI;
    b_sortIdx(&st, vwork, dr_emlrtRSI);
    st.site = &ah_emlrtRSI;
    for (k = 0; k <= vlen; k++) {
      x->data[dim + k * vstride] = vwork->data[k];
    }
  }

  emxFree_int32_T(&dr_emlrtRSI);
  emxFree_real_T(&vwork);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

void sort(const emlrtStack *sp, emxArray_real_T *x, emxArray_int32_T *idx)
{
  emlrtStack b_st;
  emlrtStack st;
  emxArray_int32_T *iidx;
  emxArray_real_T *vwork;
  int32_T dim;
  int32_T i;
  int32_T i1;
  int32_T k;
  int32_T vlen;
  int32_T vstride;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  dim = 0;
  if (x->size[0] != 1) {
    dim = -1;
  }

  emxInit_real_T(sp, &vwork, 1, &wl_emlrtRTEI, true);
  if (dim + 2 <= 1) {
    i = x->size[0];
  } else {
    i = 1;
  }

  vlen = i - 1;
  i1 = vwork->size[0];
  vwork->size[0] = i;
  emxEnsureCapacity_real_T(sp, vwork, i1, &ul_emlrtRTEI);
  i1 = idx->size[0];
  idx->size[0] = x->size[0];
  emxEnsureCapacity_int32_T(sp, idx, i1, &vl_emlrtRTEI);
  st.site = &ug_emlrtRSI;
  vstride = 1;
  for (k = 0; k <= dim; k++) {
    vstride *= x->size[0];
  }

  st.site = &vg_emlrtRSI;
  st.site = &wg_emlrtRSI;
  if ((1 <= vstride) && (vstride > 2147483646)) {
    b_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  emxInit_int32_T(sp, &iidx, 1, &xl_emlrtRTEI, true);
  for (dim = 0; dim < vstride; dim++) {
    st.site = &xg_emlrtRSI;
    if ((1 <= i) && (i > 2147483646)) {
      b_st.site = &fg_emlrtRSI;
      check_forloop_overflow_error(&b_st);
    }

    for (k = 0; k <= vlen; k++) {
      vwork->data[k] = x->data[dim + k * vstride];
    }

    st.site = &yg_emlrtRSI;
    b_sortIdx(&st, vwork, iidx);
    st.site = &ah_emlrtRSI;
    for (k = 0; k <= vlen; k++) {
      i1 = dim + k * vstride;
      x->data[i1] = vwork->data[k];
      idx->data[i1] = iidx->data[k];
    }
  }

  emxFree_int32_T(&iidx);
  emxFree_real_T(&vwork);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (sort.c) */
