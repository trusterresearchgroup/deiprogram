/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sum.c
 *
 * Code generation for function 'sum'
 *
 */

/* Include files */
#include "sum.h"
#include "DEIPFunc3_data.h"
#include "DEIPFunc3_emxutil.h"
#include "DEIPFunc3_types.h"
#include "eml_int_forloop_overflow_check.h"
#include "introsort.h"
#include "rt_nonfinite.h"
#include "sparse1.h"

/* Variable Definitions */
static emlrtRSInfo fp_emlrtRSI = { 20, /* lineNo */
  "sum",                               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sum.m"/* pathName */
};

static emlrtRSInfo gp_emlrtRSI = { 99, /* lineNo */
  "sumprod",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\private\\sumprod.m"/* pathName */
};

static emlrtRSInfo hp_emlrtRSI = { 21, /* lineNo */
  "checkAndSaturateExpandSize",        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\checkAndSaturateExpandSize.m"/* pathName */
};

static emlrtRSInfo ip_emlrtRSI = { 87, /* lineNo */
  "combineVectorElements",             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\private\\combineVectorElements.m"/* pathName */
};

static emlrtRSInfo jp_emlrtRSI = { 126,/* lineNo */
  "combineVectorElements",             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\private\\combineVectorElements.m"/* pathName */
};

static emlrtRSInfo kp_emlrtRSI = { 28, /* lineNo */
  "sparse/genericLike",                /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\genericLike.m"/* pathName */
};

static emlrtRSInfo lp_emlrtRSI = { 363,/* lineNo */
  "sparseIter",                        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\private\\combineVectorElements.m"/* pathName */
};

static emlrtRSInfo mp_emlrtRSI = { 365,/* lineNo */
  "sparseIter",                        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\private\\combineVectorElements.m"/* pathName */
};

static emlrtRSInfo np_emlrtRSI = { 432,/* lineNo */
  "sparseRowMapIter",                  /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\private\\combineVectorElements.m"/* pathName */
};

static emlrtRSInfo op_emlrtRSI = { 439,/* lineNo */
  "sparseRowMapIter",                  /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\private\\combineVectorElements.m"/* pathName */
};

static emlrtRSInfo pp_emlrtRSI = { 444,/* lineNo */
  "sparseRowMapIter",                  /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\private\\combineVectorElements.m"/* pathName */
};

static emlrtRSInfo qp_emlrtRSI = { 448,/* lineNo */
  "sparseRowMapIter",                  /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\private\\combineVectorElements.m"/* pathName */
};

static emlrtRSInfo rp_emlrtRSI = { 471,/* lineNo */
  "sparseRowReduceIter",               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\private\\combineVectorElements.m"/* pathName */
};

static emlrtRSInfo sp_emlrtRSI = { 20, /* lineNo */
  "sparse/rowReduction",               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\rowReduction.m"/* pathName */
};

static emlrtRSInfo tp_emlrtRSI = { 21, /* lineNo */
  "sparse/rowReduction",               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\rowReduction.m"/* pathName */
};

static emlrtRSInfo up_emlrtRSI = { 14, /* lineNo */
  "introsortIdx",                      /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\introsortIdx.m"/* pathName */
};

static emlrtRSInfo vp_emlrtRSI = { 15, /* lineNo */
  "introsortIdx",                      /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\introsortIdx.m"/* pathName */
};

static emlrtRSInfo wp_emlrtRSI = { 81, /* lineNo */
  "colon",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pathName */
};

static emlrtRSInfo xp_emlrtRSI = { 126,/* lineNo */
  "eml_integer_colon_dispatcher",      /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pathName */
};

static emlrtRSInfo yp_emlrtRSI = { 149,/* lineNo */
  "eml_signed_integer_colon",          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pathName */
};

static emlrtRSInfo aq_emlrtRSI = { 154,/* lineNo */
  "eml_signed_integer_colon",          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pathName */
};

static emlrtRTEInfo te_emlrtRTEI = { 20,/* lineNo */
  1,                                   /* colNo */
  "sum",                               /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sum.m"/* pName */
};

static emlrtRTEInfo ue_emlrtRTEI = { 429,/* lineNo */
  5,                                   /* colNo */
  "combineVectorElements",             /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\private\\combineVectorElements.m"/* pName */
};

static emlrtRTEInfo ve_emlrtRTEI = { 150,/* lineNo */
  20,                                  /* colNo */
  "colon",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pName */
};

static emlrtRTEInfo we_emlrtRTEI = { 21,/* lineNo */
  13,                                  /* colNo */
  "function_handle",                   /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\function_handle.m"/* pName */
};

static emlrtRTEInfo xe_emlrtRTEI = { 15,/* lineNo */
  1,                                   /* colNo */
  "introsortIdx",                      /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\introsortIdx.m"/* pName */
};

static emlrtRTEInfo ye_emlrtRTEI = { 419,/* lineNo */
  5,                                   /* colNo */
  "combineVectorElements",             /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\private\\combineVectorElements.m"/* pName */
};

static emlrtRTEInfo af_emlrtRTEI = { 1,/* lineNo */
  14,                                  /* colNo */
  "sum",                               /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sum.m"/* pName */
};

static emlrtRTEInfo bf_emlrtRTEI = { 14,/* lineNo */
  8,                                   /* colNo */
  "introsortIdx",                      /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\introsortIdx.m"/* pName */
};

static emlrtRTEInfo cf_emlrtRTEI = { 15,/* lineNo */
  43,                                  /* colNo */
  "introsortIdx",                      /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\introsortIdx.m"/* pName */
};

/* Function Definitions */
void sum(const emlrtStack *sp, const emxArray_real_T *x_d, const
         emxArray_int32_T *x_colidx, const emxArray_int32_T *x_rowidx, int32_T
         x_m, int32_T x_n, f_sparse *y)
{
  cell_wrap_6 this_tunableEnvironment[1];
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack f_st;
  emlrtStack g_st;
  emlrtStack h_st;
  emlrtStack i_st;
  emlrtStack j_st;
  emlrtStack k_st;
  emlrtStack l_st;
  emlrtStack st;
  emxArray_int32_T *b_y;
  emxArray_int32_T *xrowidxPerm;
  emxArray_real_T *yt;
  int32_T currentRow;
  int32_T k;
  int32_T n;
  int32_T nzx;
  int32_T yk;
  boolean_T overflow;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  f_st.prev = &e_st;
  f_st.tls = e_st.tls;
  g_st.prev = &f_st;
  g_st.tls = f_st.tls;
  h_st.prev = &g_st;
  h_st.tls = g_st.tls;
  i_st.prev = &h_st;
  i_st.tls = h_st.tls;
  j_st.prev = &i_st;
  j_st.tls = i_st.tls;
  k_st.prev = &j_st;
  k_st.tls = j_st.tls;
  l_st.prev = &k_st;
  l_st.tls = k_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  st.site = &fp_emlrtRSI;
  b_st.site = &gp_emlrtRSI;
  if ((x_m == 0) || (x_n == 0)) {
    overflow = true;
  } else {
    overflow = false;
  }

  emxInit_real_T(&b_st, &yt, 1, &ye_emlrtRTEI, true);
  emxInit_int32_T(&b_st, &xrowidxPerm, 1, &af_emlrtRTEI, true);
  emxInit_int32_T(&b_st, &b_y, 2, &bf_emlrtRTEI, true);
  emxInitMatrix_cell_wrap_61(&b_st, this_tunableEnvironment, &cf_emlrtRTEI, true);
  if (overflow || (x_n == 0)) {
    c_st.site = &ip_emlrtRSI;
    y->m = x_m;
    d_st.site = &kp_emlrtRSI;
    e_st.site = &hp_emlrtRSI;
    k = y->colidx->size[0];
    y->colidx->size[0] = 2;
    emxEnsureCapacity_int32_T(&c_st, y->colidx, k, &te_emlrtRTEI);
    y->colidx->data[0] = 1;
    y->colidx->data[1] = 1;
    k = y->d->size[0];
    y->d->size[0] = 1;
    emxEnsureCapacity_real_T(&c_st, y->d, k, &te_emlrtRTEI);
    y->d->data[0] = 0.0;
    k = y->rowidx->size[0];
    y->rowidx->size[0] = 1;
    emxEnsureCapacity_int32_T(&c_st, y->rowidx, k, &te_emlrtRTEI);
    y->rowidx->data[0] = 1;
  } else {
    c_st.site = &jp_emlrtRSI;
    if ((x_n != 0) && ((x_m <= x_colidx->data[x_colidx->size[0] - 1] - 1) ||
                       (x_m <= x_n + 1))) {
      d_st.site = &lp_emlrtRSI;
      k = yt->size[0];
      yt->size[0] = x_m;
      emxEnsureCapacity_real_T(&d_st, yt, k, &ue_emlrtRTEI);
      for (k = 0; k < x_m; k++) {
        yt->data[k] = 0.0;
      }

      nzx = x_colidx->data[x_colidx->size[0] - 1];
      e_st.site = &np_emlrtRSI;
      if (1 > x_colidx->data[x_colidx->size[0] - 1] - 1) {
        overflow = false;
      } else {
        overflow = (x_colidx->data[x_colidx->size[0] - 1] - 1 > 2147483646);
      }

      if (overflow) {
        f_st.site = &fg_emlrtRSI;
        check_forloop_overflow_error(&f_st);
      }

      for (currentRow = 0; currentRow <= nzx - 2; currentRow++) {
        yt->data[x_rowidx->data[currentRow] - 1] += x_d->data[currentRow];
      }

      yk = 0;
      e_st.site = &op_emlrtRSI;
      if ((1 <= x_m) && (x_m > 2147483646)) {
        f_st.site = &fg_emlrtRSI;
        check_forloop_overflow_error(&f_st);
      }

      for (k = 0; k < x_m; k++) {
        if (yt->data[k] != 0.0) {
          yk++;
        }
      }

      e_st.site = &pp_emlrtRSI;
      sparse_spallocLike(&e_st, x_m, yk, y);
      y->colidx->data[0] = 1;
      y->colidx->data[y->colidx->size[0] - 1] = yk + 1;
      yk = 0;
      e_st.site = &qp_emlrtRSI;
      for (k = 0; k < x_m; k++) {
        if (yt->data[k] != 0.0) {
          y->rowidx->data[yk] = k + 1;
          y->d->data[yk] = yt->data[k];
          yk++;
        }
      }
    } else {
      d_st.site = &mp_emlrtRSI;
      e_st.site = &rp_emlrtRSI;
      nzx = x_colidx->data[x_colidx->size[0] - 1] - 1;
      if (1 > x_colidx->data[x_colidx->size[0] - 1] - 1) {
        currentRow = 0;
      } else {
        currentRow = x_colidx->data[x_colidx->size[0] - 1] - 1;
      }

      f_st.site = &sp_emlrtRSI;
      g_st.site = &up_emlrtRSI;
      h_st.site = &lo_emlrtRSI;
      i_st.site = &wp_emlrtRSI;
      j_st.site = &xp_emlrtRSI;
      k_st.site = &yp_emlrtRSI;
      if (currentRow < 1) {
        n = 0;
      } else {
        n = currentRow;
      }

      k = b_y->size[0] * b_y->size[1];
      b_y->size[0] = 1;
      b_y->size[1] = n;
      emxEnsureCapacity_int32_T(&j_st, b_y, k, &ve_emlrtRTEI);
      if (n > 0) {
        b_y->data[0] = 1;
        yk = 1;
        k_st.site = &aq_emlrtRSI;
        if ((2 <= n) && (n > 2147483646)) {
          l_st.site = &fg_emlrtRSI;
          check_forloop_overflow_error(&l_st);
        }

        for (k = 2; k <= n; k++) {
          yk++;
          b_y->data[k - 1] = yk;
        }
      }

      k = this_tunableEnvironment[0].f1->size[0];
      this_tunableEnvironment[0].f1->size[0] = currentRow;
      emxEnsureCapacity_int32_T(&f_st, this_tunableEnvironment[0].f1, k,
        &we_emlrtRTEI);
      for (k = 0; k < currentRow; k++) {
        this_tunableEnvironment[0].f1->data[k] = x_rowidx->data[k];
      }

      k = xrowidxPerm->size[0];
      xrowidxPerm->size[0] = b_y->size[1];
      emxEnsureCapacity_int32_T(&f_st, xrowidxPerm, k, &xe_emlrtRTEI);
      yk = b_y->size[1];
      for (k = 0; k < yk; k++) {
        xrowidxPerm->data[k] = b_y->data[k];
      }

      g_st.site = &vp_emlrtRSI;
      b_introsort(&g_st, xrowidxPerm, currentRow, this_tunableEnvironment);
      f_st.site = &tp_emlrtRSI;
      sparse_spallocLike(&f_st, x_m, x_colidx->data[x_colidx->size[0] - 1] - 1,
                         y);
      yk = 0;
      n = 0;
      while (yk + 1 <= nzx) {
        currentRow = x_rowidx->data[xrowidxPerm->data[yk] - 1];
        y->d->data[n] = x_d->data[xrowidxPerm->data[yk] - 1];
        yk++;
        while ((yk + 1 <= nzx) && (x_rowidx->data[xrowidxPerm->data[yk] - 1] ==
                currentRow)) {
          y->d->data[n] += x_d->data[xrowidxPerm->data[yk] - 1];
          yk++;
        }

        if (y->d->data[n] != 0.0) {
          y->rowidx->data[n] = currentRow;
          n++;
        }
      }

      y->colidx->data[y->colidx->size[0] - 1] = n + 1;
    }
  }

  emxFreeMatrix_cell_wrap_61(this_tunableEnvironment);
  emxFree_int32_T(&b_y);
  emxFree_int32_T(&xrowidxPerm);
  emxFree_real_T(&yt);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (sum.c) */
