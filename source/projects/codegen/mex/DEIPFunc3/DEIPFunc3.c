/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc3.c
 *
 * Code generation for function 'DEIPFunc3'
 *
 */

/* Include files */
#include "DEIPFunc3.h"
#include "DEIPFunc3_data.h"
#include "DEIPFunc3_emxutil.h"
#include "DEIPFunc3_mexutil.h"
#include "DEIPFunc3_types.h"
#include "abs.h"
#include "all.h"
#include "any.h"
#include "colon.h"
#include "diag.h"
#include "eml_int_forloop_overflow_check.h"
#include "eml_setop.h"
#include "error.h"
#include "find.h"
#include "ifWhileCond.h"
#include "indexShapeCheck.h"
#include "ismember.h"
#include "nchoosek.h"
#include "nnz.h"
#include "norm.h"
#include "not.h"
#include "parenAssign2D.h"
#include "rt_nonfinite.h"
#include "sign.h"
#include "sort.h"
#include "sortIdx.h"
#include "sortrows.h"
#include "sparse.h"
#include "sparse1.h"
#include "sub2ind.h"
#include "sum.h"
#include "unique.h"
#include "mwmathutil.h"
#include <string.h>

/* Variable Definitions */
static emlrtRTEInfo emlrtRTEI = { 360, /* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo b_emlrtRTEI = { 366,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo c_emlrtRTEI = { 372,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRSInfo emlrtRSI = { 681,   /* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo b_emlrtRSI = { 1283,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo c_emlrtRSI = { 1228,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo d_emlrtRSI = { 1202,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo e_emlrtRSI = { 1187,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo f_emlrtRSI = { 1318,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo g_emlrtRSI = { 1300,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo h_emlrtRSI = { 1282,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo i_emlrtRSI = { 1262,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo j_emlrtRSI = { 1247,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo k_emlrtRSI = { 1230,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo l_emlrtRSI = { 1227,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo m_emlrtRSI = { 1226,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo n_emlrtRSI = { 1216,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo o_emlrtRSI = { 1204,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo p_emlrtRSI = { 1201,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo q_emlrtRSI = { 1200,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo r_emlrtRSI = { 1189,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo s_emlrtRSI = { 1186,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo t_emlrtRSI = { 1185,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo u_emlrtRSI = { 1174,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo v_emlrtRSI = { 1169,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo w_emlrtRSI = { 1165,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo x_emlrtRSI = { 1156,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo y_emlrtRSI = { 1143,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ab_emlrtRSI = { 1133,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo bb_emlrtRSI = { 1132,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo cb_emlrtRSI = { 1121,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo db_emlrtRSI = { 1120,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo eb_emlrtRSI = { 1111,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo fb_emlrtRSI = { 1109,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo gb_emlrtRSI = { 1106,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo hb_emlrtRSI = { 1100,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ib_emlrtRSI = { 1099,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo jb_emlrtRSI = { 1094,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo kb_emlrtRSI = { 1070,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo lb_emlrtRSI = { 1061,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo mb_emlrtRSI = { 1055,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo nb_emlrtRSI = { 1051,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ob_emlrtRSI = { 1045,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo pb_emlrtRSI = { 1030,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo qb_emlrtRSI = { 1023,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo rb_emlrtRSI = { 1019,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo sb_emlrtRSI = { 1018,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo tb_emlrtRSI = { 1012,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ub_emlrtRSI = { 1011,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo vb_emlrtRSI = { 1010,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo wb_emlrtRSI = { 1009,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo xb_emlrtRSI = { 1008,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo yb_emlrtRSI = { 984,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ac_emlrtRSI = { 981,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo bc_emlrtRSI = { 980,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo cc_emlrtRSI = { 979,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo dc_emlrtRSI = { 978,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ec_emlrtRSI = { 977,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo fc_emlrtRSI = { 970,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo gc_emlrtRSI = { 969,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo hc_emlrtRSI = { 939,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ic_emlrtRSI = { 938,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo jc_emlrtRSI = { 937,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo kc_emlrtRSI = { 936,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo lc_emlrtRSI = { 935,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo mc_emlrtRSI = { 927,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo nc_emlrtRSI = { 926,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo oc_emlrtRSI = { 924,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo pc_emlrtRSI = { 914,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo qc_emlrtRSI = { 910,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo rc_emlrtRSI = { 909,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo sc_emlrtRSI = { 908,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo tc_emlrtRSI = { 867,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo uc_emlrtRSI = { 866,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo vc_emlrtRSI = { 827,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo wc_emlrtRSI = { 782,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo xc_emlrtRSI = { 775,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo yc_emlrtRSI = { 772,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ad_emlrtRSI = { 771,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo bd_emlrtRSI = { 765,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo cd_emlrtRSI = { 754,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo dd_emlrtRSI = { 749,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ed_emlrtRSI = { 748,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo fd_emlrtRSI = { 747,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo gd_emlrtRSI = { 728,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo hd_emlrtRSI = { 644,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo id_emlrtRSI = { 627,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo jd_emlrtRSI = { 620,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo kd_emlrtRSI = { 617,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ld_emlrtRSI = { 616,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo md_emlrtRSI = { 610,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo nd_emlrtRSI = { 535,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo od_emlrtRSI = { 519,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo pd_emlrtRSI = { 505,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo qd_emlrtRSI = { 503,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo rd_emlrtRSI = { 502,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo sd_emlrtRSI = { 501,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo td_emlrtRSI = { 500,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ud_emlrtRSI = { 499,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo vd_emlrtRSI = { 498,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo wd_emlrtRSI = { 451,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo xd_emlrtRSI = { 450,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo yd_emlrtRSI = { 449,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ae_emlrtRSI = { 448,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo be_emlrtRSI = { 447,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ce_emlrtRSI = { 446,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo de_emlrtRSI = { 445,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ee_emlrtRSI = { 444,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo fe_emlrtRSI = { 380,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ge_emlrtRSI = { 372,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo he_emlrtRSI = { 370,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ie_emlrtRSI = { 366,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo je_emlrtRSI = { 363,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ke_emlrtRSI = { 360,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo le_emlrtRSI = { 357,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo me_emlrtRSI = { 356,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ne_emlrtRSI = { 354,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo oe_emlrtRSI = { 351,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo pe_emlrtRSI = { 346,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo qe_emlrtRSI = { 345,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo re_emlrtRSI = { 336,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo se_emlrtRSI = { 331,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo te_emlrtRSI = { 323,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ue_emlrtRSI = { 298,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ve_emlrtRSI = { 285,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo we_emlrtRSI = { 284,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo xe_emlrtRSI = { 283,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ye_emlrtRSI = { 282,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo af_emlrtRSI = { 281,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo bf_emlrtRSI = { 272,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo cf_emlrtRSI = { 266,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo df_emlrtRSI = { 265,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ef_emlrtRSI = { 255,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo ff_emlrtRSI = { 253,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo gf_emlrtRSI = { 249,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo hf_emlrtRSI = { 241,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo if_emlrtRSI = { 235,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo jf_emlrtRSI = { 227,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo kf_emlrtRSI = { 222,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo lf_emlrtRSI = { 201,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo mf_emlrtRSI = { 190,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo nf_emlrtRSI = { 182,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo of_emlrtRSI = { 164,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo pf_emlrtRSI = { 146,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo qf_emlrtRSI = { 120,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo rf_emlrtRSI = { 118,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo sf_emlrtRSI = { 107,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo tf_emlrtRSI = { 77, /* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo uf_emlrtRSI = { 67, /* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo vf_emlrtRSI = { 30, /* lineNo */
  "unique",                            /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pathName */
};

static emlrtRSInfo ng_emlrtRSI = { 38, /* lineNo */
  "ismember",                          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\ismember.m"/* pathName */
};

static emlrtRSInfo uh_emlrtRSI = { 39, /* lineNo */
  "find",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pathName */
};

static emlrtRSInfo ai_emlrtRSI = { 44, /* lineNo */
  "unique",                            /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pathName */
};

static emlrtRSInfo li_emlrtRSI = { 19, /* lineNo */
  "setdiff",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\setdiff.m"/* pathName */
};

static emlrtRSInfo mi_emlrtRSI = { 70, /* lineNo */
  "eml_setop",                         /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pathName */
};

static emlrtRSInfo aj_emlrtRSI = { 27, /* lineNo */
  "cat",                               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\cat.m"/* pathName */
};

static emlrtRSInfo bj_emlrtRSI = { 102,/* lineNo */
  "cat_impl",                          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\cat.m"/* pathName */
};

static emlrtRSInfo cj_emlrtRSI = { 32, /* lineNo */
  "sort",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sort.m"/* pathName */
};

static emlrtRSInfo hj_emlrtRSI = { 68, /* lineNo */
  "eml_setop",                         /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pathName */
};

static emlrtRSInfo wj_emlrtRSI = { 328,/* lineNo */
  "unaryMinOrMaxDispatch",             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pathName */
};

static emlrtRSInfo xj_emlrtRSI = { 396,/* lineNo */
  "minOrMax2D",                        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pathName */
};

static emlrtRSInfo yj_emlrtRSI = { 478,/* lineNo */
  "minOrMax2DColumnMajorDim1",         /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pathName */
};

static emlrtRSInfo ak_emlrtRSI = { 476,/* lineNo */
  "minOrMax2DColumnMajorDim1",         /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pathName */
};

static emlrtRSInfo ek_emlrtRSI = { 38, /* lineNo */
  "fprintf",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\iofun\\fprintf.m"/* pathName */
};

static emlrtRSInfo fk_emlrtRSI = { 994,/* lineNo */
  "minOrMaxRealVectorKernel",          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pathName */
};

static emlrtRSInfo il_emlrtRSI = { 35, /* lineNo */
  "find",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pathName */
};

static emlrtRSInfo pl_emlrtRSI = { 147,/* lineNo */
  "unaryMinOrMax",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pathName */
};

static emlrtRSInfo ql_emlrtRSI = { 1021,/* lineNo */
  "maxRealVectorOmitNaN",              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pathName */
};

static emlrtRSInfo rl_emlrtRSI = { 934,/* lineNo */
  "minOrMaxRealVector",                /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pathName */
};

static emlrtRSInfo sl_emlrtRSI = { 926,/* lineNo */
  "minOrMaxRealVector",                /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pathName */
};

static emlrtRSInfo tl_emlrtRSI = { 16, /* lineNo */
  "sub2ind",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\sub2ind.m"/* pathName */
};

static emlrtRSInfo mo_emlrtRSI = { 103,/* lineNo */
  "colon",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pathName */
};

static emlrtRSInfo dq_emlrtRSI = { 23, /* lineNo */
  "intersect",                         /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\intersect.m"/* pathName */
};

static emlrtMCInfo emlrtMCI = { 320,   /* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtMCInfo b_emlrtMCI = { 321, /* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtMCInfo c_emlrtMCI = { 328, /* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtMCInfo d_emlrtMCI = { 329, /* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtMCInfo e_emlrtMCI = { 335, /* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtMCInfo f_emlrtMCI = { 359, /* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtMCInfo g_emlrtMCI = { 373, /* lineNo */
  9,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtMCInfo h_emlrtMCI = { 365, /* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtMCInfo l_emlrtMCI = { 64,  /* lineNo */
  18,                                  /* colNo */
  "fprintf",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\iofun\\fprintf.m"/* pName */
};

static emlrtBCInfo emlrtBCI = { -1,    /* iFirst */
  -1,                                  /* iLast */
  782,                                 /* lineNo */
  43,                                  /* colNo */
  "twoelem",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo b_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  816,                                 /* lineNo */
  25,                                  /* colNo */
  "FacetsOnPBC",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo c_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  814,                                 /* lineNo */
  25,                                  /* colNo */
  "numEonPBC",                         /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo e_emlrtDCI = { 771, /* lineNo */
  54,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo d_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  771,                                 /* lineNo */
  54,                                  /* colNo */
  "ElementsOnNodePBCNum",              /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo e_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  1190,                                /* lineNo */
  51,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo f_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  755,                                 /* lineNo */
  63,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo g_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  1205,                                /* lineNo */
  51,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo h_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  752,                                 /* lineNo */
  54,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo i_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  751,                                 /* lineNo */
  54,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo j_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  750,                                 /* lineNo */
  54,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo k_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  1231,                                /* lineNo */
  51,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo l_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  1146,                                /* lineNo */
  21,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo m_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  1145,                                /* lineNo */
  21,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo n_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  1145,                                /* lineNo */
  53,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo o_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  1145,                                /* lineNo */
  38,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo p_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  1194,                                /* lineNo */
  34,                                  /* colNo */
  "copyonce",                          /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo q_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  1289,                                /* lineNo */
  21,                                  /* colNo */
  "NodeCGDG",                          /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo r_emlrtBCI = { 1,   /* iFirst */
  100,                                 /* iLast */
  1289,                                /* lineNo */
  21,                                  /* colNo */
  "NodeCGDG",                          /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  3                                    /* checkKind */
};

static emlrtBCInfo s_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  1287,                                /* lineNo */
  28,                                  /* colNo */
  "ElementsOnNodeNum2",                /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo t_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  1174,                                /* lineNo */
  33,                                  /* colNo */
  "copyonce",                          /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo u_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  852,                                 /* lineNo */
  29,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo v_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  1142,                                /* lineNo */
  46,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo w_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  1142,                                /* lineNo */
  31,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo x_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  746,                                 /* lineNo */
  61,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo y_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  1071,                                /* lineNo */
  59,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ab_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  850,                                 /* lineNo */
  65,                                  /* colNo */
  "nodeABCD",                          /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1166,                                /* lineNo */
  53,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo cb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  745,                                 /* lineNo */
  59,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo db_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  848,                                 /* lineNo */
  25,                                  /* colNo */
  "nodeABCD",                          /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo f_emlrtDCI = { 848, /* lineNo */
  25,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo eb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  848,                                 /* lineNo */
  25,                                  /* colNo */
  "FacetsOnNodeNum",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1130,                                /* lineNo */
  28,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  847,                                 /* lineNo */
  38,                                  /* colNo */
  "nodeABCD",                          /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo g_emlrtDCI = { 847, /* lineNo */
  38,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo hb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  847,                                 /* lineNo */
  38,                                  /* colNo */
  "FacetsOnNodeNum",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ib_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1281,                                /* lineNo */
  31,                                  /* colNo */
  "NodesOnElement",                    /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo jb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  834,                                 /* lineNo */
  25,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo kb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1118,                                /* lineNo */
  28,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo lb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  832,                                 /* lineNo */
  61,                                  /* colNo */
  "nodeABCD",                          /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  845,                                 /* lineNo */
  31,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo nb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  844,                                 /* lineNo */
  54,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ob_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1276,                                /* lineNo */
  21,                                  /* colNo */
  "NodeCGDG",                          /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1275,                                /* lineNo */
  21,                                  /* colNo */
  "ElementsOnNodeNum2",                /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1279,                                /* lineNo */
  21,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  830,                                 /* lineNo */
  21,                                  /* colNo */
  "nodeABCD",                          /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo sb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  830,                                 /* lineNo */
  21,                                  /* colNo */
  "FacetsOnNodeNum",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo h_emlrtDCI = { 1274,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo tb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1274,                                /* lineNo */
  20,                                  /* colNo */
  "ElementsOnNodeNum2",                /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ub_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1113,                                /* lineNo */
  29,                                  /* colNo */
  "ElementsOnFacet",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1273,                                /* lineNo */
  24,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo i_emlrtDCI = { 1112,/* lineNo */
  29,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo wb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1112,                                /* lineNo */
  29,                                  /* colNo */
  "ElementsOnFacet",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1160,                                /* lineNo */
  28,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  829,                                 /* lineNo */
  34,                                  /* colNo */
  "nodeABCD",                          /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ac_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  829,                                 /* lineNo */
  34,                                  /* colNo */
  "FacetsOnNodeNum",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1214,                                /* lineNo */
  28,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo cc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1032,                                /* lineNo */
  39,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo j_emlrtDCI = { 1158,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo dc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1157,                                /* lineNo */
  25,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo k_emlrtDCI = { 827, /* lineNo */
  27,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ec_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  827,                                 /* lineNo */
  27,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1031,                                /* lineNo */
  51,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1156,                                /* lineNo */
  22,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1268,                                /* lineNo */
  20,                                  /* colNo */
  "elems",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ic_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  727,                                 /* lineNo */
  17,                                  /* colNo */
  "FacetsOnInterface",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo jc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1301,                                /* lineNo */
  20,                                  /* colNo */
  "ElementsOnNodeNum2",                /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo kc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  726,                                 /* lineNo */
  17,                                  /* colNo */
  "ElementsOnFacet",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo l_emlrtDCI = { 1096,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo lc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  725,                                 /* lineNo */
  17,                                  /* colNo */
  "ElementsOnFacet",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1258,                                /* lineNo */
  8,                                   /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo nc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  724,                                 /* lineNo */
  17,                                  /* colNo */
  "ElementsOnFacet",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo oc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  945,                                 /* lineNo */
  9,                                   /* colNo */
  "FacetsOnIntMinusPBCNum",            /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  945,                                 /* lineNo */
  41,                                  /* colNo */
  "FacetsOnIntMinusPBCNum",            /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1042,                                /* lineNo */
  24,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  723,                                 /* lineNo */
  17,                                  /* colNo */
  "ElementsOnFacet",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo sc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  722,                                 /* lineNo */
  17,                                  /* colNo */
  "FacetsOnElementInt",                /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo tc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  721,                                 /* lineNo */
  17,                                  /* colNo */
  "FacetsOnElementInt",                /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo uc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  720,                                 /* lineNo */
  17,                                  /* colNo */
  "FacetsOnElement",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  719,                                 /* lineNo */
  17,                                  /* colNo */
  "FacetsOnElement",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  718,                                 /* lineNo */
  17,                                  /* colNo */
  "numEonF",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  938,                                 /* lineNo */
  30,                                  /* colNo */
  "FacetsOnPBC",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo m_emlrtDCI = { 938, /* lineNo */
  30,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo yc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1019,                                /* lineNo */
  24,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo n_emlrtDCI = { 1252,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo o_emlrtDCI = { 1307,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo p_emlrtDCI = { 1251,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ad_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1092,                                /* lineNo */
  22,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  684,                                 /* lineNo */
  31,                                  /* colNo */
  "numfn",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo cd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  936,                                 /* lineNo */
  36,                                  /* colNo */
  "FacetsOnInterface",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo q_emlrtDCI = { 936, /* lineNo */
  36,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo r_emlrtDCI = { 1306,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo dd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  993,                                 /* lineNo */
  9,                                   /* colNo */
  "intermat2",                         /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ed_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  998,                                 /* lineNo */
  12,                                  /* colNo */
  "NodesOnInterface",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  992,                                 /* lineNo */
  9,                                   /* colNo */
  "intermat2",                         /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  892,                                 /* lineNo */
  61,                                  /* colNo */
  "nodeABCD",                          /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo s_emlrtDCI = { 989, /* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo hd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  890,                                 /* lineNo */
  21,                                  /* colNo */
  "nodeABCD",                          /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo t_emlrtDCI = { 890, /* lineNo */
  21,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo id_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  890,                                 /* lineNo */
  21,                                  /* colNo */
  "FacetsOnNodeNum",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo jd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  676,                                 /* lineNo */
  30,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo kd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  674,                                 /* lineNo */
  41,                                  /* colNo */
  "nodeABCD",                          /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ld_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  889,                                 /* lineNo */
  34,                                  /* colNo */
  "nodeABCD",                          /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo u_emlrtDCI = { 889, /* lineNo */
  34,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo md_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  889,                                 /* lineNo */
  34,                                  /* colNo */
  "FacetsOnNodeNum",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo nd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  673,                                 /* lineNo */
  26,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo od_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  881,                                 /* lineNo */
  57,                                  /* colNo */
  "nodeABCD",                          /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  930,                                 /* lineNo */
  9,                                   /* colNo */
  "FacetsOnPBCNum",                    /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  930,                                 /* lineNo */
  33,                                  /* colNo */
  "FacetsOnPBCNum",                    /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo v_emlrtDCI = { 932, /* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  4                                    /* checkKind */
};

static emlrtBCInfo rd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  627,                                 /* lineNo */
  31,                                  /* colNo */
  "twoelem",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo sd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  887,                                 /* lineNo */
  50,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo td_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  879,                                 /* lineNo */
  17,                                  /* colNo */
  "nodeABCD",                          /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ud_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  879,                                 /* lineNo */
  17,                                  /* colNo */
  "FacetsOnNodeNum",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  966,                                 /* lineNo */
  12,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  878,                                 /* lineNo */
  30,                                  /* colNo */
  "nodeABCD",                          /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  878,                                 /* lineNo */
  30,                                  /* colNo */
  "FacetsOnNodeNum",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  875,                                 /* lineNo */
  13,                                  /* colNo */
  "ElementsOnBoundary",                /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ae_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  874,                                 /* lineNo */
  13,                                  /* colNo */
  "ElementsOnBoundary",                /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo be_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  873,                                 /* lineNo */
  13,                                  /* colNo */
  "FacetsOnElement",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ce_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  917,                                 /* lineNo */
  5,                                   /* colNo */
  "FacetsOnInterfaceNum",              /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo de_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  917,                                 /* lineNo */
  35,                                  /* colNo */
  "FacetsOnInterfaceNum",              /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo w_emlrtDCI = { 616, /* lineNo */
  37,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ee_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  616,                                 /* lineNo */
  37,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fe_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  615,                                 /* lineNo */
  42,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ge_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  613,                                 /* lineNo */
  21,                                  /* colNo */
  "numfn",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo he_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  610,                                 /* lineNo */
  11,                                  /* colNo */
  "FacetsOnElement",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo x_emlrtDCI = { 535, /* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ie_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  453,                                 /* lineNo */
  5,                                   /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo y_emlrtDCI = { 533, /* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo ab_emlrtDCI = { 532,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo je_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  452,                                 /* lineNo */
  5,                                   /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo bb_emlrtDCI = { 529,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo cb_emlrtDCI = { 521,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo db_emlrtDCI = { 520,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo eb_emlrtDCI = { 517,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo fb_emlrtDCI = { 517,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ke_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  504,                                 /* lineNo */
  5,                                   /* colNo */
  "NodesOnElement",                    /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo le_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  416,                                 /* lineNo */
  17,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo me_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  417,                                 /* lineNo */
  17,                                  /* colNo */
  "ElementsOnNodeRow",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo gb_emlrtDCI = { 415,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ne_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  415,                                 /* lineNo */
  24,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo oe_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  413,                                 /* lineNo */
  17,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pe_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  443,                                 /* lineNo */
  5,                                   /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qe_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  262,                                 /* lineNo */
  28,                                  /* colNo */
  "NodesOnLinknum",                    /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo re_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  427,                                 /* lineNo */
  17,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo se_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  428,                                 /* lineNo */
  17,                                  /* colNo */
  "ElementsOnNodeRow",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo te_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  426,                                 /* lineNo */
  24,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ue_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  261,                                 /* lineNo */
  27,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ve_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  406,                                 /* lineNo */
  37,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo we_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  405,                                 /* lineNo */
  18,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xe_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  437,                                 /* lineNo */
  13,                                  /* colNo */
  "ElementsOnNodePBCRow",              /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ye_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  260,                                 /* lineNo */
  27,                                  /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo hb_emlrtDCI = { 260,/* lineNo */
  27,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo af_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  367,                                 /* lineNo */
  28,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  436,                                 /* lineNo */
  13,                                  /* colNo */
  "ElementsOnNodeRow",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo cf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  400,                                 /* lineNo */
  13,                                  /* colNo */
  "ElementsOnNodePBCNum",              /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo df_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  442,                                 /* lineNo */
  5,                                   /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ef_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  483,                                 /* lineNo */
  13,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ff_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  401,                                 /* lineNo */
  13,                                  /* colNo */
  "ElementsOnNodePBCRow",              /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ib_emlrtDCI = { 399,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo gf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  399,                                 /* lineNo */
  20,                                  /* colNo */
  "ElementsOnNodePBCNum",              /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  484,                                 /* lineNo */
  13,                                  /* colNo */
  "ElementsOnNodeRow",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo jb_emlrtDCI = { 481,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo if_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  481,                                 /* lineNo */
  20,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo jf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  395,                                 /* lineNo */
  20,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo kf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  492,                                 /* lineNo */
  13,                                  /* colNo */
  "ElementsOnNodeRow",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo lf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  497,                                 /* lineNo */
  5,                                   /* colNo */
  "NodesOnElement",                    /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  391,                                 /* lineNo */
  15,                                  /* colNo */
  "RegionOnElement",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo nf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  474,                                 /* lineNo */
  20,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo of_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  361,                                 /* lineNo */
  24,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo kb_emlrtDCI = { 361,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo lb_emlrtDCI = { 383,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo mb_emlrtDCI = { 463,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo pf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  295,                                 /* lineNo */
  28,                                  /* colNo */
  "NodesOnLinknum",                    /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  253,                                 /* lineNo */
  50,                                  /* colNo */
  "locPBC",                            /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  255,                                 /* lineNo */
  47,                                  /* colNo */
  "locPBC",                            /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo sf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  292,                                 /* lineNo */
  47,                                  /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo nb_emlrtDCI = { 292,/* lineNo */
  47,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo ob_emlrtDCI = { 382,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo pb_emlrtDCI = { 462,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo tf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  294,                                 /* lineNo */
  27,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo qb_emlrtDCI = { 294,/* lineNo */
  27,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo rb_emlrtDCI = { 355,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo uf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  355,                                 /* lineNo */
  13,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  231,                                 /* lineNo */
  28,                                  /* colNo */
  "NodesOnLinknum",                    /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo sb_emlrtDCI = { 231,/* lineNo */
  28,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo wf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  230,                                 /* lineNo */
  32,                                  /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo tb_emlrtDCI = { 230,/* lineNo */
  32,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo xf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  223,                                 /* lineNo */
  27,                                  /* colNo */
  "NodesOnLinknum",                    /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  352,                                 /* lineNo */
  12,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ag_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  228,                                 /* lineNo */
  22,                                  /* colNo */
  "links2",                            /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ub_emlrtDCI = { 221,/* lineNo */
  17,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo bg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  221,                                 /* lineNo */
  17,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo vb_emlrtDCI = { 280,/* lineNo */
  30,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo cg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  280,                                 /* lineNo */
  30,                                  /* colNo */
  "PBCList",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo wb_emlrtDCI = { 248,/* lineNo */
  30,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo dg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  248,                                 /* lineNo */
  30,                                  /* colNo */
  "PBCList",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo xb_emlrtDCI = { 175,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo eg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  175,                                 /* lineNo */
  13,                                  /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fg_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  175,                                 /* lineNo */
  13,                                  /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  3                                    /* checkKind */
};

static emlrtDCInfo yb_emlrtDCI = { 176,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo gg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  176,                                 /* lineNo */
  13,                                  /* colNo */
  "NodesOnLinknum",                    /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ac_emlrtDCI = { 174,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo hg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  174,                                 /* lineNo */
  20,                                  /* colNo */
  "NodesOnLinknum",                    /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo bc_emlrtDCI = { 172,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ig_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  172,                                 /* lineNo */
  13,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo cc_emlrtDCI = { 171,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo jg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  171,                                 /* lineNo */
  13,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo kg_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  171,                                 /* lineNo */
  13,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  3                                    /* checkKind */
};

static emlrtBCInfo lg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  246,                                 /* lineNo */
  12,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  277,                                 /* lineNo */
  23,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo dc_emlrtDCI = { 242,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo ec_emlrtDCI = { 273,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo fc_emlrtDCI = { 158,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ng_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  158,                                 /* lineNo */
  13,                                  /* colNo */
  "NodesOnLinknum",                    /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo gc_emlrtDCI = { 157,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo og_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  157,                                 /* lineNo */
  13,                                  /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pg_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  157,                                 /* lineNo */
  13,                                  /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  3                                    /* checkKind */
};

static emlrtDCInfo hc_emlrtDCI = { 156,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo qg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  156,                                 /* lineNo */
  20,                                  /* colNo */
  "NodesOnLinknum",                    /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ic_emlrtDCI = { 154,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo rg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  154,                                 /* lineNo */
  13,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo jc_emlrtDCI = { 153,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo sg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  153,                                 /* lineNo */
  13,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo tg_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  153,                                 /* lineNo */
  13,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  3                                    /* checkKind */
};

static emlrtBCInfo ug_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  186,                                 /* lineNo */
  12,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  185,                                 /* lineNo */
  20,                                  /* colNo */
  "MorePBC",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo kc_emlrtDCI = { 142,/* lineNo */
  16,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo wg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  142,                                 /* lineNo */
  16,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo lc_emlrtDCI = { 112,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo mc_emlrtDCI = { 111,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo nc_emlrtDCI = { 110,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo oc_emlrtDCI = { 109,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo pc_emlrtDCI = { 75, /* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo qc_emlrtDCI = { 70, /* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo rc_emlrtDCI = { 63, /* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo sc_emlrtDCI = { 63, /* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo tc_emlrtDCI = { 1252,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo uc_emlrtDCI = { 1307,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo vc_emlrtDCI = { 989,/* lineNo */
  26,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo wc_emlrtDCI = { 989,/* lineNo */
  19,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo xc_emlrtDCI = { 989,/* lineNo */
  19,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo yc_emlrtDCI = { 535,/* lineNo */
  27,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo ad_emlrtDCI = { 535,/* lineNo */
  27,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo bd_emlrtDCI = { 532,/* lineNo */
  25,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo cd_emlrtDCI = { 529,/* lineNo */
  28,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo dd_emlrtDCI = { 383,/* lineNo */
  34,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo ed_emlrtDCI = { 383,/* lineNo */
  34,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo fd_emlrtDCI = { 382,/* lineNo */
  26,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo gd_emlrtDCI = { 382,/* lineNo */
  26,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo hd_emlrtDCI = { 463,/* lineNo */
  31,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo id_emlrtDCI = { 463,/* lineNo */
  31,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo jd_emlrtDCI = { 462,/* lineNo */
  26,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo kd_emlrtDCI = { 462,/* lineNo */
  26,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo ld_emlrtDCI = { 242,/* lineNo */
  22,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo md_emlrtDCI = { 273,/* lineNo */
  22,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo nd_emlrtDCI = { 111,/* lineNo */
  31,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo od_emlrtDCI = { 109,/* lineNo */
  30,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo pd_emlrtDCI = { 75, /* lineNo */
  22,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo qd_emlrtDCI = { 75, /* lineNo */
  22,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtECInfo emlrtECI = { -1,    /* nDims */
  947,                                 /* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo xg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  947,                                 /* lineNo */
  62,                                  /* colNo */
  "FacetsOnIntMinusPBC",               /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  947,                                 /* lineNo */
  33,                                  /* colNo */
  "FacetsOnIntMinusPBC",               /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ah_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  947,                                 /* lineNo */
  85,                                  /* colNo */
  "FacetsOnIntMinusPBCNum",            /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  947,                                 /* lineNo */
  56,                                  /* colNo */
  "FacetsOnIntMinusPBCNum",            /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo b_emlrtECI = { -1,  /* nDims */
  1308,                                /* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtECInfo c_emlrtECI = { -1,  /* nDims */
  1253,                                /* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtECInfo d_emlrtECI = { 2,   /* nDims */
  1317,                                /* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo ch_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1315,                                /* lineNo */
  30,                                  /* colNo */
  "Coordinates3",                      /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo dh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1315,                                /* lineNo */
  28,                                  /* colNo */
  "Coordinates3",                      /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo eh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1282,                                /* lineNo */
  73,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo rd_emlrtDCI = { 1282,/* lineNo */
  73,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo fh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1280,                                /* lineNo */
  34,                                  /* colNo */
  "Coordinates3",                      /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1280,                                /* lineNo */
  62,                                  /* colNo */
  "Coordinates3",                      /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1219,                                /* lineNo */
  38,                                  /* colNo */
  "Coordinates3",                      /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ih_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1178,                                /* lineNo */
  38,                                  /* colNo */
  "Coordinates3",                      /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo jh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1072,                                /* lineNo */
  50,                                  /* colNo */
  "Coordinates3",                      /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo e_emlrtECI = { -1,  /* nDims */
  514,                                 /* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo kh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  514,                                 /* lineNo */
  16,                                  /* colNo */
  "Coordinates3",                      /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo lh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  514,                                 /* lineNo */
  14,                                  /* colNo */
  "Coordinates3",                      /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1269,                                /* lineNo */
  47,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo nh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1269,                                /* lineNo */
  45,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo oh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1269,                                /* lineNo */
  40,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo rb_emlrtRTEI = { 1255,/* lineNo */
  12,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo ph_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1231,                                /* lineNo */
  46,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1205,                                /* lineNo */
  46,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1190,                                /* lineNo */
  46,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo sh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1071,                                /* lineNo */
  54,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo f_emlrtECI = { -1,  /* nDims */
  1032,                                /* lineNo */
  17,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo th_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1032,                                /* lineNo */
  34,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo uh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1230,                                /* lineNo */
  68,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1230,                                /* lineNo */
  66,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1230,                                /* lineNo */
  61,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1227,                                /* lineNo */
  81,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1225,                                /* lineNo */
  51,                                  /* colNo */
  "RegionOnElement",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo sd_emlrtDCI = { 1225,/* lineNo */
  51,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ai_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1224,                                /* lineNo */
  42,                                  /* colNo */
  "elems",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo sb_emlrtRTEI = { 1222,/* lineNo */
  33,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo bi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1219,                                /* lineNo */
  65,                                  /* colNo */
  "Coordinates",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ci_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1217,                                /* lineNo */
  52,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo di_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1217,                                /* lineNo */
  43,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ei_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1217,                                /* lineNo */
  41,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo tb_emlrtRTEI = { 1213,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo fi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1204,                                /* lineNo */
  68,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1204,                                /* lineNo */
  66,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1204,                                /* lineNo */
  61,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ii_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1201,                                /* lineNo */
  81,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ji_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1199,                                /* lineNo */
  51,                                  /* colNo */
  "RegionOnElement",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo td_emlrtDCI = { 1199,/* lineNo */
  51,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ki_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1198,                                /* lineNo */
  42,                                  /* colNo */
  "elems",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo ub_emlrtRTEI = { 1196,/* lineNo */
  36,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo li_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1189,                                /* lineNo */
  68,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1189,                                /* lineNo */
  66,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ni_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1189,                                /* lineNo */
  61,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo oi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1186,                                /* lineNo */
  81,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1184,                                /* lineNo */
  51,                                  /* colNo */
  "RegionOnElement",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ud_emlrtDCI = { 1184,/* lineNo */
  51,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo qi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1183,                                /* lineNo */
  42,                                  /* colNo */
  "elems",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo vb_emlrtRTEI = { 1181,/* lineNo */
  36,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo ri_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1178,                                /* lineNo */
  64,                                  /* colNo */
  "Coordinates",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo vd_emlrtDCI = { 1178,/* lineNo */
  64,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo si_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1175,                                /* lineNo */
  52,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ti_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1175,                                /* lineNo */
  43,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ui_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1175,                                /* lineNo */
  41,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1169,                                /* lineNo */
  65,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wi_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  1169,                                /* lineNo */
  53,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1167,                                /* lineNo */
  36,                                  /* colNo */
  "nodeP2",                            /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1166,                                /* lineNo */
  48,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo aj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1165,                                /* lineNo */
  60,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1165,                                /* lineNo */
  58,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo cj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1165,                                /* lineNo */
  53,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo wd_emlrtDCI = { 1165,/* lineNo */
  53,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo dj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1163,                                /* lineNo */
  48,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ej_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1163,                                /* lineNo */
  39,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1163,                                /* lineNo */
  37,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo wb_emlrtRTEI = { 1159,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo gj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1144,                                /* lineNo */
  31,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo g_emlrtECI = { -1,  /* nDims */
  1142,                                /* lineNo */
  21,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo hj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1142,                                /* lineNo */
  61,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ij_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1142,                                /* lineNo */
  31,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo jj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1142,                                /* lineNo */
  29,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo kj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1143,                                /* lineNo */
  89,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo lj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1143,                                /* lineNo */
  74,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1143,                                /* lineNo */
  72,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo nj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1143,                                /* lineNo */
  82,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo oj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1143,                                /* lineNo */
  56,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1143,                                /* lineNo */
  41,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1143,                                /* lineNo */
  39,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1143,                                /* lineNo */
  49,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo sj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1131,                                /* lineNo */
  60,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo tj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1131,                                /* lineNo */
  45,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo uj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1131,                                /* lineNo */
  43,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1131,                                /* lineNo */
  53,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1119,                                /* lineNo */
  60,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1119,                                /* lineNo */
  45,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1119,                                /* lineNo */
  43,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ak_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1119,                                /* lineNo */
  53,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo h_emlrtECI = { -1,  /* nDims */
  1095,                                /* lineNo */
  9,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo bk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1095,                                /* lineNo */
  21,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ck_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1095,                                /* lineNo */
  19,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo dk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1095,                                /* lineNo */
  17,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo xb_emlrtRTEI = { 1098,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtDCInfo xd_emlrtDCI = { 1093,/* lineNo */
  25,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ek_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1072,                                /* lineNo */
  77,                                  /* colNo */
  "Coordinates",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1070,                                /* lineNo */
  76,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1070,                                /* lineNo */
  74,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1070,                                /* lineNo */
  69,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ik_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1067,                                /* lineNo */
  55,                                  /* colNo */
  "RegionOnElement",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo yd_emlrtDCI = { 1067,/* lineNo */
  55,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo jk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1066,                                /* lineNo */
  46,                                  /* colNo */
  "elems",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo yb_emlrtRTEI = { 1064,/* lineNo */
  37,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo kk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1060,                                /* lineNo */
  58,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo ac_emlrtRTEI = { 1038,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo lk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1031,                                /* lineNo */
  46,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1030,                                /* lineNo */
  56,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo nk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1030,                                /* lineNo */
  54,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ok_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1030,                                /* lineNo */
  49,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ae_emlrtDCI = { 1030,/* lineNo */
  49,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo pk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1028,                                /* lineNo */
  30,                                  /* colNo */
  "elems",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo bc_emlrtRTEI = { 1026,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo qk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1022,                                /* lineNo */
  42,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1009,                                /* lineNo */
  41,                                  /* colNo */
  "NodesOnElement",                    /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo be_emlrtDCI = { 1009,/* lineNo */
  41,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo sk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1001,                                /* lineNo */
  30,                                  /* colNo */
  "FacetsOnNodeNum",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo cc_emlrtRTEI = { 990,/* lineNo */
  12,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo dc_emlrtRTEI = { 962,/* lineNo */
  12,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo tk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  937,                                 /* lineNo */
  54,                                  /* colNo */
  "FacetsOnPBCNum",                    /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo uk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  937,                                 /* lineNo */
  33,                                  /* colNo */
  "FacetsOnPBCNum",                    /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  935,                                 /* lineNo */
  66,                                  /* colNo */
  "FacetsOnInterfaceNum",              /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  935,                                 /* lineNo */
  39,                                  /* colNo */
  "FacetsOnInterfaceNum",              /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo ec_emlrtRTEI = { 934,/* lineNo */
  15,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo fc_emlrtRTEI = { 929,/* lineNo */
  15,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo xk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  927,                                 /* lineNo */
  48,                                  /* colNo */
  "FacetsOnPBC",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  927,                                 /* lineNo */
  31,                                  /* colNo */
  "FacetsOnPBC",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo al_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  926,                                 /* lineNo */
  42,                                  /* colNo */
  "FacetsOnPBC",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  926,                                 /* lineNo */
  40,                                  /* colNo */
  "FacetsOnPBC",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo gc_emlrtRTEI = { 916,/* lineNo */
  11,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo cl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  914,                                 /* lineNo */
  50,                                  /* colNo */
  "FacetsOnInterface",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo dl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  914,                                 /* lineNo */
  48,                                  /* colNo */
  "FacetsOnInterface",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo el_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  910,                                 /* lineNo */
  98,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  910,                                 /* lineNo */
  96,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  910,                                 /* lineNo */
  72,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  910,                                 /* lineNo */
  70,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo il_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  910,                                 /* lineNo */
  46,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo jl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  910,                                 /* lineNo */
  44,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo kl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  909,                                 /* lineNo */
  98,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ll_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  909,                                 /* lineNo */
  96,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ml_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  909,                                 /* lineNo */
  72,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo nl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  909,                                 /* lineNo */
  70,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ol_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  909,                                 /* lineNo */
  46,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  909,                                 /* lineNo */
  44,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ql_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  908,                                 /* lineNo */
  95,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  908,                                 /* lineNo */
  93,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo sl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  908,                                 /* lineNo */
  69,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo tl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  908,                                 /* lineNo */
  67,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ul_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  908,                                 /* lineNo */
  43,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  908,                                 /* lineNo */
  41,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  892,                                 /* lineNo */
  41,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  881,                                 /* lineNo */
  37,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  850,                                 /* lineNo */
  45,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo am_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  832,                                 /* lineNo */
  41,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  907,                                 /* lineNo */
  43,                                  /* colNo */
  "ElementsOnBoundary",                /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo cm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  907,                                 /* lineNo */
  41,                                  /* colNo */
  "ElementsOnBoundary",                /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo dm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  906,                                 /* lineNo */
  37,                                  /* colNo */
  "ElementsOnFacet",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo em_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  906,                                 /* lineNo */
  35,                                  /* colNo */
  "ElementsOnFacet",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  887,                                 /* lineNo */
  73,                                  /* colNo */
  "nloop",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  887,                                 /* lineNo */
  56,                                  /* colNo */
  "nloop",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  887,                                 /* lineNo */
  45,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo im_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  844,                                 /* lineNo */
  77,                                  /* colNo */
  "nloop",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo jm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  844,                                 /* lineNo */
  60,                                  /* colNo */
  "nloop",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo km_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  844,                                 /* lineNo */
  49,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo lm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  812,                                 /* lineNo */
  43,                                  /* colNo */
  "numEonPBC",                         /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  782,                                 /* lineNo */
  100,                                 /* colNo */
  "ElementsOnNodeABCD",                /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo nm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  782,                                 /* lineNo */
  89,                                  /* colNo */
  "ElementsOnNodeABCD",                /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo om_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  782,                                 /* lineNo */
  87,                                  /* colNo */
  "ElementsOnNodeABCD",                /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  782,                                 /* lineNo */
  97,                                  /* colNo */
  "numABCD",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  780,                                 /* lineNo */
  60,                                  /* colNo */
  "ElementsOnNodeABCD",                /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  780,                                 /* lineNo */
  58,                                  /* colNo */
  "ElementsOnNodeABCD",                /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo i_emlrtECI = { 2,   /* nDims */
  765,                                 /* lineNo */
  28,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo sm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  755,                                 /* lineNo */
  81,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo tm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  754,                                 /* lineNo */
  78,                                  /* colNo */
  "nodeAABBCCDD",                      /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo um_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  754,                                 /* lineNo */
  55,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  752,                                 /* lineNo */
  48,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  751,                                 /* lineNo */
  48,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  750,                                 /* lineNo */
  48,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ym_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  749,                                 /* lineNo */
  51,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo an_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  748,                                 /* lineNo */
  51,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  747,                                 /* lineNo */
  51,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo cn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  746,                                 /* lineNo */
  68,                                  /* colNo */
  "nloopA",                            /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo dn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  746,                                 /* lineNo */
  55,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo en_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  745,                                 /* lineNo */
  66,                                  /* colNo */
  "nloopA",                            /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  745,                                 /* lineNo */
  53,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  744,                                 /* lineNo */
  41,                                  /* colNo */
  "numfnA",                            /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  730,                                 /* lineNo */
  57,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo in_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  730,                                 /* lineNo */
  55,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo jn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  730,                                 /* lineNo */
  49,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo kn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  715,                                 /* lineNo */
  33,                                  /* colNo */
  "numEonF",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ce_emlrtDCI = { 715,/* lineNo */
  33,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ln_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  696,                                 /* lineNo */
  40,                                  /* colNo */
  "RegionOnElement",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mn_emlrtBCI = { 1,  /* iFirst */
  6,                                   /* iLast */
  692,                                 /* lineNo */
  43,                                  /* colNo */
  "FacetsOnElement",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo nn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  692,                                 /* lineNo */
  37,                                  /* colNo */
  "FacetsOnElement",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo j_emlrtECI = { 2,   /* nDims */
  685,                                 /* lineNo */
  25,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo on_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  685,                                 /* lineNo */
  38,                                  /* colNo */
  "nloopA",                            /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  673,                                 /* lineNo */
  49,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  660,                                 /* lineNo */
  49,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  660,                                 /* lineNo */
  47,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo sn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  660,                                 /* lineNo */
  41,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo de_emlrtDCI = { 660,/* lineNo */
  41,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo tn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  627,                                 /* lineNo */
  88,                                  /* colNo */
  "ElementsOnNodeABCD",                /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo un_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  627,                                 /* lineNo */
  77,                                  /* colNo */
  "ElementsOnNodeABCD",                /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  627,                                 /* lineNo */
  75,                                  /* colNo */
  "ElementsOnNodeABCD",                /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  627,                                 /* lineNo */
  85,                                  /* colNo */
  "numABCD",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  625,                                 /* lineNo */
  48,                                  /* colNo */
  "ElementsOnNodeABCD",                /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  625,                                 /* lineNo */
  46,                                  /* colNo */
  "ElementsOnNodeABCD",                /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ao_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  615,                                 /* lineNo */
  48,                                  /* colNo */
  "nloop",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  615,                                 /* lineNo */
  37,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo co_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  612,                                 /* lineNo */
  32,                                  /* colNo */
  "RegionOnElement",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo do_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  574,                                 /* lineNo */
  39,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo eo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  574,                                 /* lineNo */
  37,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  574,                                 /* lineNo */
  32,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo go_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  514,                                 /* lineNo */
  41,                                  /* colNo */
  "Coordinates",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ho_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  514,                                 /* lineNo */
  39,                                  /* colNo */
  "Coordinates",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo io_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  502,                                 /* lineNo */
  36,                                  /* colNo */
  "col",                               /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo jo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  500,                                 /* lineNo */
  77,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ko_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  500,                                 /* lineNo */
  75,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo lo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  500,                                 /* lineNo */
  57,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  500,                                 /* lineNo */
  55,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo no_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  500,                                 /* lineNo */
  37,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo oo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  500,                                 /* lineNo */
  35,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo po_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  476,                                 /* lineNo */
  26,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo hc_emlrtRTEI = { 490,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo qo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  470,                                 /* lineNo */
  31,                                  /* colNo */
  "RegionOnElement",                   /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ro_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  469,                                 /* lineNo */
  43,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo so_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  469,                                 /* lineNo */
  41,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo to_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  469,                                 /* lineNo */
  36,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo uo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  450,                                 /* lineNo */
  39,                                  /* colNo */
  "col",                               /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  448,                                 /* lineNo */
  77,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  448,                                 /* lineNo */
  75,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  448,                                 /* lineNo */
  57,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  448,                                 /* lineNo */
  55,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ap_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  448,                                 /* lineNo */
  37,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  448,                                 /* lineNo */
  35,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo cp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  423,                                 /* lineNo */
  30,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo dp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  411,                                 /* lineNo */
  34,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ep_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  408,                                 /* lineNo */
  30,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo ic_emlrtRTEI = { 434,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo fp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  390,                                 /* lineNo */
  44,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  390,                                 /* lineNo */
  42,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  390,                                 /* lineNo */
  37,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ip_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  370,                                 /* lineNo */
  37,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ee_emlrtDCI = { 370,/* lineNo */
  37,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo jp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  370,                                 /* lineNo */
  31,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo fe_emlrtDCI = { 370,/* lineNo */
  31,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo kp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  370,                                 /* lineNo */
  29,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo lp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  367,                                 /* lineNo */
  24,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ge_emlrtDCI = { 367,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo mp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  363,                                 /* lineNo */
  39,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo he_emlrtDCI = { 363,/* lineNo */
  39,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo np_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  363,                                 /* lineNo */
  37,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo op_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  363,                                 /* lineNo */
  33,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ie_emlrtDCI = { 363,/* lineNo */
  33,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo pp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  361,                                 /* lineNo */
  41,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo je_emlrtDCI = { 361,/* lineNo */
  41,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo qp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  357,                                 /* lineNo */
  54,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ke_emlrtDCI = { 357,/* lineNo */
  54,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo rp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  357,                                 /* lineNo */
  45,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo le_emlrtDCI = { 357,/* lineNo */
  45,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo sp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  357,                                 /* lineNo */
  39,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo me_emlrtDCI = { 357,/* lineNo */
  39,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo tp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  345,                                 /* lineNo */
  35,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo up_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  345,                                 /* lineNo */
  33,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  345,                                 /* lineNo */
  27,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ne_emlrtDCI = { 345,/* lineNo */
  27,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo wp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  345,                                 /* lineNo */
  25,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  291,                                 /* lineNo */
  26,                                  /* colNo */
  "PBCList2",                          /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo k_emlrtECI = { -1,  /* nDims */
  292,                                 /* lineNo */
  17,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo yp_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  292,                                 /* lineNo */
  29,                                  /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  3                                    /* checkKind */
};

static emlrtBCInfo aq_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  289,                                 /* lineNo */
  62,                                  /* colNo */
  "Coordinates",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo oe_emlrtDCI = { 289,/* lineNo */
  62,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo bq_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  289,                                 /* lineNo */
  39,                                  /* colNo */
  "Coordinates",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo pe_emlrtDCI = { 289,/* lineNo */
  39,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo cq_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  288,                                 /* lineNo */
  33,                                  /* colNo */
  "newpairs",                          /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo dq_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  281,                                 /* lineNo */
  61,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo eq_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  281,                                 /* lineNo */
  49,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fq_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  279,                                 /* lineNo */
  57,                                  /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gq_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  279,                                 /* lineNo */
  35,                                  /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hq_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  279,                                 /* lineNo */
  50,                                  /* colNo */
  "NodesOnLinknum",                    /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo iq_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  276,                                 /* lineNo */
  25,                                  /* colNo */
  "MorePBC",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo l_emlrtECI = { -1,  /* nDims */
  259,                                 /* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo jq_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  259,                                 /* lineNo */
  22,                                  /* colNo */
  "PBCList2",                          /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo m_emlrtECI = { -1,  /* nDims */
  260,                                 /* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo kq_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  257,                                 /* lineNo */
  63,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo lq_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  257,                                 /* lineNo */
  42,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mq_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  257,                                 /* lineNo */
  56,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo nq_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  252,                                 /* lineNo */
  38,                                  /* colNo */
  "nodesP",                            /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo oq_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  252,                                 /* lineNo */
  23,                                  /* colNo */
  "otherP",                            /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pq_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  247,                                 /* lineNo */
  57,                                  /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qq_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  247,                                 /* lineNo */
  35,                                  /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rq_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  247,                                 /* lineNo */
  50,                                  /* colNo */
  "NodesOnLinknum",                    /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo sq_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  245,                                 /* lineNo */
  25,                                  /* colNo */
  "MorePBC",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo n_emlrtECI = { -1,  /* nDims */
  230,                                 /* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo tq_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  230,                                 /* lineNo */
  40,                                  /* colNo */
  "nodes",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo o_emlrtECI = { -1,  /* nDims */
  224,                                 /* lineNo */
  17,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo uq_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  224,                                 /* lineNo */
  26,                                  /* colNo */
  "links",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vq_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  224,                                 /* lineNo */
  23,                                  /* colNo */
  "links",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wq_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  224,                                 /* lineNo */
  69,                                  /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xq_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  224,                                 /* lineNo */
  48,                                  /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yq_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  224,                                 /* lineNo */
  63,                                  /* colNo */
  "NodesOnLinknum",                    /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo p_emlrtECI = { -1,  /* nDims */
  222,                                 /* lineNo */
  17,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo ar_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  222,                                 /* lineNo */
  37,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo br_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  222,                                 /* lineNo */
  30,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo cr_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  222,                                 /* lineNo */
  61,                                  /* colNo */
  "nodes",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo dr_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  211,                                 /* lineNo */
  19,                                  /* colNo */
  "nodes",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo q_emlrtECI = { -1,  /* nDims */
  207,                                 /* lineNo */
  21,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo er_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  207,                                 /* lineNo */
  29,                                  /* colNo */
  "nodes",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo r_emlrtECI = { -1,  /* nDims */
  204,                                 /* lineNo */
  21,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo fr_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  204,                                 /* lineNo */
  29,                                  /* colNo */
  "nodes",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gr_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  201,                                 /* lineNo */
  41,                                  /* colNo */
  "nodes",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo s_emlrtECI = { -1,  /* nDims */
  198,                                 /* lineNo */
  21,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtBCInfo hr_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  198,                                 /* lineNo */
  30,                                  /* colNo */
  "nodes",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ir_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  198,                                 /* lineNo */
  27,                                  /* colNo */
  "nodes",                             /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo jr_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  196,                                 /* lineNo */
  63,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo kr_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  196,                                 /* lineNo */
  43,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo lr_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  196,                                 /* lineNo */
  57,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo qe_emlrtDCI = { 196,/* lineNo */
  57,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo mr_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  161,                                 /* lineNo */
  37,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo re_emlrtDCI = { 161,/* lineNo */
  37,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo nr_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  161,                                 /* lineNo */
  32,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo or_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  160,                                 /* lineNo */
  30,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo se_emlrtDCI = { 160,/* lineNo */
  30,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo pr_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  143,                                 /* lineNo */
  37,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo te_emlrtDCI = { 143,/* lineNo */
  37,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo qr_emlrtBCI = { 1,  /* iFirst */
  100,                                 /* iLast */
  143,                                 /* lineNo */
  32,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rr_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  141,                                 /* lineNo */
  27,                                  /* colNo */
  "PBCList",                           /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo sr_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  250,                                 /* lineNo */
  29,                                  /* colNo */
  "locPBC",                            /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo jc_emlrtRTEI = { 283,/* lineNo */
  27,                                  /* colNo */
  "check_non_axis_size",               /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\cat.m"/* pName */
};

static emlrtRTEInfo kc_emlrtRTEI = { 26,/* lineNo */
  27,                                  /* colNo */
  "unaryMinOrMax",                     /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pName */
};

static emlrtRTEInfo lc_emlrtRTEI = { 97,/* lineNo */
  27,                                  /* colNo */
  "unaryMinOrMax",                     /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pName */
};

static emlrtRTEInfo mc_emlrtRTEI = { 13,/* lineNo */
  13,                                  /* colNo */
  "toLogicalCheck",                    /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\toLogicalCheck.m"/* pName */
};

static emlrtBCInfo tr_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  125,                                 /* lineNo */
  17,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ue_emlrtDCI = { 125,/* lineNo */
  17,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ur_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  126,                                 /* lineNo */
  17,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ve_emlrtDCI = { 126,/* lineNo */
  17,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo vr_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  128,                                 /* lineNo */
  17,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo we_emlrtDCI = { 128,/* lineNo */
  17,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo wr_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  129,                                 /* lineNo */
  17,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo xe_emlrtDCI = { 129,/* lineNo */
  17,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo xr_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  131,                                 /* lineNo */
  17,                                  /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ye_emlrtDCI = { 131,/* lineNo */
  17,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo yr_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  131,                                 /* lineNo */
  49,                                  /* colNo */
  "IA",                                /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo as_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  132,                                 /* lineNo */
  17,                                  /* colNo */
  "NodesOnLinknum",                    /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo af_emlrtDCI = { 132,/* lineNo */
  17,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo bs_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  134,                                 /* lineNo */
  17,                                  /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo bf_emlrtDCI = { 134,/* lineNo */
  17,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo cs_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  134,                                 /* lineNo */
  49,                                  /* colNo */
  "IA",                                /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ds_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  135,                                 /* lineNo */
  17,                                  /* colNo */
  "NodesOnLinknum",                    /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo cf_emlrtDCI = { 135,/* lineNo */
  17,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo es_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  251,                                 /* lineNo */
  29,                                  /* colNo */
  "locPBC",                            /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fs_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  255,                                 /* lineNo */
  32,                                  /* colNo */
  "otherP",                            /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gs_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  253,                                 /* lineNo */
  32,                                  /* colNo */
  "otherP",                            /* aName */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo jf_emlrtRTEI = { 63,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo kf_emlrtRTEI = { 72,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo lf_emlrtRTEI = { 70,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo mf_emlrtRTEI = { 75,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo nf_emlrtRTEI = { 76,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo of_emlrtRTEI = { 98,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo pf_emlrtRTEI = { 106,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo qf_emlrtRTEI = { 95,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo rf_emlrtRTEI = { 109,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo sf_emlrtRTEI = { 110,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo tf_emlrtRTEI = { 111,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo uf_emlrtRTEI = { 112,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo vf_emlrtRTEI = { 118,/* lineNo */
  32,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo wf_emlrtRTEI = { 182,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo xf_emlrtRTEI = { 31,/* lineNo */
  9,                                   /* colNo */
  "unique",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pName */
};

static emlrtRTEInfo yf_emlrtRTEI = { 182,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ag_emlrtRTEI = { 235,/* lineNo */
  25,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo bg_emlrtRTEI = { 272,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo cg_emlrtRTEI = { 241,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo dg_emlrtRTEI = { 272,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo eg_emlrtRTEI = { 241,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo fg_emlrtRTEI = { 273,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo gg_emlrtRTEI = { 242,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo hg_emlrtRTEI = { 462,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ig_emlrtRTEI = { 382,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo jg_emlrtRTEI = { 298,/* lineNo */
  25,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo kg_emlrtRTEI = { 265,/* lineNo */
  25,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo lg_emlrtRTEI = { 299,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo mg_emlrtRTEI = { 9,/* lineNo */
  10,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ng_emlrtRTEI = { 463,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo og_emlrtRTEI = { 383,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo pg_emlrtRTEI = { 357,/* lineNo */
  28,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo qg_emlrtRTEI = { 266,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo rg_emlrtRTEI = { 370,/* lineNo */
  18,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo sg_emlrtRTEI = { 357,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo tg_emlrtRTEI = { 230,/* lineNo */
  51,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ug_emlrtRTEI = { 384,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo vg_emlrtRTEI = { 361,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo wg_emlrtRTEI = { 283,/* lineNo */
  30,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo xg_emlrtRTEI = { 363,/* lineNo */
  22,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo yg_emlrtRTEI = { 498,/* lineNo */
  63,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ah_emlrtRTEI = { 363,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo bh_emlrtRTEI = { 74,/* lineNo */
  13,                                  /* colNo */
  "eml_mtimes_helper",                 /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\eml_mtimes_helper.m"/* pName */
};

static emlrtRTEInfo ch_emlrtRTEI = { 283,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo dh_emlrtRTEI = { 367,/* lineNo */
  28,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo eh_emlrtRTEI = { 500,/* lineNo */
  22,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo fh_emlrtRTEI = { 444,/* lineNo */
  65,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo gh_emlrtRTEI = { 74,/* lineNo */
  24,                                  /* colNo */
  "eml_mtimes_helper",                 /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\eml_mtimes_helper.m"/* pName */
};

static emlrtRTEInfo hh_emlrtRTEI = { 500,/* lineNo */
  42,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ih_emlrtRTEI = { 500,/* lineNo */
  62,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo jh_emlrtRTEI = { 36,/* lineNo */
  5,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo kh_emlrtRTEI = { 37,/* lineNo */
  5,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo lh_emlrtRTEI = { 502,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo mh_emlrtRTEI = { 448,/* lineNo */
  22,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo nh_emlrtRTEI = { 448,/* lineNo */
  42,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo oh_emlrtRTEI = { 448,/* lineNo */
  62,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ph_emlrtRTEI = { 505,/* lineNo */
  32,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo qh_emlrtRTEI = { 513,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo rh_emlrtRTEI = { 517,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo sh_emlrtRTEI = { 450,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo th_emlrtRTEI = { 524,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo uh_emlrtRTEI = { 520,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo vh_emlrtRTEI = { 525,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo wh_emlrtRTEI = { 521,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo xh_emlrtRTEI = { 529,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo yh_emlrtRTEI = { 530,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ai_emlrtRTEI = { 531,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo bi_emlrtRTEI = { 532,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ci_emlrtRTEI = { 533,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo di_emlrtRTEI = { 534,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ei_emlrtRTEI = { 535,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo fi_emlrtRTEI = { 906,/* lineNo */
  19,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo gi_emlrtRTEI = { 906,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo hi_emlrtRTEI = { 907,/* lineNo */
  22,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ii_emlrtRTEI = { 907,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ji_emlrtRTEI = { 908,/* lineNo */
  23,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ki_emlrtRTEI = { 908,/* lineNo */
  49,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo li_emlrtRTEI = { 908,/* lineNo */
  75,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo mi_emlrtRTEI = { 909,/* lineNo */
  26,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ni_emlrtRTEI = { 909,/* lineNo */
  52,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo oi_emlrtRTEI = { 909,/* lineNo */
  78,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo pi_emlrtRTEI = { 910,/* lineNo */
  26,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo qi_emlrtRTEI = { 28,/* lineNo */
  9,                                   /* colNo */
  "colon",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pName */
};

static emlrtRTEInfo ri_emlrtRTEI = { 910,/* lineNo */
  52,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo si_emlrtRTEI = { 910,/* lineNo */
  78,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ti_emlrtRTEI = { 625,/* lineNo */
  17,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ui_emlrtRTEI = { 28,/* lineNo */
  5,                                   /* colNo */
  "sort",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sort.m"/* pName */
};

static emlrtRTEInfo vi_emlrtRTEI = { 915,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo wi_emlrtRTEI = { 627,/* lineNo */
  56,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo xi_emlrtRTEI = { 915,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo yi_emlrtRTEI = { 32,/* lineNo */
  9,                                   /* colNo */
  "sort",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sort.m"/* pName */
};

static emlrtRTEInfo aj_emlrtRTEI = { 953,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo bj_emlrtRTEI = { 954,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo cj_emlrtRTEI = { 32,/* lineNo */
  5,                                   /* colNo */
  "sort",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sort.m"/* pName */
};

static emlrtRTEInfo dj_emlrtRTEI = { 955,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ej_emlrtRTEI = { 681,/* lineNo */
  19,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo fj_emlrtRTEI = { 979,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo gj_emlrtRTEI = { 969,/* lineNo */
  46,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo hj_emlrtRTEI = { 927,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ij_emlrtRTEI = { 928,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo jj_emlrtRTEI = { 989,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo kj_emlrtRTEI = { 40,/* lineNo */
  9,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo lj_emlrtRTEI = { 932,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo mj_emlrtRTEI = { 971,/* lineNo */
  32,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo nj_emlrtRTEI = { 982,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo oj_emlrtRTEI = { 971,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo pj_emlrtRTEI = { 982,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo qj_emlrtRTEI = { 1249,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo rj_emlrtRTEI = { 1306,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo sj_emlrtRTEI = { 935,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo tj_emlrtRTEI = { 1251,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo uj_emlrtRTEI = { 1307,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo vj_emlrtRTEI = { 1252,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo wj_emlrtRTEI = { 1093,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo xj_emlrtRTEI = { 937,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo yj_emlrtRTEI = { 1308,/* lineNo */
  25,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ak_emlrtRTEI = { 1094,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo bk_emlrtRTEI = { 936,/* lineNo */
  18,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ck_emlrtRTEI = { 1253,/* lineNo */
  25,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo dk_emlrtRTEI = { 927,/* lineNo */
  19,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ek_emlrtRTEI = { 1315,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo fk_emlrtRTEI = { 1095,/* lineNo */
  35,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo gk_emlrtRTEI = { 1300,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo hk_emlrtRTEI = { 1317,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ik_emlrtRTEI = { 1261,/* lineNo */
  17,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo jk_emlrtRTEI = { 1096,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo kk_emlrtRTEI = { 1262,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo lk_emlrtRTEI = { 1319,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo mk_emlrtRTEI = { 1322,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo nk_emlrtRTEI = { 1158,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ok_emlrtRTEI = { 1120,/* lineNo */
  38,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo pk_emlrtRTEI = { 1132,/* lineNo */
  38,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo qk_emlrtRTEI = { 1227,/* lineNo */
  40,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo rk_emlrtRTEI = { 1143,/* lineNo */
  25,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo sk_emlrtRTEI = { 1282,/* lineNo */
  32,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo tk_emlrtRTEI = { 1201,/* lineNo */
  40,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo uk_emlrtRTEI = { 1186,/* lineNo */
  40,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo vk_emlrtRTEI = { 780,/* lineNo */
  29,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo wk_emlrtRTEI = { 782,/* lineNo */
  68,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo xk_emlrtRTEI = { 282,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo yk_emlrtRTEI = { 285,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo al_emlrtRTEI = { 351,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo bl_emlrtRTEI = { 617,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo cl_emlrtRTEI = { 939,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo dl_emlrtRTEI = { 978,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo el_emlrtRTEI = { 6,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo fl_emlrtRTEI = { 504,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo gl_emlrtRTEI = { 452,/* lineNo */
  23,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo hl_emlrtRTEI = { 453,/* lineNo */
  22,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo il_emlrtRTEI = { 31,/* lineNo */
  6,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo jl_emlrtRTEI = { 497,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo kl_emlrtRTEI = { 617,/* lineNo */
  35,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ll_emlrtRTEI = { 1045,/* lineNo */
  25,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ml_emlrtRTEI = { 446,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo nl_emlrtRTEI = { 980,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ol_emlrtRTEI = { 969,/* lineNo */
  26,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo pl_emlrtRTEI = { 969,/* lineNo */
  83,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo ql_emlrtRTEI = { 1045,/* lineNo */
  38,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRTEInfo rl_emlrtRTEI = { 1045,/* lineNo */
  84,                                  /* colNo */
  "DEIPFunc3",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pName */
};

static emlrtRSInfo pq_emlrtRSI = { 320,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo qq_emlrtRSI = { 329,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo rq_emlrtRSI = { 321,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo sq_emlrtRSI = { 66, /* lineNo */
  "fprintf",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\iofun\\fprintf.m"/* pathName */
};

static emlrtRSInfo tq_emlrtRSI = { 328,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo uq_emlrtRSI = { 335,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo vq_emlrtRSI = { 365,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo wq_emlrtRSI = { 359,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo xq_emlrtRSI = { 373,/* lineNo */
  "DEIPFunc3",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc3.m"/* pathName */
};

static emlrtRSInfo yq_emlrtRSI = { 64, /* lineNo */
  "fprintf",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\iofun\\fprintf.m"/* pathName */
};

/* Function Declarations */
static const mxArray *b_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [33]);
static const mxArray *c_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [59]);
static const mxArray *c_feval(const emlrtStack *sp, const mxArray *b, const
  mxArray *c, const mxArray *d, const mxArray *e, emlrtMCInfo *location);
static const mxArray *d_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [103]);
static void disp(const emlrtStack *sp, const mxArray *b, emlrtMCInfo *location);
static const mxArray *e_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [72]);
static const mxArray *emlrt_marshallOut(const emlrtStack *sp, const char_T u[53]);
static const mxArray *g_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [47]);
static const mxArray *h_emlrt_marshallOut(const emlrtStack *sp, const char_T u[7]);
static const mxArray *i_emlrt_marshallOut(const int64_T u);
static const mxArray *j_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [71]);

/* Function Definitions */
static const mxArray *b_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [33])
{
  static const int32_T iv[2] = { 1, 33 };

  const mxArray *m;
  const mxArray *y;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 33, m, &u[0]);
  emlrtAssign(&y, m);
  return y;
}

static const mxArray *c_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [59])
{
  static const int32_T iv[2] = { 1, 59 };

  const mxArray *m;
  const mxArray *y;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 59, m, &u[0]);
  emlrtAssign(&y, m);
  return y;
}

static const mxArray *c_feval(const emlrtStack *sp, const mxArray *b, const
  mxArray *c, const mxArray *d, const mxArray *e, emlrtMCInfo *location)
{
  const mxArray *pArrays[4];
  const mxArray *m;
  pArrays[0] = b;
  pArrays[1] = c;
  pArrays[2] = d;
  pArrays[3] = e;
  return emlrtCallMATLABR2012b(sp, 1, &m, 4, pArrays, "feval", true, location);
}

static const mxArray *d_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [103])
{
  static const int32_T iv[2] = { 1, 103 };

  const mxArray *m;
  const mxArray *y;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 103, m, &u[0]);
  emlrtAssign(&y, m);
  return y;
}

static void disp(const emlrtStack *sp, const mxArray *b, emlrtMCInfo *location)
{
  const mxArray *pArray;
  pArray = b;
  emlrtCallMATLABR2012b(sp, 0, NULL, 1, &pArray, "disp", true, location);
}

static const mxArray *e_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [72])
{
  static const int32_T iv[2] = { 1, 72 };

  const mxArray *m;
  const mxArray *y;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 72, m, &u[0]);
  emlrtAssign(&y, m);
  return y;
}

static const mxArray *emlrt_marshallOut(const emlrtStack *sp, const char_T u[53])
{
  static const int32_T iv[2] = { 1, 53 };

  const mxArray *m;
  const mxArray *y;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 53, m, &u[0]);
  emlrtAssign(&y, m);
  return y;
}

static const mxArray *g_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [47])
{
  static const int32_T iv[2] = { 1, 47 };

  const mxArray *m;
  const mxArray *y;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 47, m, &u[0]);
  emlrtAssign(&y, m);
  return y;
}

static const mxArray *h_emlrt_marshallOut(const emlrtStack *sp, const char_T u[7])
{
  static const int32_T iv[2] = { 1, 7 };

  const mxArray *m;
  const mxArray *y;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 7, m, &u[0]);
  emlrtAssign(&y, m);
  return y;
}

static const mxArray *i_emlrt_marshallOut(const int64_T u)
{
  const mxArray *m;
  const mxArray *y;
  y = NULL;
  m = emlrtCreateNumericMatrix(1, 1, mxINT64_CLASS, mxREAL);
  *(int64_T *)emlrtMxGetData(m) = u;
  emlrtAssign(&y, m);
  return y;
}

static const mxArray *j_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [71])
{
  static const int32_T iv[2] = { 1, 71 };

  const mxArray *m;
  const mxArray *y;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 71, m, &u[0]);
  emlrtAssign(&y, m);
  return y;
}

void DEIPFunc3(const emlrtStack *sp, emxArray_real_T *InterTypes,
               emxArray_real_T *NodesOnElement, const emxArray_real_T
               *RegionOnElement, emxArray_real_T *Coordinates, real_T *numnp,
               real_T numel, real_T nummat, real_T nen, real_T ndm, real_T
               usePBC, const real_T *numMPC, emxArray_real_T *MPCList, real_T
               *numEonB, emxArray_real_T *numEonF, emxArray_real_T
               *ElementsOnBoundary, real_T *numSI, emxArray_real_T
               *ElementsOnFacet, e_sparse *ElementsOnNode, e_sparse
               *ElementsOnNodeDup, emxArray_real_T *ElementsOnNodeNum, real_T
               *numfac, emxArray_real_T *ElementsOnNodeNum2, real_T *numinttype,
               emxArray_real_T *FacetsOnElement, emxArray_real_T
               *FacetsOnElementInt, emxArray_real_T *FacetsOnInterface,
               emxArray_real_T *FacetsOnInterfaceNum, e_sparse *FacetsOnNode,
               e_sparse *FacetsOnNodeCut, e_sparse *FacetsOnNodeInt,
               emxArray_real_T *FacetsOnNodeNum, emxArray_real_T *NodeCGDG,
               e_sparse *NodeReg, emxArray_real_T *NodesOnElementCG,
               emxArray_real_T *NodesOnElementDG, emxArray_real_T
               *NodesOnInterface, real_T *NodesOnInterfaceNum, real_T *numCL,
               emxArray_real_T *NodesOnPBC, emxArray_real_T *NodesOnPBCnum,
               emxArray_real_T *NodesOnLink, emxArray_real_T *NodesOnLinknum,
               emxArray_real_T *numEonPBC, emxArray_real_T *FacetsOnPBC,
               emxArray_real_T *FacetsOnPBCNum, emxArray_real_T
               *FacetsOnIntMinusPBC, emxArray_real_T *FacetsOnIntMinusPBCNum)
{
  static const char_T cv3[103] = { 'E', 'r', 'r', 'o', 'r', ',', ' ', 'c', 'o',
    'n', 'n', 'e', 'c', 't', 'i', 'v', 'i', 't', 'y', ':', ' ', 's', 'i', 'z',
    'e', ' ', 'o', 'f', ' ', 'N', 'o', 'd', 'e', 's', 'O', 'n', 'E', 'l', 'e',
    'm', 'e', 'n', 't', ' ', 'd', 'o', 'e', 's', ' ', 'n', 'o', 't', ' ', 'm',
    'a', 't', 'c', 'h', ' ', 'p', 'a', 'r', 'a', 'm', 'e', 't', 'e', 'r', ' ',
    'n', 'e', 'n', ':', ' ', 's', 'i', 'z', 'e', '(', 'N', 'o', 'd', 'e', 's',
    'O', 'n', 'E', 'l', 'e', 'm', 'e', 'n', 't', ',', '2', ')', ' ', '~', '=',
    ' ', 'n', 'e', 'n' };

  static const char_T cv7[72] = { 'W', 'a', 'r', 'n', 'i', 'n', 'g', ':', ' ',
    's', 'o', 'm', 'e', ' ', 'i', 'n', 't', 'e', 'r', 'f', 'a', 'c', 'e', 's',
    ' ', 'n', 'o', 't', ' ', 'r', 'e', 'q', 'u', 'e', 's', 't', 'e', 'd', ' ',
    'i', 'n', ' ', 'I', 'n', 't', 'e', 'r', 't', 'y', 'p', 'e', 's', ' ', 'a',
    'n', 'd', ' ', 'h', 'a', 'v', 'e', ' ', 'b', 'e', 'e', 'n', ' ', 'a', 'd',
    'd', 'e', 'd' };

  static const char_T cv5[71] = { 'W', 'a', 'r', 'n', 'i', 'n', 'g', ':', ' ',
    's', 'o', 'm', 'e', ' ', 'e', 'n', 't', 'r', 'i', 'e', 's', ' ', 'f', 'o',
    'u', 'n', 'd', ' ', 'i', 'n', ' ', 't', 'h', 'e', ' ', 'u', 'p', 'p', 'e',
    'r', ' ', 't', 'r', 'i', 'a', 'n', 'g', 'l', 'e', ' ', 'o', 'f', ' ', 'I',
    'n', 't', 'e', 'r', 't', 'y', 'p', 'e', 's', '(', ':', ',', '%', 'i', ')',
    '\\', 'n' };

  static const char_T cv2[59] = { 'W', 'a', 'r', 'n', 'i', 'n', 'g', ',', ' ',
    'n', 'u', 'm', 'b', 'e', 'r', ' ', 'o', 'f', ' ', 'e', 'l', 'e', 'm', 'e',
    'n', 't', 's', ':', ' ', 'n', 'u', 'm', 'e', 'l', ' ', '<', ' ', 's', 'i',
    'z', 'e', '(', 'N', 'o', 'd', 'e', 's', 'O', 'n', 'E', 'l', 'e', 'm', 'e',
    'n', 't', ',', '1', ')' };

  static const char_T cv8[59] = { 'i', 'n', 't', 'r', 'a', 'f', 'a', 'c', 'e',
    ':', ' ', '%', 'i', ';', ' ', 'a', 'd', 'd', 'i', 't', 'i', 'o', 'n', 'a',
    'l', ' ', 'i', 'n', 't', 'e', 'r', 'f', 'a', 'c', 'e', ' ', 'f', 'l', 'a',
    'g', 's', ' ', 'h', 'a', 'v', 'e', ' ', 'b', 'e', 'e', 'n', ' ', 'a', 'd',
    'd', 'e', 'd', '\\', 'n' };

  static const char_T cv[53] = { 'W', 'a', 'r', 'n', 'i', 'n', 'g', ',', ' ',
    'n', 'u', 'm', 'b', 'e', 'r', ' ', 'o', 'f', ' ', 'n', 'o', 'd', 'e', 's',
    ':', ' ', 'n', 'u', 'm', 'n', 'p', ' ', '<', ' ', 's', 'i', 'z', 'e', '(',
    'C', 'o', 'o', 'r', 'd', 'i', 'n', 'a', 't', 'e', 's', ',', '1', ')' };

  static const char_T cv6[47] = { 'T', 'h', 'e', 's', 'e', ' ', 'e', 'n', 't',
    'r', 'i', 'e', 's', ' ', 'a', 'r', 'e', ' ', 'i', 'g', 'n', 'o', 'r', 'e',
    'd', ' ', 'f', 'o', 'r', ' ', 'c', 'o', 'u', 'p', 'l', 'e', 'r', ' ', 'i',
    'n', 's', 'e', 'r', 't', 'i', 'o', 'n' };

  static const char_T cv1[33] = { 'P', 'a', 'u', 's', 'e', 'd', ':', ' ', 'p',
    'r', 'e', 's', 's', ' ', 'a', 'n', 'y', ' ', 'k', 'e', 'y', ' ', 't', 'o',
    ' ', 'c', 'o', 'n', 't', 'i', 'n', 'u', 'e' };

  static const char_T cv4[7] = { 'f', 'p', 'r', 'i', 'n', 't', 'f' };

  static const int8_T nloopH[48] = { 1, 2, 1, 3, 1, 5, 4, 3, 2, 4, 2, 6, 5, 6, 5,
    7, 3, 7, 8, 7, 6, 8, 4, 8, 12, 10, 9, 11, 9, 13, 16, 14, 13, 15, 10, 14, 17,
    18, 17, 19, 11, 15, 20, 19, 18, 20, 12, 16 };

  static const int8_T nloopP[40] = { 1, 2, 3, 1, 1, 2, 3, 4, 4, 2, 5, 5, 5, 5, 3,
    6, 7, 8, 9, 4, 10, 11, 12, 10, 6, 11, 12, 13, 13, 7, 0, 0, 0, 0, 8, 0, 0, 0,
    0, 9 };

  static const int8_T nloopW[40] = { 2, 1, 1, 1, 4, 3, 3, 2, 2, 5, 5, 4, 4, 3, 6,
    6, 6, 5, 7, 10, 8, 9, 7, 8, 11, 11, 12, 10, 9, 12, 14, 13, 13, 0, 0, 15, 15,
    14, 0, 0 };

  static const int8_T nloopT[24] = { 2, 1, 1, 1, 3, 3, 2, 2, 4, 4, 4, 3, 6, 7, 5,
    5, 9, 8, 8, 6, 10, 10, 9, 7 };

  static const int8_T numfnP[5] = { 3, 3, 3, 3, 4 };

  static const int8_T numfnW[5] = { 4, 4, 4, 3, 3 };

  e_sparse dr_emlrtRSI;
  e_sparse expl_temp;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack f_st;
  emlrtStack g_st;
  emlrtStack h_st;
  emlrtStack i_st;
  emlrtStack j_st;
  emlrtStack st;
  emxArray_boolean_T d_NodesOnPBC_data;
  emxArray_boolean_T *b_NodesOnPBCnum;
  emxArray_int32_T *b_ii;
  emxArray_int32_T *idx;
  emxArray_int32_T *ii;
  emxArray_int32_T *jj;
  emxArray_int32_T *r1;
  emxArray_int32_T *r2;
  emxArray_int32_T *r3;
  emxArray_int64_T *inter;
  emxArray_real_T b_NodesOnPBC_data;
  emxArray_real_T b_links2_data;
  emxArray_real_T b_locPBC_data;
  emxArray_real_T c_NodesOnPBC_data;
  emxArray_real_T *Coordinates3;
  emxArray_real_T *ElementsOnNodeABCD;
  emxArray_real_T *ElementsOnNodePBCNum;
  emxArray_real_T *ElementsOnNodePBCRow;
  emxArray_real_T *ElementsOnNodeRow;
  emxArray_real_T *FacetsOnNodeInd;
  emxArray_real_T *MorePBC;
  emxArray_real_T *NodeRegInd;
  emxArray_real_T *NodesOnElementPBC;
  emxArray_real_T *PBCList;
  emxArray_real_T *PBCList2;
  emxArray_real_T *b_MPCList;
  emxArray_real_T *b_MorePBC;
  emxArray_real_T *b_NodeRegInd;
  emxArray_real_T *b_NodesOnInterface;
  emxArray_real_T *b_PBCList2;
  emxArray_real_T *c_NodeRegInd;
  emxArray_real_T *intermat2;
  emxArray_real_T *newpairs;
  emxArray_real_T *pairs;
  emxArray_real_T *r;
  emxArray_real_T *setAll;
  emxArray_real_T *setPBC;
  emxArray_real_T *setnPBC;
  emxArray_real_T *t5_d;
  f_sparse NodeRegSum;
  f_sparse b_expl_temp;
  g_sparse c_expl_temp;
  g_sparse d_expl_temp;
  g_sparse e_expl_temp;
  h_sparse f_expl_temp;
  h_sparse g_expl_temp;
  h_sparse h_expl_temp;
  int64_T b_qY;
  int64_T i4;
  int64_T mat;
  int64_T q0;
  int64_T qY;
  real_T locPBC_data[500];
  real_T c_locPBC_data[200];
  real_T otherP_data[200];
  real_T nodeEEFFGGHH2_data[108];
  real_T allnodes_data[101];
  real_T f_tmp_data[101];
  real_T NodesOnPBC_data[100];
  real_T links[100];
  real_T links2_data[100];
  real_T nodes[100];
  real_T nodeE2_data[27];
  real_T nodeF2_data[27];
  real_T nodeG2_data[27];
  real_T nodeABCD_data[5];
  real_T link_data[4];
  real_T numABCD_data[4];
  real_T b_i1;
  real_T b_i2;
  real_T d;
  real_T d1;
  real_T elemA;
  real_T node;
  real_T numA;
  real_T numFPBC;
  real_T numSI_tmp;
  int32_T e_tmp_data[101];
  int32_T b_tmp_data[100];
  int32_T c_tmp_data[100];
  int32_T d_tmp_data[100];
  int32_T ia_data[100];
  int32_T tmp_data[100];
  int32_T b_ii_data[27];
  int32_T c_ii_data[27];
  int32_T ii_data[27];
  int32_T NodesOnElementCG_size[2];
  int32_T allnodes_size[2];
  int32_T b_link_size[2];
  int32_T b_nodeE2_size[2];
  int32_T ii_size[2];
  int32_T input_sizes[2];
  int32_T link_size[2];
  int32_T locPBC_size[2];
  int32_T nloopA_size[2];
  int32_T nodeABCD_size[2];
  int32_T nodeE2_size[2];
  int32_T nodeEEFFGGHH2_size[2];
  int32_T tmp_size[2];
  int32_T NodesOnPBC_size[1];
  int32_T b_NodesOnElement[1];
  int32_T b_NodesOnPBC_size[1];
  int32_T c_NodesOnPBC_size[1];
  int32_T ia_size[1];
  int32_T ib_size[1];
  int32_T links2_size[1];
  int32_T ElementsOnNodePBC_m;
  int32_T ElementsOnNodePBC_n;
  int32_T b_NodesOnElementPBC;
  int32_T b_i;
  int32_T b_loop_ub;
  int32_T c_loop_ub;
  int32_T d_loop_ub;
  int32_T e_loop_ub;
  int32_T elem;
  int32_T elemB;
  int32_T end;
  int32_T exitg1;
  int32_T f_loop_ub;
  int32_T facIL;
  int32_T g_loop_ub;
  int32_T h_loop_ub;
  int32_T i;
  int32_T i1;
  int32_T i2;
  int32_T i3;
  int32_T i7;
  int32_T j;
  int32_T lenn;
  int32_T locF;
  int32_T locFA;
  int32_T loop_ub;
  int32_T nel;
  int32_T nel2;
  int32_T nloop_size_idx_0;
  int32_T nloop_size_idx_1;
  int32_T notdone;
  int32_T numPBC;
  int32_T nume;
  int32_T numfn_size_idx_1;
  int32_T partialTrueCount;
  int32_T trueCount;
  uint32_T iSec2;
  uint32_T nri;
  int8_T nloopA_data[48];
  int8_T nloop_data[48];
  int8_T g_tmp_data[27];
  int8_T numfnA_data[6];
  int8_T numfn_data[6];
  int8_T nodeAABBCCDD[4];
  int8_T i5;
  int8_T i6;
  boolean_T e_NodesOnPBC_data[100];
  boolean_T b_nodeE2_data[27];
  boolean_T b_numABCD_data[4];
  boolean_T b_guard1 = false;
  boolean_T exitg2;
  boolean_T guard1 = false;
  boolean_T old;
  (void)ndm;
  (void)numMPC;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  f_st.prev = &e_st;
  f_st.tls = e_st.tls;
  g_st.prev = &f_st;
  g_st.tls = f_st.tls;
  h_st.prev = &g_st;
  h_st.tls = g_st.tls;
  i_st.prev = &h_st;
  i_st.tls = h_st.tls;
  j_st.prev = &b_st;
  j_st.tls = b_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);

  /*  Program: DEIProgram3 */
  /*  Tim Truster */
  /*  08/15/2014 */
  /*  */
  /*  Script to convert CG mesh (e.g. from block program output) into DG mesh */
  /*  Addition of modules to insert interfaces selectively between materials */
  /*   */
  /*  Input: matrNodesOnElement InterTypes(nummat,nummat) for listing the interface */
  /*  material types to assign; only lower triangle is accessed; value 0 means */
  /*  interface is not inserted between those materials (including diagonal). */
  /*  */
  /*  Numbering of interface types (matI) is like: */
  /*  1 */
  /*  2 3 */
  /*  4 5 6 ... */
  /*  */
  /*  Output: */
  /*  Actual list of used interfaces is SurfacesI and numCL, new BC arrays and */
  /*  load arrays */
  /*  Total list of interfaces is in numIfac and Interfac, although there is */
  /*  no designation on whether the interfaces are active or not. These are */
  /*  useful for assigning different DG element types to different interfaces. */
  /*  Revisions: */
  /*  02/10/2017: adding periodic boundary conditions (PBC); uses both zipped mesh */
  /*  (collapsing linked PBC nodes into a single node and updating */
  /*  connectivity), and ghost elements (leaving original mesh, added elements */
  /*  along PBC edges that connect across the domain); both result in a 'torus' */
  /*  mesh. */
  /*  Revisions: */
  /*  04/04/2017: adding periodic BC in the form of multi-point constraints, */
  /*  which do not require the domain to be a cube. Still uses the zipped mesh, */
  /*  but does not add ghost elements. Instead, node duplication is performed */
  /*  directly on the zipped mesh, and MPC elements are applied outside of this */
  /*  subroutine. */
  /*  % flags for clearing out intermediate variables in this script; turned on */
  /*  % by default */
  /*  currvariables = who(); */
  /*  clearinter = 1; */
  numSI_tmp = 0.0;

  /*  Test for Octave vs Matlab compatibility */
  /*  isOctave = exist('OCTAVE_VERSION', 'builtin') ~= 0; */
  /*  ElementsOnNode = zeros(maxel,numnp); % Array of elements connected to nodes */
  /*  ElementsOnNodeDup = zeros(maxel,numnp); % actual node ID given to element for an original node; general counterpart of NodeMat */
  if (!(*numnp >= 0.0)) {
    emlrtNonNegativeCheckR2012b(*numnp, &rc_emlrtDCI, sp);
  }

  i = (int32_T)muDoubleScalarFloor(*numnp);
  if (*numnp != i) {
    emlrtIntegerCheckR2012b(*numnp, &sc_emlrtDCI, sp);
  }

  i1 = ElementsOnNodeNum->size[0];
  ElementsOnNodeNum->size[0] = (int32_T)*numnp;
  emxEnsureCapacity_real_T(sp, ElementsOnNodeNum, i1, &jf_emlrtRTEI);
  if (!(*numnp >= 0.0)) {
    emlrtNonNegativeCheckR2012b(*numnp, &rc_emlrtDCI, sp);
  }

  if (*numnp != i) {
    emlrtIntegerCheckR2012b(*numnp, &sc_emlrtDCI, sp);
  }

  loop_ub = (int32_T)*numnp;
  for (i1 = 0; i1 < loop_ub; i1++) {
    ElementsOnNodeNum->data[i1] = 0.0;
  }

  /*  if ~exist('usePBC','var') */
  /*      usePBC = 0; */
  /*  end */
  st.site = &uf_emlrtRSI;
  if (muDoubleScalarIsNaN(usePBC)) {
    emlrtErrorWithMessageIdR2018a(&st, &mc_emlrtRTEI, "MATLAB:nologicalnan",
      "MATLAB:nologicalnan", 0);
  }

  emxInit_real_T(sp, &ElementsOnNodePBCNum, 1, &lf_emlrtRTEI, true);
  if (usePBC != 0.0) {
    /*  create extra arrays for zipped mesh */
    /*  ElementsOnNodePBC = zeros(maxel,numnp); % Array of elements connected to nodes */
    /*  ElementsOnNodePBCDup = zeros(maxel,numnp); % actual node ID given to element for an original node; general counterpart of NodeMat */
    if (*numnp != i) {
      emlrtIntegerCheckR2012b(*numnp, &qc_emlrtDCI, sp);
    }

    i1 = ElementsOnNodePBCNum->size[0];
    ElementsOnNodePBCNum->size[0] = (int32_T)*numnp;
    emxEnsureCapacity_real_T(sp, ElementsOnNodePBCNum, i1, &lf_emlrtRTEI);
    if (*numnp != i) {
      emlrtIntegerCheckR2012b(*numnp, &qc_emlrtDCI, sp);
    }

    loop_ub = (int32_T)*numnp;
    for (i1 = 0; i1 < loop_ub; i1++) {
      ElementsOnNodePBCNum->data[i1] = 0.0;
    }
  } else {
    i1 = ElementsOnNodePBCNum->size[0];
    ElementsOnNodePBCNum->size[0] = 1;
    emxEnsureCapacity_real_T(sp, ElementsOnNodePBCNum, i1, &kf_emlrtRTEI);
    ElementsOnNodePBCNum->data[0] = 0.0;
  }

  emxInit_real_T(sp, &Coordinates3, 2, &mf_emlrtRTEI, true);

  /*  NodeReg = zeros(numnp,nummat); */
  d = numel * nen;
  if (!(d >= 0.0)) {
    emlrtNonNegativeCheckR2012b(d, &pd_emlrtDCI, sp);
  }

  d1 = (int32_T)muDoubleScalarFloor(d);
  if (d != d1) {
    emlrtIntegerCheckR2012b(d, &qd_emlrtDCI, sp);
  }

  i1 = Coordinates3->size[0] * Coordinates3->size[1];
  Coordinates3->size[0] = (int32_T)d;
  Coordinates3->size[1] = 3;
  emxEnsureCapacity_real_T(sp, Coordinates3, i1, &mf_emlrtRTEI);
  if (d != d1) {
    emlrtIntegerCheckR2012b(d, &pc_emlrtDCI, sp);
  }

  loop_ub = (int32_T)d * 3;
  for (i1 = 0; i1 < loop_ub; i1++) {
    Coordinates3->data[i1] = 0.0;
  }

  i1 = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
  NodesOnElementCG->size[0] = NodesOnElement->size[0];
  NodesOnElementCG->size[1] = NodesOnElement->size[1];
  emxEnsureCapacity_real_T(sp, NodesOnElementCG, i1, &nf_emlrtRTEI);
  loop_ub = NodesOnElement->size[0] * NodesOnElement->size[1];
  for (i1 = 0; i1 < loop_ub; i1++) {
    NodesOnElementCG->data[i1] = NodesOnElement->data[i1];
  }

  st.site = &tf_emlrtRSI;
  if (muDoubleScalarIsNaN(usePBC)) {
    emlrtErrorWithMessageIdR2018a(&st, &mc_emlrtRTEI, "MATLAB:nologicalnan",
      "MATLAB:nologicalnan", 0);
  }

  emxInit_real_T(sp, &NodesOnElementPBC, 2, &qf_emlrtRTEI, true);
  if (usePBC != 0.0) {
    /*  Add space in connectivity for ghost elements */
    /*      if nen == 4 */
    /*          NodesOnElementCG = [NodesOnElementCG zeros(numel,8-nen) */
    /*                              zeros(numPBC*2,8)]; */
    /*          nen = 8; */
    /*      elseif nen == 6 */
    /*          NodesOnElementCG = [NodesOnElementCG zeros(numel,8-nen) */
    /*                              zeros(numPBC*2,8)]; */
    /*          nen = 8; */
    /*      elseif nen == 10 */
    /*          NodesOnElementCG = [NodesOnElementCG zeros(numel,27-nen) */
    /*                              zeros(numPBC*2,27)]; */
    /*          nen = 27; */
    /*      elseif nen == 18 */
    /*          NodesOnElementCG = [NodesOnElementCG zeros(numel,27-nen) */
    /*                              zeros(numPBC*2,27)]; */
    /*          nen = 27; */
    /*      end */
    i1 = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1];
    NodesOnElementPBC->size[0] = NodesOnElement->size[0];
    NodesOnElementPBC->size[1] = NodesOnElement->size[1];
    emxEnsureCapacity_real_T(sp, NodesOnElementPBC, i1, &qf_emlrtRTEI);
    loop_ub = NodesOnElement->size[0] * NodesOnElement->size[1];
    for (i1 = 0; i1 < loop_ub; i1++) {
      NodesOnElementPBC->data[i1] = NodesOnElement->data[i1];
    }

    /*  Connectivity that is zipped, having PBC nodes condensed together */
    /*      RegionOnElement = [RegionOnElement; zeros(numPBC*2,1)]; */
  } else {
    i1 = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1];
    NodesOnElementPBC->size[0] = 1;
    NodesOnElementPBC->size[1] = 1;
    emxEnsureCapacity_real_T(sp, NodesOnElementPBC, i1, &of_emlrtRTEI);
    NodesOnElementPBC->data[0] = 0.0;

    /*  RegionOnElement = RegionOnElement; */
  }

  emxInit_real_T(sp, &PBCList, 2, &pf_emlrtRTEI, true);

  /*  Set up PBC data structures */
  i1 = PBCList->size[0] * PBCList->size[1];
  PBCList->size[0] = MPCList->size[0];
  PBCList->size[1] = 5;
  emxEnsureCapacity_real_T(sp, PBCList, i1, &pf_emlrtRTEI);
  loop_ub = MPCList->size[0] * MPCList->size[1];
  for (i1 = 0; i1 < loop_ub; i1++) {
    PBCList->data[i1] = MPCList->data[i1];
  }

  st.site = &sf_emlrtRSI;
  if (muDoubleScalarIsNaN(usePBC)) {
    emlrtErrorWithMessageIdR2018a(&st, &mc_emlrtRTEI, "MATLAB:nologicalnan",
      "MATLAB:nologicalnan", 0);
  }

  emxInit_real_T(sp, &MorePBC, 1, &yf_emlrtRTEI, true);
  emxInit_real_T(sp, &setnPBC, 1, &cl_emlrtRTEI, true);
  emxInit_real_T(sp, &PBCList2, 2, &gg_emlrtRTEI, true);
  emxInit_int32_T(sp, &ii, 1, &il_emlrtRTEI, true);
  emxInit_int32_T(sp, &idx, 1, &vc_emlrtRTEI, true);
  emxInit_real_T(sp, &b_MPCList, 2, &vf_emlrtRTEI, true);
  emxInit_boolean_T(sp, &b_NodesOnPBCnum, 1, &wf_emlrtRTEI, true);
  if (usePBC != 0.0) {
    /*  Lists of node pairs in convenient table format */
    i1 = NodesOnPBC->size[0] * NodesOnPBC->size[1];
    NodesOnPBC->size[0] = 100;
    emxEnsureCapacity_real_T(sp, NodesOnPBC, i1, &rf_emlrtRTEI);
    if (*numnp != i) {
      emlrtIntegerCheckR2012b(*numnp, &od_emlrtDCI, sp);
    }

    i1 = NodesOnPBC->size[0] * NodesOnPBC->size[1];
    NodesOnPBC->size[1] = (int32_T)*numnp;
    emxEnsureCapacity_real_T(sp, NodesOnPBC, i1, &rf_emlrtRTEI);
    if (*numnp != i) {
      emlrtIntegerCheckR2012b(*numnp, &oc_emlrtDCI, sp);
    }

    numPBC = 100 * (int32_T)*numnp;
    for (i1 = 0; i1 < numPBC; i1++) {
      NodesOnPBC->data[i1] = 0.0;
    }

    /*  nodes that are paired up */
    if (*numnp != i) {
      emlrtIntegerCheckR2012b(*numnp, &nc_emlrtDCI, sp);
    }

    i1 = NodesOnPBCnum->size[0];
    NodesOnPBCnum->size[0] = (int32_T)*numnp;
    emxEnsureCapacity_real_T(sp, NodesOnPBCnum, i1, &sf_emlrtRTEI);
    if (*numnp != i) {
      emlrtIntegerCheckR2012b(*numnp, &nc_emlrtDCI, sp);
    }

    loop_ub = (int32_T)*numnp;
    for (i1 = 0; i1 < loop_ub; i1++) {
      NodesOnPBCnum->data[i1] = 0.0;
    }

    /*  number of nodes with common pairing */
    i1 = NodesOnLink->size[0] * NodesOnLink->size[1];
    NodesOnLink->size[0] = 100;
    emxEnsureCapacity_real_T(sp, NodesOnLink, i1, &tf_emlrtRTEI);
    if (*numnp != i) {
      emlrtIntegerCheckR2012b(*numnp, &nd_emlrtDCI, sp);
    }

    i1 = NodesOnLink->size[0] * NodesOnLink->size[1];
    NodesOnLink->size[1] = (int32_T)*numnp;
    emxEnsureCapacity_real_T(sp, NodesOnLink, i1, &tf_emlrtRTEI);
    if (*numnp != i) {
      emlrtIntegerCheckR2012b(*numnp, &mc_emlrtDCI, sp);
    }

    for (i1 = 0; i1 < numPBC; i1++) {
      NodesOnLink->data[i1] = 0.0;
    }

    /*  IDs of PBC referring to node */
    if (*numnp != i) {
      emlrtIntegerCheckR2012b(*numnp, &lc_emlrtDCI, sp);
    }

    i1 = NodesOnLinknum->size[0];
    NodesOnLinknum->size[0] = (int32_T)*numnp;
    emxEnsureCapacity_real_T(sp, NodesOnLinknum, i1, &uf_emlrtRTEI);
    if (*numnp != i) {
      emlrtIntegerCheckR2012b(*numnp, &lc_emlrtDCI, sp);
    }

    loop_ub = (int32_T)*numnp;
    for (i = 0; i < loop_ub; i++) {
      NodesOnLinknum->data[i] = 0.0;
    }

    /*  number of PBC referring to node */
    /*  Handle the corners first */
    emxInit_real_T(sp, &pairs, 2, &xk_emlrtRTEI, true);
    if (usePBC == 2.0) {
      numPBC = MPCList->size[0];
    } else {
      numPBC = MPCList->size[0];
      st.site = &rf_emlrtRSI;
      loop_ub = MPCList->size[0];
      i = b_MPCList->size[0] * b_MPCList->size[1];
      b_MPCList->size[0] = loop_ub;
      b_MPCList->size[1] = 2;
      emxEnsureCapacity_real_T(&st, b_MPCList, i, &vf_emlrtRTEI);
      for (i = 0; i < loop_ub; i++) {
        b_MPCList->data[i] = MPCList->data[i + MPCList->size[0] * 2];
      }

      for (i = 0; i < loop_ub; i++) {
        b_MPCList->data[i + b_MPCList->size[0]] = MPCList->data[i +
          MPCList->size[0] * 3];
      }

      b_st.site = &vf_emlrtRSI;
      unique_rows(&b_st, b_MPCList, pairs, idx);
      i = MorePBC->size[0];
      MorePBC->size[0] = idx->size[0];
      emxEnsureCapacity_real_T(&st, MorePBC, i, &xf_emlrtRTEI);
      loop_ub = idx->size[0];
      for (i = 0; i < loop_ub; i++) {
        MorePBC->data[i] = idx->data[i];
      }

      if (pairs->size[0] != 3) {
        st.site = &qf_emlrtRSI;
        error(&st);
      } else {
        d = pairs->data[0];
        d1 = pairs->data[pairs->size[0]];
        i = (int32_T)muDoubleScalarFloor(d);
        if (d != i) {
          emlrtIntegerCheckR2012b(d, &ue_emlrtDCI, sp);
        }

        i1 = (int32_T)d;
        if ((d < 1.0) || (i1 > NodesOnPBC->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnPBC->size[1],
            &tr_emlrtBCI, sp);
        }

        i2 = 100 * (i1 - 1);
        NodesOnPBC->data[i2] = d1;
        if (i1 != i) {
          emlrtIntegerCheckR2012b(d, &ve_emlrtDCI, sp);
        }

        if ((d < 1.0) || (i1 > NodesOnPBCnum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnPBCnum->size[0],
            &ur_emlrtBCI, sp);
        }

        NodesOnPBCnum->data[(int32_T)d - 1] = 1.0;
        i3 = (int32_T)muDoubleScalarFloor(d1);
        if (d1 != i3) {
          emlrtIntegerCheckR2012b(d1, &we_emlrtDCI, sp);
        }

        b_i = (int32_T)d1;
        if ((d1 < 1.0) || (b_i > NodesOnPBC->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, NodesOnPBC->size[1],
            &vr_emlrtBCI, sp);
        }

        trueCount = 100 * (b_i - 1);
        NodesOnPBC->data[trueCount] = d;
        if (b_i != i3) {
          emlrtIntegerCheckR2012b(d1, &xe_emlrtDCI, sp);
        }

        if ((d1 < 1.0) || (b_i > NodesOnPBCnum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, NodesOnPBCnum->size[0],
            &wr_emlrtBCI, sp);
        }

        NodesOnPBCnum->data[(int32_T)d1 - 1] = 1.0;
        if (i1 != i) {
          emlrtIntegerCheckR2012b(d, &ye_emlrtDCI, sp);
        }

        if ((d < 1.0) || (i1 > NodesOnLink->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLink->size[1],
            &xr_emlrtBCI, sp);
        }

        if (1 > MorePBC->size[0]) {
          emlrtDynamicBoundsCheckR2012b(1, 1, MorePBC->size[0], &yr_emlrtBCI, sp);
        }

        NodesOnLink->data[i2] = -MorePBC->data[0];
        if (i1 != i) {
          emlrtIntegerCheckR2012b(d, &af_emlrtDCI, sp);
        }

        if ((d < 1.0) || (i1 > NodesOnLinknum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLinknum->size[0],
            &as_emlrtBCI, sp);
        }

        NodesOnLinknum->data[(int32_T)d - 1] = 1.0;
        if (b_i != i3) {
          emlrtIntegerCheckR2012b(d1, &bf_emlrtDCI, sp);
        }

        if ((d1 < 1.0) || (b_i > NodesOnLink->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, NodesOnLink->size[1],
            &bs_emlrtBCI, sp);
        }

        if (1 > MorePBC->size[0]) {
          emlrtDynamicBoundsCheckR2012b(1, 1, MorePBC->size[0], &cs_emlrtBCI, sp);
        }

        NodesOnLink->data[trueCount] = -MorePBC->data[0];
        if (b_i != i3) {
          emlrtIntegerCheckR2012b(d1, &cf_emlrtDCI, sp);
        }

        if ((d1 < 1.0) || (b_i > NodesOnLinknum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, NodesOnLinknum->size[0],
            &ds_emlrtBCI, sp);
        }

        NodesOnLinknum->data[(int32_T)d1 - 1] = 1.0;
        if (*emlrtBreakCheckR2012bFlagVar != 0) {
          emlrtBreakCheckR2012b(sp);
        }

        d = pairs->data[1];
        d1 = pairs->data[pairs->size[0] + 1];
        i = (int32_T)muDoubleScalarFloor(d);
        if (d != i) {
          emlrtIntegerCheckR2012b(d, &ue_emlrtDCI, sp);
        }

        i1 = (int32_T)d;
        if ((d < 1.0) || (i1 > NodesOnPBC->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnPBC->size[1],
            &tr_emlrtBCI, sp);
        }

        i2 = 100 * (i1 - 1);
        NodesOnPBC->data[i2] = d1;
        if (i1 != i) {
          emlrtIntegerCheckR2012b(d, &ve_emlrtDCI, sp);
        }

        if ((d < 1.0) || (i1 > NodesOnPBCnum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnPBCnum->size[0],
            &ur_emlrtBCI, sp);
        }

        NodesOnPBCnum->data[(int32_T)d - 1] = 1.0;
        i3 = (int32_T)muDoubleScalarFloor(d1);
        if (d1 != i3) {
          emlrtIntegerCheckR2012b(d1, &we_emlrtDCI, sp);
        }

        b_i = (int32_T)d1;
        if ((d1 < 1.0) || (b_i > NodesOnPBC->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, NodesOnPBC->size[1],
            &vr_emlrtBCI, sp);
        }

        trueCount = 100 * (b_i - 1) + 1;
        NodesOnPBC->data[trueCount] = d;
        if (b_i != i3) {
          emlrtIntegerCheckR2012b(d1, &xe_emlrtDCI, sp);
        }

        if ((d1 < 1.0) || (b_i > NodesOnPBCnum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, NodesOnPBCnum->size[0],
            &wr_emlrtBCI, sp);
        }

        NodesOnPBCnum->data[(int32_T)d1 - 1] = 2.0;
        if (i1 != i) {
          emlrtIntegerCheckR2012b(d, &ye_emlrtDCI, sp);
        }

        if ((d < 1.0) || (i1 > NodesOnLink->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLink->size[1],
            &xr_emlrtBCI, sp);
        }

        if (2 > MorePBC->size[0]) {
          emlrtDynamicBoundsCheckR2012b(2, 1, MorePBC->size[0], &yr_emlrtBCI, sp);
        }

        NodesOnLink->data[i2] = -MorePBC->data[1];
        if (i1 != i) {
          emlrtIntegerCheckR2012b(d, &af_emlrtDCI, sp);
        }

        if ((d < 1.0) || (i1 > NodesOnLinknum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLinknum->size[0],
            &as_emlrtBCI, sp);
        }

        NodesOnLinknum->data[(int32_T)d - 1] = 1.0;
        if (b_i != i3) {
          emlrtIntegerCheckR2012b(d1, &bf_emlrtDCI, sp);
        }

        if ((d1 < 1.0) || (b_i > NodesOnLink->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, NodesOnLink->size[1],
            &bs_emlrtBCI, sp);
        }

        if (2 > MorePBC->size[0]) {
          emlrtDynamicBoundsCheckR2012b(2, 1, MorePBC->size[0], &cs_emlrtBCI, sp);
        }

        NodesOnLink->data[trueCount] = -MorePBC->data[1];
        if (b_i != i3) {
          emlrtIntegerCheckR2012b(d1, &cf_emlrtDCI, sp);
        }

        if ((d1 < 1.0) || (b_i > NodesOnLinknum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, NodesOnLinknum->size[0],
            &ds_emlrtBCI, sp);
        }

        NodesOnLinknum->data[(int32_T)d1 - 1] = 2.0;
        if (*emlrtBreakCheckR2012bFlagVar != 0) {
          emlrtBreakCheckR2012b(sp);
        }

        d = pairs->data[2];
        d1 = pairs->data[pairs->size[0] + 2];
        i = (int32_T)muDoubleScalarFloor(d);
        if (d != i) {
          emlrtIntegerCheckR2012b(d, &ue_emlrtDCI, sp);
        }

        i1 = (int32_T)d;
        if ((d < 1.0) || (i1 > NodesOnPBC->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnPBC->size[1],
            &tr_emlrtBCI, sp);
        }

        i2 = 100 * (i1 - 1);
        NodesOnPBC->data[i2] = d1;
        if (i1 != i) {
          emlrtIntegerCheckR2012b(d, &ve_emlrtDCI, sp);
        }

        if ((d < 1.0) || (i1 > NodesOnPBCnum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnPBCnum->size[0],
            &ur_emlrtBCI, sp);
        }

        NodesOnPBCnum->data[(int32_T)d - 1] = 1.0;
        i3 = (int32_T)muDoubleScalarFloor(d1);
        if (d1 != i3) {
          emlrtIntegerCheckR2012b(d1, &we_emlrtDCI, sp);
        }

        b_i = (int32_T)d1;
        if ((d1 < 1.0) || (b_i > NodesOnPBC->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, NodesOnPBC->size[1],
            &vr_emlrtBCI, sp);
        }

        trueCount = 100 * (b_i - 1) + 2;
        NodesOnPBC->data[trueCount] = d;
        if (b_i != i3) {
          emlrtIntegerCheckR2012b(d1, &xe_emlrtDCI, sp);
        }

        if ((d1 < 1.0) || (b_i > NodesOnPBCnum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, NodesOnPBCnum->size[0],
            &wr_emlrtBCI, sp);
        }

        NodesOnPBCnum->data[(int32_T)d1 - 1] = 3.0;
        if (i1 != i) {
          emlrtIntegerCheckR2012b(d, &ye_emlrtDCI, sp);
        }

        if ((d < 1.0) || (i1 > NodesOnLink->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLink->size[1],
            &xr_emlrtBCI, sp);
        }

        if (3 > MorePBC->size[0]) {
          emlrtDynamicBoundsCheckR2012b(3, 1, MorePBC->size[0], &yr_emlrtBCI, sp);
        }

        NodesOnLink->data[i2] = -MorePBC->data[2];
        if (i1 != i) {
          emlrtIntegerCheckR2012b(d, &af_emlrtDCI, sp);
        }

        if ((d < 1.0) || (i1 > NodesOnLinknum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLinknum->size[0],
            &as_emlrtBCI, sp);
        }

        NodesOnLinknum->data[(int32_T)d - 1] = 1.0;
        if (b_i != i3) {
          emlrtIntegerCheckR2012b(d1, &bf_emlrtDCI, sp);
        }

        if ((d1 < 1.0) || (b_i > NodesOnLink->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, NodesOnLink->size[1],
            &bs_emlrtBCI, sp);
        }

        if (3 > MorePBC->size[0]) {
          emlrtDynamicBoundsCheckR2012b(3, 1, MorePBC->size[0], &cs_emlrtBCI, sp);
        }

        NodesOnLink->data[trueCount] = -MorePBC->data[2];
        if (b_i != i3) {
          emlrtIntegerCheckR2012b(d1, &cf_emlrtDCI, sp);
        }

        if ((d1 < 1.0) || (b_i > NodesOnLinknum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, NodesOnLinknum->size[0],
            &ds_emlrtBCI, sp);
        }

        NodesOnLinknum->data[(int32_T)d1 - 1] = 3.0;
        if (*emlrtBreakCheckR2012bFlagVar != 0) {
          emlrtBreakCheckR2012b(sp);
        }
      }
    }

    /*  Now do the rest of the nodes */
    for (lenn = 0; lenn < numPBC; lenn++) {
      i = MPCList->size[0];
      i1 = lenn + 1;
      if ((i1 < 1) || (i1 > i)) {
        emlrtDynamicBoundsCheckR2012b(i1, 1, i, &rr_emlrtBCI, sp);
      }

      i = (int32_T)muDoubleScalarFloor(MPCList->data[lenn]);
      if (MPCList->data[lenn] != i) {
        emlrtIntegerCheckR2012b(MPCList->data[lenn], &kc_emlrtDCI, sp);
      }

      notdone = (int32_T)MPCList->data[lenn];
      if ((notdone < 1) || (notdone > NodesOnPBCnum->size[0])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)MPCList->data[lenn], 1,
          NodesOnPBCnum->size[0], &wg_emlrtBCI, sp);
      }

      numA = NodesOnPBCnum->data[notdone - 1];
      if (1.0 > numA) {
        loop_ub = 0;
      } else {
        if (((int32_T)numA < 1) || ((int32_T)numA > 100)) {
          emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1, 100, &qr_emlrtBCI, sp);
        }

        loop_ub = (int32_T)numA;
      }

      if (MPCList->data[lenn] != i) {
        emlrtIntegerCheckR2012b(MPCList->data[lenn], &te_emlrtDCI, sp);
      }

      if (notdone > NodesOnPBC->size[1]) {
        emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnPBC->size[1],
          &pr_emlrtBCI, sp);
      }

      /*  make sure pair isn't already in the list for nodeA */
      /*      if isOctave */
      st.site = &pf_emlrtRSI;
      NodesOnPBC_size[0] = loop_ub;
      for (i1 = 0; i1 < loop_ub; i1++) {
        NodesOnPBC_data[i1] = NodesOnPBC->data[i1 + 100 * (notdone - 1)];
      }

      b_NodesOnPBC_size[0] = NodesOnPBC_size[0];
      b_st.site = &ng_emlrtRSI;
      old = local_ismember(MPCList->data[lenn + MPCList->size[0]],
                           NodesOnPBC_data, b_NodesOnPBC_size);

      /*      else */
      /*      old = ismembc(nodesAB(2),currPBC); */
      /*      end */
      if (!old) {
        /*  add the pair to nodeA, expand by one */
        if (MPCList->data[lenn] != i) {
          emlrtIntegerCheckR2012b(MPCList->data[lenn], &jc_emlrtDCI, sp);
        }

        if (notdone > NodesOnPBC->size[1]) {
          emlrtDynamicBoundsCheckR2012b((int32_T)MPCList->data[lenn], 1,
            NodesOnPBC->size[1], &sg_emlrtBCI, sp);
        }

        if (((int32_T)(numA + 1.0) < 1) || ((int32_T)(numA + 1.0) > 100)) {
          emlrtDynamicBoundsCheckR2012b((int32_T)(numA + 1.0), 1, 100,
            &tg_emlrtBCI, sp);
        }

        i1 = 100 * (notdone - 1);
        NodesOnPBC->data[((int32_T)(numA + 1.0) + i1) - 1] = MPCList->data[lenn
          + MPCList->size[0]];
        if (MPCList->data[lenn] != i) {
          emlrtIntegerCheckR2012b(MPCList->data[lenn], &ic_emlrtDCI, sp);
        }

        if (notdone > NodesOnPBCnum->size[0]) {
          emlrtDynamicBoundsCheckR2012b((int32_T)MPCList->data[lenn], 1,
            NodesOnPBCnum->size[0], &rg_emlrtBCI, sp);
        }

        NodesOnPBCnum->data[notdone - 1]++;

        /*  Record which PBC ID refers to these nodes */
        if (MPCList->data[lenn] != i) {
          emlrtIntegerCheckR2012b(MPCList->data[lenn], &hc_emlrtDCI, sp);
        }

        if (notdone > NodesOnLinknum->size[0]) {
          emlrtDynamicBoundsCheckR2012b((int32_T)MPCList->data[lenn], 1,
            NodesOnLinknum->size[0], &qg_emlrtBCI, sp);
        }

        numA = NodesOnLinknum->data[notdone - 1] + 1.0;
        if (MPCList->data[lenn] != i) {
          emlrtIntegerCheckR2012b(MPCList->data[lenn], &gc_emlrtDCI, sp);
        }

        if (notdone > NodesOnLink->size[1]) {
          emlrtDynamicBoundsCheckR2012b((int32_T)MPCList->data[lenn], 1,
            NodesOnLink->size[1], &og_emlrtBCI, sp);
        }

        if (((int32_T)numA < 1) || ((int32_T)numA > 100)) {
          emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1, 100, &pg_emlrtBCI, sp);
        }

        NodesOnLink->data[((int32_T)numA + i1) - 1] = (real_T)lenn + 1.0;
        if (MPCList->data[lenn] != i) {
          emlrtIntegerCheckR2012b(MPCList->data[lenn], &fc_emlrtDCI, sp);
        }

        if (notdone > NodesOnLinknum->size[0]) {
          emlrtDynamicBoundsCheckR2012b((int32_T)MPCList->data[lenn], 1,
            NodesOnLinknum->size[0], &ng_emlrtBCI, sp);
        }

        NodesOnLinknum->data[notdone - 1] = numA;
      }

      i = (int32_T)muDoubleScalarFloor(MPCList->data[lenn + MPCList->size[0]]);
      if (MPCList->data[lenn + MPCList->size[0]] != i) {
        emlrtIntegerCheckR2012b(MPCList->data[lenn + MPCList->size[0]],
          &se_emlrtDCI, sp);
      }

      notdone = (int32_T)MPCList->data[lenn + MPCList->size[0]];
      if ((notdone < 1) || (notdone > NodesOnPBCnum->size[0])) {
        emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnPBCnum->size[0],
          &or_emlrtBCI, sp);
      }

      d = NodesOnPBCnum->data[notdone - 1];
      if (1.0 > d) {
        loop_ub = 0;
      } else {
        if (((int32_T)d < 1) || ((int32_T)d > 100)) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, 100, &nr_emlrtBCI, sp);
        }

        loop_ub = (int32_T)d;
      }

      if (MPCList->data[lenn + MPCList->size[0]] != i) {
        emlrtIntegerCheckR2012b(MPCList->data[lenn + MPCList->size[0]],
          &re_emlrtDCI, sp);
      }

      if (notdone > NodesOnPBC->size[1]) {
        emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnPBC->size[1],
          &mr_emlrtBCI, sp);
      }

      /*  make sure pair isn't already in the list for nodeB */
      /*      if isOctave */
      st.site = &of_emlrtRSI;
      NodesOnPBC_size[0] = loop_ub;
      for (i1 = 0; i1 < loop_ub; i1++) {
        NodesOnPBC_data[i1] = NodesOnPBC->data[i1 + 100 * (notdone - 1)];
      }

      b_NodesOnPBC_size[0] = NodesOnPBC_size[0];
      b_st.site = &ng_emlrtRSI;
      old = local_ismember(MPCList->data[lenn], NodesOnPBC_data,
                           b_NodesOnPBC_size);

      /*      else */
      /*      old = ismembc(nodesAB(1),currPBC); */
      /*      end */
      if (!old) {
        /*  add the pair to nodeB, expand by one */
        if (MPCList->data[lenn + MPCList->size[0]] != i) {
          emlrtIntegerCheckR2012b(MPCList->data[lenn + MPCList->size[0]],
            &cc_emlrtDCI, sp);
        }

        if (notdone > NodesOnPBC->size[1]) {
          emlrtDynamicBoundsCheckR2012b((int32_T)MPCList->data[lenn +
            MPCList->size[0]], 1, NodesOnPBC->size[1], &jg_emlrtBCI, sp);
        }

        if (((int32_T)(d + 1.0) < 1) || ((int32_T)(d + 1.0) > 100)) {
          emlrtDynamicBoundsCheckR2012b((int32_T)(d + 1.0), 1, 100, &kg_emlrtBCI,
            sp);
        }

        i1 = 100 * (notdone - 1);
        NodesOnPBC->data[((int32_T)(d + 1.0) + i1) - 1] = MPCList->data[lenn];
        if (MPCList->data[lenn + MPCList->size[0]] != i) {
          emlrtIntegerCheckR2012b(MPCList->data[lenn + MPCList->size[0]],
            &bc_emlrtDCI, sp);
        }

        if (notdone > NodesOnPBCnum->size[0]) {
          emlrtDynamicBoundsCheckR2012b((int32_T)MPCList->data[lenn +
            MPCList->size[0]], 1, NodesOnPBCnum->size[0], &ig_emlrtBCI, sp);
        }

        NodesOnPBCnum->data[notdone - 1]++;

        /*  Record which PBC ID refers to these nodes */
        if (MPCList->data[lenn + MPCList->size[0]] != i) {
          emlrtIntegerCheckR2012b(MPCList->data[lenn + MPCList->size[0]],
            &yb_emlrtDCI, sp);
        }

        if (notdone > NodesOnLinknum->size[0]) {
          emlrtDynamicBoundsCheckR2012b((int32_T)MPCList->data[lenn +
            MPCList->size[0]], 1, NodesOnLinknum->size[0], &gg_emlrtBCI, sp);
        }

        if (MPCList->data[lenn + MPCList->size[0]] != i) {
          emlrtIntegerCheckR2012b(MPCList->data[lenn + MPCList->size[0]],
            &ac_emlrtDCI, sp);
        }

        if (notdone > NodesOnLinknum->size[0]) {
          emlrtDynamicBoundsCheckR2012b((int32_T)MPCList->data[lenn +
            MPCList->size[0]], 1, NodesOnLinknum->size[0], &hg_emlrtBCI, sp);
        }

        NodesOnLinknum->data[notdone - 1]++;
        if (MPCList->data[lenn + MPCList->size[0]] != i) {
          emlrtIntegerCheckR2012b(MPCList->data[lenn + MPCList->size[0]],
            &xb_emlrtDCI, sp);
        }

        if (notdone > NodesOnLink->size[1]) {
          emlrtDynamicBoundsCheckR2012b((int32_T)MPCList->data[lenn +
            MPCList->size[0]], 1, NodesOnLink->size[1], &eg_emlrtBCI, sp);
        }

        if (((int32_T)numA < 1) || ((int32_T)numA > 100)) {
          emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1, 100, &fg_emlrtBCI, sp);
        }

        NodesOnLink->data[((int32_T)numA + i1) - 1] = (real_T)lenn + 1.0;
      }

      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    /*  Find additional connections for nodes with pairs of pairs; this WILL find */
    /*  the extra pairs that are usually deleted to remove linear dependence in */
    /*  the stiffness matrix, e.g. the repeated edges in 2d or 3d. */
    st.site = &nf_emlrtRSI;
    i = b_NodesOnPBCnum->size[0];
    b_NodesOnPBCnum->size[0] = NodesOnPBCnum->size[0];
    emxEnsureCapacity_boolean_T(&st, b_NodesOnPBCnum, i, &wf_emlrtRTEI);
    loop_ub = NodesOnPBCnum->size[0];
    for (i = 0; i < loop_ub; i++) {
      b_NodesOnPBCnum->data[i] = (NodesOnPBCnum->data[i] > 1.0);
    }

    b_st.site = &uh_emlrtRSI;
    eml_find(&b_st, b_NodesOnPBCnum, ii);
    i = MorePBC->size[0];
    MorePBC->size[0] = ii->size[0];
    emxEnsureCapacity_real_T(&st, MorePBC, i, &yf_emlrtRTEI);
    loop_ub = ii->size[0];
    for (i = 0; i < loop_ub; i++) {
      MorePBC->data[i] = ii->data[i];
    }

    memset(&nodes[0], 0, 100U * sizeof(real_T));
    i = MorePBC->size[0];
    emxInit_real_T(sp, &r, 2, &mg_emlrtRTEI, true);
    for (b_i = 0; b_i < i; b_i++) {
      if ((b_i + 1 < 1) || (b_i + 1 > MorePBC->size[0])) {
        emlrtDynamicBoundsCheckR2012b(b_i + 1, 1, MorePBC->size[0], &vg_emlrtBCI,
          sp);
      }

      nodes[0] = MorePBC->data[b_i];
      i1 = (int32_T)MorePBC->data[b_i];
      if ((i1 < 1) || (i1 > NodesOnPBCnum->size[0])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)MorePBC->data[b_i], 1,
          NodesOnPBCnum->size[0], &ug_emlrtBCI, sp);
      }

      if (NodesOnPBCnum->data[i1 - 1] > 0.0) {
        /*  pairs not already condensed */
        notdone = 1;
        lenn = 1;

        /*  use a tree of connectivity to get to all node pairs involving MorePBC(i) */
        do {
          exitg1 = 0;
          st.site = &mf_emlrtRSI;
          if (notdone != 0) {
            /*              nodesS = nodes(1:lenn); */
            b_i1 = (real_T)lenn + 1.0;
            b_i2 = (real_T)lenn + 1.0;
            for (j = 0; j < lenn; j++) {
              numA = nodes[j];
              if (numA != (int32_T)muDoubleScalarFloor(numA)) {
                emlrtIntegerCheckR2012b(numA, &qe_emlrtDCI, sp);
              }

              if (((int32_T)numA < 1) || ((int32_T)numA > NodesOnPBCnum->size[0]))
              {
                emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1,
                  NodesOnPBCnum->size[0], &lr_emlrtBCI, sp);
              }

              d = NodesOnPBCnum->data[(int32_T)numA - 1];
              if (1.0 > d) {
                loop_ub = 0;
              } else {
                if (((int32_T)d < 1) || ((int32_T)d > 100)) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, 100, &kr_emlrtBCI,
                    sp);
                }

                loop_ub = (int32_T)d;
              }

              if ((int32_T)numA > NodesOnPBC->size[1]) {
                emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1, NodesOnPBC->
                  size[1], &jr_emlrtBCI, sp);
              }

              b_i2 = (b_i1 + (real_T)loop_ub) - 1.0;
              if (b_i1 > b_i2) {
                i1 = 0;
                i2 = 0;
              } else {
                if (((int32_T)b_i1 < 1) || ((int32_T)b_i1 > 100)) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)b_i1, 1, 100,
                    &ir_emlrtBCI, sp);
                }

                i1 = (int32_T)b_i1 - 1;
                if (((int32_T)b_i2 < 1) || ((int32_T)b_i2 > 100)) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)b_i2, 1, 100,
                    &hr_emlrtBCI, sp);
                }

                i2 = (int32_T)b_i2;
              }

              ib_size[0] = i2 - i1;
              emlrtSubAssignSizeCheckR2012b(&ib_size[0], 1, &loop_ub, 1,
                &s_emlrtECI, sp);
              for (i2 = 0; i2 < loop_ub; i2++) {
                nodes[i1 + i2] = NodesOnPBC->data[i2 + 100 * ((int32_T)numA - 1)];
              }

              b_i1 = b_i2 + 1.0;
              if (*emlrtBreakCheckR2012bFlagVar != 0) {
                emlrtBreakCheckR2012b(sp);
              }
            }

            if (1.0 > b_i2) {
              loop_ub = 0;
            } else {
              if (((int32_T)b_i2 < 1) || ((int32_T)b_i2 > 100)) {
                emlrtDynamicBoundsCheckR2012b((int32_T)b_i2, 1, 100,
                  &gr_emlrtBCI, sp);
              }

              loop_ub = (int32_T)b_i2;
            }

            st.site = &lf_emlrtRSI;
            NodesOnPBC_size[0] = loop_ub;
            if (0 <= loop_ub - 1) {
              memcpy(&NodesOnPBC_data[0], &nodes[0], loop_ub * sizeof(real_T));
            }

            b_NodesOnPBC_data.data = &NodesOnPBC_data[0];
            b_NodesOnPBC_data.size = &NodesOnPBC_size[0];
            b_NodesOnPBC_data.allocatedSize = 100;
            b_NodesOnPBC_data.numDimensions = 1;
            b_NodesOnPBC_data.canFreeData = false;
            b_st.site = &ai_emlrtRSI;
            unique_vector(&b_st, &b_NodesOnPBC_data, setnPBC);
            if (setnPBC->size[0] == lenn) {
              /*  starting list of node pairs matches the unique short list */
              if (1 > setnPBC->size[0]) {
                i1 = 0;
              } else {
                if ((setnPBC->size[0] < 1) || (setnPBC->size[0] > 100)) {
                  emlrtDynamicBoundsCheckR2012b(setnPBC->size[0], 1, 100,
                    &fr_emlrtBCI, sp);
                }

                i1 = setnPBC->size[0];
              }

              emlrtSubAssignSizeCheckR2012b(&i1, 1, &setnPBC->size[0], 1,
                &r_emlrtECI, sp);
              loop_ub = setnPBC->size[0];
              for (i1 = 0; i1 < loop_ub; i1++) {
                nodes[i1] = setnPBC->data[i1];
              }

              notdone = 0;
            } else {
              /*  found some new pairs, try searching again */
              if (1 > setnPBC->size[0]) {
                i1 = 0;
              } else {
                if ((setnPBC->size[0] < 1) || (setnPBC->size[0] > 100)) {
                  emlrtDynamicBoundsCheckR2012b(setnPBC->size[0], 1, 100,
                    &er_emlrtBCI, sp);
                }

                i1 = setnPBC->size[0];
              }

              emlrtSubAssignSizeCheckR2012b(&i1, 1, &setnPBC->size[0], 1,
                &q_emlrtECI, sp);
              loop_ub = setnPBC->size[0];
              for (i1 = 0; i1 < loop_ub; i1++) {
                nodes[i1] = setnPBC->data[i1];
              }
            }

            lenn = setnPBC->size[0];
            if (*emlrtBreakCheckR2012bFlagVar != 0) {
              emlrtBreakCheckR2012b(sp);
            }
          } else {
            exitg1 = 1;
          }
        } while (exitg1 == 0);

        if (lenn + 1U > 100U) {
          i1 = -1;
          i2 = -1;
        } else {
          if (((int32_T)(lenn + 1U) < 1) || ((int32_T)(lenn + 1U) > 100)) {
            emlrtDynamicBoundsCheckR2012b((int32_T)(lenn + 1U), 1, 100,
              &dr_emlrtBCI, sp);
          }

          i1 = lenn - 1;
          i2 = 99;
        }

        loop_ub = i2 - i1;
        if (0 <= loop_ub - 1) {
          memset(&nodes[i1 + 1], 0, ((loop_ub + i1) - i1) * sizeof(real_T));
        }

        /* clean it out */
        /*  record the maximum connected pairs into the NodesOnPBC for all */
        /*  the connected nodes in the set; also combine together the */
        /*  NodesOnLink at the same time */
        /*          lenn = length(nodes); */
        memset(&links[0], 0, 100U * sizeof(real_T));
        b_i1 = 1.0;
        b_i2 = 1.0;
        if (0 <= lenn - 1) {
          if (1 > lenn) {
            b_loop_ub = 0;
          } else {
            if (lenn > 100) {
              emlrtDynamicBoundsCheckR2012b(lenn, 1, 100, &cr_emlrtBCI, sp);
            }

            b_loop_ub = lenn;
          }

          if (1 > lenn - 1) {
            elemB = 0;
          } else {
            if ((lenn - 1 < 1) || (lenn - 1 > 100)) {
              emlrtDynamicBoundsCheckR2012b(lenn - 1, 1, 100, &br_emlrtBCI, sp);
            }

            elemB = lenn - 1;
          }
        }

        for (j = 0; j < lenn; j++) {
          d = nodes[j];
          if (d != (int32_T)muDoubleScalarFloor(d)) {
            emlrtIntegerCheckR2012b(d, &ub_emlrtDCI, sp);
          }

          if (((int32_T)d < 1) || ((int32_T)d > NodesOnPBCnum->size[0])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnPBCnum->size[0],
              &bg_emlrtBCI, sp);
          }

          NodesOnPBCnum->data[(int32_T)d - 1] = -((real_T)lenn - 1.0);

          /*  flag this node as already condensed */
          if ((int32_T)d > NodesOnPBC->size[1]) {
            emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnPBC->size[1],
              &ar_emlrtBCI, sp);
          }

          st.site = &kf_emlrtRSI;
          b_st.site = &li_emlrtRSI;
          NodesOnPBC_size[0] = b_loop_ub;
          if (0 <= b_loop_ub - 1) {
            memcpy(&NodesOnPBC_data[0], &nodes[0], b_loop_ub * sizeof(real_T));
          }

          b_NodesOnPBC_size[0] = NodesOnPBC_size[0];
          c_st.site = &mi_emlrtRSI;
          do_vectors(&c_st, NodesOnPBC_data, b_NodesOnPBC_size, nodes[j],
                     links2_data, links2_size, ia_data, ia_size, ib_size);
          emlrtSubAssignSizeCheckR2012b(&elemB, 1, &links2_size[0], 1,
            &p_emlrtECI, sp);
          loop_ub = links2_size[0];
          for (i1 = 0; i1 < loop_ub; i1++) {
            NodesOnPBC->data[i1 + 100 * ((int32_T)d - 1)] = links2_data[i1];
          }

          if ((int32_T)d > NodesOnLinknum->size[0]) {
            emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLinknum->size[0],
              &xf_emlrtBCI, sp);
          }

          b_i2 = (b_i1 + NodesOnLinknum->data[(int32_T)d - 1]) - 1.0;
          if ((int32_T)d > NodesOnLinknum->size[0]) {
            emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLinknum->size[0],
              &yq_emlrtBCI, sp);
          }

          d1 = NodesOnLinknum->data[(int32_T)d - 1];
          if (1.0 > d1) {
            loop_ub = 0;
          } else {
            if (((int32_T)d1 < 1) || ((int32_T)d1 > 100)) {
              emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, 100, &xq_emlrtBCI,
                sp);
            }

            loop_ub = (int32_T)d1;
          }

          if (b_i1 > b_i2) {
            i1 = 0;
            i2 = 0;
          } else {
            if (((int32_T)b_i1 < 1) || ((int32_T)b_i1 > 100)) {
              emlrtDynamicBoundsCheckR2012b((int32_T)b_i1, 1, 100, &vq_emlrtBCI,
                sp);
            }

            i1 = (int32_T)b_i1 - 1;
            if (((int32_T)b_i2 < 1) || ((int32_T)b_i2 > 100)) {
              emlrtDynamicBoundsCheckR2012b((int32_T)b_i2, 1, 100, &uq_emlrtBCI,
                sp);
            }

            i2 = (int32_T)b_i2;
          }

          if ((int32_T)d > NodesOnLink->size[1]) {
            emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLink->size[1],
              &wq_emlrtBCI, sp);
          }

          ib_size[0] = i2 - i1;
          emlrtSubAssignSizeCheckR2012b(&ib_size[0], 1, &loop_ub, 1, &o_emlrtECI,
            sp);
          for (i2 = 0; i2 < loop_ub; i2++) {
            links[i1 + i2] = NodesOnLink->data[i2 + 100 * ((int32_T)d - 1)];
          }

          b_i1 = b_i2 + 1.0;
          if (*emlrtBreakCheckR2012bFlagVar != 0) {
            emlrtBreakCheckR2012b(sp);
          }
        }

        if (1.0 > b_i2) {
          loop_ub = 0;
        } else {
          loop_ub = (int32_T)b_i2;
        }

        st.site = &jf_emlrtRSI;
        NodesOnPBC_size[0] = loop_ub;
        if (0 <= loop_ub - 1) {
          memcpy(&NodesOnPBC_data[0], &links[0], loop_ub * sizeof(real_T));
        }

        c_NodesOnPBC_data.data = &NodesOnPBC_data[0];
        c_NodesOnPBC_data.size = &NodesOnPBC_size[0];
        c_NodesOnPBC_data.allocatedSize = 100;
        c_NodesOnPBC_data.numDimensions = 1;
        c_NodesOnPBC_data.canFreeData = false;
        b_st.site = &ai_emlrtRSI;
        unique_vector(&b_st, &c_NodesOnPBC_data, setnPBC);
        links2_size[0] = setnPBC->size[0];
        loop_ub = setnPBC->size[0];
        end = setnPBC->size[0] - 1;
        trueCount = 0;
        for (nume = 0; nume < loop_ub; nume++) {
          links2_data[nume] = setnPBC->data[nume];
          if (setnPBC->data[nume] != 0.0) {
            trueCount++;
          }
        }

        partialTrueCount = 0;
        for (nume = 0; nume <= end; nume++) {
          if (links2_data[nume] != 0.0) {
            if ((nume + 1 < 1) || (nume + 1 > links2_size[0])) {
              emlrtDynamicBoundsCheckR2012b(nume + 1, 1, links2_size[0],
                &ag_emlrtBCI, sp);
            }

            links2_data[partialTrueCount] = links2_data[nume];
            partialTrueCount++;
          }
        }

        links2_size[0] = trueCount;
        if (1 > lenn) {
          loop_ub = 0;
        } else {
          if (lenn > 100) {
            emlrtDynamicBoundsCheckR2012b(lenn, 1, 100, &tq_emlrtBCI, sp);
          }

          loop_ub = lenn;
        }

        for (i1 = 0; i1 < loop_ub; i1++) {
          d = nodes[i1];
          if (d != (int32_T)muDoubleScalarFloor(d)) {
            emlrtIntegerCheckR2012b(d, &tb_emlrtDCI, sp);
          }

          if (((int32_T)d < 1) || ((int32_T)d > NodesOnLink->size[1])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLink->size[1],
              &wf_emlrtBCI, sp);
          }

          ia_data[i1] = (int32_T)d - 1;
        }

        i1 = r->size[0] * r->size[1];
        r->size[0] = trueCount;
        r->size[1] = lenn;
        emxEnsureCapacity_real_T(sp, r, i1, &tg_emlrtRTEI);
        for (i1 = 0; i1 < lenn; i1++) {
          for (i2 = 0; i2 < trueCount; i2++) {
            r->data[i2 + r->size[0] * i1] = links2_data[i2];
          }
        }

        if (1 > trueCount) {
          input_sizes[0] = 0;
        } else {
          input_sizes[0] = trueCount;
        }

        input_sizes[1] = loop_ub;
        emlrtSubAssignSizeCheckR2012b(&input_sizes[0], 2, &r->size[0], 2,
          &n_emlrtECI, sp);
        loop_ub = r->size[1];
        for (i1 = 0; i1 < loop_ub; i1++) {
          notdone = r->size[0];
          for (i2 = 0; i2 < notdone; i2++) {
            NodesOnLink->data[i2 + 100 * ia_data[i1]] = r->data[i2 + r->size[0] *
              i1];
          }
        }

        if (1 > lenn) {
          loop_ub = 0;
        } else {
          loop_ub = lenn;
        }

        for (i1 = 0; i1 < loop_ub; i1++) {
          d = nodes[i1];
          if (d != (int32_T)muDoubleScalarFloor(d)) {
            emlrtIntegerCheckR2012b(d, &sb_emlrtDCI, sp);
          }

          if (((int32_T)d < 1) || ((int32_T)d > NodesOnLinknum->size[0])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLinknum->size[0],
              &vf_emlrtBCI, sp);
          }

          ia_data[i1] = (int32_T)d;
        }

        for (i1 = 0; i1 < loop_ub; i1++) {
          NodesOnLinknum->data[ia_data[i1] - 1] = (int8_T)trueCount;
        }
      }

      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    emxFree_real_T(&r);
    i = setnPBC->size[0];
    setnPBC->size[0] = NodesOnPBCnum->size[0];
    emxEnsureCapacity_real_T(sp, setnPBC, i, &ag_emlrtRTEI);
    loop_ub = NodesOnPBCnum->size[0] - 1;
    for (i = 0; i <= loop_ub; i++) {
      setnPBC->data[i] = NodesOnPBCnum->data[i];
    }

    st.site = &if_emlrtRSI;
    b_abs(&st, setnPBC, NodesOnPBCnum);
    if (usePBC != 2.0) {
      /*  add in extra PBC on nodes with 3 links, so that ghost elements can always */
      /*  find their direction */
      st.site = &hf_emlrtRSI;
      i = b_NodesOnPBCnum->size[0];
      b_NodesOnPBCnum->size[0] = NodesOnPBCnum->size[0];
      emxEnsureCapacity_boolean_T(&st, b_NodesOnPBCnum, i, &cg_emlrtRTEI);
      loop_ub = NodesOnPBCnum->size[0];
      for (i = 0; i < loop_ub; i++) {
        b_NodesOnPBCnum->data[i] = (NodesOnPBCnum->data[i] == 3.0);
      }

      b_st.site = &uh_emlrtRSI;
      eml_find(&b_st, b_NodesOnPBCnum, ii);
      i = MorePBC->size[0];
      MorePBC->size[0] = ii->size[0];
      emxEnsureCapacity_real_T(&st, MorePBC, i, &eg_emlrtRTEI);
      loop_ub = ii->size[0];
      for (i = 0; i < loop_ub; i++) {
        MorePBC->data[i] = ii->data[i];
      }

      d = (real_T)MorePBC->size[0] / 4.0;
      if (d != muDoubleScalarFloor(d)) {
        emlrtIntegerCheckR2012b(d, &ld_emlrtDCI, sp);
      }

      i = PBCList2->size[0] * PBCList2->size[1];
      PBCList2->size[0] = (int32_T)d;
      PBCList2->size[1] = 4;
      emxEnsureCapacity_real_T(sp, PBCList2, i, &gg_emlrtRTEI);
      d = (real_T)MorePBC->size[0] / 4.0;
      if (d != muDoubleScalarFloor(d)) {
        emlrtIntegerCheckR2012b(d, &dc_emlrtDCI, sp);
      }

      loop_ub = (int32_T)d << 2;
      for (i = 0; i < loop_ub; i++) {
        PBCList2->data[i] = 0.0;
      }

      b_i1 = numPBC;
      i = MorePBC->size[0];
      for (b_i = 0; b_i < i; b_i++) {
        i1 = b_i + 1;
        if ((i1 < 1) || (i1 > MorePBC->size[0])) {
          emlrtDynamicBoundsCheckR2012b(i1, 1, MorePBC->size[0], &sq_emlrtBCI,
            sp);
        }

        i1 = (int32_T)MorePBC->data[b_i];
        if ((i1 < 1) || (i1 > NodesOnPBCnum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)MorePBC->data[b_i], 1,
            NodesOnPBCnum->size[0], &lg_emlrtBCI, sp);
        }

        d = NodesOnPBCnum->data[i1 - 1];
        if (d > 0.0) {
          /*  pairs not already condensed */
          if (i1 > NodesOnLinknum->size[0]) {
            emlrtDynamicBoundsCheckR2012b(i1, 1, NodesOnLinknum->size[0],
              &rq_emlrtBCI, sp);
          }

          d1 = NodesOnLinknum->data[i1 - 1];
          if (1.0 > d1) {
            loop_ub = 0;
          } else {
            if (((int32_T)d1 < 1) || ((int32_T)d1 > 100)) {
              emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, 100, &qq_emlrtBCI,
                sp);
            }

            loop_ub = (int32_T)d1;
          }

          if (i1 > NodesOnLink->size[1]) {
            emlrtDynamicBoundsCheckR2012b(i1, 1, NodesOnLink->size[1],
              &pq_emlrtBCI, sp);
          }

          notdone = MPCList->size[0];
          for (i2 = 0; i2 < 5; i2++) {
            for (i3 = 0; i3 < loop_ub; i3++) {
              d1 = NodesOnLink->data[i3 + 100 * (i1 - 1)];
              if (d1 != (int32_T)muDoubleScalarFloor(d1)) {
                emlrtIntegerCheckR2012b(d1, &wb_emlrtDCI, sp);
              }

              if (((int32_T)d1 < 1) || ((int32_T)d1 > notdone)) {
                emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, notdone,
                  &dg_emlrtBCI, sp);
              }

              locPBC_data[i3 + loop_ub * i2] = MPCList->data[((int32_T)d1 +
                MPCList->size[0] * i2) - 1];
            }
          }

          st.site = &gf_emlrtRSI;
          NodesOnPBC_size[0] = loop_ub;
          if (0 <= loop_ub - 1) {
            memcpy(&NodesOnPBC_data[0], &locPBC_data[0], loop_ub * sizeof(real_T));
          }

          links2_size[0] = loop_ub;
          for (i2 = 0; i2 < loop_ub; i2++) {
            links2_data[i2] = locPBC_data[i2 + loop_ub];
          }

          b_locPBC_data.data = &NodesOnPBC_data[0];
          b_locPBC_data.size = &NodesOnPBC_size[0];
          b_locPBC_data.allocatedSize = 100;
          b_locPBC_data.numDimensions = 1;
          b_locPBC_data.canFreeData = false;
          b_links2_data.data = &links2_data[0];
          b_links2_data.size = &links2_size[0];
          b_links2_data.allocatedSize = 100;
          b_links2_data.numDimensions = 1;
          b_links2_data.canFreeData = false;
          b_st.site = &ng_emlrtRSI;
          b_local_ismember(&b_st, &b_locPBC_data, &b_links2_data,
                           b_NodesOnPBCnum);
          end = b_NodesOnPBCnum->size[0];
          for (nume = 0; nume < end; nume++) {
            if (b_NodesOnPBCnum->data[nume] && ((nume + 1 < 1) || (nume + 1 >
                  loop_ub))) {
              emlrtDynamicBoundsCheckR2012b(nume + 1, 1, loop_ub, &sr_emlrtBCI,
                sp);
            }
          }

          end = b_NodesOnPBCnum->size[0] - 1;
          trueCount = 0;
          for (nume = 0; nume <= end; nume++) {
            if (!b_NodesOnPBCnum->data[nume]) {
              trueCount++;
            }
          }

          partialTrueCount = 0;
          for (nume = 0; nume <= end; nume++) {
            if (!b_NodesOnPBCnum->data[nume]) {
              tmp_data[partialTrueCount] = nume + 1;
              partialTrueCount++;
            }
          }

          for (i2 = 0; i2 < trueCount; i2++) {
            i3 = tmp_data[i2];
            if ((i3 < 1) || (i3 > loop_ub)) {
              emlrtDynamicBoundsCheckR2012b(i3, 1, loop_ub, &es_emlrtBCI, sp);
            }

            otherP_data[i2] = locPBC_data[tmp_data[i2] - 1];
          }

          for (i2 = 0; i2 < trueCount; i2++) {
            i3 = tmp_data[i2];
            if ((i3 < 1) || (i3 > loop_ub)) {
              emlrtDynamicBoundsCheckR2012b(i3, 1, loop_ub, &es_emlrtBCI, sp);
            }

            otherP_data[i2 + trueCount] = locPBC_data[(tmp_data[i2] + loop_ub) -
              1];
          }

          if (1 > trueCount) {
            emlrtDynamicBoundsCheckR2012b(1, 1, trueCount, &oq_emlrtBCI, sp);
          }

          end = b_NodesOnPBCnum->size[0] - 1;
          notdone = 0;
          for (nume = 0; nume <= end; nume++) {
            if (b_NodesOnPBCnum->data[nume]) {
              notdone++;
            }
          }

          i2 = notdone << 1;
          if (2 > i2) {
            emlrtDynamicBoundsCheckR2012b(2, 1, i2, &nq_emlrtBCI, sp);
          }

          end = b_NodesOnPBCnum->size[0] - 1;
          notdone = 0;
          for (nume = 0; nume <= end; nume++) {
            if (b_NodesOnPBCnum->data[nume]) {
              notdone++;
            }
          }

          partialTrueCount = 0;
          for (nume = 0; nume <= end; nume++) {
            if (b_NodesOnPBCnum->data[nume]) {
              b_tmp_data[partialTrueCount] = nume + 1;
              partialTrueCount++;
            }
          }

          if (locPBC_data[(tmp_data[0] + loop_ub) - 1] == locPBC_data
              [(b_tmp_data[1 % notdone] + loop_ub * (1 / notdone)) - 1]) {
            end = b_NodesOnPBCnum->size[0] - 1;
            notdone = 0;
            for (nume = 0; nume <= end; nume++) {
              if (b_NodesOnPBCnum->data[nume]) {
                notdone++;
              }
            }

            partialTrueCount = 0;
            for (nume = 0; nume <= end; nume++) {
              if (b_NodesOnPBCnum->data[nume]) {
                d_tmp_data[partialTrueCount] = nume + 1;
                partialTrueCount++;
              }
            }

            st.site = &ff_emlrtRSI;
            if (2 > trueCount) {
              emlrtDynamicBoundsCheckR2012b(2, 1, 1, &gs_emlrtBCI, &st);
            }

            for (i2 = 0; i2 < notdone; i2++) {
              i3 = d_tmp_data[i2];
              if ((i3 < 1) || (i3 > loop_ub)) {
                emlrtDynamicBoundsCheckR2012b(i3, 1, loop_ub, &qf_emlrtBCI, &st);
              }
            }

            b_st.site = &aj_emlrtRSI;
            c_st.site = &bj_emlrtRSI;
            if ((notdone != 1) && (notdone != 0)) {
              emlrtErrorWithMessageIdR2018a(&c_st, &jc_emlrtRTEI,
                "MATLAB:catenate:matrixDimensionMismatch",
                "MATLAB:catenate:matrixDimensionMismatch", 0);
            }

            if (notdone != 0) {
              i5 = 2;
            } else {
              i5 = 0;
            }

            locPBC_size[0] = notdone;
            locPBC_size[1] = 2;
            link_size[0] = 1;
            link_size[1] = i5 + 2;
            for (i2 = 0; i2 < notdone; i2++) {
              c_locPBC_data[i2] = locPBC_data[(d_tmp_data[i2] + loop_ub * 2) - 1];
            }

            link_data[0] = otherP_data[1];
            for (i2 = 0; i2 < notdone; i2++) {
              c_locPBC_data[i2 + locPBC_size[0]] = locPBC_data[(d_tmp_data[i2] +
                loop_ub * 3) - 1];
            }

            link_data[1] = otherP_data[0];
            loop_ub = i5;
            if (0 <= loop_ub - 1) {
              memcpy(&link_data[2], &c_locPBC_data[0], loop_ub * sizeof(real_T));
            }
          } else {
            end = b_NodesOnPBCnum->size[0] - 1;
            notdone = 0;
            for (nume = 0; nume <= end; nume++) {
              if (b_NodesOnPBCnum->data[nume]) {
                notdone++;
              }
            }

            partialTrueCount = 0;
            for (nume = 0; nume <= end; nume++) {
              if (b_NodesOnPBCnum->data[nume]) {
                c_tmp_data[partialTrueCount] = nume + 1;
                partialTrueCount++;
              }
            }

            st.site = &ef_emlrtRSI;
            if (2 > trueCount) {
              emlrtDynamicBoundsCheckR2012b(2, 1, 1, &fs_emlrtBCI, &st);
            }

            for (i2 = 0; i2 < notdone; i2++) {
              i3 = c_tmp_data[i2];
              if ((i3 < 1) || (i3 > loop_ub)) {
                emlrtDynamicBoundsCheckR2012b(i3, 1, loop_ub, &rf_emlrtBCI, &st);
              }
            }

            b_st.site = &aj_emlrtRSI;
            c_st.site = &bj_emlrtRSI;
            if ((notdone != 1) && (notdone != 0)) {
              emlrtErrorWithMessageIdR2018a(&c_st, &jc_emlrtRTEI,
                "MATLAB:catenate:matrixDimensionMismatch",
                "MATLAB:catenate:matrixDimensionMismatch", 0);
            }

            if (notdone != 0) {
              i5 = 2;
            } else {
              i5 = 0;
            }

            locPBC_size[0] = notdone;
            locPBC_size[1] = 2;
            link_size[0] = 1;
            link_size[1] = i5 + 2;
            for (i2 = 0; i2 < notdone; i2++) {
              c_locPBC_data[i2] = locPBC_data[(c_tmp_data[i2] + loop_ub * 2) - 1];
            }

            link_data[0] = otherP_data[0];
            for (i2 = 0; i2 < notdone; i2++) {
              c_locPBC_data[i2 + locPBC_size[0]] = locPBC_data[(c_tmp_data[i2] +
                loop_ub * 3) - 1];
            }

            link_data[1] = otherP_data[1];
            loop_ub = i5;
            if (0 <= loop_ub - 1) {
              memcpy(&link_data[2], &c_locPBC_data[0], loop_ub * sizeof(real_T));
            }
          }

          if (i1 > NodesOnPBCnum->size[0]) {
            emlrtDynamicBoundsCheckR2012b(i1, 1, NodesOnPBCnum->size[0],
              &mq_emlrtBCI, sp);
          }

          if (1.0 > d) {
            loop_ub = 0;
          } else {
            if (((int32_T)d < 1) || ((int32_T)d > 100)) {
              emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, 100, &lq_emlrtBCI, sp);
            }

            loop_ub = (int32_T)d;
          }

          if (i1 > NodesOnPBC->size[1]) {
            emlrtDynamicBoundsCheckR2012b((int32_T)MorePBC->data[b_i], 1,
              NodesOnPBC->size[1], &kq_emlrtBCI, sp);
          }

          notdone = loop_ub + 1;
          allnodes_data[0] = MorePBC->data[b_i];
          for (i2 = 0; i2 < loop_ub; i2++) {
            allnodes_data[i2 + 1] = NodesOnPBC->data[i2 + 100 * (i1 - 1)];
          }

          b_i1++;
          i1 = (int32_T)(b_i1 - (real_T)numPBC);
          if ((i1 < 1) || (i1 > PBCList2->size[0])) {
            emlrtDynamicBoundsCheckR2012b(i1, 1, PBCList2->size[0], &jq_emlrtBCI,
              sp);
          }

          input_sizes[0] = 1;
          input_sizes[1] = 4;
          emlrtSubAssignSizeCheckR2012b(&input_sizes[0], 2, &link_size[0], 2,
            &l_emlrtECI, sp);
          PBCList2->data[i1 - 1] = link_data[0];
          PBCList2->data[(i1 + PBCList2->size[0]) - 1] = link_data[1];
          PBCList2->data[(i1 + PBCList2->size[0] * 2) - 1] = link_data[2];
          PBCList2->data[(i1 + PBCList2->size[0] * 3) - 1] = link_data[3];
          for (i1 = 0; i1 < notdone; i1++) {
            d = allnodes_data[i1];
            if (d != (int32_T)muDoubleScalarFloor(d)) {
              emlrtIntegerCheckR2012b(d, &hb_emlrtDCI, sp);
            }

            if (((int32_T)d < 1) || ((int32_T)d > NodesOnLink->size[1])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLink->size[1],
                &ye_emlrtBCI, sp);
            }

            e_tmp_data[i1] = (int32_T)d - 1;
          }

          input_sizes[0] = 1;
          input_sizes[1] = loop_ub + 1;
          allnodes_size[0] = 1;
          allnodes_size[1] = 4;
          emlrtSubAssignSizeCheckR2012b(&input_sizes[0], 2, &allnodes_size[0], 2,
            &m_emlrtECI, sp);
          numABCD_data[0] = b_i1;
          numABCD_data[1] = b_i1;
          numABCD_data[2] = b_i1;
          numABCD_data[3] = b_i1;
          for (i1 = 0; i1 < notdone; i1++) {
            NodesOnLink->data[100 * e_tmp_data[i1] + 3] = numABCD_data[i1];
          }

          for (i1 = 0; i1 < notdone; i1++) {
            d = allnodes_data[i1];
            if (((int32_T)d < 1) || ((int32_T)d > NodesOnPBCnum->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnPBCnum->size[0],
                &ue_emlrtBCI, sp);
            }

            e_tmp_data[i1] = (int32_T)d;
          }

          for (i1 = 0; i1 < notdone; i1++) {
            NodesOnPBCnum->data[e_tmp_data[i1] - 1] = -4.0;
          }

          for (i1 = 0; i1 < notdone; i1++) {
            d = allnodes_data[i1];
            if (((int32_T)d < 1) || ((int32_T)d > NodesOnLinknum->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLinknum->size
                [0], &qe_emlrtBCI, sp);
            }

            e_tmp_data[i1] = (int32_T)d;
          }

          for (i1 = 0; i1 < notdone; i1++) {
            NodesOnLinknum->data[e_tmp_data[i1] - 1] = 4.0;
          }
        }

        if (*emlrtBreakCheckR2012bFlagVar != 0) {
          emlrtBreakCheckR2012b(sp);
        }
      }

      i = setnPBC->size[0];
      setnPBC->size[0] = NodesOnPBCnum->size[0];
      emxEnsureCapacity_real_T(sp, setnPBC, i, &kg_emlrtRTEI);
      loop_ub = NodesOnPBCnum->size[0] - 1;
      for (i = 0; i <= loop_ub; i++) {
        setnPBC->data[i] = NodesOnPBCnum->data[i];
      }

      st.site = &df_emlrtRSI;
      b_abs(&st, setnPBC, NodesOnPBCnum);
      st.site = &cf_emlrtRSI;
      b_st.site = &aj_emlrtRSI;
      c_st.site = &bj_emlrtRSI;
      if (PBCList2->size[0] != 0) {
        emlrtErrorWithMessageIdR2018a(&c_st, &jc_emlrtRTEI,
          "MATLAB:catenate:matrixDimensionMismatch",
          "MATLAB:catenate:matrixDimensionMismatch", 0);
      }

      if (PBCList2->size[0] != 0) {
        notdone = PBCList2->size[0];
      } else {
        notdone = 0;
      }

      if (MPCList->size[0] != 0) {
        input_sizes[0] = MPCList->size[0];
      } else {
        input_sizes[0] = 0;
      }

      if (PBCList2->size[0] != 0) {
        lenn = PBCList2->size[0];
      } else {
        lenn = 0;
      }

      i = PBCList->size[0] * PBCList->size[1];
      PBCList->size[0] = input_sizes[0] + notdone;
      PBCList->size[1] = 5;
      emxEnsureCapacity_real_T(&b_st, PBCList, i, &qg_emlrtRTEI);
      loop_ub = input_sizes[0];
      for (i = 0; i < 5; i++) {
        for (i1 = 0; i1 < loop_ub; i1++) {
          PBCList->data[i1 + PBCList->size[0] * i] = MPCList->data[i1 +
            MPCList->size[0] * i];
        }

        for (i1 = 0; i1 < notdone; i1++) {
          PBCList->data[(i1 + input_sizes[0]) + PBCList->size[0] * i] =
            PBCList2->data[i1 + lenn * i];
        }
      }
    } else {
      /*  add in extra PBC on nodes with 3 links, so that ghost elements can always */
      /*  find their direction */
      st.site = &bf_emlrtRSI;
      i = b_NodesOnPBCnum->size[0];
      b_NodesOnPBCnum->size[0] = NodesOnPBCnum->size[0];
      emxEnsureCapacity_boolean_T(&st, b_NodesOnPBCnum, i, &bg_emlrtRTEI);
      loop_ub = NodesOnPBCnum->size[0];
      for (i = 0; i < loop_ub; i++) {
        b_NodesOnPBCnum->data[i] = (NodesOnPBCnum->data[i] > 1.0);
      }

      b_st.site = &uh_emlrtRSI;
      eml_find(&b_st, b_NodesOnPBCnum, ii);
      i = MorePBC->size[0];
      MorePBC->size[0] = ii->size[0];
      emxEnsureCapacity_real_T(&st, MorePBC, i, &dg_emlrtRTEI);
      loop_ub = ii->size[0];
      for (i = 0; i < loop_ub; i++) {
        MorePBC->data[i] = ii->data[i];
      }

      emxInit_real_T(&st, &b_PBCList2, 2, &gg_emlrtRTEI, true);
      d = 3.0 * (real_T)MorePBC->size[0];
      if (d != (int32_T)d) {
        emlrtIntegerCheckR2012b(d, &md_emlrtDCI, sp);
      }

      i = b_PBCList2->size[0] * b_PBCList2->size[1];
      b_PBCList2->size[0] = (int32_T)d;
      b_PBCList2->size[1] = 5;
      emxEnsureCapacity_real_T(sp, b_PBCList2, i, &fg_emlrtRTEI);
      d = 3.0 * (real_T)MorePBC->size[0];
      if (d != (int32_T)d) {
        emlrtIntegerCheckR2012b(d, &ec_emlrtDCI, sp);
      }

      loop_ub = (int32_T)d * 5;
      for (i = 0; i < loop_ub; i++) {
        b_PBCList2->data[i] = 0.0;
      }

      b_i1 = numPBC;
      i = MorePBC->size[0];
      emxInit_real_T(sp, &newpairs, 2, &yk_emlrtRTEI, true);
      for (b_i = 0; b_i < i; b_i++) {
        i1 = b_i + 1;
        if ((i1 < 1) || (i1 > MorePBC->size[0])) {
          emlrtDynamicBoundsCheckR2012b(i1, 1, MorePBC->size[0], &iq_emlrtBCI,
            sp);
        }

        i1 = (int32_T)MorePBC->data[b_i];
        if ((i1 < 1) || (i1 > NodesOnPBCnum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)MorePBC->data[b_i], 1,
            NodesOnPBCnum->size[0], &mg_emlrtBCI, sp);
        }

        numA = NodesOnPBCnum->data[i1 - 1];
        if (numA > 0.0) {
          /*  pairs not already condensed */
          if (i1 > NodesOnLinknum->size[0]) {
            emlrtDynamicBoundsCheckR2012b(i1, 1, NodesOnLinknum->size[0],
              &hq_emlrtBCI, sp);
          }

          d = NodesOnLinknum->data[i1 - 1];
          if (1.0 > d) {
            loop_ub = 0;
          } else {
            if (((int32_T)d < 1) || ((int32_T)d > 100)) {
              emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, 100, &gq_emlrtBCI, sp);
            }

            loop_ub = (int32_T)d;
          }

          if (i1 > NodesOnLink->size[1]) {
            emlrtDynamicBoundsCheckR2012b(i1, 1, NodesOnLink->size[1],
              &fq_emlrtBCI, sp);
          }

          notdone = MPCList->size[0];
          for (i2 = 0; i2 < 5; i2++) {
            for (i3 = 0; i3 < loop_ub; i3++) {
              d = NodesOnLink->data[i3 + 100 * (i1 - 1)];
              if (d != (int32_T)muDoubleScalarFloor(d)) {
                emlrtIntegerCheckR2012b(d, &vb_emlrtDCI, sp);
              }

              if (((int32_T)d < 1) || ((int32_T)d > notdone)) {
                emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, notdone,
                  &cg_emlrtBCI, sp);
              }

              locPBC_data[i3 + loop_ub * i2] = MPCList->data[((int32_T)d +
                MPCList->size[0] * i2) - 1];
            }
          }

          if (((int32_T)numA < 1) || ((int32_T)numA > 100)) {
            emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1, 100, &eq_emlrtBCI,
              sp);
          }

          st.site = &af_emlrtRSI;
          if (i1 > NodesOnPBC->size[1]) {
            emlrtDynamicBoundsCheckR2012b(i1, 1, NodesOnPBC->size[1],
              &dq_emlrtBCI, &st);
          }

          b_loop_ub = (int32_T)NodesOnPBCnum->data[i1 - 1];
          allnodes_size[0] = 1;
          allnodes_size[1] = (int32_T)NodesOnPBCnum->data[i1 - 1] + 1;
          allnodes_data[0] = MorePBC->data[b_i];
          for (i2 = 0; i2 < b_loop_ub; i2++) {
            allnodes_data[i2 + 1] = NodesOnPBC->data[i2 + 100 * (i1 - 1)];
          }

          b_st.site = &cj_emlrtRSI;
          b_sort(&b_st, allnodes_data, allnodes_size);
          st.site = &ye_emlrtRSI;
          nchoosek(&st, allnodes_data, allnodes_size, pairs);
          st.site = &xe_emlrtRSI;
          b_loop_ub = pairs->size[0];
          i1 = b_MPCList->size[0] * b_MPCList->size[1];
          b_MPCList->size[0] = pairs->size[0];
          b_MPCList->size[1] = 2;
          emxEnsureCapacity_real_T(&st, b_MPCList, i1, &wg_emlrtRTEI);
          for (i1 = 0; i1 < b_loop_ub; i1++) {
            b_MPCList->data[i1] = pairs->data[i1 + pairs->size[0]];
          }

          for (i1 = 0; i1 < b_loop_ub; i1++) {
            b_MPCList->data[i1 + b_MPCList->size[0]] = pairs->data[i1];
          }

          b_st.site = &bg_emlrtRSI;
          sortIdx(&b_st, b_MPCList, idx);
          notdone = pairs->size[0] - 1;
          i1 = b_MPCList->size[0] * b_MPCList->size[1];
          b_MPCList->size[0] = pairs->size[0];
          b_MPCList->size[1] = 2;
          emxEnsureCapacity_real_T(&st, b_MPCList, i1, &wg_emlrtRTEI);
          for (i1 = 0; i1 <= notdone; i1++) {
            b_MPCList->data[i1] = pairs->data[i1 + pairs->size[0]];
          }

          for (i1 = 0; i1 <= notdone; i1++) {
            b_MPCList->data[i1 + b_MPCList->size[0]] = pairs->data[i1];
          }

          i1 = pairs->size[0] * pairs->size[1];
          pairs->size[0] = b_MPCList->size[0];
          pairs->size[1] = 2;
          emxEnsureCapacity_real_T(&st, pairs, i1, &ch_emlrtRTEI);
          b_loop_ub = b_MPCList->size[0] * b_MPCList->size[1];
          for (i1 = 0; i1 < b_loop_ub; i1++) {
            pairs->data[i1] = b_MPCList->data[i1];
          }

          b_st.site = &cg_emlrtRSI;
          apply_row_permutation(&b_st, pairs, idx);
          st.site = &we_emlrtRSI;
          locPBC_size[0] = loop_ub;
          locPBC_size[1] = 2;
          if (0 <= loop_ub - 1) {
            memcpy(&c_locPBC_data[0], &locPBC_data[0], loop_ub * sizeof(real_T));
          }

          for (i1 = 0; i1 < loop_ub; i1++) {
            c_locPBC_data[i1 + locPBC_size[0]] = locPBC_data[i1 + loop_ub];
          }

          b_locPBC_data.data = &c_locPBC_data[0];
          b_locPBC_data.size = &locPBC_size[0];
          b_locPBC_data.allocatedSize = 200;
          b_locPBC_data.numDimensions = 2;
          b_locPBC_data.canFreeData = false;
          b_st.site = &bg_emlrtRSI;
          sortIdx(&b_st, &b_locPBC_data, idx);
          i1 = b_MPCList->size[0] * b_MPCList->size[1];
          b_MPCList->size[0] = loop_ub;
          b_MPCList->size[1] = 2;
          emxEnsureCapacity_real_T(&st, b_MPCList, i1, &rc_emlrtRTEI);
          for (i1 = 0; i1 < loop_ub; i1++) {
            b_MPCList->data[i1] = locPBC_data[i1];
          }

          for (i1 = 0; i1 < loop_ub; i1++) {
            b_MPCList->data[i1 + b_MPCList->size[0]] = locPBC_data[i1 + loop_ub];
          }

          b_st.site = &cg_emlrtRSI;
          apply_row_permutation(&b_st, b_MPCList, idx);
          st.site = &ve_emlrtRSI;
          b_st.site = &li_emlrtRSI;
          c_st.site = &hj_emlrtRSI;
          do_rows(&c_st, pairs, (real_T *)b_MPCList->data, b_MPCList->size,
                  newpairs, ii, ib_size);
          i1 = newpairs->size[0];
          if (0 <= newpairs->size[0] - 1) {
            c_loop_ub = allnodes_size[1];
            d_loop_ub = allnodes_size[1];
            input_sizes[0] = 1;
          }

          if (0 <= i1 - 1) {
            tmp_size[0] = 1;
            tmp_size[1] = allnodes_size[1];
            input_sizes[1] = allnodes_size[1];
            e_loop_ub = allnodes_size[1];
          }

          for (elemB = 0; elemB < i1; elemB++) {
            i2 = elemB + 1;
            if ((i2 < 1) || (i2 > newpairs->size[0])) {
              emlrtDynamicBoundsCheckR2012b(i2, 1, newpairs->size[0],
                &cq_emlrtBCI, sp);
            }

            i2 = Coordinates->size[0];
            if (newpairs->data[elemB] != (int32_T)muDoubleScalarFloor
                (newpairs->data[elemB])) {
              emlrtIntegerCheckR2012b(newpairs->data[elemB], &pe_emlrtDCI, sp);
            }

            notdone = (int32_T)newpairs->data[elemB];
            if ((notdone < 1) || (notdone > i2)) {
              emlrtDynamicBoundsCheckR2012b(notdone, 1, i2, &bq_emlrtBCI, sp);
            }

            i2 = Coordinates->size[0];
            if (newpairs->data[elemB + newpairs->size[0]] != (int32_T)
                muDoubleScalarFloor(newpairs->data[elemB + newpairs->size[0]]))
            {
              emlrtIntegerCheckR2012b(newpairs->data[elemB + newpairs->size[0]],
                &oe_emlrtDCI, sp);
            }

            lenn = (int32_T)newpairs->data[elemB + newpairs->size[0]];
            if ((lenn < 1) || (lenn > i2)) {
              emlrtDynamicBoundsCheckR2012b(lenn, 1, i2, &aq_emlrtBCI, sp);
            }

            b_i1++;
            i2 = (int32_T)(b_i1 - (real_T)numPBC);
            if ((i2 < 1) || (i2 > b_PBCList2->size[0])) {
              emlrtDynamicBoundsCheckR2012b(i2, 1, b_PBCList2->size[0],
                &xp_emlrtBCI, sp);
            }

            b_PBCList2->data[i2 - 1] = newpairs->data[elemB];
            b_PBCList2->data[(i2 + b_PBCList2->size[0]) - 1] = newpairs->
              data[elemB + newpairs->size[0]];
            b_PBCList2->data[(i2 + b_PBCList2->size[0] * 2) - 1] =
              Coordinates->data[notdone - 1] - Coordinates->data[lenn - 1];
            b_PBCList2->data[(i2 + b_PBCList2->size[0] * 3) - 1] =
              Coordinates->data[(notdone + Coordinates->size[0]) - 1] -
              Coordinates->data[(lenn + Coordinates->size[0]) - 1];
            b_PBCList2->data[(i2 + b_PBCList2->size[0] * 4) - 1] =
              Coordinates->data[(notdone + Coordinates->size[0] * 2) - 1] -
              Coordinates->data[(lenn + Coordinates->size[0] * 2) - 1];
            i2 = (int32_T)(numA + ((real_T)elemB + 1.0));
            if ((i2 < 1) || (i2 > 100)) {
              emlrtDynamicBoundsCheckR2012b(i2, 1, 100, &yp_emlrtBCI, sp);
            }

            for (i3 = 0; i3 < c_loop_ub; i3++) {
              d = allnodes_data[i3];
              if (d != (int32_T)muDoubleScalarFloor(d)) {
                emlrtIntegerCheckR2012b(d, &nb_emlrtDCI, sp);
              }

              if (((int32_T)d < 1) || ((int32_T)d > NodesOnLink->size[1])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLink->size[1],
                  &sf_emlrtBCI, sp);
              }

              e_tmp_data[i3] = (int32_T)d - 1;
            }

            for (i3 = 0; i3 < d_loop_ub; i3++) {
              f_tmp_data[i3] = b_i1;
            }

            emlrtSubAssignSizeCheckR2012b(&input_sizes[0], 2, &tmp_size[0], 2,
              &k_emlrtECI, sp);
            for (i3 = 0; i3 < e_loop_ub; i3++) {
              NodesOnLink->data[(i2 + 100 * e_tmp_data[i3]) - 1] = f_tmp_data[i3];
            }

            if (*emlrtBreakCheckR2012bFlagVar != 0) {
              emlrtBreakCheckR2012b(sp);
            }
          }

          notdone = allnodes_size[1];
          loop_ub = allnodes_size[0] * allnodes_size[1];
          for (i1 = 0; i1 < loop_ub; i1++) {
            d = allnodes_data[i1];
            if (d != (int32_T)muDoubleScalarFloor(d)) {
              emlrtIntegerCheckR2012b(d, &qb_emlrtDCI, sp);
            }

            if (((int32_T)d < 1) || ((int32_T)d > NodesOnPBCnum->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnPBCnum->size[0],
                &tf_emlrtBCI, sp);
            }

            e_tmp_data[i1] = (int32_T)d;
          }

          for (i1 = 0; i1 < notdone; i1++) {
            NodesOnPBCnum->data[e_tmp_data[i1] - 1] = (int8_T)-allnodes_size[1];
          }

          notdone = allnodes_size[1];
          loop_ub = allnodes_size[0] * allnodes_size[1];
          for (i1 = 0; i1 < loop_ub; i1++) {
            d = allnodes_data[i1];
            if (((int32_T)d < 1) || ((int32_T)d > NodesOnLinknum->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLinknum->size
                [0], &pf_emlrtBCI, sp);
            }

            e_tmp_data[i1] = (int32_T)d;
          }

          for (i1 = 0; i1 < notdone; i1++) {
            NodesOnLinknum->data[e_tmp_data[i1] - 1] = (real_T)newpairs->size[0]
              + numA;
          }
        }

        if (*emlrtBreakCheckR2012bFlagVar != 0) {
          emlrtBreakCheckR2012b(sp);
        }
      }

      emxFree_real_T(&newpairs);
      i = setnPBC->size[0];
      setnPBC->size[0] = NodesOnPBCnum->size[0];
      emxEnsureCapacity_real_T(sp, setnPBC, i, &jg_emlrtRTEI);
      loop_ub = NodesOnPBCnum->size[0] - 1;
      for (i = 0; i <= loop_ub; i++) {
        setnPBC->data[i] = NodesOnPBCnum->data[i];
      }

      st.site = &ue_emlrtRSI;
      b_abs(&st, setnPBC, NodesOnPBCnum);
      i = PBCList->size[0] * PBCList->size[1];
      PBCList->size[0] = MPCList->size[0] + b_PBCList2->size[0];
      PBCList->size[1] = 5;
      emxEnsureCapacity_real_T(sp, PBCList, i, &lg_emlrtRTEI);
      loop_ub = MPCList->size[0];
      for (i = 0; i < 5; i++) {
        for (i1 = 0; i1 < loop_ub; i1++) {
          PBCList->data[i1 + PBCList->size[0] * i] = MPCList->data[i1 +
            MPCList->size[0] * i];
        }
      }

      loop_ub = b_PBCList2->size[0];
      for (i = 0; i < 5; i++) {
        for (i1 = 0; i1 < loop_ub; i1++) {
          PBCList->data[(i1 + MPCList->size[0]) + PBCList->size[0] * i] =
            b_PBCList2->data[i1 + b_PBCList2->size[0] * i];
        }
      }

      emxFree_real_T(&b_PBCList2);
    }

    emxFree_real_T(&pairs);

    /*  Get some memory back */
    /*      NodesOnPBC = sparse(NodesOnPBC); % nodes that are paired up */
    /*      NodesOnLink = sparse(NodesOnLink); % IDs of PBC referring to node */
  } else {
    NodesOnPBC->size[0] = 100;
    NodesOnPBC->size[1] = 0;

    /*  nodes that are paired up */
    NodesOnPBCnum->size[0] = 0;

    /*  number of nodes with common pairing */
    NodesOnLink->size[0] = 100;
    NodesOnLink->size[1] = 0;

    /*  IDs of PBC referring to node */
    NodesOnLinknum->size[0] = 0;

    /*  number of PBC referring to node */
  }

  /*  Mesh error checks */
  /*  Error checks for mesh arrays */
  /*  Nodes */
  if (*numnp < Coordinates->size[0]) {
    st.site = &pq_emlrtRSI;
    disp(&st, emlrt_marshallOut(&st, cv), &emlrtMCI);
    st.site = &rq_emlrtRSI;
    disp(&st, b_emlrt_marshallOut(&st, cv1), &b_emlrtMCI);
  } else {
    if (*numnp > Coordinates->size[0]) {
      st.site = &te_emlrtRSI;
      b_error(&st);
    }
  }

  /*  Elements */
  if (numel < NodesOnElement->size[0]) {
    st.site = &tq_emlrtRSI;
    disp(&st, c_emlrt_marshallOut(&st, cv2), &c_emlrtMCI);
    st.site = &qq_emlrtRSI;
    disp(&st, b_emlrt_marshallOut(&st, cv1), &d_emlrtMCI);
  } else {
    if (numel > NodesOnElement->size[0]) {
      st.site = &se_emlrtRSI;
      c_error(&st);
    }
  }

  if (NodesOnElement->size[1] != nen) {
    st.site = &uq_emlrtRSI;
    disp(&st, d_emlrt_marshallOut(&st, cv3), &e_emlrtMCI);
    st.site = &re_emlrtRSI;
    d_error(&st);
  }

  if (1.0 > numel) {
    i = 0;
  } else {
    i = NodesOnElement->size[0];
    if (1 > i) {
      emlrtDynamicBoundsCheckR2012b(1, 1, i, &wp_emlrtBCI, sp);
    }

    i = NodesOnElement->size[0];
    if (numel != (int32_T)muDoubleScalarFloor(numel)) {
      emlrtIntegerCheckR2012b(numel, &ne_emlrtDCI, sp);
    }

    if ((int32_T)numel > i) {
      emlrtDynamicBoundsCheckR2012b((int32_T)numel, 1, i, &vp_emlrtBCI, sp);
    }

    i = (int32_T)numel;
  }

  if (1.0 > nen) {
    loop_ub = 0;
  } else {
    i1 = NodesOnElement->size[1];
    if (1 > i1) {
      emlrtDynamicBoundsCheckR2012b(1, 1, i1, &up_emlrtBCI, sp);
    }

    i1 = NodesOnElement->size[1];
    loop_ub = (int32_T)nen;
    if (loop_ub > i1) {
      emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1, i1, &tp_emlrtBCI, sp);
    }
  }

  st.site = &qe_emlrtRSI;
  b_st.site = &sj_emlrtRSI;
  c_st.site = &tj_emlrtRSI;
  d_st.site = &uj_emlrtRSI;
  if (((i != 1) || (loop_ub != 1)) && (i == 1)) {
    emlrtErrorWithMessageIdR2018a(&d_st, &kc_emlrtRTEI,
      "Coder:toolbox:autoDimIncompatibility",
      "Coder:toolbox:autoDimIncompatibility", 0);
  }

  if (i < 1) {
    emlrtErrorWithMessageIdR2018a(&d_st, &lc_emlrtRTEI,
      "Coder:toolbox:eml_min_or_max_varDimZero",
      "Coder:toolbox:eml_min_or_max_varDimZero", 0);
  }

  e_st.site = &vj_emlrtRSI;
  f_st.site = &wj_emlrtRSI;
  g_st.site = &xj_emlrtRSI;
  i1 = loop_ub - 1;
  if (loop_ub >= 1) {
    h_st.site = &ak_emlrtRSI;
    for (j = 0; j <= i1; j++) {
      nodeE2_data[j] = NodesOnElement->data[NodesOnElement->size[0] * j];
      h_st.site = &yj_emlrtRSI;
      if ((2 <= i) && (i > 2147483646)) {
        i_st.site = &fg_emlrtRSI;
        check_forloop_overflow_error(&i_st);
      }

      for (b_i = 2; b_i <= i; b_i++) {
        numA = nodeE2_data[j];
        b_i1 = NodesOnElement->data[(b_i + NodesOnElement->size[0] * j) - 1];
        if ((!muDoubleScalarIsNaN(b_i1)) && (muDoubleScalarIsNaN(numA) || (numA <
              b_i1))) {
          nodeE2_data[j] = b_i1;
        }
      }
    }
  }

  nodeE2_size[0] = 1;
  nodeE2_size[1] = loop_ub;
  for (i = 0; i < loop_ub; i++) {
    b_nodeE2_data[i] = (nodeE2_data[i] > *numnp);
  }

  st.site = &qe_emlrtRSI;
  if (ifWhileCond(b_nodeE2_data, nodeE2_size)) {
    st.site = &pe_emlrtRSI;
    e_error(&st);
  }

  /*  Check that interfaces are assigned also when intraface couplers are requested */
  st.site = &oe_emlrtRSI;
  b_st.site = &uh_emlrtRSI;
  b_eml_find(&b_st, InterTypes, ii);

  /*  reset to logical */
  lenn = InterTypes->size[0] * InterTypes->size[1];
  loop_ub = ii->size[0];
  for (i = 0; i < loop_ub; i++) {
    i1 = ii->data[i];
    if ((i1 < 1LL) || ((int64_T)i1 > lenn)) {
      emlrtDynamicBoundsCheckInt64(i1, 1, lenn, &yf_emlrtBCI, sp);
    }

    ii->data[i] = i1;
  }

  loop_ub = ii->size[0] - 1;
  for (i = 0; i <= loop_ub; i++) {
    InterTypes->data[ii->data[i] - 1] = 1.0;
  }

  d = muDoubleScalarRound(nummat);
  if (d < 9.2233720368547758E+18) {
    if (d >= -9.2233720368547758E+18) {
      q0 = (int64_T)d;
    } else {
      q0 = MIN_int64_T;
    }
  } else if (d >= 9.2233720368547758E+18) {
    q0 = MAX_int64_T;
  } else {
    q0 = 0LL;
  }

  if (q0 < -9223372036854775807LL) {
    qY = MIN_int64_T;
  } else {
    qY = q0 - 1LL;
  }

  st.site = &ne_emlrtRSI;
  if ((1LL <= qY) && (qY > 9223372036854775806LL)) {
    b_st.site = &fg_emlrtRSI;
    b_check_forloop_overflow_error(&b_st);
  }

  mat = 1LL;
  emxInit_int64_T(sp, &inter, 1, &al_emlrtRTEI, true);
  emxInit_real_T(sp, &setAll, 2, &sj_emlrtRTEI, true);
  emxInit_real_T(sp, &setPBC, 2, &xj_emlrtRTEI, true);
  emxInit_int32_T(sp, &b_ii, 2, &il_emlrtRTEI, true);
  emxInit_real_T(sp, &t5_d, 1, &kl_emlrtRTEI, true);
  while (mat <= qY) {
    d = (int32_T)muDoubleScalarFloor((real_T)mat);
    if (mat != d) {
      emlrtIntegerCheckR2012b((real_T)mat, &rb_emlrtDCI, sp);
    }

    i4 = (int64_T)muDoubleScalarFloor((real_T)mat);
    if ((i4 < 1LL) || (i4 > InterTypes->size[1])) {
      emlrtDynamicBoundsCheckInt64((int64_T)muDoubleScalarFloor((real_T)mat), 1,
        InterTypes->size[1], &uf_emlrtBCI, sp);
    }

    if (mat != d) {
      emlrtIntegerCheckR2012b((real_T)mat, &rb_emlrtDCI, sp);
    }

    if ((i4 < 1LL) || (i4 > InterTypes->size[0])) {
      emlrtDynamicBoundsCheckInt64((int64_T)muDoubleScalarFloor((real_T)mat), 1,
        InterTypes->size[0], &uf_emlrtBCI, sp);
    }

    lenn = (int32_T)(int64_T)muDoubleScalarFloor((real_T)mat);
    numA = InterTypes->data[(lenn + InterTypes->size[0] * (lenn - 1)) - 1];
    st.site = &me_emlrtRSI;
    if (muDoubleScalarIsNaN(numA)) {
      emlrtErrorWithMessageIdR2018a(&st, &mc_emlrtRTEI, "MATLAB:nologicalnan",
        "MATLAB:nologicalnan", 0);
    }

    if (numA != 0.0) {
      if (mat > 9223372036854775806LL) {
        b_qY = MAX_int64_T;
      } else {
        b_qY = mat + 1LL;
      }

      if (b_qY > q0) {
        i = 0;
        i1 = 0;
      } else {
        if ((real_T)b_qY != (int32_T)muDoubleScalarFloor((real_T)b_qY)) {
          emlrtIntegerCheckR2012b((real_T)b_qY, &me_emlrtDCI, sp);
        }

        b_qY = (int64_T)muDoubleScalarFloor((real_T)b_qY);
        if ((b_qY < 1LL) || (b_qY > InterTypes->size[0])) {
          emlrtDynamicBoundsCheckInt64(b_qY, 1, InterTypes->size[0],
            &sp_emlrtBCI, sp);
        }

        i = (int32_T)b_qY - 1;
        if ((real_T)q0 != (int32_T)muDoubleScalarFloor((real_T)q0)) {
          emlrtIntegerCheckR2012b((real_T)q0, &le_emlrtDCI, sp);
        }

        b_qY = (int64_T)muDoubleScalarFloor((real_T)q0);
        if ((b_qY < 1LL) || (b_qY > InterTypes->size[0])) {
          emlrtDynamicBoundsCheckInt64(b_qY, 1, InterTypes->size[0],
            &rp_emlrtBCI, sp);
        }

        i1 = (int32_T)b_qY;
      }

      st.site = &le_emlrtRSI;
      if (mat != d) {
        emlrtIntegerCheckR2012b((real_T)mat, &ke_emlrtDCI, &st);
      }

      if ((i4 < 1LL) || (i4 > InterTypes->size[1])) {
        emlrtDynamicBoundsCheckInt64(i4, 1, InterTypes->size[1], &qp_emlrtBCI,
          &st);
      }

      loop_ub = i1 - i;
      i1 = t5_d->size[0];
      t5_d->size[0] = loop_ub;
      emxEnsureCapacity_real_T(&st, t5_d, i1, &pg_emlrtRTEI);
      for (i1 = 0; i1 < loop_ub; i1++) {
        t5_d->data[i1] = InterTypes->data[(i + i1) + InterTypes->size[0] *
          ((int32_T)mat - 1)] - 1.0;
      }

      b_st.site = &uh_emlrtRSI;
      c_eml_find(&b_st, t5_d, ii);
      i = inter->size[0];
      inter->size[0] = ii->size[0];
      emxEnsureCapacity_int64_T(sp, inter, i, &sg_emlrtRTEI);
      loop_ub = ii->size[0];
      for (i = 0; i < loop_ub; i++) {
        inter->data[i] = ii->data[i];
      }

      if (inter->size[0] != 0) {
        st.site = &wq_emlrtRSI;
        disp(&st, e_emlrt_marshallOut(&st, cv7), &f_emlrtMCI);
        st.site = &ke_emlrtRSI;
        b_st.site = &ek_emlrtRSI;
        c_st.site = &yq_emlrtRSI;
        j_st.site = &sq_emlrtRSI;
        numA = emlrt_marshallIn(&c_st, c_feval(&c_st, h_emlrt_marshallOut(&c_st,
          cv4), f_emlrt_marshallOut(1.0), c_emlrt_marshallOut(&j_st, cv8),
          i_emlrt_marshallOut(mat), &l_emlrtMCI), "<output of feval>");
        emlrtDisplayR2012b(f_emlrt_marshallOut(numA), "ans", &emlrtRTEI, sp);
        i = idx->size[0];
        idx->size[0] = inter->size[0];
        emxEnsureCapacity_int32_T(sp, idx, i, &vg_emlrtRTEI);
        loop_ub = inter->size[0];
        for (i = 0; i < loop_ub; i++) {
          b_qY = inter->data[i];
          if ((b_qY < 0LL) && (mat < MIN_int64_T - b_qY)) {
            b_qY = MIN_int64_T;
          } else if ((b_qY > 0LL) && (mat > MAX_int64_T - b_qY)) {
            b_qY = MAX_int64_T;
          } else {
            b_qY += mat;
          }

          if ((real_T)b_qY != (int32_T)muDoubleScalarFloor((real_T)b_qY)) {
            emlrtIntegerCheckR2012b((real_T)b_qY, &kb_emlrtDCI, sp);
          }

          b_qY = (int64_T)muDoubleScalarFloor((real_T)b_qY);
          if ((b_qY < 1LL) || (b_qY > InterTypes->size[0])) {
            emlrtDynamicBoundsCheckInt64(b_qY, 1, InterTypes->size[0],
              &of_emlrtBCI, sp);
          }

          idx->data[i] = (int32_T)b_qY;
        }

        if (mat != d) {
          emlrtIntegerCheckR2012b((real_T)mat, &je_emlrtDCI, sp);
        }

        if ((i4 < 1LL) || (i4 > InterTypes->size[1])) {
          emlrtDynamicBoundsCheckInt64(i4, 1, InterTypes->size[1], &pp_emlrtBCI,
            sp);
        }

        loop_ub = idx->size[0];
        for (i = 0; i < loop_ub; i++) {
          InterTypes->data[(idx->data[i] + InterTypes->size[0] * ((int32_T)mat -
            1)) - 1] = 1.0;
        }
      }

      if (mat < -9223372036854775807LL) {
        b_qY = MIN_int64_T;
      } else {
        b_qY = mat - 1LL;
      }

      if (1LL > b_qY) {
        loop_ub = 0;
      } else {
        if (1 > InterTypes->size[1]) {
          emlrtDynamicBoundsCheckInt64(1LL, 1, InterTypes->size[1], &np_emlrtBCI,
            sp);
        }

        if ((real_T)b_qY != (int32_T)muDoubleScalarFloor((real_T)b_qY)) {
          emlrtIntegerCheckR2012b((real_T)b_qY, &he_emlrtDCI, sp);
        }

        b_qY = (int64_T)muDoubleScalarFloor((real_T)b_qY);
        if ((b_qY < 1LL) || (b_qY > InterTypes->size[1])) {
          emlrtDynamicBoundsCheckInt64(b_qY, 1, InterTypes->size[1],
            &mp_emlrtBCI, sp);
        }

        loop_ub = (int32_T)b_qY;
      }

      st.site = &je_emlrtRSI;
      if (mat != d) {
        emlrtIntegerCheckR2012b((real_T)mat, &ie_emlrtDCI, &st);
      }

      if ((i4 < 1LL) || (i4 > InterTypes->size[0])) {
        emlrtDynamicBoundsCheckInt64(i4, 1, InterTypes->size[0], &op_emlrtBCI,
          &st);
      }

      i = setPBC->size[0] * setPBC->size[1];
      setPBC->size[0] = 1;
      setPBC->size[1] = loop_ub;
      emxEnsureCapacity_real_T(&st, setPBC, i, &xg_emlrtRTEI);
      for (i = 0; i < loop_ub; i++) {
        setPBC->data[i] = InterTypes->data[((int32_T)mat + InterTypes->size[0] *
          i) - 1] - 1.0;
      }

      b_st.site = &uh_emlrtRSI;
      d_eml_find(&b_st, setPBC, b_ii);
      i = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      setAll->size[1] = b_ii->size[1];
      emxEnsureCapacity_real_T(&st, setAll, i, &ah_emlrtRTEI);
      loop_ub = b_ii->size[0] * b_ii->size[1];
      for (i = 0; i < loop_ub; i++) {
        setAll->data[i] = b_ii->data[i];
      }

      if (setAll->size[1] != 0) {
        st.site = &vq_emlrtRSI;
        disp(&st, e_emlrt_marshallOut(&st, cv7), &h_emlrtMCI);
        st.site = &ie_emlrtRSI;
        b_st.site = &ek_emlrtRSI;
        c_st.site = &yq_emlrtRSI;
        j_st.site = &sq_emlrtRSI;
        numA = emlrt_marshallIn(&c_st, c_feval(&c_st, h_emlrt_marshallOut(&c_st,
          cv4), f_emlrt_marshallOut(1.0), c_emlrt_marshallOut(&j_st, cv8),
          i_emlrt_marshallOut(mat), &l_emlrtMCI), "<output of feval>");
        emlrtDisplayR2012b(f_emlrt_marshallOut(numA), "ans", &b_emlrtRTEI, sp);
        i = idx->size[0];
        idx->size[0] = setAll->size[1];
        emxEnsureCapacity_int32_T(sp, idx, i, &dh_emlrtRTEI);
        loop_ub = setAll->size[1];
        for (i = 0; i < loop_ub; i++) {
          i1 = (int32_T)setAll->data[i];
          if ((i1 < 1) || (i1 > InterTypes->size[1])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)setAll->data[i], 1,
              InterTypes->size[1], &af_emlrtBCI, sp);
          }

          idx->data[i] = i1;
        }

        if (mat != d) {
          emlrtIntegerCheckR2012b((real_T)mat, &ge_emlrtDCI, sp);
        }

        if ((i4 < 1LL) || (i4 > InterTypes->size[0])) {
          emlrtDynamicBoundsCheckInt64(i4, 1, InterTypes->size[0], &lp_emlrtBCI,
            sp);
        }

        loop_ub = idx->size[0];
        for (i = 0; i < loop_ub; i++) {
          InterTypes->data[((int32_T)mat + InterTypes->size[0] * (idx->data[i] -
            1)) - 1] = 1.0;
        }
      }
    }

    if (mat < -9223372036854775807LL) {
      b_qY = MIN_int64_T;
    } else {
      b_qY = mat - 1LL;
    }

    if (1LL > b_qY) {
      loop_ub = 0;
    } else {
      if (1 > InterTypes->size[0]) {
        emlrtDynamicBoundsCheckInt64(1LL, 1, InterTypes->size[0], &kp_emlrtBCI,
          sp);
      }

      if ((real_T)b_qY != (int32_T)muDoubleScalarFloor((real_T)b_qY)) {
        emlrtIntegerCheckR2012b((real_T)b_qY, &fe_emlrtDCI, sp);
      }

      b_qY = (int64_T)muDoubleScalarFloor((real_T)b_qY);
      if ((b_qY < 1LL) || (b_qY > InterTypes->size[0])) {
        emlrtDynamicBoundsCheckInt64(b_qY, 1, InterTypes->size[0], &jp_emlrtBCI,
          sp);
      }

      loop_ub = (int32_T)b_qY;
    }

    st.site = &he_emlrtRSI;
    if (mat != d) {
      emlrtIntegerCheckR2012b((real_T)mat, &ee_emlrtDCI, &st);
    }

    if ((i4 < 1LL) || (i4 > InterTypes->size[1])) {
      emlrtDynamicBoundsCheckInt64(i4, 1, InterTypes->size[1], &ip_emlrtBCI, &st);
    }

    i = t5_d->size[0];
    t5_d->size[0] = loop_ub;
    emxEnsureCapacity_real_T(&st, t5_d, i, &rg_emlrtRTEI);
    for (i = 0; i < loop_ub; i++) {
      t5_d->data[i] = InterTypes->data[i + InterTypes->size[0] * ((int32_T)mat -
        1)];
    }

    b_st.site = &uh_emlrtRSI;
    c_eml_find(&b_st, t5_d, idx);
    if (idx->size[0] != 0) {
      st.site = &ge_emlrtRSI;
      b_st.site = &ek_emlrtRSI;
      c_st.site = &yq_emlrtRSI;
      j_st.site = &sq_emlrtRSI;
      numA = emlrt_marshallIn(&c_st, c_feval(&c_st, h_emlrt_marshallOut(&c_st,
        cv4), f_emlrt_marshallOut(1.0), j_emlrt_marshallOut(&j_st, cv5),
        i_emlrt_marshallOut(mat), &l_emlrtMCI), "<output of feval>");
      emlrtDisplayR2012b(f_emlrt_marshallOut(numA), "ans", &c_emlrtRTEI, sp);
      st.site = &xq_emlrtRSI;
      disp(&st, g_emlrt_marshallOut(&st, cv6), &g_emlrtMCI);
    }

    mat++;
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  emxFree_int32_T(&b_ii);
  emxFree_int64_T(&inter);

  /*  Form ElementsOnNode, ElementsOnNodeNum, DG nodes and connectivity */
  st.site = &fe_emlrtRSI;
  if (muDoubleScalarIsNaN(usePBC)) {
    emlrtErrorWithMessageIdR2018a(&st, &mc_emlrtRTEI, "MATLAB:nologicalnan",
      "MATLAB:nologicalnan", 0);
  }

  emxInit_real_T(sp, &ElementsOnNodePBCRow, 2, &og_emlrtRTEI, true);
  emxInit_real_T(sp, &ElementsOnNodeRow, 2, &ug_emlrtRTEI, true);
  emxInitStruct_sparse(sp, &NodeRegSum, &dl_emlrtRTEI, true);
  emxInit_int32_T(sp, &r1, 1, &fl_emlrtRTEI, true);
  emxInit_int32_T(sp, &jj, 1, &jl_emlrtRTEI, true);
  emxInitStruct_sparse1(sp, &expl_temp, &ml_emlrtRTEI, true);
  emxInitStruct_sparse(sp, &b_expl_temp, &pl_emlrtRTEI, true);
  emxInit_real_T(sp, &NodeRegInd, 2, &ih_emlrtRTEI, true);
  if (usePBC != 0.0) {
    emxInit_real_T(sp, &c_NodeRegInd, 2, &ig_emlrtRTEI, true);

    /*  also form the zipped mesh at the same time */
    i = c_NodeRegInd->size[0] * c_NodeRegInd->size[1];
    c_NodeRegInd->size[0] = 3;
    emxEnsureCapacity_real_T(sp, c_NodeRegInd, i, &ig_emlrtRTEI);
    d = numel * 27.0;
    if (!(d >= 0.0)) {
      emlrtNonNegativeCheckR2012b(d, &fd_emlrtDCI, sp);
    }

    d1 = (int32_T)muDoubleScalarFloor(d);
    if (d != d1) {
      emlrtIntegerCheckR2012b(d, &gd_emlrtDCI, sp);
    }

    i = c_NodeRegInd->size[0] * c_NodeRegInd->size[1];
    c_NodeRegInd->size[1] = (int32_T)d;
    emxEnsureCapacity_real_T(sp, c_NodeRegInd, i, &ig_emlrtRTEI);
    if (d != d1) {
      emlrtIntegerCheckR2012b(d, &ob_emlrtDCI, sp);
    }

    loop_ub = 3 * (int32_T)d;
    for (i = 0; i < loop_ub; i++) {
      c_NodeRegInd->data[i] = 0.0;
    }

    if (!(numel >= 0.0)) {
      emlrtNonNegativeCheckR2012b(numel, &dd_emlrtDCI, sp);
    }

    i = (int32_T)muDoubleScalarFloor(numel);
    if (numel != i) {
      emlrtIntegerCheckR2012b(numel, &ed_emlrtDCI, sp);
    }

    i1 = ElementsOnNodePBCRow->size[0] * ElementsOnNodePBCRow->size[1];
    ElementsOnNodePBCRow->size[0] = (int32_T)numel;
    loop_ub = (int32_T)nen;
    ElementsOnNodePBCRow->size[1] = loop_ub;
    emxEnsureCapacity_real_T(sp, ElementsOnNodePBCRow, i1, &og_emlrtRTEI);
    if ((int32_T)numel != i) {
      emlrtIntegerCheckR2012b(numel, &lb_emlrtDCI, sp);
    }

    numPBC = (int32_T)numel * (int32_T)nen;
    for (i = 0; i < numPBC; i++) {
      ElementsOnNodePBCRow->data[i] = 0.0;
    }

    i = ElementsOnNodeRow->size[0] * ElementsOnNodeRow->size[1];
    ElementsOnNodeRow->size[0] = (int32_T)numel;
    ElementsOnNodeRow->size[1] = loop_ub;
    emxEnsureCapacity_real_T(sp, ElementsOnNodeRow, i, &ug_emlrtRTEI);
    for (i = 0; i < numPBC; i++) {
      ElementsOnNodeRow->data[i] = 0.0;
    }

    nri = 0U;
    b_i2 = 0.0;
    i = (int32_T)numel;
    if (0.0 <= numel - 1.0) {
      if (1.0 > nen) {
        locFA = 0;
      } else {
        if (1 > NodesOnElementPBC->size[1]) {
          emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementPBC->size[1],
            &gp_emlrtBCI, sp);
        }

        if (loop_ub > NodesOnElementPBC->size[1]) {
          emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1, NodesOnElementPBC->
            size[1], &fp_emlrtBCI, sp);
        }

        locFA = loop_ub;
      }
    }

    if (0 <= i - 1) {
      b_nodeE2_size[0] = 1;
      b_nodeE2_size[1] = locFA;
    }

    for (elem = 0; elem < i; elem++) {
      i1 = elem + 1;
      if (i1 > NodesOnElementPBC->size[0]) {
        emlrtDynamicBoundsCheckR2012b(i1, 1, NodesOnElementPBC->size[0],
          &hp_emlrtBCI, sp);
      }

      for (i1 = 0; i1 < locFA; i1++) {
        nodeE2_data[i1] = NodesOnElementPBC->data[elem + NodesOnElementPBC->
          size[0] * i1];
      }

      nel = intnnz(nodeE2_data, b_nodeE2_size);
      i1 = RegionOnElement->size[0];
      if (elem + 1 > i1) {
        emlrtDynamicBoundsCheckR2012b(elem + 1, 1, i1, &mf_emlrtBCI, sp);
      }

      b_i1 = RegionOnElement->data[elem];
      for (partialTrueCount = 0; partialTrueCount < nel; partialTrueCount++) {
        /*  Loop over local Nodes */
        if (((int32_T)(partialTrueCount + 1U) < 1) || ((int32_T)
             (partialTrueCount + 1U) > NodesOnElementPBC->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)(partialTrueCount + 1U), 1,
            NodesOnElementPBC->size[1], &jf_emlrtBCI, sp);
        }

        if (elem + 1 > NodesOnElementPBC->size[0]) {
          emlrtDynamicBoundsCheckR2012b(elem + 1, 1, NodesOnElementPBC->size[0],
            &jf_emlrtBCI, sp);
        }

        node = NodesOnElementPBC->data[elem + NodesOnElementPBC->size[0] *
          partialTrueCount];

        /*    Add element to star list, increment number of elem in star */
        if (((int32_T)(partialTrueCount + 1U) < 1) || ((int32_T)
             (partialTrueCount + 1U) > ElementsOnNodePBCRow->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)(partialTrueCount + 1U), 1,
            ElementsOnNodePBCRow->size[1], &ff_emlrtBCI, sp);
        }

        if (elem + 1 > ElementsOnNodePBCRow->size[0]) {
          emlrtDynamicBoundsCheckR2012b(elem + 1, 1, ElementsOnNodePBCRow->size
            [0], &ff_emlrtBCI, sp);
        }

        if (node != (int32_T)muDoubleScalarFloor(node)) {
          emlrtIntegerCheckR2012b(node, &ib_emlrtDCI, sp);
        }

        if (((int32_T)node < 1) || ((int32_T)node > ElementsOnNodePBCNum->size[0]))
        {
          emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
            ElementsOnNodePBCNum->size[0], &gf_emlrtBCI, sp);
        }

        numA = ElementsOnNodePBCNum->data[(int32_T)node - 1] + 1.0;
        ElementsOnNodePBCRow->data[elem + ElementsOnNodePBCRow->size[0] *
          partialTrueCount] = numA;
        if (((int32_T)node < 1) || ((int32_T)node > ElementsOnNodePBCNum->size[0]))
        {
          emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
            ElementsOnNodePBCNum->size[0], &cf_emlrtBCI, sp);
        }

        ElementsOnNodePBCNum->data[(int32_T)node - 1] = numA;

        /*              ElementsOnNodePBC(locE,node) = elem; */
        /*              ElementsOnNodePBCDup(locE,node) = node; */
        if (((int32_T)node < 1) || ((int32_T)node > NodesOnPBCnum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)node, 1, NodesOnPBCnum->size[0],
            &we_emlrtBCI, sp);
        }

        if (NodesOnPBCnum->data[(int32_T)node - 1] > 0.0) {
          if (((int32_T)node < 1) || ((int32_T)node > NodesOnPBC->size[1])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)node, 1, NodesOnPBC->size[1],
              &ve_emlrtBCI, sp);
          }

          numA = NodesOnPBC->data[100 * ((int32_T)node - 1)];
          if ((!(node > numA)) && ((!muDoubleScalarIsNaN(node)) ||
               muDoubleScalarIsNaN(numA))) {
            numA = node;
          }

          /*  use smallest node number as the zipped node */
          nri++;
          if (((int32_T)nri < 1) || ((int32_T)nri > c_NodeRegInd->size[1])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, c_NodeRegInd->size[1],
              &ep_emlrtBCI, sp);
          }

          i1 = 3 * ((int32_T)nri - 1);
          c_NodeRegInd->data[i1] = numA;
          c_NodeRegInd->data[i1 + 1] = b_i1;
          c_NodeRegInd->data[i1 + 2] = 1.0;

          /*  flag that node is in current material set */
          if (node != numA) {
            nri++;

            /*  also add for original edge */
            if (((int32_T)nri < 1) || ((int32_T)nri > c_NodeRegInd->size[1])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, c_NodeRegInd->size
                [1], &dp_emlrtBCI, sp);
            }

            i1 = 3 * ((int32_T)nri - 1);
            c_NodeRegInd->data[i1] = node;
            c_NodeRegInd->data[i1 + 1] = b_i1;
            c_NodeRegInd->data[i1 + 2] = -1.0;

            /*  flag that node is in current material set */
          }

          if (((int32_T)(partialTrueCount + 1U) < 1) || ((int32_T)
               (partialTrueCount + 1U) > NodesOnElementCG->size[1])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)(partialTrueCount + 1U), 1,
              NodesOnElementCG->size[1], &oe_emlrtBCI, sp);
          }

          if (elem + 1 > NodesOnElementCG->size[0]) {
            emlrtDynamicBoundsCheckR2012b(elem + 1, 1, NodesOnElementCG->size[0],
              &oe_emlrtBCI, sp);
          }

          NodesOnElementCG->data[elem + NodesOnElementCG->size[0] *
            partialTrueCount] = numA;

          /*    Add element to star list, increment number of elem in star */
          if (((int32_T)(partialTrueCount + 1U) < 1) || ((int32_T)
               (partialTrueCount + 1U) > ElementsOnNodeRow->size[1])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)(partialTrueCount + 1U), 1,
              ElementsOnNodeRow->size[1], &me_emlrtBCI, sp);
          }

          if (elem + 1 > ElementsOnNodeRow->size[0]) {
            emlrtDynamicBoundsCheckR2012b(elem + 1, 1, ElementsOnNodeRow->size[0],
              &me_emlrtBCI, sp);
          }

          if (numA != (int32_T)muDoubleScalarFloor(numA)) {
            emlrtIntegerCheckR2012b(numA, &gb_emlrtDCI, sp);
          }

          if (((int32_T)numA < 1) || ((int32_T)numA > ElementsOnNodeNum->size[0]))
          {
            emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1,
              ElementsOnNodeNum->size[0], &ne_emlrtBCI, sp);
          }

          ElementsOnNodeRow->data[elem + ElementsOnNodeRow->size[0] *
            partialTrueCount] = ElementsOnNodeNum->data[(int32_T)numA - 1] + 1.0;
          if (((int32_T)numA < 1) || ((int32_T)numA > ElementsOnNodeNum->size[0]))
          {
            emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1,
              ElementsOnNodeNum->size[0], &le_emlrtBCI, sp);
          }

          ElementsOnNodeNum->data[(int32_T)numA - 1]++;

          /*                  ElementsOnNode(locE,nodePBC) = elem; */
          /*                  ElementsOnNodeDup(locE,nodePBC) = nodePBC; */
        } else {
          nri++;
          if (((int32_T)nri < 1) || ((int32_T)nri > c_NodeRegInd->size[1])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, c_NodeRegInd->size[1],
              &cp_emlrtBCI, sp);
          }

          i1 = 3 * ((int32_T)nri - 1);
          c_NodeRegInd->data[i1] = node;
          c_NodeRegInd->data[i1 + 1] = b_i1;
          c_NodeRegInd->data[i1 + 2] = 1.0;

          /*  flag that node is in current material set */
          /*                  NodeReg(node,reg) = node; % flag that node is in current material set */
          /*    Add element to star list, increment number of elem in star */
          if (((int32_T)(partialTrueCount + 1U) < 1) || ((int32_T)
               (partialTrueCount + 1U) > ElementsOnNodeRow->size[1])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)(partialTrueCount + 1U), 1,
              ElementsOnNodeRow->size[1], &se_emlrtBCI, sp);
          }

          if (elem + 1 > ElementsOnNodeRow->size[0]) {
            emlrtDynamicBoundsCheckR2012b(elem + 1, 1, ElementsOnNodeRow->size[0],
              &se_emlrtBCI, sp);
          }

          if (((int32_T)node < 1) || ((int32_T)node > ElementsOnNodeNum->size[0]))
          {
            emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
              ElementsOnNodeNum->size[0], &te_emlrtBCI, sp);
          }

          ElementsOnNodeRow->data[elem + ElementsOnNodeRow->size[0] *
            partialTrueCount] = ElementsOnNodeNum->data[(int32_T)node - 1] + 1.0;
          if (((int32_T)node < 1) || ((int32_T)node > ElementsOnNodeNum->size[0]))
          {
            emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
              ElementsOnNodeNum->size[0], &re_emlrtBCI, sp);
          }

          ElementsOnNodeNum->data[(int32_T)node - 1]++;

          /*                  ElementsOnNode(locE,nodePBC) = elem; */
          /*                  ElementsOnNodeDup(locE,nodePBC) = nodePBC; */
        }

        if (*emlrtBreakCheckR2012bFlagVar != 0) {
          emlrtBreakCheckR2012b(sp);
        }
      }

      i1 = (int32_T)nen - nel;
      emlrtForLoopVectorCheckR2012b((real_T)nel + 1.0, 1.0, nen, mxDOUBLE_CLASS,
        i1, &ic_emlrtRTEI, sp);
      for (partialTrueCount = 0; partialTrueCount < i1; partialTrueCount++) {
        numA = ((real_T)nel + 1.0) + (real_T)partialTrueCount;
        b_i2++;
        if (((int32_T)numA < 1) || ((int32_T)numA > ElementsOnNodeRow->size[1]))
        {
          emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1,
            ElementsOnNodeRow->size[1], &bf_emlrtBCI, sp);
        }

        if (elem + 1 > ElementsOnNodeRow->size[0]) {
          emlrtDynamicBoundsCheckR2012b(elem + 1, 1, ElementsOnNodeRow->size[0],
            &bf_emlrtBCI, sp);
        }

        ElementsOnNodeRow->data[elem + ElementsOnNodeRow->size[0] * ((int32_T)
          numA - 1)] = b_i2;
        if (((int32_T)numA < 1) || ((int32_T)numA > ElementsOnNodePBCRow->size[1]))
        {
          emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1,
            ElementsOnNodePBCRow->size[1], &xe_emlrtBCI, sp);
        }

        if (elem + 1 > ElementsOnNodePBCRow->size[0]) {
          emlrtDynamicBoundsCheckR2012b(elem + 1, 1, ElementsOnNodePBCRow->size
            [0], &xe_emlrtBCI, sp);
        }

        ElementsOnNodePBCRow->data[elem + ElementsOnNodePBCRow->size[0] *
          ((int32_T)numA - 1)] = b_i2;
        if (*emlrtBreakCheckR2012bFlagVar != 0) {
          emlrtBreakCheckR2012b(sp);
        }
      }

      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    end = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1] - 1;
    trueCount = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElementPBC->data[b_i] == 0.0) {
        trueCount++;
      }
    }

    i = jj->size[0];
    jj->size[0] = trueCount;
    emxEnsureCapacity_int32_T(sp, jj, i, &mg_emlrtRTEI);
    partialTrueCount = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElementPBC->data[b_i] == 0.0) {
        jj->data[partialTrueCount] = b_i + 1;
        partialTrueCount++;
      }
    }

    b_loop_ub = jj->size[0] - 1;
    elemB = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1];
    for (i = 0; i <= b_loop_ub; i++) {
      if ((jj->data[i] < 1) || (jj->data[i] > elemB)) {
        emlrtDynamicBoundsCheckR2012b(jj->data[i], 1, elemB, &df_emlrtBCI, sp);
      }

      NodesOnElementPBC->data[jj->data[i] - 1] = *numnp + 1.0;
    }

    /* move empty numbers to end of array */
    end = NodesOnElementCG->size[0] * NodesOnElementCG->size[1] - 1;
    trueCount = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElementCG->data[b_i] == 0.0) {
        trueCount++;
      }
    }

    i = r1->size[0];
    r1->size[0] = trueCount;
    emxEnsureCapacity_int32_T(sp, r1, i, &mg_emlrtRTEI);
    partialTrueCount = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElementCG->data[b_i] == 0.0) {
        r1->data[partialTrueCount] = b_i + 1;
        partialTrueCount++;
      }
    }

    b_loop_ub = r1->size[0] - 1;
    elemB = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
    for (i = 0; i <= b_loop_ub; i++) {
      if ((r1->data[i] < 1) || (r1->data[i] > elemB)) {
        emlrtDynamicBoundsCheckR2012b(r1->data[i], 1, elemB, &pe_emlrtBCI, sp);
      }

      NodesOnElementCG->data[r1->data[i] - 1] = *numnp + 1.0;
    }

    /* move empty numbers to end of array */
    if (numel < 1.0) {
      setAll->size[0] = 1;
      setAll->size[1] = 0;
    } else {
      i = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      setAll->size[1] = (int32_T)numel;
      emxEnsureCapacity_real_T(sp, setAll, i, &fh_emlrtRTEI);
      b_loop_ub = (int32_T)numel - 1;
      for (i = 0; i <= b_loop_ub; i++) {
        setAll->data[i] = (real_T)i + 1.0;
      }
    }

    i = MorePBC->size[0];
    MorePBC->size[0] = setAll->size[1];
    emxEnsureCapacity_real_T(sp, MorePBC, i, &gh_emlrtRTEI);
    b_loop_ub = setAll->size[1];
    for (i = 0; i < b_loop_ub; i++) {
      MorePBC->data[i] = setAll->data[i];
    }

    emxInit_real_T(sp, &b_MorePBC, 2, &bh_emlrtRTEI, true);
    i = b_MorePBC->size[0] * b_MorePBC->size[1];
    b_MorePBC->size[0] = MorePBC->size[0];
    b_MorePBC->size[1] = loop_ub;
    emxEnsureCapacity_real_T(sp, b_MorePBC, i, &bh_emlrtRTEI);
    for (i = 0; i < loop_ub; i++) {
      b_loop_ub = MorePBC->size[0];
      for (i1 = 0; i1 < b_loop_ub; i1++) {
        b_MorePBC->data[i1 + b_MorePBC->size[0] * i] = MorePBC->data[i1];
      }
    }

    st.site = &ee_emlrtRSI;
    sparse(&st, ElementsOnNodeRow, NodesOnElementCG, b_MorePBC, ElementsOnNode);

    /* zeros(maxel,numnp); % Array of elements connected to nodes */
    st.site = &de_emlrtRSI;
    sparse(&st, ElementsOnNodeRow, NodesOnElementCG, NodesOnElementCG,
           ElementsOnNodeDup);

    /*  actual node ID given to element for an original node; general counterpart of NodeMat */
    i = b_MorePBC->size[0] * b_MorePBC->size[1];
    b_MorePBC->size[0] = MorePBC->size[0];
    b_MorePBC->size[1] = loop_ub;
    emxEnsureCapacity_real_T(sp, b_MorePBC, i, &bh_emlrtRTEI);
    for (i = 0; i < loop_ub; i++) {
      b_loop_ub = MorePBC->size[0];
      for (i1 = 0; i1 < b_loop_ub; i1++) {
        b_MorePBC->data[i1 + b_MorePBC->size[0] * i] = MorePBC->data[i1];
      }
    }

    emxInitStruct_sparse1(sp, &dr_emlrtRSI, &mg_emlrtRTEI, true);
    st.site = &ce_emlrtRSI;
    sparse(&st, ElementsOnNodePBCRow, NodesOnElementPBC, b_MorePBC, &expl_temp);
    ElementsOnNodePBC_m = expl_temp.m;
    ElementsOnNodePBC_n = expl_temp.n;

    /* zeros(maxel,numnp); % Array of elements connected to nodes */
    st.site = &be_emlrtRSI;
    sparse(&st, ElementsOnNodePBCRow, NodesOnElementPBC, NodesOnElementPBC,
           &dr_emlrtRSI);

    /*  actual node ID given to element for an original node; general counterpart of NodeMat */
    emxFreeStruct_sparse1(&dr_emlrtRSI);
    emxFree_real_T(&b_MorePBC);
    if (1 > (int32_T)nri) {
      loop_ub = 0;
      b_loop_ub = 0;
      notdone = 0;
    } else {
      if (1 > c_NodeRegInd->size[1]) {
        emlrtDynamicBoundsCheckR2012b(1, 1, c_NodeRegInd->size[1], &bp_emlrtBCI,
          sp);
      }

      if (((int32_T)nri < 1) || ((int32_T)nri > c_NodeRegInd->size[1])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, c_NodeRegInd->size[1],
          &ap_emlrtBCI, sp);
      }

      loop_ub = (int32_T)nri;
      if (1 > c_NodeRegInd->size[1]) {
        emlrtDynamicBoundsCheckR2012b(1, 1, c_NodeRegInd->size[1], &yo_emlrtBCI,
          sp);
      }

      if (((int32_T)nri < 1) || ((int32_T)nri > c_NodeRegInd->size[1])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, c_NodeRegInd->size[1],
          &xo_emlrtBCI, sp);
      }

      b_loop_ub = (int32_T)nri;
      if (1 > c_NodeRegInd->size[1]) {
        emlrtDynamicBoundsCheckR2012b(1, 1, c_NodeRegInd->size[1], &wo_emlrtBCI,
          sp);
      }

      if (((int32_T)nri < 1) || ((int32_T)nri > c_NodeRegInd->size[1])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, c_NodeRegInd->size[1],
          &vo_emlrtBCI, sp);
      }

      notdone = (int32_T)nri;
    }

    i = setPBC->size[0] * setPBC->size[1];
    setPBC->size[0] = 1;
    setPBC->size[1] = loop_ub;
    emxEnsureCapacity_real_T(sp, setPBC, i, &mh_emlrtRTEI);
    for (i = 0; i < loop_ub; i++) {
      setPBC->data[i] = c_NodeRegInd->data[3 * i];
    }

    i = setAll->size[0] * setAll->size[1];
    setAll->size[0] = 1;
    setAll->size[1] = b_loop_ub;
    emxEnsureCapacity_real_T(sp, setAll, i, &nh_emlrtRTEI);
    for (i = 0; i < b_loop_ub; i++) {
      setAll->data[i] = c_NodeRegInd->data[3 * i + 1];
    }

    i = NodeRegInd->size[0] * NodeRegInd->size[1];
    NodeRegInd->size[0] = 1;
    NodeRegInd->size[1] = notdone;
    emxEnsureCapacity_real_T(sp, NodeRegInd, i, &oh_emlrtRTEI);
    for (i = 0; i < notdone; i++) {
      NodeRegInd->data[i] = c_NodeRegInd->data[3 * i + 2];
    }

    emxFree_real_T(&c_NodeRegInd);
    st.site = &ae_emlrtRSI;
    b_sparse(&st, setPBC, setAll, NodeRegInd, NodeReg);

    /*  allow allocation of PBC nodes too, but with negatives so as not to be in facet counts */
    st.site = &yd_emlrtRSI;
    b_st.site = &il_emlrtRSI;
    e_eml_find(&b_st, NodeReg->d, NodeReg->colidx, NodeReg->rowidx, NodeReg->m,
               NodeReg->n, ii, jj);
    i = setnPBC->size[0];
    setnPBC->size[0] = ii->size[0];
    emxEnsureCapacity_real_T(&st, setnPBC, i, &jh_emlrtRTEI);
    loop_ub = ii->size[0];
    for (i = 0; i < loop_ub; i++) {
      setnPBC->data[i] = ii->data[i];
    }

    i = MorePBC->size[0];
    MorePBC->size[0] = jj->size[0];
    emxEnsureCapacity_real_T(&st, MorePBC, i, &kh_emlrtRTEI);
    loop_ub = jj->size[0];
    for (i = 0; i < loop_ub; i++) {
      MorePBC->data[i] = jj->data[i];
    }

    /*  sparse adds together entries when duplicated, so reduce back to original magnitude */
    st.site = &xd_emlrtRSI;
    b_st.site = &sj_emlrtRSI;
    c_st.site = &tj_emlrtRSI;
    d_st.site = &uj_emlrtRSI;
    if (setnPBC->size[0] < 1) {
      emlrtErrorWithMessageIdR2018a(&d_st, &lc_emlrtRTEI,
        "Coder:toolbox:eml_min_or_max_varDimZero",
        "Coder:toolbox:eml_min_or_max_varDimZero", 0);
    }

    e_st.site = &pl_emlrtRSI;
    f_st.site = &ql_emlrtRSI;
    notdone = setnPBC->size[0];
    if (setnPBC->size[0] <= 2) {
      if (setnPBC->size[0] == 1) {
        elemB = (int32_T)setnPBC->data[0];
      } else if (setnPBC->data[0] < setnPBC->data[1]) {
        elemB = (int32_T)setnPBC->data[1];
      } else {
        elemB = (int32_T)setnPBC->data[0];
      }
    } else {
      g_st.site = &sl_emlrtRSI;
      g_st.site = &rl_emlrtRSI;
      elemB = (int32_T)setnPBC->data[0];
      h_st.site = &fk_emlrtRSI;
      if (setnPBC->size[0] > 2147483646) {
        i_st.site = &fg_emlrtRSI;
        check_forloop_overflow_error(&i_st);
      }

      for (lenn = 2; lenn <= notdone; lenn++) {
        i = (int32_T)setnPBC->data[lenn - 1];
        if (elemB < i) {
          elemB = i;
        }
      }
    }

    st.site = &xd_emlrtRSI;
    if (MorePBC->size[0] < 1) {
      emlrtDynamicBoundsCheckR2012b(MorePBC->size[0], 1, MorePBC->size[0],
        &uo_emlrtBCI, &st);
    }

    allnodes_size[0] = elemB;
    allnodes_size[1] = (int32_T)MorePBC->data[MorePBC->size[0] - 1];
    b_st.site = &tl_emlrtRSI;
    eml_sub2ind(&b_st, allnodes_size, setnPBC, MorePBC, jj);
    i = MorePBC->size[0];
    MorePBC->size[0] = jj->size[0];
    emxEnsureCapacity_real_T(&st, MorePBC, i, &sh_emlrtRTEI);
    loop_ub = jj->size[0];
    for (i = 0; i < loop_ub; i++) {
      MorePBC->data[i] = jj->data[i];
    }

    st.site = &wd_emlrtRSI;
    sparse_parenReference(&st, NodeReg->d, NodeReg->colidx, NodeReg->rowidx,
                          NodeReg->m, NodeReg->n, MorePBC, &NodeRegSum);
    st.site = &wd_emlrtRSI;
    b_sign(&st, &NodeRegSum);
    st.site = &wd_emlrtRSI;
    sparse_times(&st, setnPBC, NodeRegSum.d, NodeRegSum.colidx,
                 NodeRegSum.rowidx, NodeRegSum.m, b_expl_temp.d,
                 b_expl_temp.colidx, b_expl_temp.rowidx, &lenn);
    st.site = &wd_emlrtRSI;
    sparse_parenAssign(&st, NodeReg, b_expl_temp.d, b_expl_temp.colidx,
                       b_expl_temp.rowidx, lenn, MorePBC);

    /* keep the negative sign for PBC nodes to ignore below */
    end = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1] - 1;
    trueCount = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElementPBC->data[b_i] == *numnp + 1.0) {
        trueCount++;
      }
    }

    emxInit_int32_T(sp, &r2, 1, &gl_emlrtRTEI, true);
    i = r2->size[0];
    r2->size[0] = trueCount;
    emxEnsureCapacity_int32_T(sp, r2, i, &mg_emlrtRTEI);
    partialTrueCount = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElementPBC->data[b_i] == *numnp + 1.0) {
        r2->data[partialTrueCount] = b_i + 1;
        partialTrueCount++;
      }
    }

    loop_ub = r2->size[0] - 1;
    elemB = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1];
    for (i = 0; i <= loop_ub; i++) {
      if ((r2->data[i] < 1) || (r2->data[i] > elemB)) {
        emlrtDynamicBoundsCheckR2012b(r2->data[i], 1, elemB, &je_emlrtBCI, sp);
      }

      NodesOnElementPBC->data[r2->data[i] - 1] = 0.0;
    }

    emxFree_int32_T(&r2);

    /* move empty numbers to end of array */
    end = NodesOnElementCG->size[0] * NodesOnElementCG->size[1] - 1;
    trueCount = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElementCG->data[b_i] == *numnp + 1.0) {
        trueCount++;
      }
    }

    emxInit_int32_T(sp, &r3, 1, &hl_emlrtRTEI, true);
    i = r3->size[0];
    r3->size[0] = trueCount;
    emxEnsureCapacity_int32_T(sp, r3, i, &mg_emlrtRTEI);
    partialTrueCount = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElementCG->data[b_i] == *numnp + 1.0) {
        r3->data[partialTrueCount] = b_i + 1;
        partialTrueCount++;
      }
    }

    loop_ub = r3->size[0] - 1;
    elemB = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
    for (i = 0; i <= loop_ub; i++) {
      if ((r3->data[i] < 1) || (r3->data[i] > elemB)) {
        emlrtDynamicBoundsCheckR2012b(r3->data[i], 1, elemB, &ie_emlrtBCI, sp);
      }

      NodesOnElementCG->data[r3->data[i] - 1] = 0.0;
    }

    emxFree_int32_T(&r3);

    /* move empty numbers to end of array */
    /*      % get memory back */
    /*      clear NodeRegInd */
    /*      clear ElementsOnNodeRow */
    /*      clear ElementsOnNodePBCRow */
  } else {
    emxInit_real_T(sp, &b_NodeRegInd, 2, &ig_emlrtRTEI, true);
    i = b_NodeRegInd->size[0] * b_NodeRegInd->size[1];
    b_NodeRegInd->size[0] = 2;
    emxEnsureCapacity_real_T(sp, b_NodeRegInd, i, &hg_emlrtRTEI);
    d = numel * 8.0;
    if (!(d >= 0.0)) {
      emlrtNonNegativeCheckR2012b(d, &jd_emlrtDCI, sp);
    }

    d1 = (int32_T)muDoubleScalarFloor(d);
    if (d != d1) {
      emlrtIntegerCheckR2012b(d, &kd_emlrtDCI, sp);
    }

    i = b_NodeRegInd->size[0] * b_NodeRegInd->size[1];
    b_NodeRegInd->size[1] = (int32_T)d;
    emxEnsureCapacity_real_T(sp, b_NodeRegInd, i, &hg_emlrtRTEI);
    if (d != d1) {
      emlrtIntegerCheckR2012b(d, &pb_emlrtDCI, sp);
    }

    loop_ub = (int32_T)d << 1;
    for (i = 0; i < loop_ub; i++) {
      b_NodeRegInd->data[i] = 0.0;
    }

    if (!(numel >= 0.0)) {
      emlrtNonNegativeCheckR2012b(numel, &hd_emlrtDCI, sp);
    }

    i = (int32_T)muDoubleScalarFloor(numel);
    if (numel != i) {
      emlrtIntegerCheckR2012b(numel, &id_emlrtDCI, sp);
    }

    i1 = ElementsOnNodeRow->size[0] * ElementsOnNodeRow->size[1];
    ElementsOnNodeRow->size[0] = (int32_T)numel;
    loop_ub = (int32_T)nen;
    ElementsOnNodeRow->size[1] = loop_ub;
    emxEnsureCapacity_real_T(sp, ElementsOnNodeRow, i1, &ng_emlrtRTEI);
    if ((int32_T)numel != i) {
      emlrtIntegerCheckR2012b(numel, &mb_emlrtDCI, sp);
    }

    numPBC = (int32_T)numel * loop_ub;
    for (i = 0; i < numPBC; i++) {
      ElementsOnNodeRow->data[i] = 0.0;
    }

    /*      ElementsOnNodeCol = zeros(nen,numel); */
    nri = 0U;
    b_i2 = 0.0;
    i = (int32_T)numel;
    if (0.0 <= numel - 1.0) {
      if (1.0 > nen) {
        locFA = 0;
      } else {
        i1 = NodesOnElement->size[1];
        if (1 > i1) {
          emlrtDynamicBoundsCheckR2012b(1, 1, i1, &so_emlrtBCI, sp);
        }

        i1 = NodesOnElement->size[1];
        if (loop_ub > i1) {
          emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1, i1, &ro_emlrtBCI, sp);
        }

        locFA = loop_ub;
      }
    }

    if (0 <= i - 1) {
      b_nodeE2_size[0] = 1;
      b_nodeE2_size[1] = locFA;
    }

    for (elem = 0; elem < i; elem++) {
      i1 = NodesOnElement->size[0];
      i2 = elem + 1;
      if (i2 > i1) {
        emlrtDynamicBoundsCheckR2012b(i2, 1, i1, &to_emlrtBCI, sp);
      }

      for (i1 = 0; i1 < locFA; i1++) {
        nodeE2_data[i1] = NodesOnElement->data[elem + NodesOnElement->size[0] *
          i1];
      }

      nel = intnnz(nodeE2_data, b_nodeE2_size);
      i1 = RegionOnElement->size[0];
      if (i2 > i1) {
        emlrtDynamicBoundsCheckR2012b(i2, 1, i1, &qo_emlrtBCI, sp);
      }

      for (partialTrueCount = 0; partialTrueCount < nel; partialTrueCount++) {
        /*  Loop over local Nodes */
        i1 = NodesOnElement->size[1];
        if (((int32_T)(partialTrueCount + 1U) < 1) || ((int32_T)
             (partialTrueCount + 1U) > i1)) {
          emlrtDynamicBoundsCheckR2012b((int32_T)(partialTrueCount + 1U), 1, i1,
            &nf_emlrtBCI, sp);
        }

        i1 = NodesOnElement->size[0];
        if (elem + 1 > i1) {
          emlrtDynamicBoundsCheckR2012b(elem + 1, 1, i1, &nf_emlrtBCI, sp);
        }

        node = NodesOnElement->data[elem + NodesOnElement->size[0] *
          partialTrueCount];
        nri++;
        if (((int32_T)nri < 1) || ((int32_T)nri > b_NodeRegInd->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, b_NodeRegInd->size[1],
            &po_emlrtBCI, sp);
        }

        b_NodeRegInd->data[2 * ((int32_T)nri - 1)] = node;
        b_NodeRegInd->data[2 * ((int32_T)nri - 1) + 1] = RegionOnElement->
          data[elem];

        /*              NodeReg(node,reg) = node; % flag that node is in current material set */
        /*    Add element to star list, increment number of elem in star */
        if (((int32_T)(partialTrueCount + 1U) < 1) || ((int32_T)
             (partialTrueCount + 1U) > ElementsOnNodeRow->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)(partialTrueCount + 1U), 1,
            ElementsOnNodeRow->size[1], &hf_emlrtBCI, sp);
        }

        if (elem + 1 > ElementsOnNodeRow->size[0]) {
          emlrtDynamicBoundsCheckR2012b(elem + 1, 1, ElementsOnNodeRow->size[0],
            &hf_emlrtBCI, sp);
        }

        if (node != (int32_T)muDoubleScalarFloor(node)) {
          emlrtIntegerCheckR2012b(node, &jb_emlrtDCI, sp);
        }

        if (((int32_T)node < 1) || ((int32_T)node > ElementsOnNodeNum->size[0]))
        {
          emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
            ElementsOnNodeNum->size[0], &if_emlrtBCI, sp);
        }

        numA = ElementsOnNodeNum->data[(int32_T)node - 1] + 1.0;
        ElementsOnNodeRow->data[elem + ElementsOnNodeRow->size[0] *
          partialTrueCount] = numA;

        /*              ElementsOnNode(locE,node) = elem; */
        if (((int32_T)node < 1) || ((int32_T)node > ElementsOnNodeNum->size[0]))
        {
          emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
            ElementsOnNodeNum->size[0], &ef_emlrtBCI, sp);
        }

        ElementsOnNodeNum->data[(int32_T)node - 1] = numA;

        /*              ElementsOnNodeCol(locN,elem) = node; */
        /*              ElementsOnNodeDup(locE,node) = node; */
        if (*emlrtBreakCheckR2012bFlagVar != 0) {
          emlrtBreakCheckR2012b(sp);
        }
      }

      i1 = loop_ub - nel;
      emlrtForLoopVectorCheckR2012b((real_T)nel + 1.0, 1.0, nen, mxDOUBLE_CLASS,
        i1, &hc_emlrtRTEI, sp);
      for (partialTrueCount = 0; partialTrueCount < i1; partialTrueCount++) {
        b_i2++;
        i2 = (int32_T)(((real_T)nel + 1.0) + (real_T)partialTrueCount);
        if ((i2 < 1) || (i2 > ElementsOnNodeRow->size[1])) {
          emlrtDynamicBoundsCheckR2012b(i2, 1, ElementsOnNodeRow->size[1],
            &kf_emlrtBCI, sp);
        }

        if (elem + 1 > ElementsOnNodeRow->size[0]) {
          emlrtDynamicBoundsCheckR2012b(elem + 1, 1, ElementsOnNodeRow->size[0],
            &kf_emlrtBCI, sp);
        }

        ElementsOnNodeRow->data[elem + ElementsOnNodeRow->size[0] * (i2 - 1)] =
          b_i2;
        if (*emlrtBreakCheckR2012bFlagVar != 0) {
          emlrtBreakCheckR2012b(sp);
        }
      }

      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    end = NodesOnElement->size[0] * NodesOnElement->size[1] - 1;
    trueCount = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElement->data[b_i] == 0.0) {
        trueCount++;
      }
    }

    i = jj->size[0];
    jj->size[0] = trueCount;
    emxEnsureCapacity_int32_T(sp, jj, i, &mg_emlrtRTEI);
    partialTrueCount = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElement->data[b_i] == 0.0) {
        jj->data[partialTrueCount] = b_i + 1;
        partialTrueCount++;
      }
    }

    elemB = NodesOnElement->size[0] * NodesOnElement->size[1];
    b_loop_ub = jj->size[0];
    for (i = 0; i < b_loop_ub; i++) {
      if ((jj->data[i] < 1) || (jj->data[i] > elemB)) {
        emlrtDynamicBoundsCheckR2012b(jj->data[i], 1, elemB, &lf_emlrtBCI, sp);
      }

      NodesOnElement->data[jj->data[i] - 1] = *numnp + 1.0;
    }

    /* move empty numbers to end of array */
    if (numel < 1.0) {
      setAll->size[0] = 1;
      setAll->size[1] = 0;
    } else {
      i = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      b_loop_ub = (int32_T)numel - 1;
      setAll->size[1] = b_loop_ub + 1;
      emxEnsureCapacity_real_T(sp, setAll, i, &yg_emlrtRTEI);
      for (i = 0; i <= b_loop_ub; i++) {
        setAll->data[i] = (real_T)i + 1.0;
      }
    }

    i = ElementsOnNodePBCRow->size[0] * ElementsOnNodePBCRow->size[1];
    ElementsOnNodePBCRow->size[0] = setAll->size[1];
    ElementsOnNodePBCRow->size[1] = loop_ub;
    emxEnsureCapacity_real_T(sp, ElementsOnNodePBCRow, i, &bh_emlrtRTEI);
    for (i = 0; i < loop_ub; i++) {
      b_loop_ub = setAll->size[1];
      for (i1 = 0; i1 < b_loop_ub; i1++) {
        ElementsOnNodePBCRow->data[i1 + ElementsOnNodePBCRow->size[0] * i] =
          setAll->data[i1];
      }
    }

    st.site = &vd_emlrtRSI;
    sparse(&st, ElementsOnNodeRow, NodesOnElement, ElementsOnNodePBCRow,
           ElementsOnNode);

    /* zeros(maxel,numnp); % Array of elements connected to nodes */
    st.site = &ud_emlrtRSI;
    sparse(&st, ElementsOnNodeRow, NodesOnElement, NodesOnElement,
           ElementsOnNodeDup);

    /*  actual node ID given to element for an original node; general counterpart of NodeMat */
    if (1 > (int32_T)nri) {
      b_loop_ub = 0;
      notdone = 0;
      c_loop_ub = 0;
    } else {
      if (1 > b_NodeRegInd->size[1]) {
        emlrtDynamicBoundsCheckR2012b(1, 1, b_NodeRegInd->size[1], &oo_emlrtBCI,
          sp);
      }

      if (((int32_T)nri < 1) || ((int32_T)nri > b_NodeRegInd->size[1])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, b_NodeRegInd->size[1],
          &no_emlrtBCI, sp);
      }

      b_loop_ub = (int32_T)nri;
      if (1 > b_NodeRegInd->size[1]) {
        emlrtDynamicBoundsCheckR2012b(1, 1, b_NodeRegInd->size[1], &mo_emlrtBCI,
          sp);
      }

      if (((int32_T)nri < 1) || ((int32_T)nri > b_NodeRegInd->size[1])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, b_NodeRegInd->size[1],
          &lo_emlrtBCI, sp);
      }

      notdone = (int32_T)nri;
      if (1 > b_NodeRegInd->size[1]) {
        emlrtDynamicBoundsCheckR2012b(1, 1, b_NodeRegInd->size[1], &ko_emlrtBCI,
          sp);
      }

      if (((int32_T)nri < 1) || ((int32_T)nri > b_NodeRegInd->size[1])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, b_NodeRegInd->size[1],
          &jo_emlrtBCI, sp);
      }

      c_loop_ub = (int32_T)nri;
    }

    i = setPBC->size[0] * setPBC->size[1];
    setPBC->size[0] = 1;
    setPBC->size[1] = b_loop_ub;
    emxEnsureCapacity_real_T(sp, setPBC, i, &eh_emlrtRTEI);
    for (i = 0; i < b_loop_ub; i++) {
      setPBC->data[i] = b_NodeRegInd->data[2 * i];
    }

    i = setAll->size[0] * setAll->size[1];
    setAll->size[0] = 1;
    setAll->size[1] = notdone;
    emxEnsureCapacity_real_T(sp, setAll, i, &hh_emlrtRTEI);
    for (i = 0; i < notdone; i++) {
      setAll->data[i] = b_NodeRegInd->data[2 * i + 1];
    }

    i = NodeRegInd->size[0] * NodeRegInd->size[1];
    NodeRegInd->size[0] = 1;
    NodeRegInd->size[1] = c_loop_ub;
    emxEnsureCapacity_real_T(sp, NodeRegInd, i, &ih_emlrtRTEI);
    for (i = 0; i < c_loop_ub; i++) {
      NodeRegInd->data[i] = b_NodeRegInd->data[2 * i];
    }

    emxFree_real_T(&b_NodeRegInd);
    st.site = &td_emlrtRSI;
    b_sparse(&st, setPBC, setAll, NodeRegInd, NodeReg);
    st.site = &sd_emlrtRSI;
    b_st.site = &il_emlrtRSI;
    e_eml_find(&b_st, NodeReg->d, NodeReg->colidx, NodeReg->rowidx, NodeReg->m,
               NodeReg->n, ii, jj);
    i = setnPBC->size[0];
    setnPBC->size[0] = ii->size[0];
    emxEnsureCapacity_real_T(&st, setnPBC, i, &jh_emlrtRTEI);
    b_loop_ub = ii->size[0];
    for (i = 0; i < b_loop_ub; i++) {
      setnPBC->data[i] = ii->data[i];
    }

    i = MorePBC->size[0];
    MorePBC->size[0] = jj->size[0];
    emxEnsureCapacity_real_T(&st, MorePBC, i, &kh_emlrtRTEI);
    b_loop_ub = jj->size[0];
    for (i = 0; i < b_loop_ub; i++) {
      MorePBC->data[i] = jj->data[i];
    }

    st.site = &rd_emlrtRSI;
    if (MorePBC->size[0] < 1) {
      emlrtDynamicBoundsCheckR2012b(MorePBC->size[0], 1, MorePBC->size[0],
        &io_emlrtBCI, &st);
    }

    allnodes_size[0] = (int32_T)*numnp;
    allnodes_size[1] = (int32_T)MorePBC->data[MorePBC->size[0] - 1];
    b_st.site = &tl_emlrtRSI;
    eml_sub2ind(&b_st, allnodes_size, setnPBC, MorePBC, jj);
    i = MorePBC->size[0];
    MorePBC->size[0] = jj->size[0];
    emxEnsureCapacity_real_T(&st, MorePBC, i, &lh_emlrtRTEI);
    b_loop_ub = jj->size[0];
    for (i = 0; i < b_loop_ub; i++) {
      MorePBC->data[i] = jj->data[i];
    }

    st.site = &qd_emlrtRSI;
    b_sparse_parenAssign(&st, NodeReg, setnPBC, MorePBC);
    end = NodesOnElement->size[0] * NodesOnElement->size[1] - 1;
    trueCount = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElement->data[b_i] == *numnp + 1.0) {
        trueCount++;
      }
    }

    i = r1->size[0];
    r1->size[0] = trueCount;
    emxEnsureCapacity_int32_T(sp, r1, i, &mg_emlrtRTEI);
    partialTrueCount = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElement->data[b_i] == *numnp + 1.0) {
        r1->data[partialTrueCount] = b_i + 1;
        partialTrueCount++;
      }
    }

    elemB = NodesOnElement->size[0] * NodesOnElement->size[1];
    b_loop_ub = r1->size[0];
    for (i = 0; i < b_loop_ub; i++) {
      if ((r1->data[i] < 1) || (r1->data[i] > elemB)) {
        emlrtDynamicBoundsCheckR2012b(r1->data[i], 1, elemB, &ke_emlrtBCI, sp);
      }

      NodesOnElement->data[r1->data[i] - 1] = 0.0;
    }

    /* move empty numbers to end of array */
    i = ElementsOnNodePBCRow->size[0] * ElementsOnNodePBCRow->size[1];
    ElementsOnNodePBCRow->size[0] = (int32_T)numel;
    ElementsOnNodePBCRow->size[1] = loop_ub;
    emxEnsureCapacity_real_T(sp, ElementsOnNodePBCRow, i, &ph_emlrtRTEI);
    for (i = 0; i < numPBC; i++) {
      ElementsOnNodePBCRow->data[i] = 0.0;
    }

    st.site = &pd_emlrtRSI;
    c_sparse(&st, ElementsOnNodePBCRow, expl_temp.d, expl_temp.colidx,
             expl_temp.rowidx, &ElementsOnNodePBC_m, &ElementsOnNodePBC_n,
             &elemB);

    /*      % get memory back */
    /*      clear NodeRegInd */
    /*      clear ElementsOnNodeRow */
  }

  emxFree_int32_T(&r1);
  emxFree_real_T(&ElementsOnNodeRow);
  emxFree_real_T(&ElementsOnNodePBCRow);
  i = NodesOnElementDG->size[0] * NodesOnElementDG->size[1];
  NodesOnElementDG->size[0] = NodesOnElementCG->size[0];
  NodesOnElementDG->size[1] = NodesOnElementCG->size[1];
  emxEnsureCapacity_real_T(sp, NodesOnElementDG, i, &qh_emlrtRTEI);
  loop_ub = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
  for (i = 0; i < loop_ub; i++) {
    NodesOnElementDG->data[i] = NodesOnElementCG->data[i];
  }

  if (1.0 > *numnp) {
    loop_ub = 0;
  } else {
    i = Coordinates->size[0];
    if (1 > i) {
      emlrtDynamicBoundsCheckR2012b(1, 1, i, &ho_emlrtBCI, sp);
    }

    i = Coordinates->size[0];
    loop_ub = (int32_T)*numnp;
    if ((loop_ub < 1) || (loop_ub > i)) {
      emlrtDynamicBoundsCheckR2012b(loop_ub, 1, i, &go_emlrtBCI, sp);
    }
  }

  if (1.0 > *numnp) {
    i1 = 0;
  } else {
    i = (int32_T)numel * (int32_T)nen;
    if (1 > i) {
      emlrtDynamicBoundsCheckR2012b(1, 1, i, &lh_emlrtBCI, sp);
    }

    i1 = (int32_T)*numnp;
    if ((i1 < 1) || (i1 > i)) {
      emlrtDynamicBoundsCheckR2012b(i1, 1, i, &kh_emlrtBCI, sp);
    }
  }

  input_sizes[0] = i1;
  input_sizes[1] = 3;
  allnodes_size[0] = loop_ub;
  allnodes_size[1] = 3;
  emlrtSubAssignSizeCheckR2012b(&input_sizes[0], 2, &allnodes_size[0], 2,
    &e_emlrtECI, sp);
  for (i = 0; i < 3; i++) {
    for (i1 = 0; i1 < loop_ub; i1++) {
      Coordinates3->data[i1 + Coordinates3->size[0] * i] = Coordinates->data[i1
        + Coordinates->size[0] * i];
    }
  }

  *numinttype = nummat * (nummat + 1.0) / 2.0;
  if (!(*numinttype >= 0.0)) {
    emlrtNonNegativeCheckR2012b(*numinttype, &eb_emlrtDCI, sp);
  }

  i = (int32_T)muDoubleScalarFloor(*numinttype);
  if (*numinttype != i) {
    emlrtIntegerCheckR2012b(*numinttype, &fb_emlrtDCI, sp);
  }

  i1 = numEonF->size[0];
  loop_ub = (int32_T)*numinttype;
  numEonF->size[0] = loop_ub;
  emxEnsureCapacity_real_T(sp, numEonF, i1, &rh_emlrtRTEI);
  if (loop_ub != i) {
    emlrtIntegerCheckR2012b(*numinttype, &fb_emlrtDCI, sp);
  }

  for (i1 = 0; i1 < loop_ub; i1++) {
    numEonF->data[i1] = 0.0;
  }

  *numEonB = 0.0;
  st.site = &od_emlrtRSI;
  if (muDoubleScalarIsNaN(usePBC)) {
    emlrtErrorWithMessageIdR2018a(&st, &mc_emlrtRTEI, "MATLAB:nologicalnan",
      "MATLAB:nologicalnan", 0);
  }

  if (usePBC != 0.0) {
    d = 8.0 * numel;
    if (d != (int32_T)d) {
      emlrtIntegerCheckR2012b(d, &db_emlrtDCI, sp);
    }

    i1 = FacetsOnPBC->size[0];
    FacetsOnPBC->size[0] = (int32_T)d;
    emxEnsureCapacity_real_T(sp, FacetsOnPBC, i1, &uh_emlrtRTEI);
    d = 8.0 * numel;
    if (d != (int32_T)d) {
      emlrtIntegerCheckR2012b(d, &db_emlrtDCI, sp);
    }

    b_loop_ub = (int32_T)d;
    for (i1 = 0; i1 < b_loop_ub; i1++) {
      FacetsOnPBC->data[i1] = 0.0;
    }

    /*  separate list for PBC facets that are found; grouped by interface type */
    if (loop_ub != i) {
      emlrtIntegerCheckR2012b(*numinttype, &cb_emlrtDCI, sp);
    }

    i1 = numEonPBC->size[0];
    numEonPBC->size[0] = loop_ub;
    emxEnsureCapacity_real_T(sp, numEonPBC, i1, &wh_emlrtRTEI);
    if (loop_ub != i) {
      emlrtIntegerCheckR2012b(*numinttype, &cb_emlrtDCI, sp);
    }

    for (i = 0; i < loop_ub; i++) {
      numEonPBC->data[i] = 0.0;
    }

    numFPBC = 0.0;

    /*  record how many PBC facets are found */
  } else {
    i = FacetsOnPBC->size[0];
    FacetsOnPBC->size[0] = 4;
    emxEnsureCapacity_real_T(sp, FacetsOnPBC, i, &th_emlrtRTEI);
    FacetsOnPBC->data[0] = 0.0;
    FacetsOnPBC->data[1] = 0.0;
    FacetsOnPBC->data[2] = 0.0;
    FacetsOnPBC->data[3] = 0.0;

    /*  separate list for PBC facets that are found; grouped by interface type */
    i = numEonPBC->size[0];
    numEonPBC->size[0] = 2;
    emxEnsureCapacity_real_T(sp, numEonPBC, i, &vh_emlrtRTEI);
    numEonPBC->data[0] = 0.0;
    numEonPBC->data[1] = 0.0;
    numFPBC = 0.0;

    /*  record how many PBC facets are found */
  }

  d = 6.0 * numel;
  if (d != (int32_T)d) {
    emlrtIntegerCheckR2012b(d, &cd_emlrtDCI, sp);
  }

  i = ElementsOnBoundary->size[0] * ElementsOnBoundary->size[1];
  ElementsOnBoundary->size[0] = (int32_T)d;
  ElementsOnBoundary->size[1] = 2;
  emxEnsureCapacity_real_T(sp, ElementsOnBoundary, i, &xh_emlrtRTEI);
  if (d != (int32_T)d) {
    emlrtIntegerCheckR2012b(d, &bb_emlrtDCI, sp);
  }

  b_loop_ub = (int32_T)d << 1;
  for (i = 0; i < b_loop_ub; i++) {
    ElementsOnBoundary->data[i] = 0.0;
  }

  /*  All exposed faces */
  i = FacetsOnElement->size[0] * FacetsOnElement->size[1];
  FacetsOnElement->size[0] = (int32_T)numel;
  FacetsOnElement->size[1] = 6;
  emxEnsureCapacity_real_T(sp, FacetsOnElement, i, &yh_emlrtRTEI);
  numPBC = (int32_T)numel * 6;
  for (i = 0; i < numPBC; i++) {
    FacetsOnElement->data[i] = 0.0;
  }

  i = FacetsOnElementInt->size[0] * FacetsOnElementInt->size[1];
  FacetsOnElementInt->size[0] = (int32_T)numel;
  FacetsOnElementInt->size[1] = 6;
  emxEnsureCapacity_real_T(sp, FacetsOnElementInt, i, &ai_emlrtRTEI);
  for (i = 0; i < numPBC; i++) {
    FacetsOnElementInt->data[i] = 0.0;
  }

  if (d != (int32_T)d) {
    emlrtIntegerCheckR2012b(d, &bd_emlrtDCI, sp);
  }

  i = ElementsOnFacet->size[0] * ElementsOnFacet->size[1];
  ElementsOnFacet->size[0] = (int32_T)d;
  ElementsOnFacet->size[1] = 4;
  emxEnsureCapacity_real_T(sp, ElementsOnFacet, i, &bi_emlrtRTEI);
  if (d != (int32_T)d) {
    emlrtIntegerCheckR2012b(d, &ab_emlrtDCI, sp);
  }

  b_loop_ub = (int32_T)d << 2;
  for (i = 0; i < b_loop_ub; i++) {
    ElementsOnFacet->data[i] = 0.0;
  }

  if (d != (int32_T)d) {
    emlrtIntegerCheckR2012b(d, &y_emlrtDCI, sp);
  }

  i = FacetsOnInterface->size[0];
  FacetsOnInterface->size[0] = (int32_T)d;
  emxEnsureCapacity_real_T(sp, FacetsOnInterface, i, &ci_emlrtRTEI);
  if (d != (int32_T)d) {
    emlrtIntegerCheckR2012b(d, &y_emlrtDCI, sp);
  }

  b_loop_ub = (int32_T)d;
  for (i = 0; i < b_loop_ub; i++) {
    FacetsOnInterface->data[i] = 0.0;
  }

  i = FacetsOnNodeNum->size[0];
  FacetsOnNodeNum->size[0] = (int32_T)*numnp;
  emxEnsureCapacity_real_T(sp, FacetsOnNodeNum, i, &di_emlrtRTEI);
  b_loop_ub = (int32_T)*numnp;
  for (i = 0; i < b_loop_ub; i++) {
    FacetsOnNodeNum->data[i] = 0.0;
  }

  /*  number of edges attached to a node. */
  st.site = &nd_emlrtRSI;
  b_st.site = &sj_emlrtRSI;
  c_st.site = &tj_emlrtRSI;
  d_st.site = &uj_emlrtRSI;
  if (ElementsOnNodeNum->size[0] < 1) {
    emlrtErrorWithMessageIdR2018a(&d_st, &lc_emlrtRTEI,
      "Coder:toolbox:eml_min_or_max_varDimZero",
      "Coder:toolbox:eml_min_or_max_varDimZero", 0);
  }

  e_st.site = &pl_emlrtRSI;
  f_st.site = &ql_emlrtRSI;
  notdone = ElementsOnNodeNum->size[0];
  if (ElementsOnNodeNum->size[0] <= 2) {
    if (ElementsOnNodeNum->size[0] == 1) {
      numA = ElementsOnNodeNum->data[0];
    } else if (ElementsOnNodeNum->data[0] < ElementsOnNodeNum->data[1]) {
      numA = ElementsOnNodeNum->data[1];
    } else {
      numA = ElementsOnNodeNum->data[0];
    }
  } else {
    g_st.site = &sl_emlrtRSI;
    g_st.site = &rl_emlrtRSI;
    numA = ElementsOnNodeNum->data[0];
    h_st.site = &fk_emlrtRSI;
    if (ElementsOnNodeNum->size[0] > 2147483646) {
      i_st.site = &fg_emlrtRSI;
      check_forloop_overflow_error(&i_st);
    }

    for (lenn = 2; lenn <= notdone; lenn++) {
      d = ElementsOnNodeNum->data[lenn - 1];
      if (numA < d) {
        numA = d;
      }
    }
  }

  emxInit_real_T(&f_st, &FacetsOnNodeInd, 2, &ei_emlrtRTEI, true);
  i = FacetsOnNodeInd->size[0] * FacetsOnNodeInd->size[1];
  FacetsOnNodeInd->size[0] = 5;
  emxEnsureCapacity_real_T(sp, FacetsOnNodeInd, i, &ei_emlrtRTEI);
  d = *numnp * 3.0 * numA;
  if (!(d >= 0.0)) {
    emlrtNonNegativeCheckR2012b(d, &yc_emlrtDCI, sp);
  }

  d1 = (int32_T)muDoubleScalarFloor(d);
  if (d != d1) {
    emlrtIntegerCheckR2012b(d, &ad_emlrtDCI, sp);
  }

  i = FacetsOnNodeInd->size[0] * FacetsOnNodeInd->size[1];
  FacetsOnNodeInd->size[1] = (int32_T)d;
  emxEnsureCapacity_real_T(sp, FacetsOnNodeInd, i, &ei_emlrtRTEI);
  if (d != d1) {
    emlrtIntegerCheckR2012b(d, &x_emlrtDCI, sp);
  }

  b_loop_ub = 5 * (int32_T)d;
  for (i = 0; i < b_loop_ub; i++) {
    FacetsOnNodeInd->data[i] = 0.0;
  }

  /*  # of nodes times 3 faces times max # of elements on any node */
  nri = 0U;
  *numfac = 0.0;

  /*  Templates for nodes and facets */
  /*  Find all facets in mesh */
  i = (int32_T)numel;
  if (0.0 <= numel - 1.0) {
    if (1.0 > nen) {
      f_loop_ub = 0;
    } else {
      if (1 > NodesOnElementCG->size[1]) {
        emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementCG->size[1],
          &eo_emlrtBCI, sp);
      }

      f_loop_ub = (int32_T)nen;
      if (f_loop_ub > NodesOnElementCG->size[1]) {
        emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1, NodesOnElementCG->size[1],
          &do_emlrtBCI, sp);
      }
    }
  }

  emxInit_real_T(sp, &ElementsOnNodeABCD, 2, &bl_emlrtRTEI, true);
  for (elem = 0; elem < i; elem++) {
    i1 = elem + 1;
    if (i1 > NodesOnElementCG->size[0]) {
      emlrtDynamicBoundsCheckR2012b(i1, 1, NodesOnElementCG->size[0],
        &fo_emlrtBCI, sp);
    }

    b_nodeE2_size[0] = 1;
    b_nodeE2_size[1] = f_loop_ub;
    for (i2 = 0; i2 < f_loop_ub; i2++) {
      nodeE2_data[i2] = NodesOnElementCG->data[elem + NodesOnElementCG->size[0] *
        i2];
    }

    nel = intnnz(nodeE2_data, b_nodeE2_size);
    if ((nel == 4) || (nel == 10)) {
      nume = 3;
      numfn_size_idx_1 = 5;
      for (i2 = 0; i2 < 5; i2++) {
        numfn_data[i2] = 3;
      }

      nloop_size_idx_0 = 4;
      nloop_size_idx_1 = 6;
      for (i2 = 0; i2 < 24; i2++) {
        nloop_data[i2] = nloopT[i2];
      }

      nel2 = 4;
    } else if ((nel == 6) || (nel == 18)) {
      nume = 4;
      numfn_size_idx_1 = 5;
      for (i2 = 0; i2 < 5; i2++) {
        numfn_data[i2] = numfnW[i2];
      }

      nloop_size_idx_0 = 5;
      nloop_size_idx_1 = 8;
      for (i2 = 0; i2 < 40; i2++) {
        nloop_data[i2] = nloopW[i2];
      }

      nel2 = 6;
    } else if ((nel == 5) || (nel == 14)) {
      nume = 4;
      numfn_size_idx_1 = 5;
      for (i2 = 0; i2 < 5; i2++) {
        numfn_data[i2] = numfnP[i2];
      }

      nloop_size_idx_0 = 5;
      nloop_size_idx_1 = 8;
      for (i2 = 0; i2 < 40; i2++) {
        nloop_data[i2] = nloopP[i2];
      }

      nel2 = 5;
    } else {
      nume = 5;
      numfn_size_idx_1 = 6;
      for (i2 = 0; i2 < 6; i2++) {
        numfn_data[i2] = 4;
      }

      nloop_size_idx_0 = 6;
      nloop_size_idx_1 = 8;
      for (i2 = 0; i2 < 48; i2++) {
        nloop_data[i2] = nloopH[i2];
      }

      nel2 = 8;
    }

    /*  Loop over edges of element */
    for (locF = 0; locF <= nume; locF++) {
      if (elem + 1 > FacetsOnElement->size[0]) {
        emlrtDynamicBoundsCheckR2012b(elem + 1, 1, FacetsOnElement->size[0],
          &he_emlrtBCI, sp);
      }

      st.site = &md_emlrtRSI;
      if (!(FacetsOnElement->data[elem + FacetsOnElement->size[0] * locF] != 0.0))
      {
        i2 = RegionOnElement->size[0];
        if (i1 > i2) {
          emlrtDynamicBoundsCheckR2012b(i1, 1, i2, &co_emlrtBCI, sp);
        }

        /*  reg1 is overwritten below, so it must be re-evaluated */
        if (locF + 1 > numfn_size_idx_1) {
          emlrtDynamicBoundsCheckR2012b(locF + 1, 1, numfn_size_idx_1,
            &ge_emlrtBCI, sp);
        }

        i5 = numfn_data[locF];

        /*  number of nodes on face */
        if (locF + 1 > nloop_size_idx_0) {
          emlrtDynamicBoundsCheckR2012b(locF + 1, 1, nloop_size_idx_0,
            &ao_emlrtBCI, sp);
        }

        if (elem + 1 > NodesOnElementCG->size[0]) {
          emlrtDynamicBoundsCheckR2012b(elem + 1, 1, NodesOnElementCG->size[0],
            &bo_emlrtBCI, sp);
        }

        b_loop_ub = i5;
        nodeABCD_size[0] = 1;
        nodeABCD_size[1] = i5;
        for (i2 = 0; i2 < b_loop_ub; i2++) {
          i3 = nloop_data[locF + nloop_size_idx_0 * i2];
          if ((i3 < 1) || (i3 > NodesOnElementCG->size[1])) {
            emlrtDynamicBoundsCheckR2012b(i3, 1, NodesOnElementCG->size[1],
              &fe_emlrtBCI, sp);
          }

          nodeABCD_data[i2] = NodesOnElementCG->data[elem +
            NodesOnElementCG->size[0] * (i3 - 1)];
        }

        NodesOnElementCG_size[0] = 1;
        NodesOnElementCG_size[1] = numfn_data[locF];
        st.site = &ld_emlrtRSI;
        indexShapeCheck(&st, ElementsOnNodeNum->size[0], NodesOnElementCG_size);
        b_loop_ub = i5;
        for (i2 = 0; i2 < b_loop_ub; i2++) {
          d = nodeABCD_data[i2];
          if (d != (int32_T)muDoubleScalarFloor(d)) {
            emlrtIntegerCheckR2012b(d, &w_emlrtDCI, sp);
          }

          if (((int32_T)d < 1) || ((int32_T)d > ElementsOnNodeNum->size[0])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, ElementsOnNodeNum->
              size[0], &ee_emlrtBCI, sp);
          }

          numABCD_data[i2] = ElementsOnNodeNum->data[(int32_T)d - 1];
        }

        st.site = &kd_emlrtRSI;
        b_st.site = &sj_emlrtRSI;
        c_st.site = &tj_emlrtRSI;
        d_st.site = &uj_emlrtRSI;
        e_st.site = &pl_emlrtRSI;
        f_st.site = &ql_emlrtRSI;
        i2 = numfn_data[locF];
        g_st.site = &sl_emlrtRSI;
        g_st.site = &rl_emlrtRSI;
        numA = numABCD_data[0];
        h_st.site = &fk_emlrtRSI;
        for (lenn = 2; lenn <= i2; lenn++) {
          d = numABCD_data[lenn - 1];
          if (numA < d) {
            numA = d;
          }
        }

        if (numA < 1.0) {
          setAll->size[0] = 1;
          setAll->size[1] = 0;
        } else if (muDoubleScalarIsInf(numA) && (1.0 == numA)) {
          i2 = setAll->size[0] * setAll->size[1];
          setAll->size[0] = 1;
          setAll->size[1] = 1;
          emxEnsureCapacity_real_T(sp, setAll, i2, &qi_emlrtRTEI);
          setAll->data[0] = rtNaN;
        } else {
          i2 = setAll->size[0] * setAll->size[1];
          setAll->size[0] = 1;
          notdone = (int32_T)muDoubleScalarFloor(numA - 1.0);
          setAll->size[1] = notdone + 1;
          emxEnsureCapacity_real_T(sp, setAll, i2, &qi_emlrtRTEI);
          for (i2 = 0; i2 <= notdone; i2++) {
            setAll->data[i2] = (real_T)i2 + 1.0;
          }
        }

        st.site = &kd_emlrtRSI;
        b_sparse_parenReference(&st, ElementsOnNode->d, ElementsOnNode->colidx,
          ElementsOnNode->rowidx, ElementsOnNode->m, ElementsOnNode->n, setAll,
          nodeABCD_data, nodeABCD_size, t5_d, ii, idx, &notdone, &lenn, &elemB);
        st.site = &kd_emlrtRSI;
        b_sparse_full(&st, t5_d, ii, idx, notdone, lenn, ElementsOnNodeABCD);

        /*  Determine if edge is on domain interior or boundary */
        b_NodesOnPBC_size[0] = i5;
        for (i2 = 0; i2 < b_loop_ub; i2++) {
          b_numABCD_data[i2] = (numABCD_data[i2] > 1.0);
        }

        st.site = &jd_emlrtRSI;
        if (all(b_numABCD_data, b_NodesOnPBC_size)) {
          /*  Clean and fast way to intersect the 3 sets of elements, using */
          /*  built-in Matlab functions; change ismembc to ismember if the */
          /*  function is not in the standard package */
          /*              if isOctave */
          d = ElementsOnNodeNum->data[(int32_T)nodeABCD_data[0] - 1];
          if (1.0 > d) {
            b_loop_ub = 0;
          } else {
            if (1 > ElementsOnNodeABCD->size[0]) {
              emlrtDynamicBoundsCheckR2012b(1, 1, ElementsOnNodeABCD->size[0],
                &yn_emlrtBCI, sp);
            }

            if (((int32_T)d < 1) || ((int32_T)d > ElementsOnNodeABCD->size[0]))
            {
              emlrtDynamicBoundsCheckR2012b((int32_T)d, 1,
                ElementsOnNodeABCD->size[0], &xn_emlrtBCI, sp);
            }

            b_loop_ub = (int32_T)d;
          }

          i2 = MorePBC->size[0];
          MorePBC->size[0] = b_loop_ub;
          emxEnsureCapacity_real_T(sp, MorePBC, i2, &ti_emlrtRTEI);
          for (i2 = 0; i2 < b_loop_ub; i2++) {
            MorePBC->data[i2] = ElementsOnNodeABCD->data[i2];
          }

          i2 = numfn_data[locF];
          for (j = 0; j <= i2 - 2; j++) {
            if (j + 2 > i5) {
              emlrtDynamicBoundsCheckR2012b(j + 2, 1, i5, &wn_emlrtBCI, sp);
            }

            d = ElementsOnNodeNum->data[(int32_T)nodeABCD_data[j + 1] - 1];
            if (1.0 > d) {
              b_loop_ub = 0;
            } else {
              if (1 > ElementsOnNodeABCD->size[0]) {
                emlrtDynamicBoundsCheckR2012b(1, 1, ElementsOnNodeABCD->size[0],
                  &vn_emlrtBCI, sp);
              }

              if (((int32_T)d < 1) || ((int32_T)d > ElementsOnNodeABCD->size[0]))
              {
                emlrtDynamicBoundsCheckR2012b((int32_T)d, 1,
                  ElementsOnNodeABCD->size[0], &un_emlrtBCI, sp);
              }

              b_loop_ub = (int32_T)d;
            }

            st.site = &id_emlrtRSI;
            if (j + 2 > ElementsOnNodeABCD->size[1]) {
              emlrtDynamicBoundsCheckR2012b(j + 2, 1, ElementsOnNodeABCD->size[1],
                &tn_emlrtBCI, &st);
            }

            i3 = t5_d->size[0];
            t5_d->size[0] = b_loop_ub;
            emxEnsureCapacity_real_T(&st, t5_d, i3, &wi_emlrtRTEI);
            for (i3 = 0; i3 < b_loop_ub; i3++) {
              t5_d->data[i3] = ElementsOnNodeABCD->data[i3 +
                ElementsOnNodeABCD->size[0] * (j + 1)];
            }

            b_st.site = &ng_emlrtRSI;
            b_local_ismember(&b_st, MorePBC, t5_d, b_NodesOnPBCnum);
            end = b_NodesOnPBCnum->size[0] - 1;
            trueCount = 0;
            for (b_i = 0; b_i <= end; b_i++) {
              if (b_NodesOnPBCnum->data[b_i]) {
                trueCount++;
              }
            }

            partialTrueCount = 0;
            for (b_i = 0; b_i <= end; b_i++) {
              if (b_NodesOnPBCnum->data[b_i]) {
                if ((b_i + 1 < 1) || (b_i + 1 > MorePBC->size[0])) {
                  emlrtDynamicBoundsCheckR2012b(b_i + 1, 1, MorePBC->size[0],
                    &rd_emlrtBCI, sp);
                }

                MorePBC->data[partialTrueCount] = MorePBC->data[b_i];
                partialTrueCount++;
              }
            }

            i3 = MorePBC->size[0];
            MorePBC->size[0] = trueCount;
            emxEnsureCapacity_real_T(sp, MorePBC, i3, &mg_emlrtRTEI);
            if (*emlrtBreakCheckR2012bFlagVar != 0) {
              emlrtBreakCheckR2012b(sp);
            }
          }

          /*              else */
          /*                  twoelem = ElementsOnNodeABCD(1:numABCD(1),1); */
          /*                  for j = 2:numfacnod */
          /*                      twoelem = twoelem(ismembc(twoelem,ElementsOnNodeABCD(1:numABCD(j),j))); */
          /*                  end */
          /*              end */
          if (MorePBC->size[0] == 2) {
            /*  element interface */
            facIL = 1;
            if (MorePBC->data[0] == (real_T)elem + 1.0) {
              elemA = MorePBC->data[1];
            } else {
              elemA = MorePBC->data[0];
            }
          } else if (MorePBC->size[0] > 2) {
            /*  likely a PBC degenerating case */
            st.site = &hd_emlrtRSI;
            f_error(&st);
          } else {
            /*  domain boundary */
            facIL = 0;
            elemA = 0.0;
          }
        } else {
          /*  domain boundary */
          facIL = 0;
          elemA = 0.0;
        }

        if (facIL == 1) {
          /* element interface, add to SurfacesI */
          /*  Find which slots each node occupies on elemA */
          nodeAABBCCDD[0] = 0;
          nodeAABBCCDD[1] = 0;
          nodeAABBCCDD[2] = 0;
          nodeAABBCCDD[3] = 0;

          /*  Other element may be a different type */
          if (1.0 > nen) {
            b_loop_ub = 0;
          } else {
            if (1 > NodesOnElementCG->size[1]) {
              emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementCG->size[1],
                &rn_emlrtBCI, sp);
            }

            if ((int32_T)nen > NodesOnElementCG->size[1]) {
              emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1,
                NodesOnElementCG->size[1], &qn_emlrtBCI, sp);
            }

            b_loop_ub = (int32_T)nen;
          }

          if (elemA != (int32_T)muDoubleScalarFloor(elemA)) {
            emlrtIntegerCheckR2012b(elemA, &de_emlrtDCI, sp);
          }

          if (((int32_T)elemA < 1) || ((int32_T)elemA > NodesOnElementCG->size[0]))
          {
            emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
              NodesOnElementCG->size[0], &sn_emlrtBCI, sp);
          }

          b_nodeE2_size[0] = 1;
          b_nodeE2_size[1] = b_loop_ub;
          for (i2 = 0; i2 < b_loop_ub; i2++) {
            nodeE2_data[i2] = NodesOnElementCG->data[((int32_T)elemA +
              NodesOnElementCG->size[0] * i2) - 1];
          }

          lenn = intnnz(nodeE2_data, b_nodeE2_size);
          if ((lenn == 4) || (lenn == 10)) {
            e_loop_ub = 4;
            for (i2 = 0; i2 < 24; i2++) {
              nloopA_data[i2] = nloopT[i2];
            }
          } else if ((lenn == 6) || (lenn == 18)) {
            e_loop_ub = 5;
            for (i2 = 0; i2 < 40; i2++) {
              nloopA_data[i2] = nloopW[i2];
            }
          } else if ((lenn == 5) || (lenn == 14)) {
            e_loop_ub = 5;
            for (i2 = 0; i2 < 40; i2++) {
              nloopA_data[i2] = nloopP[i2];
            }
          } else {
            e_loop_ub = 6;
            for (i2 = 0; i2 < 48; i2++) {
              nloopA_data[i2] = nloopH[i2];
            }
          }

          /*  But the number of nodes on face won't change */
          i2 = numfn_data[locF];
          for (j = 0; j < i2; j++) {
            b_i = 1;
            if (1 > NodesOnElementCG->size[1]) {
              emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementCG->size[1],
                &pn_emlrtBCI, sp);
            }

            if (((int32_T)elemA < 1) || ((int32_T)elemA > NodesOnElementCG->
                 size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
                NodesOnElementCG->size[0], &nd_emlrtBCI, sp);
            }

            numA = NodesOnElementCG->data[(int32_T)elemA - 1];
            exitg2 = false;
            while ((!exitg2) && (b_i < nel2 + 1)) {
              if (j + 1 > i5) {
                emlrtDynamicBoundsCheckR2012b(j + 1, 1, i5, &kd_emlrtBCI, sp);
              }

              if (numA != nodeABCD_data[j]) {
                b_i++;
                if (b_i > NodesOnElementCG->size[1]) {
                  emlrtDynamicBoundsCheckR2012b(b_i, 1, NodesOnElementCG->size[1],
                    &jd_emlrtBCI, sp);
                }

                if (((int32_T)elemA < 1) || ((int32_T)elemA >
                     NodesOnElementCG->size[0])) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
                    NodesOnElementCG->size[0], &jd_emlrtBCI, sp);
                }

                numA = NodesOnElementCG->data[((int32_T)elemA +
                  NodesOnElementCG->size[0] * (b_i - 1)) - 1];
                if (*emlrtBreakCheckR2012bFlagVar != 0) {
                  emlrtBreakCheckR2012b(sp);
                }
              } else {
                exitg2 = true;
              }
            }

            nodeAABBCCDD[j] = (int8_T)b_i;
            if (*emlrtBreakCheckR2012bFlagVar != 0) {
              emlrtBreakCheckR2012b(sp);
            }
          }

          st.site = &emlrtRSI;
          b_loop_ub = numfn_data[locF];
          i2 = MorePBC->size[0];
          MorePBC->size[0] = b_loop_ub;
          emxEnsureCapacity_real_T(&st, MorePBC, i2, &yi_emlrtRTEI);
          for (i2 = 0; i2 < b_loop_ub; i2++) {
            MorePBC->data[i2] = nodeAABBCCDD[i2];
          }

          b_st.site = &cj_emlrtRSI;
          c_sort(&b_st, MorePBC);
          ib_size[0] = MorePBC->size[0];
          st.site = &emlrtRSI;
          b_loop_ub = numfn_data[locF];
          i2 = MorePBC->size[0];
          MorePBC->size[0] = b_loop_ub;
          emxEnsureCapacity_real_T(&st, MorePBC, i2, &cj_emlrtRTEI);
          for (i2 = 0; i2 < b_loop_ub; i2++) {
            MorePBC->data[i2] = nodeAABBCCDD[i2];
          }

          b_st.site = &cj_emlrtRSI;
          c_sort(&b_st, MorePBC);
          i2 = setPBC->size[0] * setPBC->size[1];
          setPBC->size[0] = 1;
          setPBC->size[1] = MorePBC->size[0];
          emxEnsureCapacity_real_T(sp, setPBC, i2, &ej_emlrtRTEI);
          b_loop_ub = MorePBC->size[0];
          for (i2 = 0; i2 < b_loop_ub; i2++) {
            setPBC->data[i2] = MorePBC->data[i2];
          }

          link_size[0] = 1;
          link_size[1] = ib_size[0];
          b_loop_ub = ib_size[0];
          for (i2 = 0; i2 < b_loop_ub; i2++) {
            link_data[i2] = setPBC->data[i2];
          }

          locFA = -1;
          lenn = 0;
          exitg2 = false;
          while ((!exitg2) && (lenn <= e_loop_ub - 1)) {
            if (lenn + 1 > numfn_size_idx_1) {
              emlrtDynamicBoundsCheckR2012b(lenn + 1, 1, numfn_size_idx_1,
                &bd_emlrtBCI, sp);
            }

            guard1 = false;
            if (i5 == numfn_data[lenn]) {
              i2 = lenn + 1;
              if (i2 > e_loop_ub) {
                emlrtDynamicBoundsCheckR2012b(i2, 1, e_loop_ub, &on_emlrtBCI, sp);
              }

              nloopA_size[0] = 1;
              nloopA_size[1] = i5;
              emlrtSizeEqCheckNDR2012b(link_size, nloopA_size, &j_emlrtECI, sp);
              NodesOnElementCG_size[0] = 1;
              NodesOnElementCG_size[1] = link_size[1];
              b_loop_ub = link_size[1];
              for (i2 = 0; i2 < b_loop_ub; i2++) {
                numABCD_data[i2] = link_data[i2] - (real_T)nloopA_data[lenn +
                  e_loop_ub * i2];
              }

              if (b_norm(numABCD_data, NodesOnElementCG_size) == 0.0) {
                locFA = lenn;
                exitg2 = true;
              } else {
                guard1 = true;
              }
            } else {
              guard1 = true;
            }

            if (guard1) {
              lenn++;
              if (*emlrtBreakCheckR2012bFlagVar != 0) {
                emlrtBreakCheckR2012b(sp);
              }
            }
          }

          if (((int32_T)elemA < 1) || ((int32_T)elemA > FacetsOnElement->size[0]))
          {
            emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
              FacetsOnElement->size[0], &nn_emlrtBCI, sp);
          }

          if (locFA + 1 < 1) {
            emlrtDynamicBoundsCheckR2012b(locFA + 1, 1, 6, &mn_emlrtBCI, sp);
          }

          if (FacetsOnElement->data[((int32_T)elemA + FacetsOnElement->size[0] *
               locFA) - 1] == 0.0) {
            /*  New edge, add to list */
            i2 = RegionOnElement->size[0];
            if (((int32_T)elemA < 1) || ((int32_T)elemA > i2)) {
              emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1, i2, &ln_emlrtBCI,
                sp);
            }

            elemB = elem + 1;
            lenn = locF;
            b_i2 = RegionOnElement->data[(int32_T)elemA - 1];
            if (b_i2 > RegionOnElement->data[elem]) {
              /* swap the order of L and R so that L is always larger material ID */
              b_i1 = RegionOnElement->data[elem];
              elemB = (int32_T)elemA;
              elemA = (real_T)elem + 1.0;
              lenn = locFA;
              locFA = locF;
            } else {
              b_i1 = b_i2;
              b_i2 = RegionOnElement->data[elem];
            }

            node = b_i2 * (b_i2 - 1.0) / 2.0 + b_i1;

            /*  ID for material pair (row=mat2, col=mat1) */
            if (node != (int32_T)muDoubleScalarFloor(node)) {
              emlrtIntegerCheckR2012b(node, &ce_emlrtDCI, sp);
            }

            if (((int32_T)node < 1) || ((int32_T)node > numEonF->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)node, 1, numEonF->size[0],
                &kn_emlrtBCI, sp);
            }

            numSI_tmp = numEonF->data[(int32_T)node - 1] + 1.0;
            (*numfac)++;
            if (((int32_T)node < 1) || ((int32_T)node > numEonF->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)node, 1, numEonF->size[0],
                &wc_emlrtBCI, sp);
            }

            numEonF->data[(int32_T)node - 1] = numSI_tmp;
            if ((elemB < 1) || (elemB > FacetsOnElement->size[0])) {
              emlrtDynamicBoundsCheckR2012b(elemB, 1, FacetsOnElement->size[0],
                &vc_emlrtBCI, sp);
            }

            FacetsOnElement->data[(elemB + FacetsOnElement->size[0] * lenn) - 1]
              = *numfac;
            if (((int32_T)elemA < 1) || ((int32_T)elemA > FacetsOnElement->size
                 [0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
                FacetsOnElement->size[0], &uc_emlrtBCI, sp);
            }

            FacetsOnElement->data[((int32_T)elemA + FacetsOnElement->size[0] *
              locFA) - 1] = *numfac;
            if (elemB > FacetsOnElementInt->size[0]) {
              emlrtDynamicBoundsCheckR2012b(elemB, 1, FacetsOnElementInt->size[0],
                &tc_emlrtBCI, sp);
            }

            FacetsOnElementInt->data[(elemB + FacetsOnElementInt->size[0] * lenn)
              - 1] = node;
            if (((int32_T)elemA < 1) || ((int32_T)elemA >
                 FacetsOnElementInt->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
                FacetsOnElementInt->size[0], &sc_emlrtBCI, sp);
            }

            FacetsOnElementInt->data[((int32_T)elemA + FacetsOnElementInt->size
              [0] * locFA) - 1] = node;
            if (((int32_T)*numfac < 1) || ((int32_T)*numfac >
                 ElementsOnFacet->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)*numfac, 1,
                ElementsOnFacet->size[0], &rc_emlrtBCI, sp);
            }

            ElementsOnFacet->data[(int32_T)*numfac - 1] = elemB;

            /* elemL */
            if (((int32_T)*numfac < 1) || ((int32_T)*numfac >
                 ElementsOnFacet->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)*numfac, 1,
                ElementsOnFacet->size[0], &nc_emlrtBCI, sp);
            }

            ElementsOnFacet->data[((int32_T)*numfac + ElementsOnFacet->size[0])
              - 1] = elemA;

            /* elemR */
            if (((int32_T)*numfac < 1) || ((int32_T)*numfac >
                 ElementsOnFacet->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)*numfac, 1,
                ElementsOnFacet->size[0], &lc_emlrtBCI, sp);
            }

            ElementsOnFacet->data[((int32_T)*numfac + ElementsOnFacet->size[0] *
              2) - 1] = (real_T)lenn + 1.0;

            /* facL */
            if (((int32_T)*numfac < 1) || ((int32_T)*numfac >
                 ElementsOnFacet->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)*numfac, 1,
                ElementsOnFacet->size[0], &kc_emlrtBCI, sp);
            }

            ElementsOnFacet->data[((int32_T)*numfac + ElementsOnFacet->size[0] *
              3) - 1] = (real_T)locFA + 1.0;

            /* facR */
            if (((int32_T)*numfac < 1) || ((int32_T)*numfac >
                 FacetsOnInterface->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)*numfac, 1,
                FacetsOnInterface->size[0], &ic_emlrtBCI, sp);
            }

            FacetsOnInterface->data[(int32_T)*numfac - 1] = node;
            st.site = &gd_emlrtRSI;
            if (muDoubleScalarIsNaN(usePBC)) {
              emlrtErrorWithMessageIdR2018a(&st, &mc_emlrtRTEI,
                "MATLAB:nologicalnan", "MATLAB:nologicalnan", 0);
            }

            if (usePBC != 0.0) {
              /*  Other element may be a different type */
              if (1.0 > nen) {
                b_loop_ub = 0;
              } else {
                if (1 > NodesOnElementCG->size[1]) {
                  emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementCG->size[1],
                    &in_emlrtBCI, sp);
                }

                if ((int32_T)nen > NodesOnElementCG->size[1]) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1,
                    NodesOnElementCG->size[1], &hn_emlrtBCI, sp);
                }

                b_loop_ub = (int32_T)nen;
              }

              if (((int32_T)elemA < 1) || ((int32_T)elemA >
                   NodesOnElementCG->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
                  NodesOnElementCG->size[0], &jn_emlrtBCI, sp);
              }

              b_nodeE2_size[0] = 1;
              b_nodeE2_size[1] = b_loop_ub;
              for (i2 = 0; i2 < b_loop_ub; i2++) {
                nodeE2_data[i2] = NodesOnElementCG->data[((int32_T)elemA +
                  NodesOnElementCG->size[0] * i2) - 1];
              }

              lenn = intnnz(nodeE2_data, b_nodeE2_size);
              if ((lenn == 4) || (lenn == 10)) {
                lenn = 5;
                for (i2 = 0; i2 < 5; i2++) {
                  numfnA_data[i2] = 3;
                }

                e_loop_ub = 4;
                for (i2 = 0; i2 < 24; i2++) {
                  nloopA_data[i2] = nloopT[i2];
                }
              } else if ((lenn == 6) || (lenn == 18)) {
                lenn = 5;
                for (i2 = 0; i2 < 5; i2++) {
                  numfnA_data[i2] = numfnW[i2];
                }

                e_loop_ub = 5;
                for (i2 = 0; i2 < 40; i2++) {
                  nloopA_data[i2] = nloopW[i2];
                }
              } else if ((lenn == 5) || (lenn == 14)) {
                lenn = 5;
                for (i2 = 0; i2 < 5; i2++) {
                  numfnA_data[i2] = numfnP[i2];
                }

                e_loop_ub = 5;
                for (i2 = 0; i2 < 40; i2++) {
                  nloopA_data[i2] = nloopP[i2];
                }
              } else {
                lenn = 6;
                for (i2 = 0; i2 < 6; i2++) {
                  numfnA_data[i2] = 4;
                }

                e_loop_ub = 6;
                for (i2 = 0; i2 < 48; i2++) {
                  nloopA_data[i2] = nloopH[i2];
                }
              }

              if ((locFA + 1 < 1) || (locFA + 1 > lenn)) {
                emlrtDynamicBoundsCheckR2012b(locFA + 1, 1, lenn, &gn_emlrtBCI,
                  sp);
              }

              /*  number of nodes on face */
              if (((int32_T)elemA < 1) || ((int32_T)elemA >
                   NodesOnElementCG->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
                  NodesOnElementCG->size[0], &fn_emlrtBCI, sp);
              }

              if ((locFA + 1 < 1) || (locFA + 1 > e_loop_ub)) {
                emlrtDynamicBoundsCheckR2012b(locFA + 1, 1, e_loop_ub,
                  &en_emlrtBCI, sp);
              }

              numPBC = numfnA_data[locFA];
              for (i2 = 0; i2 < numPBC; i2++) {
                i3 = nloopA_data[locFA + e_loop_ub * i2];
                if ((i3 < 1) || (i3 > NodesOnElementCG->size[1])) {
                  emlrtDynamicBoundsCheckR2012b(i3, 1, NodesOnElementCG->size[1],
                    &cb_emlrtBCI, sp);
                }
              }

              if ((locFA + 1 < 1) || (locFA + 1 > e_loop_ub)) {
                emlrtDynamicBoundsCheckR2012b(locFA + 1, 1, e_loop_ub,
                  &cn_emlrtBCI, sp);
              }

              if (((int32_T)elemA < 1) || ((int32_T)elemA >
                   NodesOnElementPBC->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
                  NodesOnElementPBC->size[0], &dn_emlrtBCI, sp);
              }

              link_size[0] = 1;
              link_size[1] = numPBC;
              for (i2 = 0; i2 < numPBC; i2++) {
                i3 = nloopA_data[locFA + e_loop_ub * i2];
                if ((i3 < 1) || (i3 > NodesOnElementPBC->size[1])) {
                  emlrtDynamicBoundsCheckR2012b(i3, 1, NodesOnElementPBC->size[1],
                    &x_emlrtBCI, sp);
                }

                link_data[i2] = NodesOnElementPBC->data[((int32_T)elemA +
                  NodesOnElementPBC->size[0] * (i3 - 1)) - 1];
              }

              st.site = &fd_emlrtRSI;
              if (elemB > NodesOnElementCG->size[0]) {
                emlrtDynamicBoundsCheckR2012b(elemB, 1, NodesOnElementCG->size[0],
                  &bn_emlrtBCI, &st);
              }

              for (i2 = 0; i2 < numPBC; i2++) {
                numABCD_data[i2] = nloopA_data[locFA + e_loop_ub * i2];
              }

              numA = NodesOnElementCG->data[((int32_T)elemA +
                NodesOnElementCG->size[0] * ((int32_T)numABCD_data[0] - 1)) - 1];
              b_loop_ub = NodesOnElementCG->size[1];
              nodeE2_size[0] = 1;
              nodeE2_size[1] = NodesOnElementCG->size[1];
              for (i2 = 0; i2 < b_loop_ub; i2++) {
                b_nodeE2_data[i2] = (NodesOnElementCG->data[(elemB +
                  NodesOnElementCG->size[0] * i2) - 1] == numA);
              }

              b_st.site = &uh_emlrtRSI;
              f_eml_find(&b_st, b_nodeE2_data, nodeE2_size, ii_data, ii_size);
              st.site = &ed_emlrtRSI;
              if (elemB > NodesOnElementCG->size[0]) {
                emlrtDynamicBoundsCheckR2012b(elemB, 1, NodesOnElementCG->size[0],
                  &an_emlrtBCI, &st);
              }

              for (i2 = 0; i2 < numPBC; i2++) {
                numABCD_data[i2] = nloopA_data[locFA + e_loop_ub * i2];
              }

              numA = NodesOnElementCG->data[((int32_T)elemA +
                NodesOnElementCG->size[0] * ((int32_T)numABCD_data[1] - 1)) - 1];
              b_loop_ub = NodesOnElementCG->size[1];
              nodeE2_size[0] = 1;
              nodeE2_size[1] = NodesOnElementCG->size[1];
              for (i2 = 0; i2 < b_loop_ub; i2++) {
                b_nodeE2_data[i2] = (NodesOnElementCG->data[(elemB +
                  NodesOnElementCG->size[0] * i2) - 1] == numA);
              }

              b_st.site = &uh_emlrtRSI;
              f_eml_find(&b_st, b_nodeE2_data, nodeE2_size, b_ii_data,
                         allnodes_size);
              st.site = &dd_emlrtRSI;
              if (elemB > NodesOnElementCG->size[0]) {
                emlrtDynamicBoundsCheckR2012b(elemB, 1, NodesOnElementCG->size[0],
                  &ym_emlrtBCI, &st);
              }

              for (i2 = 0; i2 < numPBC; i2++) {
                numABCD_data[i2] = nloopA_data[locFA + e_loop_ub * i2];
              }

              numA = NodesOnElementCG->data[((int32_T)elemA +
                NodesOnElementCG->size[0] * ((int32_T)numABCD_data[2] - 1)) - 1];
              b_loop_ub = NodesOnElementCG->size[1];
              nodeE2_size[0] = 1;
              nodeE2_size[1] = NodesOnElementCG->size[1];
              for (i2 = 0; i2 < b_loop_ub; i2++) {
                b_nodeE2_data[i2] = (NodesOnElementCG->data[(elemB +
                  NodesOnElementCG->size[0] * i2) - 1] == numA);
              }

              b_st.site = &uh_emlrtRSI;
              f_eml_find(&b_st, b_nodeE2_data, nodeE2_size, c_ii_data, tmp_size);
              if (elemB > NodesOnElementPBC->size[0]) {
                emlrtDynamicBoundsCheckR2012b(elemB, 1, NodesOnElementPBC->size
                  [0], &xm_emlrtBCI, sp);
              }

              b_loop_ub = ii_size[1];
              b_nodeE2_size[1] = ii_size[1];
              for (i2 = 0; i2 < b_loop_ub; i2++) {
                i3 = ii_data[i2];
                if ((i3 < 1) || (i3 > NodesOnElementPBC->size[1])) {
                  emlrtDynamicBoundsCheckR2012b(i3, 1, NodesOnElementPBC->size[1],
                    &j_emlrtBCI, sp);
                }

                nodeE2_data[i2] = NodesOnElementPBC->data[(elemB +
                  NodesOnElementPBC->size[0] * (ii_data[i2] - 1)) - 1];
              }

              if (elemB > NodesOnElementPBC->size[0]) {
                emlrtDynamicBoundsCheckR2012b(elemB, 1, NodesOnElementPBC->size
                  [0], &wm_emlrtBCI, sp);
              }

              notdone = allnodes_size[1];
              for (i2 = 0; i2 < notdone; i2++) {
                i3 = b_ii_data[i2];
                if ((i3 < 1) || (i3 > NodesOnElementPBC->size[1])) {
                  emlrtDynamicBoundsCheckR2012b(i3, 1, NodesOnElementPBC->size[1],
                    &i_emlrtBCI, sp);
                }

                nodeF2_data[i2] = NodesOnElementPBC->data[(elemB +
                  NodesOnElementPBC->size[0] * (b_ii_data[i2] - 1)) - 1];
              }

              if (elemB > NodesOnElementPBC->size[0]) {
                emlrtDynamicBoundsCheckR2012b(elemB, 1, NodesOnElementPBC->size
                  [0], &vm_emlrtBCI, sp);
              }

              c_loop_ub = tmp_size[1];
              for (i2 = 0; i2 < c_loop_ub; i2++) {
                i3 = c_ii_data[i2];
                if ((i3 < 1) || (i3 > NodesOnElementPBC->size[1])) {
                  emlrtDynamicBoundsCheckR2012b(i3, 1, NodesOnElementPBC->size[1],
                    &h_emlrtBCI, sp);
                }

                nodeG2_data[i2] = NodesOnElementPBC->data[(elemB +
                  NodesOnElementPBC->size[0] * (c_ii_data[i2] - 1)) - 1];
              }

              if (numfnA_data[locFA] == 4) {
                if (4 > numfnA_data[locFA]) {
                  emlrtDynamicBoundsCheckR2012b(4, 1, numfnA_data[locFA],
                    &tm_emlrtBCI, sp);
                }

                st.site = &cd_emlrtRSI;
                if (elemB > NodesOnElementCG->size[0]) {
                  emlrtDynamicBoundsCheckR2012b(elemB, 1, NodesOnElementCG->
                    size[0], &um_emlrtBCI, &st);
                }

                for (i2 = 0; i2 < numPBC; i2++) {
                  numABCD_data[i2] = nloopA_data[locFA + e_loop_ub * i2];
                }

                numA = NodesOnElementCG->data[((int32_T)elemA +
                  NodesOnElementCG->size[0] * ((int32_T)numABCD_data[3] - 1)) -
                  1];
                d_loop_ub = NodesOnElementCG->size[1];
                nodeE2_size[0] = 1;
                nodeE2_size[1] = NodesOnElementCG->size[1];
                for (i2 = 0; i2 < d_loop_ub; i2++) {
                  b_nodeE2_data[i2] = (NodesOnElementCG->data[(elemB +
                    NodesOnElementCG->size[0] * i2) - 1] == numA);
                }

                b_st.site = &uh_emlrtRSI;
                f_eml_find(&b_st, b_nodeE2_data, nodeE2_size, ii_data, ii_size);
                if (elemB > NodesOnElementPBC->size[0]) {
                  emlrtDynamicBoundsCheckR2012b(elemB, 1,
                    NodesOnElementPBC->size[0], &sm_emlrtBCI, sp);
                }

                nodeEEFFGGHH2_size[0] = 1;
                nodeEEFFGGHH2_size[1] = ((b_nodeE2_size[1] + allnodes_size[1]) +
                  tmp_size[1]) + ii_size[1];
                if (0 <= b_loop_ub - 1) {
                  memcpy(&nodeEEFFGGHH2_data[0], &nodeE2_data[0], b_loop_ub *
                         sizeof(real_T));
                }

                for (i2 = 0; i2 < notdone; i2++) {
                  nodeEEFFGGHH2_data[i2 + b_loop_ub] = nodeF2_data[i2];
                }

                for (i2 = 0; i2 < c_loop_ub; i2++) {
                  nodeEEFFGGHH2_data[(i2 + b_loop_ub) + notdone] =
                    nodeG2_data[i2];
                }

                d_loop_ub = ii_size[1];
                for (i2 = 0; i2 < d_loop_ub; i2++) {
                  i3 = ii_data[i2];
                  if ((i3 < 1) || (i3 > NodesOnElementPBC->size[1])) {
                    emlrtDynamicBoundsCheckR2012b(i3, 1, NodesOnElementPBC->
                      size[1], &f_emlrtBCI, sp);
                  }

                  nodeEEFFGGHH2_data[((i2 + b_loop_ub) + notdone) + c_loop_ub] =
                    NodesOnElementPBC->data[(elemB + NodesOnElementPBC->size[0] *
                    (ii_data[i2] - 1)) - 1];
                }
              } else {
                nodeEEFFGGHH2_size[0] = 1;
                d_loop_ub = ii_size[1];
                nodeEEFFGGHH2_size[1] = (ii_size[1] + allnodes_size[1]) +
                  tmp_size[1];
                if (0 <= d_loop_ub - 1) {
                  memcpy(&nodeEEFFGGHH2_data[0], &nodeE2_data[0], d_loop_ub *
                         sizeof(real_T));
                }

                for (i2 = 0; i2 < notdone; i2++) {
                  nodeEEFFGGHH2_data[i2 + b_loop_ub] = nodeF2_data[i2];
                }

                for (i2 = 0; i2 < c_loop_ub; i2++) {
                  nodeEEFFGGHH2_data[(i2 + b_loop_ub) + notdone] =
                    nodeG2_data[i2];
                }
              }

              /*  Condition: if the nodes opposite each other on the */
              /*  two elements adjoining the facet from the unzipped */
              /*  model (NodesOnElementPBC) are all different, then the */
              /*  two elements are on opposite sides of the domain and */
              /*  the facet needs to be cut */
              NodesOnElementCG_size[0] = 1;
              NodesOnElementCG_size[1] = numPBC;
              emlrtSizeEqCheckNDR2012b(NodesOnElementCG_size, nodeEEFFGGHH2_size,
                &i_emlrtECI, sp);
              b_link_size[0] = 1;
              b_link_size[1] = numPBC;
              for (i2 = 0; i2 < numPBC; i2++) {
                b_numABCD_data[i2] = (link_data[i2] != nodeEEFFGGHH2_data[i2]);
              }

              st.site = &bd_emlrtRSI;
              if (b_all(b_numABCD_data, b_link_size)) {
                /*  Make sure facet is NOT an interface in the */
                /*  unzipped mesh, because for triangles both nodes */
                /*  might be in PBCList but they are on different */
                /*  boundary surfaces */
                NodesOnElementCG_size[0] = 1;
                NodesOnElementCG_size[1] = numPBC;
                st.site = &ad_emlrtRSI;
                indexShapeCheck(&st, ElementsOnNodePBCNum->size[0],
                                NodesOnElementCG_size);
                for (i2 = 0; i2 < numPBC; i2++) {
                  d = link_data[i2];
                  if (d != (int32_T)muDoubleScalarFloor(d)) {
                    emlrtIntegerCheckR2012b(d, &e_emlrtDCI, sp);
                  }

                  if (((int32_T)d < 1) || ((int32_T)d >
                       ElementsOnNodePBCNum->size[0])) {
                    emlrtDynamicBoundsCheckR2012b((int32_T)d, 1,
                      ElementsOnNodePBCNum->size[0], &d_emlrtBCI, sp);
                  }

                  numABCD_data[i2] = ElementsOnNodePBCNum->data[(int32_T)d - 1];
                }

                st.site = &yc_emlrtRSI;
                b_st.site = &sj_emlrtRSI;
                c_st.site = &tj_emlrtRSI;
                d_st.site = &uj_emlrtRSI;
                e_st.site = &pl_emlrtRSI;
                f_st.site = &ql_emlrtRSI;
                g_st.site = &sl_emlrtRSI;
                g_st.site = &rl_emlrtRSI;
                numA = numABCD_data[0];
                h_st.site = &fk_emlrtRSI;
                for (lenn = 2; lenn <= numPBC; lenn++) {
                  d = numABCD_data[lenn - 1];
                  if (numA < d) {
                    numA = d;
                  }
                }

                if (numA < 1.0) {
                  setAll->size[0] = 1;
                  setAll->size[1] = 0;
                } else if (muDoubleScalarIsInf(numA) && (1.0 == numA)) {
                  i2 = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  setAll->size[1] = 1;
                  emxEnsureCapacity_real_T(sp, setAll, i2, &qi_emlrtRTEI);
                  setAll->data[0] = rtNaN;
                } else {
                  i2 = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  b_loop_ub = (int32_T)muDoubleScalarFloor(numA - 1.0);
                  setAll->size[1] = b_loop_ub + 1;
                  emxEnsureCapacity_real_T(sp, setAll, i2, &qi_emlrtRTEI);
                  for (i2 = 0; i2 <= b_loop_ub; i2++) {
                    setAll->data[i2] = (real_T)i2 + 1.0;
                  }
                }

                st.site = &yc_emlrtRSI;
                b_sparse_parenReference(&st, expl_temp.d, expl_temp.colidx,
                  expl_temp.rowidx, ElementsOnNodePBC_m, ElementsOnNodePBC_n,
                  setAll, link_data, link_size, t5_d, ii, idx, &notdone, &lenn,
                  &elemB);
                st.site = &yc_emlrtRSI;
                b_sparse_full(&st, t5_d, ii, idx, notdone, lenn,
                              ElementsOnNodeABCD);

                /*  Determine if edge is on domain interior or boundary */
                b_NodesOnPBC_size[0] = numPBC;
                for (i2 = 0; i2 < numPBC; i2++) {
                  b_numABCD_data[i2] = (numABCD_data[i2] > 1.0);
                }

                b_guard1 = false;
                st.site = &xc_emlrtRSI;
                if (all(b_numABCD_data, b_NodesOnPBC_size)) {
                  /*  Clean and fast way to intersect the 3 sets of elements, using */
                  /*  built-in Matlab functions; change ismembc to ismember if the */
                  /*  function is not in the standard package */
                  /*                          if isOctave */
                  d = ElementsOnNodePBCNum->data[(int32_T)
                    NodesOnElementPBC->data[((int32_T)elemA +
                    NodesOnElementPBC->size[0] * (nloopA_data[locFA] - 1)) - 1]
                    - 1];
                  if (1.0 > d) {
                    b_loop_ub = 0;
                  } else {
                    if (1 > ElementsOnNodeABCD->size[0]) {
                      emlrtDynamicBoundsCheckR2012b(1, 1,
                        ElementsOnNodeABCD->size[0], &rm_emlrtBCI, sp);
                    }

                    if (((int32_T)d < 1) || ((int32_T)d >
                         ElementsOnNodeABCD->size[0])) {
                      emlrtDynamicBoundsCheckR2012b((int32_T)d, 1,
                        ElementsOnNodeABCD->size[0], &qm_emlrtBCI, sp);
                    }

                    b_loop_ub = (int32_T)d;
                  }

                  i2 = MorePBC->size[0];
                  MorePBC->size[0] = b_loop_ub;
                  emxEnsureCapacity_real_T(sp, MorePBC, i2, &vk_emlrtRTEI);
                  for (i2 = 0; i2 < b_loop_ub; i2++) {
                    MorePBC->data[i2] = ElementsOnNodeABCD->data[i2];
                  }

                  i2 = numfn_data[locF];
                  for (j = 0; j <= i2 - 2; j++) {
                    if (j + 2 > numfnA_data[locFA]) {
                      emlrtDynamicBoundsCheckR2012b(j + 2, 1, numfnA_data[locFA],
                        &pm_emlrtBCI, sp);
                    }

                    d = ElementsOnNodePBCNum->data[(int32_T)
                      NodesOnElementPBC->data[((int32_T)elemA +
                      NodesOnElementPBC->size[0] * (nloopA_data[locFA +
                      e_loop_ub * (j + 1)] - 1)) - 1] - 1];
                    if (1.0 > d) {
                      b_loop_ub = 0;
                    } else {
                      if (1 > ElementsOnNodeABCD->size[0]) {
                        emlrtDynamicBoundsCheckR2012b(1, 1,
                          ElementsOnNodeABCD->size[0], &om_emlrtBCI, sp);
                      }

                      if (((int32_T)d < 1) || ((int32_T)d >
                           ElementsOnNodeABCD->size[0])) {
                        emlrtDynamicBoundsCheckR2012b((int32_T)d, 1,
                          ElementsOnNodeABCD->size[0], &nm_emlrtBCI, sp);
                      }

                      b_loop_ub = (int32_T)d;
                    }

                    st.site = &wc_emlrtRSI;
                    if (j + 2 > ElementsOnNodeABCD->size[1]) {
                      emlrtDynamicBoundsCheckR2012b(j + 2, 1,
                        ElementsOnNodeABCD->size[1], &mm_emlrtBCI, &st);
                    }

                    i3 = t5_d->size[0];
                    t5_d->size[0] = b_loop_ub;
                    emxEnsureCapacity_real_T(&st, t5_d, i3, &wk_emlrtRTEI);
                    for (i3 = 0; i3 < b_loop_ub; i3++) {
                      t5_d->data[i3] = ElementsOnNodeABCD->data[i3 +
                        ElementsOnNodeABCD->size[0] * (j + 1)];
                    }

                    b_st.site = &ng_emlrtRSI;
                    b_local_ismember(&b_st, MorePBC, t5_d, b_NodesOnPBCnum);
                    end = b_NodesOnPBCnum->size[0] - 1;
                    trueCount = 0;
                    for (b_i = 0; b_i <= end; b_i++) {
                      if (b_NodesOnPBCnum->data[b_i]) {
                        trueCount++;
                      }
                    }

                    partialTrueCount = 0;
                    for (b_i = 0; b_i <= end; b_i++) {
                      if (b_NodesOnPBCnum->data[b_i]) {
                        if ((b_i + 1 < 1) || (b_i + 1 > MorePBC->size[0])) {
                          emlrtDynamicBoundsCheckR2012b(b_i + 1, 1,
                            MorePBC->size[0], &emlrtBCI, sp);
                        }

                        MorePBC->data[partialTrueCount] = MorePBC->data[b_i];
                        partialTrueCount++;
                      }
                    }

                    i3 = MorePBC->size[0];
                    MorePBC->size[0] = trueCount;
                    emxEnsureCapacity_real_T(sp, MorePBC, i3, &mg_emlrtRTEI);
                    if (*emlrtBreakCheckR2012bFlagVar != 0) {
                      emlrtBreakCheckR2012b(sp);
                    }
                  }

                  /*                          else */
                  /*                              twoelem = ElementsOnNodeABCD(1:numABCD(1),1); */
                  /*                              for j = 2:numfacnod */
                  /*                                  twoelem = twoelem(ismembc(twoelem,ElementsOnNodeABCD(1:numABCD(j),j))); */
                  /*                              end */
                  /*                          end */
                  if (MorePBC->size[0] == 2) {
                    /*  element interface */
                    /*                  if usePBC % Don't reconsider new ghost elements */
                    /*                      if elemA > numel */
                    /*                          facIL = 0; */
                    /*                      end */
                    /*                  end */
                    notdone = 0;
                  } else {
                    /*  domain boundary */
                    b_guard1 = true;
                  }
                } else {
                  /*  domain boundary */
                  b_guard1 = true;
                }

                if (b_guard1) {
                  notdone = 1;
                  if (((int32_T)node < 1) || ((int32_T)node > numEonPBC->size[0]))
                  {
                    emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
                      numEonPBC->size[0], &lm_emlrtBCI, sp);
                  }

                  numSI_tmp = numEonPBC->data[(int32_T)node - 1] + 1.0;
                  if (((int32_T)node < 1) || ((int32_T)node > numEonPBC->size[0]))
                  {
                    emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
                      numEonPBC->size[0], &c_emlrtBCI, sp);
                  }

                  numEonPBC->data[(int32_T)node - 1] = numSI_tmp;
                  numFPBC++;
                  if (((int32_T)*numfac < 1) || ((int32_T)*numfac >
                       FacetsOnPBC->size[0])) {
                    emlrtDynamicBoundsCheckR2012b((int32_T)*numfac, 1,
                      FacetsOnPBC->size[0], &b_emlrtBCI, sp);
                  }

                  FacetsOnPBC->data[(int32_T)*numfac - 1] = node;
                }
              } else {
                notdone = 0;
              }
            } else {
              notdone = 0;
            }

            /*  Assign nodal edge pairs */
            if (b_i1 != (int32_T)muDoubleScalarFloor(b_i1)) {
              emlrtIntegerCheckR2012b(b_i1, &k_emlrtDCI, sp);
            }

            if (((int32_T)b_i1 < 1) || ((int32_T)b_i1 > InterTypes->size[1])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)b_i1, 1, InterTypes->size[1],
                &ec_emlrtBCI, sp);
            }

            if (b_i2 != (int32_T)muDoubleScalarFloor(b_i2)) {
              emlrtIntegerCheckR2012b(b_i2, &k_emlrtDCI, sp);
            }

            if (((int32_T)b_i2 < 1) || ((int32_T)b_i2 > InterTypes->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)b_i2, 1, InterTypes->size[0],
                &ec_emlrtBCI, sp);
            }

            d = InterTypes->data[((int32_T)b_i2 + InterTypes->size[0] *
                                  ((int32_T)b_i1 - 1)) - 1];
            if (d > 0.0) {
              old = true;
            } else {
              st.site = &vc_emlrtRSI;
              if (notdone != 0) {
                old = true;
              } else {
                old = false;
              }
            }

            i2 = numfn_data[locF];
            for (j = 0; j < i2; j++) {
              if (j + 1 > i5) {
                emlrtDynamicBoundsCheckR2012b(j + 1, 1, i5, &yb_emlrtBCI, sp);
              }

              d1 = nodeABCD_data[j];
              if (((int32_T)d1 < 1) || ((int32_T)d1 > FacetsOnNodeNum->size[0]))
              {
                emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1,
                  FacetsOnNodeNum->size[0], &ac_emlrtBCI, sp);
              }

              numA = FacetsOnNodeNum->data[(int32_T)d1 - 1] + 1.0;
              if (j + 1 > i5) {
                emlrtDynamicBoundsCheckR2012b(j + 1, 1, i5, &rb_emlrtBCI, sp);
              }

              if (((int32_T)d1 < 1) || ((int32_T)d1 > FacetsOnNodeNum->size[0]))
              {
                emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1,
                  FacetsOnNodeNum->size[0], &sb_emlrtBCI, sp);
              }

              FacetsOnNodeNum->data[(int32_T)d1 - 1] = FacetsOnNodeNum->data
                [(int32_T)nodeABCD_data[j] - 1] + 1.0;
              nri++;
              if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1]))
              {
                emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1,
                  FacetsOnNodeInd->size[1], &am_emlrtBCI, sp);
              }

              i3 = 5 * ((int32_T)nri - 1);
              FacetsOnNodeInd->data[i3] = numA;
              if (j + 1 > i5) {
                emlrtDynamicBoundsCheckR2012b(j + 1, 1, i5, &lb_emlrtBCI, sp);
              }

              FacetsOnNodeInd->data[i3 + 1] = nodeABCD_data[j];
              FacetsOnNodeInd->data[i3 + 2] = *numfac;
              FacetsOnNodeInd->data[i3 + 3] = node;
              if (old) {
                if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size
                     [1])) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1,
                    FacetsOnNodeInd->size[1], &jb_emlrtBCI, sp);
                }

                FacetsOnNodeInd->data[i3 + 4] = 1.0;
              }

              /*                      FacetsOnNode(facnumABCD,nodeABCD(j)) = numfac; */
              /*                      FacetsOnNodeInt(facnumABCD,nodeABCD(j)) = regI; */
              /*                      if nodecut */
              /*                          FacetsOnNodeCut(facnumABCD,nodeABCD(j)) = 1; */
              /*                      end */
              if (*emlrtBreakCheckR2012bFlagVar != 0) {
                emlrtBreakCheckR2012b(sp);
              }
            }

            if (nel > nel2) {
              /*  Add in the nodes along the edge too, but not the face */
              i6 = numfn_data[locF];
              i2 = i6 << 1;
              if (i2 > nloop_size_idx_1) {
                emlrtDynamicBoundsCheckR2012b(i2, 1, nloop_size_idx_1,
                  &im_emlrtBCI, sp);
              }

              if (locF + 1 > nloop_size_idx_0) {
                emlrtDynamicBoundsCheckR2012b(locF + 1, 1, nloop_size_idx_0,
                  &jm_emlrtBCI, sp);
              }

              if (elem + 1 > NodesOnElementCG->size[0]) {
                emlrtDynamicBoundsCheckR2012b(elem + 1, 1,
                  NodesOnElementCG->size[0], &km_emlrtBCI, sp);
              }

              b_loop_ub = i2 - i6;
              for (i3 = 0; i3 < b_loop_ub; i3++) {
                b_i = nloop_data[locF + nloop_size_idx_0 * (i6 + i3)];
                if ((b_i < 1) || (b_i > NodesOnElementCG->size[1])) {
                  emlrtDynamicBoundsCheckR2012b(b_i, 1, NodesOnElementCG->size[1],
                    &nb_emlrtBCI, sp);
                }

                nodeABCD_data[i3] = NodesOnElementCG->data[elem +
                  NodesOnElementCG->size[0] * (b_i - 1)];
              }

              if (((int32_T)b_i1 < 1) || ((int32_T)b_i1 > InterTypes->size[1]))
              {
                emlrtDynamicBoundsCheckR2012b((int32_T)b_i1, 1, InterTypes->
                  size[1], &mb_emlrtBCI, sp);
              }

              if (((int32_T)b_i2 < 1) || ((int32_T)b_i2 > InterTypes->size[0]))
              {
                emlrtDynamicBoundsCheckR2012b((int32_T)b_i2, 1, InterTypes->
                  size[0], &mb_emlrtBCI, sp);
              }

              old = (d > 0.0);
              i3 = numfn_data[locF];
              for (j = 0; j < i3; j++) {
                b_i = i2 - i5;
                if (j + 1 > b_i) {
                  emlrtDynamicBoundsCheckR2012b(j + 1, 1, b_i, &gb_emlrtBCI, sp);
                }

                d = nodeABCD_data[j];
                if (d != (int32_T)muDoubleScalarFloor(d)) {
                  emlrtIntegerCheckR2012b(d, &g_emlrtDCI, sp);
                }

                if (((int32_T)d < 1) || ((int32_T)d > FacetsOnNodeNum->size[0]))
                {
                  emlrtDynamicBoundsCheckR2012b((int32_T)d, 1,
                    FacetsOnNodeNum->size[0], &hb_emlrtBCI, sp);
                }

                lenn = (int32_T)nodeABCD_data[j] - 1;
                numA = FacetsOnNodeNum->data[lenn] + 1.0;
                if (j + 1 > b_i) {
                  emlrtDynamicBoundsCheckR2012b(j + 1, 1, b_i, &db_emlrtBCI, sp);
                }

                d = nodeABCD_data[j];
                if (d != (int32_T)muDoubleScalarFloor(d)) {
                  emlrtIntegerCheckR2012b(d, &f_emlrtDCI, sp);
                }

                if (((int32_T)d < 1) || ((int32_T)d > FacetsOnNodeNum->size[0]))
                {
                  emlrtDynamicBoundsCheckR2012b((int32_T)d, 1,
                    FacetsOnNodeNum->size[0], &eb_emlrtBCI, sp);
                }

                FacetsOnNodeNum->data[lenn]++;
                nri++;
                if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size
                     [1])) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1,
                    FacetsOnNodeInd->size[1], &yl_emlrtBCI, sp);
                }

                trueCount = 5 * ((int32_T)nri - 1);
                FacetsOnNodeInd->data[trueCount] = numA;
                if (j + 1 > b_i) {
                  emlrtDynamicBoundsCheckR2012b(j + 1, 1, b_i, &ab_emlrtBCI, sp);
                }

                FacetsOnNodeInd->data[trueCount + 1] = nodeABCD_data[j];
                FacetsOnNodeInd->data[trueCount + 2] = *numfac;
                FacetsOnNodeInd->data[trueCount + 3] = node;
                if (old) {
                  if (((int32_T)nri < 1) || ((int32_T)nri >
                       FacetsOnNodeInd->size[1])) {
                    emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1,
                      FacetsOnNodeInd->size[1], &u_emlrtBCI, sp);
                  }

                  FacetsOnNodeInd->data[trueCount + 4] = 1.0;
                }

                /*                          FacetsOnNode(facnumABCD,nodeABCD(j)) = numfac; */
                /*                          FacetsOnNodeInt(facnumABCD,nodeABCD(j)) = regI; */
                /*                          if nodecut */
                /*                              FacetsOnNodeCut(facnumABCD,nodeABCD(j)) = 1; */
                /*                          end */
                if (*emlrtBreakCheckR2012bFlagVar != 0) {
                  emlrtBreakCheckR2012b(sp);
                }
              }
            }
          }
        } else {
          /* domain boundary, add to SurfaceF */
          st.site = &uc_emlrtRSI;
          if (muDoubleScalarIsNaN(usePBC)) {
            emlrtErrorWithMessageIdR2018a(&st, &mc_emlrtRTEI,
              "MATLAB:nologicalnan", "MATLAB:nologicalnan", 0);
          }

          if (usePBC != 0.0) {
            st.site = &tc_emlrtRSI;
            g_error(&st);
          }

          numSI_tmp = *numEonB + 1.0;
          (*numEonB)++;
          if (elem + 1 > FacetsOnElement->size[0]) {
            emlrtDynamicBoundsCheckR2012b(elem + 1, 1, FacetsOnElement->size[0],
              &be_emlrtBCI, sp);
          }

          FacetsOnElement->data[elem + FacetsOnElement->size[0] * locF] =
            -numSI_tmp;
          if (((int32_T)numSI_tmp < 1) || ((int32_T)numSI_tmp >
               ElementsOnBoundary->size[0])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)numSI_tmp, 1,
              ElementsOnBoundary->size[0], &ae_emlrtBCI, sp);
          }

          ElementsOnBoundary->data[(int32_T)numSI_tmp - 1] = (real_T)elem + 1.0;
          if (((int32_T)numSI_tmp < 1) || ((int32_T)numSI_tmp >
               ElementsOnBoundary->size[0])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)numSI_tmp, 1,
              ElementsOnBoundary->size[0], &yd_emlrtBCI, sp);
          }

          ElementsOnBoundary->data[((int32_T)numSI_tmp +
            ElementsOnBoundary->size[0]) - 1] = (real_T)locF + 1.0;

          /*  Assign nodal edge pairs */
          i2 = numfn_data[locF];
          for (j = 0; j < i2; j++) {
            if (j + 1 > i5) {
              emlrtDynamicBoundsCheckR2012b(j + 1, 1, i5, &wd_emlrtBCI, sp);
            }

            d = nodeABCD_data[j];
            if (((int32_T)d < 1) || ((int32_T)d > FacetsOnNodeNum->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, FacetsOnNodeNum->
                size[0], &xd_emlrtBCI, sp);
            }

            numA = FacetsOnNodeNum->data[(int32_T)d - 1] + 1.0;
            if (j + 1 > i5) {
              emlrtDynamicBoundsCheckR2012b(j + 1, 1, i5, &td_emlrtBCI, sp);
            }

            if (((int32_T)d < 1) || ((int32_T)d > FacetsOnNodeNum->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, FacetsOnNodeNum->
                size[0], &ud_emlrtBCI, sp);
            }

            FacetsOnNodeNum->data[(int32_T)d - 1] = FacetsOnNodeNum->data
              [(int32_T)nodeABCD_data[j] - 1] + 1.0;
            nri++;
            if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1]))
            {
              emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1,
                FacetsOnNodeInd->size[1], &xl_emlrtBCI, sp);
            }

            i3 = 5 * ((int32_T)nri - 1);
            FacetsOnNodeInd->data[i3] = numA;
            if (j + 1 > i5) {
              emlrtDynamicBoundsCheckR2012b(j + 1, 1, i5, &od_emlrtBCI, sp);
            }

            FacetsOnNodeInd->data[i3 + 1] = nodeABCD_data[j];
            FacetsOnNodeInd->data[i3 + 2] = numSI_tmp;
            FacetsOnNodeInd->data[i3 + 3] = -1.0;

            /*                  FacetsOnNode(facnumABCD,nodeABCD(j)) = numSI; */
            /*                  FacetsOnNodeInt(facnumABCD,nodeABCD(j)) = -1; */
            if (*emlrtBreakCheckR2012bFlagVar != 0) {
              emlrtBreakCheckR2012b(sp);
            }
          }

          if (nel > nel2) {
            /*  Add in the nodes along the edge too, but not the face */
            i6 = numfn_data[locF];
            i2 = i6 << 1;
            if (i2 > nloop_size_idx_1) {
              emlrtDynamicBoundsCheckR2012b(i2, 1, nloop_size_idx_1,
                &fm_emlrtBCI, sp);
            }

            if (locF + 1 > nloop_size_idx_0) {
              emlrtDynamicBoundsCheckR2012b(locF + 1, 1, nloop_size_idx_0,
                &gm_emlrtBCI, sp);
            }

            if (elem + 1 > NodesOnElementCG->size[0]) {
              emlrtDynamicBoundsCheckR2012b(elem + 1, 1, NodesOnElementCG->size
                [0], &hm_emlrtBCI, sp);
            }

            b_loop_ub = i2 - i6;
            for (i3 = 0; i3 < b_loop_ub; i3++) {
              b_i = nloop_data[locF + nloop_size_idx_0 * (i6 + i3)];
              if ((b_i < 1) || (b_i > NodesOnElementCG->size[1])) {
                emlrtDynamicBoundsCheckR2012b(b_i, 1, NodesOnElementCG->size[1],
                  &sd_emlrtBCI, sp);
              }

              nodeABCD_data[i3] = NodesOnElementCG->data[elem +
                NodesOnElementCG->size[0] * (b_i - 1)];
            }

            i3 = numfn_data[locF];
            for (j = 0; j < i3; j++) {
              b_i = i2 - i5;
              if (j + 1 > b_i) {
                emlrtDynamicBoundsCheckR2012b(j + 1, 1, b_i, &ld_emlrtBCI, sp);
              }

              d = nodeABCD_data[j];
              if (d != (int32_T)muDoubleScalarFloor(d)) {
                emlrtIntegerCheckR2012b(d, &u_emlrtDCI, sp);
              }

              if (((int32_T)d < 1) || ((int32_T)d > FacetsOnNodeNum->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)d, 1,
                  FacetsOnNodeNum->size[0], &md_emlrtBCI, sp);
              }

              lenn = (int32_T)nodeABCD_data[j] - 1;
              numA = FacetsOnNodeNum->data[lenn] + 1.0;
              if (j + 1 > b_i) {
                emlrtDynamicBoundsCheckR2012b(j + 1, 1, b_i, &hd_emlrtBCI, sp);
              }

              d = nodeABCD_data[j];
              if (d != (int32_T)muDoubleScalarFloor(d)) {
                emlrtIntegerCheckR2012b(d, &t_emlrtDCI, sp);
              }

              if (((int32_T)d < 1) || ((int32_T)d > FacetsOnNodeNum->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)d, 1,
                  FacetsOnNodeNum->size[0], &id_emlrtBCI, sp);
              }

              FacetsOnNodeNum->data[lenn]++;
              nri++;
              if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1]))
              {
                emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1,
                  FacetsOnNodeInd->size[1], &wl_emlrtBCI, sp);
              }

              trueCount = 5 * ((int32_T)nri - 1);
              FacetsOnNodeInd->data[trueCount] = numA;
              if (j + 1 > b_i) {
                emlrtDynamicBoundsCheckR2012b(j + 1, 1, b_i, &gd_emlrtBCI, sp);
              }

              FacetsOnNodeInd->data[trueCount + 1] = nodeABCD_data[j];
              FacetsOnNodeInd->data[trueCount + 2] = numSI_tmp;
              FacetsOnNodeInd->data[trueCount + 3] = -1.0;

              /*                      FacetsOnNode(facnumABCD,nodeABCD(j)) = numSI; */
              /*                      FacetsOnNodeInt(facnumABCD,nodeABCD(j)) = -1; */
              if (*emlrtBreakCheckR2012bFlagVar != 0) {
                emlrtBreakCheckR2012b(sp);
              }
            }
          }
        }

        /* facIL */
      }

      /* facet not identified */
      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    /* locF */
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  emxFreeStruct_sparse1(&expl_temp);
  emxFree_real_T(&ElementsOnNodeABCD);

  /* elem */
  if (1.0 > *numfac) {
    b_loop_ub = 0;
  } else {
    if (1 > ElementsOnFacet->size[0]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, ElementsOnFacet->size[0], &em_emlrtBCI,
        sp);
    }

    if (((int32_T)*numfac < 1) || ((int32_T)*numfac > ElementsOnFacet->size[0]))
    {
      emlrtDynamicBoundsCheckR2012b((int32_T)*numfac, 1, ElementsOnFacet->size[0],
        &dm_emlrtBCI, sp);
    }

    b_loop_ub = (int32_T)*numfac;
  }

  i = PBCList2->size[0] * PBCList2->size[1];
  PBCList2->size[0] = b_loop_ub;
  PBCList2->size[1] = 4;
  emxEnsureCapacity_real_T(sp, PBCList2, i, &fi_emlrtRTEI);
  for (i = 0; i < b_loop_ub; i++) {
    PBCList2->data[i] = ElementsOnFacet->data[i];
  }

  for (i = 0; i < b_loop_ub; i++) {
    PBCList2->data[i + PBCList2->size[0]] = ElementsOnFacet->data[i +
      ElementsOnFacet->size[0]];
  }

  for (i = 0; i < b_loop_ub; i++) {
    PBCList2->data[i + PBCList2->size[0] * 2] = ElementsOnFacet->data[i +
      ElementsOnFacet->size[0] * 2];
  }

  for (i = 0; i < b_loop_ub; i++) {
    PBCList2->data[i + PBCList2->size[0] * 3] = ElementsOnFacet->data[i +
      ElementsOnFacet->size[0] * 3];
  }

  i = ElementsOnFacet->size[0] * ElementsOnFacet->size[1];
  ElementsOnFacet->size[0] = PBCList2->size[0];
  ElementsOnFacet->size[1] = 4;
  emxEnsureCapacity_real_T(sp, ElementsOnFacet, i, &gi_emlrtRTEI);
  b_loop_ub = PBCList2->size[0] * PBCList2->size[1];
  for (i = 0; i < b_loop_ub; i++) {
    ElementsOnFacet->data[i] = PBCList2->data[i];
  }

  emxFree_real_T(&PBCList2);
  if (1.0 > *numEonB) {
    b_loop_ub = 0;
  } else {
    if (1 > ElementsOnBoundary->size[0]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, ElementsOnBoundary->size[0],
        &cm_emlrtBCI, sp);
    }

    if (((int32_T)*numEonB < 1) || ((int32_T)*numEonB > ElementsOnBoundary->
         size[0])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)*numEonB, 1,
        ElementsOnBoundary->size[0], &bm_emlrtBCI, sp);
    }

    b_loop_ub = (int32_T)*numEonB;
  }

  i = b_MPCList->size[0] * b_MPCList->size[1];
  b_MPCList->size[0] = b_loop_ub;
  b_MPCList->size[1] = 2;
  emxEnsureCapacity_real_T(sp, b_MPCList, i, &hi_emlrtRTEI);
  for (i = 0; i < b_loop_ub; i++) {
    b_MPCList->data[i] = ElementsOnBoundary->data[i];
  }

  for (i = 0; i < b_loop_ub; i++) {
    b_MPCList->data[i + b_MPCList->size[0]] = ElementsOnBoundary->data[i +
      ElementsOnBoundary->size[0]];
  }

  i = ElementsOnBoundary->size[0] * ElementsOnBoundary->size[1];
  ElementsOnBoundary->size[0] = b_MPCList->size[0];
  ElementsOnBoundary->size[1] = 2;
  emxEnsureCapacity_real_T(sp, ElementsOnBoundary, i, &ii_emlrtRTEI);
  b_loop_ub = b_MPCList->size[0] * b_MPCList->size[1];
  for (i = 0; i < b_loop_ub; i++) {
    ElementsOnBoundary->data[i] = b_MPCList->data[i];
  }

  emxFree_real_T(&b_MPCList);
  if (1 > (int32_T)nri) {
    b_loop_ub = 0;
    notdone = 0;
    c_loop_ub = 0;
  } else {
    if (1 > FacetsOnNodeInd->size[1]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, FacetsOnNodeInd->size[1], &vl_emlrtBCI,
        sp);
    }

    if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, FacetsOnNodeInd->size[1],
        &ul_emlrtBCI, sp);
    }

    b_loop_ub = (int32_T)nri;
    if (1 > FacetsOnNodeInd->size[1]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, FacetsOnNodeInd->size[1], &tl_emlrtBCI,
        sp);
    }

    if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, FacetsOnNodeInd->size[1],
        &sl_emlrtBCI, sp);
    }

    notdone = (int32_T)nri;
    if (1 > FacetsOnNodeInd->size[1]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, FacetsOnNodeInd->size[1], &rl_emlrtBCI,
        sp);
    }

    if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, FacetsOnNodeInd->size[1],
        &ql_emlrtBCI, sp);
    }

    c_loop_ub = (int32_T)nri;
  }

  i = setPBC->size[0] * setPBC->size[1];
  setPBC->size[0] = 1;
  setPBC->size[1] = b_loop_ub;
  emxEnsureCapacity_real_T(sp, setPBC, i, &ji_emlrtRTEI);
  for (i = 0; i < b_loop_ub; i++) {
    setPBC->data[i] = FacetsOnNodeInd->data[5 * i];
  }

  i = setAll->size[0] * setAll->size[1];
  setAll->size[0] = 1;
  setAll->size[1] = notdone;
  emxEnsureCapacity_real_T(sp, setAll, i, &ki_emlrtRTEI);
  for (i = 0; i < notdone; i++) {
    setAll->data[i] = FacetsOnNodeInd->data[5 * i + 1];
  }

  i = NodeRegInd->size[0] * NodeRegInd->size[1];
  NodeRegInd->size[0] = 1;
  NodeRegInd->size[1] = c_loop_ub;
  emxEnsureCapacity_real_T(sp, NodeRegInd, i, &li_emlrtRTEI);
  for (i = 0; i < c_loop_ub; i++) {
    NodeRegInd->data[i] = FacetsOnNodeInd->data[5 * i + 2];
  }

  st.site = &sc_emlrtRSI;
  b_sparse(&st, setPBC, setAll, NodeRegInd, FacetsOnNode);

  /*  for each node, which other node is connected to it by an edge; value is the nodal ID of the connecting node */
  if (1 > (int32_T)nri) {
    b_loop_ub = 0;
    notdone = 0;
    c_loop_ub = 0;
  } else {
    if (1 > FacetsOnNodeInd->size[1]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, FacetsOnNodeInd->size[1], &pl_emlrtBCI,
        sp);
    }

    if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, FacetsOnNodeInd->size[1],
        &ol_emlrtBCI, sp);
    }

    b_loop_ub = (int32_T)nri;
    if (1 > FacetsOnNodeInd->size[1]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, FacetsOnNodeInd->size[1], &nl_emlrtBCI,
        sp);
    }

    if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, FacetsOnNodeInd->size[1],
        &ml_emlrtBCI, sp);
    }

    notdone = (int32_T)nri;
    if (1 > FacetsOnNodeInd->size[1]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, FacetsOnNodeInd->size[1], &ll_emlrtBCI,
        sp);
    }

    if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, FacetsOnNodeInd->size[1],
        &kl_emlrtBCI, sp);
    }

    c_loop_ub = (int32_T)nri;
  }

  i = setPBC->size[0] * setPBC->size[1];
  setPBC->size[0] = 1;
  setPBC->size[1] = b_loop_ub;
  emxEnsureCapacity_real_T(sp, setPBC, i, &mi_emlrtRTEI);
  for (i = 0; i < b_loop_ub; i++) {
    setPBC->data[i] = FacetsOnNodeInd->data[5 * i];
  }

  i = setAll->size[0] * setAll->size[1];
  setAll->size[0] = 1;
  setAll->size[1] = notdone;
  emxEnsureCapacity_real_T(sp, setAll, i, &ni_emlrtRTEI);
  for (i = 0; i < notdone; i++) {
    setAll->data[i] = FacetsOnNodeInd->data[5 * i + 1];
  }

  i = NodeRegInd->size[0] * NodeRegInd->size[1];
  NodeRegInd->size[0] = 1;
  NodeRegInd->size[1] = c_loop_ub;
  emxEnsureCapacity_real_T(sp, NodeRegInd, i, &oi_emlrtRTEI);
  for (i = 0; i < c_loop_ub; i++) {
    NodeRegInd->data[i] = FacetsOnNodeInd->data[5 * i + 4];
  }

  st.site = &rc_emlrtRSI;
  b_sparse(&st, setPBC, setAll, NodeRegInd, FacetsOnNodeCut);

  /*  flag for whether that edge is being cut between two nodes; 1 for cut, 0 for retain. */
  if (1 > (int32_T)nri) {
    b_loop_ub = 0;
    notdone = 0;
    c_loop_ub = 0;
  } else {
    if (1 > FacetsOnNodeInd->size[1]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, FacetsOnNodeInd->size[1], &jl_emlrtBCI,
        sp);
    }

    if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, FacetsOnNodeInd->size[1],
        &il_emlrtBCI, sp);
    }

    b_loop_ub = (int32_T)nri;
    if (1 > FacetsOnNodeInd->size[1]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, FacetsOnNodeInd->size[1], &hl_emlrtBCI,
        sp);
    }

    if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, FacetsOnNodeInd->size[1],
        &gl_emlrtBCI, sp);
    }

    notdone = (int32_T)nri;
    if (1 > FacetsOnNodeInd->size[1]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, FacetsOnNodeInd->size[1], &fl_emlrtBCI,
        sp);
    }

    if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, FacetsOnNodeInd->size[1],
        &el_emlrtBCI, sp);
    }

    c_loop_ub = (int32_T)nri;
  }

  i = setPBC->size[0] * setPBC->size[1];
  setPBC->size[0] = 1;
  setPBC->size[1] = b_loop_ub;
  emxEnsureCapacity_real_T(sp, setPBC, i, &pi_emlrtRTEI);
  for (i = 0; i < b_loop_ub; i++) {
    setPBC->data[i] = FacetsOnNodeInd->data[5 * i];
  }

  i = setAll->size[0] * setAll->size[1];
  setAll->size[0] = 1;
  setAll->size[1] = notdone;
  emxEnsureCapacity_real_T(sp, setAll, i, &ri_emlrtRTEI);
  for (i = 0; i < notdone; i++) {
    setAll->data[i] = FacetsOnNodeInd->data[5 * i + 1];
  }

  i = NodeRegInd->size[0] * NodeRegInd->size[1];
  NodeRegInd->size[0] = 1;
  NodeRegInd->size[1] = c_loop_ub;
  emxEnsureCapacity_real_T(sp, NodeRegInd, i, &si_emlrtRTEI);
  for (i = 0; i < c_loop_ub; i++) {
    NodeRegInd->data[i] = FacetsOnNodeInd->data[5 * i + 3];
  }

  emxFree_real_T(&FacetsOnNodeInd);
  st.site = &qc_emlrtRSI;
  b_sparse(&st, setPBC, setAll, NodeRegInd, FacetsOnNodeInt);

  /*  flag for materialI ID for that edge; -1 means domain edge. */
  /*  clear FacetsOnNodeInd */
  /*  Group facets according to interface type */
  emxFree_real_T(&NodeRegInd);
  if (1.0 > *numfac) {
    b_loop_ub = 0;
  } else {
    if (1 > FacetsOnInterface->size[0]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, FacetsOnInterface->size[0],
        &dl_emlrtBCI, sp);
    }

    if (((int32_T)*numfac < 1) || ((int32_T)*numfac > FacetsOnInterface->size[0]))
    {
      emlrtDynamicBoundsCheckR2012b((int32_T)*numfac, 1, FacetsOnInterface->
        size[0], &cl_emlrtBCI, sp);
    }

    b_loop_ub = (int32_T)*numfac;
  }

  input_sizes[0] = 1;
  input_sizes[1] = b_loop_ub;
  st.site = &pc_emlrtRSI;
  indexShapeCheck(&st, FacetsOnInterface->size[0], input_sizes);
  st.site = &pc_emlrtRSI;
  i = MorePBC->size[0];
  MorePBC->size[0] = b_loop_ub;
  emxEnsureCapacity_real_T(&st, MorePBC, i, &ld_emlrtRTEI);
  for (i = 0; i < b_loop_ub; i++) {
    MorePBC->data[i] = FacetsOnInterface->data[i];
  }

  b_st.site = &tg_emlrtRSI;
  sort(&b_st, MorePBC, idx);
  i = FacetsOnInterface->size[0];
  FacetsOnInterface->size[0] = idx->size[0];
  emxEnsureCapacity_real_T(&st, FacetsOnInterface, i, &ui_emlrtRTEI);
  b_loop_ub = idx->size[0];
  for (i = 0; i < b_loop_ub; i++) {
    FacetsOnInterface->data[i] = idx->data[i];
  }

  i = FacetsOnIntMinusPBCNum->size[0];
  FacetsOnIntMinusPBCNum->size[0] = numEonF->size[0] + 1;
  emxEnsureCapacity_real_T(sp, FacetsOnIntMinusPBCNum, i, &vi_emlrtRTEI);
  FacetsOnIntMinusPBCNum->data[0] = 1.0;
  b_loop_ub = numEonF->size[0];
  for (i = 0; i < b_loop_ub; i++) {
    FacetsOnIntMinusPBCNum->data[i + 1] = numEonF->data[i];
  }

  i = FacetsOnInterfaceNum->size[0];
  FacetsOnInterfaceNum->size[0] = FacetsOnIntMinusPBCNum->size[0];
  emxEnsureCapacity_real_T(sp, FacetsOnInterfaceNum, i, &xi_emlrtRTEI);
  b_loop_ub = FacetsOnIntMinusPBCNum->size[0];
  for (i = 0; i < b_loop_ub; i++) {
    FacetsOnInterfaceNum->data[i] = FacetsOnIntMinusPBCNum->data[i];
  }

  emlrtForLoopVectorCheckR2012b(1.0, 1.0, *numinttype, mxDOUBLE_CLASS, (int32_T)*
    numinttype, &gc_emlrtRTEI, sp);
  for (lenn = 0; lenn < loop_ub; lenn++) {
    if (((int32_T)(lenn + 2U) < 1) || ((int32_T)(lenn + 2U) >
         FacetsOnInterfaceNum->size[0])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)(lenn + 2U), 1,
        FacetsOnInterfaceNum->size[0], &ce_emlrtBCI, sp);
    }

    if ((int32_T)(lenn + 1U) > FacetsOnInterfaceNum->size[0]) {
      emlrtDynamicBoundsCheckR2012b((int32_T)(lenn + 1U), 1,
        FacetsOnInterfaceNum->size[0], &de_emlrtBCI, sp);
    }

    FacetsOnInterfaceNum->data[lenn + 1] += FacetsOnInterfaceNum->data[lenn];
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  /*  % The actual facet identifiers for interface type regI are then: */
  /*  locF = FacetsOnInterfaceNum(regI):(FacetsOnInterfaceNum(regI+1)-1); */
  /*  facs = FacetsOnInterface(locF); */
  /*  true = all(ElementsOnFacet(facs,1:4) == ElementsOnFacetInt(1:numEonF(regI),1:4,regI)); */
  st.site = &oc_emlrtRSI;
  if (muDoubleScalarIsNaN(usePBC)) {
    emlrtErrorWithMessageIdR2018a(&st, &mc_emlrtRTEI, "MATLAB:nologicalnan",
      "MATLAB:nologicalnan", 0);
  }

  if (usePBC != 0.0) {
    /*  sort to find the list of PBC facets grouped by interface type */
    if (1.0 > *numfac) {
      b_loop_ub = 0;
    } else {
      if (1 > FacetsOnPBC->size[0]) {
        emlrtDynamicBoundsCheckR2012b(1, 1, FacetsOnPBC->size[0], &bl_emlrtBCI,
          sp);
      }

      if (((int32_T)*numfac < 1) || ((int32_T)*numfac > FacetsOnPBC->size[0])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)*numfac, 1, FacetsOnPBC->size[0],
          &al_emlrtBCI, sp);
      }

      b_loop_ub = (int32_T)*numfac;
    }

    input_sizes[0] = 1;
    input_sizes[1] = b_loop_ub;
    st.site = &nc_emlrtRSI;
    indexShapeCheck(&st, FacetsOnPBC->size[0], input_sizes);
    st.site = &nc_emlrtRSI;
    i = MorePBC->size[0];
    MorePBC->size[0] = b_loop_ub;
    emxEnsureCapacity_real_T(&st, MorePBC, i, &ld_emlrtRTEI);
    for (i = 0; i < b_loop_ub; i++) {
      MorePBC->data[i] = FacetsOnPBC->data[i];
    }

    b_st.site = &tg_emlrtRSI;
    sort(&b_st, MorePBC, idx);
    i = MorePBC->size[0];
    MorePBC->size[0] = idx->size[0];
    emxEnsureCapacity_real_T(&st, MorePBC, i, &ui_emlrtRTEI);
    b_loop_ub = idx->size[0];
    for (i = 0; i < b_loop_ub; i++) {
      MorePBC->data[i] = idx->data[i];
    }

    d = *numfac - numFPBC;
    if (d + 1.0 > *numfac) {
      i = 0;
      i1 = 0;
    } else {
      if (((int32_T)(d + 1.0) < 1) || ((int32_T)(d + 1.0) > MorePBC->size[0])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(d + 1.0), 1, MorePBC->size[0],
          &yk_emlrtBCI, sp);
      }

      i = (int32_T)(d + 1.0) - 1;
      if (((int32_T)*numfac < 1) || ((int32_T)*numfac > MorePBC->size[0])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)*numfac, 1, MorePBC->size[0],
          &xk_emlrtBCI, sp);
      }

      i1 = (int32_T)*numfac;
    }

    input_sizes[0] = 1;
    notdone = i1 - i;
    input_sizes[1] = notdone;
    st.site = &mc_emlrtRSI;
    indexShapeCheck(&st, MorePBC->size[0], input_sizes);
    i2 = FacetsOnPBC->size[0];
    FacetsOnPBC->size[0] = notdone;
    emxEnsureCapacity_real_T(sp, FacetsOnPBC, i2, &hj_emlrtRTEI);
    for (i2 = 0; i2 < notdone; i2++) {
      FacetsOnPBC->data[i2] = MorePBC->data[i + i2];
    }

    /*  delete the zeros */
    i2 = FacetsOnPBCNum->size[0];
    FacetsOnPBCNum->size[0] = numEonPBC->size[0] + 1;
    emxEnsureCapacity_real_T(sp, FacetsOnPBCNum, i2, &ij_emlrtRTEI);
    FacetsOnPBCNum->data[0] = 1.0;
    b_loop_ub = numEonPBC->size[0];
    for (i2 = 0; i2 < b_loop_ub; i2++) {
      FacetsOnPBCNum->data[i2 + 1] = numEonPBC->data[i2];
    }

    emlrtForLoopVectorCheckR2012b(1.0, 1.0, *numinttype, mxDOUBLE_CLASS,
      (int32_T)*numinttype, &fc_emlrtRTEI, sp);
    for (lenn = 0; lenn < loop_ub; lenn++) {
      if (((int32_T)(lenn + 2U) < 1) || ((int32_T)(lenn + 2U) >
           FacetsOnPBCNum->size[0])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(lenn + 2U), 1,
          FacetsOnPBCNum->size[0], &pd_emlrtBCI, sp);
      }

      if ((int32_T)(lenn + 1U) > FacetsOnPBCNum->size[0]) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(lenn + 1U), 1,
          FacetsOnPBCNum->size[0], &qd_emlrtBCI, sp);
      }

      FacetsOnPBCNum->data[lenn + 1] += FacetsOnPBCNum->data[lenn];
      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    if ((int32_T)d < 0) {
      emlrtNonNegativeCheckR2012b((int32_T)d, &v_emlrtDCI, sp);
    }

    i2 = FacetsOnIntMinusPBC->size[0];
    FacetsOnIntMinusPBC->size[0] = (int32_T)d;
    emxEnsureCapacity_real_T(sp, FacetsOnIntMinusPBC, i2, &lj_emlrtRTEI);
    b_loop_ub = (int32_T)(*numfac - numFPBC);
    if (b_loop_ub < 0) {
      emlrtNonNegativeCheckR2012b(b_loop_ub, &v_emlrtDCI, sp);
    }

    for (i2 = 0; i2 < b_loop_ub; i2++) {
      FacetsOnIntMinusPBC->data[i2] = 0.0;
    }

    emlrtForLoopVectorCheckR2012b(1.0, 1.0, *numinttype, mxDOUBLE_CLASS,
      (int32_T)*numinttype, &ec_emlrtRTEI, sp);
    for (lenn = 0; lenn < loop_ub; lenn++) {
      st.site = &lc_emlrtRSI;
      i2 = lenn + 1;
      if (i2 > FacetsOnInterfaceNum->size[0]) {
        emlrtDynamicBoundsCheckR2012b(i2, 1, FacetsOnInterfaceNum->size[0],
          &wk_emlrtBCI, &st);
      }

      if ((lenn + 2 < 1) || (lenn + 2 > FacetsOnInterfaceNum->size[0])) {
        emlrtDynamicBoundsCheckR2012b(lenn + 2, 1, FacetsOnInterfaceNum->size[0],
          &vk_emlrtBCI, &st);
      }

      b_st.site = &lo_emlrtRSI;
      d = FacetsOnInterfaceNum->data[lenn + 1] - 1.0;
      if (d < FacetsOnInterfaceNum->data[lenn]) {
        setAll->size[0] = 1;
        setAll->size[1] = 0;
      } else if ((muDoubleScalarIsInf(FacetsOnInterfaceNum->data[lenn]) ||
                  muDoubleScalarIsInf(d)) && (FacetsOnInterfaceNum->data[lenn] ==
                  d)) {
        i3 = setAll->size[0] * setAll->size[1];
        setAll->size[0] = 1;
        setAll->size[1] = 1;
        emxEnsureCapacity_real_T(&b_st, setAll, i3, &sj_emlrtRTEI);
        setAll->data[0] = rtNaN;
      } else if (FacetsOnInterfaceNum->data[lenn] == FacetsOnInterfaceNum->
                 data[lenn]) {
        i3 = setAll->size[0] * setAll->size[1];
        setAll->size[0] = 1;
        b_loop_ub = (int32_T)muDoubleScalarFloor(d - FacetsOnInterfaceNum->
          data[lenn]);
        setAll->size[1] = b_loop_ub + 1;
        emxEnsureCapacity_real_T(&b_st, setAll, i3, &sj_emlrtRTEI);
        for (i3 = 0; i3 <= b_loop_ub; i3++) {
          setAll->data[i3] = FacetsOnInterfaceNum->data[lenn] + (real_T)i3;
        }
      } else {
        c_st.site = &mo_emlrtRSI;
        eml_float_colon(&c_st, FacetsOnInterfaceNum->data[lenn],
                        FacetsOnInterfaceNum->data[lenn + 1] - 1.0, setAll);
      }

      st.site = &kc_emlrtRSI;
      indexShapeCheck(&st, FacetsOnInterface->size[0], *(int32_T (*)[2])
                      setAll->size);
      b_loop_ub = setAll->size[0] * setAll->size[1];
      for (i3 = 0; i3 < b_loop_ub; i3++) {
        if (setAll->data[i3] != (int32_T)muDoubleScalarFloor(setAll->data[i3]))
        {
          emlrtIntegerCheckR2012b(setAll->data[i3], &q_emlrtDCI, sp);
        }

        b_i = (int32_T)setAll->data[i3];
        if ((b_i < 1) || (b_i > FacetsOnInterface->size[0])) {
          emlrtDynamicBoundsCheckR2012b(b_i, 1, FacetsOnInterface->size[0],
            &cd_emlrtBCI, sp);
        }
      }

      st.site = &jc_emlrtRSI;
      if (i2 > FacetsOnPBCNum->size[0]) {
        emlrtDynamicBoundsCheckR2012b(i2, 1, FacetsOnPBCNum->size[0],
          &uk_emlrtBCI, &st);
      }

      if ((lenn + 2 < 1) || (lenn + 2 > FacetsOnPBCNum->size[0])) {
        emlrtDynamicBoundsCheckR2012b(lenn + 2, 1, FacetsOnPBCNum->size[0],
          &tk_emlrtBCI, &st);
      }

      b_st.site = &lo_emlrtRSI;
      d = FacetsOnPBCNum->data[lenn + 1] - 1.0;
      if (d < FacetsOnPBCNum->data[lenn]) {
        setPBC->size[0] = 1;
        setPBC->size[1] = 0;
      } else if ((muDoubleScalarIsInf(FacetsOnPBCNum->data[lenn]) ||
                  muDoubleScalarIsInf(d)) && (FacetsOnPBCNum->data[lenn] == d))
      {
        i3 = setPBC->size[0] * setPBC->size[1];
        setPBC->size[0] = 1;
        setPBC->size[1] = 1;
        emxEnsureCapacity_real_T(&b_st, setPBC, i3, &xj_emlrtRTEI);
        setPBC->data[0] = rtNaN;
      } else if (FacetsOnPBCNum->data[lenn] == FacetsOnPBCNum->data[lenn]) {
        i3 = setPBC->size[0] * setPBC->size[1];
        setPBC->size[0] = 1;
        b_loop_ub = (int32_T)muDoubleScalarFloor(d - FacetsOnPBCNum->data[lenn]);
        setPBC->size[1] = b_loop_ub + 1;
        emxEnsureCapacity_real_T(&b_st, setPBC, i3, &xj_emlrtRTEI);
        for (i3 = 0; i3 <= b_loop_ub; i3++) {
          setPBC->data[i3] = FacetsOnPBCNum->data[lenn] + (real_T)i3;
        }
      } else {
        c_st.site = &mo_emlrtRSI;
        eml_float_colon(&c_st, FacetsOnPBCNum->data[lenn], FacetsOnPBCNum->
                        data[lenn + 1] - 1.0, setPBC);
      }

      st.site = &ic_emlrtRSI;
      indexShapeCheck(&st, i1 - i, *(int32_T (*)[2])setPBC->size);
      b_loop_ub = setPBC->size[0] * setPBC->size[1];
      for (i3 = 0; i3 < b_loop_ub; i3++) {
        if (setPBC->data[i3] != (int32_T)muDoubleScalarFloor(setPBC->data[i3]))
        {
          emlrtIntegerCheckR2012b(setPBC->data[i3], &m_emlrtDCI, sp);
        }

        b_i = (int32_T)setPBC->data[i3];
        if ((b_i < 1) || (b_i > notdone)) {
          emlrtDynamicBoundsCheckR2012b(b_i, 1, notdone, &xc_emlrtBCI, sp);
        }
      }

      st.site = &hc_emlrtRSI;
      b_st.site = &li_emlrtRSI;
      i3 = t5_d->size[0];
      t5_d->size[0] = setAll->size[1];
      emxEnsureCapacity_real_T(&b_st, t5_d, i3, &bk_emlrtRTEI);
      b_loop_ub = setAll->size[1];
      for (i3 = 0; i3 < b_loop_ub; i3++) {
        t5_d->data[i3] = FacetsOnInterface->data[(int32_T)setAll->data[i3] - 1];
      }

      i3 = ElementsOnNodePBCNum->size[0];
      ElementsOnNodePBCNum->size[0] = setPBC->size[1];
      emxEnsureCapacity_real_T(&b_st, ElementsOnNodePBCNum, i3, &dk_emlrtRTEI);
      b_loop_ub = setPBC->size[1];
      for (i3 = 0; i3 < b_loop_ub; i3++) {
        ElementsOnNodePBCNum->data[i3] = MorePBC->data[(i + (int32_T)
          setPBC->data[i3]) - 1];
      }

      c_st.site = &mi_emlrtRSI;
      b_do_vectors(&c_st, t5_d, ElementsOnNodePBCNum, setnPBC, ii, ib_size);
      numSI_tmp = setnPBC->size[0];
      if (((int32_T)(lenn + 2U) < 1) || ((int32_T)(lenn + 2U) >
           FacetsOnIntMinusPBCNum->size[0])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(lenn + 2U), 1,
          FacetsOnIntMinusPBCNum->size[0], &oc_emlrtBCI, sp);
      }

      if ((int32_T)(lenn + 1U) > FacetsOnIntMinusPBCNum->size[0]) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(lenn + 1U), 1,
          FacetsOnIntMinusPBCNum->size[0], &pc_emlrtBCI, sp);
      }

      FacetsOnIntMinusPBCNum->data[lenn + 1] = FacetsOnIntMinusPBCNum->data[lenn]
        + (real_T)setnPBC->size[0];
      if (setnPBC->size[0] > 0) {
        if (i2 > FacetsOnIntMinusPBCNum->size[0]) {
          emlrtDynamicBoundsCheckR2012b(i2, 1, FacetsOnIntMinusPBCNum->size[0],
            &bh_emlrtBCI, sp);
        }

        if ((lenn + 2 < 1) || (lenn + 2 > FacetsOnIntMinusPBCNum->size[0])) {
          emlrtDynamicBoundsCheckR2012b(lenn + 2, 1,
            FacetsOnIntMinusPBCNum->size[0], &ah_emlrtBCI, sp);
        }

        d = FacetsOnIntMinusPBCNum->data[lenn + 1] - 1.0;
        if (FacetsOnIntMinusPBCNum->data[lenn] > d) {
          i2 = -1;
          i3 = -1;
        } else {
          i2 = (int32_T)FacetsOnIntMinusPBCNum->data[lenn];
          if ((i2 < 1) || (i2 > FacetsOnIntMinusPBC->size[0])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)FacetsOnIntMinusPBCNum->
              data[lenn], 1, FacetsOnIntMinusPBC->size[0], &yg_emlrtBCI, sp);
          }

          i2 -= 2;
          if (((int32_T)d < 1) || ((int32_T)d > FacetsOnIntMinusPBC->size[0])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)d, 1,
              FacetsOnIntMinusPBC->size[0], &xg_emlrtBCI, sp);
          }

          i3 = (int32_T)d - 1;
        }

        b_loop_ub = i3 - i2;
        if (b_loop_ub != setnPBC->size[0]) {
          emlrtSubAssignSizeCheck1dR2017a(b_loop_ub, setnPBC->size[0], &emlrtECI,
            sp);
        }

        for (i3 = 0; i3 < b_loop_ub; i3++) {
          FacetsOnIntMinusPBC->data[(i2 + i3) + 1] = setnPBC->data[i3];
        }
      }

      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }
  } else {
    i = FacetsOnPBCNum->size[0];
    FacetsOnPBCNum->size[0] = 1;
    emxEnsureCapacity_real_T(sp, FacetsOnPBCNum, i, &aj_emlrtRTEI);
    FacetsOnPBCNum->data[0] = 0.0;
    i = FacetsOnIntMinusPBC->size[0];
    FacetsOnIntMinusPBC->size[0] = 1;
    emxEnsureCapacity_real_T(sp, FacetsOnIntMinusPBC, i, &bj_emlrtRTEI);
    FacetsOnIntMinusPBC->data[0] = 0.0;
    i = FacetsOnIntMinusPBCNum->size[0];
    FacetsOnIntMinusPBCNum->size[0] = 1;
    emxEnsureCapacity_real_T(sp, FacetsOnIntMinusPBCNum, i, &dj_emlrtRTEI);
    FacetsOnIntMinusPBCNum->data[0] = 0.0;
  }

  emxInit_real_T(sp, &b_NodesOnInterface, 2, &el_emlrtRTEI, true);

  /*  Find material interfaces and duplicate nodes */
  b_NodesOnInterface->size[0] = 0;
  b_NodesOnInterface->size[1] = 1;
  i = (int32_T)(nummat + -1.0);
  emlrtForLoopVectorCheckR2012b(2.0, 1.0, nummat, mxDOUBLE_CLASS, (int32_T)
    (nummat + -1.0), &dc_emlrtRTEI, sp);
  emxInitStruct_sparse3(sp, &c_expl_temp, &nl_emlrtRTEI, true);
  emxInitStruct_sparse3(sp, &d_expl_temp, &ol_emlrtRTEI, true);
  emxInitStruct_sparse3(sp, &e_expl_temp, &gj_emlrtRTEI, true);
  for (end = 0; end < i; end++) {
    for (numPBC = 0; numPBC <= end; numPBC++) {
      /*  ID for material pair (row=mat2, col=mat1) */
      if (((int32_T)(numPBC + 1U) < 1) || ((int32_T)(numPBC + 1U) >
           InterTypes->size[1])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(numPBC + 1U), 1,
          InterTypes->size[1], &vd_emlrtBCI, sp);
      }

      if (((int32_T)(end + 2U) < 1) || ((int32_T)(end + 2U) > InterTypes->size[0]))
      {
        emlrtDynamicBoundsCheckR2012b((int32_T)(end + 2U), 1, InterTypes->size[0],
          &vd_emlrtBCI, sp);
      }

      if (InterTypes->data[(end + InterTypes->size[0] * numPBC) + 1] > 0.0) {
        /*  Mark nodes on the interfaces */
        st.site = &gc_emlrtRSI;
        c_sparse_parenReference(&st, NodeReg->d, NodeReg->colidx,
          NodeReg->rowidx, NodeReg->m, NodeReg->n, (real_T)numPBC + 1.0,
          &NodeRegSum);
        st.site = &gc_emlrtRSI;
        sparse_gt(&st, NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  NodeRegSum.m, &d_expl_temp);
        st.site = &gc_emlrtRSI;
        c_sparse_parenReference(&st, NodeReg->d, NodeReg->colidx,
          NodeReg->rowidx, NodeReg->m, NodeReg->n, (real_T)end + 2.0,
          &NodeRegSum);
        st.site = &gc_emlrtRSI;
        sparse_gt(&st, NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  NodeRegSum.m, &e_expl_temp);
        i1 = c_expl_temp.colidx->size[0];
        c_expl_temp.colidx->size[0] = e_expl_temp.colidx->size[0];
        emxEnsureCapacity_int32_T(sp, c_expl_temp.colidx, i1, &gj_emlrtRTEI);
        loop_ub = e_expl_temp.colidx->size[0];
        for (i1 = 0; i1 < loop_ub; i1++) {
          c_expl_temp.colidx->data[i1] = e_expl_temp.colidx->data[i1];
        }

        i1 = c_expl_temp.rowidx->size[0];
        c_expl_temp.rowidx->size[0] = e_expl_temp.rowidx->size[0];
        emxEnsureCapacity_int32_T(sp, c_expl_temp.rowidx, i1, &gj_emlrtRTEI);
        loop_ub = e_expl_temp.rowidx->size[0];
        for (i1 = 0; i1 < loop_ub; i1++) {
          c_expl_temp.rowidx->data[i1] = e_expl_temp.rowidx->data[i1];
        }

        st.site = &gc_emlrtRSI;
        sparse_and(&st, d_expl_temp.d, d_expl_temp.colidx, d_expl_temp.rowidx,
                   d_expl_temp.m, e_expl_temp.d, c_expl_temp.colidx,
                   c_expl_temp.rowidx, e_expl_temp.m, b_NodesOnPBCnum, ii, idx,
                   &lenn);
        st.site = &gc_emlrtRSI;
        c_sparse_parenReference(&st, NodeReg->d, NodeReg->colidx,
          NodeReg->rowidx, NodeReg->m, NodeReg->n, (real_T)end + 2.0,
          &NodeRegSum);
        st.site = &gc_emlrtRSI;
        c_sparse_parenReference(&st, NodeReg->d, NodeReg->colidx,
          NodeReg->rowidx, NodeReg->m, NodeReg->n, (real_T)numPBC + 1.0,
          &b_expl_temp);
        st.site = &gc_emlrtRSI;
        sparse_eq(&st, NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  NodeRegSum.m, b_expl_temp.d, b_expl_temp.colidx,
                  b_expl_temp.rowidx, b_expl_temp.m, d_expl_temp.d,
                  d_expl_temp.colidx, d_expl_temp.rowidx, &elemB);
        st.site = &gc_emlrtRSI;
        sparse_and(&st, b_NodesOnPBCnum, ii, idx, lenn, d_expl_temp.d,
                   d_expl_temp.colidx, d_expl_temp.rowidx, elemB, e_expl_temp.d,
                   c_expl_temp.colidx, c_expl_temp.rowidx, &notdone);
        st.site = &fc_emlrtRSI;
        b_st.site = &uh_emlrtRSI;
        g_eml_find(&b_st, c_expl_temp.colidx, c_expl_temp.rowidx, notdone, ii);
        loop_ub = b_NodesOnInterface->size[0];
        i1 = NodesOnInterface->size[0];
        NodesOnInterface->size[0] = b_NodesOnInterface->size[0] + ii->size[0];
        emxEnsureCapacity_real_T(sp, NodesOnInterface, i1, &mj_emlrtRTEI);
        for (i1 = 0; i1 < loop_ub; i1++) {
          NodesOnInterface->data[i1] = b_NodesOnInterface->data[i1];
        }

        loop_ub = ii->size[0];
        for (i1 = 0; i1 < loop_ub; i1++) {
          NodesOnInterface->data[i1 + b_NodesOnInterface->size[0]] = ii->data[i1];
        }

        i1 = b_NodesOnInterface->size[0] * b_NodesOnInterface->size[1];
        b_NodesOnInterface->size[0] = NodesOnInterface->size[0];
        b_NodesOnInterface->size[1] = 1;
        emxEnsureCapacity_real_T(sp, b_NodesOnInterface, i1, &oj_emlrtRTEI);
        loop_ub = NodesOnInterface->size[0];
        for (i1 = 0; i1 < loop_ub; i1++) {
          b_NodesOnInterface->data[i1] = NodesOnInterface->data[i1];
        }
      }

      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  emxFreeStruct_sparse(&b_expl_temp);
  emxFreeStruct_sparse3(&e_expl_temp);
  st.site = &ec_emlrtRSI;
  if (muDoubleScalarIsNaN(usePBC)) {
    emlrtErrorWithMessageIdR2018a(&st, &mc_emlrtRTEI, "MATLAB:nologicalnan",
      "MATLAB:nologicalnan", 0);
  }

  if (usePBC != 0.0) {
    /*  Add PBC nodes into duplicating list */
    st.site = &dc_emlrtRSI;
    sum(&st, NodeReg->d, NodeReg->colidx, NodeReg->rowidx, NodeReg->m,
        NodeReg->n, &NodeRegSum);

    /*  from zipped nodes, ONLY the one with regions attached is in the zipped connectivity */
    st.site = &cc_emlrtRSI;
    i = b_NodesOnPBCnum->size[0];
    b_NodesOnPBCnum->size[0] = NodesOnPBCnum->size[0];
    emxEnsureCapacity_boolean_T(&st, b_NodesOnPBCnum, i, &fj_emlrtRTEI);
    loop_ub = NodesOnPBCnum->size[0];
    for (i = 0; i < loop_ub; i++) {
      b_NodesOnPBCnum->data[i] = (NodesOnPBCnum->data[i] > 0.0);
    }

    b_st.site = &uh_emlrtRSI;
    eml_find(&b_st, b_NodesOnPBCnum, ii);
    st.site = &bc_emlrtRSI;
    b_st.site = &bc_emlrtRSI;
    sparse_gt(&b_st, NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
              NodeRegSum.m, &c_expl_temp);
    b_st.site = &uh_emlrtRSI;
    g_eml_find(&b_st, c_expl_temp.colidx, c_expl_temp.rowidx, c_expl_temp.m, idx);
    st.site = &ac_emlrtRSI;
    b_st.site = &dq_emlrtRSI;
    i = ElementsOnNodePBCNum->size[0];
    ElementsOnNodePBCNum->size[0] = ii->size[0];
    emxEnsureCapacity_real_T(&b_st, ElementsOnNodePBCNum, i, &kj_emlrtRTEI);
    loop_ub = ii->size[0];
    for (i = 0; i < loop_ub; i++) {
      ElementsOnNodePBCNum->data[i] = ii->data[i];
    }

    i = t5_d->size[0];
    t5_d->size[0] = idx->size[0];
    emxEnsureCapacity_real_T(&b_st, t5_d, i, &kj_emlrtRTEI);
    loop_ub = idx->size[0];
    for (i = 0; i < loop_ub; i++) {
      t5_d->data[i] = idx->data[i];
    }

    c_st.site = &mi_emlrtRSI;
    c_do_vectors(&c_st, ElementsOnNodePBCNum, t5_d, MorePBC, ii, jj);
    i = NodesOnInterface->size[0];
    NodesOnInterface->size[0] = b_NodesOnInterface->size[0] + MorePBC->size[0];
    emxEnsureCapacity_real_T(sp, NodesOnInterface, i, &nj_emlrtRTEI);
    loop_ub = b_NodesOnInterface->size[0];
    for (i = 0; i < loop_ub; i++) {
      NodesOnInterface->data[i] = b_NodesOnInterface->data[i];
    }

    loop_ub = MorePBC->size[0];
    for (i = 0; i < loop_ub; i++) {
      NodesOnInterface->data[i + b_NodesOnInterface->size[0]] = MorePBC->data[i];
    }

    i = b_NodesOnInterface->size[0] * b_NodesOnInterface->size[1];
    b_NodesOnInterface->size[0] = NodesOnInterface->size[0];
    b_NodesOnInterface->size[1] = 1;
    emxEnsureCapacity_real_T(sp, b_NodesOnInterface, i, &pj_emlrtRTEI);
    loop_ub = NodesOnInterface->size[0];
    for (i = 0; i < loop_ub; i++) {
      b_NodesOnInterface->data[i] = NodesOnInterface->data[i];
    }

    /*  add these zipped PBC nodes into the list for duplicates too */
  }

  emxInit_real_T(sp, &intermat2, 2, &jj_emlrtRTEI, true);
  st.site = &yb_emlrtRSI;
  ib_size[0] = b_NodesOnInterface->size[0];
  b_locPBC_data = *b_NodesOnInterface;
  b_NodesOnElement[0] = ib_size[0];
  b_locPBC_data.size = &b_NodesOnElement[0];
  b_locPBC_data.numDimensions = 1;
  b_st.site = &ai_emlrtRSI;
  unique_vector(&b_st, &b_locPBC_data, NodesOnInterface);
  e_loop_ub = NodesOnInterface->size[0];

  /*  Criteria: only nodes for which ALL inter-material edges involving a given */
  /*  material are being cut, then they are duplicated. */
  if (!(nummat >= 0.0)) {
    emlrtNonNegativeCheckR2012b(nummat, &wc_emlrtDCI, sp);
  }

  i = (int32_T)muDoubleScalarFloor(nummat);
  if (nummat != i) {
    emlrtIntegerCheckR2012b(nummat, &xc_emlrtDCI, sp);
  }

  i1 = intermat2->size[0] * intermat2->size[1];
  i2 = (int32_T)nummat;
  intermat2->size[0] = i2;
  emxEnsureCapacity_real_T(sp, intermat2, i1, &jj_emlrtRTEI);
  if (i2 != i) {
    emlrtIntegerCheckR2012b(nummat, &vc_emlrtDCI, sp);
  }

  i1 = intermat2->size[0] * intermat2->size[1];
  intermat2->size[1] = i2;
  emxEnsureCapacity_real_T(sp, intermat2, i1, &jj_emlrtRTEI);
  if (i2 != i) {
    emlrtIntegerCheckR2012b(nummat, &s_emlrtDCI, sp);
  }

  loop_ub = i2 * i2;
  for (i = 0; i < loop_ub; i++) {
    intermat2->data[i] = 0.0;
  }

  emlrtForLoopVectorCheckR2012b(1.0, 1.0, nummat, mxDOUBLE_CLASS, (int32_T)
    nummat, &cc_emlrtRTEI, sp);
  for (end = 0; end < i2; end++) {
    for (numPBC = 0; numPBC <= end; numPBC++) {
      if ((int32_T)(numPBC + 1U) > intermat2->size[1]) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(numPBC + 1U), 1, intermat2->
          size[1], &fd_emlrtBCI, sp);
      }

      if ((int32_T)(end + 1U) > intermat2->size[0]) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(end + 1U), 1, intermat2->size[0],
          &fd_emlrtBCI, sp);
      }

      numA = ((real_T)end + 1.0) * (((real_T)end + 1.0) - 1.0) / 2.0 + ((real_T)
        numPBC + 1.0);
      intermat2->data[end + intermat2->size[0] * numPBC] = numA;
      if ((int32_T)(end + 1U) > intermat2->size[1]) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(end + 1U), 1, intermat2->size[1],
          &dd_emlrtBCI, sp);
      }

      if ((int32_T)(numPBC + 1U) > intermat2->size[0]) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(numPBC + 1U), 1, intermat2->
          size[0], &dd_emlrtBCI, sp);
      }

      intermat2->data[numPBC + intermat2->size[0] * end] = numA;
      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  b_i2 = *numnp;
  i = NodesOnInterface->size[0];
  emxInitStruct_sparse2(sp, &f_expl_temp, &ll_emlrtRTEI, true);
  emxInitStruct_sparse2(sp, &g_expl_temp, &ql_emlrtRTEI, true);
  emxInitStruct_sparse2(sp, &h_expl_temp, &rl_emlrtRTEI, true);
  for (partialTrueCount = 0; partialTrueCount < i; partialTrueCount++) {
    if ((partialTrueCount + 1 < 1) || (partialTrueCount + 1 >
         NodesOnInterface->size[0])) {
      emlrtDynamicBoundsCheckR2012b(partialTrueCount + 1, 1,
        NodesOnInterface->size[0], &ed_emlrtBCI, sp);
    }

    node = NodesOnInterface->data[partialTrueCount];

    /*      matnode = sum(NodeMat(node,:)>0); */
    numA = NodesOnInterface->data[partialTrueCount];
    i1 = (int32_T)NodesOnInterface->data[partialTrueCount];
    if ((i1 < 1) || (i1 > FacetsOnNodeNum->size[0])) {
      emlrtDynamicBoundsCheckR2012b(i1, 1, FacetsOnNodeNum->size[0],
        &sk_emlrtBCI, sp);
    }

    d = FacetsOnNodeNum->data[i1 - 1];
    if (d == 0.0) {
      /*  Determine if midedge or midface node */
      elemB = 0;
      if ((!(nen == 10.0)) && (!(nen == 20.0)) && (nen == 27.0)) {
        st.site = &xb_emlrtRSI;
        d_sparse_parenReference(&st, ElementsOnNode->d, ElementsOnNode->colidx,
          ElementsOnNode->rowidx, ElementsOnNode->m, ElementsOnNode->n,
          NodesOnInterface->data[partialTrueCount], ElementsOnNodePBCNum, jj, ii);
        st.site = &xb_emlrtRSI;
        b_i1 = sparse_full(&st, ElementsOnNodePBCNum, jj);
        st.site = &wb_emlrtRSI;
        i3 = NodesOnElement->size[0];
        if (b_i1 != (int32_T)muDoubleScalarFloor(b_i1)) {
          emlrtIntegerCheckR2012b(b_i1, &be_emlrtDCI, &st);
        }

        if (((int32_T)b_i1 < 1) || ((int32_T)b_i1 > i3)) {
          emlrtDynamicBoundsCheckR2012b((int32_T)b_i1, 1, i3, &rk_emlrtBCI, &st);
        }

        loop_ub = NodesOnElement->size[1];
        nodeE2_size[0] = 1;
        nodeE2_size[1] = loop_ub;
        for (i3 = 0; i3 < loop_ub; i3++) {
          b_nodeE2_data[i3] = (NodesOnElement->data[((int32_T)b_i1 +
            NodesOnElement->size[0] * i3) - 1] == numA);
        }

        b_st.site = &uh_emlrtRSI;
        f_eml_find(&b_st, b_nodeE2_data, nodeE2_size, ii_data, ii_size);
        loop_ub = ii_size[0] * ii_size[1];
        for (i3 = 0; i3 < loop_ub; i3++) {
          nodeE2_data[i3] = ii_data[i3];
        }

        nodeE2_size[0] = 1;
        loop_ub = ii_size[1];
        nodeE2_size[1] = ii_size[1];
        for (i3 = 0; i3 < loop_ub; i3++) {
          b_nodeE2_data[i3] = (nodeE2_data[i3] > 26.0);
        }

        st.site = &vb_emlrtRSI;
        if (ifWhileCond(b_nodeE2_data, nodeE2_size)) {
          st.site = &ub_emlrtRSI;
          h_error(&st);
        } else {
          nodeE2_size[0] = 1;
          loop_ub = ii_size[1];
          nodeE2_size[1] = ii_size[1];
          for (i3 = 0; i3 < loop_ub; i3++) {
            b_nodeE2_data[i3] = (nodeE2_data[i3] > 20.0);
          }

          st.site = &tb_emlrtRSI;
          if (ifWhileCond(b_nodeE2_data, nodeE2_size)) {
            elemB = 1;
          }
        }
      } else {
        /*  tetrahedra, no mid-face elements */
      }

      st.site = &sb_emlrtRSI;
      if (elemB != 0) {
        /*  midface; midedge will be handled below */
        st.site = &rb_emlrtRSI;
        if (muDoubleScalarIsNaN(usePBC)) {
          emlrtErrorWithMessageIdR2018a(&st, &mc_emlrtRTEI,
            "MATLAB:nologicalnan", "MATLAB:nologicalnan", 0);
        }

        b_guard1 = false;
        if (usePBC != 0.0) {
          if (i1 > NodesOnPBCnum->size[0]) {
            emlrtDynamicBoundsCheckR2012b(i1, 1, NodesOnPBCnum->size[0],
              &yc_emlrtBCI, sp);
          }

          if (NodesOnPBCnum->data[i1 - 1] > 0.0) {
            /*  handle copies of PBC nodes specially */
            /*  Just get the old node numbers back and copy them into place */
            if (i1 > ElementsOnNodeNum->size[0]) {
              emlrtDynamicBoundsCheckR2012b(i1, 1, ElementsOnNodeNum->size[0],
                &qk_emlrtBCI, sp);
            }

            st.site = &qb_emlrtRSI;
            c_sparse_parenReference(&st, ElementsOnNode->d,
              ElementsOnNode->colidx, ElementsOnNode->rowidx, ElementsOnNode->m,
              ElementsOnNode->n, NodesOnInterface->data[partialTrueCount],
              &NodeRegSum);
            st.site = &qb_emlrtRSI;
            c_sparse_full(&st, NodeRegSum.d, NodeRegSum.colidx,
                          NodeRegSum.rowidx, NodeRegSum.m, b_NodesOnInterface);

            /*  Loop over all elements attached to node */
            i1 = (int32_T)ElementsOnNodeNum->data[i1 - 1];
            emlrtForLoopVectorCheckR2012b(1.0, 1.0, ElementsOnNodeNum->data
              [(int32_T)NodesOnInterface->data[partialTrueCount] - 1],
              mxDOUBLE_CLASS, (int32_T)ElementsOnNodeNum->data[(int32_T)
              NodesOnInterface->data[partialTrueCount] - 1], &bc_emlrtRTEI, sp);
            if (0 <= i1 - 1) {
              if (1.0 > nen) {
                g_loop_ub = 0;
              } else {
                if (1 > NodesOnElementCG->size[1]) {
                  emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementCG->size[1],
                    &nk_emlrtBCI, sp);
                }

                if ((int32_T)nen > NodesOnElementCG->size[1]) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1,
                    NodesOnElementCG->size[1], &mk_emlrtBCI, sp);
                }

                g_loop_ub = (int32_T)nen;
              }

              b_NodesOnElementPBC = NodesOnElementPBC->size[1];
              input_sizes[0] = 1;
              nodeE2_size[0] = 1;
              nodeE2_size[1] = g_loop_ub;
              b_nodeE2_size[0] = 1;
            }

            for (elemB = 0; elemB < i1; elemB++) {
              i3 = elemB + 1;
              if ((i3 < 1) || (i3 > b_NodesOnInterface->size[0])) {
                emlrtDynamicBoundsCheckR2012b(i3, 1, b_NodesOnInterface->size[0],
                  &pk_emlrtBCI, sp);
              }

              /*  Reset nodal ID for element in higher material ID */
              st.site = &pb_emlrtRSI;
              if (b_NodesOnInterface->data[elemB] != (int32_T)
                  muDoubleScalarFloor(b_NodesOnInterface->data[elemB])) {
                emlrtIntegerCheckR2012b(b_NodesOnInterface->data[elemB],
                  &ae_emlrtDCI, &st);
              }

              notdone = (int32_T)b_NodesOnInterface->data[elemB];
              if ((notdone < 1) || (notdone > NodesOnElementCG->size[0])) {
                emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnElementCG->
                  size[0], &ok_emlrtBCI, &st);
              }

              for (i3 = 0; i3 < g_loop_ub; i3++) {
                b_nodeE2_data[i3] = (NodesOnElementCG->data[(notdone +
                  NodesOnElementCG->size[0] * i3) - 1] == node);
              }

              b_st.site = &uh_emlrtRSI;
              f_eml_find(&b_st, b_nodeE2_data, nodeE2_size, ii_data, ii_size);
              loop_ub = ii_size[1];
              if (0 <= loop_ub - 1) {
                memcpy(&b_ii_data[0], &ii_data[0], loop_ub * sizeof(int32_T));
              }

              notdone = (int32_T)b_NodesOnInterface->data[elemB];
              if ((notdone < 1) || (notdone > NodesOnElementPBC->size[0])) {
                emlrtDynamicBoundsCheckR2012b(notdone, 1,
                  NodesOnElementPBC->size[0], &lk_emlrtBCI, sp);
              }

              for (i3 = 0; i3 < loop_ub; i3++) {
                b_i = b_ii_data[i3];
                if ((b_i < 1) || (b_i > b_NodesOnElementPBC)) {
                  emlrtDynamicBoundsCheckR2012b(b_i, 1, b_NodesOnElementPBC,
                    &fc_emlrtBCI, sp);
                }
              }

              if (notdone > NodesOnElementDG->size[0]) {
                emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnElementDG->
                  size[0], &th_emlrtBCI, sp);
              }

              for (i3 = 0; i3 < loop_ub; i3++) {
                b_i = b_ii_data[i3];
                if (((int8_T)b_i < 1) || ((int8_T)b_i > NodesOnElementDG->size[1]))
                {
                  emlrtDynamicBoundsCheckR2012b((int8_T)b_i, 1,
                    NodesOnElementDG->size[1], &cc_emlrtBCI, sp);
                }

                g_tmp_data[i3] = (int8_T)((int8_T)b_i - 1);
              }

              b_nodeE2_size[1] = ii_size[1];
              input_sizes[1] = ii_size[1];
              emlrtSubAssignSizeCheckR2012b(&input_sizes[0], 2, &b_nodeE2_size[0],
                2, &f_emlrtECI, sp);
              for (i3 = 0; i3 < loop_ub; i3++) {
                NodesOnElementDG->data[(notdone + NodesOnElementDG->size[0] *
                  g_tmp_data[i3]) - 1] = NodesOnElementPBC->data[(notdone +
                  NodesOnElementPBC->size[0] * (b_ii_data[i3] - 1)) - 1];
              }

              if (*emlrtBreakCheckR2012bFlagVar != 0) {
                emlrtBreakCheckR2012b(sp);
              }
            }
          } else {
            b_guard1 = true;
          }
        } else {
          b_guard1 = true;
        }

        if (b_guard1) {
          /*  regular midside node */
          i1 = i2 + -1;
          emlrtForLoopVectorCheckR2012b(2.0, 1.0, nummat, mxDOUBLE_CLASS,
            (int32_T)nummat + -1, &ac_emlrtRTEI, sp);
          for (end = 0; end < i1; end++) {
            for (numPBC = 0; numPBC <= end; numPBC++) {
              /*  ID for material pair (row=mat2, col=mat1) */
              if ((int32_T)(numPBC + 1U) > InterTypes->size[1]) {
                emlrtDynamicBoundsCheckR2012b((int32_T)(numPBC + 1U), 1,
                  InterTypes->size[1], &qc_emlrtBCI, sp);
              }

              if ((int32_T)(end + 2U) > InterTypes->size[0]) {
                emlrtDynamicBoundsCheckR2012b((int32_T)(end + 2U), 1,
                  InterTypes->size[0], &qc_emlrtBCI, sp);
              }

              if (InterTypes->data[(end + InterTypes->size[0] * numPBC) + 1] >
                  0.0) {
                /*  Duplicate nodes along material interface */
                st.site = &ob_emlrtRSI;
                e_sparse_parenReference(&st, NodeReg->d, NodeReg->colidx,
                  NodeReg->rowidx, NodeReg->m, NodeReg->n, node, (real_T)numPBC
                  + 1.0, ElementsOnNodePBCNum, jj, ii);
                st.site = &ob_emlrtRSI;
                b_sparse_gt(&st, ElementsOnNodePBCNum, jj, h_expl_temp.d,
                            h_expl_temp.colidx, h_expl_temp.rowidx);
                st.site = &ob_emlrtRSI;
                e_sparse_parenReference(&st, NodeReg->d, NodeReg->colidx,
                  NodeReg->rowidx, NodeReg->m, NodeReg->n, node, (real_T)end +
                  2.0, ElementsOnNodePBCNum, jj, ii);
                st.site = &ob_emlrtRSI;
                b_sparse_gt(&st, ElementsOnNodePBCNum, jj, f_expl_temp.d,
                            f_expl_temp.colidx, ii);
                st.site = &ob_emlrtRSI;
                b_sparse_and(&st, h_expl_temp.d, h_expl_temp.colidx,
                             f_expl_temp.d, f_expl_temp.colidx, ii, &g_expl_temp);
                st.site = &ob_emlrtRSI;
                e_sparse_parenReference(&st, NodeReg->d, NodeReg->colidx,
                  NodeReg->rowidx, NodeReg->m, NodeReg->n, node, (real_T)end +
                  2.0, ElementsOnNodePBCNum, jj, ii);
                st.site = &ob_emlrtRSI;
                e_sparse_parenReference(&st, NodeReg->d, NodeReg->colidx,
                  NodeReg->rowidx, NodeReg->m, NodeReg->n, node, (real_T)numPBC
                  + 1.0, t5_d, ii, idx);
                st.site = &ob_emlrtRSI;
                b_sparse_eq(&st, ElementsOnNodePBCNum, jj, t5_d, ii, idx,
                            &h_expl_temp);
                st.site = &ob_emlrtRSI;
                b_sparse_and(&st, g_expl_temp.d, g_expl_temp.colidx,
                             h_expl_temp.d, h_expl_temp.colidx,
                             h_expl_temp.rowidx, &f_expl_temp);

                /*  Namely, only find nodes that are part of materials mat1 and */
                /*  mat2, but ONLY if those nodal IDs have not been reset before, */
                /*  in which case the ID for mat1 will equal the ID for mat2. */
                /*  In this way, each new nodal ID is assigned to a single */
                /*  material. */
                st.site = &nb_emlrtRSI;
                if (d_sparse_full(&st, f_expl_temp.d, f_expl_temp.colidx)) {
                  /*  Duplicate the nodal coordinates */
                  b_i2++;
                  st.site = &mb_emlrtRSI;
                  b_st.site = &iq_emlrtRSI;
                  sparse_parenAssign2D(&b_st, NodeReg, b_i2, node, (real_T)end +
                                       2.0);

                  /*  Loop over all nodes on material interface that need */
                  /*  duplicated */
                  if (((int32_T)node < 1) || ((int32_T)node >
                       ElementsOnNodeNum->size[0])) {
                    emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
                      ElementsOnNodeNum->size[0], &kk_emlrtBCI, sp);
                  }

                  st.site = &lb_emlrtRSI;
                  c_sparse_parenReference(&st, ElementsOnNode->d,
                    ElementsOnNode->colidx, ElementsOnNode->rowidx,
                    ElementsOnNode->m, ElementsOnNode->n, node, &NodeRegSum);
                  st.site = &lb_emlrtRSI;
                  c_sparse_full(&st, NodeRegSum.d, NodeRegSum.colidx,
                                NodeRegSum.rowidx, NodeRegSum.m,
                                b_NodesOnInterface);

                  /*  Loop over all elements attached to node */
                  i3 = (int32_T)ElementsOnNodeNum->data[(int32_T)node - 1];
                  emlrtForLoopVectorCheckR2012b(1.0, 1.0,
                    ElementsOnNodeNum->data[(int32_T)node - 1], mxDOUBLE_CLASS,
                    (int32_T)ElementsOnNodeNum->data[(int32_T)node - 1],
                    &yb_emlrtRTEI, sp);
                  for (j = 0; j < i3; j++) {
                    b_i = j + 1;
                    if ((b_i < 1) || (b_i > b_NodesOnInterface->size[0])) {
                      emlrtDynamicBoundsCheckR2012b(b_i, 1,
                        b_NodesOnInterface->size[0], &jk_emlrtBCI, sp);
                    }

                    b_i = RegionOnElement->size[0];
                    if (b_NodesOnInterface->data[j] != (int32_T)
                        muDoubleScalarFloor(b_NodesOnInterface->data[j])) {
                      emlrtIntegerCheckR2012b(b_NodesOnInterface->data[j],
                        &yd_emlrtDCI, sp);
                    }

                    notdone = (int32_T)b_NodesOnInterface->data[j];
                    if ((notdone < 1) || (notdone > b_i)) {
                      emlrtDynamicBoundsCheckR2012b(notdone, 1, b_i,
                        &ik_emlrtBCI, sp);
                    }

                    if (RegionOnElement->data[notdone - 1] == (real_T)end + 2.0)
                    {
                      /*  Reset nodal ID for element in higher material ID */
                      if (1.0 > nen) {
                        loop_ub = 0;
                      } else {
                        if (1 > NodesOnElementCG->size[1]) {
                          emlrtDynamicBoundsCheckR2012b(1, 1,
                            NodesOnElementCG->size[1], &gk_emlrtBCI, sp);
                        }

                        if ((int32_T)nen > NodesOnElementCG->size[1]) {
                          emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1,
                            NodesOnElementCG->size[1], &fk_emlrtBCI, sp);
                        }

                        loop_ub = (int32_T)nen;
                      }

                      st.site = &kb_emlrtRSI;
                      if (notdone > NodesOnElementCG->size[0]) {
                        emlrtDynamicBoundsCheckR2012b(notdone, 1,
                          NodesOnElementCG->size[0], &hk_emlrtBCI, &st);
                      }

                      nodeE2_size[0] = 1;
                      nodeE2_size[1] = loop_ub;
                      for (b_i = 0; b_i < loop_ub; b_i++) {
                        b_nodeE2_data[b_i] = (NodesOnElementCG->data[(notdone +
                          NodesOnElementCG->size[0] * b_i) - 1] == node);
                      }

                      b_st.site = &uh_emlrtRSI;
                      f_eml_find(&b_st, b_nodeE2_data, nodeE2_size, ii_data,
                                 ii_size);
                      loop_ub = ii_size[1];
                      for (b_i = 0; b_i < loop_ub; b_i++) {
                        trueCount = ii_data[b_i];
                        if (((int8_T)trueCount < 1) || ((int8_T)trueCount >
                             NodesOnElementDG->size[1])) {
                          emlrtDynamicBoundsCheckR2012b((int8_T)trueCount, 1,
                            NodesOnElementDG->size[1], &y_emlrtBCI, sp);
                        }

                        g_tmp_data[b_i] = (int8_T)trueCount;
                      }

                      if (notdone > NodesOnElementDG->size[0]) {
                        emlrtDynamicBoundsCheckR2012b(notdone, 1,
                          NodesOnElementDG->size[0], &sh_emlrtBCI, sp);
                      }

                      for (b_i = 0; b_i < loop_ub; b_i++) {
                        NodesOnElementDG->data[(notdone + NodesOnElementDG->
                          size[0] * (g_tmp_data[b_i] - 1)) - 1] = b_i2;
                      }

                      b_i = Coordinates->size[0];
                      if (((int32_T)node < 1) || ((int32_T)node > b_i)) {
                        emlrtDynamicBoundsCheckR2012b((int32_T)node, 1, b_i,
                          &ek_emlrtBCI, sp);
                      }

                      if (((int32_T)b_i2 < 1) || ((int32_T)b_i2 >
                           Coordinates3->size[0])) {
                        emlrtDynamicBoundsCheckR2012b((int32_T)b_i2, 1,
                          Coordinates3->size[0], &jh_emlrtBCI, sp);
                      }

                      Coordinates3->data[(int32_T)b_i2 - 1] = Coordinates->data
                        [(int32_T)node - 1];
                      Coordinates3->data[((int32_T)b_i2 + Coordinates3->size[0])
                        - 1] = Coordinates->data[((int32_T)node +
                        Coordinates->size[0]) - 1];
                      Coordinates3->data[((int32_T)b_i2 + Coordinates3->size[0] *
                                          2) - 1] = Coordinates->data[((int32_T)
                        node + Coordinates->size[0] * 2) - 1];
                    }

                    if (*emlrtBreakCheckR2012bFlagVar != 0) {
                      emlrtBreakCheckR2012b(sp);
                    }
                  }
                }
              }

              if (*emlrtBreakCheckR2012bFlagVar != 0) {
                emlrtBreakCheckR2012b(sp);
              }
            }

            if (*emlrtBreakCheckR2012bFlagVar != 0) {
              emlrtBreakCheckR2012b(sp);
            }
          }
        }
      }

      /*  midface/midedge */
    } else {
      /*  All corner nodes */
      /*  form secs; a sec is a contiguous region of elements that is not */
      /*  cut apart by any CZM facs */
      /*  start by assuming all elements are separated */
      if (i1 > ElementsOnNodeNum->size[0]) {
        emlrtDynamicBoundsCheckR2012b(i1, 1, ElementsOnNodeNum->size[0],
          &ad_emlrtBCI, sp);
      }

      b_i1 = ElementsOnNodeNum->data[i1 - 1];
      d1 = ElementsOnNodeNum->data[i1 - 1];
      numA = (int32_T)muDoubleScalarFloor(d1);
      if (d1 != numA) {
        emlrtIntegerCheckR2012b(d1, &xd_emlrtDCI, sp);
      }

      i3 = intermat2->size[0] * intermat2->size[1];
      intermat2->size[0] = (int32_T)d1;
      intermat2->size[1] = (int32_T)ElementsOnNodeNum->data[i1 - 1];
      emxEnsureCapacity_real_T(sp, intermat2, i3, &wj_emlrtRTEI);
      loop_ub = (int32_T)ElementsOnNodeNum->data[i1 - 1] * (int32_T)
        ElementsOnNodeNum->data[i1 - 1];
      for (i3 = 0; i3 < loop_ub; i3++) {
        intermat2->data[i3] = 0.0;
      }

      if (d1 < 1.0) {
        setAll->size[0] = 1;
        setAll->size[1] = 0;
      } else if (muDoubleScalarIsInf(d1) && (1.0 == d1)) {
        i3 = setAll->size[0] * setAll->size[1];
        setAll->size[0] = 1;
        setAll->size[1] = 1;
        emxEnsureCapacity_real_T(sp, setAll, i3, &qi_emlrtRTEI);
        setAll->data[0] = rtNaN;
      } else {
        i3 = setAll->size[0] * setAll->size[1];
        setAll->size[0] = 1;
        loop_ub = (int32_T)muDoubleScalarFloor(d1 - 1.0);
        setAll->size[1] = loop_ub + 1;
        emxEnsureCapacity_real_T(sp, setAll, i3, &qi_emlrtRTEI);
        for (i3 = 0; i3 <= loop_ub; i3++) {
          setAll->data[i3] = (real_T)i3 + 1.0;
        }
      }

      st.site = &jb_emlrtRSI;
      f_sparse_parenReference(&st, ElementsOnNode->d, ElementsOnNode->colidx,
        ElementsOnNode->rowidx, ElementsOnNode->m, ElementsOnNode->n, setAll,
        NodesOnInterface->data[partialTrueCount], NodeRegSum.d,
        NodeRegSum.colidx, NodeRegSum.rowidx, &NodeRegSum.m);
      st.site = &jb_emlrtRSI;
      c_sparse_full(&st, NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                    NodeRegSum.m, b_NodesOnInterface);
      i3 = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      setAll->size[1] = b_NodesOnInterface->size[0];
      emxEnsureCapacity_real_T(sp, setAll, i3, &ak_emlrtRTEI);
      loop_ub = b_NodesOnInterface->size[0];
      for (i3 = 0; i3 < loop_ub; i3++) {
        setAll->data[i3] = b_NodesOnInterface->data[i3];
      }

      if (1.0 > d1) {
        b_i = 0;
      } else {
        i3 = (int32_T)ElementsOnNodeNum->data[i1 - 1];
        if (1 > i3) {
          emlrtDynamicBoundsCheckR2012b(1, 1, i3, &ck_emlrtBCI, sp);
        }

        i3 = (int32_T)ElementsOnNodeNum->data[i1 - 1];
        b_i = (int32_T)ElementsOnNodeNum->data[i1 - 1];
        if ((b_i < 1) || (b_i > i3)) {
          emlrtDynamicBoundsCheckR2012b(b_i, 1, i3, &bk_emlrtBCI, sp);
        }
      }

      i3 = (int32_T)ElementsOnNodeNum->data[i1 - 1];
      if (1 > i3) {
        emlrtDynamicBoundsCheckR2012b(1, 1, i3, &dk_emlrtBCI, sp);
      }

      loop_ub = setAll->size[1];
      i3 = setPBC->size[0] * setPBC->size[1];
      setPBC->size[0] = 1;
      setPBC->size[1] = setAll->size[1];
      emxEnsureCapacity_real_T(sp, setPBC, i3, &fk_emlrtRTEI);
      for (i3 = 0; i3 < loop_ub; i3++) {
        setPBC->data[i3] = setAll->data[i3];
      }

      input_sizes[0] = 1;
      input_sizes[1] = b_i;
      emlrtSubAssignSizeCheckR2012b(&input_sizes[0], 2, &setPBC->size[0], 2,
        &h_emlrtECI, sp);
      loop_ub = setAll->size[1];
      for (i3 = 0; i3 < loop_ub; i3++) {
        intermat2->data[intermat2->size[0] * i3] = setAll->data[i3];
      }

      if (d1 != numA) {
        emlrtIntegerCheckR2012b(d1, &l_emlrtDCI, sp);
      }

      i3 = setnPBC->size[0];
      setnPBC->size[0] = (int32_T)d1;
      emxEnsureCapacity_real_T(sp, setnPBC, i3, &jk_emlrtRTEI);
      if (d1 != numA) {
        emlrtIntegerCheckR2012b(d1, &l_emlrtDCI, sp);
      }

      loop_ub = (int32_T)d1;
      for (i3 = 0; i3 < loop_ub; i3++) {
        setnPBC->data[i3] = 1.0;
      }

      i3 = (int32_T)d;
      emlrtForLoopVectorCheckR2012b(1.0, 1.0, FacetsOnNodeNum->data[(int32_T)
        NodesOnInterface->data[partialTrueCount] - 1], mxDOUBLE_CLASS, (int32_T)
        FacetsOnNodeNum->data[(int32_T)NodesOnInterface->data[partialTrueCount]
        - 1], &xb_emlrtRTEI, sp);
      for (locF = 0; locF < i3; locF++) {
        st.site = &ib_emlrtRSI;
        e_sparse_parenReference(&st, FacetsOnNodeInt->d, FacetsOnNodeInt->colidx,
          FacetsOnNodeInt->rowidx, FacetsOnNodeInt->m, FacetsOnNodeInt->n,
          (real_T)locF + 1.0, node, ElementsOnNodePBCNum, jj, ii);
        st.site = &hb_emlrtRSI;
        b_sparse_gt(&st, ElementsOnNodePBCNum, jj, h_expl_temp.d,
                    h_expl_temp.colidx, h_expl_temp.rowidx);
        st.site = &hb_emlrtRSI;
        if (d_sparse_full(&st, h_expl_temp.d, h_expl_temp.colidx)) {
          /*  exclude internal facs */
          /*                  intramattrue = ~isempty(find(matI==diag(intermat2),1)); I */
          /*                  found out that the code is only putting cut in */
          /*                  nodeedgecut for intermaterials, not the diagonal of */
          /*                  intermat2, so no if-test is needed */
          st.site = &gb_emlrtRSI;
          e_sparse_parenReference(&st, FacetsOnNodeCut->d,
            FacetsOnNodeCut->colidx, FacetsOnNodeCut->rowidx, FacetsOnNodeCut->m,
            FacetsOnNodeCut->n, (real_T)locF + 1.0, node, ElementsOnNodePBCNum,
            jj, ii);

          /*                  if ~cuttrue || intramattrue % two elements should be joined into one sec, along with their neighbors currently in the sec */
          st.site = &fb_emlrtRSI;
          sparse_not(&st, ElementsOnNodePBCNum, jj, ii, h_expl_temp.d,
                     h_expl_temp.colidx, h_expl_temp.rowidx);
          st.site = &fb_emlrtRSI;
          if (d_sparse_full(&st, h_expl_temp.d, h_expl_temp.colidx)) {
            /*  two elements should be joined into one sec, along with their neighbors currently in the sec */
            st.site = &eb_emlrtRSI;
            e_sparse_parenReference(&st, FacetsOnNode->d, FacetsOnNode->colidx,
              FacetsOnNode->rowidx, FacetsOnNode->m, FacetsOnNode->n, (real_T)
              locF + 1.0, node, ElementsOnNodePBCNum, jj, ii);
            st.site = &eb_emlrtRSI;
            numA = sparse_full(&st, ElementsOnNodePBCNum, jj);
            if (numA != (int32_T)muDoubleScalarFloor(numA)) {
              emlrtIntegerCheckR2012b(numA, &i_emlrtDCI, sp);
            }

            if (((int32_T)numA < 1) || ((int32_T)numA > ElementsOnFacet->size[0]))
            {
              emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1,
                ElementsOnFacet->size[0], &wb_emlrtBCI, sp);
            }

            notdone = (int32_T)ElementsOnFacet->data[(int32_T)numA - 1];
            if (((int32_T)numA < 1) || ((int32_T)numA > ElementsOnFacet->size[0]))
            {
              emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1,
                ElementsOnFacet->size[0], &ub_emlrtBCI, sp);
            }

            lenn = (int32_T)ElementsOnFacet->data[((int32_T)numA +
              ElementsOnFacet->size[0]) - 1];

            /*  find the secs for each element */
            nri = 0U;
            old = false;
            while ((nri < b_i1) && (!old)) {
              nri++;
              if (((int32_T)nri < 1) || ((int32_T)nri > setnPBC->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, setnPBC->size[0],
                  &kb_emlrtBCI, sp);
              }

              if (setnPBC->data[(int32_T)nri - 1] > 0.0) {
                if (((int32_T)nri < 1) || ((int32_T)nri > setnPBC->size[0])) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, setnPBC->size[0],
                    &ak_emlrtBCI, sp);
                }

                d = setnPBC->data[(int32_T)nri - 1];
                if (1.0 > d) {
                  loop_ub = 0;
                } else {
                  if (1 > intermat2->size[0]) {
                    emlrtDynamicBoundsCheckR2012b(1, 1, intermat2->size[0],
                      &yj_emlrtBCI, sp);
                  }

                  if (((int32_T)d < 1) || ((int32_T)setnPBC->data[(int32_T)nri -
                       1] > intermat2->size[0])) {
                    emlrtDynamicBoundsCheckR2012b((int32_T)setnPBC->data
                      [(int32_T)nri - 1], 1, intermat2->size[0], &xj_emlrtBCI,
                      sp);
                  }

                  loop_ub = (int32_T)setnPBC->data[(int32_T)nri - 1];
                }

                if (((int32_T)nri < 1) || ((int32_T)nri > intermat2->size[1])) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, intermat2->
                    size[1], &wj_emlrtBCI, sp);
                }

                st.site = &db_emlrtRSI;
                b_i = b_NodesOnPBCnum->size[0];
                b_NodesOnPBCnum->size[0] = loop_ub;
                emxEnsureCapacity_boolean_T(&st, b_NodesOnPBCnum, b_i,
                  &ok_emlrtRTEI);
                for (b_i = 0; b_i < loop_ub; b_i++) {
                  b_NodesOnPBCnum->data[b_i] = (intermat2->data[b_i +
                    intermat2->size[0] * ((int32_T)nri - 1)] == notdone);
                }

                b_st.site = &uh_emlrtRSI;
                eml_find(&b_st, b_NodesOnPBCnum, ii);

                /*  find is MUCH faster than ismember */
                b_i = ElementsOnNodePBCNum->size[0];
                ElementsOnNodePBCNum->size[0] = ii->size[0];
                emxEnsureCapacity_real_T(sp, ElementsOnNodePBCNum, b_i,
                  &kj_emlrtRTEI);
                loop_ub = ii->size[0];
                for (b_i = 0; b_i < loop_ub; b_i++) {
                  ElementsOnNodePBCNum->data[b_i] = ii->data[b_i];
                }

                st.site = &cb_emlrtRSI;
                old = any(&st, ElementsOnNodePBCNum);

                /*                          if sec1>0 */
                /*                              break */
                /*                          end */
              }

              if (*emlrtBreakCheckR2012bFlagVar != 0) {
                emlrtBreakCheckR2012b(sp);
              }
            }

            iSec2 = 0U;
            old = false;
            while ((iSec2 < b_i1) && (!old)) {
              iSec2++;
              if (((int32_T)iSec2 < 1) || ((int32_T)iSec2 > setnPBC->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)iSec2, 1, setnPBC->size[0],
                  &fb_emlrtBCI, sp);
              }

              d = setnPBC->data[(int32_T)iSec2 - 1];
              if (d > 0.0) {
                if (((int32_T)iSec2 < 1) || ((int32_T)iSec2 > setnPBC->size[0]))
                {
                  emlrtDynamicBoundsCheckR2012b((int32_T)iSec2, 1, setnPBC->
                    size[0], &vj_emlrtBCI, sp);
                }

                if (1.0 > d) {
                  loop_ub = 0;
                } else {
                  if (1 > intermat2->size[0]) {
                    emlrtDynamicBoundsCheckR2012b(1, 1, intermat2->size[0],
                      &uj_emlrtBCI, sp);
                  }

                  loop_ub = (int32_T)setnPBC->data[(int32_T)iSec2 - 1];
                  if ((loop_ub < 1) || (loop_ub > intermat2->size[0])) {
                    emlrtDynamicBoundsCheckR2012b((int32_T)setnPBC->data
                      [(int32_T)iSec2 - 1], 1, intermat2->size[0], &tj_emlrtBCI,
                      sp);
                  }
                }

                if (((int32_T)iSec2 < 1) || ((int32_T)iSec2 > intermat2->size[1]))
                {
                  emlrtDynamicBoundsCheckR2012b((int32_T)iSec2, 1,
                    intermat2->size[1], &sj_emlrtBCI, sp);
                }

                st.site = &bb_emlrtRSI;
                b_i = b_NodesOnPBCnum->size[0];
                b_NodesOnPBCnum->size[0] = loop_ub;
                emxEnsureCapacity_boolean_T(&st, b_NodesOnPBCnum, b_i,
                  &pk_emlrtRTEI);
                for (b_i = 0; b_i < loop_ub; b_i++) {
                  b_NodesOnPBCnum->data[b_i] = (lenn == intermat2->data[b_i +
                    intermat2->size[0] * ((int32_T)iSec2 - 1)]);
                }

                b_st.site = &uh_emlrtRSI;
                eml_find(&b_st, b_NodesOnPBCnum, ii);
                b_i = ElementsOnNodePBCNum->size[0];
                ElementsOnNodePBCNum->size[0] = ii->size[0];
                emxEnsureCapacity_real_T(sp, ElementsOnNodePBCNum, b_i,
                  &kj_emlrtRTEI);
                loop_ub = ii->size[0];
                for (b_i = 0; b_i < loop_ub; b_i++) {
                  ElementsOnNodePBCNum->data[b_i] = ii->data[b_i];
                }

                st.site = &ab_emlrtRSI;
                old = any(&st, ElementsOnNodePBCNum);

                /*                          if sec2>0 */
                /*                              break */
                /*                          end */
              }

              if (*emlrtBreakCheckR2012bFlagVar != 0) {
                emlrtBreakCheckR2012b(sp);
              }
            }

            /*  merge secs */
            if ((int32_T)iSec2 != (int32_T)nri) {
              if (((int32_T)iSec2 < 1) || ((int32_T)iSec2 > setnPBC->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)iSec2, 1, setnPBC->size[0],
                  &rj_emlrtBCI, sp);
              }

              d = setnPBC->data[(int32_T)iSec2 - 1];
              if (1.0 > d) {
                loop_ub = 0;
              } else {
                if (1 > intermat2->size[0]) {
                  emlrtDynamicBoundsCheckR2012b(1, 1, intermat2->size[0],
                    &qj_emlrtBCI, sp);
                }

                if (((int32_T)d < 1) || ((int32_T)setnPBC->data[(int32_T)iSec2 -
                     1] > intermat2->size[0])) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)setnPBC->data[(int32_T)
                    iSec2 - 1], 1, intermat2->size[0], &pj_emlrtBCI, sp);
                }

                loop_ub = (int32_T)setnPBC->data[(int32_T)iSec2 - 1];
              }

              if (((int32_T)nri < 1) || ((int32_T)nri > setnPBC->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, setnPBC->size[0],
                  &nj_emlrtBCI, sp);
              }

              d1 = setnPBC->data[(int32_T)nri - 1];
              if (1.0 > d1) {
                b_loop_ub = 0;
              } else {
                if (1 > intermat2->size[0]) {
                  emlrtDynamicBoundsCheckR2012b(1, 1, intermat2->size[0],
                    &mj_emlrtBCI, sp);
                }

                b_loop_ub = (int32_T)setnPBC->data[(int32_T)nri - 1];
                if ((b_loop_ub < 1) || (b_loop_ub > intermat2->size[0])) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)setnPBC->data[(int32_T)
                    nri - 1], 1, intermat2->size[0], &lj_emlrtBCI, sp);
                }
              }

              if (((int32_T)nri < 1) || ((int32_T)nri > setnPBC->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, setnPBC->size[0],
                  &v_emlrtBCI, sp);
              }

              if (((int32_T)iSec2 < 1) || ((int32_T)iSec2 > setnPBC->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)iSec2, 1, setnPBC->size[0],
                  &w_emlrtBCI, sp);
              }

              d += d1;
              if (1.0 > d) {
                b_i = 0;
              } else {
                if (1 > intermat2->size[0]) {
                  emlrtDynamicBoundsCheckR2012b(1, 1, intermat2->size[0],
                    &jj_emlrtBCI, sp);
                }

                if (((int32_T)d < 1) || ((int32_T)d > intermat2->size[0])) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, intermat2->size[0],
                    &ij_emlrtBCI, sp);
                }

                b_i = (int32_T)d;
              }

              if (((int32_T)iSec2 < 1) || ((int32_T)iSec2 > intermat2->size[1]))
              {
                emlrtDynamicBoundsCheckR2012b((int32_T)iSec2, 1, intermat2->
                  size[1], &hj_emlrtBCI, sp);
              }

              st.site = &y_emlrtRSI;
              if (((int32_T)iSec2 < 1) || ((int32_T)iSec2 > intermat2->size[1]))
              {
                emlrtDynamicBoundsCheckR2012b((int32_T)iSec2, 1, intermat2->
                  size[1], &oj_emlrtBCI, &st);
              }

              if (((int32_T)nri < 1) || ((int32_T)nri > intermat2->size[1])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, intermat2->size[1],
                  &kj_emlrtBCI, &st);
              }

              trueCount = MorePBC->size[0];
              MorePBC->size[0] = loop_ub + b_loop_ub;
              emxEnsureCapacity_real_T(&st, MorePBC, trueCount, &rk_emlrtRTEI);
              for (trueCount = 0; trueCount < loop_ub; trueCount++) {
                MorePBC->data[trueCount] = intermat2->data[trueCount +
                  intermat2->size[0] * ((int32_T)iSec2 - 1)];
              }

              for (trueCount = 0; trueCount < b_loop_ub; trueCount++) {
                MorePBC->data[trueCount + loop_ub] = intermat2->data[trueCount +
                  intermat2->size[0] * ((int32_T)nri - 1)];
              }

              b_st.site = &cj_emlrtRSI;
              c_sort(&b_st, MorePBC);
              emlrtSubAssignSizeCheckR2012b(&b_i, 1, &MorePBC->size[0], 1,
                &g_emlrtECI, sp);
              loop_ub = MorePBC->size[0];
              for (b_i = 0; b_i < loop_ub; b_i++) {
                intermat2->data[b_i + intermat2->size[0] * ((int32_T)iSec2 - 1)]
                  = MorePBC->data[b_i];
              }

              if (((int32_T)nri < 1) || ((int32_T)nri > intermat2->size[1])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, intermat2->size[1],
                  &gj_emlrtBCI, sp);
              }

              loop_ub = intermat2->size[0];
              for (b_i = 0; b_i < loop_ub; b_i++) {
                intermat2->data[b_i + intermat2->size[0] * ((int32_T)nri - 1)] =
                  0.0;
              }

              if (((int32_T)iSec2 < 1) || ((int32_T)iSec2 > setnPBC->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)iSec2, 1, setnPBC->size[0],
                  &m_emlrtBCI, sp);
              }

              if (((int32_T)nri < 1) || ((int32_T)nri > setnPBC->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, setnPBC->size[0],
                  &n_emlrtBCI, sp);
              }

              if (((int32_T)iSec2 < 1) || ((int32_T)iSec2 > setnPBC->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)iSec2, 1, setnPBC->size[0],
                  &o_emlrtBCI, sp);
              }

              setnPBC->data[(int32_T)iSec2 - 1] = d;
              if (((int32_T)nri < 1) || ((int32_T)nri > setnPBC->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, setnPBC->size[0],
                  &l_emlrtBCI, sp);
              }

              setnPBC->data[(int32_T)nri - 1] = 0.0;
            }
          }
        }

        /*  if external edge */
        if (*emlrtBreakCheckR2012bFlagVar != 0) {
          emlrtBreakCheckR2012b(sp);
        }
      }

      /*  assign node IDs to each sec */
      st.site = &x_emlrtRSI;
      if (muDoubleScalarIsNaN(usePBC)) {
        emlrtErrorWithMessageIdR2018a(&st, &mc_emlrtRTEI, "MATLAB:nologicalnan",
          "MATLAB:nologicalnan", 0);
      }

      b_guard1 = false;
      if (usePBC != 0.0) {
        if (i1 > NodesOnPBCnum->size[0]) {
          emlrtDynamicBoundsCheckR2012b(i1, 1, NodesOnPBCnum->size[0],
            &gc_emlrtBCI, sp);
        }

        if (NodesOnPBCnum->data[i1 - 1] > 0.0) {
          /*  handle copies of PBC nodes specially */
          if (i1 > NodesOnPBCnum->size[0]) {
            emlrtDynamicBoundsCheckR2012b(i1, 1, NodesOnPBCnum->size[0],
              &dc_emlrtBCI, sp);
          }

          b_i1 = NodesOnPBCnum->data[i1 - 1] + 1.0;
          d = NodesOnPBCnum->data[i1 - 1] + 1.0;
          d1 = (int32_T)muDoubleScalarFloor(d);
          if (d != d1) {
            emlrtIntegerCheckR2012b(d, &j_emlrtDCI, sp);
          }

          i3 = MorePBC->size[0];
          MorePBC->size[0] = (int32_T)d;
          emxEnsureCapacity_real_T(sp, MorePBC, i3, &nk_emlrtRTEI);
          if (d != d1) {
            emlrtIntegerCheckR2012b(d, &j_emlrtDCI, sp);
          }

          loop_ub = (int32_T)d;
          for (i3 = 0; i3 < loop_ub; i3++) {
            MorePBC->data[i3] = 0.0;
          }

          i1 = (int32_T)ElementsOnNodeNum->data[i1 - 1];
          emlrtForLoopVectorCheckR2012b(1.0, 1.0, ElementsOnNodeNum->data
            [(int32_T)NodesOnInterface->data[partialTrueCount] - 1],
            mxDOUBLE_CLASS, (int32_T)ElementsOnNodeNum->data[(int32_T)
            NodesOnInterface->data[partialTrueCount] - 1], &wb_emlrtRTEI, sp);
          for (numPBC = 0; numPBC < i1; numPBC++) {
            if (((int32_T)(numPBC + 1U) < 1) || ((int32_T)(numPBC + 1U) >
                 setnPBC->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)(numPBC + 1U), 1,
                setnPBC->size[0], &xb_emlrtBCI, sp);
            }

            if (setnPBC->data[numPBC] > 0.0) {
              if (1 > intermat2->size[0]) {
                emlrtDynamicBoundsCheckR2012b(1, 1, intermat2->size[0],
                  &fj_emlrtBCI, sp);
              }

              i3 = (int32_T)setnPBC->data[numPBC];
              if ((i3 < 1) || (i3 > intermat2->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)setnPBC->data[numPBC], 1,
                  intermat2->size[0], &ej_emlrtBCI, sp);
              }

              b_i = numPBC + 1;
              if ((b_i < 1) || (b_i > intermat2->size[1])) {
                emlrtDynamicBoundsCheckR2012b(b_i, 1, intermat2->size[1],
                  &dj_emlrtBCI, sp);
              }

              if (1.0 > nen) {
                loop_ub = 0;
              } else {
                if (1 > NodesOnElementCG->size[1]) {
                  emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementCG->size[1],
                    &bj_emlrtBCI, sp);
                }

                if ((int32_T)nen > NodesOnElementCG->size[1]) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1,
                    NodesOnElementCG->size[1], &aj_emlrtBCI, sp);
                }

                loop_ub = (int32_T)nen;
              }

              st.site = &w_emlrtRSI;
              if (intermat2->data[intermat2->size[0] * numPBC] != (int32_T)
                  muDoubleScalarFloor(intermat2->data[intermat2->size[0] *
                                      numPBC])) {
                emlrtIntegerCheckR2012b(intermat2->data[intermat2->size[0] *
                  numPBC], &wd_emlrtDCI, &st);
              }

              lenn = (int32_T)intermat2->data[intermat2->size[0] * numPBC];
              if ((lenn < 1) || (lenn > NodesOnElementCG->size[0])) {
                emlrtDynamicBoundsCheckR2012b(lenn, 1, NodesOnElementCG->size[0],
                  &cj_emlrtBCI, &st);
              }

              nodeE2_size[0] = 1;
              nodeE2_size[1] = loop_ub;
              for (trueCount = 0; trueCount < loop_ub; trueCount++) {
                b_nodeE2_data[trueCount] = (NodesOnElementCG->data[(lenn +
                  NodesOnElementCG->size[0] * trueCount) - 1] == node);
              }

              b_st.site = &uh_emlrtRSI;
              f_eml_find(&b_st, b_nodeE2_data, nodeE2_size, ii_data, ii_size);
              loop_ub = ii_size[0] * ii_size[1];
              for (trueCount = 0; trueCount < loop_ub; trueCount++) {
                nodeE2_data[trueCount] = ii_data[trueCount];
              }

              if (lenn > NodesOnElementPBC->size[0]) {
                emlrtDynamicBoundsCheckR2012b(lenn, 1, NodesOnElementPBC->size[0],
                  &yi_emlrtBCI, sp);
              }

              loop_ub = ii_size[1];
              for (trueCount = 0; trueCount < loop_ub; trueCount++) {
                elemB = ii_data[trueCount];
                if ((elemB < 1) || (elemB > NodesOnElementPBC->size[1])) {
                  emlrtDynamicBoundsCheckR2012b(elemB, 1,
                    NodesOnElementPBC->size[1], &bb_emlrtBCI, sp);
                }
              }

              if (1 > ii_size[1]) {
                emlrtDynamicBoundsCheckR2012b(1, 1, ii_size[1], &xi_emlrtBCI, sp);
              }

              numA = NodesOnElementPBC->data[(lenn + NodesOnElementPBC->size[0] *
                ((int32_T)nodeE2_data[0] - 1)) - 1];
              if (NodesOnElementPBC->data[(lenn + NodesOnElementPBC->size[0] *
                   (ii_data[0] - 1)) - 1] > node) {
                if (1.0 > b_i1 - 1.0) {
                  loop_ub = 0;
                } else {
                  if (((int32_T)(b_i1 - 1.0) < 1) || ((int32_T)(b_i1 - 1.0) >
                       100)) {
                    emlrtDynamicBoundsCheckR2012b((int32_T)(b_i1 - 1.0), 1, 100,
                      &wi_emlrtBCI, sp);
                  }

                  loop_ub = (int32_T)(b_i1 - 1.0);
                }

                st.site = &v_emlrtRSI;
                if (((int32_T)node < 1) || ((int32_T)node > NodesOnPBC->size[1]))
                {
                  emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
                    NodesOnPBC->size[1], &vi_emlrtBCI, &st);
                }

                c_NodesOnPBC_size[0] = loop_ub;
                for (trueCount = 0; trueCount < loop_ub; trueCount++) {
                  e_NodesOnPBC_data[trueCount] = (NodesOnPBC->data[trueCount +
                    100 * ((int32_T)node - 1)] == numA);
                }

                d_NodesOnPBC_data.data = &e_NodesOnPBC_data[0];
                d_NodesOnPBC_data.size = &c_NodesOnPBC_size[0];
                d_NodesOnPBC_data.allocatedSize = 100;
                d_NodesOnPBC_data.numDimensions = 1;
                d_NodesOnPBC_data.canFreeData = false;
                b_st.site = &uh_emlrtRSI;
                eml_find(&b_st, &d_NodesOnPBC_data, ii);
                links2_size[0] = ii->size[0];
                loop_ub = ii->size[0];
                for (trueCount = 0; trueCount < loop_ub; trueCount++) {
                  links2_data[trueCount] = (real_T)ii->data[trueCount] + 1.0;
                }
              } else {
                links2_size[0] = 1;
                links2_data[0] = 1.0;
              }

              NodesOnPBC_size[0] = links2_size[0];
              loop_ub = links2_size[0];
              for (trueCount = 0; trueCount < loop_ub; trueCount++) {
                elemB = (int32_T)links2_data[trueCount];
                if ((elemB < 1) || (elemB > MorePBC->size[0])) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)links2_data[trueCount],
                    1, MorePBC->size[0], &t_emlrtBCI, sp);
                }

                NodesOnPBC_data[trueCount] = MorePBC->data[elemB - 1];
              }

              b_NodesOnPBC_size[0] = NodesOnPBC_size[0];
              st.site = &u_emlrtRSI;
              if (b_ifWhileCond(&st, NodesOnPBC_data, b_NodesOnPBC_size)) {
                if (1 > intermat2->size[0]) {
                  emlrtDynamicBoundsCheckR2012b(1, 1, intermat2->size[0],
                    &ui_emlrtBCI, sp);
                }

                if (i3 > intermat2->size[0]) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)setnPBC->data[numPBC],
                    1, intermat2->size[0], &ti_emlrtBCI, sp);
                }

                if (b_i > intermat2->size[1]) {
                  emlrtDynamicBoundsCheckR2012b(b_i, 1, intermat2->size[1],
                    &si_emlrtBCI, sp);
                }

                b_i2++;
                b_i = Coordinates->size[0];
                d = NodesOnElementPBC->data[(lenn + NodesOnElementPBC->size[0] *
                  (ii_data[0] - 1)) - 1];
                if (d != (int32_T)muDoubleScalarFloor(d)) {
                  emlrtIntegerCheckR2012b(d, &vd_emlrtDCI, sp);
                }

                if (((int32_T)d < 1) || ((int32_T)d > b_i)) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, b_i, &ri_emlrtBCI,
                    sp);
                }

                if (((int32_T)b_i2 < 1) || ((int32_T)b_i2 > Coordinates3->size[0]))
                {
                  emlrtDynamicBoundsCheckR2012b((int32_T)b_i2, 1,
                    Coordinates3->size[0], &ih_emlrtBCI, sp);
                }

                Coordinates3->data[(int32_T)b_i2 - 1] = Coordinates->data
                  [(int32_T)d - 1];
                Coordinates3->data[((int32_T)b_i2 + Coordinates3->size[0]) - 1] =
                  Coordinates->data[((int32_T)d + Coordinates->size[0]) - 1];
                Coordinates3->data[((int32_T)b_i2 + Coordinates3->size[0] * 2) -
                  1] = Coordinates->data[((int32_T)d + Coordinates->size[0] * 2)
                  - 1];

                /*  Loop over all elements attached to node */
                emlrtForLoopVectorCheckR2012b(1.0, 1.0, setnPBC->data[numPBC],
                  mxDOUBLE_CLASS, (int32_T)setnPBC->data[numPBC], &vb_emlrtRTEI,
                  sp);
                if (0 <= (int32_T)setnPBC->data[numPBC] - 1) {
                  i7 = i3;
                  d = ElementsOnNodeNum->data[(int32_T)node - 1];
                  if (d < 1.0) {
                    setAll->size[0] = 1;
                    setAll->size[1] = 0;
                  } else if (muDoubleScalarIsInf(d) && (1.0 == d)) {
                    b_i = setAll->size[0] * setAll->size[1];
                    setAll->size[0] = 1;
                    setAll->size[1] = 1;
                    emxEnsureCapacity_real_T(sp, setAll, b_i, &qi_emlrtRTEI);
                    setAll->data[0] = rtNaN;
                  } else {
                    b_i = setAll->size[0] * setAll->size[1];
                    setAll->size[0] = 1;
                    setAll->size[1] = (int32_T)muDoubleScalarFloor
                      (ElementsOnNodeNum->data[(int32_T)node - 1] - 1.0) + 1;
                    emxEnsureCapacity_real_T(sp, setAll, b_i, &qi_emlrtRTEI);
                    loop_ub = (int32_T)muDoubleScalarFloor
                      (ElementsOnNodeNum->data[(int32_T)node - 1] - 1.0);
                    for (b_i = 0; b_i <= loop_ub; b_i++) {
                      setAll->data[b_i] = (real_T)b_i + 1.0;
                    }
                  }

                  if (1.0 > nen) {
                    h_loop_ub = 0;
                  } else {
                    if (1 > NodesOnElementCG->size[1]) {
                      emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementCG->
                        size[1], &mi_emlrtBCI, sp);
                    }

                    if ((int32_T)nen > NodesOnElementCG->size[1]) {
                      emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1,
                        NodesOnElementCG->size[1], &li_emlrtBCI, sp);
                    }

                    h_loop_ub = (int32_T)nen;
                  }
                }

                if (0 <= i3 - 1) {
                  nodeE2_size[0] = 1;
                  nodeE2_size[1] = h_loop_ub;
                }

                for (elemB = 0; elemB < i3; elemB++) {
                  b_i = elemB + 1;
                  if (b_i > i7) {
                    emlrtDynamicBoundsCheckR2012b(b_i, 1, i7, &qi_emlrtBCI, sp);
                  }

                  b_i = RegionOnElement->size[0];
                  if (intermat2->data[elemB + intermat2->size[0] * numPBC] !=
                      (int32_T)muDoubleScalarFloor(intermat2->data[elemB +
                       intermat2->size[0] * numPBC])) {
                    emlrtIntegerCheckR2012b(intermat2->data[elemB +
                      intermat2->size[0] * numPBC], &ud_emlrtDCI, sp);
                  }

                  lenn = (int32_T)intermat2->data[elemB + intermat2->size[0] *
                    numPBC];
                  if ((lenn < 1) || (lenn > b_i)) {
                    emlrtDynamicBoundsCheckR2012b(lenn, 1, b_i, &pi_emlrtBCI, sp);
                  }

                  st.site = &t_emlrtRSI;
                  b_st.site = &iq_emlrtRSI;
                  sparse_parenAssign2D(&b_st, NodeReg, b_i2, numA,
                                       RegionOnElement->data[(int32_T)
                                       intermat2->data[elemB + intermat2->size[0]
                                       * numPBC] - 1]);
                  if (((int32_T)node < 1) || ((int32_T)node >
                       ElementsOnNodeNum->size[0])) {
                    emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
                      ElementsOnNodeNum->size[0], &oi_emlrtBCI, sp);
                  }

                  st.site = &s_emlrtRSI;
                  b_st.site = &s_emlrtRSI;
                  f_sparse_parenReference(&b_st, ElementsOnNode->d,
                    ElementsOnNode->colidx, ElementsOnNode->rowidx,
                    ElementsOnNode->m, ElementsOnNode->n, setAll, node,
                    NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                    &NodeRegSum.m);
                  b_st.site = &s_emlrtRSI;
                  c_sparse_eq(&b_st, intermat2->data[elemB + intermat2->size[0] *
                              numPBC], NodeRegSum.d, NodeRegSum.colidx,
                              NodeRegSum.rowidx, NodeRegSum.m, &d_expl_temp);
                  b_i = c_expl_temp.colidx->size[0];
                  c_expl_temp.colidx->size[0] = d_expl_temp.colidx->size[0];
                  emxEnsureCapacity_int32_T(&st, c_expl_temp.colidx, b_i,
                    &uk_emlrtRTEI);
                  loop_ub = d_expl_temp.colidx->size[0];
                  for (b_i = 0; b_i < loop_ub; b_i++) {
                    c_expl_temp.colidx->data[b_i] = d_expl_temp.colidx->data[b_i];
                  }

                  b_i = c_expl_temp.rowidx->size[0];
                  c_expl_temp.rowidx->size[0] = d_expl_temp.rowidx->size[0];
                  emxEnsureCapacity_int32_T(&st, c_expl_temp.rowidx, b_i,
                    &uk_emlrtRTEI);
                  loop_ub = d_expl_temp.rowidx->size[0];
                  for (b_i = 0; b_i < loop_ub; b_i++) {
                    c_expl_temp.rowidx->data[b_i] = d_expl_temp.rowidx->data[b_i];
                  }

                  b_st.site = &uh_emlrtRSI;
                  g_eml_find(&b_st, c_expl_temp.colidx, c_expl_temp.rowidx,
                             d_expl_temp.m, ii);
                  b_i = ElementsOnNodePBCNum->size[0];
                  ElementsOnNodePBCNum->size[0] = ii->size[0];
                  emxEnsureCapacity_real_T(sp, ElementsOnNodePBCNum, b_i,
                    &kj_emlrtRTEI);
                  loop_ub = ii->size[0];
                  for (b_i = 0; b_i < loop_ub; b_i++) {
                    ElementsOnNodePBCNum->data[b_i] = ii->data[b_i];
                  }

                  st.site = &e_emlrtRSI;
                  c_sparse_parenAssign(&st, ElementsOnNodeDup, b_i2,
                                       ElementsOnNodePBCNum, node);

                  /*  Reset nodal ID for element in higher material ID */
                  st.site = &r_emlrtRSI;
                  d = intermat2->data[elemB + intermat2->size[0] * numPBC];
                  if (((int32_T)d < 1) || ((int32_T)d > NodesOnElementCG->size[0]))
                  {
                    emlrtDynamicBoundsCheckR2012b((int32_T)d, 1,
                      NodesOnElementCG->size[0], &ni_emlrtBCI, &st);
                  }

                  for (b_i = 0; b_i < h_loop_ub; b_i++) {
                    b_nodeE2_data[b_i] = (NodesOnElementCG->data[((int32_T)d +
                      NodesOnElementCG->size[0] * b_i) - 1] == node);
                  }

                  b_st.site = &uh_emlrtRSI;
                  f_eml_find(&b_st, b_nodeE2_data, nodeE2_size, ii_data, ii_size);
                  loop_ub = ii_size[1];
                  for (b_i = 0; b_i < loop_ub; b_i++) {
                    trueCount = ii_data[b_i];
                    if (((int8_T)trueCount < 1) || ((int8_T)trueCount >
                         NodesOnElementDG->size[1])) {
                      emlrtDynamicBoundsCheckR2012b((int8_T)trueCount, 1,
                        NodesOnElementDG->size[1], &e_emlrtBCI, sp);
                    }

                    g_tmp_data[b_i] = (int8_T)trueCount;
                  }

                  if (lenn > NodesOnElementDG->size[0]) {
                    emlrtDynamicBoundsCheckR2012b(lenn, 1,
                      NodesOnElementDG->size[0], &rh_emlrtBCI, sp);
                  }

                  for (b_i = 0; b_i < loop_ub; b_i++) {
                    NodesOnElementDG->data[(lenn + NodesOnElementDG->size[0] *
                      (g_tmp_data[b_i] - 1)) - 1] = b_i2;
                  }

                  if (*emlrtBreakCheckR2012bFlagVar != 0) {
                    emlrtBreakCheckR2012b(sp);
                  }
                }
              } else {
                ia_size[0] = links2_size[0];
                loop_ub = links2_size[0];
                for (b_i = 0; b_i < loop_ub; b_i++) {
                  trueCount = (int32_T)links2_data[b_i];
                  if ((trueCount < 1) || (trueCount > MorePBC->size[0])) {
                    emlrtDynamicBoundsCheckR2012b((int32_T)links2_data[b_i], 1,
                      MorePBC->size[0], &p_emlrtBCI, sp);
                  }

                  ia_data[b_i] = trueCount;
                }

                loop_ub = ia_size[0];
                for (b_i = 0; b_i < loop_ub; b_i++) {
                  MorePBC->data[ia_data[b_i] - 1] = 1.0;
                }

                emlrtForLoopVectorCheckR2012b(1.0, 1.0, setnPBC->data[numPBC],
                  mxDOUBLE_CLASS, (int32_T)setnPBC->data[numPBC], &ub_emlrtRTEI,
                  sp);
                d = ElementsOnNodeNum->data[(int32_T)node - 1];
                if (d < 1.0) {
                  setAll->size[0] = 1;
                  setAll->size[1] = 0;
                } else if (muDoubleScalarIsInf(d) && (1.0 == d)) {
                  b_i = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  setAll->size[1] = 1;
                  emxEnsureCapacity_real_T(sp, setAll, b_i, &qi_emlrtRTEI);
                  setAll->data[0] = rtNaN;
                } else {
                  b_i = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  setAll->size[1] = (int32_T)muDoubleScalarFloor
                    (ElementsOnNodeNum->data[(int32_T)node - 1] - 1.0) + 1;
                  emxEnsureCapacity_real_T(sp, setAll, b_i, &qi_emlrtRTEI);
                  loop_ub = (int32_T)muDoubleScalarFloor(ElementsOnNodeNum->
                    data[(int32_T)node - 1] - 1.0);
                  for (b_i = 0; b_i <= loop_ub; b_i++) {
                    setAll->data[b_i] = (real_T)b_i + 1.0;
                  }
                }

                if (1.0 > nen) {
                  loop_ub = 0;
                } else {
                  if (1 > NodesOnElementCG->size[1]) {
                    emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementCG->size[1],
                      &gi_emlrtBCI, sp);
                  }

                  if ((int32_T)nen > NodesOnElementCG->size[1]) {
                    emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1,
                      NodesOnElementCG->size[1], &fi_emlrtBCI, sp);
                  }

                  loop_ub = (int32_T)nen;
                }

                if (0 <= i3 - 1) {
                  nodeE2_size[0] = 1;
                  nodeE2_size[1] = loop_ub;
                }

                for (elemB = 0; elemB < i3; elemB++) {
                  b_i = elemB + 1;
                  if (b_i > i3) {
                    emlrtDynamicBoundsCheckR2012b(b_i, 1, i3, &ki_emlrtBCI, sp);
                  }

                  b_i = RegionOnElement->size[0];
                  if (intermat2->data[elemB + intermat2->size[0] * numPBC] !=
                      (int32_T)muDoubleScalarFloor(intermat2->data[elemB +
                       intermat2->size[0] * numPBC])) {
                    emlrtIntegerCheckR2012b(intermat2->data[elemB +
                      intermat2->size[0] * numPBC], &td_emlrtDCI, sp);
                  }

                  lenn = (int32_T)intermat2->data[elemB + intermat2->size[0] *
                    numPBC];
                  if ((lenn < 1) || (lenn > b_i)) {
                    emlrtDynamicBoundsCheckR2012b(lenn, 1, b_i, &ji_emlrtBCI, sp);
                  }

                  st.site = &q_emlrtRSI;
                  b_st.site = &iq_emlrtRSI;
                  sparse_parenAssign2D(&b_st, NodeReg, numA, numA,
                                       RegionOnElement->data[(int32_T)
                                       intermat2->data[elemB + intermat2->size[0]
                                       * numPBC] - 1]);
                  if (((int32_T)node < 1) || ((int32_T)node >
                       ElementsOnNodeNum->size[0])) {
                    emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
                      ElementsOnNodeNum->size[0], &ii_emlrtBCI, sp);
                  }

                  st.site = &p_emlrtRSI;
                  b_st.site = &p_emlrtRSI;
                  f_sparse_parenReference(&b_st, ElementsOnNode->d,
                    ElementsOnNode->colidx, ElementsOnNode->rowidx,
                    ElementsOnNode->m, ElementsOnNode->n, setAll, node,
                    NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                    &NodeRegSum.m);
                  b_st.site = &p_emlrtRSI;
                  c_sparse_eq(&b_st, intermat2->data[elemB + intermat2->size[0] *
                              numPBC], NodeRegSum.d, NodeRegSum.colidx,
                              NodeRegSum.rowidx, NodeRegSum.m, &d_expl_temp);
                  b_i = c_expl_temp.colidx->size[0];
                  c_expl_temp.colidx->size[0] = d_expl_temp.colidx->size[0];
                  emxEnsureCapacity_int32_T(&st, c_expl_temp.colidx, b_i,
                    &tk_emlrtRTEI);
                  b_loop_ub = d_expl_temp.colidx->size[0];
                  for (b_i = 0; b_i < b_loop_ub; b_i++) {
                    c_expl_temp.colidx->data[b_i] = d_expl_temp.colidx->data[b_i];
                  }

                  b_i = c_expl_temp.rowidx->size[0];
                  c_expl_temp.rowidx->size[0] = d_expl_temp.rowidx->size[0];
                  emxEnsureCapacity_int32_T(&st, c_expl_temp.rowidx, b_i,
                    &tk_emlrtRTEI);
                  b_loop_ub = d_expl_temp.rowidx->size[0];
                  for (b_i = 0; b_i < b_loop_ub; b_i++) {
                    c_expl_temp.rowidx->data[b_i] = d_expl_temp.rowidx->data[b_i];
                  }

                  b_st.site = &uh_emlrtRSI;
                  g_eml_find(&b_st, c_expl_temp.colidx, c_expl_temp.rowidx,
                             d_expl_temp.m, ii);
                  b_i = ElementsOnNodePBCNum->size[0];
                  ElementsOnNodePBCNum->size[0] = ii->size[0];
                  emxEnsureCapacity_real_T(sp, ElementsOnNodePBCNum, b_i,
                    &kj_emlrtRTEI);
                  b_loop_ub = ii->size[0];
                  for (b_i = 0; b_i < b_loop_ub; b_i++) {
                    ElementsOnNodePBCNum->data[b_i] = ii->data[b_i];
                  }

                  st.site = &d_emlrtRSI;
                  c_sparse_parenAssign(&st, ElementsOnNodeDup, numA,
                                       ElementsOnNodePBCNum, node);

                  /*  Reset nodal ID for element in higher material ID */
                  st.site = &o_emlrtRSI;
                  d = intermat2->data[elemB + intermat2->size[0] * numPBC];
                  if (((int32_T)d < 1) || ((int32_T)d > NodesOnElementCG->size[0]))
                  {
                    emlrtDynamicBoundsCheckR2012b((int32_T)d, 1,
                      NodesOnElementCG->size[0], &hi_emlrtBCI, &st);
                  }

                  for (b_i = 0; b_i < loop_ub; b_i++) {
                    b_nodeE2_data[b_i] = (NodesOnElementCG->data[((int32_T)d +
                      NodesOnElementCG->size[0] * b_i) - 1] == node);
                  }

                  b_st.site = &uh_emlrtRSI;
                  f_eml_find(&b_st, b_nodeE2_data, nodeE2_size, ii_data, ii_size);
                  b_loop_ub = ii_size[1];
                  for (b_i = 0; b_i < b_loop_ub; b_i++) {
                    trueCount = ii_data[b_i];
                    if (((int8_T)trueCount < 1) || ((int8_T)trueCount >
                         NodesOnElementDG->size[1])) {
                      emlrtDynamicBoundsCheckR2012b((int8_T)trueCount, 1,
                        NodesOnElementDG->size[1], &g_emlrtBCI, sp);
                    }

                    g_tmp_data[b_i] = (int8_T)trueCount;
                  }

                  if (lenn > NodesOnElementDG->size[0]) {
                    emlrtDynamicBoundsCheckR2012b(lenn, 1,
                      NodesOnElementDG->size[0], &qh_emlrtBCI, sp);
                  }

                  for (b_i = 0; b_i < b_loop_ub; b_i++) {
                    NodesOnElementDG->data[(lenn + NodesOnElementDG->size[0] *
                      (g_tmp_data[b_i] - 1)) - 1] = numA;
                  }

                  if (*emlrtBreakCheckR2012bFlagVar != 0) {
                    emlrtBreakCheckR2012b(sp);
                  }
                }
              }
            }

            if (*emlrtBreakCheckR2012bFlagVar != 0) {
              emlrtBreakCheckR2012b(sp);
            }
          }
        } else {
          b_guard1 = true;
        }
      } else {
        b_guard1 = true;
      }

      if (b_guard1) {
        /*  regular node */
        notdone = 0;
        i1 = (int32_T)ElementsOnNodeNum->data[i1 - 1];
        emlrtForLoopVectorCheckR2012b(1.0, 1.0, ElementsOnNodeNum->data[(int32_T)
          NodesOnInterface->data[partialTrueCount] - 1], mxDOUBLE_CLASS,
          (int32_T)ElementsOnNodeNum->data[(int32_T)NodesOnInterface->
          data[partialTrueCount] - 1], &tb_emlrtRTEI, sp);
        for (numPBC = 0; numPBC < i1; numPBC++) {
          if (((int32_T)(numPBC + 1U) < 1) || ((int32_T)(numPBC + 1U) >
               setnPBC->size[0])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)(numPBC + 1U), 1,
              setnPBC->size[0], &bc_emlrtBCI, sp);
          }

          if (setnPBC->data[numPBC] > 0.0) {
            st.site = &n_emlrtRSI;
            if (notdone != 0) {
              if (1 > intermat2->size[0]) {
                emlrtDynamicBoundsCheckR2012b(1, 1, intermat2->size[0],
                  &ei_emlrtBCI, sp);
              }

              i3 = (int32_T)setnPBC->data[numPBC];
              if ((i3 < 1) || (i3 > intermat2->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)setnPBC->data[numPBC], 1,
                  intermat2->size[0], &di_emlrtBCI, sp);
              }

              b_i = numPBC + 1;
              if ((b_i < 1) || (b_i > intermat2->size[1])) {
                emlrtDynamicBoundsCheckR2012b(b_i, 1, intermat2->size[1],
                  &ci_emlrtBCI, sp);
              }

              b_i2++;
              b_i = Coordinates->size[0];
              if (((int32_T)node < 1) || ((int32_T)node > b_i)) {
                emlrtDynamicBoundsCheckR2012b((int32_T)node, 1, b_i,
                  &bi_emlrtBCI, sp);
              }

              if (((int32_T)b_i2 < 1) || ((int32_T)b_i2 > Coordinates3->size[0]))
              {
                emlrtDynamicBoundsCheckR2012b((int32_T)b_i2, 1,
                  Coordinates3->size[0], &hh_emlrtBCI, sp);
              }

              Coordinates3->data[(int32_T)b_i2 - 1] = Coordinates->data[(int32_T)
                node - 1];
              Coordinates3->data[((int32_T)b_i2 + Coordinates3->size[0]) - 1] =
                Coordinates->data[((int32_T)node + Coordinates->size[0]) - 1];
              Coordinates3->data[((int32_T)b_i2 + Coordinates3->size[0] * 2) - 1]
                = Coordinates->data[((int32_T)node + Coordinates->size[0] * 2) -
                1];

              /*  Loop over all elements attached to node */
              emlrtForLoopVectorCheckR2012b(1.0, 1.0, setnPBC->data[numPBC],
                mxDOUBLE_CLASS, (int32_T)setnPBC->data[numPBC], &sb_emlrtRTEI,
                sp);
              d = ElementsOnNodeNum->data[(int32_T)node - 1];
              if (d < 1.0) {
                setAll->size[0] = 1;
                setAll->size[1] = 0;
              } else if (muDoubleScalarIsInf(d) && (1.0 == d)) {
                b_i = setAll->size[0] * setAll->size[1];
                setAll->size[0] = 1;
                setAll->size[1] = 1;
                emxEnsureCapacity_real_T(sp, setAll, b_i, &qi_emlrtRTEI);
                setAll->data[0] = rtNaN;
              } else {
                b_i = setAll->size[0] * setAll->size[1];
                setAll->size[0] = 1;
                loop_ub = (int32_T)muDoubleScalarFloor(d - 1.0);
                setAll->size[1] = loop_ub + 1;
                emxEnsureCapacity_real_T(sp, setAll, b_i, &qi_emlrtRTEI);
                for (b_i = 0; b_i <= loop_ub; b_i++) {
                  setAll->data[b_i] = (real_T)b_i + 1.0;
                }
              }

              if (1.0 > nen) {
                loop_ub = 0;
              } else {
                if (1 > NodesOnElementCG->size[1]) {
                  emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementCG->size[1],
                    &vh_emlrtBCI, sp);
                }

                if ((int32_T)nen > NodesOnElementCG->size[1]) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1,
                    NodesOnElementCG->size[1], &uh_emlrtBCI, sp);
                }

                loop_ub = (int32_T)nen;
              }

              if (0 <= i3 - 1) {
                nodeE2_size[0] = 1;
                nodeE2_size[1] = loop_ub;
              }

              for (j = 0; j < i3; j++) {
                b_i = j + 1;
                if (b_i > i3) {
                  emlrtDynamicBoundsCheckR2012b(b_i, 1, i3, &ai_emlrtBCI, sp);
                }

                b_i = RegionOnElement->size[0];
                if (intermat2->data[j + intermat2->size[0] * numPBC] != (int32_T)
                    muDoubleScalarFloor(intermat2->data[j + intermat2->size[0] *
                     numPBC])) {
                  emlrtIntegerCheckR2012b(intermat2->data[j + intermat2->size[0]
                    * numPBC], &sd_emlrtDCI, sp);
                }

                lenn = (int32_T)intermat2->data[j + intermat2->size[0] * numPBC];
                if ((lenn < 1) || (lenn > b_i)) {
                  emlrtDynamicBoundsCheckR2012b(lenn, 1, b_i, &yh_emlrtBCI, sp);
                }

                st.site = &m_emlrtRSI;
                b_st.site = &iq_emlrtRSI;
                sparse_parenAssign2D(&b_st, NodeReg, b_i2, node,
                                     RegionOnElement->data[(int32_T)
                                     intermat2->data[j + intermat2->size[0] *
                                     numPBC] - 1]);
                if (((int32_T)node < 1) || ((int32_T)node >
                     ElementsOnNodeNum->size[0])) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
                    ElementsOnNodeNum->size[0], &xh_emlrtBCI, sp);
                }

                st.site = &l_emlrtRSI;
                b_st.site = &l_emlrtRSI;
                f_sparse_parenReference(&b_st, ElementsOnNode->d,
                  ElementsOnNode->colidx, ElementsOnNode->rowidx,
                  ElementsOnNode->m, ElementsOnNode->n, setAll, node,
                  NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  &NodeRegSum.m);
                b_st.site = &l_emlrtRSI;
                c_sparse_eq(&b_st, intermat2->data[j + intermat2->size[0] *
                            numPBC], NodeRegSum.d, NodeRegSum.colidx,
                            NodeRegSum.rowidx, NodeRegSum.m, &d_expl_temp);
                b_i = c_expl_temp.colidx->size[0];
                c_expl_temp.colidx->size[0] = d_expl_temp.colidx->size[0];
                emxEnsureCapacity_int32_T(&st, c_expl_temp.colidx, b_i,
                  &qk_emlrtRTEI);
                b_loop_ub = d_expl_temp.colidx->size[0];
                for (b_i = 0; b_i < b_loop_ub; b_i++) {
                  c_expl_temp.colidx->data[b_i] = d_expl_temp.colidx->data[b_i];
                }

                b_i = c_expl_temp.rowidx->size[0];
                c_expl_temp.rowidx->size[0] = d_expl_temp.rowidx->size[0];
                emxEnsureCapacity_int32_T(&st, c_expl_temp.rowidx, b_i,
                  &qk_emlrtRTEI);
                b_loop_ub = d_expl_temp.rowidx->size[0];
                for (b_i = 0; b_i < b_loop_ub; b_i++) {
                  c_expl_temp.rowidx->data[b_i] = d_expl_temp.rowidx->data[b_i];
                }

                b_st.site = &uh_emlrtRSI;
                g_eml_find(&b_st, c_expl_temp.colidx, c_expl_temp.rowidx,
                           d_expl_temp.m, ii);
                b_i = ElementsOnNodePBCNum->size[0];
                ElementsOnNodePBCNum->size[0] = ii->size[0];
                emxEnsureCapacity_real_T(sp, ElementsOnNodePBCNum, b_i,
                  &kj_emlrtRTEI);
                b_loop_ub = ii->size[0];
                for (b_i = 0; b_i < b_loop_ub; b_i++) {
                  ElementsOnNodePBCNum->data[b_i] = ii->data[b_i];
                }

                st.site = &c_emlrtRSI;
                c_sparse_parenAssign(&st, ElementsOnNodeDup, b_i2,
                                     ElementsOnNodePBCNum, node);

                /*  Reset nodal ID for element in higher material ID */
                st.site = &k_emlrtRSI;
                d = intermat2->data[j + intermat2->size[0] * numPBC];
                if (((int32_T)d < 1) || ((int32_T)d > NodesOnElementCG->size[0]))
                {
                  emlrtDynamicBoundsCheckR2012b((int32_T)d, 1,
                    NodesOnElementCG->size[0], &wh_emlrtBCI, &st);
                }

                for (b_i = 0; b_i < loop_ub; b_i++) {
                  b_nodeE2_data[b_i] = (NodesOnElementCG->data[((int32_T)d +
                    NodesOnElementCG->size[0] * b_i) - 1] == node);
                }

                b_st.site = &uh_emlrtRSI;
                f_eml_find(&b_st, b_nodeE2_data, nodeE2_size, ii_data, ii_size);
                b_loop_ub = ii_size[1];
                for (b_i = 0; b_i < b_loop_ub; b_i++) {
                  trueCount = ii_data[b_i];
                  if (((int8_T)trueCount < 1) || ((int8_T)trueCount >
                       NodesOnElementDG->size[1])) {
                    emlrtDynamicBoundsCheckR2012b((int8_T)trueCount, 1,
                      NodesOnElementDG->size[1], &k_emlrtBCI, sp);
                  }

                  g_tmp_data[b_i] = (int8_T)trueCount;
                }

                if (lenn > NodesOnElementDG->size[0]) {
                  emlrtDynamicBoundsCheckR2012b(lenn, 1, NodesOnElementDG->size
                    [0], &ph_emlrtBCI, sp);
                }

                for (b_i = 0; b_i < b_loop_ub; b_i++) {
                  NodesOnElementDG->data[(lenn + NodesOnElementDG->size[0] *
                    (g_tmp_data[b_i] - 1)) - 1] = b_i2;
                }

                if (*emlrtBreakCheckR2012bFlagVar != 0) {
                  emlrtBreakCheckR2012b(sp);
                }
              }
            } else {
              notdone = 1;
            }
          }

          if (*emlrtBreakCheckR2012bFlagVar != 0) {
            emlrtBreakCheckR2012b(sp);
          }
        }
      }
    }

    /* if facnum */
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  emxFreeStruct_sparse2(&h_expl_temp);
  emxFreeStruct_sparse2(&g_expl_temp);
  emxFreeStruct_sparse2(&f_expl_temp);
  emxFree_real_T(&t5_d);
  emxFree_int32_T(&idx);
  emxFree_int32_T(&jj);
  emxFree_real_T(&b_NodesOnInterface);
  emxFree_real_T(&intermat2);
  emxFree_real_T(&setnPBC);
  emxFree_real_T(&setPBC);

  /*  Form interfaces between all edges on interiors of specified material regions */
  st.site = &j_emlrtRSI;
  diag(&st, InterTypes, MorePBC);
  if (c_norm(MorePBC) > 0.0) {
    i = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
    NodesOnElementCG->size[0] = NodesOnElementDG->size[0];
    NodesOnElementCG->size[1] = NodesOnElementDG->size[1];
    emxEnsureCapacity_real_T(sp, NodesOnElementCG, i, &qj_emlrtRTEI);
    loop_ub = NodesOnElementDG->size[0] * NodesOnElementDG->size[1];
    for (i = 0; i < loop_ub; i++) {
      NodesOnElementCG->data[i] = NodesOnElementDG->data[i];
    }

    /*  Update connectivities to reflect presence of interfaces */
    i = (int32_T)muDoubleScalarFloor(b_i2);
    if (b_i2 != i) {
      emlrtIntegerCheckR2012b(b_i2, &p_emlrtDCI, sp);
    }

    i1 = ElementsOnNodeNum2->size[0];
    loop_ub = (int32_T)b_i2;
    ElementsOnNodeNum2->size[0] = loop_ub;
    emxEnsureCapacity_real_T(sp, ElementsOnNodeNum2, i1, &tj_emlrtRTEI);
    if (loop_ub != i) {
      emlrtIntegerCheckR2012b(b_i2, &p_emlrtDCI, sp);
    }

    for (i1 = 0; i1 < loop_ub; i1++) {
      ElementsOnNodeNum2->data[i1] = 0.0;
    }

    i1 = NodeCGDG->size[0] * NodeCGDG->size[1];
    NodeCGDG->size[0] = 100;
    emxEnsureCapacity_real_T(sp, NodeCGDG, i1, &vj_emlrtRTEI);
    if (loop_ub != i) {
      emlrtIntegerCheckR2012b(b_i2, &tc_emlrtDCI, sp);
    }

    i1 = NodeCGDG->size[0] * NodeCGDG->size[1];
    NodeCGDG->size[1] = loop_ub;
    emxEnsureCapacity_real_T(sp, NodeCGDG, i1, &vj_emlrtRTEI);
    if (loop_ub != i) {
      emlrtIntegerCheckR2012b(b_i2, &n_emlrtDCI, sp);
    }

    b_loop_ub = 100 * (int32_T)b_i2;
    for (i = 0; i < b_loop_ub; i++) {
      NodeCGDG->data[i] = 0.0;
    }

    /*  Mapping of DG nodal IDs to CG nodal IDs */
    if (1.0 > b_i2) {
      loop_ub = 0;
      setAll->size[0] = 1;
      setAll->size[1] = 0;
    } else {
      i = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      setAll->size[1] = loop_ub;
      emxEnsureCapacity_real_T(sp, setAll, i, &ck_emlrtRTEI);
      b_loop_ub = (int32_T)b_i2 - 1;
      for (i = 0; i <= b_loop_ub; i++) {
        setAll->data[i] = (real_T)i + 1.0;
      }
    }

    input_sizes[0] = 1;
    input_sizes[1] = loop_ub;
    emlrtSubAssignSizeCheckR2012b(&input_sizes[0], 2, &setAll->size[0], 2,
      &c_emlrtECI, sp);
    loop_ub = setAll->size[1];
    for (i = 0; i < loop_ub; i++) {
      NodeCGDG->data[100 * i] = setAll->data[i];
    }

    emlrtForLoopVectorCheckR2012b(1.0, 1.0, nummat, mxDOUBLE_CLASS, (int32_T)
      nummat, &rb_emlrtRTEI, sp);
    for (end = 0; end < i2; end++) {
      /*  ID for material pair (row=mat2, col=mat1) */
      if ((int32_T)(end + 1U) > InterTypes->size[1]) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(end + 1U), 1, InterTypes->size[1],
          &mc_emlrtBCI, sp);
      }

      if ((int32_T)(end + 1U) > InterTypes->size[0]) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(end + 1U), 1, InterTypes->size[0],
          &mc_emlrtBCI, sp);
      }

      if (InterTypes->data[end + InterTypes->size[0] * end] > 0.0) {
        /*  Find elements belonging to material mat2 */
        st.site = &i_emlrtRSI;
        i = b_NodesOnPBCnum->size[0];
        b_NodesOnPBCnum->size[0] = RegionOnElement->size[0];
        emxEnsureCapacity_boolean_T(&st, b_NodesOnPBCnum, i, &ik_emlrtRTEI);
        loop_ub = RegionOnElement->size[0];
        for (i = 0; i < loop_ub; i++) {
          b_NodesOnPBCnum->data[i] = (RegionOnElement->data[i] == (real_T)end +
            1.0);
        }

        b_st.site = &uh_emlrtRSI;
        eml_find(&b_st, b_NodesOnPBCnum, ii);
        i = MorePBC->size[0];
        MorePBC->size[0] = ii->size[0];
        emxEnsureCapacity_real_T(&st, MorePBC, i, &kk_emlrtRTEI);
        loop_ub = ii->size[0];
        for (i = 0; i < loop_ub; i++) {
          MorePBC->data[i] = ii->data[i];
        }

        /*  Loop over elements in material mat2, explode all the elements */
        i = MorePBC->size[0];
        if (0 <= i - 1) {
          b_nodeE2_size[0] = 1;
        }

        for (b_i = 0; b_i < i; b_i++) {
          if ((b_i + 1 < 1) || (b_i + 1 > MorePBC->size[0])) {
            emlrtDynamicBoundsCheckR2012b(b_i + 1, 1, MorePBC->size[0],
              &hc_emlrtBCI, sp);
          }

          b_i1 = MorePBC->data[b_i];
          if (1.0 > nen) {
            loop_ub = 0;
          } else {
            if (1 > NodesOnElementDG->size[1]) {
              emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementDG->size[1],
                &nh_emlrtBCI, sp);
            }

            if ((int32_T)nen > NodesOnElementDG->size[1]) {
              emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1,
                NodesOnElementDG->size[1], &mh_emlrtBCI, sp);
            }

            loop_ub = (int32_T)nen;
          }

          i1 = (int32_T)MorePBC->data[b_i];
          if ((i1 < 1) || (i1 > NodesOnElementDG->size[0])) {
            emlrtDynamicBoundsCheckR2012b(i1, 1, NodesOnElementDG->size[0],
              &oh_emlrtBCI, sp);
          }

          b_nodeE2_size[1] = loop_ub;
          for (i3 = 0; i3 < loop_ub; i3++) {
            nodeE2_data[i3] = NodesOnElementDG->data[(i1 +
              NodesOnElementDG->size[0] * i3) - 1];
          }

          i1 = intnnz(nodeE2_data, b_nodeE2_size);
          for (partialTrueCount = 0; partialTrueCount < i1; partialTrueCount++)
          {
            /*  Loop over local Nodes */
            if (((int32_T)(partialTrueCount + 1U) < 1) || ((int32_T)
                 (partialTrueCount + 1U) > NodesOnElementCG->size[1])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)(partialTrueCount + 1U), 1,
                NodesOnElementCG->size[1], &vb_emlrtBCI, sp);
            }

            if (((int32_T)b_i1 < 1) || ((int32_T)b_i1 > NodesOnElementCG->size[0]))
            {
              emlrtDynamicBoundsCheckR2012b((int32_T)b_i1, 1,
                NodesOnElementCG->size[0], &vb_emlrtBCI, sp);
            }

            node = NodesOnElementCG->data[((int32_T)b_i1 +
              NodesOnElementCG->size[0] * partialTrueCount) - 1];
            if (node != (int32_T)muDoubleScalarFloor(node)) {
              emlrtIntegerCheckR2012b(node, &h_emlrtDCI, sp);
            }

            if (((int32_T)node < 1) || ((int32_T)node > ElementsOnNodeNum2->
                 size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
                ElementsOnNodeNum2->size[0], &tb_emlrtBCI, sp);
            }

            d = ElementsOnNodeNum2->data[(int32_T)node - 1];
            if (d == 0.0) {
              /*  only duplicate nodes after the first time encountered */
              if (((int32_T)node < 1) || ((int32_T)node >
                   ElementsOnNodeNum2->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
                  ElementsOnNodeNum2->size[0], &pb_emlrtBCI, sp);
              }

              ElementsOnNodeNum2->data[(int32_T)node - 1] = 1.0;
              if (((int32_T)node < 1) || ((int32_T)node > NodeCGDG->size[1])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)node, 1, NodeCGDG->size[1],
                  &ob_emlrtBCI, sp);
              }

              NodeCGDG->data[100 * ((int32_T)node - 1)] = node;

              /* node in DG mesh with same coordinates */
            } else {
              b_i2++;
              if (((int32_T)(partialTrueCount + 1U) < 1) || ((int32_T)
                   (partialTrueCount + 1U) > NodesOnElementDG->size[1])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)(partialTrueCount + 1U),
                  1, NodesOnElementDG->size[1], &qb_emlrtBCI, sp);
              }

              if (((int32_T)b_i1 < 1) || ((int32_T)b_i1 > NodesOnElementDG->
                   size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)b_i1, 1,
                  NodesOnElementDG->size[0], &qb_emlrtBCI, sp);
              }

              NodesOnElementDG->data[((int32_T)b_i1 + NodesOnElementDG->size[0] *
                partialTrueCount) - 1] = b_i2;
              if (((int32_T)node < 1) || ((int32_T)node > Coordinates3->size[0]))
              {
                emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
                  Coordinates3->size[0], &gh_emlrtBCI, sp);
              }

              if (((int32_T)b_i2 < 1) || ((int32_T)b_i2 > Coordinates3->size[0]))
              {
                emlrtDynamicBoundsCheckR2012b((int32_T)b_i2, 1,
                  Coordinates3->size[0], &fh_emlrtBCI, sp);
              }

              Coordinates3->data[(int32_T)b_i2 - 1] = Coordinates3->data
                [(int32_T)node - 1];
              Coordinates3->data[((int32_T)b_i2 + Coordinates3->size[0]) - 1] =
                Coordinates3->data[((int32_T)node + Coordinates3->size[0]) - 1];
              Coordinates3->data[((int32_T)b_i2 + Coordinates3->size[0] * 2) - 1]
                = Coordinates3->data[((int32_T)node + Coordinates3->size[0] * 2)
                - 1];
              i3 = NodesOnElement->size[1];
              if (((int32_T)(partialTrueCount + 1U) < 1) || ((int32_T)
                   (partialTrueCount + 1U) > i3)) {
                emlrtDynamicBoundsCheckR2012b((int32_T)(partialTrueCount + 1U),
                  1, i3, &ib_emlrtBCI, sp);
              }

              i3 = NodesOnElement->size[0];
              if (((int32_T)b_i1 < 1) || ((int32_T)b_i1 > i3)) {
                emlrtDynamicBoundsCheckR2012b((int32_T)b_i1, 1, i3, &ib_emlrtBCI,
                  sp);
              }

              numA = NodesOnElement->data[((int32_T)b_i1 + NodesOnElement->size
                [0] * partialTrueCount) - 1];
              if (numA != (int32_T)muDoubleScalarFloor(numA)) {
                emlrtIntegerCheckR2012b(numA, &rd_emlrtDCI, sp);
              }

              if (((int32_T)numA < 1) || ((int32_T)numA >
                   ElementsOnNodeNum->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1,
                  ElementsOnNodeNum->size[0], &eh_emlrtBCI, sp);
              }

              d1 = ElementsOnNodeNum->data[(int32_T)numA - 1];
              if (d1 < 1.0) {
                setAll->size[0] = 1;
                setAll->size[1] = 0;
              } else if (muDoubleScalarIsInf(d1) && (1.0 == d1)) {
                i3 = setAll->size[0] * setAll->size[1];
                setAll->size[0] = 1;
                setAll->size[1] = 1;
                emxEnsureCapacity_real_T(sp, setAll, i3, &qi_emlrtRTEI);
                setAll->data[0] = rtNaN;
              } else {
                i3 = setAll->size[0] * setAll->size[1];
                setAll->size[0] = 1;
                loop_ub = (int32_T)muDoubleScalarFloor(d1 - 1.0);
                setAll->size[1] = loop_ub + 1;
                emxEnsureCapacity_real_T(sp, setAll, i3, &qi_emlrtRTEI);
                for (i3 = 0; i3 <= loop_ub; i3++) {
                  setAll->data[i3] = (real_T)i3 + 1.0;
                }
              }

              st.site = &h_emlrtRSI;
              b_st.site = &h_emlrtRSI;
              f_sparse_parenReference(&b_st, ElementsOnNode->d,
                ElementsOnNode->colidx, ElementsOnNode->rowidx,
                ElementsOnNode->m, ElementsOnNode->n, setAll, numA, NodeRegSum.d,
                NodeRegSum.colidx, NodeRegSum.rowidx, &NodeRegSum.m);
              b_st.site = &h_emlrtRSI;
              c_sparse_eq(&b_st, b_i1, NodeRegSum.d, NodeRegSum.colidx,
                          NodeRegSum.rowidx, NodeRegSum.m, &d_expl_temp);
              i3 = c_expl_temp.colidx->size[0];
              c_expl_temp.colidx->size[0] = d_expl_temp.colidx->size[0];
              emxEnsureCapacity_int32_T(&st, c_expl_temp.colidx, i3,
                &sk_emlrtRTEI);
              loop_ub = d_expl_temp.colidx->size[0];
              for (i3 = 0; i3 < loop_ub; i3++) {
                c_expl_temp.colidx->data[i3] = d_expl_temp.colidx->data[i3];
              }

              i3 = c_expl_temp.rowidx->size[0];
              c_expl_temp.rowidx->size[0] = d_expl_temp.rowidx->size[0];
              emxEnsureCapacity_int32_T(&st, c_expl_temp.rowidx, i3,
                &sk_emlrtRTEI);
              loop_ub = d_expl_temp.rowidx->size[0];
              for (i3 = 0; i3 < loop_ub; i3++) {
                c_expl_temp.rowidx->data[i3] = d_expl_temp.rowidx->data[i3];
              }

              b_st.site = &uh_emlrtRSI;
              g_eml_find(&b_st, c_expl_temp.colidx, c_expl_temp.rowidx,
                         d_expl_temp.m, ii);
              i3 = ElementsOnNodePBCNum->size[0];
              ElementsOnNodePBCNum->size[0] = ii->size[0];
              emxEnsureCapacity_real_T(sp, ElementsOnNodePBCNum, i3,
                &kj_emlrtRTEI);
              loop_ub = ii->size[0];
              for (i3 = 0; i3 < loop_ub; i3++) {
                ElementsOnNodePBCNum->data[i3] = ii->data[i3];
              }

              st.site = &b_emlrtRSI;
              c_sparse_parenAssign(&st, ElementsOnNodeDup, b_i2,
                                   ElementsOnNodePBCNum, numA);

              /*    Add element to star list, increment number of elem in star */
              if (((int32_T)node < 1) || ((int32_T)node >
                   ElementsOnNodeNum2->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
                  ElementsOnNodeNum2->size[0], &s_emlrtBCI, sp);
              }

              ElementsOnNodeNum2->data[(int32_T)node - 1]++;
              if (((int32_T)node < 1) || ((int32_T)node > NodeCGDG->size[1])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)node, 1, NodeCGDG->size[1],
                  &q_emlrtBCI, sp);
              }

              if (((int32_T)(d + 1.0) < 1) || ((int32_T)(d + 1.0) > 100)) {
                emlrtDynamicBoundsCheckR2012b((int32_T)(d + 1.0), 1, 100,
                  &r_emlrtBCI, sp);
              }

              NodeCGDG->data[((int32_T)(d + 1.0) + 100 * ((int32_T)node - 1)) -
                1] = b_i2;

              /* node in DG mesh with same coordinates */
            }

            if (*emlrtBreakCheckR2012bFlagVar != 0) {
              emlrtBreakCheckR2012b(sp);
            }
          }

          if (*emlrtBreakCheckR2012bFlagVar != 0) {
            emlrtBreakCheckR2012b(sp);
          }
        }
      }

      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    st.site = &g_emlrtRSI;
    i = b_NodesOnPBCnum->size[0];
    b_NodesOnPBCnum->size[0] = ElementsOnNodeNum2->size[0];
    emxEnsureCapacity_boolean_T(&st, b_NodesOnPBCnum, i, &gk_emlrtRTEI);
    loop_ub = ElementsOnNodeNum2->size[0];
    for (i = 0; i < loop_ub; i++) {
      b_NodesOnPBCnum->data[i] = (ElementsOnNodeNum2->data[i] == 0.0);
    }

    b_st.site = &uh_emlrtRSI;
    eml_find(&b_st, b_NodesOnPBCnum, ii);
    loop_ub = ii->size[0];
    for (i = 0; i < loop_ub; i++) {
      if ((ii->data[i] < 1) || (ii->data[i] > ElementsOnNodeNum2->size[0])) {
        emlrtDynamicBoundsCheckR2012b(ii->data[i], 1, ElementsOnNodeNum2->size[0],
          &jc_emlrtBCI, sp);
      }
    }

    loop_ub = ii->size[0];
    for (i = 0; i < loop_ub; i++) {
      ElementsOnNodeNum2->data[ii->data[i] - 1] = 1.0;
    }
  } else {
    /*  no DG in interior of any materials */
    i = (int32_T)muDoubleScalarFloor(b_i2);
    if (b_i2 != i) {
      emlrtIntegerCheckR2012b(b_i2, &r_emlrtDCI, sp);
    }

    i1 = ElementsOnNodeNum2->size[0];
    loop_ub = (int32_T)b_i2;
    ElementsOnNodeNum2->size[0] = loop_ub;
    emxEnsureCapacity_real_T(sp, ElementsOnNodeNum2, i1, &rj_emlrtRTEI);
    if (loop_ub != i) {
      emlrtIntegerCheckR2012b(b_i2, &r_emlrtDCI, sp);
    }

    for (i1 = 0; i1 < loop_ub; i1++) {
      ElementsOnNodeNum2->data[i1] = 1.0;
    }

    /*  set one copy of duplicated nodes per material ID so that BC expanding subroutine works properly. */
    i1 = NodeCGDG->size[0] * NodeCGDG->size[1];
    NodeCGDG->size[0] = 100;
    emxEnsureCapacity_real_T(sp, NodeCGDG, i1, &uj_emlrtRTEI);
    if (loop_ub != i) {
      emlrtIntegerCheckR2012b(b_i2, &uc_emlrtDCI, sp);
    }

    i1 = NodeCGDG->size[0] * NodeCGDG->size[1];
    NodeCGDG->size[1] = loop_ub;
    emxEnsureCapacity_real_T(sp, NodeCGDG, i1, &uj_emlrtRTEI);
    if (loop_ub != i) {
      emlrtIntegerCheckR2012b(b_i2, &o_emlrtDCI, sp);
    }

    b_loop_ub = 100 * loop_ub;
    for (i = 0; i < b_loop_ub; i++) {
      NodeCGDG->data[i] = 0.0;
    }

    /*  Mapping of DG nodal IDs to CG nodal IDs */
    if (1.0 > b_i2) {
      loop_ub = 0;
      setAll->size[0] = 1;
      setAll->size[1] = 0;
    } else {
      i = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      b_loop_ub = loop_ub - 1;
      setAll->size[1] = loop_ub;
      emxEnsureCapacity_real_T(sp, setAll, i, &yj_emlrtRTEI);
      for (i = 0; i <= b_loop_ub; i++) {
        setAll->data[i] = (real_T)i + 1.0;
      }
    }

    input_sizes[0] = 1;
    input_sizes[1] = loop_ub;
    emlrtSubAssignSizeCheckR2012b(&input_sizes[0], 2, &setAll->size[0], 2,
      &b_emlrtECI, sp);
    loop_ub = setAll->size[1];
    for (i = 0; i < loop_ub; i++) {
      NodeCGDG->data[100 * i] = setAll->data[i];
    }
  }

  emxFree_boolean_T(&b_NodesOnPBCnum);
  emxFreeStruct_sparse3(&d_expl_temp);
  emxFreeStruct_sparse3(&c_expl_temp);
  emxFree_int32_T(&ii);
  emxFreeStruct_sparse(&NodeRegSum);
  emxFree_real_T(&setAll);
  emxFree_real_T(&MorePBC);
  emxFree_real_T(&ElementsOnNodePBCNum);

  /*  Form final lists of DG interfaces */
  *numnp = b_i2;
  if (1.0 > b_i2) {
    loop_ub = 0;
  } else {
    if (1 > Coordinates3->size[0]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, Coordinates3->size[0], &dh_emlrtBCI,
        sp);
    }

    if (((int32_T)b_i2 < 1) || ((int32_T)b_i2 > Coordinates3->size[0])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)b_i2, 1, Coordinates3->size[0],
        &ch_emlrtBCI, sp);
    }

    loop_ub = (int32_T)b_i2;
  }

  i = Coordinates->size[0] * Coordinates->size[1];
  Coordinates->size[0] = loop_ub;
  Coordinates->size[1] = 3;
  emxEnsureCapacity_real_T(sp, Coordinates, i, &ek_emlrtRTEI);
  for (i = 0; i < 3; i++) {
    for (i1 = 0; i1 < loop_ub; i1++) {
      Coordinates->data[i1 + Coordinates->size[0] * i] = Coordinates3->data[i1 +
        Coordinates3->size[0] * i];
    }
  }

  emxFree_real_T(&Coordinates3);
  if (27 < NodesOnElementDG->size[1]) {
    emlrtDimSizeGeqCheckR2012b(27, NodesOnElementDG->size[1], &d_emlrtECI, sp);
  }

  i = NodesOnElement->size[0] * NodesOnElement->size[1];
  NodesOnElement->size[0] = NodesOnElementDG->size[0];
  NodesOnElement->size[1] = NodesOnElementDG->size[1];
  emxEnsureCapacity_real_T(sp, NodesOnElement, i, &hk_emlrtRTEI);
  loop_ub = NodesOnElementDG->size[1];
  for (i = 0; i < loop_ub; i++) {
    b_loop_ub = NodesOnElementDG->size[0];
    for (i1 = 0; i1 < b_loop_ub; i1++) {
      NodesOnElement->data[i1 + NodesOnElement->size[0] * i] =
        NodesOnElementDG->data[i1 + NodesOnElementDG->size[0] * i];
    }
  }

  st.site = &f_emlrtRSI;
  if (muDoubleScalarIsNaN(usePBC)) {
    emlrtErrorWithMessageIdR2018a(&st, &mc_emlrtRTEI, "MATLAB:nologicalnan",
      "MATLAB:nologicalnan", 0);
  }

  if (usePBC != 0.0) {
    i = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
    NodesOnElementCG->size[0] = NodesOnElementPBC->size[0];
    NodesOnElementCG->size[1] = NodesOnElementPBC->size[1];
    emxEnsureCapacity_real_T(sp, NodesOnElementCG, i, &lk_emlrtRTEI);
    loop_ub = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1];
    for (i = 0; i < loop_ub; i++) {
      NodesOnElementCG->data[i] = NodesOnElementPBC->data[i];
    }
  }

  emxFree_real_T(&NodesOnElementPBC);
  if (usePBC == 2.0) {
    i = MPCList->size[0] * MPCList->size[1];
    MPCList->size[0] = PBCList->size[0];
    MPCList->size[1] = 5;
    emxEnsureCapacity_real_T(sp, MPCList, i, &mk_emlrtRTEI);
    loop_ub = PBCList->size[0];
    for (i = 0; i < 5; i++) {
      for (i1 = 0; i1 < loop_ub; i1++) {
        MPCList->data[i1 + MPCList->size[0] * i] = PBCList->data[i1 +
          PBCList->size[0] * i];
      }
    }
  }

  emxFree_real_T(&PBCList);

  /*  %% Clear out intermediate variables */
  /*  if clearinter */
  /*  KeyList = {'numEonB','numEonF','ElementsOnBoundary','numSI','ElementsOnFacet',... */
  /*  'ElementsOnNode','ElementsOnNodeA','ElementsOnNodeB','ElementsOnNodeDup',... */
  /*  'ElementsOnNodeNum','numfac','ElementsOnNodeNum2','numinttype','FacetsOnElement',... */
  /*  'FacetsOnElementInt','FacetsOnInterface','FacetsOnInterfaceNum','FacetsOnNode',... */
  /*  'FacetsOnNodeCut','FacetsOnNodeInt','FacetsOnNodeNum','NodeCGDG','NodeReg',... */
  /*  'NodesOnElementCG','NodesOnElementDG','NodesOnInterface','NodesOnInterfaceNum',... */
  /*  'numCL','maxel','numelPBC','RegionOnElementDG','numPBC','TieNodesNew','TieNodes',... */
  /*  'NodesOnPBC','NodesOnPBCnum','NodesOnLink','NodesOnLinknum','numEonPBC',... */
  /*  'FacetsOnPBC','FacetsOnPBCNum','FacetsOnIntMinusPBC','FacetsOnIntMinusPBCNum','MPCList'}; */
  /*  if isOctave */
  /*      wholething = [{'-x'},KeyList,currvariables']; */
  /*      clear(wholething{:}) */
  /*  else */
  /*      wholething = [{'-except'},KeyList,currvariables']; */
  /*      clearvars(wholething{:}) */
  /*  end */
  /*  end */
  *numSI = numSI_tmp;
  *NodesOnInterfaceNum = e_loop_ub;
  *numCL = 0.0;
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (DEIPFunc3.c) */
