/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * not.c
 *
 * Code generation for function 'not'
 *
 */

/* Include files */
#include "not.h"
#include "DEIPFunc2_types.h"
#include "rt_nonfinite.h"
#include "sparse1.h"

/* Function Definitions */
void sparse_not(const emxArray_int32_T *S_colidx, const emxArray_int32_T
                *S_rowidx, emxArray_boolean_T *out_d, emxArray_int32_T
                *out_colidx, emxArray_int32_T *out_rowidx)
{
  int b_i;
  int i;
  int nnzInThisRowOfS;
  int outNNZ;
  int writeEnd;
  int writeRow;
  int writeStart;
  if (2 - S_colidx->data[S_colidx->size[0] - 1] == 0) {
    outNNZ = 0;
  } else {
    outNNZ = 2 - S_colidx->data[S_colidx->size[0] - 1];
  }

  b_sparse_spallocLike(outNNZ, out_d, out_colidx, out_rowidx);
  if (1 > outNNZ) {
    outNNZ = 0;
  }

  for (i = 0; i < outNNZ; i++) {
    out_d->data[i] = true;
  }

  outNNZ = 0;
  out_colidx->data[0] = 1;
  if (S_colidx->data[0] == S_colidx->data[1]) {
    out_rowidx->data[0] = 1;
    outNNZ = 1;
  } else {
    i = S_rowidx->data[S_colidx->data[0] - 1];
    for (writeRow = 0; writeRow <= i - 2; writeRow++) {
      out_rowidx->data[outNNZ] = writeRow + 1;
      outNNZ++;
    }

    nnzInThisRowOfS = (S_colidx->data[1] - S_colidx->data[0]) - 2;
    for (b_i = 0; b_i <= nnzInThisRowOfS; b_i++) {
      writeStart = i + 1;
      writeEnd = S_rowidx->data[S_colidx->data[0]] - 1;
      for (writeRow = writeStart; writeRow <= writeEnd; writeRow++) {
        out_rowidx->data[outNNZ] = writeRow;
        outNNZ++;
      }
    }

    writeStart = S_rowidx->data[S_colidx->data[0] + nnzInThisRowOfS] + 1;
    for (writeRow = writeStart; writeRow < 2; writeRow++) {
      out_rowidx->data[outNNZ] = 1;
      outNNZ++;
    }
  }

  out_colidx->data[1] = outNNZ + 1;
}

/* End of code generation (not.c) */
