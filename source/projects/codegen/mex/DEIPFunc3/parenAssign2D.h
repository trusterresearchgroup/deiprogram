/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * parenAssign2D.h
 *
 * Code generation for function 'parenAssign2D'
 *
 */

#pragma once

/* Include files */
#include "DEIPFunc3_types.h"
#include "rtwtypes.h"
#include "emlrt.h"
#include "mex.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Function Declarations */
void b_realloc(const emlrtStack *sp, e_sparse *this, int32_T numAllocRequested,
               int32_T ub1, int32_T lb2, int32_T ub2);
void decrColIdx(const emlrtStack *sp, e_sparse *this, int32_T col);
void incrColIdx(const emlrtStack *sp, e_sparse *this, int32_T col);
void sparse_parenAssign2D(const emlrtStack *sp, e_sparse *this, real_T rhs,
  real_T r, real_T c);

/* End of code generation (parenAssign2D.h) */
