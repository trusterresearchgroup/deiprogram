% Interactive example that plots solutions from tutorial MPC_2d_CZB to show
% the deformation of an RVE containing cohesive couplers
%
% Moderate example of insertion of couplers using DEIProgram2. Corresponds
% to Figure 5 in "Discontinuous Element Insertion Algorithm" manuscript
% Domain: 4x3 rectangle, combination of T3 and Q4 elements; insertion of CZ
% couplers on region interfaces; demonstrates separation of mesh
% Loading: Displacement of 0.1 on right edge and top edge
%
% Last revision: 12/11/2018 TJT

batchinter = 'batch';
batchname = 'MPC_2d_CZB.m';
fprintf('starting file %s\n',batchname)
FEA_Program

disp(' Plot the shear stress after a 2% strain; press any key to continue')
pause
plotElemCont2(Coordinates+50*Node_U_V,StreListE(3,:,:)',NodesOnElement(1:16,1:5),1,(1:16),[1 0 0],0,[3 4 6 9 0],{'cb'},{'FontSize',20})

disp(' Extract the macroscale strain and stress*volume')
[RVE_E,RVE_F] = GetMacroSS(DispList,ForcList,NodeTypeNum,ndm)
