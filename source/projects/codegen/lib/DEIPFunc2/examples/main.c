/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * main.c
 *
 * Code generation for function 'main'
 *
 */

/*************************************************************************/
/* This automatically generated example C main file shows how to call    */
/* entry-point functions that MATLAB Coder generated. You must customize */
/* this file for your application. Do not modify this file directly.     */
/* Instead, make a copy of this file, modify it, and integrate it into   */
/* your development environment.                                         */
/*                                                                       */
/* This file initializes entry-point function arguments to a default     */
/* size and value before calling the entry-point functions. It does      */
/* not store or use any values returned from the entry-point functions.  */
/* If necessary, it does pre-allocate memory for returned values.        */
/* You can use this file as a starting point for a main function that    */
/* you can deploy in your application.                                   */
/*                                                                       */
/* After you copy the file, and before you deploy it, you must make the  */
/* following changes:                                                    */
/* * For variable-size function arguments, change the example sizes to   */
/* the sizes that your application requires.                             */
/* * Change the example values of function arguments to the values that  */
/* your application requires.                                            */
/* * If the entry-point functions return values, store these values or   */
/* otherwise use them as required by your application.                   */
/*                                                                       */
/*************************************************************************/

/* Include files */
#include "main.h"
#include "DEIPFunc2.h"
#include "DEIPFunc2_emxAPI.h"
#include "DEIPFunc2_terminate.h"
#include "DEIPFunc2_types.h"
#include "rt_nonfinite.h"

/* Function Declarations */
static emxArray_real_T *argInit_Unboundedx1_real_T(void);
static emxArray_real_T *argInit_Unboundedx2_real_T(void);
static emxArray_real_T *argInit_Unboundedx4_real_T(void);
static double argInit_real_T(void);
static emxArray_real_T *c_argInit_UnboundedxUnbounded_r(void);
static void main_DEIPFunc2(void);

/* Function Definitions */
static emxArray_real_T *argInit_Unboundedx1_real_T(void)
{
  static const int iv[1] = { 2 };

  emxArray_real_T *result;
  int idx0;

  /* Set the size of the array.
     Change this size to the value that the application requires. */
  result = emxCreateND_real_T(1, iv);

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < result->size[0U]; idx0++) {
    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result->data[idx0] = argInit_real_T();
  }

  return result;
}

static emxArray_real_T *argInit_Unboundedx2_real_T(void)
{
  emxArray_real_T *result;
  int idx0;
  int idx1;

  /* Set the size of the array.
     Change this size to the value that the application requires. */
  result = emxCreate_real_T(2, 2);

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < result->size[0U]; idx0++) {
    for (idx1 = 0; idx1 < 2; idx1++) {
      /* Set the value of the array element.
         Change this value to the value that the application requires. */
      result->data[idx0 + result->size[0] * idx1] = argInit_real_T();
    }
  }

  return result;
}

static emxArray_real_T *argInit_Unboundedx4_real_T(void)
{
  emxArray_real_T *result;
  int idx0;
  int idx1;

  /* Set the size of the array.
     Change this size to the value that the application requires. */
  result = emxCreate_real_T(2, 4);

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < result->size[0U]; idx0++) {
    for (idx1 = 0; idx1 < 4; idx1++) {
      /* Set the value of the array element.
         Change this value to the value that the application requires. */
      result->data[idx0 + result->size[0] * idx1] = argInit_real_T();
    }
  }

  return result;
}

static double argInit_real_T(void)
{
  return 0.0;
}

static emxArray_real_T *c_argInit_UnboundedxUnbounded_r(void)
{
  emxArray_real_T *result;
  int idx0;
  int idx1;

  /* Set the size of the array.
     Change this size to the value that the application requires. */
  result = emxCreate_real_T(2, 2);

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < result->size[0U]; idx0++) {
    for (idx1 = 0; idx1 < result->size[1U]; idx1++) {
      /* Set the value of the array element.
         Change this value to the value that the application requires. */
      result->data[idx0 + result->size[0] * idx1] = argInit_real_T();
    }
  }

  return result;
}

static void main_DEIPFunc2(void)
{
  d_sparse ElementsOnNode;
  d_sparse ElementsOnNodeDup;
  d_sparse FacetsOnNode;
  d_sparse FacetsOnNodeCut;
  d_sparse FacetsOnNodeInt;
  d_sparse NodeReg;
  emxArray_real_T *Coordinates;
  emxArray_real_T *ElementsOnBoundary;
  emxArray_real_T *ElementsOnFacet;
  emxArray_real_T *ElementsOnNodeNum;
  emxArray_real_T *ElementsOnNodeNum2;
  emxArray_real_T *FacetsOnElement;
  emxArray_real_T *FacetsOnElementInt;
  emxArray_real_T *FacetsOnIntMinusPBC;
  emxArray_real_T *FacetsOnIntMinusPBCNum;
  emxArray_real_T *FacetsOnInterface;
  emxArray_real_T *FacetsOnInterfaceNum;
  emxArray_real_T *FacetsOnNodeNum;
  emxArray_real_T *FacetsOnPBC;
  emxArray_real_T *FacetsOnPBCNum;
  emxArray_real_T *InterTypes;
  emxArray_real_T *MPCList;
  emxArray_real_T *NodeCGDG;
  emxArray_real_T *NodesOnElement;
  emxArray_real_T *NodesOnElementCG;
  emxArray_real_T *NodesOnElementDG;
  emxArray_real_T *NodesOnInterface;
  emxArray_real_T *NodesOnLink;
  emxArray_real_T *NodesOnLinknum;
  emxArray_real_T *NodesOnPBC;
  emxArray_real_T *NodesOnPBCnum;
  emxArray_real_T *RegionOnElement;
  emxArray_real_T *numEonF;
  emxArray_real_T *numEonPBC;
  double NodesOnInterfaceNum;
  double numCL;
  double numEonB;
  double numSI;
  double numfac;
  double numinttype;
  double numnp;
  double numnp_tmp;
  emxInitArray_real_T(&numEonF, 1);
  emxInitArray_real_T(&ElementsOnBoundary, 2);
  emxInitArray_real_T(&ElementsOnFacet, 2);
  emxInit_sparse(&ElementsOnNode);
  emxInit_sparse(&ElementsOnNodeDup);
  emxInitArray_real_T(&ElementsOnNodeNum, 1);
  emxInitArray_real_T(&ElementsOnNodeNum2, 1);
  emxInitArray_real_T(&FacetsOnElement, 2);
  emxInitArray_real_T(&FacetsOnElementInt, 2);
  emxInitArray_real_T(&FacetsOnInterface, 1);
  emxInitArray_real_T(&FacetsOnInterfaceNum, 1);
  emxInit_sparse(&FacetsOnNode);
  emxInit_sparse(&FacetsOnNodeCut);
  emxInit_sparse(&FacetsOnNodeInt);
  emxInitArray_real_T(&FacetsOnNodeNum, 1);
  emxInitArray_real_T(&NodeCGDG, 2);
  emxInit_sparse(&NodeReg);
  emxInitArray_real_T(&NodesOnElementCG, 2);
  emxInitArray_real_T(&NodesOnElementDG, 2);
  emxInitArray_real_T(&NodesOnInterface, 1);
  emxInitArray_real_T(&NodesOnPBC, 2);
  emxInitArray_real_T(&NodesOnPBCnum, 1);
  emxInitArray_real_T(&NodesOnLink, 2);
  emxInitArray_real_T(&NodesOnLinknum, 1);
  emxInitArray_real_T(&numEonPBC, 1);
  emxInitArray_real_T(&FacetsOnPBC, 1);
  emxInitArray_real_T(&FacetsOnPBCNum, 1);
  emxInitArray_real_T(&FacetsOnIntMinusPBC, 1);
  emxInitArray_real_T(&FacetsOnIntMinusPBCNum, 1);

  /* Initialize function 'DEIPFunc2' input arguments. */
  /* Initialize function input argument 'InterTypes'. */
  InterTypes = c_argInit_UnboundedxUnbounded_r();

  /* Initialize function input argument 'NodesOnElement'. */
  NodesOnElement = c_argInit_UnboundedxUnbounded_r();

  /* Initialize function input argument 'RegionOnElement'. */
  RegionOnElement = argInit_Unboundedx1_real_T();

  /* Initialize function input argument 'Coordinates'. */
  Coordinates = argInit_Unboundedx2_real_T();
  numnp_tmp = argInit_real_T();

  /* Initialize function input argument 'MPCList'. */
  MPCList = argInit_Unboundedx4_real_T();

  /* Call the entry-point 'DEIPFunc2'. */
  numnp = numnp_tmp;
  DEIPFunc2(InterTypes, NodesOnElement, RegionOnElement, Coordinates, &numnp,
            numnp_tmp, numnp_tmp, numnp_tmp, numnp_tmp, numnp_tmp, &numnp_tmp,
            MPCList, &numEonB, numEonF, ElementsOnBoundary, &numSI,
            ElementsOnFacet, &ElementsOnNode, &ElementsOnNodeDup,
            ElementsOnNodeNum, &numfac, ElementsOnNodeNum2, &numinttype,
            FacetsOnElement, FacetsOnElementInt, FacetsOnInterface,
            FacetsOnInterfaceNum, &FacetsOnNode, &FacetsOnNodeCut,
            &FacetsOnNodeInt, FacetsOnNodeNum, NodeCGDG, &NodeReg,
            NodesOnElementCG, NodesOnElementDG, NodesOnInterface,
            &NodesOnInterfaceNum, &numCL, NodesOnPBC, NodesOnPBCnum, NodesOnLink,
            NodesOnLinknum, numEonPBC, FacetsOnPBC, FacetsOnPBCNum,
            FacetsOnIntMinusPBC, FacetsOnIntMinusPBCNum);
  emxDestroyArray_real_T(FacetsOnIntMinusPBCNum);
  emxDestroyArray_real_T(FacetsOnIntMinusPBC);
  emxDestroyArray_real_T(FacetsOnPBCNum);
  emxDestroyArray_real_T(FacetsOnPBC);
  emxDestroyArray_real_T(numEonPBC);
  emxDestroyArray_real_T(NodesOnLinknum);
  emxDestroyArray_real_T(NodesOnLink);
  emxDestroyArray_real_T(NodesOnPBCnum);
  emxDestroyArray_real_T(NodesOnPBC);
  emxDestroyArray_real_T(NodesOnInterface);
  emxDestroyArray_real_T(NodesOnElementDG);
  emxDestroyArray_real_T(NodesOnElementCG);
  emxDestroy_sparse(NodeReg);
  emxDestroyArray_real_T(NodeCGDG);
  emxDestroyArray_real_T(FacetsOnNodeNum);
  emxDestroy_sparse(FacetsOnNodeInt);
  emxDestroy_sparse(FacetsOnNodeCut);
  emxDestroy_sparse(FacetsOnNode);
  emxDestroyArray_real_T(FacetsOnInterfaceNum);
  emxDestroyArray_real_T(FacetsOnInterface);
  emxDestroyArray_real_T(FacetsOnElementInt);
  emxDestroyArray_real_T(FacetsOnElement);
  emxDestroyArray_real_T(ElementsOnNodeNum2);
  emxDestroyArray_real_T(ElementsOnNodeNum);
  emxDestroy_sparse(ElementsOnNodeDup);
  emxDestroy_sparse(ElementsOnNode);
  emxDestroyArray_real_T(ElementsOnFacet);
  emxDestroyArray_real_T(ElementsOnBoundary);
  emxDestroyArray_real_T(numEonF);
  emxDestroyArray_real_T(MPCList);
  emxDestroyArray_real_T(Coordinates);
  emxDestroyArray_real_T(RegionOnElement);
  emxDestroyArray_real_T(NodesOnElement);
  emxDestroyArray_real_T(InterTypes);
}

int main(int argc, const char * const argv[])
{
  (void)argc;
  (void)argv;

  /* The initialize function is being called automatically from your entry-point function. So, a call to initialize is not included here. */
  /* Invoke the entry-point functions.
     You can call entry-point functions multiple times. */
  main_DEIPFunc2();

  /* Terminate the application.
     You do not need to do this more than one time. */
  DEIPFunc2_terminate();
  return 0;
}

/* End of code generation (main.c) */
