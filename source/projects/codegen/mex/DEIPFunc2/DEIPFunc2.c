/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc2.c
 *
 * Code generation for function 'DEIPFunc2'
 *
 */

/* Include files */
#include "DEIPFunc2.h"
#include "DEIPFunc2_data.h"
#include "DEIPFunc2_emxutil.h"
#include "DEIPFunc2_mexutil.h"
#include "DEIPFunc2_types.h"
#include "abs.h"
#include "any.h"
#include "colon.h"
#include "diag.h"
#include "eml_int_forloop_overflow_check.h"
#include "eml_setop.h"
#include "error.h"
#include "find.h"
#include "ifWhileCond.h"
#include "indexShapeCheck.h"
#include "isequal.h"
#include "ismember.h"
#include "nnz.h"
#include "norm.h"
#include "not.h"
#include "parenAssign2D.h"
#include "rt_nonfinite.h"
#include "sign.h"
#include "sort.h"
#include "sparse.h"
#include "sparse1.h"
#include "sub2ind.h"
#include "sum.h"
#include "unique.h"
#include "mwmathutil.h"
#include <string.h>

/* Variable Definitions */
static emlrtRTEInfo emlrtRTEI = { 282, /* lineNo */
  13,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo b_emlrtRTEI = { 288,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo c_emlrtRTEI = { 294,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRSInfo emlrtRSI = { 1118,  /* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo b_emlrtRSI = { 1063,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo c_emlrtRSI = { 1037,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo d_emlrtRSI = { 1022,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo e_emlrtRSI = { 1153,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo f_emlrtRSI = { 1135,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo g_emlrtRSI = { 1117,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo h_emlrtRSI = { 1097,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo i_emlrtRSI = { 1082,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo j_emlrtRSI = { 1065,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo k_emlrtRSI = { 1062,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo l_emlrtRSI = { 1061,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo m_emlrtRSI = { 1051,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo n_emlrtRSI = { 1039,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo o_emlrtRSI = { 1036,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo p_emlrtRSI = { 1035,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo q_emlrtRSI = { 1024,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo r_emlrtRSI = { 1021,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo s_emlrtRSI = { 1020,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo t_emlrtRSI = { 1009,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo u_emlrtRSI = { 1004,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo v_emlrtRSI = { 1000,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo w_emlrtRSI = { 991, /* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo x_emlrtRSI = { 978, /* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo y_emlrtRSI = { 968, /* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo ab_emlrtRSI = { 967,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo bb_emlrtRSI = { 956,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo cb_emlrtRSI = { 955,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo db_emlrtRSI = { 946,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo eb_emlrtRSI = { 944,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo fb_emlrtRSI = { 941,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo gb_emlrtRSI = { 935,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo hb_emlrtRSI = { 934,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo ib_emlrtRSI = { 929,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo jb_emlrtRSI = { 908,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo kb_emlrtRSI = { 899,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo lb_emlrtRSI = { 893,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo mb_emlrtRSI = { 889,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo nb_emlrtRSI = { 883,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo ob_emlrtRSI = { 869,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo pb_emlrtRSI = { 862,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo qb_emlrtRSI = { 858,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo rb_emlrtRSI = { 838,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo sb_emlrtRSI = { 835,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo tb_emlrtRSI = { 834,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo ub_emlrtRSI = { 833,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo vb_emlrtRSI = { 832,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo wb_emlrtRSI = { 831,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo xb_emlrtRSI = { 824,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo yb_emlrtRSI = { 823,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo ac_emlrtRSI = { 794,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo bc_emlrtRSI = { 793,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo cc_emlrtRSI = { 792,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo dc_emlrtRSI = { 791,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo ec_emlrtRSI = { 790,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo fc_emlrtRSI = { 782,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo gc_emlrtRSI = { 781,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo hc_emlrtRSI = { 779,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo ic_emlrtRSI = { 770,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo jc_emlrtRSI = { 765,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo kc_emlrtRSI = { 764,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo lc_emlrtRSI = { 763,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo mc_emlrtRSI = { 730,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo nc_emlrtRSI = { 729,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo oc_emlrtRSI = { 717,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo pc_emlrtRSI = { 693,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo qc_emlrtRSI = { 685,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo rc_emlrtRSI = { 677,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo sc_emlrtRSI = { 676,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo tc_emlrtRSI = { 659,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo uc_emlrtRSI = { 658,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo vc_emlrtRSI = { 649,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo wc_emlrtRSI = { 521,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo xc_emlrtRSI = { 508,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo yc_emlrtRSI = { 499,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo ad_emlrtRSI = { 498,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo bd_emlrtRSI = { 485,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo cd_emlrtRSI = { 456,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo dd_emlrtRSI = { 442,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo ed_emlrtRSI = { 426,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo fd_emlrtRSI = { 424,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo gd_emlrtRSI = { 423,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo hd_emlrtRSI = { 422,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo id_emlrtRSI = { 421,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo jd_emlrtRSI = { 420,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo kd_emlrtRSI = { 419,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo ld_emlrtRSI = { 372,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo md_emlrtRSI = { 371,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo nd_emlrtRSI = { 370,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo od_emlrtRSI = { 369,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo pd_emlrtRSI = { 368,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo qd_emlrtRSI = { 367,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo rd_emlrtRSI = { 366,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo sd_emlrtRSI = { 365,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo td_emlrtRSI = { 302,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo ud_emlrtRSI = { 294,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo vd_emlrtRSI = { 292,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo wd_emlrtRSI = { 288,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo xd_emlrtRSI = { 285,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo yd_emlrtRSI = { 282,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo ae_emlrtRSI = { 279,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo be_emlrtRSI = { 278,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo ce_emlrtRSI = { 276,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo de_emlrtRSI = { 273,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo ee_emlrtRSI = { 268,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo fe_emlrtRSI = { 267,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo ge_emlrtRSI = { 258,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo he_emlrtRSI = { 253,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo ie_emlrtRSI = { 245,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo je_emlrtRSI = { 227,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo ke_emlrtRSI = { 220,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo le_emlrtRSI = { 215,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo me_emlrtRSI = { 194,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo ne_emlrtRSI = { 183,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo oe_emlrtRSI = { 175,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo pe_emlrtRSI = { 157,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo qe_emlrtRSI = { 139,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo re_emlrtRSI = { 113,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo se_emlrtRSI = { 111,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo te_emlrtRSI = { 98, /* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo ue_emlrtRSI = { 76, /* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo ve_emlrtRSI = { 66, /* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo we_emlrtRSI = { 30, /* lineNo */
  "unique",                            /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pathName */
};

static emlrtRSInfo nf_emlrtRSI = { 38, /* lineNo */
  "ismember",                          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\ismember.m"/* pathName */
};

static emlrtRSInfo ug_emlrtRSI = { 39, /* lineNo */
  "find",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pathName */
};

static emlrtRSInfo ah_emlrtRSI = { 44, /* lineNo */
  "unique",                            /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pathName */
};

static emlrtRSInfo lh_emlrtRSI = { 19, /* lineNo */
  "setdiff",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\setdiff.m"/* pathName */
};

static emlrtRSInfo mh_emlrtRSI = { 70, /* lineNo */
  "eml_setop",                         /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\private\\eml_setop.m"/* pathName */
};

static emlrtRSInfo yh_emlrtRSI = { 328,/* lineNo */
  "unaryMinOrMaxDispatch",             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pathName */
};

static emlrtRSInfo ai_emlrtRSI = { 396,/* lineNo */
  "minOrMax2D",                        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pathName */
};

static emlrtRSInfo bi_emlrtRSI = { 478,/* lineNo */
  "minOrMax2DColumnMajorDim1",         /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pathName */
};

static emlrtRSInfo ci_emlrtRSI = { 476,/* lineNo */
  "minOrMax2DColumnMajorDim1",         /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pathName */
};

static emlrtRSInfo gi_emlrtRSI = { 38, /* lineNo */
  "fprintf",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\iofun\\fprintf.m"/* pathName */
};

static emlrtRSInfo hi_emlrtRSI = { 994,/* lineNo */
  "minOrMaxRealVectorKernel",          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pathName */
};

static emlrtRSInfo kj_emlrtRSI = { 35, /* lineNo */
  "find",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pathName */
};

static emlrtRSInfo rj_emlrtRSI = { 147,/* lineNo */
  "unaryMinOrMax",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pathName */
};

static emlrtRSInfo sj_emlrtRSI = { 1021,/* lineNo */
  "maxRealVectorOmitNaN",              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pathName */
};

static emlrtRSInfo tj_emlrtRSI = { 934,/* lineNo */
  "minOrMaxRealVector",                /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pathName */
};

static emlrtRSInfo uj_emlrtRSI = { 926,/* lineNo */
  "minOrMaxRealVector",                /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pathName */
};

static emlrtRSInfo vj_emlrtRSI = { 16, /* lineNo */
  "sub2ind",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\sub2ind.m"/* pathName */
};

static emlrtRSInfo rm_emlrtRSI = { 103,/* lineNo */
  "colon",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pathName */
};

static emlrtRSInfo io_emlrtRSI = { 23, /* lineNo */
  "intersect",                         /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\intersect.m"/* pathName */
};

static emlrtRSInfo uo_emlrtRSI = { 32, /* lineNo */
  "sort",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sort.m"/* pathName */
};

static emlrtMCInfo emlrtMCI = { 242,   /* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtMCInfo b_emlrtMCI = { 243, /* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtMCInfo c_emlrtMCI = { 250, /* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtMCInfo d_emlrtMCI = { 251, /* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtMCInfo e_emlrtMCI = { 257, /* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtMCInfo f_emlrtMCI = { 281, /* lineNo */
  13,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtMCInfo g_emlrtMCI = { 295, /* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtMCInfo h_emlrtMCI = { 287, /* lineNo */
  13,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtMCInfo l_emlrtMCI = { 64,  /* lineNo */
  18,                                  /* colNo */
  "fprintf",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\iofun\\fprintf.m"/* pName */
};

static emlrtRTEInfo mb_emlrtRTEI = { 13,/* lineNo */
  13,                                  /* colNo */
  "toLogicalCheck",                    /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\toLogicalCheck.m"/* pName */
};

static emlrtRTEInfo nb_emlrtRTEI = { 97,/* lineNo */
  27,                                  /* colNo */
  "unaryMinOrMax",                     /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pName */
};

static emlrtRTEInfo ob_emlrtRTEI = { 26,/* lineNo */
  27,                                  /* colNo */
  "unaryMinOrMax",                     /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pName */
};

static emlrtBCInfo emlrtBCI = { -1,    /* iFirst */
  -1,                                  /* iLast */
  134,                                 /* lineNo */
  23,                                  /* colNo */
  "PBCList",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo b_emlrtBCI = { 1,   /* iFirst */
  4,                                   /* iLast */
  136,                                 /* lineNo */
  28,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo d_emlrtDCI = { 136, /* lineNo */
  33,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo c_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  136,                                 /* lineNo */
  33,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo e_emlrtDCI = { 153, /* lineNo */
  26,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo d_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  153,                                 /* lineNo */
  26,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo e_emlrtBCI = { 1,   /* iFirst */
  4,                                   /* iLast */
  154,                                 /* lineNo */
  28,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo f_emlrtDCI = { 154, /* lineNo */
  33,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo f_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  154,                                 /* lineNo */
  33,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo g_emlrtDCI = { 189, /* lineNo */
  53,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo g_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  189,                                 /* lineNo */
  53,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo h_emlrtBCI = { 1,   /* iFirst */
  4,                                   /* iLast */
  189,                                 /* lineNo */
  39,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo i_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  189,                                 /* lineNo */
  59,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo j_emlrtBCI = { 1,   /* iFirst */
  20,                                  /* iLast */
  191,                                 /* lineNo */
  23,                                  /* colNo */
  "nodes",                             /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo k_emlrtBCI = { 1,   /* iFirst */
  20,                                  /* iLast */
  191,                                 /* lineNo */
  26,                                  /* colNo */
  "nodes",                             /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo emlrtECI = { -1,    /* nDims */
  191,                                 /* lineNo */
  17,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo l_emlrtBCI = { 1,   /* iFirst */
  20,                                  /* iLast */
  194,                                 /* lineNo */
  37,                                  /* colNo */
  "nodes",                             /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo m_emlrtBCI = { 1,   /* iFirst */
  20,                                  /* iLast */
  197,                                 /* lineNo */
  25,                                  /* colNo */
  "nodes",                             /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo b_emlrtECI = { -1,  /* nDims */
  197,                                 /* lineNo */
  17,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo n_emlrtBCI = { 1,   /* iFirst */
  20,                                  /* iLast */
  200,                                 /* lineNo */
  25,                                  /* colNo */
  "nodes",                             /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo c_emlrtECI = { -1,  /* nDims */
  200,                                 /* lineNo */
  17,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo o_emlrtBCI = { 1,   /* iFirst */
  20,                                  /* iLast */
  204,                                 /* lineNo */
  15,                                  /* colNo */
  "nodes",                             /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo p_emlrtBCI = { 1,   /* iFirst */
  20,                                  /* iLast */
  215,                                 /* lineNo */
  57,                                  /* colNo */
  "nodes",                             /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo q_emlrtBCI = { 1,   /* iFirst */
  4,                                   /* iLast */
  215,                                 /* lineNo */
  26,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo r_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  215,                                 /* lineNo */
  33,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo d_emlrtECI = { -1,  /* nDims */
  215,                                 /* lineNo */
  13,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo s_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  217,                                 /* lineNo */
  59,                                  /* colNo */
  "NodesOnLinknum",                    /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo t_emlrtBCI = { 1,   /* iFirst */
  4,                                   /* iLast */
  217,                                 /* lineNo */
  44,                                  /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo u_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  217,                                 /* lineNo */
  65,                                  /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo v_emlrtBCI = { 1,   /* iFirst */
  20,                                  /* iLast */
  217,                                 /* lineNo */
  19,                                  /* colNo */
  "links",                             /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo w_emlrtBCI = { 1,   /* iFirst */
  20,                                  /* iLast */
  217,                                 /* lineNo */
  22,                                  /* colNo */
  "links",                             /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo e_emlrtECI = { -1,  /* nDims */
  217,                                 /* lineNo */
  13,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo x_emlrtBCI = { 1,   /* iFirst */
  4,                                   /* iLast */
  223,                                 /* lineNo */
  23,                                  /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo y_emlrtBCI = { 1,   /* iFirst */
  20,                                  /* iLast */
  223,                                 /* lineNo */
  36,                                  /* colNo */
  "nodes",                             /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo f_emlrtECI = { -1,  /* nDims */
  223,                                 /* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo ab_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  267,                                 /* lineNo */
  25,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo h_emlrtDCI = { 267, /* lineNo */
  27,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo bb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  267,                                 /* lineNo */
  27,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo cb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  267,                                 /* lineNo */
  33,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo db_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  267,                                 /* lineNo */
  35,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo i_emlrtDCI = { 279, /* lineNo */
  39,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo eb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  279,                                 /* lineNo */
  39,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo j_emlrtDCI = { 279, /* lineNo */
  45,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo fb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  279,                                 /* lineNo */
  45,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo k_emlrtDCI = { 279, /* lineNo */
  54,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo gb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  279,                                 /* lineNo */
  54,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo l_emlrtDCI = { 283, /* lineNo */
  41,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo hb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  283,                                 /* lineNo */
  41,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo m_emlrtDCI = { 285, /* lineNo */
  33,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ib_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  285,                                 /* lineNo */
  33,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo jb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  285,                                 /* lineNo */
  37,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo n_emlrtDCI = { 285, /* lineNo */
  39,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo kb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  285,                                 /* lineNo */
  39,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo o_emlrtDCI = { 289, /* lineNo */
  24,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo lb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  289,                                 /* lineNo */
  24,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  292,                                 /* lineNo */
  29,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo p_emlrtDCI = { 292, /* lineNo */
  31,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo nb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  292,                                 /* lineNo */
  31,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo q_emlrtDCI = { 292, /* lineNo */
  37,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ob_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  292,                                 /* lineNo */
  37,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  312,                                 /* lineNo */
  37,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  312,                                 /* lineNo */
  42,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  312,                                 /* lineNo */
  44,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo pb_emlrtRTEI = { 355,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo sb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  330,                                 /* lineNo */
  30,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo tb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  333,                                 /* lineNo */
  34,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ub_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  345,                                 /* lineNo */
  30,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  369,                                 /* lineNo */
  35,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  369,                                 /* lineNo */
  37,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  369,                                 /* lineNo */
  55,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  369,                                 /* lineNo */
  57,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ac_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  369,                                 /* lineNo */
  75,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  369,                                 /* lineNo */
  77,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo cc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  371,                                 /* lineNo */
  39,                                  /* colNo */
  "col",                               /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo dc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  390,                                 /* lineNo */
  36,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ec_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  390,                                 /* lineNo */
  41,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  390,                                 /* lineNo */
  43,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  391,                                 /* lineNo */
  31,                                  /* colNo */
  "RegionOnElement",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo qb_emlrtRTEI = { 411,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo hc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  397,                                 /* lineNo */
  26,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ic_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  421,                                 /* lineNo */
  35,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo jc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  421,                                 /* lineNo */
  37,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo kc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  421,                                 /* lineNo */
  55,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo lc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  421,                                 /* lineNo */
  57,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  421,                                 /* lineNo */
  75,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo nc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  421,                                 /* lineNo */
  77,                                  /* colNo */
  "NodeRegInd",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo oc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  423,                                 /* lineNo */
  36,                                  /* colNo */
  "col",                               /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  434,                                 /* lineNo */
  39,                                  /* colNo */
  "Coordinates",                       /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  434,                                 /* lineNo */
  41,                                  /* colNo */
  "Coordinates",                       /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  475,                                 /* lineNo */
  32,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo sc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  475,                                 /* lineNo */
  37,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo tc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  475,                                 /* lineNo */
  39,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo uc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  487,                                 /* lineNo */
  32,                                  /* colNo */
  "RegionOnElement",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo r_emlrtDCI = { 496, /* lineNo */
  34,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo vc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  496,                                 /* lineNo */
  34,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo s_emlrtDCI = { 497, /* lineNo */
  34,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo wc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  497,                                 /* lineNo */
  34,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  515,                                 /* lineNo */
  28,                                  /* colNo */
  "twoelem",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  516,                                 /* lineNo */
  33,                                  /* colNo */
  "twoelem",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ad_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  518,                                 /* lineNo */
  33,                                  /* colNo */
  "twoelem",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  536,                                 /* lineNo */
  44,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo cd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  543,                                 /* lineNo */
  44,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo dd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  587,                                 /* lineNo */
  37,                                  /* colNo */
  "FacetsOnElement",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ed_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  591,                                 /* lineNo */
  45,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  591,                                 /* lineNo */
  51,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  591,                                 /* lineNo */
  53,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  593,                                 /* lineNo */
  40,                                  /* colNo */
  "RegionOnElement",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo t_emlrtDCI = { 615, /* lineNo */
  33,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo id_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  615,                                 /* lineNo */
  33,                                  /* colNo */
  "numEonF",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo jd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  658,                                 /* lineNo */
  51,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo kd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  659,                                 /* lineNo */
  51,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ld_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  660,                                 /* lineNo */
  48,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo md_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  661,                                 /* lineNo */
  48,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo u_emlrtDCI = { 674, /* lineNo */
  51,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo nd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  674,                                 /* lineNo */
  51,                                  /* colNo */
  "ElementsOnNodePBCNum",              /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo v_emlrtDCI = { 675, /* lineNo */
  51,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo od_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  675,                                 /* lineNo */
  51,                                  /* colNo */
  "ElementsOnNodePBCNum",              /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  703,                                 /* lineNo */
  43,                                  /* colNo */
  "numEonPBC",                         /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  761,                                 /* lineNo */
  35,                                  /* colNo */
  "ElementsOnFacet",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  761,                                 /* lineNo */
  37,                                  /* colNo */
  "ElementsOnFacet",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo sd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  762,                                 /* lineNo */
  41,                                  /* colNo */
  "ElementsOnBoundary",                /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo td_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  762,                                 /* lineNo */
  43,                                  /* colNo */
  "ElementsOnBoundary",                /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ud_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  639,                                 /* lineNo */
  37,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  645,                                 /* lineNo */
  37,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  743,                                 /* lineNo */
  33,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  749,                                 /* lineNo */
  33,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  763,                                 /* lineNo */
  41,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ae_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  763,                                 /* lineNo */
  43,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo be_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  763,                                 /* lineNo */
  67,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ce_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  763,                                 /* lineNo */
  69,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo de_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  763,                                 /* lineNo */
  93,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ee_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  763,                                 /* lineNo */
  95,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fe_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  764,                                 /* lineNo */
  44,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ge_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  764,                                 /* lineNo */
  46,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo he_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  764,                                 /* lineNo */
  70,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ie_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  764,                                 /* lineNo */
  72,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo je_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  764,                                 /* lineNo */
  96,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ke_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  764,                                 /* lineNo */
  98,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo le_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  765,                                 /* lineNo */
  44,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo me_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  765,                                 /* lineNo */
  46,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ne_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  765,                                 /* lineNo */
  70,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo oe_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  765,                                 /* lineNo */
  72,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pe_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  765,                                 /* lineNo */
  96,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qe_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  765,                                 /* lineNo */
  98,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo re_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  770,                                 /* lineNo */
  48,                                  /* colNo */
  "FacetsOnInterface",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo se_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  770,                                 /* lineNo */
  50,                                  /* colNo */
  "FacetsOnInterface",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo rb_emlrtRTEI = { 772,/* lineNo */
  11,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo te_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  781,                                 /* lineNo */
  40,                                  /* colNo */
  "FacetsOnPBC",                       /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ue_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  781,                                 /* lineNo */
  42,                                  /* colNo */
  "FacetsOnPBC",                       /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ve_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  782,                                 /* lineNo */
  31,                                  /* colNo */
  "FacetsOnPBC",                       /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo we_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  782,                                 /* lineNo */
  48,                                  /* colNo */
  "FacetsOnPBC",                       /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo sb_emlrtRTEI = { 784,/* lineNo */
  15,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo tb_emlrtRTEI = { 789,/* lineNo */
  15,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo xe_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  790,                                 /* lineNo */
  39,                                  /* colNo */
  "FacetsOnInterfaceNum",              /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ye_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  790,                                 /* lineNo */
  66,                                  /* colNo */
  "FacetsOnInterfaceNum",              /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo af_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  792,                                 /* lineNo */
  33,                                  /* colNo */
  "FacetsOnPBCNum",                    /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  792,                                 /* lineNo */
  54,                                  /* colNo */
  "FacetsOnPBCNum",                    /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo ub_emlrtRTEI = { 816,/* lineNo */
  12,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo vb_emlrtRTEI = { 845,/* lineNo */
  12,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo cf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  856,                                 /* lineNo */
  30,                                  /* colNo */
  "FacetsOnNodeNum",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo df_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  861,                                 /* lineNo */
  42,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo wb_emlrtRTEI = { 865,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo ef_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  867,                                 /* lineNo */
  30,                                  /* colNo */
  "elems",                             /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo w_emlrtDCI = { 869, /* lineNo */
  49,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ff_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  869,                                 /* lineNo */
  49,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  869,                                 /* lineNo */
  54,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  869,                                 /* lineNo */
  56,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo if_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  870,                                 /* lineNo */
  46,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo xb_emlrtRTEI = { 876,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo jf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  898,                                 /* lineNo */
  58,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo yb_emlrtRTEI = { 902,/* lineNo */
  40,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo kf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  904,                                 /* lineNo */
  46,                                  /* colNo */
  "elems",                             /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo x_emlrtDCI = { 905, /* lineNo */
  55,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo lf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  905,                                 /* lineNo */
  55,                                  /* colNo */
  "RegionOnElement",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  908,                                 /* lineNo */
  69,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo nf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  908,                                 /* lineNo */
  74,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo of_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  908,                                 /* lineNo */
  76,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  910,                                 /* lineNo */
  77,                                  /* colNo */
  "Coordinates",                       /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo y_emlrtDCI = { 928, /* lineNo */
  25,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtRTEInfo ac_emlrtRTEI = { 933,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo qf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  930,                                 /* lineNo */
  17,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  930,                                 /* lineNo */
  19,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo sf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  930,                                 /* lineNo */
  21,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo g_emlrtECI = { -1,  /* nDims */
  930,                                 /* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo tf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  954,                                 /* lineNo */
  53,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo uf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  954,                                 /* lineNo */
  43,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  954,                                 /* lineNo */
  45,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  954,                                 /* lineNo */
  60,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  966,                                 /* lineNo */
  53,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yf_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  966,                                 /* lineNo */
  43,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ag_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  966,                                 /* lineNo */
  45,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  966,                                 /* lineNo */
  60,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo cg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  978,                                 /* lineNo */
  49,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo dg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  978,                                 /* lineNo */
  39,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo eg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  978,                                 /* lineNo */
  41,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  978,                                 /* lineNo */
  56,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  978,                                 /* lineNo */
  82,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  978,                                 /* lineNo */
  72,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ig_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  978,                                 /* lineNo */
  74,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo jg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  978,                                 /* lineNo */
  89,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo kg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  977,                                 /* lineNo */
  29,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo lg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  977,                                 /* lineNo */
  31,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  977,                                 /* lineNo */
  61,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo h_emlrtECI = { -1,  /* nDims */
  977,                                 /* lineNo */
  21,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo ng_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  979,                                 /* lineNo */
  31,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo bc_emlrtRTEI = { 994,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo og_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  998,                                 /* lineNo */
  37,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  998,                                 /* lineNo */
  39,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  998,                                 /* lineNo */
  48,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ab_emlrtDCI = { 1000,/* lineNo */
  53,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo rg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1000,                                /* lineNo */
  53,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo sg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1000,                                /* lineNo */
  58,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo tg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1000,                                /* lineNo */
  60,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ug_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1001,                                /* lineNo */
  48,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1002,                                /* lineNo */
  36,                                  /* colNo */
  "nodeP2",                            /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wg_emlrtBCI = { 1,  /* iFirst */
  4,                                   /* iLast */
  1004,                                /* lineNo */
  53,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1004,                                /* lineNo */
  65,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yg_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1010,                                /* lineNo */
  41,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ah_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1010,                                /* lineNo */
  43,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1010,                                /* lineNo */
  52,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo bb_emlrtDCI = { 1013,/* lineNo */
  64,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ch_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1013,                                /* lineNo */
  64,                                  /* colNo */
  "Coordinates",                       /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo cc_emlrtRTEI = { 1016,/* lineNo */
  36,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo dh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1018,                                /* lineNo */
  42,                                  /* colNo */
  "elems",                             /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo cb_emlrtDCI = { 1019,/* lineNo */
  51,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo eh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1019,                                /* lineNo */
  51,                                  /* colNo */
  "RegionOnElement",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1021,                                /* lineNo */
  81,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1024,                                /* lineNo */
  61,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1024,                                /* lineNo */
  66,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ih_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1024,                                /* lineNo */
  68,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo dc_emlrtRTEI = { 1031,/* lineNo */
  36,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo jh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1033,                                /* lineNo */
  42,                                  /* colNo */
  "elems",                             /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo db_emlrtDCI = { 1034,/* lineNo */
  51,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo kh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1034,                                /* lineNo */
  51,                                  /* colNo */
  "RegionOnElement",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo lh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1036,                                /* lineNo */
  81,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1039,                                /* lineNo */
  61,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo nh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1039,                                /* lineNo */
  66,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo oh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1039,                                /* lineNo */
  68,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo ec_emlrtRTEI = { 1048,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo ph_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1052,                                /* lineNo */
  41,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1052,                                /* lineNo */
  43,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1052,                                /* lineNo */
  52,                                  /* colNo */
  "Sectors",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo sh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1054,                                /* lineNo */
  65,                                  /* colNo */
  "Coordinates",                       /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo fc_emlrtRTEI = { 1057,/* lineNo */
  36,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo th_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1059,                                /* lineNo */
  42,                                  /* colNo */
  "elems",                             /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo eb_emlrtDCI = { 1060,/* lineNo */
  51,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo uh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1060,                                /* lineNo */
  51,                                  /* colNo */
  "RegionOnElement",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1062,                                /* lineNo */
  81,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1065,                                /* lineNo */
  61,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1065,                                /* lineNo */
  66,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yh_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1065,                                /* lineNo */
  68,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ai_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  871,                                 /* lineNo */
  34,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo i_emlrtECI = { -1,  /* nDims */
  871,                                 /* lineNo */
  17,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo bi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  909,                                 /* lineNo */
  54,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ci_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1025,                                /* lineNo */
  46,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo di_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1040,                                /* lineNo */
  46,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ei_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1066,                                /* lineNo */
  46,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo gc_emlrtRTEI = { 1090,/* lineNo */
  12,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo fi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1104,                                /* lineNo */
  40,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1104,                                /* lineNo */
  45,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1104,                                /* lineNo */
  47,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ii_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  434,                                 /* lineNo */
  14,                                  /* colNo */
  "Coordinates3",                      /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ji_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  434,                                 /* lineNo */
  16,                                  /* colNo */
  "Coordinates3",                      /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo j_emlrtECI = { -1,  /* nDims */
  434,                                 /* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo ki_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  910,                                 /* lineNo */
  50,                                  /* colNo */
  "Coordinates3",                      /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo li_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1013,                                /* lineNo */
  38,                                  /* colNo */
  "Coordinates3",                      /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1054,                                /* lineNo */
  38,                                  /* colNo */
  "Coordinates3",                      /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ni_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1115,                                /* lineNo */
  62,                                  /* colNo */
  "Coordinates3",                      /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo oi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1115,                                /* lineNo */
  34,                                  /* colNo */
  "Coordinates3",                      /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo fb_emlrtDCI = { 1117,/* lineNo */
  73,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo pi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1117,                                /* lineNo */
  73,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1150,                                /* lineNo */
  28,                                  /* colNo */
  "Coordinates3",                      /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ri_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1150,                                /* lineNo */
  30,                                  /* colNo */
  "Coordinates3",                      /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo k_emlrtECI = { 2,   /* nDims */
  1152,                                /* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtECInfo l_emlrtECI = { -1,  /* nDims */
  1088,                                /* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtECInfo m_emlrtECI = { -1,  /* nDims */
  1143,                                /* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtBCInfo si_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  802,                                 /* lineNo */
  56,                                  /* colNo */
  "FacetsOnIntMinusPBCNum",            /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ti_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  802,                                 /* lineNo */
  85,                                  /* colNo */
  "FacetsOnIntMinusPBCNum",            /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ui_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  802,                                 /* lineNo */
  33,                                  /* colNo */
  "FacetsOnIntMinusPBC",               /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  802,                                 /* lineNo */
  62,                                  /* colNo */
  "FacetsOnIntMinusPBC",               /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo n_emlrtECI = { -1,  /* nDims */
  802,                                 /* lineNo */
  13,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtDCInfo gb_emlrtDCI = { 74, /* lineNo */
  22,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo hb_emlrtDCI = { 74, /* lineNo */
  22,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo ib_emlrtDCI = { 100,/* lineNo */
  22,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo jb_emlrtDCI = { 102,/* lineNo */
  23,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo kb_emlrtDCI = { 383,/* lineNo */
  26,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo lb_emlrtDCI = { 383,/* lineNo */
  26,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo mb_emlrtDCI = { 384,/* lineNo */
  31,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo nb_emlrtDCI = { 384,/* lineNo */
  31,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo ob_emlrtDCI = { 304,/* lineNo */
  26,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo pb_emlrtDCI = { 304,/* lineNo */
  26,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo qb_emlrtDCI = { 305,/* lineNo */
  34,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo rb_emlrtDCI = { 305,/* lineNo */
  34,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo sb_emlrtDCI = { 439,/* lineNo */
  25,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo tb_emlrtDCI = { 441,/* lineNo */
  28,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo ub_emlrtDCI = { 456,/* lineNo */
  27,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo vb_emlrtDCI = { 456,/* lineNo */
  27,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo wb_emlrtDCI = { 844,/* lineNo */
  19,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo xb_emlrtDCI = { 844,/* lineNo */
  19,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo yb_emlrtDCI = { 844,/* lineNo */
  26,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo ac_emlrtDCI = { 1142,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo bc_emlrtDCI = { 1087,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo cc_emlrtDCI = { 62, /* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo dc_emlrtDCI = { 62, /* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo ec_emlrtDCI = { 69, /* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo fc_emlrtDCI = { 74, /* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo gc_emlrtDCI = { 100,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo hc_emlrtDCI = { 101,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo ic_emlrtDCI = { 102,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo jc_emlrtDCI = { 103,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo wi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  135,                                 /* lineNo */
  12,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo kc_emlrtDCI = { 135,/* lineNo */
  12,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo xi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  178,                                 /* lineNo */
  16,                                  /* colNo */
  "MorePBC",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yi_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  179,                                 /* lineNo */
  8,                                   /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo aj_emlrtBCI = { 1,  /* iFirst */
  4,                                   /* iLast */
  146,                                 /* lineNo */
  9,                                   /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  3                                    /* checkKind */
};

static emlrtBCInfo bj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  146,                                 /* lineNo */
  9,                                   /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo lc_emlrtDCI = { 146,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo cj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  147,                                 /* lineNo */
  9,                                   /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo mc_emlrtDCI = { 147,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo dj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  149,                                 /* lineNo */
  16,                                  /* colNo */
  "NodesOnLinknum",                    /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo nc_emlrtDCI = { 149,/* lineNo */
  16,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ej_emlrtBCI = { 1,  /* iFirst */
  4,                                   /* iLast */
  150,                                 /* lineNo */
  9,                                   /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  3                                    /* checkKind */
};

static emlrtBCInfo fj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  150,                                 /* lineNo */
  9,                                   /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo oc_emlrtDCI = { 150,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo gj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  151,                                 /* lineNo */
  9,                                   /* colNo */
  "NodesOnLinknum",                    /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo pc_emlrtDCI = { 151,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo hj_emlrtBCI = { 1,  /* iFirst */
  4,                                   /* iLast */
  164,                                 /* lineNo */
  9,                                   /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  3                                    /* checkKind */
};

static emlrtBCInfo ij_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  164,                                 /* lineNo */
  9,                                   /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo qc_emlrtDCI = { 164,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo jj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  165,                                 /* lineNo */
  9,                                   /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo rc_emlrtDCI = { 165,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo kj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  167,                                 /* lineNo */
  16,                                  /* colNo */
  "NodesOnLinknum",                    /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo sc_emlrtDCI = { 167,/* lineNo */
  16,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo lj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  169,                                 /* lineNo */
  9,                                   /* colNo */
  "NodesOnLinknum",                    /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo tc_emlrtDCI = { 169,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo mj_emlrtBCI = { 1,  /* iFirst */
  4,                                   /* iLast */
  168,                                 /* lineNo */
  9,                                   /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  3                                    /* checkKind */
};

static emlrtBCInfo nj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  168,                                 /* lineNo */
  9,                                   /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo uc_emlrtDCI = { 168,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo oj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  214,                                 /* lineNo */
  13,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo vc_emlrtDCI = { 214,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo pj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  221,                                 /* lineNo */
  18,                                  /* colNo */
  "links2",                            /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  274,                                 /* lineNo */
  12,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  216,                                 /* lineNo */
  23,                                  /* colNo */
  "NodesOnLinknum",                    /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo wc_emlrtDCI = { 223,/* lineNo */
  28,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo sj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  223,                                 /* lineNo */
  28,                                  /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo xc_emlrtDCI = { 224,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo tj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  224,                                 /* lineNo */
  24,                                  /* colNo */
  "NodesOnLinknum",                    /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo uj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  277,                                 /* lineNo */
  13,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo yc_emlrtDCI = { 277,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo ad_emlrtDCI = { 383,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo bd_emlrtDCI = { 304,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo cd_emlrtDCI = { 304,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo dd_emlrtDCI = { 384,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo ed_emlrtDCI = { 305,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo fd_emlrtDCI = { 283,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo vj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  283,                                 /* lineNo */
  24,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  395,                                 /* lineNo */
  20,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  313,                                 /* lineNo */
  15,                                  /* colNo */
  "RegionOnElement",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yj_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  418,                                 /* lineNo */
  5,                                   /* colNo */
  "NodesOnElement",                    /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ak_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  413,                                 /* lineNo */
  13,                                  /* colNo */
  "ElementsOnNodeRow",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  402,                                 /* lineNo */
  20,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo gd_emlrtDCI = { 402,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ck_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  405,                                 /* lineNo */
  13,                                  /* colNo */
  "ElementsOnNodeRow",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo dk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  317,                                 /* lineNo */
  20,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ek_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  404,                                 /* lineNo */
  13,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  321,                                 /* lineNo */
  20,                                  /* colNo */
  "ElementsOnNodePBCNum",              /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo hd_emlrtDCI = { 321,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo gk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  323,                                 /* lineNo */
  13,                                  /* colNo */
  "ElementsOnNodePBCRow",              /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  363,                                 /* lineNo */
  5,                                   /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ik_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  322,                                 /* lineNo */
  13,                                  /* colNo */
  "ElementsOnNodePBCNum",              /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo jk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  289,                                 /* lineNo */
  28,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo kk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  357,                                 /* lineNo */
  13,                                  /* colNo */
  "ElementsOnNodeRow",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo lk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  358,                                 /* lineNo */
  13,                                  /* colNo */
  "ElementsOnNodePBCRow",              /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  327,                                 /* lineNo */
  18,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo nk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  328,                                 /* lineNo */
  37,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ok_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  347,                                 /* lineNo */
  24,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  349,                                 /* lineNo */
  17,                                  /* colNo */
  "ElementsOnNodeRow",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  348,                                 /* lineNo */
  17,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  364,                                 /* lineNo */
  5,                                   /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo sk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  335,                                 /* lineNo */
  17,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo tk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  337,                                 /* lineNo */
  24,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo id_emlrtDCI = { 337,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo uk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  339,                                 /* lineNo */
  17,                                  /* colNo */
  "ElementsOnNodeRow",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  338,                                 /* lineNo */
  17,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  425,                                 /* lineNo */
  5,                                   /* colNo */
  "NodesOnElement",                    /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo jd_emlrtDCI = { 437,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo kd_emlrtDCI = { 437,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo ld_emlrtDCI = { 439,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo md_emlrtDCI = { 440,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo nd_emlrtDCI = { 441,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo od_emlrtDCI = { 443,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo pd_emlrtDCI = { 444,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo xk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  373,                                 /* lineNo */
  5,                                   /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yk_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  374,                                 /* lineNo */
  5,                                   /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo qd_emlrtDCI = { 456,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo al_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  485,                                 /* lineNo */
  11,                                  /* colNo */
  "FacetsOnElement",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  493,                                 /* lineNo */
  17,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo cl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  490,                                 /* lineNo */
  17,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo dl_emlrtBCI = { 1,  /* iFirst */
  3,                                   /* iLast */
  490,                                 /* lineNo */
  17,                                  /* colNo */
  "nloop3",                            /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo el_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  494,                                 /* lineNo */
  17,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  491,                                 /* lineNo */
  17,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gl_emlrtBCI = { 1,  /* iFirst */
  3,                                   /* iLast */
  491,                                 /* lineNo */
  17,                                  /* colNo */
  "nloop3",                            /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  536,                                 /* lineNo */
  21,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo rd_emlrtDCI = { 536,/* lineNo */
  21,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo il_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  736,                                 /* lineNo */
  13,                                  /* colNo */
  "FacetsOnElement",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo jl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  539,                                 /* lineNo */
  25,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo kl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  737,                                 /* lineNo */
  13,                                  /* colNo */
  "ElementsOnBoundary",                /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ll_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  738,                                 /* lineNo */
  13,                                  /* colNo */
  "ElementsOnBoundary",                /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ml_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  543,                                 /* lineNo */
  21,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo nl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  508,                                 /* lineNo */
  39,                                  /* colNo */
  "ElementsOnNodeA",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ol_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  740,                                 /* lineNo */
  22,                                  /* colNo */
  "FacetsOnNodeNum",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  741,                                 /* lineNo */
  13,                                  /* colNo */
  "FacetsOnNodeNum",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ql_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  546,                                 /* lineNo */
  25,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  746,                                 /* lineNo */
  22,                                  /* colNo */
  "FacetsOnNodeNum",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo sl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  747,                                 /* lineNo */
  13,                                  /* colNo */
  "FacetsOnNodeNum",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo tl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  773,                                 /* lineNo */
  35,                                  /* colNo */
  "FacetsOnInterfaceNum",              /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ul_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  773,                                 /* lineNo */
  63,                                  /* colNo */
  "FacetsOnInterfaceNum",              /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  618,                                 /* lineNo */
  17,                                  /* colNo */
  "numEonF",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  619,                                 /* lineNo */
  17,                                  /* colNo */
  "FacetsOnElement",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  620,                                 /* lineNo */
  17,                                  /* colNo */
  "FacetsOnElement",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yl_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  621,                                 /* lineNo */
  17,                                  /* colNo */
  "FacetsOnElementInt",                /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo am_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  622,                                 /* lineNo */
  17,                                  /* colNo */
  "FacetsOnElementInt",                /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  627,                                 /* lineNo */
  25,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo cm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  624,                                 /* lineNo */
  25,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo dm_emlrtBCI = { 1,  /* iFirst */
  3,                                   /* iLast */
  624,                                 /* lineNo */
  25,                                  /* colNo */
  "nloop3",                            /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo em_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  628,                                 /* lineNo */
  25,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  625,                                 /* lineNo */
  25,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gm_emlrtBCI = { 1,  /* iFirst */
  3,                                   /* iLast */
  625,                                 /* lineNo */
  25,                                  /* colNo */
  "nloop3",                            /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  630,                                 /* lineNo */
  17,                                  /* colNo */
  "ElementsOnFacet",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo im_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  631,                                 /* lineNo */
  17,                                  /* colNo */
  "ElementsOnFacet",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo jm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  632,                                 /* lineNo */
  17,                                  /* colNo */
  "ElementsOnFacet",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo km_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  820,                                 /* lineNo */
  12,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo lm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  633,                                 /* lineNo */
  17,                                  /* colNo */
  "ElementsOnFacet",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  634,                                 /* lineNo */
  17,                                  /* colNo */
  "FacetsOnInterface",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo nm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  636,                                 /* lineNo */
  27,                                  /* colNo */
  "FacetsOnNodeNum",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo sd_emlrtDCI = { 636,/* lineNo */
  27,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo td_emlrtDCI = { 787,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  4                                    /* checkKind */
};

static emlrtBCInfo om_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  785,                                 /* lineNo */
  33,                                  /* colNo */
  "FacetsOnPBCNum",                    /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  785,                                 /* lineNo */
  55,                                  /* colNo */
  "FacetsOnPBCNum",                    /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  637,                                 /* lineNo */
  17,                                  /* colNo */
  "FacetsOnNodeNum",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  642,                                 /* lineNo */
  27,                                  /* colNo */
  "FacetsOnNodeNum",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ud_emlrtDCI = { 642,/* lineNo */
  27,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo sm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  643,                                 /* lineNo */
  17,                                  /* colNo */
  "FacetsOnNodeNum",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo vd_emlrtDCI = { 844,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo tm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  847,                                 /* lineNo */
  9,                                   /* colNo */
  "interreg2",                         /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo um_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  853,                                 /* lineNo */
  12,                                  /* colNo */
  "NodesOnInterface",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  848,                                 /* lineNo */
  9,                                   /* colNo */
  "interreg2",                         /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo wd_emlrtDCI = { 791,/* lineNo */
  36,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo wm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  791,                                 /* lineNo */
  36,                                  /* colNo */
  "FacetsOnInterface",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xm_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  717,                                 /* lineNo */
  20,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo xd_emlrtDCI = { 717,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo yd_emlrtDCI = { 1141,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ym_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  654,                                 /* lineNo */
  30,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo an_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  651,                                 /* lineNo */
  30,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bn_emlrtBCI = { 1,  /* iFirst */
  3,                                   /* iLast */
  651,                                 /* lineNo */
  30,                                  /* colNo */
  "nloop3",                            /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo cn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  655,                                 /* lineNo */
  30,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo dn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  652,                                 /* lineNo */
  30,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo en_emlrtBCI = { 1,  /* iFirst */
  3,                                   /* iLast */
  652,                                 /* lineNo */
  30,                                  /* colNo */
  "nloop3",                            /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  719,                                 /* lineNo */
  21,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  720,                                 /* lineNo */
  21,                                  /* colNo */
  "FacetsOnNodeInd",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  927,                                 /* lineNo */
  22,                                  /* colNo */
  "ElementsOnNodeNum",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ae_emlrtDCI = { 1086,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo in_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  858,                                 /* lineNo */
  22,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo be_emlrtDCI = { 1142,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo ce_emlrtDCI = { 1087,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo jn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  880,                                 /* lineNo */
  24,                                  /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo de_emlrtDCI = { 793,/* lineNo */
  30,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo kn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  793,                                 /* lineNo */
  30,                                  /* colNo */
  "FacetsOnPBC",                       /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ln_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  660,                                 /* lineNo */
  54,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  661,                                 /* lineNo */
  54,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo nn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  800,                                 /* lineNo */
  41,                                  /* colNo */
  "FacetsOnIntMinusPBCNum",            /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo on_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  800,                                 /* lineNo */
  9,                                   /* colNo */
  "FacetsOnIntMinusPBCNum",            /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1093,                                /* lineNo */
  8,                                   /* colNo */
  "InterTypes",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ee_emlrtDCI = { 931,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo qn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1136,                                /* lineNo */
  20,                                  /* colNo */
  "ElementsOnNodeNum2",                /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  870,                                 /* lineNo */
  51,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo sn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  871,                                 /* lineNo */
  39,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo tn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1103,                                /* lineNo */
  20,                                  /* colNo */
  "elems",                             /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo un_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  991,                                 /* lineNo */
  22,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  992,                                 /* lineNo */
  25,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo fe_emlrtDCI = { 993,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo wn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1049,                                /* lineNo */
  28,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  995,                                 /* lineNo */
  28,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yn_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  947,                                 /* lineNo */
  29,                                  /* colNo */
  "ElementsOnFacet",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ge_emlrtDCI = { 947,/* lineNo */
  29,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ao_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1108,                                /* lineNo */
  24,                                  /* colNo */
  "NodesOnElementCG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  948,                                 /* lineNo */
  29,                                  /* colNo */
  "ElementsOnFacet",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo co_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1109,                                /* lineNo */
  20,                                  /* colNo */
  "ElementsOnNodeNum2",                /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo he_emlrtDCI = { 1109,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo do_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1114,                                /* lineNo */
  21,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo eo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1110,                                /* lineNo */
  21,                                  /* colNo */
  "ElementsOnNodeNum2",                /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1111,                                /* lineNo */
  21,                                  /* colNo */
  "NodeCGDG",                          /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo go_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  953,                                 /* lineNo */
  28,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ho_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1116,                                /* lineNo */
  31,                                  /* colNo */
  "NodesOnElement",                    /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo io_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  909,                                 /* lineNo */
  59,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo jo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  705,                                 /* lineNo */
  25,                                  /* colNo */
  "numEonPBC",                         /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ko_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  965,                                 /* lineNo */
  28,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo lo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  707,                                 /* lineNo */
  25,                                  /* colNo */
  "FacetsOnPBC",                       /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1001,                                /* lineNo */
  53,                                  /* colNo */
  "NodesOnElementPBC",                 /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo no_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  977,                                 /* lineNo */
  31,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo oo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  977,                                 /* lineNo */
  46,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo po_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  685,                                 /* lineNo */
  53,                                  /* colNo */
  "ElementsOnNodeA",                   /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1009,                                /* lineNo */
  33,                                  /* colNo */
  "copyonce",                          /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ro_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1122,                                /* lineNo */
  28,                                  /* colNo */
  "ElementsOnNodeNum2",                /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo so_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1123,                                /* lineNo */
  21,                                  /* colNo */
  "ElementsOnNodeNum2",                /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo to_emlrtBCI = { 1,  /* iFirst */
  12,                                  /* iLast */
  1124,                                /* lineNo */
  21,                                  /* colNo */
  "NodeCGDG",                          /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  3                                    /* checkKind */
};

static emlrtBCInfo uo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1124,                                /* lineNo */
  21,                                  /* colNo */
  "NodeCGDG",                          /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1029,                                /* lineNo */
  34,                                  /* colNo */
  "copyonce",                          /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  980,                                 /* lineNo */
  38,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  980,                                 /* lineNo */
  53,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yo_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  980,                                 /* lineNo */
  21,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ap_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  981,                                 /* lineNo */
  21,                                  /* colNo */
  "numSecs",                           /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1066,                                /* lineNo */
  51,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo cp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1040,                                /* lineNo */
  51,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo dp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  1025,                                /* lineNo */
  51,                                  /* colNo */
  "NodesOnElementDG",                  /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ep_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  118,                                 /* lineNo */
  13,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ie_emlrtDCI = { 118,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo fp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  119,                                 /* lineNo */
  13,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo je_emlrtDCI = { 119,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo gp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  121,                                 /* lineNo */
  13,                                  /* colNo */
  "NodesOnPBC",                        /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ke_emlrtDCI = { 121,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo hp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  122,                                 /* lineNo */
  13,                                  /* colNo */
  "NodesOnPBCnum",                     /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo le_emlrtDCI = { 122,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ip_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  124,                                 /* lineNo */
  13,                                  /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo me_emlrtDCI = { 124,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo jp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  124,                                 /* lineNo */
  45,                                  /* colNo */
  "IA",                                /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo kp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  125,                                 /* lineNo */
  13,                                  /* colNo */
  "NodesOnLinknum",                    /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo ne_emlrtDCI = { 125,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo lp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  127,                                 /* lineNo */
  13,                                  /* colNo */
  "NodesOnLink",                       /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo oe_emlrtDCI = { 127,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo mp_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  127,                                 /* lineNo */
  45,                                  /* colNo */
  "IA",                                /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo np_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  128,                                 /* lineNo */
  13,                                  /* colNo */
  "NodesOnLinknum",                    /* aName */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo pe_emlrtDCI = { 128,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtRTEInfo ye_emlrtRTEI = { 62,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo af_emlrtRTEI = { 71,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo bf_emlrtRTEI = { 69,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo cf_emlrtRTEI = { 74,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo df_emlrtRTEI = { 75,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ef_emlrtRTEI = { 89,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ff_emlrtRTEI = { 86,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo gf_emlrtRTEI = { 100,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo hf_emlrtRTEI = { 101,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo if_emlrtRTEI = { 102,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo jf_emlrtRTEI = { 103,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo kf_emlrtRTEI = { 111,/* lineNo */
  28,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo lf_emlrtRTEI = { 175,/* lineNo */
  16,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo mf_emlrtRTEI = { 31,/* lineNo */
  9,                                   /* colNo */
  "unique",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\unique.m"/* pName */
};

static emlrtRTEInfo nf_emlrtRTEI = { 175,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo of_emlrtRTEI = { 227,/* lineNo */
  21,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo pf_emlrtRTEI = { 383,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo qf_emlrtRTEI = { 304,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo rf_emlrtRTEI = { 384,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo sf_emlrtRTEI = { 9,/* lineNo */
  10,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo tf_emlrtRTEI = { 305,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo uf_emlrtRTEI = { 279,/* lineNo */
  28,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo vf_emlrtRTEI = { 292,/* lineNo */
  18,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo wf_emlrtRTEI = { 279,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo xf_emlrtRTEI = { 306,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo yf_emlrtRTEI = { 283,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ag_emlrtRTEI = { 285,/* lineNo */
  22,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo bg_emlrtRTEI = { 419,/* lineNo */
  63,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo cg_emlrtRTEI = { 285,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo dg_emlrtRTEI = { 74,/* lineNo */
  13,                                  /* colNo */
  "eml_mtimes_helper",                 /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\eml_mtimes_helper.m"/* pName */
};

static emlrtRTEInfo eg_emlrtRTEI = { 289,/* lineNo */
  28,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo fg_emlrtRTEI = { 421,/* lineNo */
  22,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo gg_emlrtRTEI = { 365,/* lineNo */
  65,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo hg_emlrtRTEI = { 421,/* lineNo */
  42,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ig_emlrtRTEI = { 74,/* lineNo */
  24,                                  /* colNo */
  "eml_mtimes_helper",                 /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\eml_mtimes_helper.m"/* pName */
};

static emlrtRTEInfo jg_emlrtRTEI = { 421,/* lineNo */
  62,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo kg_emlrtRTEI = { 36,/* lineNo */
  5,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo lg_emlrtRTEI = { 37,/* lineNo */
  5,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo mg_emlrtRTEI = { 423,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ng_emlrtRTEI = { 369,/* lineNo */
  22,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo og_emlrtRTEI = { 369,/* lineNo */
  42,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo pg_emlrtRTEI = { 369,/* lineNo */
  62,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo qg_emlrtRTEI = { 426,/* lineNo */
  32,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo rg_emlrtRTEI = { 433,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo sg_emlrtRTEI = { 437,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo tg_emlrtRTEI = { 371,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ug_emlrtRTEI = { 439,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo vg_emlrtRTEI = { 440,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo wg_emlrtRTEI = { 441,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo xg_emlrtRTEI = { 447,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo yg_emlrtRTEI = { 443,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ah_emlrtRTEI = { 448,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo bh_emlrtRTEI = { 444,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ch_emlrtRTEI = { 453,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo dh_emlrtRTEI = { 454,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo eh_emlrtRTEI = { 455,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo fh_emlrtRTEI = { 456,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo gh_emlrtRTEI = { 761,/* lineNo */
  19,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo hh_emlrtRTEI = { 761,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ih_emlrtRTEI = { 762,/* lineNo */
  22,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo jh_emlrtRTEI = { 762,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo kh_emlrtRTEI = { 763,/* lineNo */
  23,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo lh_emlrtRTEI = { 763,/* lineNo */
  49,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo mh_emlrtRTEI = { 763,/* lineNo */
  75,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo nh_emlrtRTEI = { 28,/* lineNo */
  9,                                   /* colNo */
  "colon",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pName */
};

static emlrtRTEInfo oh_emlrtRTEI = { 764,/* lineNo */
  26,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ph_emlrtRTEI = { 764,/* lineNo */
  52,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo qh_emlrtRTEI = { 764,/* lineNo */
  78,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo rh_emlrtRTEI = { 765,/* lineNo */
  26,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo sh_emlrtRTEI = { 765,/* lineNo */
  52,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo th_emlrtRTEI = { 765,/* lineNo */
  78,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo uh_emlrtRTEI = { 28,/* lineNo */
  5,                                   /* colNo */
  "sort",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sort.m"/* pName */
};

static emlrtRTEInfo vh_emlrtRTEI = { 771,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo wh_emlrtRTEI = { 771,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo xh_emlrtRTEI = { 808,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo yh_emlrtRTEI = { 809,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ai_emlrtRTEI = { 810,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo bi_emlrtRTEI = { 833,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ci_emlrtRTEI = { 823,/* lineNo */
  46,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo di_emlrtRTEI = { 782,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ei_emlrtRTEI = { 783,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo fi_emlrtRTEI = { 844,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo gi_emlrtRTEI = { 40,/* lineNo */
  9,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo hi_emlrtRTEI = { 787,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ii_emlrtRTEI = { 825,/* lineNo */
  32,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ji_emlrtRTEI = { 836,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ki_emlrtRTEI = { 825,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo li_emlrtRTEI = { 836,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo mi_emlrtRTEI = { 1084,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ni_emlrtRTEI = { 1141,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo oi_emlrtRTEI = { 790,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo pi_emlrtRTEI = { 1086,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo qi_emlrtRTEI = { 1142,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ri_emlrtRTEI = { 1087,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo si_emlrtRTEI = { 928,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ti_emlrtRTEI = { 792,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ui_emlrtRTEI = { 1143,/* lineNo */
  25,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo vi_emlrtRTEI = { 929,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo wi_emlrtRTEI = { 791,/* lineNo */
  18,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo xi_emlrtRTEI = { 1088,/* lineNo */
  25,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo yi_emlrtRTEI = { 782,/* lineNo */
  19,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo aj_emlrtRTEI = { 1150,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo bj_emlrtRTEI = { 930,/* lineNo */
  35,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo cj_emlrtRTEI = { 1135,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo dj_emlrtRTEI = { 1152,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ej_emlrtRTEI = { 1096,/* lineNo */
  17,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo fj_emlrtRTEI = { 931,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo gj_emlrtRTEI = { 1097,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo hj_emlrtRTEI = { 1154,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ij_emlrtRTEI = { 993,/* lineNo */
  13,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo jj_emlrtRTEI = { 955,/* lineNo */
  38,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo kj_emlrtRTEI = { 967,/* lineNo */
  38,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo lj_emlrtRTEI = { 685,/* lineNo */
  53,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo mj_emlrtRTEI = { 1062,/* lineNo */
  40,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo nj_emlrtRTEI = { 1117,/* lineNo */
  32,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo oj_emlrtRTEI = { 978,/* lineNo */
  25,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo pj_emlrtRTEI = { 1036,/* lineNo */
  40,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo qj_emlrtRTEI = { 1021,/* lineNo */
  40,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo rj_emlrtRTEI = { 273,/* lineNo */
  1,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo sj_emlrtRTEI = { 498,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo tj_emlrtRTEI = { 499,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo uj_emlrtRTEI = { 794,/* lineNo */
  9,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo vj_emlrtRTEI = { 832,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo wj_emlrtRTEI = { 425,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo xj_emlrtRTEI = { 373,/* lineNo */
  23,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo yj_emlrtRTEI = { 374,/* lineNo */
  22,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ak_emlrtRTEI = { 508,/* lineNo */
  39,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo bk_emlrtRTEI = { 823,/* lineNo */
  26,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ck_emlrtRTEI = { 31,/* lineNo */
  6,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo dk_emlrtRTEI = { 418,/* lineNo */
  20,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ek_emlrtRTEI = { 883,/* lineNo */
  25,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo fk_emlrtRTEI = { 367,/* lineNo */
  5,                                   /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo gk_emlrtRTEI = { 834,/* lineNo */
  24,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo hk_emlrtRTEI = { 823,/* lineNo */
  83,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo ik_emlrtRTEI = { 883,/* lineNo */
  38,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRTEInfo jk_emlrtRTEI = { 883,/* lineNo */
  84,                                  /* colNo */
  "DEIPFunc2",                         /* fName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pName */
};

static emlrtRSInfo wo_emlrtRSI = { 242,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo xo_emlrtRSI = { 251,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo yo_emlrtRSI = { 243,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo ap_emlrtRSI = { 66, /* lineNo */
  "fprintf",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\iofun\\fprintf.m"/* pathName */
};

static emlrtRSInfo bp_emlrtRSI = { 250,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo cp_emlrtRSI = { 257,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo dp_emlrtRSI = { 287,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo ep_emlrtRSI = { 281,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo fp_emlrtRSI = { 295,/* lineNo */
  "DEIPFunc2",                         /* fcnName */
  "C:\\Users\\ttruster\\_Prog_Repo\\DEIP\\DEIProgram_rel\\source\\projects\\DEIPFunc2.m"/* pathName */
};

static emlrtRSInfo gp_emlrtRSI = { 64, /* lineNo */
  "fprintf",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\iofun\\fprintf.m"/* pathName */
};

/* Function Declarations */
static const mxArray *b_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [33]);
static const mxArray *c_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [59]);
static const mxArray *c_feval(const emlrtStack *sp, const mxArray *b, const
  mxArray *c, const mxArray *d, const mxArray *e, emlrtMCInfo *location);
static const mxArray *d_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [103]);
static void disp(const emlrtStack *sp, const mxArray *b, emlrtMCInfo *location);
static const mxArray *e_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [72]);
static const mxArray *emlrt_marshallOut(const emlrtStack *sp, const char_T u[53]);
static const mxArray *g_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [47]);
static const mxArray *h_emlrt_marshallOut(const emlrtStack *sp, const char_T u[7]);
static const mxArray *i_emlrt_marshallOut(const int64_T u);
static const mxArray *j_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [71]);

/* Function Definitions */
static const mxArray *b_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [33])
{
  static const int32_T iv[2] = { 1, 33 };

  const mxArray *m;
  const mxArray *y;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 33, m, &u[0]);
  emlrtAssign(&y, m);
  return y;
}

static const mxArray *c_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [59])
{
  static const int32_T iv[2] = { 1, 59 };

  const mxArray *m;
  const mxArray *y;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 59, m, &u[0]);
  emlrtAssign(&y, m);
  return y;
}

static const mxArray *c_feval(const emlrtStack *sp, const mxArray *b, const
  mxArray *c, const mxArray *d, const mxArray *e, emlrtMCInfo *location)
{
  const mxArray *pArrays[4];
  const mxArray *m;
  pArrays[0] = b;
  pArrays[1] = c;
  pArrays[2] = d;
  pArrays[3] = e;
  return emlrtCallMATLABR2012b(sp, 1, &m, 4, pArrays, "feval", true, location);
}

static const mxArray *d_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [103])
{
  static const int32_T iv[2] = { 1, 103 };

  const mxArray *m;
  const mxArray *y;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 103, m, &u[0]);
  emlrtAssign(&y, m);
  return y;
}

static void disp(const emlrtStack *sp, const mxArray *b, emlrtMCInfo *location)
{
  const mxArray *pArray;
  pArray = b;
  emlrtCallMATLABR2012b(sp, 0, NULL, 1, &pArray, "disp", true, location);
}

static const mxArray *e_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [72])
{
  static const int32_T iv[2] = { 1, 72 };

  const mxArray *m;
  const mxArray *y;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 72, m, &u[0]);
  emlrtAssign(&y, m);
  return y;
}

static const mxArray *emlrt_marshallOut(const emlrtStack *sp, const char_T u[53])
{
  static const int32_T iv[2] = { 1, 53 };

  const mxArray *m;
  const mxArray *y;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 53, m, &u[0]);
  emlrtAssign(&y, m);
  return y;
}

static const mxArray *g_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [47])
{
  static const int32_T iv[2] = { 1, 47 };

  const mxArray *m;
  const mxArray *y;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 47, m, &u[0]);
  emlrtAssign(&y, m);
  return y;
}

static const mxArray *h_emlrt_marshallOut(const emlrtStack *sp, const char_T u[7])
{
  static const int32_T iv[2] = { 1, 7 };

  const mxArray *m;
  const mxArray *y;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 7, m, &u[0]);
  emlrtAssign(&y, m);
  return y;
}

static const mxArray *i_emlrt_marshallOut(const int64_T u)
{
  const mxArray *m;
  const mxArray *y;
  y = NULL;
  m = emlrtCreateNumericMatrix(1, 1, mxINT64_CLASS, mxREAL);
  *(int64_T *)emlrtMxGetData(m) = u;
  emlrtAssign(&y, m);
  return y;
}

static const mxArray *j_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [71])
{
  static const int32_T iv[2] = { 1, 71 };

  const mxArray *m;
  const mxArray *y;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 71, m, &u[0]);
  emlrtAssign(&y, m);
  return y;
}

void DEIPFunc2(const emlrtStack *sp, emxArray_real_T *InterTypes,
               emxArray_real_T *NodesOnElement, const emxArray_real_T
               *RegionOnElement, emxArray_real_T *Coordinates, real_T *numnp,
               real_T numel, real_T nummat, real_T nen, real_T ndm, real_T
               usePBC, const real_T *numMPC, const emxArray_real_T *MPCList,
               real_T *numEonB, emxArray_real_T *numEonF, emxArray_real_T
               *ElementsOnBoundary, real_T *numSI, emxArray_real_T
               *ElementsOnFacet, e_sparse *ElementsOnNode, e_sparse
               *ElementsOnNodeDup, emxArray_real_T *ElementsOnNodeNum, real_T
               *numfac, emxArray_real_T *ElementsOnNodeNum2, real_T *numinttype,
               emxArray_real_T *FacetsOnElement, emxArray_real_T
               *FacetsOnElementInt, emxArray_real_T *FacetsOnInterface,
               emxArray_real_T *FacetsOnInterfaceNum, e_sparse *FacetsOnNode,
               e_sparse *FacetsOnNodeCut, e_sparse *FacetsOnNodeInt,
               emxArray_real_T *FacetsOnNodeNum, emxArray_real_T *NodeCGDG,
               e_sparse *NodeReg, emxArray_real_T *NodesOnElementCG,
               emxArray_real_T *NodesOnElementDG, emxArray_real_T
               *NodesOnInterface, real_T *NodesOnInterfaceNum, real_T *numCL,
               emxArray_real_T *NodesOnPBC, emxArray_real_T *NodesOnPBCnum,
               emxArray_real_T *NodesOnLink, emxArray_real_T *NodesOnLinknum,
               emxArray_real_T *numEonPBC, emxArray_real_T *FacetsOnPBC,
               emxArray_real_T *FacetsOnPBCNum, emxArray_real_T
               *FacetsOnIntMinusPBC, emxArray_real_T *FacetsOnIntMinusPBCNum)
{
  static const char_T cv3[103] = { 'E', 'r', 'r', 'o', 'r', ',', ' ', 'c', 'o',
    'n', 'n', 'e', 'c', 't', 'i', 'v', 'i', 't', 'y', ':', ' ', 's', 'i', 'z',
    'e', ' ', 'o', 'f', ' ', 'N', 'o', 'd', 'e', 's', 'O', 'n', 'E', 'l', 'e',
    'm', 'e', 'n', 't', ' ', 'd', 'o', 'e', 's', ' ', 'n', 'o', 't', ' ', 'm',
    'a', 't', 'c', 'h', ' ', 'p', 'a', 'r', 'a', 'm', 'e', 't', 'e', 'r', ' ',
    'n', 'e', 'n', ':', ' ', 's', 'i', 'z', 'e', '(', 'N', 'o', 'd', 'e', 's',
    'O', 'n', 'E', 'l', 'e', 'm', 'e', 'n', 't', ',', '2', ')', ' ', '~', '=',
    ' ', 'n', 'e', 'n' };

  static const char_T cv7[72] = { 'W', 'a', 'r', 'n', 'i', 'n', 'g', ':', ' ',
    's', 'o', 'm', 'e', ' ', 'i', 'n', 't', 'e', 'r', 'f', 'a', 'c', 'e', 's',
    ' ', 'n', 'o', 't', ' ', 'r', 'e', 'q', 'u', 'e', 's', 't', 'e', 'd', ' ',
    'i', 'n', ' ', 'I', 'n', 't', 'e', 'r', 't', 'y', 'p', 'e', 's', ' ', 'a',
    'n', 'd', ' ', 'h', 'a', 'v', 'e', ' ', 'b', 'e', 'e', 'n', ' ', 'a', 'd',
    'd', 'e', 'd' };

  static const char_T cv5[71] = { 'W', 'a', 'r', 'n', 'i', 'n', 'g', ':', ' ',
    's', 'o', 'm', 'e', ' ', 'e', 'n', 't', 'r', 'i', 'e', 's', ' ', 'f', 'o',
    'u', 'n', 'd', ' ', 'i', 'n', ' ', 't', 'h', 'e', ' ', 'u', 'p', 'p', 'e',
    'r', ' ', 't', 'r', 'i', 'a', 'n', 'g', 'l', 'e', ' ', 'o', 'f', ' ', 'I',
    'n', 't', 'e', 'r', 't', 'y', 'p', 'e', 's', '(', ':', ',', '%', 'i', ')',
    '\\', 'n' };

  static const char_T cv2[59] = { 'W', 'a', 'r', 'n', 'i', 'n', 'g', ',', ' ',
    'n', 'u', 'm', 'b', 'e', 'r', ' ', 'o', 'f', ' ', 'e', 'l', 'e', 'm', 'e',
    'n', 't', 's', ':', ' ', 'n', 'u', 'm', 'e', 'l', ' ', '<', ' ', 's', 'i',
    'z', 'e', '(', 'N', 'o', 'd', 'e', 's', 'O', 'n', 'E', 'l', 'e', 'm', 'e',
    'n', 't', ',', '1', ')' };

  static const char_T cv8[59] = { 'i', 'n', 't', 'r', 'a', 'f', 'a', 'c', 'e',
    ':', ' ', '%', 'i', ';', ' ', 'a', 'd', 'd', 'i', 't', 'i', 'o', 'n', 'a',
    'l', ' ', 'i', 'n', 't', 'e', 'r', 'f', 'a', 'c', 'e', ' ', 'f', 'l', 'a',
    'g', 's', ' ', 'h', 'a', 'v', 'e', ' ', 'b', 'e', 'e', 'n', ' ', 'a', 'd',
    'd', 'e', 'd', '\\', 'n' };

  static const char_T cv[53] = { 'W', 'a', 'r', 'n', 'i', 'n', 'g', ',', ' ',
    'n', 'u', 'm', 'b', 'e', 'r', ' ', 'o', 'f', ' ', 'n', 'o', 'd', 'e', 's',
    ':', ' ', 'n', 'u', 'm', 'n', 'p', ' ', '<', ' ', 's', 'i', 'z', 'e', '(',
    'C', 'o', 'o', 'r', 'd', 'i', 'n', 'a', 't', 'e', 's', ',', '1', ')' };

  static const char_T cv6[47] = { 'T', 'h', 'e', 's', 'e', ' ', 'e', 'n', 't',
    'r', 'i', 'e', 's', ' ', 'a', 'r', 'e', ' ', 'i', 'g', 'n', 'o', 'r', 'e',
    'd', ' ', 'f', 'o', 'r', ' ', 'c', 'o', 'u', 'p', 'l', 'e', 'r', ' ', 'i',
    'n', 's', 'e', 'r', 't', 'i', 'o', 'n' };

  static const char_T cv1[33] = { 'P', 'a', 'u', 's', 'e', 'd', ':', ' ', 'p',
    'r', 'e', 's', 's', ' ', 'a', 'n', 'y', ' ', 'k', 'e', 'y', ' ', 't', 'o',
    ' ', 'c', 'o', 'n', 't', 'i', 'n', 'u', 'e' };

  static const char_T cv4[7] = { 'f', 'p', 'r', 'i', 'n', 't', 'f' };

  e_sparse expl_temp;
  e_sparse kp_emlrtRSI;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack f_st;
  emlrtStack g_st;
  emlrtStack h_st;
  emlrtStack i_st;
  emlrtStack j_st;
  emlrtStack st;
  emxArray_boolean_T NodesOnPBC_data;
  emxArray_boolean_T *b_NodesOnPBCnum;
  emxArray_boolean_T *r3;
  emxArray_int32_T *b_ii;
  emxArray_int32_T *c_ii;
  emxArray_int32_T *ib;
  emxArray_int32_T *ii;
  emxArray_int32_T *indx;
  emxArray_int32_T *r;
  emxArray_int32_T *r1;
  emxArray_int32_T *r2;
  emxArray_int32_T *r4;
  emxArray_int32_T *r5;
  emxArray_int64_T *inter;
  emxArray_real_T b_nodes_data;
  emxArray_real_T c_nodes_data;
  emxArray_real_T *Coordinates3;
  emxArray_real_T *ElementsOnNodeA;
  emxArray_real_T *ElementsOnNodeB;
  emxArray_real_T *ElementsOnNodePBCNum;
  emxArray_real_T *ElementsOnNodePBCRow;
  emxArray_real_T *ElementsOnNodeRow;
  emxArray_real_T *FacetsOnNodeInd;
  emxArray_real_T *MorePBC;
  emxArray_real_T *NodeRegInd;
  emxArray_real_T *NodesOnElementPBC;
  emxArray_real_T *b;
  emxArray_real_T *b_ElementsOnFacet;
  emxArray_real_T *b_MPCList;
  emxArray_real_T *b_MorePBC;
  emxArray_real_T *b_NodeRegInd;
  emxArray_real_T *c_MorePBC;
  emxArray_real_T *c_NodeRegInd;
  emxArray_real_T *interreg2;
  emxArray_real_T *setAll;
  emxArray_real_T *setPBC;
  emxArray_real_T *setnPBC;
  f_sparse NodeRegSum;
  f_sparse b_expl_temp;
  g_sparse c_expl_temp;
  g_sparse d_expl_temp;
  g_sparse e_expl_temp;
  h_sparse f_expl_temp;
  h_sparse g_expl_temp;
  h_sparse h_expl_temp;
  int64_T b_qY;
  int64_T i4;
  int64_T i5;
  int64_T mat;
  int64_T q0;
  int64_T qY;
  real_T tmp_data[400];
  real_T nodeC2_data[27];
  real_T nodeD2_data[27];
  real_T links[20];
  real_T links2_data[20];
  real_T nodes[20];
  real_T nodes_data[20];
  real_T MorePBC_data[4];
  real_T nodeloc_data[4];
  real_T b_i1;
  real_T b_i2;
  real_T d;
  real_T d1;
  real_T elemA;
  real_T node;
  real_T nodeA;
  real_T nodeB;
  real_T numA;
  real_T numFPBC;
  real_T numSI_tmp;
  real_T regI;
  int32_T b_ii_data[27];
  int32_T ii_data[27];
  int32_T ia_data[20];
  int32_T c_tmp_data[4];
  int32_T b_nodeC2_size[2];
  int32_T b_numel[2];
  int32_T ii_size[2];
  int32_T nodeC2_size[2];
  int32_T nodeD2_size[2];
  int32_T tmp_size[2];
  int32_T NodesOnPBC_size[1];
  int32_T b_NodesOnElement[1];
  int32_T ia_size[1];
  int32_T ib_size[1];
  int32_T links2_size[1];
  int32_T nodeloc_size[1];
  int32_T nodes_size[1];
  int32_T ElementsOnNodePBC_n;
  int32_T b_NodesOnElementPBC;
  int32_T b_i;
  int32_T b_loop_ub;
  int32_T c_loop_ub;
  int32_T d_loop_ub;
  int32_T elem;
  int32_T elemB;
  int32_T end;
  int32_T exitg1;
  int32_T facI1;
  int32_T i;
  int32_T i1;
  int32_T i2;
  int32_T i3;
  int32_T i6;
  int32_T locF;
  int32_T locN;
  int32_T loop_ub;
  int32_T nel;
  int32_T nelA;
  int32_T notdone;
  int32_T numPBC;
  int32_T nume;
  int32_T trueCount;
  uint32_T iSec2;
  uint32_T nri;
  int8_T b_tmp_data[27];
  int8_T nloop4[8];
  int8_T nloop3[6];
  boolean_T b_nodeC2_data[27];
  boolean_T b_NodesOnPBC_data[4];
  boolean_T guard1 = false;
  boolean_T guard2 = false;
  boolean_T old;
  (void)ndm;
  (void)numMPC;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  f_st.prev = &e_st;
  f_st.tls = e_st.tls;
  g_st.prev = &f_st;
  g_st.tls = f_st.tls;
  h_st.prev = &g_st;
  h_st.tls = g_st.tls;
  i_st.prev = &h_st;
  i_st.tls = h_st.tls;
  j_st.prev = &b_st;
  j_st.tls = b_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);

  /*  Program: DEIProgram2 */
  /*  Tim Truster */
  /*  08/15/2014 */
  /*  */
  /*  Script to convert CG mesh (e.g. from block program output) into DG mesh */
  /*  Addition of modules to insert interfaces selectively between materials */
  /*   */
  /*  Input: matrix InterTypes(nummat,nummat) for listing the interface */
  /*  material types to assign; only lower triangle is accessed; value 0 means */
  /*  interface is not inserted between those materials (including diagonal). */
  /*  */
  /*  Numbering of interface types (matI) is like: */
  /*  1 */
  /*  2 3 */
  /*  4 5 6 ... */
  /*  */
  /*  Output: */
  /*  Actual list of used interfaces is SurfacesI and numCL, new BC arrays and */
  /*  load arrays */
  /*  Total list of interfaces is in numIfac and Interfac, although there is */
  /*  no designation on whether the interfaces are active or not. These are */
  /*  useful for assigning different DG element types to different interfaces. */
  /*  Revisions: */
  /*  02/10/2017: adding periodic boundary conditions (PBC); uses both zipped mesh */
  /*  (collapsing linked PBC nodes into a single node and updating */
  /*  connectivity), and ghost elements (leaving original mesh, added elements */
  /*  along PBC edges that connect across the domain); both result in a 'torus' */
  /*  mesh. */
  /*  Revisions: */
  /*  04/04/2017: adding periodic BC in the form of multi-point constraints, */
  /*  which do not require the domain to be a cube. Still uses the zipped mesh, */
  /*  but does not add ghost elements. Instead, node duplication is performed */
  /*  directly on the zipped mesh, and MPC elements are applied outside of this */
  /*  subroutine. */
  /*  % flags for clearing out intermediate variables in this script; turned on */
  /*  % by default */
  /*  currvariables = who(); */
  /*  clearinter = 1; */
  numSI_tmp = 0.0;

  /*  % Test for Octave vs Matlab compatibility */
  /*  isOctave = exist('OCTAVE_VERSION', 'builtin') ~= 0; */
  /*  ElementsOnNode = zeros(maxel,numnp); % Array of elements connected to nodes */
  /*  ElementsOnNodeDup = zeros(maxel,numnp); % actual node ID given to element for an original node; general counterpart of NodeMat */
  if (!(*numnp >= 0.0)) {
    emlrtNonNegativeCheckR2012b(*numnp, &dc_emlrtDCI, sp);
  }

  d = *numnp;
  if (d != (int32_T)muDoubleScalarFloor(d)) {
    emlrtIntegerCheckR2012b(d, &cc_emlrtDCI, sp);
  }

  i = ElementsOnNodeNum->size[0];
  ElementsOnNodeNum->size[0] = (int32_T)d;
  emxEnsureCapacity_real_T(sp, ElementsOnNodeNum, i, &ye_emlrtRTEI);
  if (!(*numnp >= 0.0)) {
    emlrtNonNegativeCheckR2012b(*numnp, &dc_emlrtDCI, sp);
  }

  d = *numnp;
  if (d != (int32_T)muDoubleScalarFloor(d)) {
    emlrtIntegerCheckR2012b(d, &cc_emlrtDCI, sp);
  }

  loop_ub = (int32_T)d;
  for (i = 0; i < loop_ub; i++) {
    ElementsOnNodeNum->data[i] = 0.0;
  }

  /*  if ~exist('usePBC','var') */
  /*      usePBC = 0; */
  /*  end */
  st.site = &ve_emlrtRSI;
  if (muDoubleScalarIsNaN(usePBC)) {
    emlrtErrorWithMessageIdR2018a(&st, &mb_emlrtRTEI, "MATLAB:nologicalnan",
      "MATLAB:nologicalnan", 0);
  }

  emxInit_real_T(sp, &ElementsOnNodePBCNum, 1, &bf_emlrtRTEI, true);
  if (usePBC != 0.0) {
    /*  create extra arrays for zipped mesh */
    /*  ElementsOnNodePBC = zeros(maxel,numnp); % Array of elements connected to nodes */
    /*  ElementsOnNodePBCDup = zeros(maxel,numnp); % actual node ID given to element for an original node; general counterpart of NodeMat */
    i = (int32_T)muDoubleScalarFloor(*numnp);
    if (*numnp != i) {
      emlrtIntegerCheckR2012b(*numnp, &ec_emlrtDCI, sp);
    }

    i1 = ElementsOnNodePBCNum->size[0];
    ElementsOnNodePBCNum->size[0] = (int32_T)*numnp;
    emxEnsureCapacity_real_T(sp, ElementsOnNodePBCNum, i1, &bf_emlrtRTEI);
    if (*numnp != i) {
      emlrtIntegerCheckR2012b(*numnp, &ec_emlrtDCI, sp);
    }

    loop_ub = (int32_T)*numnp;
    for (i = 0; i < loop_ub; i++) {
      ElementsOnNodePBCNum->data[i] = 0.0;
    }
  } else {
    i = ElementsOnNodePBCNum->size[0];
    ElementsOnNodePBCNum->size[0] = 1;
    emxEnsureCapacity_real_T(sp, ElementsOnNodePBCNum, i, &af_emlrtRTEI);
    ElementsOnNodePBCNum->data[0] = 0.0;
  }

  emxInit_real_T(sp, &Coordinates3, 2, &cf_emlrtRTEI, true);

  /*  NodeReg = zeros(numnp,nummat); */
  d = numel * nen;
  if (!(d >= 0.0)) {
    emlrtNonNegativeCheckR2012b(d, &hb_emlrtDCI, sp);
  }

  d1 = (int32_T)muDoubleScalarFloor(d);
  if (d != d1) {
    emlrtIntegerCheckR2012b(d, &gb_emlrtDCI, sp);
  }

  i = Coordinates3->size[0] * Coordinates3->size[1];
  Coordinates3->size[0] = (int32_T)d;
  Coordinates3->size[1] = 2;
  emxEnsureCapacity_real_T(sp, Coordinates3, i, &cf_emlrtRTEI);
  if (d != d1) {
    emlrtIntegerCheckR2012b(d, &fc_emlrtDCI, sp);
  }

  loop_ub = (int32_T)d << 1;
  for (i = 0; i < loop_ub; i++) {
    Coordinates3->data[i] = 0.0;
  }

  i = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
  NodesOnElementCG->size[0] = NodesOnElement->size[0];
  NodesOnElementCG->size[1] = NodesOnElement->size[1];
  emxEnsureCapacity_real_T(sp, NodesOnElementCG, i, &df_emlrtRTEI);
  loop_ub = NodesOnElement->size[0] * NodesOnElement->size[1];
  for (i = 0; i < loop_ub; i++) {
    NodesOnElementCG->data[i] = NodesOnElement->data[i];
  }

  st.site = &ue_emlrtRSI;
  if (muDoubleScalarIsNaN(usePBC)) {
    emlrtErrorWithMessageIdR2018a(&st, &mb_emlrtRTEI, "MATLAB:nologicalnan",
      "MATLAB:nologicalnan", 0);
  }

  emxInit_real_T(sp, &NodesOnElementPBC, 2, &ff_emlrtRTEI, true);
  if (usePBC != 0.0) {
    /*  Add space in connectivity for ghost elements */
    /*      if nen == 3 */
    /*          NodesOnElementCG = [NodesOnElementCG zeros(numel,4-nen) */
    /*                              zeros(numPBC*2,4)]; */
    /*          nen = 4; */
    /*      elseif nen == 6 */
    /*          NodesOnElementCG = [NodesOnElementCG zeros(numel,9-nen) */
    /*                              zeros(numPBC*2,9)]; */
    /*          nen = 9; */
    /*      end */
    i = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1];
    NodesOnElementPBC->size[0] = NodesOnElement->size[0];
    NodesOnElementPBC->size[1] = NodesOnElement->size[1];
    emxEnsureCapacity_real_T(sp, NodesOnElementPBC, i, &ff_emlrtRTEI);
    loop_ub = NodesOnElement->size[0] * NodesOnElement->size[1];
    for (i = 0; i < loop_ub; i++) {
      NodesOnElementPBC->data[i] = NodesOnElement->data[i];
    }

    /*  Connectivity that is zipped, having PBC nodes condensed together */
    /*      RegionOnElement = [RegionOnElement; zeros(numPBC*2,1)]; */
  } else {
    i = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1];
    NodesOnElementPBC->size[0] = 1;
    NodesOnElementPBC->size[1] = 1;
    emxEnsureCapacity_real_T(sp, NodesOnElementPBC, i, &ef_emlrtRTEI);
    NodesOnElementPBC->data[0] = 0.0;

    /*  RegionOnElement = RegionOnElement; */
  }

  /*  Set up PBC data structures */
  st.site = &te_emlrtRSI;
  if (muDoubleScalarIsNaN(usePBC)) {
    emlrtErrorWithMessageIdR2018a(&st, &mb_emlrtRTEI, "MATLAB:nologicalnan",
      "MATLAB:nologicalnan", 0);
  }

  emxInit_real_T(sp, &MorePBC, 1, &nf_emlrtRTEI, true);
  emxInit_real_T(sp, &setnPBC, 1, &uj_emlrtRTEI, true);
  emxInit_int32_T(sp, &indx, 1, &bk_emlrtRTEI, true);
  emxInit_int32_T(sp, &ii, 1, &ck_emlrtRTEI, true);
  emxInit_real_T(sp, &b_MPCList, 2, &kf_emlrtRTEI, true);
  emxInit_boolean_T(sp, &b_NodesOnPBCnum, 1, &lf_emlrtRTEI, true);
  if (usePBC != 0.0) {
    /*  Lists of node pairs in convenient table format */
    i = NodesOnPBC->size[0] * NodesOnPBC->size[1];
    NodesOnPBC->size[0] = 4;
    emxEnsureCapacity_real_T(sp, NodesOnPBC, i, &gf_emlrtRTEI);
    i = (int32_T)muDoubleScalarFloor(*numnp);
    if (*numnp != i) {
      emlrtIntegerCheckR2012b(*numnp, &ib_emlrtDCI, sp);
    }

    i1 = NodesOnPBC->size[0] * NodesOnPBC->size[1];
    NodesOnPBC->size[1] = (int32_T)*numnp;
    emxEnsureCapacity_real_T(sp, NodesOnPBC, i1, &gf_emlrtRTEI);
    if (*numnp != i) {
      emlrtIntegerCheckR2012b(*numnp, &gc_emlrtDCI, sp);
    }

    notdone = (int32_T)*numnp << 2;
    for (i1 = 0; i1 < notdone; i1++) {
      NodesOnPBC->data[i1] = 0.0;
    }

    /*  nodes that are paired up */
    if (*numnp != i) {
      emlrtIntegerCheckR2012b(*numnp, &hc_emlrtDCI, sp);
    }

    i1 = NodesOnPBCnum->size[0];
    NodesOnPBCnum->size[0] = (int32_T)*numnp;
    emxEnsureCapacity_real_T(sp, NodesOnPBCnum, i1, &hf_emlrtRTEI);
    if (*numnp != i) {
      emlrtIntegerCheckR2012b(*numnp, &hc_emlrtDCI, sp);
    }

    loop_ub = (int32_T)*numnp;
    for (i1 = 0; i1 < loop_ub; i1++) {
      NodesOnPBCnum->data[i1] = 0.0;
    }

    /*  number of nodes with common pairing */
    i1 = NodesOnLink->size[0] * NodesOnLink->size[1];
    NodesOnLink->size[0] = 4;
    emxEnsureCapacity_real_T(sp, NodesOnLink, i1, &if_emlrtRTEI);
    if (*numnp != i) {
      emlrtIntegerCheckR2012b(*numnp, &jb_emlrtDCI, sp);
    }

    i1 = NodesOnLink->size[0] * NodesOnLink->size[1];
    NodesOnLink->size[1] = (int32_T)*numnp;
    emxEnsureCapacity_real_T(sp, NodesOnLink, i1, &if_emlrtRTEI);
    if (*numnp != i) {
      emlrtIntegerCheckR2012b(*numnp, &ic_emlrtDCI, sp);
    }

    for (i1 = 0; i1 < notdone; i1++) {
      NodesOnLink->data[i1] = 0.0;
    }

    /*  IDs of PBC referring to node */
    if (*numnp != i) {
      emlrtIntegerCheckR2012b(*numnp, &jc_emlrtDCI, sp);
    }

    i1 = NodesOnLinknum->size[0];
    NodesOnLinknum->size[0] = (int32_T)*numnp;
    emxEnsureCapacity_real_T(sp, NodesOnLinknum, i1, &jf_emlrtRTEI);
    if (*numnp != i) {
      emlrtIntegerCheckR2012b(*numnp, &jc_emlrtDCI, sp);
    }

    loop_ub = (int32_T)*numnp;
    for (i = 0; i < loop_ub; i++) {
      NodesOnLinknum->data[i] = 0.0;
    }

    /*  number of PBC referring to node */
    /*  Handle the corners first */
    if (usePBC == 2.0) {
      /*      PBCList = MPCList; */
      numPBC = MPCList->size[0];
    } else {
      /*      PBCList = MPCList; */
      numPBC = MPCList->size[0];
      st.site = &se_emlrtRSI;
      loop_ub = MPCList->size[0];
      i = b_MPCList->size[0] * b_MPCList->size[1];
      b_MPCList->size[0] = loop_ub;
      b_MPCList->size[1] = 2;
      emxEnsureCapacity_real_T(&st, b_MPCList, i, &kf_emlrtRTEI);
      for (i = 0; i < loop_ub; i++) {
        b_MPCList->data[i] = MPCList->data[i + MPCList->size[0] * 2];
      }

      for (i = 0; i < loop_ub; i++) {
        b_MPCList->data[i + b_MPCList->size[0]] = MPCList->data[i +
          MPCList->size[0] * 3];
      }

      emxInit_real_T(&st, &b, 2, &sf_emlrtRTEI, true);
      b_st.site = &we_emlrtRSI;
      unique_rows(&b_st, b_MPCList, b, indx);
      i = MorePBC->size[0];
      MorePBC->size[0] = indx->size[0];
      emxEnsureCapacity_real_T(&st, MorePBC, i, &mf_emlrtRTEI);
      loop_ub = indx->size[0];
      for (i = 0; i < loop_ub; i++) {
        MorePBC->data[i] = indx->data[i];
      }

      if (b->size[0] != 2) {
        st.site = &re_emlrtRSI;
        error(&st);
      } else {
        d = b->data[0];
        d1 = b->data[b->size[0]];
        i = (int32_T)muDoubleScalarFloor(d);
        if (d != i) {
          emlrtIntegerCheckR2012b(d, &ie_emlrtDCI, sp);
        }

        i1 = (int32_T)d;
        if ((d < 1.0) || (i1 > NodesOnPBC->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnPBC->size[1],
            &ep_emlrtBCI, sp);
        }

        i2 = 4 * (i1 - 1);
        NodesOnPBC->data[i2] = d1;
        if (i1 != i) {
          emlrtIntegerCheckR2012b(d, &je_emlrtDCI, sp);
        }

        if ((d < 1.0) || (i1 > NodesOnPBCnum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnPBCnum->size[0],
            &fp_emlrtBCI, sp);
        }

        NodesOnPBCnum->data[(int32_T)d - 1] = 1.0;
        i3 = (int32_T)muDoubleScalarFloor(d1);
        if (d1 != i3) {
          emlrtIntegerCheckR2012b(d1, &ke_emlrtDCI, sp);
        }

        b_i = (int32_T)d1;
        if ((d1 < 1.0) || (b_i > NodesOnPBC->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, NodesOnPBC->size[1],
            &gp_emlrtBCI, sp);
        }

        end = 4 * (b_i - 1);
        NodesOnPBC->data[end] = d;
        if (b_i != i3) {
          emlrtIntegerCheckR2012b(d1, &le_emlrtDCI, sp);
        }

        if ((d1 < 1.0) || (b_i > NodesOnPBCnum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, NodesOnPBCnum->size[0],
            &hp_emlrtBCI, sp);
        }

        NodesOnPBCnum->data[(int32_T)d1 - 1] = 1.0;
        if (i1 != i) {
          emlrtIntegerCheckR2012b(d, &me_emlrtDCI, sp);
        }

        if ((d < 1.0) || (i1 > NodesOnLink->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLink->size[1],
            &ip_emlrtBCI, sp);
        }

        if (1 > MorePBC->size[0]) {
          emlrtDynamicBoundsCheckR2012b(1, 1, MorePBC->size[0], &jp_emlrtBCI, sp);
        }

        NodesOnLink->data[i2] = -MorePBC->data[0];
        if (i1 != i) {
          emlrtIntegerCheckR2012b(d, &ne_emlrtDCI, sp);
        }

        if ((d < 1.0) || (i1 > NodesOnLinknum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLinknum->size[0],
            &kp_emlrtBCI, sp);
        }

        NodesOnLinknum->data[(int32_T)d - 1] = 1.0;
        if (b_i != i3) {
          emlrtIntegerCheckR2012b(d1, &oe_emlrtDCI, sp);
        }

        if ((d1 < 1.0) || (b_i > NodesOnLink->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, NodesOnLink->size[1],
            &lp_emlrtBCI, sp);
        }

        if (1 > MorePBC->size[0]) {
          emlrtDynamicBoundsCheckR2012b(1, 1, MorePBC->size[0], &mp_emlrtBCI, sp);
        }

        NodesOnLink->data[end] = -MorePBC->data[0];
        if (b_i != i3) {
          emlrtIntegerCheckR2012b(d1, &pe_emlrtDCI, sp);
        }

        if ((d1 < 1.0) || (b_i > NodesOnLinknum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, NodesOnLinknum->size[0],
            &np_emlrtBCI, sp);
        }

        NodesOnLinknum->data[(int32_T)d1 - 1] = 1.0;
        if (*emlrtBreakCheckR2012bFlagVar != 0) {
          emlrtBreakCheckR2012b(sp);
        }

        d = b->data[1];
        d1 = b->data[b->size[0] + 1];
        i = (int32_T)muDoubleScalarFloor(d);
        if (d != i) {
          emlrtIntegerCheckR2012b(d, &ie_emlrtDCI, sp);
        }

        i1 = (int32_T)d;
        if ((d < 1.0) || (i1 > NodesOnPBC->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnPBC->size[1],
            &ep_emlrtBCI, sp);
        }

        i2 = 4 * (i1 - 1);
        NodesOnPBC->data[i2] = d1;
        if (i1 != i) {
          emlrtIntegerCheckR2012b(d, &je_emlrtDCI, sp);
        }

        if ((d < 1.0) || (i1 > NodesOnPBCnum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnPBCnum->size[0],
            &fp_emlrtBCI, sp);
        }

        NodesOnPBCnum->data[(int32_T)d - 1] = 1.0;
        i3 = (int32_T)muDoubleScalarFloor(d1);
        if (d1 != i3) {
          emlrtIntegerCheckR2012b(d1, &ke_emlrtDCI, sp);
        }

        b_i = (int32_T)d1;
        if ((d1 < 1.0) || (b_i > NodesOnPBC->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, NodesOnPBC->size[1],
            &gp_emlrtBCI, sp);
        }

        end = 4 * (b_i - 1) + 1;
        NodesOnPBC->data[end] = d;
        if (b_i != i3) {
          emlrtIntegerCheckR2012b(d1, &le_emlrtDCI, sp);
        }

        if ((d1 < 1.0) || (b_i > NodesOnPBCnum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, NodesOnPBCnum->size[0],
            &hp_emlrtBCI, sp);
        }

        NodesOnPBCnum->data[(int32_T)d1 - 1] = 2.0;
        if (i1 != i) {
          emlrtIntegerCheckR2012b(d, &me_emlrtDCI, sp);
        }

        if ((d < 1.0) || (i1 > NodesOnLink->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLink->size[1],
            &ip_emlrtBCI, sp);
        }

        if (2 > MorePBC->size[0]) {
          emlrtDynamicBoundsCheckR2012b(2, 1, MorePBC->size[0], &jp_emlrtBCI, sp);
        }

        NodesOnLink->data[i2] = -MorePBC->data[1];
        if (i1 != i) {
          emlrtIntegerCheckR2012b(d, &ne_emlrtDCI, sp);
        }

        if ((d < 1.0) || (i1 > NodesOnLinknum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLinknum->size[0],
            &kp_emlrtBCI, sp);
        }

        NodesOnLinknum->data[(int32_T)d - 1] = 1.0;
        if (b_i != i3) {
          emlrtIntegerCheckR2012b(d1, &oe_emlrtDCI, sp);
        }

        if ((d1 < 1.0) || (b_i > NodesOnLink->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, NodesOnLink->size[1],
            &lp_emlrtBCI, sp);
        }

        if (2 > MorePBC->size[0]) {
          emlrtDynamicBoundsCheckR2012b(2, 1, MorePBC->size[0], &mp_emlrtBCI, sp);
        }

        NodesOnLink->data[end] = -MorePBC->data[1];
        if (b_i != i3) {
          emlrtIntegerCheckR2012b(d1, &pe_emlrtDCI, sp);
        }

        if ((d1 < 1.0) || (b_i > NodesOnLinknum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, NodesOnLinknum->size[0],
            &np_emlrtBCI, sp);
        }

        NodesOnLinknum->data[(int32_T)d1 - 1] = 2.0;
        if (*emlrtBreakCheckR2012bFlagVar != 0) {
          emlrtBreakCheckR2012b(sp);
        }
      }

      emxFree_real_T(&b);
    }

    /*  Now do the rest of the nodes */
    for (nelA = 0; nelA < numPBC; nelA++) {
      i = MPCList->size[0];
      i1 = nelA + 1;
      if ((i1 < 1) || (i1 > i)) {
        emlrtDynamicBoundsCheckR2012b(i1, 1, i, &emlrtBCI, sp);
      }

      i = (int32_T)muDoubleScalarFloor(MPCList->data[nelA]);
      if (MPCList->data[nelA] != i) {
        emlrtIntegerCheckR2012b(MPCList->data[nelA], &kc_emlrtDCI, sp);
      }

      notdone = (int32_T)MPCList->data[nelA];
      if ((notdone < 1) || (notdone > NodesOnPBCnum->size[0])) {
        emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnPBCnum->size[0],
          &wi_emlrtBCI, sp);
      }

      numA = NodesOnPBCnum->data[notdone - 1];
      d = NodesOnPBCnum->data[(int32_T)MPCList->data[nelA] - 1];
      if (1.0 > d) {
        loop_ub = 0;
      } else {
        if (((int32_T)d < 1) || ((int32_T)d > 4)) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, 4, &b_emlrtBCI, sp);
        }

        loop_ub = (int32_T)d;
      }

      if (MPCList->data[nelA] != i) {
        emlrtIntegerCheckR2012b(MPCList->data[nelA], &d_emlrtDCI, sp);
      }

      if (notdone > NodesOnPBC->size[1]) {
        emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnPBC->size[1],
          &c_emlrtBCI, sp);
      }

      /*  make sure pair isn't already in the list for nodeA */
      /*      if isOctave */
      st.site = &qe_emlrtRSI;
      nodeloc_size[0] = loop_ub;
      for (i1 = 0; i1 < loop_ub; i1++) {
        nodeloc_data[i1] = NodesOnPBC->data[i1 + 4 * (notdone - 1)];
      }

      b_st.site = &nf_emlrtRSI;
      old = local_ismember(MPCList->data[nelA + MPCList->size[0]], nodeloc_data,
                           nodeloc_size);

      /*      else */
      /*      old = ismembc(nodesAB(2),currPBC); */
      /*      end */
      if (!old) {
        /*  add the pair to nodeA, expand by one */
        if (((int32_T)(d + 1.0) < 1) || ((int32_T)(d + 1.0) > 4)) {
          emlrtDynamicBoundsCheckR2012b((int32_T)(d + 1.0), 1, 4, &aj_emlrtBCI,
            sp);
        }

        if (MPCList->data[nelA] != i) {
          emlrtIntegerCheckR2012b(MPCList->data[nelA], &lc_emlrtDCI, sp);
        }

        if (notdone > NodesOnPBC->size[1]) {
          emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnPBC->size[1],
            &bj_emlrtBCI, sp);
        }

        i1 = 4 * (notdone - 1);
        NodesOnPBC->data[((int32_T)(d + 1.0) + i1) - 1] = MPCList->data[nelA +
          MPCList->size[0]];
        if (MPCList->data[nelA] != i) {
          emlrtIntegerCheckR2012b(MPCList->data[nelA], &mc_emlrtDCI, sp);
        }

        if (notdone > NodesOnPBCnum->size[0]) {
          emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnPBCnum->size[0],
            &cj_emlrtBCI, sp);
        }

        NodesOnPBCnum->data[notdone - 1] = NodesOnPBCnum->data[(int32_T)
          MPCList->data[nelA] - 1] + 1.0;

        /*  Record which PBC ID refers to these nodes */
        if (MPCList->data[nelA] != i) {
          emlrtIntegerCheckR2012b(MPCList->data[nelA], &nc_emlrtDCI, sp);
        }

        if (notdone > NodesOnLinknum->size[0]) {
          emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnLinknum->size[0],
            &dj_emlrtBCI, sp);
        }

        numA = NodesOnLinknum->data[notdone - 1] + 1.0;
        i2 = (int32_T)(NodesOnLinknum->data[(int32_T)MPCList->data[nelA] - 1] +
                       1.0);
        if ((i2 < 1) || (i2 > 4)) {
          emlrtDynamicBoundsCheckR2012b(i2, 1, 4, &ej_emlrtBCI, sp);
        }

        if (MPCList->data[nelA] != i) {
          emlrtIntegerCheckR2012b(MPCList->data[nelA], &oc_emlrtDCI, sp);
        }

        if (notdone > NodesOnLink->size[1]) {
          emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnLink->size[1],
            &fj_emlrtBCI, sp);
        }

        NodesOnLink->data[(i2 + i1) - 1] = (real_T)nelA + 1.0;
        if (MPCList->data[nelA] != i) {
          emlrtIntegerCheckR2012b(MPCList->data[nelA], &pc_emlrtDCI, sp);
        }

        if (notdone > NodesOnLinknum->size[0]) {
          emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnLinknum->size[0],
            &gj_emlrtBCI, sp);
        }

        NodesOnLinknum->data[notdone - 1] = NodesOnLinknum->data[(int32_T)
          MPCList->data[nelA] - 1] + 1.0;
      }

      i = (int32_T)muDoubleScalarFloor(MPCList->data[nelA + MPCList->size[0]]);
      if (MPCList->data[nelA + MPCList->size[0]] != i) {
        emlrtIntegerCheckR2012b(MPCList->data[nelA + MPCList->size[0]],
          &e_emlrtDCI, sp);
      }

      notdone = (int32_T)MPCList->data[nelA + MPCList->size[0]];
      if ((notdone < 1) || (notdone > NodesOnPBCnum->size[0])) {
        emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnPBCnum->size[0],
          &d_emlrtBCI, sp);
      }

      d = NodesOnPBCnum->data[notdone - 1];
      if (1.0 > d) {
        loop_ub = 0;
      } else {
        if (((int32_T)d < 1) || ((int32_T)d > 4)) {
          emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, 4, &e_emlrtBCI, sp);
        }

        loop_ub = (int32_T)d;
      }

      if (MPCList->data[nelA + MPCList->size[0]] != i) {
        emlrtIntegerCheckR2012b(MPCList->data[nelA + MPCList->size[0]],
          &f_emlrtDCI, sp);
      }

      if (notdone > NodesOnPBC->size[1]) {
        emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnPBC->size[1],
          &f_emlrtBCI, sp);
      }

      /*  make sure pair isn't already in the list for nodeB */
      /*      if isOctave */
      st.site = &pe_emlrtRSI;
      nodeloc_size[0] = loop_ub;
      for (i1 = 0; i1 < loop_ub; i1++) {
        nodeloc_data[i1] = NodesOnPBC->data[i1 + 4 * (notdone - 1)];
      }

      b_st.site = &nf_emlrtRSI;
      old = local_ismember(MPCList->data[nelA], nodeloc_data, nodeloc_size);

      /*      else */
      /*      old = ismembc(nodesAB(1),currPBC); */
      /*      end */
      if (!old) {
        /*  add the pair to nodeB, expand by one */
        if (((int32_T)(d + 1.0) < 1) || ((int32_T)(d + 1.0) > 4)) {
          emlrtDynamicBoundsCheckR2012b((int32_T)(d + 1.0), 1, 4, &hj_emlrtBCI,
            sp);
        }

        if (MPCList->data[nelA + MPCList->size[0]] != i) {
          emlrtIntegerCheckR2012b(MPCList->data[nelA + MPCList->size[0]],
            &qc_emlrtDCI, sp);
        }

        if (notdone > NodesOnPBC->size[1]) {
          emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnPBC->size[1],
            &ij_emlrtBCI, sp);
        }

        i1 = 4 * (notdone - 1);
        NodesOnPBC->data[((int32_T)(d + 1.0) + i1) - 1] = MPCList->data[nelA];
        if (MPCList->data[nelA + MPCList->size[0]] != i) {
          emlrtIntegerCheckR2012b(MPCList->data[nelA + MPCList->size[0]],
            &rc_emlrtDCI, sp);
        }

        if (notdone > NodesOnPBCnum->size[0]) {
          emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnPBCnum->size[0],
            &jj_emlrtBCI, sp);
        }

        NodesOnPBCnum->data[notdone - 1]++;

        /*  Record which PBC ID refers to these nodes */
        if (MPCList->data[nelA + MPCList->size[0]] != i) {
          emlrtIntegerCheckR2012b(MPCList->data[nelA + MPCList->size[0]],
            &sc_emlrtDCI, sp);
        }

        if (notdone > NodesOnLinknum->size[0]) {
          emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnLinknum->size[0],
            &kj_emlrtBCI, sp);
        }

        if (MPCList->data[nelA + MPCList->size[0]] != i) {
          emlrtIntegerCheckR2012b(MPCList->data[nelA + MPCList->size[0]],
            &tc_emlrtDCI, sp);
        }

        if (notdone > NodesOnLinknum->size[0]) {
          emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnLinknum->size[0],
            &lj_emlrtBCI, sp);
        }

        NodesOnLinknum->data[notdone - 1]++;
        if (((int32_T)numA < 1) || ((int32_T)numA > 4)) {
          emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1, 4, &mj_emlrtBCI, sp);
        }

        if (MPCList->data[nelA + MPCList->size[0]] != i) {
          emlrtIntegerCheckR2012b(MPCList->data[nelA + MPCList->size[0]],
            &uc_emlrtDCI, sp);
        }

        if (notdone > NodesOnLink->size[1]) {
          emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnLink->size[1],
            &nj_emlrtBCI, sp);
        }

        NodesOnLink->data[((int32_T)numA + i1) - 1] = (real_T)nelA + 1.0;
      }

      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    /*  Find additional connections for nodes with pairs of pairs; this WILL find */
    /*  the extra pairs that are usually deleted to remove linear dependence in */
    /*  the stiffness matrix, e.g. the repeated edges in 2d or 3d. */
    st.site = &oe_emlrtRSI;
    i = b_NodesOnPBCnum->size[0];
    b_NodesOnPBCnum->size[0] = NodesOnPBCnum->size[0];
    emxEnsureCapacity_boolean_T(&st, b_NodesOnPBCnum, i, &lf_emlrtRTEI);
    loop_ub = NodesOnPBCnum->size[0];
    for (i = 0; i < loop_ub; i++) {
      b_NodesOnPBCnum->data[i] = (NodesOnPBCnum->data[i] > 1.0);
    }

    b_st.site = &ug_emlrtRSI;
    eml_find(&b_st, b_NodesOnPBCnum, ii);
    i = MorePBC->size[0];
    MorePBC->size[0] = ii->size[0];
    emxEnsureCapacity_real_T(&st, MorePBC, i, &nf_emlrtRTEI);
    loop_ub = ii->size[0];
    for (i = 0; i < loop_ub; i++) {
      MorePBC->data[i] = ii->data[i];
    }

    memset(&nodes[0], 0, 20U * sizeof(real_T));
    i = MorePBC->size[0];
    for (b_i = 0; b_i < i; b_i++) {
      if ((b_i + 1 < 1) || (b_i + 1 > MorePBC->size[0])) {
        emlrtDynamicBoundsCheckR2012b(b_i + 1, 1, MorePBC->size[0], &xi_emlrtBCI,
          sp);
      }

      nodes[0] = MorePBC->data[b_i];
      i1 = (int32_T)MorePBC->data[b_i];
      if ((i1 < 1) || (i1 > NodesOnPBCnum->size[0])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)MorePBC->data[b_i], 1,
          NodesOnPBCnum->size[0], &yi_emlrtBCI, sp);
      }

      if (NodesOnPBCnum->data[i1 - 1] > 0.0) {
        /*  pairs not already condensed */
        notdone = 1;
        numPBC = 1;

        /*  use a tree of connectivity to get to all node pairs involving MorePBC(i) */
        do {
          exitg1 = 0;
          st.site = &ne_emlrtRSI;
          if (notdone != 0) {
            /*              nodesS = nodes(1:lenn); */
            b_i1 = (real_T)numPBC + 1.0;
            b_i2 = (real_T)numPBC + 1.0;
            for (nelA = 0; nelA < numPBC; nelA++) {
              numA = nodes[nelA];
              if (numA != (int32_T)muDoubleScalarFloor(numA)) {
                emlrtIntegerCheckR2012b(numA, &g_emlrtDCI, sp);
              }

              if (((int32_T)numA < 1) || ((int32_T)numA > NodesOnPBCnum->size[0]))
              {
                emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1,
                  NodesOnPBCnum->size[0], &g_emlrtBCI, sp);
              }

              d = NodesOnPBCnum->data[(int32_T)numA - 1];
              if (1.0 > d) {
                loop_ub = 0;
              } else {
                if (((int32_T)d < 1) || ((int32_T)d > 4)) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, 4, &h_emlrtBCI,
                    sp);
                }

                loop_ub = (int32_T)d;
              }

              if ((int32_T)numA > NodesOnPBC->size[1]) {
                emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1, NodesOnPBC->
                  size[1], &i_emlrtBCI, sp);
              }

              b_i2 = (b_i1 + (real_T)loop_ub) - 1.0;
              if (b_i1 > b_i2) {
                i1 = 0;
                i2 = 0;
              } else {
                if (((int32_T)b_i1 < 1) || ((int32_T)b_i1 > 20)) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)b_i1, 1, 20,
                    &j_emlrtBCI, sp);
                }

                i1 = (int32_T)b_i1 - 1;
                if (((int32_T)b_i2 < 1) || ((int32_T)b_i2 > 20)) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)b_i2, 1, 20,
                    &k_emlrtBCI, sp);
                }

                i2 = (int32_T)b_i2;
              }

              ia_size[0] = i2 - i1;
              emlrtSubAssignSizeCheckR2012b(&ia_size[0], 1, &loop_ub, 1,
                &emlrtECI, sp);
              for (i2 = 0; i2 < loop_ub; i2++) {
                nodes[i1 + i2] = NodesOnPBC->data[i2 + 4 * ((int32_T)numA - 1)];
              }

              b_i1 = b_i2 + 1.0;
              if (*emlrtBreakCheckR2012bFlagVar != 0) {
                emlrtBreakCheckR2012b(sp);
              }
            }

            if (1.0 > b_i2) {
              loop_ub = 0;
            } else {
              if (((int32_T)b_i2 < 1) || ((int32_T)b_i2 > 20)) {
                emlrtDynamicBoundsCheckR2012b((int32_T)b_i2, 1, 20, &l_emlrtBCI,
                  sp);
              }

              loop_ub = (int32_T)b_i2;
            }

            st.site = &me_emlrtRSI;
            nodes_size[0] = loop_ub;
            if (0 <= loop_ub - 1) {
              memcpy(&nodes_data[0], &nodes[0], loop_ub * sizeof(real_T));
            }

            b_nodes_data.data = &nodes_data[0];
            b_nodes_data.size = &nodes_size[0];
            b_nodes_data.allocatedSize = 20;
            b_nodes_data.numDimensions = 1;
            b_nodes_data.canFreeData = false;
            b_st.site = &ah_emlrtRSI;
            unique_vector(&b_st, &b_nodes_data, setnPBC);
            if (setnPBC->size[0] == numPBC) {
              /*  starting list of node pairs matches the unique short list */
              if (1 > setnPBC->size[0]) {
                i1 = 0;
              } else {
                if ((setnPBC->size[0] < 1) || (setnPBC->size[0] > 20)) {
                  emlrtDynamicBoundsCheckR2012b(setnPBC->size[0], 1, 20,
                    &m_emlrtBCI, sp);
                }

                i1 = setnPBC->size[0];
              }

              emlrtSubAssignSizeCheckR2012b(&i1, 1, &setnPBC->size[0], 1,
                &b_emlrtECI, sp);
              loop_ub = setnPBC->size[0];
              for (i1 = 0; i1 < loop_ub; i1++) {
                nodes[i1] = setnPBC->data[i1];
              }

              notdone = 0;
            } else {
              /*  found some new pairs, try searching again */
              if (1 > setnPBC->size[0]) {
                i1 = 0;
              } else {
                if ((setnPBC->size[0] < 1) || (setnPBC->size[0] > 20)) {
                  emlrtDynamicBoundsCheckR2012b(setnPBC->size[0], 1, 20,
                    &n_emlrtBCI, sp);
                }

                i1 = setnPBC->size[0];
              }

              emlrtSubAssignSizeCheckR2012b(&i1, 1, &setnPBC->size[0], 1,
                &c_emlrtECI, sp);
              loop_ub = setnPBC->size[0];
              for (i1 = 0; i1 < loop_ub; i1++) {
                nodes[i1] = setnPBC->data[i1];
              }
            }

            numPBC = setnPBC->size[0];
            if (*emlrtBreakCheckR2012bFlagVar != 0) {
              emlrtBreakCheckR2012b(sp);
            }
          } else {
            exitg1 = 1;
          }
        } while (exitg1 == 0);

        if (numPBC + 1U > 20U) {
          i1 = -1;
          i2 = -1;
        } else {
          if (((int32_T)(numPBC + 1U) < 1) || ((int32_T)(numPBC + 1U) > 20)) {
            emlrtDynamicBoundsCheckR2012b((int32_T)(numPBC + 1U), 1, 20,
              &o_emlrtBCI, sp);
          }

          i1 = numPBC - 1;
          i2 = 19;
        }

        loop_ub = i2 - i1;
        if (0 <= loop_ub - 1) {
          memset(&nodes[i1 + 1], 0, ((loop_ub + i1) - i1) * sizeof(real_T));
        }

        /* clean it out */
        /*  record the maximum connected pairs into the NodesOnPBC for all */
        /*  the connected nodes in the set; also combine together the */
        /*  NodesOnLink at the same time */
        /*          lenn = length(nodes); */
        memset(&links[0], 0, 20U * sizeof(real_T));
        b_i1 = 1.0;
        b_i2 = 1.0;
        if (0 <= numPBC - 1) {
          if (1 > numPBC) {
            nel = 0;
          } else {
            if (numPBC > 20) {
              emlrtDynamicBoundsCheckR2012b(numPBC, 1, 20, &p_emlrtBCI, sp);
            }

            nel = numPBC;
          }

          if (1 > numPBC - 1) {
            elemB = 0;
          } else {
            if ((numPBC - 1 < 1) || (numPBC - 1 > 4)) {
              emlrtDynamicBoundsCheckR2012b(numPBC - 1, 1, 4, &q_emlrtBCI, sp);
            }

            elemB = numPBC - 1;
          }
        }

        for (nelA = 0; nelA < numPBC; nelA++) {
          d = nodes[nelA];
          if (d != (int32_T)muDoubleScalarFloor(d)) {
            emlrtIntegerCheckR2012b(d, &vc_emlrtDCI, sp);
          }

          if (((int32_T)d < 1) || ((int32_T)d > NodesOnPBCnum->size[0])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnPBCnum->size[0],
              &oj_emlrtBCI, sp);
          }

          NodesOnPBCnum->data[(int32_T)d - 1] = -((real_T)numPBC - 1.0);

          /*  flag this node as already condensed */
          if ((int32_T)d > NodesOnPBC->size[1]) {
            emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnPBC->size[1],
              &r_emlrtBCI, sp);
          }

          st.site = &le_emlrtRSI;
          b_st.site = &lh_emlrtRSI;
          nodes_size[0] = nel;
          if (0 <= nel - 1) {
            memcpy(&nodes_data[0], &nodes[0], nel * sizeof(real_T));
          }

          nodeloc_size[0] = nodes_size[0];
          c_st.site = &mh_emlrtRSI;
          do_vectors(&c_st, nodes_data, nodeloc_size, nodes[nelA], links2_data,
                     links2_size, ia_data, ia_size, ib_size);
          emlrtSubAssignSizeCheckR2012b(&elemB, 1, &links2_size[0], 1,
            &d_emlrtECI, sp);
          loop_ub = links2_size[0];
          for (i1 = 0; i1 < loop_ub; i1++) {
            NodesOnPBC->data[i1 + 4 * ((int32_T)nodes[nelA] - 1)] =
              links2_data[i1];
          }

          if ((int32_T)d > NodesOnLinknum->size[0]) {
            emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLinknum->size[0],
              &rj_emlrtBCI, sp);
          }

          b_i2 = (b_i1 + NodesOnLinknum->data[(int32_T)d - 1]) - 1.0;
          if ((int32_T)d > NodesOnLinknum->size[0]) {
            emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLinknum->size[0],
              &s_emlrtBCI, sp);
          }

          d1 = NodesOnLinknum->data[(int32_T)nodes[nelA] - 1];
          if (1.0 > d1) {
            loop_ub = 0;
          } else {
            if (((int32_T)d1 < 1) || ((int32_T)d1 > 4)) {
              emlrtDynamicBoundsCheckR2012b((int32_T)d1, 1, 4, &t_emlrtBCI, sp);
            }

            loop_ub = (int32_T)d1;
          }

          if (b_i1 > b_i2) {
            i1 = 0;
            i2 = 0;
          } else {
            if (((int32_T)b_i1 < 1) || ((int32_T)b_i1 > 20)) {
              emlrtDynamicBoundsCheckR2012b((int32_T)b_i1, 1, 20, &v_emlrtBCI,
                sp);
            }

            i1 = (int32_T)b_i1 - 1;
            if (((int32_T)b_i2 < 1) || ((int32_T)b_i2 > 20)) {
              emlrtDynamicBoundsCheckR2012b((int32_T)b_i2, 1, 20, &w_emlrtBCI,
                sp);
            }

            i2 = (int32_T)b_i2;
          }

          if ((int32_T)d > NodesOnLink->size[1]) {
            emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLink->size[1],
              &u_emlrtBCI, sp);
          }

          ia_size[0] = i2 - i1;
          emlrtSubAssignSizeCheckR2012b(&ia_size[0], 1, &loop_ub, 1, &e_emlrtECI,
            sp);
          for (i2 = 0; i2 < loop_ub; i2++) {
            links[i1 + i2] = NodesOnLink->data[i2 + 4 * ((int32_T)nodes[nelA] -
              1)];
          }

          b_i1 = b_i2 + 1.0;
          if (*emlrtBreakCheckR2012bFlagVar != 0) {
            emlrtBreakCheckR2012b(sp);
          }
        }

        if (1.0 > b_i2) {
          loop_ub = 0;
        } else {
          loop_ub = (int32_T)b_i2;
        }

        st.site = &ke_emlrtRSI;
        nodes_size[0] = loop_ub;
        if (0 <= loop_ub - 1) {
          memcpy(&nodes_data[0], &links[0], loop_ub * sizeof(real_T));
        }

        c_nodes_data.data = &nodes_data[0];
        c_nodes_data.size = &nodes_size[0];
        c_nodes_data.allocatedSize = 20;
        c_nodes_data.numDimensions = 1;
        c_nodes_data.canFreeData = false;
        b_st.site = &ah_emlrtRSI;
        unique_vector(&b_st, &c_nodes_data, setnPBC);
        links2_size[0] = setnPBC->size[0];
        loop_ub = setnPBC->size[0];
        end = setnPBC->size[0] - 1;
        trueCount = 0;
        for (notdone = 0; notdone < loop_ub; notdone++) {
          links2_data[notdone] = setnPBC->data[notdone];
          if (setnPBC->data[notdone] != 0.0) {
            trueCount++;
          }
        }

        nelA = 0;
        for (notdone = 0; notdone <= end; notdone++) {
          if (links2_data[notdone] != 0.0) {
            if ((notdone + 1 < 1) || (notdone + 1 > links2_size[0])) {
              emlrtDynamicBoundsCheckR2012b(notdone + 1, 1, links2_size[0],
                &pj_emlrtBCI, sp);
            }

            links2_data[nelA] = links2_data[notdone];
            nelA++;
          }
        }

        if (1 > trueCount) {
          i1 = 0;
        } else {
          if (trueCount > 4) {
            emlrtDynamicBoundsCheckR2012b(trueCount, 1, 4, &x_emlrtBCI, sp);
          }

          i1 = trueCount;
        }

        if (1 > numPBC) {
          loop_ub = 0;
        } else {
          if (numPBC > 20) {
            emlrtDynamicBoundsCheckR2012b(numPBC, 1, 20, &y_emlrtBCI, sp);
          }

          loop_ub = numPBC;
        }

        for (i2 = 0; i2 < loop_ub; i2++) {
          d = nodes[i2];
          if (d != (int32_T)muDoubleScalarFloor(d)) {
            emlrtIntegerCheckR2012b(d, &wc_emlrtDCI, sp);
          }

          if (((int32_T)d < 1) || ((int32_T)d > NodesOnLink->size[1])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLink->size[1],
              &sj_emlrtBCI, sp);
          }

          ia_data[i2] = (int32_T)d - 1;
        }

        tmp_size[0] = trueCount;
        tmp_size[1] = numPBC;
        for (i2 = 0; i2 < numPBC; i2++) {
          for (i3 = 0; i3 < trueCount; i3++) {
            tmp_data[i3 + trueCount * i2] = links2_data[i3];
          }
        }

        b_numel[0] = i1;
        b_numel[1] = loop_ub;
        emlrtSubAssignSizeCheckR2012b(&b_numel[0], 2, &tmp_size[0], 2,
          &f_emlrtECI, sp);
        for (i1 = 0; i1 < numPBC; i1++) {
          for (i2 = 0; i2 < trueCount; i2++) {
            NodesOnLink->data[i2 + 4 * ia_data[i1]] = tmp_data[i2 + trueCount *
              i1];
          }
        }

        if (1 > numPBC) {
          loop_ub = 0;
        } else {
          loop_ub = numPBC;
        }

        for (i1 = 0; i1 < loop_ub; i1++) {
          d = nodes[i1];
          if (d != (int32_T)muDoubleScalarFloor(d)) {
            emlrtIntegerCheckR2012b(d, &xc_emlrtDCI, sp);
          }

          if (((int32_T)d < 1) || ((int32_T)d > NodesOnLinknum->size[0])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, NodesOnLinknum->size[0],
              &tj_emlrtBCI, sp);
          }

          ia_data[i1] = (int32_T)d;
        }

        for (i1 = 0; i1 < loop_ub; i1++) {
          NodesOnLinknum->data[ia_data[i1] - 1] = (int8_T)trueCount;
        }
      }

      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    i = setnPBC->size[0];
    setnPBC->size[0] = NodesOnPBCnum->size[0];
    emxEnsureCapacity_real_T(sp, setnPBC, i, &of_emlrtRTEI);
    loop_ub = NodesOnPBCnum->size[0] - 1;
    for (i = 0; i <= loop_ub; i++) {
      setnPBC->data[i] = NodesOnPBCnum->data[i];
    }

    st.site = &je_emlrtRSI;
    b_abs(&st, setnPBC, NodesOnPBCnum);
  } else {
    NodesOnPBC->size[0] = 4;
    NodesOnPBC->size[1] = 0;

    /*  nodes that are paired up */
    NodesOnPBCnum->size[0] = 0;

    /*  number of nodes with common pairing */
    NodesOnLink->size[0] = 4;
    NodesOnLink->size[1] = 0;

    /*  IDs of PBC referring to node */
    NodesOnLinknum->size[0] = 0;

    /*  number of PBC referring to node */
  }

  /*  Mesh error checks */
  /*  Error checks for mesh arrays */
  /*  Nodes */
  if (*numnp < Coordinates->size[0]) {
    st.site = &wo_emlrtRSI;
    disp(&st, emlrt_marshallOut(&st, cv), &emlrtMCI);
    st.site = &yo_emlrtRSI;
    disp(&st, b_emlrt_marshallOut(&st, cv1), &b_emlrtMCI);
  } else {
    if (*numnp > Coordinates->size[0]) {
      st.site = &ie_emlrtRSI;
      b_error(&st);
    }
  }

  /*  Elements */
  if (numel < NodesOnElement->size[0]) {
    st.site = &bp_emlrtRSI;
    disp(&st, c_emlrt_marshallOut(&st, cv2), &c_emlrtMCI);
    st.site = &xo_emlrtRSI;
    disp(&st, b_emlrt_marshallOut(&st, cv1), &d_emlrtMCI);
  } else {
    if (numel > NodesOnElement->size[0]) {
      st.site = &he_emlrtRSI;
      c_error(&st);
    }
  }

  if (NodesOnElement->size[1] != nen) {
    st.site = &cp_emlrtRSI;
    disp(&st, d_emlrt_marshallOut(&st, cv3), &e_emlrtMCI);
    st.site = &ge_emlrtRSI;
    d_error(&st);
  }

  if (1.0 > numel) {
    i = 0;
  } else {
    i = NodesOnElement->size[0];
    if (1 > i) {
      emlrtDynamicBoundsCheckR2012b(1, 1, i, &ab_emlrtBCI, sp);
    }

    if (numel != (int32_T)muDoubleScalarFloor(numel)) {
      emlrtIntegerCheckR2012b(numel, &h_emlrtDCI, sp);
    }

    i = NodesOnElement->size[0];
    if ((int32_T)numel > i) {
      emlrtDynamicBoundsCheckR2012b((int32_T)numel, 1, i, &bb_emlrtBCI, sp);
    }

    i = (int32_T)numel;
  }

  if (1.0 > nen) {
    loop_ub = 0;
  } else {
    i1 = NodesOnElement->size[1];
    if (1 > i1) {
      emlrtDynamicBoundsCheckR2012b(1, 1, i1, &cb_emlrtBCI, sp);
    }

    i1 = NodesOnElement->size[1];
    loop_ub = (int32_T)nen;
    if (loop_ub > i1) {
      emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1, i1, &db_emlrtBCI, sp);
    }
  }

  st.site = &fe_emlrtRSI;
  b_st.site = &uh_emlrtRSI;
  c_st.site = &vh_emlrtRSI;
  d_st.site = &wh_emlrtRSI;
  if (((i != 1) || (loop_ub != 1)) && (i == 1)) {
    emlrtErrorWithMessageIdR2018a(&d_st, &ob_emlrtRTEI,
      "Coder:toolbox:autoDimIncompatibility",
      "Coder:toolbox:autoDimIncompatibility", 0);
  }

  if (i < 1) {
    emlrtErrorWithMessageIdR2018a(&d_st, &nb_emlrtRTEI,
      "Coder:toolbox:eml_min_or_max_varDimZero",
      "Coder:toolbox:eml_min_or_max_varDimZero", 0);
  }

  e_st.site = &xh_emlrtRSI;
  f_st.site = &yh_emlrtRSI;
  g_st.site = &ai_emlrtRSI;
  i1 = loop_ub - 1;
  if (loop_ub >= 1) {
    h_st.site = &ci_emlrtRSI;
    for (nelA = 0; nelA <= i1; nelA++) {
      nodeC2_data[nelA] = NodesOnElement->data[NodesOnElement->size[0] * nelA];
      h_st.site = &bi_emlrtRSI;
      if ((2 <= i) && (i > 2147483646)) {
        i_st.site = &ff_emlrtRSI;
        check_forloop_overflow_error(&i_st);
      }

      for (b_i = 2; b_i <= i; b_i++) {
        numA = nodeC2_data[nelA];
        b_i1 = NodesOnElement->data[(b_i + NodesOnElement->size[0] * nelA) - 1];
        if ((!muDoubleScalarIsNaN(b_i1)) && (muDoubleScalarIsNaN(numA) || (numA <
              b_i1))) {
          nodeC2_data[nelA] = b_i1;
        }
      }
    }
  }

  nodeC2_size[0] = 1;
  nodeC2_size[1] = loop_ub;
  for (i = 0; i < loop_ub; i++) {
    b_nodeC2_data[i] = (nodeC2_data[i] > *numnp);
  }

  st.site = &fe_emlrtRSI;
  if (ifWhileCond(b_nodeC2_data, nodeC2_size)) {
    st.site = &ee_emlrtRSI;
    e_error(&st);
  }

  /*  Check that interfaces are assigned also when intraface couplers are requested */
  st.site = &de_emlrtRSI;
  b_st.site = &ug_emlrtRSI;
  b_eml_find(&b_st, InterTypes, ii);

  /*  reset to logical */
  notdone = InterTypes->size[0] * InterTypes->size[1];
  loop_ub = ii->size[0];
  for (i = 0; i < loop_ub; i++) {
    i1 = ii->data[i];
    if ((i1 < 1LL) || ((int64_T)i1 > notdone)) {
      emlrtDynamicBoundsCheckInt64(i1, 1, notdone, &qj_emlrtBCI, sp);
    }

    ii->data[i] = i1;
  }

  loop_ub = ii->size[0] - 1;
  for (i = 0; i <= loop_ub; i++) {
    InterTypes->data[ii->data[i] - 1] = 1.0;
  }

  d = muDoubleScalarRound(nummat);
  if (d < 9.2233720368547758E+18) {
    if (d >= -9.2233720368547758E+18) {
      q0 = (int64_T)d;
    } else {
      q0 = MIN_int64_T;
    }
  } else if (d >= 9.2233720368547758E+18) {
    q0 = MAX_int64_T;
  } else {
    q0 = 0LL;
  }

  if (q0 < -9223372036854775807LL) {
    qY = MIN_int64_T;
  } else {
    qY = q0 - 1LL;
  }

  st.site = &ce_emlrtRSI;
  if ((1LL <= qY) && (qY > 9223372036854775806LL)) {
    b_st.site = &ff_emlrtRSI;
    b_check_forloop_overflow_error(&b_st);
  }

  mat = 1LL;
  emxInit_int64_T(sp, &inter, 1, &rj_emlrtRTEI, true);
  emxInit_real_T(sp, &setAll, 2, &oi_emlrtRTEI, true);
  emxInit_real_T(sp, &setPBC, 2, &ti_emlrtRTEI, true);
  emxInit_int32_T(sp, &b_ii, 2, &ck_emlrtRTEI, true);
  emxInit_int32_T(sp, &c_ii, 1, &ck_emlrtRTEI, true);
  emxInit_real_T(sp, &b_MorePBC, 1, &yi_emlrtRTEI, true);
  while (mat <= qY) {
    d = (int32_T)muDoubleScalarFloor((real_T)mat);
    if (mat != d) {
      emlrtIntegerCheckR2012b((real_T)mat, &yc_emlrtDCI, sp);
    }

    i4 = (int64_T)muDoubleScalarFloor((real_T)mat);
    if ((i4 < 1LL) || (i4 > InterTypes->size[0])) {
      emlrtDynamicBoundsCheckInt64(i4, 1, InterTypes->size[0], &uj_emlrtBCI, sp);
    }

    if (mat != d) {
      emlrtIntegerCheckR2012b((real_T)mat, &yc_emlrtDCI, sp);
    }

    if ((i4 < 1LL) || (i4 > InterTypes->size[1])) {
      emlrtDynamicBoundsCheckInt64(i4, 1, InterTypes->size[1], &uj_emlrtBCI, sp);
    }

    numA = InterTypes->data[((int32_T)i4 + InterTypes->size[0] * ((int32_T)i4 -
      1)) - 1];
    st.site = &be_emlrtRSI;
    if (muDoubleScalarIsNaN(numA)) {
      emlrtErrorWithMessageIdR2018a(&st, &mb_emlrtRTEI, "MATLAB:nologicalnan",
        "MATLAB:nologicalnan", 0);
    }

    if (numA != 0.0) {
      if (mat > 9223372036854775806LL) {
        b_qY = MAX_int64_T;
      } else {
        b_qY = mat + 1LL;
      }

      if (b_qY > q0) {
        i = 0;
        i1 = 0;
      } else {
        if ((real_T)b_qY != (int32_T)muDoubleScalarFloor((real_T)b_qY)) {
          emlrtIntegerCheckR2012b((real_T)b_qY, &i_emlrtDCI, sp);
        }

        i5 = (int64_T)muDoubleScalarFloor((real_T)b_qY);
        if ((i5 < 1LL) || (i5 > InterTypes->size[0])) {
          emlrtDynamicBoundsCheckInt64((int64_T)muDoubleScalarFloor((real_T)b_qY),
            1, InterTypes->size[0], &eb_emlrtBCI, sp);
        }

        i = (int32_T)(int64_T)muDoubleScalarFloor((real_T)b_qY) - 1;
        if ((real_T)q0 != (int32_T)muDoubleScalarFloor((real_T)q0)) {
          emlrtIntegerCheckR2012b((real_T)q0, &j_emlrtDCI, sp);
        }

        i5 = (int64_T)muDoubleScalarFloor((real_T)q0);
        if ((i5 < 1LL) || (i5 > InterTypes->size[0])) {
          emlrtDynamicBoundsCheckInt64((int64_T)muDoubleScalarFloor((real_T)q0),
            1, InterTypes->size[0], &fb_emlrtBCI, sp);
        }

        i1 = (int32_T)(int64_T)muDoubleScalarFloor((real_T)q0);
      }

      st.site = &ae_emlrtRSI;
      if (mat != d) {
        emlrtIntegerCheckR2012b((real_T)mat, &k_emlrtDCI, &st);
      }

      if ((i4 < 1LL) || (i4 > InterTypes->size[1])) {
        emlrtDynamicBoundsCheckInt64((int64_T)muDoubleScalarFloor((real_T)mat),
          1, InterTypes->size[1], &gb_emlrtBCI, &st);
      }

      loop_ub = i1 - i;
      i1 = b_MorePBC->size[0];
      b_MorePBC->size[0] = loop_ub;
      emxEnsureCapacity_real_T(&st, b_MorePBC, i1, &uf_emlrtRTEI);
      for (i1 = 0; i1 < loop_ub; i1++) {
        b_MorePBC->data[i1] = InterTypes->data[(i + i1) + InterTypes->size[0] *
          ((int32_T)mat - 1)] - 1.0;
      }

      b_st.site = &ug_emlrtRSI;
      c_eml_find(&b_st, b_MorePBC, ii);
      i = inter->size[0];
      inter->size[0] = ii->size[0];
      emxEnsureCapacity_int64_T(sp, inter, i, &wf_emlrtRTEI);
      loop_ub = ii->size[0];
      for (i = 0; i < loop_ub; i++) {
        inter->data[i] = ii->data[i];
      }

      if (inter->size[0] != 0) {
        st.site = &ep_emlrtRSI;
        disp(&st, e_emlrt_marshallOut(&st, cv7), &f_emlrtMCI);
        st.site = &yd_emlrtRSI;
        b_st.site = &gi_emlrtRSI;
        c_st.site = &gp_emlrtRSI;
        j_st.site = &ap_emlrtRSI;
        numA = emlrt_marshallIn(&c_st, c_feval(&c_st, h_emlrt_marshallOut(&c_st,
          cv4), f_emlrt_marshallOut(1.0), c_emlrt_marshallOut(&j_st, cv8),
          i_emlrt_marshallOut(mat), &l_emlrtMCI), "<output of feval>");
        emlrtDisplayR2012b(f_emlrt_marshallOut(numA), "ans", &emlrtRTEI, sp);
        i = c_ii->size[0];
        c_ii->size[0] = inter->size[0];
        emxEnsureCapacity_int32_T(sp, c_ii, i, &yf_emlrtRTEI);
        loop_ub = inter->size[0];
        for (i = 0; i < loop_ub; i++) {
          b_qY = inter->data[i];
          if ((b_qY < 0LL) && (mat < MIN_int64_T - b_qY)) {
            b_qY = MIN_int64_T;
          } else if ((b_qY > 0LL) && (mat > MAX_int64_T - b_qY)) {
            b_qY = MAX_int64_T;
          } else {
            b_qY += mat;
          }

          if ((real_T)b_qY != (int32_T)muDoubleScalarFloor((real_T)b_qY)) {
            emlrtIntegerCheckR2012b((real_T)b_qY, &fd_emlrtDCI, sp);
          }

          i5 = (int64_T)muDoubleScalarFloor((real_T)b_qY);
          if ((i5 < 1LL) || (i5 > InterTypes->size[0])) {
            emlrtDynamicBoundsCheckInt64((int64_T)muDoubleScalarFloor((real_T)
              b_qY), 1, InterTypes->size[0], &vj_emlrtBCI, sp);
          }

          c_ii->data[i] = (int32_T)(int64_T)muDoubleScalarFloor((real_T)b_qY);
        }

        if (mat != d) {
          emlrtIntegerCheckR2012b((real_T)mat, &l_emlrtDCI, sp);
        }

        if ((i4 < 1LL) || (i4 > InterTypes->size[1])) {
          emlrtDynamicBoundsCheckInt64((int64_T)muDoubleScalarFloor((real_T)mat),
            1, InterTypes->size[1], &hb_emlrtBCI, sp);
        }

        loop_ub = c_ii->size[0];
        for (i = 0; i < loop_ub; i++) {
          InterTypes->data[(c_ii->data[i] + InterTypes->size[0] * ((int32_T)mat
            - 1)) - 1] = 1.0;
        }
      }

      if (mat < -9223372036854775807LL) {
        b_qY = MIN_int64_T;
      } else {
        b_qY = mat - 1LL;
      }

      if (1LL > b_qY) {
        loop_ub = 0;
      } else {
        if (1 > InterTypes->size[1]) {
          emlrtDynamicBoundsCheckInt64(1LL, 1, InterTypes->size[1], &jb_emlrtBCI,
            sp);
        }

        if ((real_T)b_qY != (int32_T)muDoubleScalarFloor((real_T)b_qY)) {
          emlrtIntegerCheckR2012b((real_T)b_qY, &n_emlrtDCI, sp);
        }

        i5 = (int64_T)muDoubleScalarFloor((real_T)b_qY);
        if ((i5 < 1LL) || (i5 > InterTypes->size[1])) {
          emlrtDynamicBoundsCheckInt64((int64_T)muDoubleScalarFloor((real_T)b_qY),
            1, InterTypes->size[1], &kb_emlrtBCI, sp);
        }

        loop_ub = (int32_T)(int64_T)muDoubleScalarFloor((real_T)b_qY);
      }

      st.site = &xd_emlrtRSI;
      if (mat != d) {
        emlrtIntegerCheckR2012b((real_T)mat, &m_emlrtDCI, &st);
      }

      if ((i4 < 1LL) || (i4 > InterTypes->size[0])) {
        emlrtDynamicBoundsCheckInt64((int64_T)muDoubleScalarFloor((real_T)mat),
          1, InterTypes->size[0], &ib_emlrtBCI, &st);
      }

      i = setPBC->size[0] * setPBC->size[1];
      setPBC->size[0] = 1;
      setPBC->size[1] = loop_ub;
      emxEnsureCapacity_real_T(&st, setPBC, i, &ag_emlrtRTEI);
      for (i = 0; i < loop_ub; i++) {
        setPBC->data[i] = InterTypes->data[((int32_T)mat + InterTypes->size[0] *
          i) - 1] - 1.0;
      }

      b_st.site = &ug_emlrtRSI;
      d_eml_find(&b_st, setPBC, b_ii);
      i = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      setAll->size[1] = b_ii->size[1];
      emxEnsureCapacity_real_T(&st, setAll, i, &cg_emlrtRTEI);
      loop_ub = b_ii->size[0] * b_ii->size[1];
      for (i = 0; i < loop_ub; i++) {
        setAll->data[i] = b_ii->data[i];
      }

      if (setAll->size[1] != 0) {
        st.site = &dp_emlrtRSI;
        disp(&st, e_emlrt_marshallOut(&st, cv7), &h_emlrtMCI);
        st.site = &wd_emlrtRSI;
        b_st.site = &gi_emlrtRSI;
        c_st.site = &gp_emlrtRSI;
        j_st.site = &ap_emlrtRSI;
        numA = emlrt_marshallIn(&c_st, c_feval(&c_st, h_emlrt_marshallOut(&c_st,
          cv4), f_emlrt_marshallOut(1.0), c_emlrt_marshallOut(&j_st, cv8),
          i_emlrt_marshallOut(mat), &l_emlrtMCI), "<output of feval>");
        emlrtDisplayR2012b(f_emlrt_marshallOut(numA), "ans", &b_emlrtRTEI, sp);
        i = c_ii->size[0];
        c_ii->size[0] = setAll->size[1];
        emxEnsureCapacity_int32_T(sp, c_ii, i, &eg_emlrtRTEI);
        loop_ub = setAll->size[1];
        for (i = 0; i < loop_ub; i++) {
          i1 = (int32_T)setAll->data[i];
          if ((i1 < 1) || (i1 > InterTypes->size[1])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)setAll->data[i], 1,
              InterTypes->size[1], &jk_emlrtBCI, sp);
          }

          c_ii->data[i] = i1;
        }

        if (mat != d) {
          emlrtIntegerCheckR2012b((real_T)mat, &o_emlrtDCI, sp);
        }

        if ((i4 < 1LL) || (i4 > InterTypes->size[0])) {
          emlrtDynamicBoundsCheckInt64((int64_T)muDoubleScalarFloor((real_T)mat),
            1, InterTypes->size[0], &lb_emlrtBCI, sp);
        }

        loop_ub = c_ii->size[0];
        for (i = 0; i < loop_ub; i++) {
          InterTypes->data[((int32_T)mat + InterTypes->size[0] * (c_ii->data[i]
            - 1)) - 1] = 1.0;
        }
      }
    }

    if (mat < -9223372036854775807LL) {
      b_qY = MIN_int64_T;
    } else {
      b_qY = mat - 1LL;
    }

    if (1LL > b_qY) {
      loop_ub = 0;
    } else {
      if (1 > InterTypes->size[0]) {
        emlrtDynamicBoundsCheckInt64(1LL, 1, InterTypes->size[0], &mb_emlrtBCI,
          sp);
      }

      if ((real_T)b_qY != (int32_T)muDoubleScalarFloor((real_T)b_qY)) {
        emlrtIntegerCheckR2012b((real_T)b_qY, &p_emlrtDCI, sp);
      }

      i5 = (int64_T)muDoubleScalarFloor((real_T)b_qY);
      if ((i5 < 1LL) || (i5 > InterTypes->size[0])) {
        emlrtDynamicBoundsCheckInt64((int64_T)muDoubleScalarFloor((real_T)b_qY),
          1, InterTypes->size[0], &nb_emlrtBCI, sp);
      }

      loop_ub = (int32_T)(int64_T)muDoubleScalarFloor((real_T)b_qY);
    }

    st.site = &vd_emlrtRSI;
    if (mat != d) {
      emlrtIntegerCheckR2012b((real_T)mat, &q_emlrtDCI, &st);
    }

    if ((i4 < 1LL) || (i4 > InterTypes->size[1])) {
      emlrtDynamicBoundsCheckInt64((int64_T)muDoubleScalarFloor((real_T)mat), 1,
        InterTypes->size[1], &ob_emlrtBCI, &st);
    }

    i = b_MorePBC->size[0];
    b_MorePBC->size[0] = loop_ub;
    emxEnsureCapacity_real_T(&st, b_MorePBC, i, &vf_emlrtRTEI);
    for (i = 0; i < loop_ub; i++) {
      b_MorePBC->data[i] = InterTypes->data[i + InterTypes->size[0] * ((int32_T)
        mat - 1)];
    }

    b_st.site = &ug_emlrtRSI;
    c_eml_find(&b_st, b_MorePBC, c_ii);
    if (c_ii->size[0] != 0) {
      st.site = &ud_emlrtRSI;
      b_st.site = &gi_emlrtRSI;
      c_st.site = &gp_emlrtRSI;
      j_st.site = &ap_emlrtRSI;
      numA = emlrt_marshallIn(&c_st, c_feval(&c_st, h_emlrt_marshallOut(&c_st,
        cv4), f_emlrt_marshallOut(1.0), j_emlrt_marshallOut(&j_st, cv5),
        i_emlrt_marshallOut(mat), &l_emlrtMCI), "<output of feval>");
      emlrtDisplayR2012b(f_emlrt_marshallOut(numA), "ans", &c_emlrtRTEI, sp);
      st.site = &fp_emlrtRSI;
      disp(&st, g_emlrt_marshallOut(&st, cv6), &g_emlrtMCI);
    }

    mat++;
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  emxFree_int32_T(&b_ii);
  emxFree_int64_T(&inter);

  /*  Form ElementsOnNode, ElementsOnNodeNum, DG nodes and connectivity */
  st.site = &td_emlrtRSI;
  if (muDoubleScalarIsNaN(usePBC)) {
    emlrtErrorWithMessageIdR2018a(&st, &mb_emlrtRTEI, "MATLAB:nologicalnan",
      "MATLAB:nologicalnan", 0);
  }

  emxInit_real_T(sp, &ElementsOnNodePBCRow, 2, &tf_emlrtRTEI, true);
  emxInit_real_T(sp, &ElementsOnNodeRow, 2, &xf_emlrtRTEI, true);
  emxInitStruct_sparse(sp, &NodeRegSum, &vj_emlrtRTEI, true);
  emxInit_int32_T(sp, &r, 1, &wj_emlrtRTEI, true);
  emxInit_int32_T(sp, &ib, 1, &dk_emlrtRTEI, true);
  emxInitStruct_sparse1(sp, &expl_temp, &fk_emlrtRTEI, true);
  emxInitStruct_sparse(sp, &b_expl_temp, &hk_emlrtRTEI, true);
  emxInit_real_T(sp, &NodeRegInd, 2, &jg_emlrtRTEI, true);
  if (usePBC != 0.0) {
    emxInit_real_T(sp, &c_NodeRegInd, 2, &qf_emlrtRTEI, true);

    /*  also form the zipped mesh at the same time */
    i = c_NodeRegInd->size[0] * c_NodeRegInd->size[1];
    c_NodeRegInd->size[0] = 3;
    emxEnsureCapacity_real_T(sp, c_NodeRegInd, i, &qf_emlrtRTEI);
    d = numel * 9.0;
    if (!(d >= 0.0)) {
      emlrtNonNegativeCheckR2012b(d, &pb_emlrtDCI, sp);
    }

    if (d != (int32_T)muDoubleScalarFloor(d)) {
      emlrtIntegerCheckR2012b(d, &ob_emlrtDCI, sp);
    }

    i = c_NodeRegInd->size[0] * c_NodeRegInd->size[1];
    c_NodeRegInd->size[1] = (int32_T)d;
    emxEnsureCapacity_real_T(sp, c_NodeRegInd, i, &qf_emlrtRTEI);
    d = numel * 9.0;
    if (!(d >= 0.0)) {
      emlrtNonNegativeCheckR2012b(d, &cd_emlrtDCI, sp);
    }

    if (d != (int32_T)muDoubleScalarFloor(d)) {
      emlrtIntegerCheckR2012b(d, &bd_emlrtDCI, sp);
    }

    loop_ub = 3 * (int32_T)d;
    for (i = 0; i < loop_ub; i++) {
      c_NodeRegInd->data[i] = 0.0;
    }

    /*  node, region attached, regular=1 or PBC=-1 */
    if (!(numel >= 0.0)) {
      emlrtNonNegativeCheckR2012b(numel, &rb_emlrtDCI, sp);
    }

    d = muDoubleScalarFloor(numel);
    if (numel != d) {
      emlrtIntegerCheckR2012b(numel, &qb_emlrtDCI, sp);
    }

    i = ElementsOnNodePBCRow->size[0] * ElementsOnNodePBCRow->size[1];
    ElementsOnNodePBCRow->size[0] = (int32_T)numel;
    loop_ub = (int32_T)nen;
    ElementsOnNodePBCRow->size[1] = loop_ub;
    emxEnsureCapacity_real_T(sp, ElementsOnNodePBCRow, i, &tf_emlrtRTEI);
    if (numel != d) {
      emlrtIntegerCheckR2012b(numel, &ed_emlrtDCI, sp);
    }

    notdone = (int32_T)numel * (int32_T)nen;
    for (i = 0; i < notdone; i++) {
      ElementsOnNodePBCRow->data[i] = 0.0;
    }

    i = ElementsOnNodeRow->size[0] * ElementsOnNodeRow->size[1];
    ElementsOnNodeRow->size[0] = (int32_T)numel;
    ElementsOnNodeRow->size[1] = loop_ub;
    emxEnsureCapacity_real_T(sp, ElementsOnNodeRow, i, &xf_emlrtRTEI);
    for (i = 0; i < notdone; i++) {
      ElementsOnNodeRow->data[i] = 0.0;
    }

    nri = 0U;
    b_i2 = 0.0;
    i = (int32_T)numel;
    if (0 <= (int32_T)numel - 1) {
      if (1.0 > nen) {
        nume = 0;
      } else {
        if (1 > NodesOnElementPBC->size[1]) {
          emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementPBC->size[1],
            &qb_emlrtBCI, sp);
        }

        if (loop_ub > NodesOnElementPBC->size[1]) {
          emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1, NodesOnElementPBC->
            size[1], &rb_emlrtBCI, sp);
        }

        nume = loop_ub;
      }
    }

    if (0 <= i - 1) {
      b_nodeC2_size[0] = 1;
      b_nodeC2_size[1] = nume;
    }

    for (elem = 0; elem < i; elem++) {
      i1 = elem + 1;
      if (i1 > NodesOnElementPBC->size[0]) {
        emlrtDynamicBoundsCheckR2012b(i1, 1, NodesOnElementPBC->size[0],
          &pb_emlrtBCI, sp);
      }

      for (i1 = 0; i1 < nume; i1++) {
        nodeC2_data[i1] = NodesOnElementPBC->data[elem + NodesOnElementPBC->
          size[0] * i1];
      }

      nel = intnnz(nodeC2_data, b_nodeC2_size);
      i1 = RegionOnElement->size[0];
      if (elem + 1 > i1) {
        emlrtDynamicBoundsCheckR2012b(elem + 1, 1, i1, &xj_emlrtBCI, sp);
      }

      numA = RegionOnElement->data[elem];
      for (locN = 0; locN < nel; locN++) {
        /*  Loop over local Nodes */
        if (elem + 1 > NodesOnElementPBC->size[0]) {
          emlrtDynamicBoundsCheckR2012b(elem + 1, 1, NodesOnElementPBC->size[0],
            &dk_emlrtBCI, sp);
        }

        if (((int32_T)(locN + 1U) < 1) || ((int32_T)(locN + 1U) >
             NodesOnElementPBC->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)(locN + 1U), 1,
            NodesOnElementPBC->size[1], &dk_emlrtBCI, sp);
        }

        node = NodesOnElementPBC->data[elem + NodesOnElementPBC->size[0] * locN];

        /*    Add element to star list, increment number of elem in star */
        if (node != (int32_T)muDoubleScalarFloor(node)) {
          emlrtIntegerCheckR2012b(node, &hd_emlrtDCI, sp);
        }

        if (((int32_T)node < 1) || ((int32_T)node > ElementsOnNodePBCNum->size[0]))
        {
          emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
            ElementsOnNodePBCNum->size[0], &fk_emlrtBCI, sp);
        }

        if (elem + 1 > ElementsOnNodePBCRow->size[0]) {
          emlrtDynamicBoundsCheckR2012b(elem + 1, 1, ElementsOnNodePBCRow->size
            [0], &gk_emlrtBCI, sp);
        }

        if (((int32_T)(locN + 1U) < 1) || ((int32_T)(locN + 1U) >
             ElementsOnNodePBCRow->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)(locN + 1U), 1,
            ElementsOnNodePBCRow->size[1], &gk_emlrtBCI, sp);
        }

        ElementsOnNodePBCRow->data[elem + ElementsOnNodePBCRow->size[0] * locN] =
          ElementsOnNodePBCNum->data[(int32_T)node - 1] + 1.0;
        if (((int32_T)node < 1) || ((int32_T)node > ElementsOnNodePBCNum->size[0]))
        {
          emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
            ElementsOnNodePBCNum->size[0], &ik_emlrtBCI, sp);
        }

        ElementsOnNodePBCNum->data[(int32_T)node - 1]++;

        /*              ElementsOnNodePBC(locE,node) = elem; */
        /*              ElementsOnNodePBCDup(locE,node) = node; */
        if (((int32_T)node < 1) || ((int32_T)node > NodesOnPBCnum->size[0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)node, 1, NodesOnPBCnum->size[0],
            &mk_emlrtBCI, sp);
        }

        if (NodesOnPBCnum->data[(int32_T)node - 1] > 0.0) {
          if (((int32_T)node < 1) || ((int32_T)node > NodesOnPBC->size[1])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)node, 1, NodesOnPBC->size[1],
              &nk_emlrtBCI, sp);
          }

          b_i1 = NodesOnPBC->data[4 * ((int32_T)node - 1)];
          if ((!(node > b_i1)) && ((!muDoubleScalarIsNaN(node)) ||
               muDoubleScalarIsNaN(b_i1))) {
            b_i1 = node;
          }

          /*  use smallest node number as the zipped node */
          nri++;
          if (((int32_T)nri < 1) || ((int32_T)nri > c_NodeRegInd->size[1])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, c_NodeRegInd->size[1],
              &sb_emlrtBCI, sp);
          }

          i1 = 3 * ((int32_T)nri - 1);
          c_NodeRegInd->data[i1] = b_i1;
          c_NodeRegInd->data[i1 + 1] = numA;
          c_NodeRegInd->data[i1 + 2] = 1.0;

          /*  flag that node is in current material set */
          if (node != b_i1) {
            nri++;

            /*  also add for original edge */
            if (((int32_T)nri < 1) || ((int32_T)nri > c_NodeRegInd->size[1])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, c_NodeRegInd->size
                [1], &tb_emlrtBCI, sp);
            }

            i1 = 3 * ((int32_T)nri - 1);
            c_NodeRegInd->data[i1] = node;
            c_NodeRegInd->data[i1 + 1] = numA;
            c_NodeRegInd->data[i1 + 2] = -1.0;

            /*  flag that node is in current material set */
          }

          if (elem + 1 > NodesOnElementCG->size[0]) {
            emlrtDynamicBoundsCheckR2012b(elem + 1, 1, NodesOnElementCG->size[0],
              &sk_emlrtBCI, sp);
          }

          if (((int32_T)(locN + 1U) < 1) || ((int32_T)(locN + 1U) >
               NodesOnElementCG->size[1])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)(locN + 1U), 1,
              NodesOnElementCG->size[1], &sk_emlrtBCI, sp);
          }

          NodesOnElementCG->data[elem + NodesOnElementCG->size[0] * locN] = b_i1;

          /*    Add element to star list, increment number of elem in star */
          if (b_i1 != (int32_T)muDoubleScalarFloor(b_i1)) {
            emlrtIntegerCheckR2012b(b_i1, &id_emlrtDCI, sp);
          }

          if (((int32_T)b_i1 < 1) || ((int32_T)b_i1 > ElementsOnNodeNum->size[0]))
          {
            emlrtDynamicBoundsCheckR2012b((int32_T)b_i1, 1,
              ElementsOnNodeNum->size[0], &tk_emlrtBCI, sp);
          }

          if (elem + 1 > ElementsOnNodeRow->size[0]) {
            emlrtDynamicBoundsCheckR2012b(elem + 1, 1, ElementsOnNodeRow->size[0],
              &uk_emlrtBCI, sp);
          }

          if (((int32_T)(locN + 1U) < 1) || ((int32_T)(locN + 1U) >
               ElementsOnNodeRow->size[1])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)(locN + 1U), 1,
              ElementsOnNodeRow->size[1], &uk_emlrtBCI, sp);
          }

          ElementsOnNodeRow->data[elem + ElementsOnNodeRow->size[0] * locN] =
            ElementsOnNodeNum->data[(int32_T)b_i1 - 1] + 1.0;
          if (((int32_T)b_i1 < 1) || ((int32_T)b_i1 > ElementsOnNodeNum->size[0]))
          {
            emlrtDynamicBoundsCheckR2012b((int32_T)b_i1, 1,
              ElementsOnNodeNum->size[0], &vk_emlrtBCI, sp);
          }

          ElementsOnNodeNum->data[(int32_T)b_i1 - 1]++;

          /*                  ElementsOnNode(locE,nodePBC) = elem; */
          /*                  ElementsOnNodeDup(locE,nodePBC) = nodePBC; */
        } else {
          nri++;
          if (((int32_T)nri < 1) || ((int32_T)nri > c_NodeRegInd->size[1])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, c_NodeRegInd->size[1],
              &ub_emlrtBCI, sp);
          }

          i1 = 3 * ((int32_T)nri - 1);
          c_NodeRegInd->data[i1] = node;
          c_NodeRegInd->data[i1 + 1] = numA;
          c_NodeRegInd->data[i1 + 2] = 1.0;

          /*  flag that node is in current material set */
          /*    Add element to star list, increment number of elem in star */
          if (((int32_T)node < 1) || ((int32_T)node > ElementsOnNodeNum->size[0]))
          {
            emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
              ElementsOnNodeNum->size[0], &ok_emlrtBCI, sp);
          }

          if (elem + 1 > ElementsOnNodeRow->size[0]) {
            emlrtDynamicBoundsCheckR2012b(elem + 1, 1, ElementsOnNodeRow->size[0],
              &pk_emlrtBCI, sp);
          }

          if (((int32_T)(locN + 1U) < 1) || ((int32_T)(locN + 1U) >
               ElementsOnNodeRow->size[1])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)(locN + 1U), 1,
              ElementsOnNodeRow->size[1], &pk_emlrtBCI, sp);
          }

          ElementsOnNodeRow->data[elem + ElementsOnNodeRow->size[0] * locN] =
            ElementsOnNodeNum->data[(int32_T)node - 1] + 1.0;
          if (((int32_T)node < 1) || ((int32_T)node > ElementsOnNodeNum->size[0]))
          {
            emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
              ElementsOnNodeNum->size[0], &qk_emlrtBCI, sp);
          }

          ElementsOnNodeNum->data[(int32_T)node - 1]++;

          /*                  ElementsOnNode(locE,nodePBC) = elem; */
          /*                  ElementsOnNodeDup(locE,nodePBC) = nodePBC; */
        }

        if (*emlrtBreakCheckR2012bFlagVar != 0) {
          emlrtBreakCheckR2012b(sp);
        }
      }

      i1 = loop_ub - nel;
      emlrtForLoopVectorCheckR2012b((real_T)nel + 1.0, 1.0, nen, mxDOUBLE_CLASS,
        i1, &pb_emlrtRTEI, sp);
      for (locN = 0; locN < i1; locN++) {
        numA = ((real_T)nel + 1.0) + (real_T)locN;
        b_i2++;
        if (elem + 1 > ElementsOnNodeRow->size[0]) {
          emlrtDynamicBoundsCheckR2012b(elem + 1, 1, ElementsOnNodeRow->size[0],
            &kk_emlrtBCI, sp);
        }

        if (((int32_T)numA < 1) || ((int32_T)numA > ElementsOnNodeRow->size[1]))
        {
          emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1,
            ElementsOnNodeRow->size[1], &kk_emlrtBCI, sp);
        }

        ElementsOnNodeRow->data[elem + ElementsOnNodeRow->size[0] * ((int32_T)
          numA - 1)] = b_i2;
        if (elem + 1 > ElementsOnNodePBCRow->size[0]) {
          emlrtDynamicBoundsCheckR2012b(elem + 1, 1, ElementsOnNodePBCRow->size
            [0], &lk_emlrtBCI, sp);
        }

        if (((int32_T)numA < 1) || ((int32_T)numA > ElementsOnNodePBCRow->size[1]))
        {
          emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1,
            ElementsOnNodePBCRow->size[1], &lk_emlrtBCI, sp);
        }

        ElementsOnNodePBCRow->data[elem + ElementsOnNodePBCRow->size[0] *
          ((int32_T)numA - 1)] = b_i2;
        if (*emlrtBreakCheckR2012bFlagVar != 0) {
          emlrtBreakCheckR2012b(sp);
        }
      }

      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    end = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1] - 1;
    trueCount = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElementPBC->data[b_i] == 0.0) {
        trueCount++;
      }
    }

    i = ib->size[0];
    ib->size[0] = trueCount;
    emxEnsureCapacity_int32_T(sp, ib, i, &sf_emlrtRTEI);
    nelA = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElementPBC->data[b_i] == 0.0) {
        ib->data[nelA] = b_i + 1;
        nelA++;
      }
    }

    nel = ib->size[0] - 1;
    notdone = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1];
    for (i = 0; i <= nel; i++) {
      if ((ib->data[i] < 1) || (ib->data[i] > notdone)) {
        emlrtDynamicBoundsCheckR2012b(ib->data[i], 1, notdone, &hk_emlrtBCI, sp);
      }

      NodesOnElementPBC->data[ib->data[i] - 1] = *numnp + 1.0;
    }

    /* move empty numbers to end of array */
    end = NodesOnElementCG->size[0] * NodesOnElementCG->size[1] - 1;
    trueCount = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElementCG->data[b_i] == 0.0) {
        trueCount++;
      }
    }

    i = r->size[0];
    r->size[0] = trueCount;
    emxEnsureCapacity_int32_T(sp, r, i, &sf_emlrtRTEI);
    nelA = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElementCG->data[b_i] == 0.0) {
        r->data[nelA] = b_i + 1;
        nelA++;
      }
    }

    nel = r->size[0] - 1;
    notdone = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
    for (i = 0; i <= nel; i++) {
      if ((r->data[i] < 1) || (r->data[i] > notdone)) {
        emlrtDynamicBoundsCheckR2012b(r->data[i], 1, notdone, &rk_emlrtBCI, sp);
      }

      NodesOnElementCG->data[r->data[i] - 1] = *numnp + 1.0;
    }

    /* move empty numbers to end of array */
    if (numel < 1.0) {
      setAll->size[0] = 1;
      setAll->size[1] = 0;
    } else {
      i = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      setAll->size[1] = (int32_T)muDoubleScalarFloor(numel - 1.0) + 1;
      emxEnsureCapacity_real_T(sp, setAll, i, &gg_emlrtRTEI);
      nel = (int32_T)muDoubleScalarFloor(numel - 1.0);
      for (i = 0; i <= nel; i++) {
        setAll->data[i] = (real_T)i + 1.0;
      }
    }

    i = MorePBC->size[0];
    MorePBC->size[0] = setAll->size[1];
    emxEnsureCapacity_real_T(sp, MorePBC, i, &ig_emlrtRTEI);
    nel = setAll->size[1];
    for (i = 0; i < nel; i++) {
      MorePBC->data[i] = setAll->data[i];
    }

    emxInit_real_T(sp, &c_MorePBC, 2, &dg_emlrtRTEI, true);
    i = c_MorePBC->size[0] * c_MorePBC->size[1];
    c_MorePBC->size[0] = MorePBC->size[0];
    c_MorePBC->size[1] = loop_ub;
    emxEnsureCapacity_real_T(sp, c_MorePBC, i, &dg_emlrtRTEI);
    for (i = 0; i < loop_ub; i++) {
      nel = MorePBC->size[0];
      for (i1 = 0; i1 < nel; i1++) {
        c_MorePBC->data[i1 + c_MorePBC->size[0] * i] = MorePBC->data[i1];
      }
    }

    st.site = &sd_emlrtRSI;
    sparse(&st, ElementsOnNodeRow, NodesOnElementCG, c_MorePBC, ElementsOnNode);

    /* zeros(maxel,numnp); % Array of elements connected to nodes */
    st.site = &rd_emlrtRSI;
    sparse(&st, ElementsOnNodeRow, NodesOnElementCG, NodesOnElementCG,
           ElementsOnNodeDup);

    /*  actual node ID given to element for an original node; general counterpart of NodeMat */
    i = c_MorePBC->size[0] * c_MorePBC->size[1];
    c_MorePBC->size[0] = MorePBC->size[0];
    c_MorePBC->size[1] = loop_ub;
    emxEnsureCapacity_real_T(sp, c_MorePBC, i, &dg_emlrtRTEI);
    for (i = 0; i < loop_ub; i++) {
      nel = MorePBC->size[0];
      for (i1 = 0; i1 < nel; i1++) {
        c_MorePBC->data[i1 + c_MorePBC->size[0] * i] = MorePBC->data[i1];
      }
    }

    emxInitStruct_sparse1(sp, &kp_emlrtRSI, &sf_emlrtRTEI, true);
    st.site = &qd_emlrtRSI;
    sparse(&st, ElementsOnNodePBCRow, NodesOnElementPBC, c_MorePBC, &expl_temp);
    locN = expl_temp.m;
    ElementsOnNodePBC_n = expl_temp.n;

    /* zeros(maxel,numnp); % Array of elements connected to nodes */
    st.site = &pd_emlrtRSI;
    sparse(&st, ElementsOnNodePBCRow, NodesOnElementPBC, NodesOnElementPBC,
           &kp_emlrtRSI);

    /*  actual node ID given to element for an original node; general counterpart of NodeMat */
    emxFreeStruct_sparse1(&kp_emlrtRSI);
    emxFree_real_T(&c_MorePBC);
    if (1 > (int32_T)nri) {
      loop_ub = 0;
      nel = 0;
      nume = 0;
    } else {
      if (1 > c_NodeRegInd->size[1]) {
        emlrtDynamicBoundsCheckR2012b(1, 1, c_NodeRegInd->size[1], &vb_emlrtBCI,
          sp);
      }

      if (((int32_T)nri < 1) || ((int32_T)nri > c_NodeRegInd->size[1])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, c_NodeRegInd->size[1],
          &wb_emlrtBCI, sp);
      }

      loop_ub = (int32_T)nri;
      if (1 > c_NodeRegInd->size[1]) {
        emlrtDynamicBoundsCheckR2012b(1, 1, c_NodeRegInd->size[1], &xb_emlrtBCI,
          sp);
      }

      if (((int32_T)nri < 1) || ((int32_T)nri > c_NodeRegInd->size[1])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, c_NodeRegInd->size[1],
          &yb_emlrtBCI, sp);
      }

      nel = (int32_T)nri;
      if (1 > c_NodeRegInd->size[1]) {
        emlrtDynamicBoundsCheckR2012b(1, 1, c_NodeRegInd->size[1], &ac_emlrtBCI,
          sp);
      }

      if (((int32_T)nri < 1) || ((int32_T)nri > c_NodeRegInd->size[1])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, c_NodeRegInd->size[1],
          &bc_emlrtBCI, sp);
      }

      nume = (int32_T)nri;
    }

    i = setPBC->size[0] * setPBC->size[1];
    setPBC->size[0] = 1;
    setPBC->size[1] = loop_ub;
    emxEnsureCapacity_real_T(sp, setPBC, i, &ng_emlrtRTEI);
    for (i = 0; i < loop_ub; i++) {
      setPBC->data[i] = c_NodeRegInd->data[3 * i];
    }

    i = setAll->size[0] * setAll->size[1];
    setAll->size[0] = 1;
    setAll->size[1] = nel;
    emxEnsureCapacity_real_T(sp, setAll, i, &og_emlrtRTEI);
    for (i = 0; i < nel; i++) {
      setAll->data[i] = c_NodeRegInd->data[3 * i + 1];
    }

    i = NodeRegInd->size[0] * NodeRegInd->size[1];
    NodeRegInd->size[0] = 1;
    NodeRegInd->size[1] = nume;
    emxEnsureCapacity_real_T(sp, NodeRegInd, i, &pg_emlrtRTEI);
    for (i = 0; i < nume; i++) {
      NodeRegInd->data[i] = c_NodeRegInd->data[3 * i + 2];
    }

    emxFree_real_T(&c_NodeRegInd);
    st.site = &od_emlrtRSI;
    b_sparse(&st, setPBC, setAll, NodeRegInd, NodeReg);

    /*  allow allocation of PBC nodes too, but with negatives so as not to be in facet counts */
    st.site = &nd_emlrtRSI;
    b_st.site = &kj_emlrtRSI;
    e_eml_find(&b_st, NodeReg->d, NodeReg->colidx, NodeReg->rowidx, NodeReg->m,
               NodeReg->n, ii, c_ii);
    i = setnPBC->size[0];
    setnPBC->size[0] = ii->size[0];
    emxEnsureCapacity_real_T(&st, setnPBC, i, &kg_emlrtRTEI);
    loop_ub = ii->size[0];
    for (i = 0; i < loop_ub; i++) {
      setnPBC->data[i] = ii->data[i];
    }

    i = MorePBC->size[0];
    MorePBC->size[0] = c_ii->size[0];
    emxEnsureCapacity_real_T(&st, MorePBC, i, &lg_emlrtRTEI);
    loop_ub = c_ii->size[0];
    for (i = 0; i < loop_ub; i++) {
      MorePBC->data[i] = c_ii->data[i];
    }

    /*  sparse adds together entries when duplicated, so reduce back to original magnitude */
    st.site = &md_emlrtRSI;
    b_st.site = &uh_emlrtRSI;
    c_st.site = &vh_emlrtRSI;
    d_st.site = &wh_emlrtRSI;
    if (setnPBC->size[0] < 1) {
      emlrtErrorWithMessageIdR2018a(&d_st, &nb_emlrtRTEI,
        "Coder:toolbox:eml_min_or_max_varDimZero",
        "Coder:toolbox:eml_min_or_max_varDimZero", 0);
    }

    e_st.site = &rj_emlrtRSI;
    f_st.site = &sj_emlrtRSI;
    notdone = setnPBC->size[0];
    if (setnPBC->size[0] <= 2) {
      if (setnPBC->size[0] == 1) {
        nelA = (int32_T)setnPBC->data[0];
      } else if (setnPBC->data[0] < setnPBC->data[1]) {
        nelA = (int32_T)setnPBC->data[1];
      } else {
        nelA = (int32_T)setnPBC->data[0];
      }
    } else {
      g_st.site = &uj_emlrtRSI;
      g_st.site = &tj_emlrtRSI;
      nelA = (int32_T)setnPBC->data[0];
      h_st.site = &hi_emlrtRSI;
      if (setnPBC->size[0] > 2147483646) {
        i_st.site = &ff_emlrtRSI;
        check_forloop_overflow_error(&i_st);
      }

      for (numPBC = 2; numPBC <= notdone; numPBC++) {
        i = (int32_T)setnPBC->data[numPBC - 1];
        if (nelA < i) {
          nelA = i;
        }
      }
    }

    st.site = &md_emlrtRSI;
    if (MorePBC->size[0] < 1) {
      emlrtDynamicBoundsCheckR2012b(MorePBC->size[0], 1, MorePBC->size[0],
        &cc_emlrtBCI, &st);
    }

    tmp_size[0] = nelA;
    tmp_size[1] = (int32_T)MorePBC->data[MorePBC->size[0] - 1];
    b_st.site = &vj_emlrtRSI;
    eml_sub2ind(&b_st, tmp_size, setnPBC, MorePBC, ib);
    i = MorePBC->size[0];
    MorePBC->size[0] = ib->size[0];
    emxEnsureCapacity_real_T(&st, MorePBC, i, &tg_emlrtRTEI);
    loop_ub = ib->size[0];
    for (i = 0; i < loop_ub; i++) {
      MorePBC->data[i] = ib->data[i];
    }

    st.site = &ld_emlrtRSI;
    sparse_parenReference(&st, NodeReg->d, NodeReg->colidx, NodeReg->rowidx,
                          NodeReg->m, NodeReg->n, MorePBC, &NodeRegSum);
    st.site = &ld_emlrtRSI;
    b_sign(&st, &NodeRegSum);
    st.site = &ld_emlrtRSI;
    sparse_times(&st, setnPBC, NodeRegSum.d, NodeRegSum.colidx,
                 NodeRegSum.rowidx, NodeRegSum.m, b_expl_temp.d,
                 b_expl_temp.colidx, b_expl_temp.rowidx, &notdone);
    st.site = &ld_emlrtRSI;
    sparse_parenAssign(&st, NodeReg, b_expl_temp.d, b_expl_temp.colidx,
                       b_expl_temp.rowidx, notdone, MorePBC);

    /* keep the negative sign for PBC nodes to ignore below */
    end = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1] - 1;
    trueCount = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElementPBC->data[b_i] == *numnp + 1.0) {
        trueCount++;
      }
    }

    emxInit_int32_T(sp, &r1, 1, &xj_emlrtRTEI, true);
    i = r1->size[0];
    r1->size[0] = trueCount;
    emxEnsureCapacity_int32_T(sp, r1, i, &sf_emlrtRTEI);
    nelA = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElementPBC->data[b_i] == *numnp + 1.0) {
        r1->data[nelA] = b_i + 1;
        nelA++;
      }
    }

    loop_ub = r1->size[0] - 1;
    notdone = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1];
    for (i = 0; i <= loop_ub; i++) {
      if ((r1->data[i] < 1) || (r1->data[i] > notdone)) {
        emlrtDynamicBoundsCheckR2012b(r1->data[i], 1, notdone, &xk_emlrtBCI, sp);
      }

      NodesOnElementPBC->data[r1->data[i] - 1] = 0.0;
    }

    emxFree_int32_T(&r1);

    /* move empty numbers to end of array */
    end = NodesOnElementCG->size[0] * NodesOnElementCG->size[1] - 1;
    trueCount = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElementCG->data[b_i] == *numnp + 1.0) {
        trueCount++;
      }
    }

    emxInit_int32_T(sp, &r2, 1, &yj_emlrtRTEI, true);
    i = r2->size[0];
    r2->size[0] = trueCount;
    emxEnsureCapacity_int32_T(sp, r2, i, &sf_emlrtRTEI);
    nelA = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElementCG->data[b_i] == *numnp + 1.0) {
        r2->data[nelA] = b_i + 1;
        nelA++;
      }
    }

    loop_ub = r2->size[0] - 1;
    notdone = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
    for (i = 0; i <= loop_ub; i++) {
      if ((r2->data[i] < 1) || (r2->data[i] > notdone)) {
        emlrtDynamicBoundsCheckR2012b(r2->data[i], 1, notdone, &yk_emlrtBCI, sp);
      }

      NodesOnElementCG->data[r2->data[i] - 1] = 0.0;
    }

    emxFree_int32_T(&r2);

    /* move empty numbers to end of array */
    /*      % get memory back */
    /*      clear NodeRegInd */
    /*      clear ElementsOnNodeRow */
    /*      clear ElementsOnNodePBCRow */
  } else {
    emxInit_real_T(sp, &b_NodeRegInd, 2, &qf_emlrtRTEI, true);
    i = b_NodeRegInd->size[0] * b_NodeRegInd->size[1];
    b_NodeRegInd->size[0] = 2;
    emxEnsureCapacity_real_T(sp, b_NodeRegInd, i, &pf_emlrtRTEI);
    d = numel * 9.0;
    if (!(d >= 0.0)) {
      emlrtNonNegativeCheckR2012b(d, &lb_emlrtDCI, sp);
    }

    d1 = (int32_T)muDoubleScalarFloor(d);
    if (d != d1) {
      emlrtIntegerCheckR2012b(d, &kb_emlrtDCI, sp);
    }

    i = b_NodeRegInd->size[0] * b_NodeRegInd->size[1];
    b_NodeRegInd->size[1] = (int32_T)d;
    emxEnsureCapacity_real_T(sp, b_NodeRegInd, i, &pf_emlrtRTEI);
    if (d != d1) {
      emlrtIntegerCheckR2012b(d, &ad_emlrtDCI, sp);
    }

    loop_ub = (int32_T)d << 1;
    for (i = 0; i < loop_ub; i++) {
      b_NodeRegInd->data[i] = 0.0;
    }

    if (!(numel >= 0.0)) {
      emlrtNonNegativeCheckR2012b(numel, &nb_emlrtDCI, sp);
    }

    d = muDoubleScalarFloor(numel);
    if (numel != d) {
      emlrtIntegerCheckR2012b(numel, &mb_emlrtDCI, sp);
    }

    i = ElementsOnNodeRow->size[0] * ElementsOnNodeRow->size[1];
    ElementsOnNodeRow->size[0] = (int32_T)numel;
    loop_ub = (int32_T)nen;
    ElementsOnNodeRow->size[1] = loop_ub;
    emxEnsureCapacity_real_T(sp, ElementsOnNodeRow, i, &rf_emlrtRTEI);
    if (numel != d) {
      emlrtIntegerCheckR2012b(numel, &dd_emlrtDCI, sp);
    }

    notdone = (int32_T)numel * loop_ub;
    for (i = 0; i < notdone; i++) {
      ElementsOnNodeRow->data[i] = 0.0;
    }

    /*      ElementsOnNodeCol = zeros(nen,numel); */
    nri = 0U;
    b_i2 = 0.0;
    i = (int32_T)numel;
    if (0 <= (int32_T)numel - 1) {
      if (1.0 > nen) {
        nume = 0;
      } else {
        i1 = NodesOnElement->size[1];
        if (1 > i1) {
          emlrtDynamicBoundsCheckR2012b(1, 1, i1, &ec_emlrtBCI, sp);
        }

        i1 = NodesOnElement->size[1];
        if (loop_ub > i1) {
          emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1, i1, &fc_emlrtBCI, sp);
        }

        nume = loop_ub;
      }
    }

    if (0 <= i - 1) {
      b_nodeC2_size[0] = 1;
      b_nodeC2_size[1] = nume;
    }

    for (elem = 0; elem < i; elem++) {
      i1 = NodesOnElement->size[0];
      i2 = elem + 1;
      if (i2 > i1) {
        emlrtDynamicBoundsCheckR2012b(i2, 1, i1, &dc_emlrtBCI, sp);
      }

      for (i1 = 0; i1 < nume; i1++) {
        nodeC2_data[i1] = NodesOnElement->data[elem + NodesOnElement->size[0] *
          i1];
      }

      nel = intnnz(nodeC2_data, b_nodeC2_size);
      i1 = RegionOnElement->size[0];
      if (i2 > i1) {
        emlrtDynamicBoundsCheckR2012b(i2, 1, i1, &gc_emlrtBCI, sp);
      }

      for (locN = 0; locN < nel; locN++) {
        /*  Loop over local Nodes */
        i1 = NodesOnElement->size[0];
        if (elem + 1 > i1) {
          emlrtDynamicBoundsCheckR2012b(elem + 1, 1, i1, &wj_emlrtBCI, sp);
        }

        i1 = NodesOnElement->size[1];
        if (((int32_T)(locN + 1U) < 1) || ((int32_T)(locN + 1U) > i1)) {
          emlrtDynamicBoundsCheckR2012b((int32_T)(locN + 1U), 1, i1,
            &wj_emlrtBCI, sp);
        }

        node = NodesOnElement->data[elem + NodesOnElement->size[0] * locN];
        nri++;
        if (((int32_T)nri < 1) || ((int32_T)nri > b_NodeRegInd->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, b_NodeRegInd->size[1],
            &hc_emlrtBCI, sp);
        }

        b_NodeRegInd->data[2 * ((int32_T)nri - 1)] = node;
        b_NodeRegInd->data[2 * ((int32_T)nri - 1) + 1] = RegionOnElement->
          data[elem];

        /*              NodeReg(node,reg) = node; % flag that node is in current material set */
        /*    Add element to star list, increment number of elem in star */
        if (node != (int32_T)muDoubleScalarFloor(node)) {
          emlrtIntegerCheckR2012b(node, &gd_emlrtDCI, sp);
        }

        if (((int32_T)node < 1) || ((int32_T)node > ElementsOnNodeNum->size[0]))
        {
          emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
            ElementsOnNodeNum->size[0], &bk_emlrtBCI, sp);
        }

        if (elem + 1 > ElementsOnNodeRow->size[0]) {
          emlrtDynamicBoundsCheckR2012b(elem + 1, 1, ElementsOnNodeRow->size[0],
            &ck_emlrtBCI, sp);
        }

        if (((int32_T)(locN + 1U) < 1) || ((int32_T)(locN + 1U) >
             ElementsOnNodeRow->size[1])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)(locN + 1U), 1,
            ElementsOnNodeRow->size[1], &ck_emlrtBCI, sp);
        }

        ElementsOnNodeRow->data[elem + ElementsOnNodeRow->size[0] * locN] =
          ElementsOnNodeNum->data[(int32_T)node - 1] + 1.0;

        /*              ElementsOnNode(locE,node) = elem; */
        if (((int32_T)node < 1) || ((int32_T)node > ElementsOnNodeNum->size[0]))
        {
          emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
            ElementsOnNodeNum->size[0], &ek_emlrtBCI, sp);
        }

        ElementsOnNodeNum->data[(int32_T)node - 1]++;

        /*              ElementsOnNodeCol(locN,elem) = node; */
        /*              ElementsOnNodeDup(locE,node) = node; */
        if (*emlrtBreakCheckR2012bFlagVar != 0) {
          emlrtBreakCheckR2012b(sp);
        }
      }

      i1 = loop_ub - nel;
      emlrtForLoopVectorCheckR2012b((real_T)nel + 1.0, 1.0, nen, mxDOUBLE_CLASS,
        i1, &qb_emlrtRTEI, sp);
      for (locN = 0; locN < i1; locN++) {
        b_i2++;
        if (elem + 1 > ElementsOnNodeRow->size[0]) {
          emlrtDynamicBoundsCheckR2012b(elem + 1, 1, ElementsOnNodeRow->size[0],
            &ak_emlrtBCI, sp);
        }

        i2 = (int32_T)(((real_T)nel + 1.0) + (real_T)locN);
        if ((i2 < 1) || (i2 > ElementsOnNodeRow->size[1])) {
          emlrtDynamicBoundsCheckR2012b(i2, 1, ElementsOnNodeRow->size[1],
            &ak_emlrtBCI, sp);
        }

        ElementsOnNodeRow->data[elem + ElementsOnNodeRow->size[0] * (i2 - 1)] =
          b_i2;
        if (*emlrtBreakCheckR2012bFlagVar != 0) {
          emlrtBreakCheckR2012b(sp);
        }
      }

      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    end = NodesOnElement->size[0] * NodesOnElement->size[1] - 1;
    trueCount = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElement->data[b_i] == 0.0) {
        trueCount++;
      }
    }

    i = ib->size[0];
    ib->size[0] = trueCount;
    emxEnsureCapacity_int32_T(sp, ib, i, &sf_emlrtRTEI);
    nelA = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElement->data[b_i] == 0.0) {
        ib->data[nelA] = b_i + 1;
        nelA++;
      }
    }

    nelA = NodesOnElement->size[0] * NodesOnElement->size[1];
    nel = ib->size[0];
    for (i = 0; i < nel; i++) {
      if ((ib->data[i] < 1) || (ib->data[i] > nelA)) {
        emlrtDynamicBoundsCheckR2012b(ib->data[i], 1, nelA, &yj_emlrtBCI, sp);
      }

      NodesOnElement->data[ib->data[i] - 1] = *numnp + 1.0;
    }

    /* move empty numbers to end of array */
    if (numel < 1.0) {
      setAll->size[0] = 1;
      setAll->size[1] = 0;
    } else {
      i = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      nel = (int32_T)muDoubleScalarFloor(numel - 1.0);
      setAll->size[1] = nel + 1;
      emxEnsureCapacity_real_T(sp, setAll, i, &bg_emlrtRTEI);
      for (i = 0; i <= nel; i++) {
        setAll->data[i] = (real_T)i + 1.0;
      }
    }

    i = ElementsOnNodePBCRow->size[0] * ElementsOnNodePBCRow->size[1];
    ElementsOnNodePBCRow->size[0] = setAll->size[1];
    ElementsOnNodePBCRow->size[1] = loop_ub;
    emxEnsureCapacity_real_T(sp, ElementsOnNodePBCRow, i, &dg_emlrtRTEI);
    for (i = 0; i < loop_ub; i++) {
      nel = setAll->size[1];
      for (i1 = 0; i1 < nel; i1++) {
        ElementsOnNodePBCRow->data[i1 + ElementsOnNodePBCRow->size[0] * i] =
          setAll->data[i1];
      }
    }

    st.site = &kd_emlrtRSI;
    sparse(&st, ElementsOnNodeRow, NodesOnElement, ElementsOnNodePBCRow,
           ElementsOnNode);

    /* zeros(maxel,numnp); % Array of elements connected to nodes */
    st.site = &jd_emlrtRSI;
    sparse(&st, ElementsOnNodeRow, NodesOnElement, NodesOnElement,
           ElementsOnNodeDup);

    /*  actual node ID given to element for an original node; general counterpart of NodeMat */
    if (1 > (int32_T)nri) {
      nel = 0;
      nume = 0;
      numPBC = 0;
    } else {
      if (1 > b_NodeRegInd->size[1]) {
        emlrtDynamicBoundsCheckR2012b(1, 1, b_NodeRegInd->size[1], &ic_emlrtBCI,
          sp);
      }

      if (((int32_T)nri < 1) || ((int32_T)nri > b_NodeRegInd->size[1])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, b_NodeRegInd->size[1],
          &jc_emlrtBCI, sp);
      }

      nel = (int32_T)nri;
      if (1 > b_NodeRegInd->size[1]) {
        emlrtDynamicBoundsCheckR2012b(1, 1, b_NodeRegInd->size[1], &kc_emlrtBCI,
          sp);
      }

      if (((int32_T)nri < 1) || ((int32_T)nri > b_NodeRegInd->size[1])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, b_NodeRegInd->size[1],
          &lc_emlrtBCI, sp);
      }

      nume = (int32_T)nri;
      if (1 > b_NodeRegInd->size[1]) {
        emlrtDynamicBoundsCheckR2012b(1, 1, b_NodeRegInd->size[1], &mc_emlrtBCI,
          sp);
      }

      if (((int32_T)nri < 1) || ((int32_T)nri > b_NodeRegInd->size[1])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, b_NodeRegInd->size[1],
          &nc_emlrtBCI, sp);
      }

      numPBC = (int32_T)nri;
    }

    i = setPBC->size[0] * setPBC->size[1];
    setPBC->size[0] = 1;
    setPBC->size[1] = nel;
    emxEnsureCapacity_real_T(sp, setPBC, i, &fg_emlrtRTEI);
    for (i = 0; i < nel; i++) {
      setPBC->data[i] = b_NodeRegInd->data[2 * i];
    }

    i = setAll->size[0] * setAll->size[1];
    setAll->size[0] = 1;
    setAll->size[1] = nume;
    emxEnsureCapacity_real_T(sp, setAll, i, &hg_emlrtRTEI);
    for (i = 0; i < nume; i++) {
      setAll->data[i] = b_NodeRegInd->data[2 * i + 1];
    }

    i = NodeRegInd->size[0] * NodeRegInd->size[1];
    NodeRegInd->size[0] = 1;
    NodeRegInd->size[1] = numPBC;
    emxEnsureCapacity_real_T(sp, NodeRegInd, i, &jg_emlrtRTEI);
    for (i = 0; i < numPBC; i++) {
      NodeRegInd->data[i] = b_NodeRegInd->data[2 * i];
    }

    emxFree_real_T(&b_NodeRegInd);
    st.site = &id_emlrtRSI;
    b_sparse(&st, setPBC, setAll, NodeRegInd, NodeReg);
    st.site = &hd_emlrtRSI;
    b_st.site = &kj_emlrtRSI;
    e_eml_find(&b_st, NodeReg->d, NodeReg->colidx, NodeReg->rowidx, NodeReg->m,
               NodeReg->n, ii, c_ii);
    i = setnPBC->size[0];
    setnPBC->size[0] = ii->size[0];
    emxEnsureCapacity_real_T(&st, setnPBC, i, &kg_emlrtRTEI);
    nel = ii->size[0];
    for (i = 0; i < nel; i++) {
      setnPBC->data[i] = ii->data[i];
    }

    i = MorePBC->size[0];
    MorePBC->size[0] = c_ii->size[0];
    emxEnsureCapacity_real_T(&st, MorePBC, i, &lg_emlrtRTEI);
    nel = c_ii->size[0];
    for (i = 0; i < nel; i++) {
      MorePBC->data[i] = c_ii->data[i];
    }

    st.site = &gd_emlrtRSI;
    if (MorePBC->size[0] < 1) {
      emlrtDynamicBoundsCheckR2012b(MorePBC->size[0], 1, MorePBC->size[0],
        &oc_emlrtBCI, &st);
    }

    tmp_size[0] = (int32_T)*numnp;
    tmp_size[1] = (int32_T)MorePBC->data[MorePBC->size[0] - 1];
    b_st.site = &vj_emlrtRSI;
    eml_sub2ind(&b_st, tmp_size, setnPBC, MorePBC, ib);
    i = MorePBC->size[0];
    MorePBC->size[0] = ib->size[0];
    emxEnsureCapacity_real_T(&st, MorePBC, i, &mg_emlrtRTEI);
    nel = ib->size[0];
    for (i = 0; i < nel; i++) {
      MorePBC->data[i] = ib->data[i];
    }

    st.site = &fd_emlrtRSI;
    b_sparse_parenAssign(&st, NodeReg, setnPBC, MorePBC);
    end = NodesOnElement->size[0] * NodesOnElement->size[1] - 1;
    trueCount = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElement->data[b_i] == *numnp + 1.0) {
        trueCount++;
      }
    }

    i = r->size[0];
    r->size[0] = trueCount;
    emxEnsureCapacity_int32_T(sp, r, i, &sf_emlrtRTEI);
    nelA = 0;
    for (b_i = 0; b_i <= end; b_i++) {
      if (NodesOnElement->data[b_i] == *numnp + 1.0) {
        r->data[nelA] = b_i + 1;
        nelA++;
      }
    }

    nelA = NodesOnElement->size[0] * NodesOnElement->size[1];
    nel = r->size[0];
    for (i = 0; i < nel; i++) {
      if ((r->data[i] < 1) || (r->data[i] > nelA)) {
        emlrtDynamicBoundsCheckR2012b(r->data[i], 1, nelA, &wk_emlrtBCI, sp);
      }

      NodesOnElement->data[r->data[i] - 1] = 0.0;
    }

    /* move empty numbers to end of array */
    i = ElementsOnNodePBCRow->size[0] * ElementsOnNodePBCRow->size[1];
    ElementsOnNodePBCRow->size[0] = (int32_T)numel;
    ElementsOnNodePBCRow->size[1] = loop_ub;
    emxEnsureCapacity_real_T(sp, ElementsOnNodePBCRow, i, &qg_emlrtRTEI);
    for (i = 0; i < notdone; i++) {
      ElementsOnNodePBCRow->data[i] = 0.0;
    }

    st.site = &ed_emlrtRSI;
    c_sparse(&st, ElementsOnNodePBCRow, expl_temp.d, expl_temp.colidx,
             expl_temp.rowidx, &locN, &ElementsOnNodePBC_n, &notdone);

    /*      % get memory back */
    /*      clear NodeRegInd */
    /*      clear ElementsOnNodeRow */
  }

  emxFree_real_T(&ElementsOnNodeRow);
  emxFree_real_T(&ElementsOnNodePBCRow);
  i = NodesOnElementDG->size[0] * NodesOnElementDG->size[1];
  NodesOnElementDG->size[0] = NodesOnElementCG->size[0];
  NodesOnElementDG->size[1] = NodesOnElementCG->size[1];
  emxEnsureCapacity_real_T(sp, NodesOnElementDG, i, &rg_emlrtRTEI);
  loop_ub = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
  for (i = 0; i < loop_ub; i++) {
    NodesOnElementDG->data[i] = NodesOnElementCG->data[i];
  }

  if (1.0 > *numnp) {
    loop_ub = 0;
  } else {
    i = Coordinates->size[0];
    if (1 > i) {
      emlrtDynamicBoundsCheckR2012b(1, 1, i, &pc_emlrtBCI, sp);
    }

    i = Coordinates->size[0];
    loop_ub = (int32_T)*numnp;
    if ((loop_ub < 1) || (loop_ub > i)) {
      emlrtDynamicBoundsCheckR2012b(loop_ub, 1, i, &qc_emlrtBCI, sp);
    }
  }

  if (1.0 > *numnp) {
    i1 = 0;
  } else {
    i = (int32_T)numel * (int32_T)nen;
    if (1 > i) {
      emlrtDynamicBoundsCheckR2012b(1, 1, i, &ii_emlrtBCI, sp);
    }

    i1 = (int32_T)*numnp;
    if ((i1 < 1) || (i1 > i)) {
      emlrtDynamicBoundsCheckR2012b(i1, 1, i, &ji_emlrtBCI, sp);
    }
  }

  b_numel[0] = i1;
  b_numel[1] = 2;
  tmp_size[0] = loop_ub;
  tmp_size[1] = 2;
  emlrtSubAssignSizeCheckR2012b(&b_numel[0], 2, &tmp_size[0], 2, &j_emlrtECI, sp);
  for (i = 0; i < loop_ub; i++) {
    Coordinates3->data[i] = Coordinates->data[i];
  }

  for (i = 0; i < loop_ub; i++) {
    Coordinates3->data[i + Coordinates3->size[0]] = Coordinates->data[i +
      Coordinates->size[0]];
  }

  *numinttype = nummat * (nummat + 1.0) / 2.0;
  if (!(*numinttype >= 0.0)) {
    emlrtNonNegativeCheckR2012b(*numinttype, &kd_emlrtDCI, sp);
  }

  i = (int32_T)muDoubleScalarFloor(*numinttype);
  if (*numinttype != i) {
    emlrtIntegerCheckR2012b(*numinttype, &jd_emlrtDCI, sp);
  }

  i1 = numEonF->size[0];
  numEonF->size[0] = (int32_T)*numinttype;
  emxEnsureCapacity_real_T(sp, numEonF, i1, &sg_emlrtRTEI);
  if (*numinttype != i) {
    emlrtIntegerCheckR2012b(*numinttype, &jd_emlrtDCI, sp);
  }

  loop_ub = (int32_T)*numinttype;
  for (i1 = 0; i1 < loop_ub; i1++) {
    numEonF->data[i1] = 0.0;
  }

  *numEonB = 0.0;
  d = 4.0 * numel;
  d1 = (int32_T)muDoubleScalarFloor(d);
  if (d != d1) {
    emlrtIntegerCheckR2012b(d, &sb_emlrtDCI, sp);
  }

  i1 = ElementsOnFacet->size[0] * ElementsOnFacet->size[1];
  ElementsOnFacet->size[0] = (int32_T)d;
  ElementsOnFacet->size[1] = 4;
  emxEnsureCapacity_real_T(sp, ElementsOnFacet, i1, &ug_emlrtRTEI);
  if (d != d1) {
    emlrtIntegerCheckR2012b(d, &ld_emlrtDCI, sp);
  }

  loop_ub = (int32_T)d << 2;
  for (i1 = 0; i1 < loop_ub; i1++) {
    ElementsOnFacet->data[i1] = 0.0;
  }

  if (d != d1) {
    emlrtIntegerCheckR2012b(d, &md_emlrtDCI, sp);
  }

  i1 = FacetsOnInterface->size[0];
  FacetsOnInterface->size[0] = (int32_T)d;
  emxEnsureCapacity_real_T(sp, FacetsOnInterface, i1, &vg_emlrtRTEI);
  if (d != d1) {
    emlrtIntegerCheckR2012b(d, &md_emlrtDCI, sp);
  }

  loop_ub = (int32_T)d;
  for (i1 = 0; i1 < loop_ub; i1++) {
    FacetsOnInterface->data[i1] = 0.0;
  }

  if (d != d1) {
    emlrtIntegerCheckR2012b(d, &tb_emlrtDCI, sp);
  }

  i1 = ElementsOnBoundary->size[0] * ElementsOnBoundary->size[1];
  ElementsOnBoundary->size[0] = (int32_T)d;
  ElementsOnBoundary->size[1] = 2;
  emxEnsureCapacity_real_T(sp, ElementsOnBoundary, i1, &wg_emlrtRTEI);
  if (d != d1) {
    emlrtIntegerCheckR2012b(d, &nd_emlrtDCI, sp);
  }

  loop_ub = (int32_T)d << 1;
  for (i1 = 0; i1 < loop_ub; i1++) {
    ElementsOnBoundary->data[i1] = 0.0;
  }

  /*  All exposed faces */
  st.site = &dd_emlrtRSI;
  if (muDoubleScalarIsNaN(usePBC)) {
    emlrtErrorWithMessageIdR2018a(&st, &mb_emlrtRTEI, "MATLAB:nologicalnan",
      "MATLAB:nologicalnan", 0);
  }

  if (usePBC != 0.0) {
    if (d != d1) {
      emlrtIntegerCheckR2012b(d, &od_emlrtDCI, sp);
    }

    i1 = FacetsOnPBC->size[0];
    FacetsOnPBC->size[0] = (int32_T)d;
    emxEnsureCapacity_real_T(sp, FacetsOnPBC, i1, &yg_emlrtRTEI);
    if (d != d1) {
      emlrtIntegerCheckR2012b(d, &od_emlrtDCI, sp);
    }

    loop_ub = (int32_T)d;
    for (i1 = 0; i1 < loop_ub; i1++) {
      FacetsOnPBC->data[i1] = 0.0;
    }

    /*  separate list for PBC facets that are found; grouped by interface type */
    if (*numinttype != i) {
      emlrtIntegerCheckR2012b(*numinttype, &pd_emlrtDCI, sp);
    }

    i1 = numEonPBC->size[0];
    loop_ub = (int32_T)*numinttype;
    numEonPBC->size[0] = loop_ub;
    emxEnsureCapacity_real_T(sp, numEonPBC, i1, &bh_emlrtRTEI);
    if (loop_ub != i) {
      emlrtIntegerCheckR2012b(*numinttype, &pd_emlrtDCI, sp);
    }

    for (i = 0; i < loop_ub; i++) {
      numEonPBC->data[i] = 0.0;
    }

    numFPBC = 0.0;

    /*  record how many PBC facets are found */
  } else {
    i = FacetsOnPBC->size[0];
    FacetsOnPBC->size[0] = 4;
    emxEnsureCapacity_real_T(sp, FacetsOnPBC, i, &xg_emlrtRTEI);
    FacetsOnPBC->data[0] = 0.0;
    FacetsOnPBC->data[1] = 0.0;
    FacetsOnPBC->data[2] = 0.0;
    FacetsOnPBC->data[3] = 0.0;

    /*  separate list for PBC facets that are found; grouped by interface type */
    i = numEonPBC->size[0];
    numEonPBC->size[0] = 2;
    emxEnsureCapacity_real_T(sp, numEonPBC, i, &ah_emlrtRTEI);
    numEonPBC->data[0] = 0.0;
    numEonPBC->data[1] = 0.0;
    numFPBC = 0.0;

    /*  record how many PBC facets are found */
  }

  i = FacetsOnElement->size[0] * FacetsOnElement->size[1];
  FacetsOnElement->size[0] = (int32_T)numel;
  FacetsOnElement->size[1] = 4;
  emxEnsureCapacity_real_T(sp, FacetsOnElement, i, &ch_emlrtRTEI);
  notdone = (int32_T)numel << 2;
  for (i = 0; i < notdone; i++) {
    FacetsOnElement->data[i] = 0.0;
  }

  /*  each fac in mesh, gets assigned an fac ID based on its material (1:numSI for that material) */
  i = FacetsOnElementInt->size[0] * FacetsOnElementInt->size[1];
  FacetsOnElementInt->size[0] = (int32_T)numel;
  FacetsOnElementInt->size[1] = 4;
  emxEnsureCapacity_real_T(sp, FacetsOnElementInt, i, &dh_emlrtRTEI);
  for (i = 0; i < notdone; i++) {
    FacetsOnElementInt->data[i] = 0.0;
  }

  /*  materialI ID for each fac in mesh, according to element */
  i = FacetsOnNodeNum->size[0];
  FacetsOnNodeNum->size[0] = (int32_T)*numnp;
  emxEnsureCapacity_real_T(sp, FacetsOnNodeNum, i, &eh_emlrtRTEI);
  loop_ub = (int32_T)*numnp;
  for (i = 0; i < loop_ub; i++) {
    FacetsOnNodeNum->data[i] = 0.0;
  }

  /*  number of facs attached to a node. */
  st.site = &cd_emlrtRSI;
  b_st.site = &uh_emlrtRSI;
  c_st.site = &vh_emlrtRSI;
  d_st.site = &wh_emlrtRSI;
  if (ElementsOnNodeNum->size[0] < 1) {
    emlrtErrorWithMessageIdR2018a(&d_st, &nb_emlrtRTEI,
      "Coder:toolbox:eml_min_or_max_varDimZero",
      "Coder:toolbox:eml_min_or_max_varDimZero", 0);
  }

  e_st.site = &rj_emlrtRSI;
  f_st.site = &sj_emlrtRSI;
  notdone = ElementsOnNodeNum->size[0];
  if (ElementsOnNodeNum->size[0] <= 2) {
    if (ElementsOnNodeNum->size[0] == 1) {
      numA = ElementsOnNodeNum->data[0];
    } else if (ElementsOnNodeNum->data[0] < ElementsOnNodeNum->data[1]) {
      numA = ElementsOnNodeNum->data[1];
    } else {
      numA = ElementsOnNodeNum->data[0];
    }
  } else {
    g_st.site = &uj_emlrtRSI;
    g_st.site = &tj_emlrtRSI;
    numA = ElementsOnNodeNum->data[0];
    h_st.site = &hi_emlrtRSI;
    if (ElementsOnNodeNum->size[0] > 2147483646) {
      i_st.site = &ff_emlrtRSI;
      check_forloop_overflow_error(&i_st);
    }

    for (numPBC = 2; numPBC <= notdone; numPBC++) {
      d = ElementsOnNodeNum->data[numPBC - 1];
      if (numA < d) {
        numA = d;
      }
    }
  }

  emxInit_real_T(&f_st, &FacetsOnNodeInd, 2, &fh_emlrtRTEI, true);
  i = FacetsOnNodeInd->size[0] * FacetsOnNodeInd->size[1];
  FacetsOnNodeInd->size[0] = 5;
  emxEnsureCapacity_real_T(sp, FacetsOnNodeInd, i, &fh_emlrtRTEI);
  d = *numnp * 3.0 * numA;
  if (!(d >= 0.0)) {
    emlrtNonNegativeCheckR2012b(d, &vb_emlrtDCI, sp);
  }

  d1 = (int32_T)muDoubleScalarFloor(d);
  if (d != d1) {
    emlrtIntegerCheckR2012b(d, &ub_emlrtDCI, sp);
  }

  i = FacetsOnNodeInd->size[0] * FacetsOnNodeInd->size[1];
  FacetsOnNodeInd->size[1] = (int32_T)d;
  emxEnsureCapacity_real_T(sp, FacetsOnNodeInd, i, &fh_emlrtRTEI);
  if (d != d1) {
    emlrtIntegerCheckR2012b(d, &qd_emlrtDCI, sp);
  }

  loop_ub = 5 * (int32_T)d;
  for (i = 0; i < loop_ub; i++) {
    FacetsOnNodeInd->data[i] = 0.0;
  }

  /*  # of nodes times 3 faces times max # of elements on any node */
  nri = 0U;
  nloop3[0] = 1;
  nloop3[3] = 2;
  if (*emlrtBreakCheckR2012bFlagVar != 0) {
    emlrtBreakCheckR2012b(sp);
  }

  nloop3[1] = 2;
  nloop3[4] = 3;
  if (*emlrtBreakCheckR2012bFlagVar != 0) {
    emlrtBreakCheckR2012b(sp);
  }

  nloop3[2] = 3;
  if (*emlrtBreakCheckR2012bFlagVar != 0) {
    emlrtBreakCheckR2012b(sp);
  }

  nloop3[5] = 1;
  nloop4[0] = 1;
  nloop4[4] = 2;
  if (*emlrtBreakCheckR2012bFlagVar != 0) {
    emlrtBreakCheckR2012b(sp);
  }

  nloop4[1] = 2;
  nloop4[5] = 3;
  if (*emlrtBreakCheckR2012bFlagVar != 0) {
    emlrtBreakCheckR2012b(sp);
  }

  nloop4[2] = 3;
  nloop4[6] = 4;
  if (*emlrtBreakCheckR2012bFlagVar != 0) {
    emlrtBreakCheckR2012b(sp);
  }

  nloop4[3] = 4;
  if (*emlrtBreakCheckR2012bFlagVar != 0) {
    emlrtBreakCheckR2012b(sp);
  }

  nloop4[7] = 1;
  *numfac = 0.0;

  /*  Find all facets in mesh */
  i = (int32_T)numel;
  if (0 <= (int32_T)numel - 1) {
    if (1.0 > nen) {
      b_loop_ub = 0;
    } else {
      if (1 > NodesOnElementCG->size[1]) {
        emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementCG->size[1],
          &sc_emlrtBCI, sp);
      }

      b_loop_ub = (int32_T)nen;
      if (b_loop_ub > NodesOnElementCG->size[1]) {
        emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1, NodesOnElementCG->size[1],
          &tc_emlrtBCI, sp);
      }
    }
  }

  emxInit_real_T(sp, &ElementsOnNodeA, 2, &sj_emlrtRTEI, true);
  emxInit_real_T(sp, &ElementsOnNodeB, 2, &tj_emlrtRTEI, true);
  emxInit_boolean_T(sp, &r3, 2, &sf_emlrtRTEI, true);
  emxInit_int32_T(sp, &r4, 1, &ak_emlrtRTEI, true);
  emxInit_int32_T(sp, &r5, 1, &lj_emlrtRTEI, true);
  for (elem = 0; elem < i; elem++) {
    i1 = elem + 1;
    if (i1 > NodesOnElementCG->size[0]) {
      emlrtDynamicBoundsCheckR2012b(i1, 1, NodesOnElementCG->size[0],
        &rc_emlrtBCI, sp);
    }

    b_nodeC2_size[0] = 1;
    b_nodeC2_size[1] = b_loop_ub;
    for (i2 = 0; i2 < b_loop_ub; i2++) {
      nodeC2_data[i2] = NodesOnElementCG->data[elem + NodesOnElementCG->size[0] *
        i2];
    }

    nel = intnnz(nodeC2_data, b_nodeC2_size);
    if ((nel == 3) || (nel == 6)) {
      nume = 2;
    } else {
      nume = 3;
    }

    /*      Loop over facs of element */
    for (locF = 0; locF <= nume; locF++) {
      if (elem + 1 > FacetsOnElement->size[0]) {
        emlrtDynamicBoundsCheckR2012b(elem + 1, 1, FacetsOnElement->size[0],
          &al_emlrtBCI, sp);
      }

      st.site = &bd_emlrtRSI;
      if (!(FacetsOnElement->data[elem + FacetsOnElement->size[0] * locF] != 0.0))
      {
        i2 = RegionOnElement->size[0];
        if (i1 > i2) {
          emlrtDynamicBoundsCheckR2012b(i1, 1, i2, &uc_emlrtBCI, sp);
        }

        /*  reg1 is overwritten below, so it must be re-evaluated */
        if ((nel == 3) || (nel == 6)) {
          if (elem + 1 > NodesOnElementCG->size[0]) {
            emlrtDynamicBoundsCheckR2012b(elem + 1, 1, NodesOnElementCG->size[0],
              &cl_emlrtBCI, sp);
          }

          if (locF + 1 > 3) {
            emlrtDynamicBoundsCheckR2012b(locF + 1, 1, 3, &dl_emlrtBCI, sp);
          }

          i2 = nloop3[locF];
          if (i2 > NodesOnElementCG->size[1]) {
            emlrtDynamicBoundsCheckR2012b(i2, 1, NodesOnElementCG->size[1],
              &cl_emlrtBCI, sp);
          }

          nodeA = NodesOnElementCG->data[elem + NodesOnElementCG->size[0] * (i2
            - 1)];
          if (elem + 1 > NodesOnElementCG->size[0]) {
            emlrtDynamicBoundsCheckR2012b(elem + 1, 1, NodesOnElementCG->size[0],
              &fl_emlrtBCI, sp);
          }

          if (locF + 1 > 3) {
            emlrtDynamicBoundsCheckR2012b(locF + 1, 1, 3, &gl_emlrtBCI, sp);
          }

          i2 = nloop3[locF + 3];
          if (i2 > NodesOnElementCG->size[1]) {
            emlrtDynamicBoundsCheckR2012b(i2, 1, NodesOnElementCG->size[1],
              &fl_emlrtBCI, sp);
          }

          nodeB = NodesOnElementCG->data[elem + NodesOnElementCG->size[0] * (i2
            - 1)];
        } else {
          if (elem + 1 > NodesOnElementCG->size[0]) {
            emlrtDynamicBoundsCheckR2012b(elem + 1, 1, NodesOnElementCG->size[0],
              &bl_emlrtBCI, sp);
          }

          i2 = nloop4[locF];
          if (i2 > NodesOnElementCG->size[1]) {
            emlrtDynamicBoundsCheckR2012b(nloop4[locF], 1,
              NodesOnElementCG->size[1], &bl_emlrtBCI, sp);
          }

          nodeA = NodesOnElementCG->data[elem + NodesOnElementCG->size[0] * (i2
            - 1)];
          if (elem + 1 > NodesOnElementCG->size[0]) {
            emlrtDynamicBoundsCheckR2012b(elem + 1, 1, NodesOnElementCG->size[0],
              &el_emlrtBCI, sp);
          }

          i2 = nloop4[locF + 4];
          if (i2 > NodesOnElementCG->size[1]) {
            emlrtDynamicBoundsCheckR2012b(i2, 1, NodesOnElementCG->size[1],
              &el_emlrtBCI, sp);
          }

          nodeB = NodesOnElementCG->data[elem + NodesOnElementCG->size[0] * (i2
            - 1)];
        }

        if (nodeA != (int32_T)muDoubleScalarFloor(nodeA)) {
          emlrtIntegerCheckR2012b(nodeA, &r_emlrtDCI, sp);
        }

        if (((int32_T)nodeA < 1) || ((int32_T)nodeA > ElementsOnNodeNum->size[0]))
        {
          emlrtDynamicBoundsCheckR2012b((int32_T)nodeA, 1,
            ElementsOnNodeNum->size[0], &vc_emlrtBCI, sp);
        }

        if (nodeB != (int32_T)muDoubleScalarFloor(nodeB)) {
          emlrtIntegerCheckR2012b(nodeB, &s_emlrtDCI, sp);
        }

        if (((int32_T)nodeB < 1) || ((int32_T)nodeB > ElementsOnNodeNum->size[0]))
        {
          emlrtDynamicBoundsCheckR2012b((int32_T)nodeB, 1,
            ElementsOnNodeNum->size[0], &wc_emlrtBCI, sp);
        }

        d = ElementsOnNodeNum->data[(int32_T)nodeA - 1];
        if (d < 1.0) {
          setAll->size[0] = 1;
          setAll->size[1] = 0;
        } else if (muDoubleScalarIsInf(d) && (1.0 == d)) {
          i2 = setAll->size[0] * setAll->size[1];
          setAll->size[0] = 1;
          setAll->size[1] = 1;
          emxEnsureCapacity_real_T(sp, setAll, i2, &nh_emlrtRTEI);
          setAll->data[0] = rtNaN;
        } else {
          i2 = setAll->size[0] * setAll->size[1];
          setAll->size[0] = 1;
          loop_ub = (int32_T)muDoubleScalarFloor(d - 1.0);
          setAll->size[1] = loop_ub + 1;
          emxEnsureCapacity_real_T(sp, setAll, i2, &nh_emlrtRTEI);
          for (i2 = 0; i2 <= loop_ub; i2++) {
            setAll->data[i2] = (real_T)i2 + 1.0;
          }
        }

        st.site = &ad_emlrtRSI;
        b_sparse_parenReference(&st, ElementsOnNode->d, ElementsOnNode->colidx,
          ElementsOnNode->rowidx, ElementsOnNode->m, ElementsOnNode->n, setAll,
          nodeA, NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
          &NodeRegSum.m);
        st.site = &ad_emlrtRSI;
        b_sparse_full(&st, NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                      NodeRegSum.m, ElementsOnNodeA);
        d1 = ElementsOnNodeNum->data[(int32_T)nodeB - 1];
        if (d1 < 1.0) {
          setAll->size[0] = 1;
          setAll->size[1] = 0;
        } else if (muDoubleScalarIsInf(d1) && (1.0 == d1)) {
          i2 = setAll->size[0] * setAll->size[1];
          setAll->size[0] = 1;
          setAll->size[1] = 1;
          emxEnsureCapacity_real_T(sp, setAll, i2, &nh_emlrtRTEI);
          setAll->data[0] = rtNaN;
        } else {
          i2 = setAll->size[0] * setAll->size[1];
          setAll->size[0] = 1;
          loop_ub = (int32_T)muDoubleScalarFloor(d1 - 1.0);
          setAll->size[1] = loop_ub + 1;
          emxEnsureCapacity_real_T(sp, setAll, i2, &nh_emlrtRTEI);
          for (i2 = 0; i2 <= loop_ub; i2++) {
            setAll->data[i2] = (real_T)i2 + 1.0;
          }
        }

        st.site = &yc_emlrtRSI;
        b_sparse_parenReference(&st, ElementsOnNode->d, ElementsOnNode->colidx,
          ElementsOnNode->rowidx, ElementsOnNode->m, ElementsOnNode->n, setAll,
          nodeB, NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
          &NodeRegSum.m);
        st.site = &yc_emlrtRSI;
        b_sparse_full(&st, NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                      NodeRegSum.m, ElementsOnNodeB);

        /*  Determine if fac is on domain interior or boundary */
        if ((d > 1.0) && (d1 > 1.0)) {
          /*  Clean and fast way to intersect the 3 sets of elements, using */
          /*  built-in Matlab functions; change ismembc to ismember if the */
          /*  function is not in the standard package */
          /*              if isOctave */
          st.site = &xc_emlrtRSI;
          b_st.site = &nf_emlrtRSI;
          b_local_ismember(&b_st, ElementsOnNodeA, ElementsOnNodeB, r3);
          st.site = &xc_emlrtRSI;
          c_indexShapeCheck(&st, *(int32_T (*)[2])ElementsOnNodeA->size,
                            *(int32_T (*)[2])r3->size);
          end = r3->size[0] - 1;
          trueCount = 0;
          for (b_i = 0; b_i <= end; b_i++) {
            if (r3->data[b_i]) {
              trueCount++;
            }
          }

          i2 = r4->size[0];
          r4->size[0] = trueCount;
          emxEnsureCapacity_int32_T(sp, r4, i2, &sf_emlrtRTEI);
          nelA = 0;
          for (b_i = 0; b_i <= end; b_i++) {
            if (r3->data[b_i]) {
              r4->data[nelA] = b_i + 1;
              nelA++;
            }
          }

          loop_ub = r4->size[0];
          for (i2 = 0; i2 < loop_ub; i2++) {
            if ((r4->data[i2] < 1) || (r4->data[i2] > ElementsOnNodeA->size[0]))
            {
              emlrtDynamicBoundsCheckR2012b(r4->data[i2], 1,
                ElementsOnNodeA->size[0], &nl_emlrtBCI, sp);
            }
          }

          /*              else */
          /*              twoelem = ElementsOnNodeA(ismembc(ElementsOnNodeA,ElementsOnNodeB)); */
          /*              end */
          if (r4->size[0] == 2) {
            /*  element interface */
            facI1 = 1;
            if (1 > r4->size[0]) {
              emlrtDynamicBoundsCheckR2012b(1, 1, r4->size[0], &xc_emlrtBCI, sp);
            }

            elemA = ElementsOnNodeA->data[r4->data[0] - 1];
            if (elemA == (real_T)elem + 1.0) {
              if (2 > r4->size[0]) {
                emlrtDynamicBoundsCheckR2012b(2, 1, r4->size[0], &yc_emlrtBCI,
                  sp);
              }

              elemA = ElementsOnNodeA->data[r4->data[1] - 1];
            } else {
              if (1 > r4->size[0]) {
                emlrtDynamicBoundsCheckR2012b(1, 1, r4->size[0], &ad_emlrtBCI,
                  sp);
              }
            }
          } else if (r4->size[0] > 2) {
            /*  likely a PBC degenerating case */
            st.site = &wc_emlrtRSI;
            f_error(&st);
          } else {
            /*  domain boundary */
            facI1 = 0;
            elemA = 0.0;
          }
        } else {
          /*  domain boundary */
          facI1 = 0;
          elemA = 0.0;
        }

        if (facI1 == 1) {
          /* element interface, add to SurfacesI */
          /*  Find which slots nodeA and nodeB occupy on elemA */
          b_i = 1;
          if (1 > NodesOnElementCG->size[1]) {
            emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementCG->size[1],
              &bd_emlrtBCI, sp);
          }

          if (elemA != (int32_T)muDoubleScalarFloor(elemA)) {
            emlrtIntegerCheckR2012b(elemA, &rd_emlrtDCI, sp);
          }

          if (((int32_T)elemA < 1) || ((int32_T)elemA > NodesOnElementCG->size[0]))
          {
            emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
              NodesOnElementCG->size[0], &hl_emlrtBCI, sp);
          }

          b_i1 = NodesOnElementCG->data[(int32_T)elemA - 1];
          while ((b_i < 5) && (b_i1 != nodeA)) {
            b_i++;
            if (((int32_T)elemA < 1) || ((int32_T)elemA > NodesOnElementCG->
                 size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
                NodesOnElementCG->size[0], &jl_emlrtBCI, sp);
            }

            if (b_i > NodesOnElementCG->size[1]) {
              emlrtDynamicBoundsCheckR2012b(b_i, 1, NodesOnElementCG->size[1],
                &jl_emlrtBCI, sp);
            }

            b_i1 = NodesOnElementCG->data[((int32_T)elemA +
              NodesOnElementCG->size[0] * (b_i - 1)) - 1];
            if (*emlrtBreakCheckR2012bFlagVar != 0) {
              emlrtBreakCheckR2012b(sp);
            }
          }

          b_i1 = b_i;
          b_i = 1;
          if (1 > NodesOnElementCG->size[1]) {
            emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementCG->size[1],
              &cd_emlrtBCI, sp);
          }

          if (((int32_T)elemA < 1) || ((int32_T)elemA > NodesOnElementCG->size[0]))
          {
            emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
              NodesOnElementCG->size[0], &ml_emlrtBCI, sp);
          }

          numA = NodesOnElementCG->data[(int32_T)elemA - 1];
          while ((b_i < 5) && (numA != nodeB)) {
            b_i++;
            if (((int32_T)elemA < 1) || ((int32_T)elemA > NodesOnElementCG->
                 size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
                NodesOnElementCG->size[0], &ql_emlrtBCI, sp);
            }

            if (b_i > NodesOnElementCG->size[1]) {
              emlrtDynamicBoundsCheckR2012b(b_i, 1, NodesOnElementCG->size[1],
                &ql_emlrtBCI, sp);
            }

            numA = NodesOnElementCG->data[((int32_T)elemA +
              NodesOnElementCG->size[0] * (b_i - 1)) - 1];
            if (*emlrtBreakCheckR2012bFlagVar != 0) {
              emlrtBreakCheckR2012b(sp);
            }
          }

          i2 = (int32_T)b_i1 * b_i;
          if (i2 == 2) {
            numPBC = 0;
          } else if (i2 == 6) {
            numPBC = 1;
          } else if (i2 == 12) {
            numPBC = 2;
          } else if (i2 == 3) {
            numPBC = 2;
          } else {
            /*  nodeC*nodeD==4 */
            numPBC = 3;
          }

          if (((int32_T)elemA < 1) || ((int32_T)elemA > FacetsOnElement->size[0]))
          {
            emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
              FacetsOnElement->size[0], &dd_emlrtBCI, sp);
          }

          if (FacetsOnElement->data[((int32_T)elemA + FacetsOnElement->size[0] *
               numPBC) - 1] == 0.0) {
            /*  New fac, add to list */
            if (1.0 > nen) {
              loop_ub = 0;
            } else {
              if (1 > NodesOnElementCG->size[1]) {
                emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementCG->size[1],
                  &fd_emlrtBCI, sp);
              }

              if ((int32_T)nen > NodesOnElementCG->size[1]) {
                emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1,
                  NodesOnElementCG->size[1], &gd_emlrtBCI, sp);
              }

              loop_ub = (int32_T)nen;
            }

            if (((int32_T)elemA < 1) || ((int32_T)elemA > NodesOnElementCG->
                 size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
                NodesOnElementCG->size[0], &ed_emlrtBCI, sp);
            }

            b_nodeC2_size[0] = 1;
            b_nodeC2_size[1] = loop_ub;
            for (i2 = 0; i2 < loop_ub; i2++) {
              nodeC2_data[i2] = NodesOnElementCG->data[((int32_T)elemA +
                NodesOnElementCG->size[0] * i2) - 1];
            }

            nelA = intnnz(nodeC2_data, b_nodeC2_size);
            i2 = RegionOnElement->size[0];
            if (((int32_T)elemA < 1) || ((int32_T)elemA > i2)) {
              emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1, i2, &hd_emlrtBCI,
                sp);
            }

            elemB = elem + 1;
            notdone = locF;
            node = RegionOnElement->data[(int32_T)elemA - 1];
            if (node > RegionOnElement->data[elem]) {
              /* swap the order of L and R so that L is always larger material ID */
              b_i2 = RegionOnElement->data[elem];
              elemB = (int32_T)elemA;
              elemA = (real_T)elem + 1.0;
              notdone = numPBC;
              numPBC = locF;
              nelA = nel;
            } else {
              b_i2 = node;
              node = RegionOnElement->data[elem];
            }

            regI = node * (node - 1.0) / 2.0 + b_i2;

            /*  ID for material pair (row=mat2, col=mat1) */
            if (regI != (int32_T)muDoubleScalarFloor(regI)) {
              emlrtIntegerCheckR2012b(regI, &t_emlrtDCI, sp);
            }

            if (((int32_T)regI < 1) || ((int32_T)regI > numEonF->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)regI, 1, numEonF->size[0],
                &id_emlrtBCI, sp);
            }

            numSI_tmp = numEonF->data[(int32_T)regI - 1] + 1.0;
            (*numfac)++;
            if (((int32_T)regI < 1) || ((int32_T)regI > numEonF->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)regI, 1, numEonF->size[0],
                &vl_emlrtBCI, sp);
            }

            numEonF->data[(int32_T)regI - 1] = numSI_tmp;
            if ((elemB < 1) || (elemB > FacetsOnElement->size[0])) {
              emlrtDynamicBoundsCheckR2012b(elemB, 1, FacetsOnElement->size[0],
                &wl_emlrtBCI, sp);
            }

            FacetsOnElement->data[(elemB + FacetsOnElement->size[0] * notdone) -
              1] = *numfac;
            if (((int32_T)elemA < 1) || ((int32_T)elemA > FacetsOnElement->size
                 [0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
                FacetsOnElement->size[0], &xl_emlrtBCI, sp);
            }

            FacetsOnElement->data[((int32_T)elemA + FacetsOnElement->size[0] *
              numPBC) - 1] = *numfac;
            if (elemB > FacetsOnElementInt->size[0]) {
              emlrtDynamicBoundsCheckR2012b(elemB, 1, FacetsOnElementInt->size[0],
                &yl_emlrtBCI, sp);
            }

            FacetsOnElementInt->data[(elemB + FacetsOnElementInt->size[0] *
              notdone) - 1] = regI;
            if (((int32_T)elemA < 1) || ((int32_T)elemA >
                 FacetsOnElementInt->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
                FacetsOnElementInt->size[0], &am_emlrtBCI, sp);
            }

            FacetsOnElementInt->data[((int32_T)elemA + FacetsOnElementInt->size
              [0] * numPBC) - 1] = regI;
            if ((nelA == 3) || (nelA == 6)) {
              if (((int32_T)elemA < 1) || ((int32_T)elemA >
                   NodesOnElementCG->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
                  NodesOnElementCG->size[0], &cm_emlrtBCI, sp);
              }

              if (numPBC + 1 > 3) {
                emlrtDynamicBoundsCheckR2012b(numPBC + 1, 1, 3, &dm_emlrtBCI, sp);
              }

              i2 = nloop3[numPBC];
              if (i2 > NodesOnElementCG->size[1]) {
                emlrtDynamicBoundsCheckR2012b(i2, 1, NodesOnElementCG->size[1],
                  &cm_emlrtBCI, sp);
              }

              nodeA = NodesOnElementCG->data[((int32_T)elemA +
                NodesOnElementCG->size[0] * (i2 - 1)) - 1];
              if (((int32_T)elemA < 1) || ((int32_T)elemA >
                   NodesOnElementCG->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
                  NodesOnElementCG->size[0], &fm_emlrtBCI, sp);
              }

              if (numPBC + 1 > 3) {
                emlrtDynamicBoundsCheckR2012b(numPBC + 1, 1, 3, &gm_emlrtBCI, sp);
              }

              i2 = nloop3[numPBC + 3];
              if (i2 > NodesOnElementCG->size[1]) {
                emlrtDynamicBoundsCheckR2012b(i2, 1, NodesOnElementCG->size[1],
                  &fm_emlrtBCI, sp);
              }

              nodeB = NodesOnElementCG->data[((int32_T)elemA +
                NodesOnElementCG->size[0] * (i2 - 1)) - 1];
            } else {
              if (((int32_T)elemA < 1) || ((int32_T)elemA >
                   NodesOnElementCG->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
                  NodesOnElementCG->size[0], &bm_emlrtBCI, sp);
              }

              i2 = nloop4[numPBC];
              if (i2 > NodesOnElementCG->size[1]) {
                emlrtDynamicBoundsCheckR2012b(nloop4[numPBC], 1,
                  NodesOnElementCG->size[1], &bm_emlrtBCI, sp);
              }

              nodeA = NodesOnElementCG->data[((int32_T)elemA +
                NodesOnElementCG->size[0] * (i2 - 1)) - 1];
              if (((int32_T)elemA < 1) || ((int32_T)elemA >
                   NodesOnElementCG->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
                  NodesOnElementCG->size[0], &em_emlrtBCI, sp);
              }

              i2 = nloop4[numPBC + 4];
              if (i2 > NodesOnElementCG->size[1]) {
                emlrtDynamicBoundsCheckR2012b(i2, 1, NodesOnElementCG->size[1],
                  &em_emlrtBCI, sp);
              }

              nodeB = NodesOnElementCG->data[((int32_T)elemA +
                NodesOnElementCG->size[0] * (i2 - 1)) - 1];
            }

            if (((int32_T)*numfac < 1) || ((int32_T)*numfac >
                 ElementsOnFacet->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)*numfac, 1,
                ElementsOnFacet->size[0], &hm_emlrtBCI, sp);
            }

            ElementsOnFacet->data[(int32_T)*numfac - 1] = elemB;

            /* elemL */
            if (((int32_T)*numfac < 1) || ((int32_T)*numfac >
                 ElementsOnFacet->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)*numfac, 1,
                ElementsOnFacet->size[0], &im_emlrtBCI, sp);
            }

            ElementsOnFacet->data[((int32_T)*numfac + ElementsOnFacet->size[0])
              - 1] = elemA;

            /* elemR */
            if (((int32_T)*numfac < 1) || ((int32_T)*numfac >
                 ElementsOnFacet->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)*numfac, 1,
                ElementsOnFacet->size[0], &jm_emlrtBCI, sp);
            }

            ElementsOnFacet->data[((int32_T)*numfac + ElementsOnFacet->size[0] *
              2) - 1] = (real_T)notdone + 1.0;

            /* facL */
            if (((int32_T)*numfac < 1) || ((int32_T)*numfac >
                 ElementsOnFacet->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)*numfac, 1,
                ElementsOnFacet->size[0], &lm_emlrtBCI, sp);
            }

            ElementsOnFacet->data[((int32_T)*numfac + ElementsOnFacet->size[0] *
              3) - 1] = (real_T)numPBC + 1.0;

            /* facR */
            if (((int32_T)*numfac < 1) || ((int32_T)*numfac >
                 FacetsOnInterface->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)*numfac, 1,
                FacetsOnInterface->size[0], &mm_emlrtBCI, sp);
            }

            FacetsOnInterface->data[(int32_T)*numfac - 1] = regI;

            /*  Assign nodal fac pairs */
            if (nodeA != (int32_T)muDoubleScalarFloor(nodeA)) {
              emlrtIntegerCheckR2012b(nodeA, &sd_emlrtDCI, sp);
            }

            if (((int32_T)nodeA < 1) || ((int32_T)nodeA > FacetsOnNodeNum->size
                 [0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)nodeA, 1,
                FacetsOnNodeNum->size[0], &nm_emlrtBCI, sp);
            }

            numA = FacetsOnNodeNum->data[(int32_T)nodeA - 1] + 1.0;
            if (((int32_T)nodeA < 1) || ((int32_T)nodeA > FacetsOnNodeNum->size
                 [0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)nodeA, 1,
                FacetsOnNodeNum->size[0], &qm_emlrtBCI, sp);
            }

            FacetsOnNodeNum->data[(int32_T)nodeA - 1]++;
            nri += 2U;
            if (((int32_T)(nri - 1U) < 1) || ((int32_T)(nri - 1U) >
                 FacetsOnNodeInd->size[1])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)(nri - 1U), 1,
                FacetsOnNodeInd->size[1], &ud_emlrtBCI, sp);
            }

            i2 = 5 * ((int32_T)nri - 2);
            FacetsOnNodeInd->data[i2] = numA;
            FacetsOnNodeInd->data[i2 + 1] = nodeA;
            FacetsOnNodeInd->data[i2 + 2] = *numfac;
            FacetsOnNodeInd->data[i2 + 3] = regI;

            /*                  FacetsOnNode(facnumA,nodeA) = numfac; */
            /*                  FacetsOnNodeInt(facnumA,nodeA) = regI; */
            if (nodeB != (int32_T)muDoubleScalarFloor(nodeB)) {
              emlrtIntegerCheckR2012b(nodeB, &ud_emlrtDCI, sp);
            }

            if (((int32_T)nodeB < 1) || ((int32_T)nodeB > FacetsOnNodeNum->size
                 [0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)nodeB, 1,
                FacetsOnNodeNum->size[0], &rm_emlrtBCI, sp);
            }

            numA = FacetsOnNodeNum->data[(int32_T)nodeB - 1] + 1.0;
            if (((int32_T)nodeB < 1) || ((int32_T)nodeB > FacetsOnNodeNum->size
                 [0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)nodeB, 1,
                FacetsOnNodeNum->size[0], &sm_emlrtBCI, sp);
            }

            FacetsOnNodeNum->data[(int32_T)nodeB - 1]++;
            if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1]))
            {
              emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1,
                FacetsOnNodeInd->size[1], &vd_emlrtBCI, sp);
            }

            i3 = 5 * ((int32_T)nri - 1);
            FacetsOnNodeInd->data[i3] = numA;
            FacetsOnNodeInd->data[i3 + 1] = nodeB;
            FacetsOnNodeInd->data[i3 + 2] = *numfac;
            FacetsOnNodeInd->data[i3 + 3] = regI;

            /*                  FacetsOnNode(facnumB,nodeB) = numfac; */
            /*                  FacetsOnNodeInt(facnumB,nodeB) = regI; */
            st.site = &vc_emlrtRSI;
            if (muDoubleScalarIsNaN(usePBC)) {
              emlrtErrorWithMessageIdR2018a(&st, &mb_emlrtRTEI,
                "MATLAB:nologicalnan", "MATLAB:nologicalnan", 0);
            }

            if (usePBC != 0.0) {
              /*  check if facet is on the PBC, if so then CUT it */
              if ((nelA == 3) || (nelA == 6)) {
                if (((int32_T)elemA < 1) || ((int32_T)elemA >
                     NodesOnElementPBC->size[0])) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
                    NodesOnElementPBC->size[0], &an_emlrtBCI, sp);
                }

                if (numPBC + 1 > 3) {
                  emlrtDynamicBoundsCheckR2012b(numPBC + 1, 1, 3, &bn_emlrtBCI,
                    sp);
                }

                b_i = nloop3[numPBC];
                if (b_i > NodesOnElementPBC->size[1]) {
                  emlrtDynamicBoundsCheckR2012b(b_i, 1, NodesOnElementPBC->size
                    [1], &an_emlrtBCI, sp);
                }

                numA = NodesOnElementPBC->data[((int32_T)elemA +
                  NodesOnElementPBC->size[0] * (b_i - 1)) - 1];
                if (((int32_T)elemA < 1) || ((int32_T)elemA >
                     NodesOnElementPBC->size[0])) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
                    NodesOnElementPBC->size[0], &dn_emlrtBCI, sp);
                }

                if (numPBC + 1 > 3) {
                  emlrtDynamicBoundsCheckR2012b(numPBC + 1, 1, 3, &en_emlrtBCI,
                    sp);
                }

                b_i = nloop3[numPBC + 3];
                if (b_i > NodesOnElementPBC->size[1]) {
                  emlrtDynamicBoundsCheckR2012b(b_i, 1, NodesOnElementPBC->size
                    [1], &dn_emlrtBCI, sp);
                }

                b_i1 = NodesOnElementPBC->data[((int32_T)elemA +
                  NodesOnElementPBC->size[0] * (b_i - 1)) - 1];
              } else {
                if (((int32_T)elemA < 1) || ((int32_T)elemA >
                     NodesOnElementPBC->size[0])) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
                    NodesOnElementPBC->size[0], &ym_emlrtBCI, sp);
                }

                if (nloop4[numPBC] > NodesOnElementPBC->size[1]) {
                  emlrtDynamicBoundsCheckR2012b(nloop4[numPBC], 1,
                    NodesOnElementPBC->size[1], &ym_emlrtBCI, sp);
                }

                numA = NodesOnElementPBC->data[((int32_T)elemA +
                  NodesOnElementPBC->size[0] * (nloop4[numPBC] - 1)) - 1];
                if (((int32_T)elemA < 1) || ((int32_T)elemA >
                     NodesOnElementPBC->size[0])) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)elemA, 1,
                    NodesOnElementPBC->size[0], &cn_emlrtBCI, sp);
                }

                b_i = nloop4[numPBC + 4];
                if (b_i > NodesOnElementPBC->size[1]) {
                  emlrtDynamicBoundsCheckR2012b(b_i, 1, NodesOnElementPBC->size
                    [1], &cn_emlrtBCI, sp);
                }

                b_i1 = NodesOnElementPBC->data[((int32_T)elemA +
                  NodesOnElementPBC->size[0] * (b_i - 1)) - 1];
              }

              /*                      if (nelB==3||nelB==6) */
              st.site = &uc_emlrtRSI;
              if (elemB > NodesOnElementCG->size[0]) {
                emlrtDynamicBoundsCheckR2012b(elemB, 1, NodesOnElementCG->size[0],
                  &jd_emlrtBCI, &st);
              }

              loop_ub = NodesOnElementCG->size[1];
              nodeC2_size[0] = 1;
              nodeC2_size[1] = NodesOnElementCG->size[1];
              for (b_i = 0; b_i < loop_ub; b_i++) {
                b_nodeC2_data[b_i] = (NodesOnElementCG->data[(elemB +
                  NodesOnElementCG->size[0] * b_i) - 1] == nodeA);
              }

              b_st.site = &ug_emlrtRSI;
              f_eml_find(&b_st, b_nodeC2_data, nodeC2_size, ii_data, ii_size);
              st.site = &tc_emlrtRSI;
              if (elemB > NodesOnElementCG->size[0]) {
                emlrtDynamicBoundsCheckR2012b(elemB, 1, NodesOnElementCG->size[0],
                  &kd_emlrtBCI, &st);
              }

              loop_ub = NodesOnElementCG->size[1];
              nodeC2_size[0] = 1;
              nodeC2_size[1] = NodesOnElementCG->size[1];
              for (b_i = 0; b_i < loop_ub; b_i++) {
                b_nodeC2_data[b_i] = (NodesOnElementCG->data[(elemB +
                  NodesOnElementCG->size[0] * b_i) - 1] == nodeB);
              }

              b_st.site = &ug_emlrtRSI;
              f_eml_find(&b_st, b_nodeC2_data, nodeC2_size, b_ii_data, tmp_size);
              if (elemB > NodesOnElementPBC->size[0]) {
                emlrtDynamicBoundsCheckR2012b(elemB, 1, NodesOnElementPBC->size
                  [0], &ld_emlrtBCI, sp);
              }

              b_nodeC2_size[0] = 1;
              loop_ub = ii_size[1];
              b_nodeC2_size[1] = ii_size[1];
              for (b_i = 0; b_i < loop_ub; b_i++) {
                end = ii_data[b_i];
                if ((end < 1) || (end > NodesOnElementPBC->size[1])) {
                  emlrtDynamicBoundsCheckR2012b(end, 1, NodesOnElementPBC->size
                    [1], &ln_emlrtBCI, sp);
                }

                nodeC2_data[b_i] = NodesOnElementPBC->data[(elemB +
                  NodesOnElementPBC->size[0] * (ii_data[b_i] - 1)) - 1];
              }

              if (elemB > NodesOnElementPBC->size[0]) {
                emlrtDynamicBoundsCheckR2012b(elemB, 1, NodesOnElementPBC->size
                  [0], &md_emlrtBCI, sp);
              }

              nodeD2_size[0] = 1;
              loop_ub = tmp_size[1];
              nodeD2_size[1] = tmp_size[1];
              for (b_i = 0; b_i < loop_ub; b_i++) {
                end = b_ii_data[b_i];
                if ((end < 1) || (end > NodesOnElementPBC->size[1])) {
                  emlrtDynamicBoundsCheckR2012b(end, 1, NodesOnElementPBC->size
                    [1], &mn_emlrtBCI, sp);
                }

                nodeD2_data[b_i] = NodesOnElementPBC->data[(elemB +
                  NodesOnElementPBC->size[0] * (b_ii_data[b_i] - 1)) - 1];
              }

              /*  Condition: if the nodes opposite each other on the */
              /*  two elements adjoining the facet from the unzipped */
              /*  model (NodesOnElementPBC) are all different, then the */
              /*  two elements are on opposite sides of the domain and */
              /*  the facet needs to be cut */
              if ((!isequal(numA, nodeC2_data, b_nodeC2_size)) && (!isequal(b_i1,
                    nodeD2_data, nodeD2_size))) {
                /*  Make sure facet is NOT an interface in the */
                /*  unzipped mesh, because for triangles both nodes */
                /*  might be in PBCList but they are on different */
                /*  boundary surfaces */
                if (numA != (int32_T)muDoubleScalarFloor(numA)) {
                  emlrtIntegerCheckR2012b(numA, &u_emlrtDCI, sp);
                }

                if (((int32_T)numA < 1) || ((int32_T)numA >
                     ElementsOnNodePBCNum->size[0])) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1,
                    ElementsOnNodePBCNum->size[0], &nd_emlrtBCI, sp);
                }

                if (b_i1 != (int32_T)muDoubleScalarFloor(b_i1)) {
                  emlrtIntegerCheckR2012b(b_i1, &v_emlrtDCI, sp);
                }

                if (((int32_T)b_i1 < 1) || ((int32_T)b_i1 >
                     ElementsOnNodePBCNum->size[0])) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)b_i1, 1,
                    ElementsOnNodePBCNum->size[0], &od_emlrtBCI, sp);
                }

                d = ElementsOnNodePBCNum->data[(int32_T)numA - 1];
                if (d < 1.0) {
                  setAll->size[0] = 1;
                  setAll->size[1] = 0;
                } else if (muDoubleScalarIsInf(d) && (1.0 == d)) {
                  b_i = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  setAll->size[1] = 1;
                  emxEnsureCapacity_real_T(sp, setAll, b_i, &nh_emlrtRTEI);
                  setAll->data[0] = rtNaN;
                } else {
                  b_i = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  loop_ub = (int32_T)muDoubleScalarFloor(d - 1.0);
                  setAll->size[1] = loop_ub + 1;
                  emxEnsureCapacity_real_T(sp, setAll, b_i, &nh_emlrtRTEI);
                  for (b_i = 0; b_i <= loop_ub; b_i++) {
                    setAll->data[b_i] = (real_T)b_i + 1.0;
                  }
                }

                st.site = &sc_emlrtRSI;
                b_sparse_parenReference(&st, expl_temp.d, expl_temp.colidx,
                  expl_temp.rowidx, locN, ElementsOnNodePBC_n, setAll, numA,
                  NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  &NodeRegSum.m);
                st.site = &sc_emlrtRSI;
                b_sparse_full(&st, NodeRegSum.d, NodeRegSum.colidx,
                              NodeRegSum.rowidx, NodeRegSum.m, ElementsOnNodeA);
                d1 = ElementsOnNodePBCNum->data[(int32_T)b_i1 - 1];
                if (d1 < 1.0) {
                  setAll->size[0] = 1;
                  setAll->size[1] = 0;
                } else if (muDoubleScalarIsInf(d1) && (1.0 == d1)) {
                  b_i = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  setAll->size[1] = 1;
                  emxEnsureCapacity_real_T(sp, setAll, b_i, &nh_emlrtRTEI);
                  setAll->data[0] = rtNaN;
                } else {
                  b_i = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  loop_ub = (int32_T)muDoubleScalarFloor(d1 - 1.0);
                  setAll->size[1] = loop_ub + 1;
                  emxEnsureCapacity_real_T(sp, setAll, b_i, &nh_emlrtRTEI);
                  for (b_i = 0; b_i <= loop_ub; b_i++) {
                    setAll->data[b_i] = (real_T)b_i + 1.0;
                  }
                }

                st.site = &rc_emlrtRSI;
                b_sparse_parenReference(&st, expl_temp.d, expl_temp.colidx,
                  expl_temp.rowidx, locN, ElementsOnNodePBC_n, setAll, b_i1,
                  NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  &NodeRegSum.m);
                st.site = &rc_emlrtRSI;
                b_sparse_full(&st, NodeRegSum.d, NodeRegSum.colidx,
                              NodeRegSum.rowidx, NodeRegSum.m, ElementsOnNodeB);

                /*  Determine if fac is on domain interior or boundary */
                if ((d > 1.0) && (d1 > 1.0)) {
                  /*  Clean and fast way to intersect the 3 sets of elements, using */
                  /*  built-in Matlab functions; change ismembc to ismember if the */
                  /*  function is not in the standard package */
                  /*                            if isOctave */
                  st.site = &qc_emlrtRSI;
                  b_st.site = &nf_emlrtRSI;
                  b_local_ismember(&b_st, ElementsOnNodeA, ElementsOnNodeB, r3);
                  st.site = &qc_emlrtRSI;
                  c_indexShapeCheck(&st, *(int32_T (*)[2])ElementsOnNodeA->size,
                                    *(int32_T (*)[2])r3->size);
                  end = r3->size[0] - 1;
                  trueCount = 0;
                  for (b_i = 0; b_i <= end; b_i++) {
                    if (r3->data[b_i]) {
                      trueCount++;
                    }
                  }

                  b_i = r5->size[0];
                  r5->size[0] = trueCount;
                  emxEnsureCapacity_int32_T(sp, r5, b_i, &sf_emlrtRTEI);
                  nelA = 0;
                  for (b_i = 0; b_i <= end; b_i++) {
                    if (r3->data[b_i]) {
                      r5->data[nelA] = b_i + 1;
                      nelA++;
                    }
                  }

                  loop_ub = r5->size[0];
                  b_i = r->size[0];
                  r->size[0] = r5->size[0];
                  emxEnsureCapacity_int32_T(sp, r, b_i, &lj_emlrtRTEI);
                  for (b_i = 0; b_i < loop_ub; b_i++) {
                    if ((r5->data[b_i] < 1) || (r5->data[b_i] >
                         ElementsOnNodeA->size[0])) {
                      emlrtDynamicBoundsCheckR2012b(r5->data[b_i], 1,
                        ElementsOnNodeA->size[0], &po_emlrtBCI, sp);
                    }

                    r->data[b_i] = r5->data[b_i];
                  }

                  /*                            else */
                  /*                            twoelem = ElementsOnNodeA(ismembc(ElementsOnNodeA,ElementsOnNodeB)); */
                  /*                            end */
                  if (r->size[0] != 2) {
                    if (r->size[0] > 2) {
                      /*  likely a PBC degenerating case */
                      st.site = &pc_emlrtRSI;
                      g_error(&st);
                    } else {
                      /*  domain boundary */
                      facI1 = 0;
                    }
                  } else {
                    /*  element interface */
                  }
                } else {
                  /*  domain boundary */
                  facI1 = 0;
                }

                if (facI1 == 0) {
                  nelA = 1;
                  if (((int32_T)regI < 1) || ((int32_T)regI > numEonPBC->size[0]))
                  {
                    emlrtDynamicBoundsCheckR2012b((int32_T)regI, 1,
                      numEonPBC->size[0], &pd_emlrtBCI, sp);
                  }

                  numSI_tmp = numEonPBC->data[(int32_T)regI - 1] + 1.0;
                  if (((int32_T)regI < 1) || ((int32_T)regI > numEonPBC->size[0]))
                  {
                    emlrtDynamicBoundsCheckR2012b((int32_T)regI, 1,
                      numEonPBC->size[0], &jo_emlrtBCI, sp);
                  }

                  numEonPBC->data[(int32_T)regI - 1] = numSI_tmp;
                  numFPBC++;
                  if (((int32_T)*numfac < 1) || ((int32_T)*numfac >
                       FacetsOnPBC->size[0])) {
                    emlrtDynamicBoundsCheckR2012b((int32_T)*numfac, 1,
                      FacetsOnPBC->size[0], &lo_emlrtBCI, sp);
                  }

                  FacetsOnPBC->data[(int32_T)*numfac - 1] = regI;
                } else {
                  nelA = 0;
                }
              } else {
                nelA = 0;
              }
            } else {
              nelA = 0;
            }

            if (node != (int32_T)muDoubleScalarFloor(node)) {
              emlrtIntegerCheckR2012b(node, &xd_emlrtDCI, sp);
            }

            if (((int32_T)node < 1) || ((int32_T)node > InterTypes->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)node, 1, InterTypes->size[0],
                &xm_emlrtBCI, sp);
            }

            if (b_i2 != (int32_T)muDoubleScalarFloor(b_i2)) {
              emlrtIntegerCheckR2012b(b_i2, &xd_emlrtDCI, sp);
            }

            if (((int32_T)b_i2 < 1) || ((int32_T)b_i2 > InterTypes->size[1])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)b_i2, 1, InterTypes->size[1],
                &xm_emlrtBCI, sp);
            }

            guard1 = false;
            if (InterTypes->data[((int32_T)node + InterTypes->size[0] *
                                  ((int32_T)b_i2 - 1)) - 1] > 0.0) {
              guard1 = true;
            } else {
              st.site = &oc_emlrtRSI;
              if (nelA != 0) {
                guard1 = true;
              }
            }

            if (guard1) {
              /*  Mark facs as being cut */
              if (((int32_T)nri - 1 < 1) || ((int32_T)nri - 1 >
                   FacetsOnNodeInd->size[1])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)nri - 1, 1,
                  FacetsOnNodeInd->size[1], &fn_emlrtBCI, sp);
              }

              FacetsOnNodeInd->data[i2 + 4] = 1.0;
              if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1]))
              {
                emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1,
                  FacetsOnNodeInd->size[1], &gn_emlrtBCI, sp);
              }

              FacetsOnNodeInd->data[i3 + 4] = 1.0;

              /*                      FacetsOnNodeCut(facnumA,nodeA) = 1; */
              /*                      FacetsOnNodeCut(facnumB,nodeB) = 1; */
            }
          }
        } else {
          /* domain boundary, add to SurfaceF */
          st.site = &nc_emlrtRSI;
          if (muDoubleScalarIsNaN(usePBC)) {
            emlrtErrorWithMessageIdR2018a(&st, &mb_emlrtRTEI,
              "MATLAB:nologicalnan", "MATLAB:nologicalnan", 0);
          }

          if (usePBC != 0.0) {
            st.site = &mc_emlrtRSI;
            h_error(&st);
          }

          numSI_tmp = *numEonB + 1.0;
          (*numEonB)++;
          if (elem + 1 > FacetsOnElement->size[0]) {
            emlrtDynamicBoundsCheckR2012b(elem + 1, 1, FacetsOnElement->size[0],
              &il_emlrtBCI, sp);
          }

          FacetsOnElement->data[elem + FacetsOnElement->size[0] * locF] =
            -numSI_tmp;
          if (((int32_T)numSI_tmp < 1) || ((int32_T)numSI_tmp >
               ElementsOnBoundary->size[0])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)numSI_tmp, 1,
              ElementsOnBoundary->size[0], &kl_emlrtBCI, sp);
          }

          ElementsOnBoundary->data[(int32_T)numSI_tmp - 1] = (real_T)elem + 1.0;
          if (((int32_T)numSI_tmp < 1) || ((int32_T)numSI_tmp >
               ElementsOnBoundary->size[0])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)numSI_tmp, 1,
              ElementsOnBoundary->size[0], &ll_emlrtBCI, sp);
          }

          ElementsOnBoundary->data[((int32_T)numSI_tmp +
            ElementsOnBoundary->size[0]) - 1] = (real_T)locF + 1.0;

          /*  Assign nodal fac pairs */
          if (((int32_T)nodeA < 1) || ((int32_T)nodeA > FacetsOnNodeNum->size[0]))
          {
            emlrtDynamicBoundsCheckR2012b((int32_T)nodeA, 1,
              FacetsOnNodeNum->size[0], &ol_emlrtBCI, sp);
          }

          numA = FacetsOnNodeNum->data[(int32_T)nodeA - 1] + 1.0;
          if (((int32_T)nodeA < 1) || ((int32_T)nodeA > FacetsOnNodeNum->size[0]))
          {
            emlrtDynamicBoundsCheckR2012b((int32_T)nodeA, 1,
              FacetsOnNodeNum->size[0], &pl_emlrtBCI, sp);
          }

          FacetsOnNodeNum->data[(int32_T)nodeA - 1] = numA;
          nri += 2U;
          if (((int32_T)(nri - 1U) < 1) || ((int32_T)(nri - 1U) >
               FacetsOnNodeInd->size[1])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)(nri - 1U), 1,
              FacetsOnNodeInd->size[1], &wd_emlrtBCI, sp);
          }

          i2 = 5 * ((int32_T)nri - 2);
          FacetsOnNodeInd->data[i2] = numA;
          FacetsOnNodeInd->data[i2 + 1] = nodeA;
          FacetsOnNodeInd->data[i2 + 2] = numSI_tmp;
          FacetsOnNodeInd->data[i2 + 3] = -1.0;

          /*              FacetsOnNode(facnum,nodeA) = numSI; */
          /*              FacetsOnNodeInt(facnum,nodeA) = -1; */
          if (((int32_T)nodeB < 1) || ((int32_T)nodeB > FacetsOnNodeNum->size[0]))
          {
            emlrtDynamicBoundsCheckR2012b((int32_T)nodeB, 1,
              FacetsOnNodeNum->size[0], &rl_emlrtBCI, sp);
          }

          numA = FacetsOnNodeNum->data[(int32_T)nodeB - 1] + 1.0;
          if (((int32_T)nodeB < 1) || ((int32_T)nodeB > FacetsOnNodeNum->size[0]))
          {
            emlrtDynamicBoundsCheckR2012b((int32_T)nodeB, 1,
              FacetsOnNodeNum->size[0], &sl_emlrtBCI, sp);
          }

          FacetsOnNodeNum->data[(int32_T)nodeB - 1] = numA;
          if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, FacetsOnNodeInd->
              size[1], &xd_emlrtBCI, sp);
          }

          i2 = 5 * ((int32_T)nri - 1);
          FacetsOnNodeInd->data[i2] = numA;
          FacetsOnNodeInd->data[i2 + 1] = nodeB;
          FacetsOnNodeInd->data[i2 + 2] = numSI_tmp;
          FacetsOnNodeInd->data[i2 + 3] = -1.0;

          /*              FacetsOnNode(facnum,nodeB) = numSI; */
          /*              FacetsOnNodeInt(facnum,nodeB) = -1; */
        }
      }

      /* facet not identified */
      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  emxFreeStruct_sparse1(&expl_temp);
  emxFree_int32_T(&r5);
  emxFree_int32_T(&r4);
  emxFree_int32_T(&r);
  emxFree_boolean_T(&r3);
  emxFree_real_T(&ElementsOnNodeB);
  if (1.0 > *numfac) {
    loop_ub = 0;
  } else {
    if (1 > ElementsOnFacet->size[0]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, ElementsOnFacet->size[0], &qd_emlrtBCI,
        sp);
    }

    if (((int32_T)*numfac < 1) || ((int32_T)*numfac > ElementsOnFacet->size[0]))
    {
      emlrtDynamicBoundsCheckR2012b((int32_T)*numfac, 1, ElementsOnFacet->size[0],
        &rd_emlrtBCI, sp);
    }

    loop_ub = (int32_T)*numfac;
  }

  emxInit_real_T(sp, &b_ElementsOnFacet, 2, &gh_emlrtRTEI, true);
  i = b_ElementsOnFacet->size[0] * b_ElementsOnFacet->size[1];
  b_ElementsOnFacet->size[0] = loop_ub;
  b_ElementsOnFacet->size[1] = 4;
  emxEnsureCapacity_real_T(sp, b_ElementsOnFacet, i, &gh_emlrtRTEI);
  for (i = 0; i < loop_ub; i++) {
    b_ElementsOnFacet->data[i] = ElementsOnFacet->data[i];
  }

  for (i = 0; i < loop_ub; i++) {
    b_ElementsOnFacet->data[i + b_ElementsOnFacet->size[0]] =
      ElementsOnFacet->data[i + ElementsOnFacet->size[0]];
  }

  for (i = 0; i < loop_ub; i++) {
    b_ElementsOnFacet->data[i + b_ElementsOnFacet->size[0] * 2] =
      ElementsOnFacet->data[i + ElementsOnFacet->size[0] * 2];
  }

  for (i = 0; i < loop_ub; i++) {
    b_ElementsOnFacet->data[i + b_ElementsOnFacet->size[0] * 3] =
      ElementsOnFacet->data[i + ElementsOnFacet->size[0] * 3];
  }

  i = ElementsOnFacet->size[0] * ElementsOnFacet->size[1];
  ElementsOnFacet->size[0] = b_ElementsOnFacet->size[0];
  ElementsOnFacet->size[1] = 4;
  emxEnsureCapacity_real_T(sp, ElementsOnFacet, i, &hh_emlrtRTEI);
  loop_ub = b_ElementsOnFacet->size[0] * b_ElementsOnFacet->size[1];
  for (i = 0; i < loop_ub; i++) {
    ElementsOnFacet->data[i] = b_ElementsOnFacet->data[i];
  }

  emxFree_real_T(&b_ElementsOnFacet);
  if (1.0 > *numEonB) {
    loop_ub = 0;
  } else {
    if (1 > ElementsOnBoundary->size[0]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, ElementsOnBoundary->size[0],
        &sd_emlrtBCI, sp);
    }

    if (((int32_T)*numEonB < 1) || ((int32_T)*numEonB > ElementsOnBoundary->
         size[0])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)*numEonB, 1,
        ElementsOnBoundary->size[0], &td_emlrtBCI, sp);
    }

    loop_ub = (int32_T)*numEonB;
  }

  i = b_MPCList->size[0] * b_MPCList->size[1];
  b_MPCList->size[0] = loop_ub;
  b_MPCList->size[1] = 2;
  emxEnsureCapacity_real_T(sp, b_MPCList, i, &ih_emlrtRTEI);
  for (i = 0; i < loop_ub; i++) {
    b_MPCList->data[i] = ElementsOnBoundary->data[i];
  }

  for (i = 0; i < loop_ub; i++) {
    b_MPCList->data[i + b_MPCList->size[0]] = ElementsOnBoundary->data[i +
      ElementsOnBoundary->size[0]];
  }

  i = ElementsOnBoundary->size[0] * ElementsOnBoundary->size[1];
  ElementsOnBoundary->size[0] = b_MPCList->size[0];
  ElementsOnBoundary->size[1] = 2;
  emxEnsureCapacity_real_T(sp, ElementsOnBoundary, i, &jh_emlrtRTEI);
  loop_ub = b_MPCList->size[0] * b_MPCList->size[1];
  for (i = 0; i < loop_ub; i++) {
    ElementsOnBoundary->data[i] = b_MPCList->data[i];
  }

  emxFree_real_T(&b_MPCList);
  if (1 > (int32_T)nri) {
    loop_ub = 0;
    nel = 0;
    nume = 0;
  } else {
    if (1 > FacetsOnNodeInd->size[1]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, FacetsOnNodeInd->size[1], &yd_emlrtBCI,
        sp);
    }

    if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, FacetsOnNodeInd->size[1],
        &ae_emlrtBCI, sp);
    }

    loop_ub = (int32_T)nri;
    if (1 > FacetsOnNodeInd->size[1]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, FacetsOnNodeInd->size[1], &be_emlrtBCI,
        sp);
    }

    if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, FacetsOnNodeInd->size[1],
        &ce_emlrtBCI, sp);
    }

    nel = (int32_T)nri;
    if (1 > FacetsOnNodeInd->size[1]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, FacetsOnNodeInd->size[1], &de_emlrtBCI,
        sp);
    }

    if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, FacetsOnNodeInd->size[1],
        &ee_emlrtBCI, sp);
    }

    nume = (int32_T)nri;
  }

  i = setPBC->size[0] * setPBC->size[1];
  setPBC->size[0] = 1;
  setPBC->size[1] = loop_ub;
  emxEnsureCapacity_real_T(sp, setPBC, i, &kh_emlrtRTEI);
  for (i = 0; i < loop_ub; i++) {
    setPBC->data[i] = FacetsOnNodeInd->data[5 * i];
  }

  i = setAll->size[0] * setAll->size[1];
  setAll->size[0] = 1;
  setAll->size[1] = nel;
  emxEnsureCapacity_real_T(sp, setAll, i, &lh_emlrtRTEI);
  for (i = 0; i < nel; i++) {
    setAll->data[i] = FacetsOnNodeInd->data[5 * i + 1];
  }

  i = NodeRegInd->size[0] * NodeRegInd->size[1];
  NodeRegInd->size[0] = 1;
  NodeRegInd->size[1] = nume;
  emxEnsureCapacity_real_T(sp, NodeRegInd, i, &mh_emlrtRTEI);
  for (i = 0; i < nume; i++) {
    NodeRegInd->data[i] = FacetsOnNodeInd->data[5 * i + 2];
  }

  st.site = &lc_emlrtRSI;
  b_sparse(&st, setPBC, setAll, NodeRegInd, FacetsOnNode);

  /*  for each node, which other node is connected to it by an edge; value is the nodal ID of the connecting node */
  if (1 > (int32_T)nri) {
    loop_ub = 0;
    nel = 0;
    nume = 0;
  } else {
    if (1 > FacetsOnNodeInd->size[1]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, FacetsOnNodeInd->size[1], &fe_emlrtBCI,
        sp);
    }

    if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, FacetsOnNodeInd->size[1],
        &ge_emlrtBCI, sp);
    }

    loop_ub = (int32_T)nri;
    if (1 > FacetsOnNodeInd->size[1]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, FacetsOnNodeInd->size[1], &he_emlrtBCI,
        sp);
    }

    if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, FacetsOnNodeInd->size[1],
        &ie_emlrtBCI, sp);
    }

    nel = (int32_T)nri;
    if (1 > FacetsOnNodeInd->size[1]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, FacetsOnNodeInd->size[1], &je_emlrtBCI,
        sp);
    }

    if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, FacetsOnNodeInd->size[1],
        &ke_emlrtBCI, sp);
    }

    nume = (int32_T)nri;
  }

  i = setPBC->size[0] * setPBC->size[1];
  setPBC->size[0] = 1;
  setPBC->size[1] = loop_ub;
  emxEnsureCapacity_real_T(sp, setPBC, i, &oh_emlrtRTEI);
  for (i = 0; i < loop_ub; i++) {
    setPBC->data[i] = FacetsOnNodeInd->data[5 * i];
  }

  i = setAll->size[0] * setAll->size[1];
  setAll->size[0] = 1;
  setAll->size[1] = nel;
  emxEnsureCapacity_real_T(sp, setAll, i, &ph_emlrtRTEI);
  for (i = 0; i < nel; i++) {
    setAll->data[i] = FacetsOnNodeInd->data[5 * i + 1];
  }

  i = NodeRegInd->size[0] * NodeRegInd->size[1];
  NodeRegInd->size[0] = 1;
  NodeRegInd->size[1] = nume;
  emxEnsureCapacity_real_T(sp, NodeRegInd, i, &qh_emlrtRTEI);
  for (i = 0; i < nume; i++) {
    NodeRegInd->data[i] = FacetsOnNodeInd->data[5 * i + 4];
  }

  st.site = &kc_emlrtRSI;
  b_sparse(&st, setPBC, setAll, NodeRegInd, FacetsOnNodeCut);

  /*  flag for whether that edge is being cut between two nodes; 1 for cut, 0 for retain. */
  if (1 > (int32_T)nri) {
    loop_ub = 0;
    nel = 0;
    nume = 0;
  } else {
    if (1 > FacetsOnNodeInd->size[1]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, FacetsOnNodeInd->size[1], &le_emlrtBCI,
        sp);
    }

    if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, FacetsOnNodeInd->size[1],
        &me_emlrtBCI, sp);
    }

    loop_ub = (int32_T)nri;
    if (1 > FacetsOnNodeInd->size[1]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, FacetsOnNodeInd->size[1], &ne_emlrtBCI,
        sp);
    }

    if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, FacetsOnNodeInd->size[1],
        &oe_emlrtBCI, sp);
    }

    nel = (int32_T)nri;
    if (1 > FacetsOnNodeInd->size[1]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, FacetsOnNodeInd->size[1], &pe_emlrtBCI,
        sp);
    }

    if (((int32_T)nri < 1) || ((int32_T)nri > FacetsOnNodeInd->size[1])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, FacetsOnNodeInd->size[1],
        &qe_emlrtBCI, sp);
    }

    nume = (int32_T)nri;
  }

  i = setPBC->size[0] * setPBC->size[1];
  setPBC->size[0] = 1;
  setPBC->size[1] = loop_ub;
  emxEnsureCapacity_real_T(sp, setPBC, i, &rh_emlrtRTEI);
  for (i = 0; i < loop_ub; i++) {
    setPBC->data[i] = FacetsOnNodeInd->data[5 * i];
  }

  i = setAll->size[0] * setAll->size[1];
  setAll->size[0] = 1;
  setAll->size[1] = nel;
  emxEnsureCapacity_real_T(sp, setAll, i, &sh_emlrtRTEI);
  for (i = 0; i < nel; i++) {
    setAll->data[i] = FacetsOnNodeInd->data[5 * i + 1];
  }

  i = NodeRegInd->size[0] * NodeRegInd->size[1];
  NodeRegInd->size[0] = 1;
  NodeRegInd->size[1] = nume;
  emxEnsureCapacity_real_T(sp, NodeRegInd, i, &th_emlrtRTEI);
  for (i = 0; i < nume; i++) {
    NodeRegInd->data[i] = FacetsOnNodeInd->data[5 * i + 3];
  }

  emxFree_real_T(&FacetsOnNodeInd);
  st.site = &jc_emlrtRSI;
  b_sparse(&st, setPBC, setAll, NodeRegInd, FacetsOnNodeInt);

  /*  flag for materialI ID for that edge; -1 means domain edge. */
  /*  clear FacetsOnNodeInd */
  /*  Group facets according to interface type */
  emxFree_real_T(&NodeRegInd);
  if (1.0 > *numfac) {
    loop_ub = 0;
  } else {
    if (1 > FacetsOnInterface->size[0]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, FacetsOnInterface->size[0],
        &re_emlrtBCI, sp);
    }

    if (((int32_T)*numfac < 1) || ((int32_T)*numfac > FacetsOnInterface->size[0]))
    {
      emlrtDynamicBoundsCheckR2012b((int32_T)*numfac, 1, FacetsOnInterface->
        size[0], &se_emlrtBCI, sp);
    }

    loop_ub = (int32_T)*numfac;
  }

  b_numel[0] = 1;
  b_numel[1] = loop_ub;
  st.site = &ic_emlrtRSI;
  indexShapeCheck(&st, FacetsOnInterface->size[0], b_numel);
  st.site = &ic_emlrtRSI;
  i = MorePBC->size[0];
  MorePBC->size[0] = loop_ub;
  emxEnsureCapacity_real_T(&st, MorePBC, i, &de_emlrtRTEI);
  for (i = 0; i < loop_ub; i++) {
    MorePBC->data[i] = FacetsOnInterface->data[i];
  }

  b_st.site = &tf_emlrtRSI;
  sort(&b_st, MorePBC, indx);
  i = FacetsOnInterface->size[0];
  FacetsOnInterface->size[0] = indx->size[0];
  emxEnsureCapacity_real_T(&st, FacetsOnInterface, i, &uh_emlrtRTEI);
  loop_ub = indx->size[0];
  for (i = 0; i < loop_ub; i++) {
    FacetsOnInterface->data[i] = indx->data[i];
  }

  i = FacetsOnIntMinusPBCNum->size[0];
  FacetsOnIntMinusPBCNum->size[0] = numEonF->size[0] + 1;
  emxEnsureCapacity_real_T(sp, FacetsOnIntMinusPBCNum, i, &vh_emlrtRTEI);
  FacetsOnIntMinusPBCNum->data[0] = 1.0;
  loop_ub = numEonF->size[0];
  for (i = 0; i < loop_ub; i++) {
    FacetsOnIntMinusPBCNum->data[i + 1] = numEonF->data[i];
  }

  i = FacetsOnInterfaceNum->size[0];
  FacetsOnInterfaceNum->size[0] = FacetsOnIntMinusPBCNum->size[0];
  emxEnsureCapacity_real_T(sp, FacetsOnInterfaceNum, i, &wh_emlrtRTEI);
  loop_ub = FacetsOnIntMinusPBCNum->size[0];
  for (i = 0; i < loop_ub; i++) {
    FacetsOnInterfaceNum->data[i] = FacetsOnIntMinusPBCNum->data[i];
  }

  i = (int32_T)*numinttype;
  emlrtForLoopVectorCheckR2012b(1.0, 1.0, *numinttype, mxDOUBLE_CLASS, (int32_T)*
    numinttype, &rb_emlrtRTEI, sp);
  for (notdone = 0; notdone < i; notdone++) {
    if (((int32_T)(notdone + 1U) < 1) || ((int32_T)(notdone + 1U) >
         FacetsOnInterfaceNum->size[0])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)(notdone + 1U), 1,
        FacetsOnInterfaceNum->size[0], &tl_emlrtBCI, sp);
    }

    if (((int32_T)(notdone + 2U) < 1) || ((int32_T)(notdone + 2U) >
         FacetsOnInterfaceNum->size[0])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)(notdone + 2U), 1,
        FacetsOnInterfaceNum->size[0], &ul_emlrtBCI, sp);
    }

    FacetsOnInterfaceNum->data[notdone + 1] += FacetsOnInterfaceNum->
      data[notdone];
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  /*  The actual facet identifiers for interface type regI are then: */
  /*  locF = FacetsOnInterfaceNum(regI):(FacetsOnInterfaceNum(regI+1)-1); */
  /*  facs = FacetsOnInterface(locF); */
  /*  true = all(ElementsOnFacet2(facs,1:4) == ElementsOnFacet(1:numEonF(regI),1:4,regI)); */
  st.site = &hc_emlrtRSI;
  if (muDoubleScalarIsNaN(usePBC)) {
    emlrtErrorWithMessageIdR2018a(&st, &mb_emlrtRTEI, "MATLAB:nologicalnan",
      "MATLAB:nologicalnan", 0);
  }

  if (usePBC != 0.0) {
    /*  sort to find the list of PBC facets grouped by interface type */
    if (1.0 > *numfac) {
      loop_ub = 0;
    } else {
      if (1 > FacetsOnPBC->size[0]) {
        emlrtDynamicBoundsCheckR2012b(1, 1, FacetsOnPBC->size[0], &te_emlrtBCI,
          sp);
      }

      if (((int32_T)*numfac < 1) || ((int32_T)*numfac > FacetsOnPBC->size[0])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)*numfac, 1, FacetsOnPBC->size[0],
          &ue_emlrtBCI, sp);
      }

      loop_ub = (int32_T)*numfac;
    }

    b_numel[0] = 1;
    b_numel[1] = loop_ub;
    st.site = &gc_emlrtRSI;
    indexShapeCheck(&st, FacetsOnPBC->size[0], b_numel);
    st.site = &gc_emlrtRSI;
    i = MorePBC->size[0];
    MorePBC->size[0] = loop_ub;
    emxEnsureCapacity_real_T(&st, MorePBC, i, &de_emlrtRTEI);
    for (i = 0; i < loop_ub; i++) {
      MorePBC->data[i] = FacetsOnPBC->data[i];
    }

    b_st.site = &tf_emlrtRSI;
    sort(&b_st, MorePBC, indx);
    i = MorePBC->size[0];
    MorePBC->size[0] = indx->size[0];
    emxEnsureCapacity_real_T(&st, MorePBC, i, &uh_emlrtRTEI);
    loop_ub = indx->size[0];
    for (i = 0; i < loop_ub; i++) {
      MorePBC->data[i] = indx->data[i];
    }

    d = *numfac - numFPBC;
    if (d + 1.0 > *numfac) {
      i = 0;
      i1 = 0;
    } else {
      if (((int32_T)(d + 1.0) < 1) || ((int32_T)(d + 1.0) > MorePBC->size[0])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(d + 1.0), 1, MorePBC->size[0],
          &ve_emlrtBCI, sp);
      }

      i = (int32_T)(d + 1.0) - 1;
      if (((int32_T)*numfac < 1) || ((int32_T)*numfac > MorePBC->size[0])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)*numfac, 1, MorePBC->size[0],
          &we_emlrtBCI, sp);
      }

      i1 = (int32_T)*numfac;
    }

    b_numel[0] = 1;
    nelA = i1 - i;
    b_numel[1] = nelA;
    st.site = &fc_emlrtRSI;
    indexShapeCheck(&st, MorePBC->size[0], b_numel);
    i2 = FacetsOnPBC->size[0];
    FacetsOnPBC->size[0] = nelA;
    emxEnsureCapacity_real_T(sp, FacetsOnPBC, i2, &di_emlrtRTEI);
    for (i2 = 0; i2 < nelA; i2++) {
      FacetsOnPBC->data[i2] = MorePBC->data[i + i2];
    }

    /*  delete the zeros */
    i2 = FacetsOnPBCNum->size[0];
    FacetsOnPBCNum->size[0] = numEonPBC->size[0] + 1;
    emxEnsureCapacity_real_T(sp, FacetsOnPBCNum, i2, &ei_emlrtRTEI);
    FacetsOnPBCNum->data[0] = 1.0;
    loop_ub = numEonPBC->size[0];
    for (i2 = 0; i2 < loop_ub; i2++) {
      FacetsOnPBCNum->data[i2 + 1] = numEonPBC->data[i2];
    }

    i2 = (int32_T)*numinttype;
    emlrtForLoopVectorCheckR2012b(1.0, 1.0, *numinttype, mxDOUBLE_CLASS,
      (int32_T)*numinttype, &sb_emlrtRTEI, sp);
    for (notdone = 0; notdone < i2; notdone++) {
      if (((int32_T)(notdone + 1U) < 1) || ((int32_T)(notdone + 1U) >
           FacetsOnPBCNum->size[0])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(notdone + 1U), 1,
          FacetsOnPBCNum->size[0], &om_emlrtBCI, sp);
      }

      if (((int32_T)(notdone + 2U) < 1) || ((int32_T)(notdone + 2U) >
           FacetsOnPBCNum->size[0])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(notdone + 2U), 1,
          FacetsOnPBCNum->size[0], &pm_emlrtBCI, sp);
      }

      FacetsOnPBCNum->data[notdone + 1] += FacetsOnPBCNum->data[notdone];
      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    if ((int32_T)d < 0) {
      emlrtNonNegativeCheckR2012b((int32_T)d, &td_emlrtDCI, sp);
    }

    i2 = FacetsOnIntMinusPBC->size[0];
    FacetsOnIntMinusPBC->size[0] = (int32_T)d;
    emxEnsureCapacity_real_T(sp, FacetsOnIntMinusPBC, i2, &hi_emlrtRTEI);
    loop_ub = (int32_T)(*numfac - numFPBC);
    if (loop_ub < 0) {
      emlrtNonNegativeCheckR2012b(loop_ub, &td_emlrtDCI, sp);
    }

    for (i2 = 0; i2 < loop_ub; i2++) {
      FacetsOnIntMinusPBC->data[i2] = 0.0;
    }

    i2 = (int32_T)*numinttype;
    emlrtForLoopVectorCheckR2012b(1.0, 1.0, *numinttype, mxDOUBLE_CLASS,
      (int32_T)*numinttype, &tb_emlrtRTEI, sp);
    for (notdone = 0; notdone < i2; notdone++) {
      st.site = &ec_emlrtRSI;
      i3 = notdone + 1;
      if ((i3 < 1) || (i3 > FacetsOnInterfaceNum->size[0])) {
        emlrtDynamicBoundsCheckR2012b(i3, 1, FacetsOnInterfaceNum->size[0],
          &xe_emlrtBCI, &st);
      }

      if (((int32_T)(((real_T)notdone + 1.0) + 1.0) < 1) || ((int32_T)(((real_T)
             notdone + 1.0) + 1.0) > FacetsOnInterfaceNum->size[0])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(((real_T)notdone + 1.0) + 1.0),
          1, FacetsOnInterfaceNum->size[0], &ye_emlrtBCI, &st);
      }

      b_st.site = &qm_emlrtRSI;
      d = FacetsOnInterfaceNum->data[notdone + 1] - 1.0;
      if (d < FacetsOnInterfaceNum->data[notdone]) {
        setAll->size[0] = 1;
        setAll->size[1] = 0;
      } else if ((muDoubleScalarIsInf(FacetsOnInterfaceNum->data[notdone]) ||
                  muDoubleScalarIsInf(d)) && (FacetsOnInterfaceNum->data[notdone]
                  == d)) {
        b_i = setAll->size[0] * setAll->size[1];
        setAll->size[0] = 1;
        setAll->size[1] = 1;
        emxEnsureCapacity_real_T(&b_st, setAll, b_i, &oi_emlrtRTEI);
        setAll->data[0] = rtNaN;
      } else if (FacetsOnInterfaceNum->data[notdone] ==
                 FacetsOnInterfaceNum->data[notdone]) {
        b_i = setAll->size[0] * setAll->size[1];
        setAll->size[0] = 1;
        loop_ub = (int32_T)muDoubleScalarFloor(d - FacetsOnInterfaceNum->
          data[notdone]);
        setAll->size[1] = loop_ub + 1;
        emxEnsureCapacity_real_T(&b_st, setAll, b_i, &oi_emlrtRTEI);
        for (b_i = 0; b_i <= loop_ub; b_i++) {
          setAll->data[b_i] = FacetsOnInterfaceNum->data[notdone] + (real_T)b_i;
        }
      } else {
        c_st.site = &rm_emlrtRSI;
        eml_float_colon(&c_st, FacetsOnInterfaceNum->data[notdone],
                        FacetsOnInterfaceNum->data[notdone + 1] - 1.0, setAll);
      }

      st.site = &dc_emlrtRSI;
      indexShapeCheck(&st, FacetsOnInterface->size[0], *(int32_T (*)[2])
                      setAll->size);
      loop_ub = setAll->size[0] * setAll->size[1];
      for (b_i = 0; b_i < loop_ub; b_i++) {
        if (setAll->data[b_i] != (int32_T)muDoubleScalarFloor(setAll->data[b_i]))
        {
          emlrtIntegerCheckR2012b(setAll->data[b_i], &wd_emlrtDCI, sp);
        }

        end = (int32_T)setAll->data[b_i];
        if ((end < 1) || (end > FacetsOnInterface->size[0])) {
          emlrtDynamicBoundsCheckR2012b(end, 1, FacetsOnInterface->size[0],
            &wm_emlrtBCI, sp);
        }
      }

      st.site = &cc_emlrtRSI;
      if (i3 > FacetsOnPBCNum->size[0]) {
        emlrtDynamicBoundsCheckR2012b(i3, 1, FacetsOnPBCNum->size[0],
          &af_emlrtBCI, &st);
      }

      if (((int32_T)(((real_T)notdone + 1.0) + 1.0) < 1) || ((int32_T)(((real_T)
             notdone + 1.0) + 1.0) > FacetsOnPBCNum->size[0])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(((real_T)notdone + 1.0) + 1.0),
          1, FacetsOnPBCNum->size[0], &bf_emlrtBCI, &st);
      }

      b_st.site = &qm_emlrtRSI;
      d = FacetsOnPBCNum->data[notdone + 1] - 1.0;
      if (d < FacetsOnPBCNum->data[notdone]) {
        setPBC->size[0] = 1;
        setPBC->size[1] = 0;
      } else if ((muDoubleScalarIsInf(FacetsOnPBCNum->data[notdone]) ||
                  muDoubleScalarIsInf(d)) && (FacetsOnPBCNum->data[notdone] == d))
      {
        b_i = setPBC->size[0] * setPBC->size[1];
        setPBC->size[0] = 1;
        setPBC->size[1] = 1;
        emxEnsureCapacity_real_T(&b_st, setPBC, b_i, &ti_emlrtRTEI);
        setPBC->data[0] = rtNaN;
      } else if (FacetsOnPBCNum->data[notdone] == FacetsOnPBCNum->data[notdone])
      {
        b_i = setPBC->size[0] * setPBC->size[1];
        setPBC->size[0] = 1;
        loop_ub = (int32_T)muDoubleScalarFloor(d - FacetsOnPBCNum->data[notdone]);
        setPBC->size[1] = loop_ub + 1;
        emxEnsureCapacity_real_T(&b_st, setPBC, b_i, &ti_emlrtRTEI);
        for (b_i = 0; b_i <= loop_ub; b_i++) {
          setPBC->data[b_i] = FacetsOnPBCNum->data[notdone] + (real_T)b_i;
        }
      } else {
        c_st.site = &rm_emlrtRSI;
        eml_float_colon(&c_st, FacetsOnPBCNum->data[notdone],
                        FacetsOnPBCNum->data[notdone + 1] - 1.0, setPBC);
      }

      st.site = &bc_emlrtRSI;
      indexShapeCheck(&st, i1 - i, *(int32_T (*)[2])setPBC->size);
      loop_ub = setPBC->size[0] * setPBC->size[1];
      for (b_i = 0; b_i < loop_ub; b_i++) {
        if (setPBC->data[b_i] != (int32_T)muDoubleScalarFloor(setPBC->data[b_i]))
        {
          emlrtIntegerCheckR2012b(setPBC->data[b_i], &de_emlrtDCI, sp);
        }

        end = (int32_T)setPBC->data[b_i];
        if ((end < 1) || (end > nelA)) {
          emlrtDynamicBoundsCheckR2012b(end, 1, nelA, &kn_emlrtBCI, sp);
        }
      }

      st.site = &ac_emlrtRSI;
      b_st.site = &lh_emlrtRSI;
      b_i = ElementsOnNodePBCNum->size[0];
      ElementsOnNodePBCNum->size[0] = setAll->size[1];
      emxEnsureCapacity_real_T(&b_st, ElementsOnNodePBCNum, b_i, &wi_emlrtRTEI);
      loop_ub = setAll->size[1];
      for (b_i = 0; b_i < loop_ub; b_i++) {
        ElementsOnNodePBCNum->data[b_i] = FacetsOnInterface->data[(int32_T)
          setAll->data[b_i] - 1];
      }

      b_i = b_MorePBC->size[0];
      b_MorePBC->size[0] = setPBC->size[1];
      emxEnsureCapacity_real_T(&b_st, b_MorePBC, b_i, &yi_emlrtRTEI);
      loop_ub = setPBC->size[1];
      for (b_i = 0; b_i < loop_ub; b_i++) {
        b_MorePBC->data[b_i] = MorePBC->data[(i + (int32_T)setPBC->data[b_i]) -
          1];
      }

      c_st.site = &mh_emlrtRSI;
      b_do_vectors(&c_st, ElementsOnNodePBCNum, b_MorePBC, setnPBC, indx,
                   ib_size);
      numSI_tmp = setnPBC->size[0];
      if (((int32_T)(notdone + 1U) < 1) || ((int32_T)(notdone + 1U) >
           FacetsOnIntMinusPBCNum->size[0])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(notdone + 1U), 1,
          FacetsOnIntMinusPBCNum->size[0], &nn_emlrtBCI, sp);
      }

      if (((int32_T)(notdone + 2U) < 1) || ((int32_T)(notdone + 2U) >
           FacetsOnIntMinusPBCNum->size[0])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(notdone + 2U), 1,
          FacetsOnIntMinusPBCNum->size[0], &on_emlrtBCI, sp);
      }

      FacetsOnIntMinusPBCNum->data[notdone + 1] = FacetsOnIntMinusPBCNum->
        data[notdone] + (real_T)setnPBC->size[0];
      if (setnPBC->size[0] > 0) {
        if (i3 > FacetsOnIntMinusPBCNum->size[0]) {
          emlrtDynamicBoundsCheckR2012b(i3, 1, FacetsOnIntMinusPBCNum->size[0],
            &si_emlrtBCI, sp);
        }

        if (((int32_T)(((real_T)notdone + 1.0) + 1.0) < 1) || ((int32_T)
             (((real_T)notdone + 1.0) + 1.0) > FacetsOnIntMinusPBCNum->size[0]))
        {
          emlrtDynamicBoundsCheckR2012b((int32_T)(((real_T)notdone + 1.0) + 1.0),
            1, FacetsOnIntMinusPBCNum->size[0], &ti_emlrtBCI, sp);
        }

        d = FacetsOnIntMinusPBCNum->data[notdone + 1] - 1.0;
        if (FacetsOnIntMinusPBCNum->data[notdone] > d) {
          i3 = -1;
          b_i = -1;
        } else {
          i3 = (int32_T)FacetsOnIntMinusPBCNum->data[notdone];
          if ((i3 < 1) || (i3 > FacetsOnIntMinusPBC->size[0])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)FacetsOnIntMinusPBCNum->
              data[notdone], 1, FacetsOnIntMinusPBC->size[0], &ui_emlrtBCI, sp);
          }

          i3 -= 2;
          if (((int32_T)d < 1) || ((int32_T)d > FacetsOnIntMinusPBC->size[0])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)d, 1,
              FacetsOnIntMinusPBC->size[0], &vi_emlrtBCI, sp);
          }

          b_i = (int32_T)d - 1;
        }

        loop_ub = b_i - i3;
        if (loop_ub != setnPBC->size[0]) {
          emlrtSubAssignSizeCheck1dR2017a(loop_ub, setnPBC->size[0], &n_emlrtECI,
            sp);
        }

        for (b_i = 0; b_i < loop_ub; b_i++) {
          FacetsOnIntMinusPBC->data[(i3 + b_i) + 1] = setnPBC->data[b_i];
        }
      }

      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }
  } else {
    i = FacetsOnPBCNum->size[0];
    FacetsOnPBCNum->size[0] = 1;
    emxEnsureCapacity_real_T(sp, FacetsOnPBCNum, i, &xh_emlrtRTEI);
    FacetsOnPBCNum->data[0] = 0.0;
    i = FacetsOnIntMinusPBC->size[0];
    FacetsOnIntMinusPBC->size[0] = 1;
    emxEnsureCapacity_real_T(sp, FacetsOnIntMinusPBC, i, &yh_emlrtRTEI);
    FacetsOnIntMinusPBC->data[0] = 0.0;
    i = FacetsOnIntMinusPBCNum->size[0];
    FacetsOnIntMinusPBCNum->size[0] = 1;
    emxEnsureCapacity_real_T(sp, FacetsOnIntMinusPBCNum, i, &ai_emlrtRTEI);
    FacetsOnIntMinusPBCNum->data[0] = 0.0;
  }

  /*  Find material interfaces and duplicate nodes */
  ElementsOnNodeA->size[0] = 0;
  ElementsOnNodeA->size[1] = 1;
  i = (int32_T)(nummat + -1.0);
  emlrtForLoopVectorCheckR2012b(2.0, 1.0, nummat, mxDOUBLE_CLASS, (int32_T)
    (nummat + -1.0), &ub_emlrtRTEI, sp);
  emxInitStruct_sparse3(sp, &c_expl_temp, &gk_emlrtRTEI, true);
  emxInitStruct_sparse3(sp, &d_expl_temp, &bk_emlrtRTEI, true);
  emxInitStruct_sparse3(sp, &e_expl_temp, &ci_emlrtRTEI, true);
  for (nume = 0; nume < i; nume++) {
    for (end = 0; end <= nume; end++) {
      /*  ID for material pair (row=mat2, col=mat1) */
      if (((int32_T)(nume + 2U) < 1) || ((int32_T)(nume + 2U) > InterTypes->
           size[0])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(nume + 2U), 1, InterTypes->size
          [0], &km_emlrtBCI, sp);
      }

      if (((int32_T)(end + 1U) < 1) || ((int32_T)(end + 1U) > InterTypes->size[1]))
      {
        emlrtDynamicBoundsCheckR2012b((int32_T)(end + 1U), 1, InterTypes->size[1],
          &km_emlrtBCI, sp);
      }

      if (InterTypes->data[(nume + InterTypes->size[0] * end) + 1] > 0.0) {
        /*  Mark nodes on the interfaces */
        st.site = &yb_emlrtRSI;
        c_sparse_parenReference(&st, NodeReg->d, NodeReg->colidx,
          NodeReg->rowidx, NodeReg->m, NodeReg->n, (real_T)end + 1.0,
          &NodeRegSum);
        st.site = &yb_emlrtRSI;
        sparse_gt(&st, NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  NodeRegSum.m, &d_expl_temp);
        st.site = &yb_emlrtRSI;
        c_sparse_parenReference(&st, NodeReg->d, NodeReg->colidx,
          NodeReg->rowidx, NodeReg->m, NodeReg->n, (real_T)nume + 2.0,
          &NodeRegSum);
        st.site = &yb_emlrtRSI;
        sparse_gt(&st, NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  NodeRegSum.m, &e_expl_temp);
        i1 = c_expl_temp.colidx->size[0];
        c_expl_temp.colidx->size[0] = e_expl_temp.colidx->size[0];
        emxEnsureCapacity_int32_T(sp, c_expl_temp.colidx, i1, &ci_emlrtRTEI);
        loop_ub = e_expl_temp.colidx->size[0];
        for (i1 = 0; i1 < loop_ub; i1++) {
          c_expl_temp.colidx->data[i1] = e_expl_temp.colidx->data[i1];
        }

        i1 = c_expl_temp.rowidx->size[0];
        c_expl_temp.rowidx->size[0] = e_expl_temp.rowidx->size[0];
        emxEnsureCapacity_int32_T(sp, c_expl_temp.rowidx, i1, &ci_emlrtRTEI);
        loop_ub = e_expl_temp.rowidx->size[0];
        for (i1 = 0; i1 < loop_ub; i1++) {
          c_expl_temp.rowidx->data[i1] = e_expl_temp.rowidx->data[i1];
        }

        st.site = &yb_emlrtRSI;
        sparse_and(&st, d_expl_temp.d, d_expl_temp.colidx, d_expl_temp.rowidx,
                   d_expl_temp.m, e_expl_temp.d, c_expl_temp.colidx,
                   c_expl_temp.rowidx, e_expl_temp.m, b_NodesOnPBCnum, indx, ii,
                   &notdone);
        st.site = &yb_emlrtRSI;
        c_sparse_parenReference(&st, NodeReg->d, NodeReg->colidx,
          NodeReg->rowidx, NodeReg->m, NodeReg->n, (real_T)nume + 2.0,
          &NodeRegSum);
        st.site = &yb_emlrtRSI;
        c_sparse_parenReference(&st, NodeReg->d, NodeReg->colidx,
          NodeReg->rowidx, NodeReg->m, NodeReg->n, (real_T)end + 1.0,
          &b_expl_temp);
        st.site = &yb_emlrtRSI;
        sparse_eq(&st, NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  NodeRegSum.m, b_expl_temp.d, b_expl_temp.colidx,
                  b_expl_temp.rowidx, b_expl_temp.m, d_expl_temp.d,
                  d_expl_temp.colidx, d_expl_temp.rowidx, &nelA);
        st.site = &yb_emlrtRSI;
        sparse_and(&st, b_NodesOnPBCnum, indx, ii, notdone, d_expl_temp.d,
                   d_expl_temp.colidx, d_expl_temp.rowidx, nelA, e_expl_temp.d,
                   c_expl_temp.colidx, c_expl_temp.rowidx, &numPBC);
        st.site = &xb_emlrtRSI;
        b_st.site = &ug_emlrtRSI;
        g_eml_find(&b_st, c_expl_temp.colidx, c_expl_temp.rowidx, numPBC, ii);
        loop_ub = ElementsOnNodeA->size[0];
        i1 = NodesOnInterface->size[0];
        NodesOnInterface->size[0] = ElementsOnNodeA->size[0] + ii->size[0];
        emxEnsureCapacity_real_T(sp, NodesOnInterface, i1, &ii_emlrtRTEI);
        for (i1 = 0; i1 < loop_ub; i1++) {
          NodesOnInterface->data[i1] = ElementsOnNodeA->data[i1];
        }

        loop_ub = ii->size[0];
        for (i1 = 0; i1 < loop_ub; i1++) {
          NodesOnInterface->data[i1 + ElementsOnNodeA->size[0]] = ii->data[i1];
        }

        i1 = ElementsOnNodeA->size[0] * ElementsOnNodeA->size[1];
        ElementsOnNodeA->size[0] = NodesOnInterface->size[0];
        ElementsOnNodeA->size[1] = 1;
        emxEnsureCapacity_real_T(sp, ElementsOnNodeA, i1, &ki_emlrtRTEI);
        loop_ub = NodesOnInterface->size[0];
        for (i1 = 0; i1 < loop_ub; i1++) {
          ElementsOnNodeA->data[i1] = NodesOnInterface->data[i1];
        }
      }

      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  emxFreeStruct_sparse(&b_expl_temp);
  emxFreeStruct_sparse3(&e_expl_temp);
  st.site = &wb_emlrtRSI;
  if (muDoubleScalarIsNaN(usePBC)) {
    emlrtErrorWithMessageIdR2018a(&st, &mb_emlrtRTEI, "MATLAB:nologicalnan",
      "MATLAB:nologicalnan", 0);
  }

  if (usePBC != 0.0) {
    /*  Add PBC nodes into duplicating list */
    st.site = &vb_emlrtRSI;
    sum(&st, NodeReg->d, NodeReg->colidx, NodeReg->rowidx, NodeReg->m,
        NodeReg->n, &NodeRegSum);

    /*  from zipped nodes, ONLY the one with regions attached is in the zipped connectivity */
    st.site = &ub_emlrtRSI;
    i = b_NodesOnPBCnum->size[0];
    b_NodesOnPBCnum->size[0] = NodesOnPBCnum->size[0];
    emxEnsureCapacity_boolean_T(&st, b_NodesOnPBCnum, i, &bi_emlrtRTEI);
    loop_ub = NodesOnPBCnum->size[0];
    for (i = 0; i < loop_ub; i++) {
      b_NodesOnPBCnum->data[i] = (NodesOnPBCnum->data[i] > 0.0);
    }

    b_st.site = &ug_emlrtRSI;
    eml_find(&b_st, b_NodesOnPBCnum, ii);
    st.site = &tb_emlrtRSI;
    b_st.site = &tb_emlrtRSI;
    sparse_gt(&b_st, NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
              NodeRegSum.m, &c_expl_temp);
    b_st.site = &ug_emlrtRSI;
    g_eml_find(&b_st, c_expl_temp.colidx, c_expl_temp.rowidx, c_expl_temp.m,
               c_ii);
    st.site = &sb_emlrtRSI;
    b_st.site = &io_emlrtRSI;
    i = ElementsOnNodePBCNum->size[0];
    ElementsOnNodePBCNum->size[0] = ii->size[0];
    emxEnsureCapacity_real_T(&b_st, ElementsOnNodePBCNum, i, &gi_emlrtRTEI);
    loop_ub = ii->size[0];
    for (i = 0; i < loop_ub; i++) {
      ElementsOnNodePBCNum->data[i] = ii->data[i];
    }

    i = b_MorePBC->size[0];
    b_MorePBC->size[0] = c_ii->size[0];
    emxEnsureCapacity_real_T(&b_st, b_MorePBC, i, &gi_emlrtRTEI);
    loop_ub = c_ii->size[0];
    for (i = 0; i < loop_ub; i++) {
      b_MorePBC->data[i] = c_ii->data[i];
    }

    c_st.site = &mh_emlrtRSI;
    c_do_vectors(&c_st, ElementsOnNodePBCNum, b_MorePBC, MorePBC, indx, ib);
    i = NodesOnInterface->size[0];
    NodesOnInterface->size[0] = ElementsOnNodeA->size[0] + MorePBC->size[0];
    emxEnsureCapacity_real_T(sp, NodesOnInterface, i, &ji_emlrtRTEI);
    loop_ub = ElementsOnNodeA->size[0];
    for (i = 0; i < loop_ub; i++) {
      NodesOnInterface->data[i] = ElementsOnNodeA->data[i];
    }

    loop_ub = MorePBC->size[0];
    for (i = 0; i < loop_ub; i++) {
      NodesOnInterface->data[i + ElementsOnNodeA->size[0]] = MorePBC->data[i];
    }

    i = ElementsOnNodeA->size[0] * ElementsOnNodeA->size[1];
    ElementsOnNodeA->size[0] = NodesOnInterface->size[0];
    ElementsOnNodeA->size[1] = 1;
    emxEnsureCapacity_real_T(sp, ElementsOnNodeA, i, &li_emlrtRTEI);
    loop_ub = NodesOnInterface->size[0];
    for (i = 0; i < loop_ub; i++) {
      ElementsOnNodeA->data[i] = NodesOnInterface->data[i];
    }

    /*  add these zipped PBC nodes into the list for duplicates too */
  }

  emxFree_int32_T(&ib);
  emxInit_real_T(sp, &interreg2, 2, &fi_emlrtRTEI, true);
  st.site = &rb_emlrtRSI;
  ia_size[0] = ElementsOnNodeA->size[0];
  c_nodes_data = *ElementsOnNodeA;
  b_NodesOnElement[0] = ia_size[0];
  c_nodes_data.size = &b_NodesOnElement[0];
  c_nodes_data.numDimensions = 1;
  b_st.site = &ah_emlrtRSI;
  unique_vector(&b_st, &c_nodes_data, NodesOnInterface);
  elemB = NodesOnInterface->size[0];

  /*  Now actually duplicate the nodes */
  /*  Criteria: only nodes for which ALL inter-material facs involving a given */
  /*  material are being cut, then they are duplicated. */
  if (!(nummat >= 0.0)) {
    emlrtNonNegativeCheckR2012b(nummat, &xb_emlrtDCI, sp);
  }

  d = (int32_T)muDoubleScalarFloor(nummat);
  if (nummat != d) {
    emlrtIntegerCheckR2012b(nummat, &wb_emlrtDCI, sp);
  }

  i = interreg2->size[0] * interreg2->size[1];
  interreg2->size[0] = (int32_T)nummat;
  emxEnsureCapacity_real_T(sp, interreg2, i, &fi_emlrtRTEI);
  if (nummat != d) {
    emlrtIntegerCheckR2012b(nummat, &yb_emlrtDCI, sp);
  }

  i = interreg2->size[0] * interreg2->size[1];
  interreg2->size[1] = (int32_T)nummat;
  emxEnsureCapacity_real_T(sp, interreg2, i, &fi_emlrtRTEI);
  if (nummat != d) {
    emlrtIntegerCheckR2012b(nummat, &vd_emlrtDCI, sp);
  }

  loop_ub = (int32_T)nummat * (int32_T)nummat;
  for (i = 0; i < loop_ub; i++) {
    interreg2->data[i] = 0.0;
  }

  i = (int32_T)nummat;
  emlrtForLoopVectorCheckR2012b(1.0, 1.0, nummat, mxDOUBLE_CLASS, (int32_T)
    nummat, &vb_emlrtRTEI, sp);
  for (nume = 0; nume < i; nume++) {
    for (end = 0; end <= nume; end++) {
      if (((int32_T)(nume + 1U) < 1) || ((int32_T)(nume + 1U) > interreg2->size
           [0])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(nume + 1U), 1, interreg2->size[0],
          &tm_emlrtBCI, sp);
      }

      if (((int32_T)(end + 1U) < 1) || ((int32_T)(end + 1U) > interreg2->size[1]))
      {
        emlrtDynamicBoundsCheckR2012b((int32_T)(end + 1U), 1, interreg2->size[1],
          &tm_emlrtBCI, sp);
      }

      numA = ((real_T)nume + 1.0) * (((real_T)nume + 1.0) - 1.0) / 2.0 +
        ((real_T)end + 1.0);
      interreg2->data[nume + interreg2->size[0] * end] = numA;
      if (((int32_T)(end + 1U) < 1) || ((int32_T)(end + 1U) > interreg2->size[0]))
      {
        emlrtDynamicBoundsCheckR2012b((int32_T)(end + 1U), 1, interreg2->size[0],
          &vm_emlrtBCI, sp);
      }

      if (((int32_T)(nume + 1U) < 1) || ((int32_T)(nume + 1U) > interreg2->size
           [1])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(nume + 1U), 1, interreg2->size[1],
          &vm_emlrtBCI, sp);
      }

      interreg2->data[end + interreg2->size[0] * nume] = numA;
      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  b_i2 = *numnp;
  i = NodesOnInterface->size[0];
  emxInitStruct_sparse2(sp, &f_expl_temp, &ek_emlrtRTEI, true);
  emxInitStruct_sparse2(sp, &g_expl_temp, &ik_emlrtRTEI, true);
  emxInitStruct_sparse2(sp, &h_expl_temp, &jk_emlrtRTEI, true);
  for (locN = 0; locN < i; locN++) {
    if ((locN + 1 < 1) || (locN + 1 > NodesOnInterface->size[0])) {
      emlrtDynamicBoundsCheckR2012b(locN + 1, 1, NodesOnInterface->size[0],
        &um_emlrtBCI, sp);
    }

    node = NodesOnInterface->data[locN];

    /*      matnode = sum(NodeMat(node,:)>0); */
    i1 = (int32_T)NodesOnInterface->data[locN];
    if ((i1 < 1) || (i1 > FacetsOnNodeNum->size[0])) {
      emlrtDynamicBoundsCheckR2012b(i1, 1, FacetsOnNodeNum->size[0],
        &cf_emlrtBCI, sp);
    }

    d = FacetsOnNodeNum->data[i1 - 1];
    guard1 = false;
    guard2 = false;
    if (d == 0.0) {
      /*  midside node */
      st.site = &qb_emlrtRSI;
      if (muDoubleScalarIsNaN(usePBC)) {
        emlrtErrorWithMessageIdR2018a(&st, &mb_emlrtRTEI, "MATLAB:nologicalnan",
          "MATLAB:nologicalnan", 0);
      }

      if (usePBC != 0.0) {
        if (i1 > NodesOnPBCnum->size[0]) {
          emlrtDynamicBoundsCheckR2012b(i1, 1, NodesOnPBCnum->size[0],
            &in_emlrtBCI, sp);
        }

        if (NodesOnPBCnum->data[i1 - 1] > 0.0) {
          /*  handle copies of PBC nodes specially */
          /*  Just get the old node numbers back and copy them into place */
          if (i1 > ElementsOnNodeNum->size[0]) {
            emlrtDynamicBoundsCheckR2012b(i1, 1, ElementsOnNodeNum->size[0],
              &df_emlrtBCI, sp);
          }

          st.site = &pb_emlrtRSI;
          c_sparse_parenReference(&st, ElementsOnNode->d, ElementsOnNode->colidx,
            ElementsOnNode->rowidx, ElementsOnNode->m, ElementsOnNode->n,
            NodesOnInterface->data[locN], &NodeRegSum);
          st.site = &pb_emlrtRSI;
          b_sparse_full(&st, NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                        NodeRegSum.m, ElementsOnNodeA);

          /*  Loop over all elements attached to node */
          i1 = (int32_T)ElementsOnNodeNum->data[i1 - 1];
          emlrtForLoopVectorCheckR2012b(1.0, 1.0, ElementsOnNodeNum->data
            [(int32_T)NodesOnInterface->data[locN] - 1], mxDOUBLE_CLASS,
            (int32_T)ElementsOnNodeNum->data[(int32_T)NodesOnInterface->
            data[locN] - 1], &wb_emlrtRTEI, sp);
          if (0 <= i1 - 1) {
            if (1.0 > nen) {
              c_loop_ub = 0;
            } else {
              if (1 > NodesOnElementCG->size[1]) {
                emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementCG->size[1],
                  &gf_emlrtBCI, sp);
              }

              if ((int32_T)nen > NodesOnElementCG->size[1]) {
                emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1,
                  NodesOnElementCG->size[1], &hf_emlrtBCI, sp);
              }

              c_loop_ub = (int32_T)nen;
            }

            b_NodesOnElementPBC = NodesOnElementPBC->size[1];
            b_numel[0] = 1;
            nodeC2_size[0] = 1;
            nodeC2_size[1] = c_loop_ub;
            b_nodeC2_size[0] = 1;
          }

          for (trueCount = 0; trueCount < i1; trueCount++) {
            i2 = trueCount + 1;
            if ((i2 < 1) || (i2 > ElementsOnNodeA->size[0])) {
              emlrtDynamicBoundsCheckR2012b(i2, 1, ElementsOnNodeA->size[0],
                &ef_emlrtBCI, sp);
            }

            /*  Reset nodal ID for element in higher material ID */
            st.site = &ob_emlrtRSI;
            if (ElementsOnNodeA->data[trueCount] != (int32_T)muDoubleScalarFloor
                (ElementsOnNodeA->data[trueCount])) {
              emlrtIntegerCheckR2012b(ElementsOnNodeA->data[trueCount],
                &w_emlrtDCI, &st);
            }

            notdone = (int32_T)ElementsOnNodeA->data[trueCount];
            if ((notdone < 1) || (notdone > NodesOnElementCG->size[0])) {
              emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnElementCG->size[0],
                &ff_emlrtBCI, &st);
            }

            for (i2 = 0; i2 < c_loop_ub; i2++) {
              b_nodeC2_data[i2] = (NodesOnElementCG->data[(notdone +
                NodesOnElementCG->size[0] * i2) - 1] == node);
            }

            b_st.site = &ug_emlrtRSI;
            f_eml_find(&b_st, b_nodeC2_data, nodeC2_size, ii_data, ii_size);
            loop_ub = ii_size[1];
            if (0 <= loop_ub - 1) {
              memcpy(&b_ii_data[0], &ii_data[0], loop_ub * sizeof(int32_T));
            }

            notdone = (int32_T)ElementsOnNodeA->data[trueCount];
            if ((notdone < 1) || (notdone > NodesOnElementPBC->size[0])) {
              emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnElementPBC->size
                [0], &if_emlrtBCI, sp);
            }

            for (i2 = 0; i2 < loop_ub; i2++) {
              i3 = b_ii_data[i2];
              if ((i3 < 1) || (i3 > b_NodesOnElementPBC)) {
                emlrtDynamicBoundsCheckR2012b(i3, 1, b_NodesOnElementPBC,
                  &rn_emlrtBCI, sp);
              }
            }

            if (notdone > NodesOnElementDG->size[0]) {
              emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnElementDG->size[0],
                &ai_emlrtBCI, sp);
            }

            for (i2 = 0; i2 < loop_ub; i2++) {
              i3 = b_ii_data[i2];
              if (((int8_T)i3 < 1) || ((int8_T)i3 > NodesOnElementDG->size[1]))
              {
                emlrtDynamicBoundsCheckR2012b((int8_T)i3, 1,
                  NodesOnElementDG->size[1], &sn_emlrtBCI, sp);
              }

              b_tmp_data[i2] = (int8_T)((int8_T)i3 - 1);
            }

            b_nodeC2_size[1] = ii_size[1];
            b_numel[1] = ii_size[1];
            emlrtSubAssignSizeCheckR2012b(&b_numel[0], 2, &b_nodeC2_size[0], 2,
              &i_emlrtECI, sp);
            for (i2 = 0; i2 < loop_ub; i2++) {
              NodesOnElementDG->data[(notdone + NodesOnElementDG->size[0] *
                b_tmp_data[i2]) - 1] = NodesOnElementPBC->data[(notdone +
                NodesOnElementPBC->size[0] * (b_ii_data[i2] - 1)) - 1];
            }

            if (*emlrtBreakCheckR2012bFlagVar != 0) {
              emlrtBreakCheckR2012b(sp);
            }
          }
        } else {
          guard2 = true;
        }
      } else {
        guard2 = true;
      }
    } else {
      /*  All corner nodes */
      /*  form secs; a sec is a contiguous region of elements that is not */
      /*  cut apart by any CZM facs */
      /*  start by assuming all elements are separated */
      if (i1 > ElementsOnNodeNum->size[0]) {
        emlrtDynamicBoundsCheckR2012b(i1, 1, ElementsOnNodeNum->size[0],
          &hn_emlrtBCI, sp);
      }

      b_i1 = ElementsOnNodeNum->data[i1 - 1];
      d1 = ElementsOnNodeNum->data[i1 - 1];
      numA = (int32_T)muDoubleScalarFloor(d1);
      if (d1 != numA) {
        emlrtIntegerCheckR2012b(d1, &y_emlrtDCI, sp);
      }

      i2 = interreg2->size[0] * interreg2->size[1];
      interreg2->size[0] = (int32_T)d1;
      interreg2->size[1] = (int32_T)ElementsOnNodeNum->data[i1 - 1];
      emxEnsureCapacity_real_T(sp, interreg2, i2, &si_emlrtRTEI);
      loop_ub = (int32_T)ElementsOnNodeNum->data[i1 - 1] * (int32_T)
        ElementsOnNodeNum->data[i1 - 1];
      for (i2 = 0; i2 < loop_ub; i2++) {
        interreg2->data[i2] = 0.0;
      }

      if (d1 < 1.0) {
        setAll->size[0] = 1;
        setAll->size[1] = 0;
      } else if (muDoubleScalarIsInf(d1) && (1.0 == d1)) {
        i2 = setAll->size[0] * setAll->size[1];
        setAll->size[0] = 1;
        setAll->size[1] = 1;
        emxEnsureCapacity_real_T(sp, setAll, i2, &nh_emlrtRTEI);
        setAll->data[0] = rtNaN;
      } else {
        i2 = setAll->size[0] * setAll->size[1];
        setAll->size[0] = 1;
        loop_ub = (int32_T)muDoubleScalarFloor(d1 - 1.0);
        setAll->size[1] = loop_ub + 1;
        emxEnsureCapacity_real_T(sp, setAll, i2, &nh_emlrtRTEI);
        for (i2 = 0; i2 <= loop_ub; i2++) {
          setAll->data[i2] = (real_T)i2 + 1.0;
        }
      }

      st.site = &ib_emlrtRSI;
      b_sparse_parenReference(&st, ElementsOnNode->d, ElementsOnNode->colidx,
        ElementsOnNode->rowidx, ElementsOnNode->m, ElementsOnNode->n, setAll,
        NodesOnInterface->data[locN], NodeRegSum.d, NodeRegSum.colidx,
        NodeRegSum.rowidx, &NodeRegSum.m);
      st.site = &ib_emlrtRSI;
      b_sparse_full(&st, NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                    NodeRegSum.m, ElementsOnNodeA);
      i2 = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      setAll->size[1] = ElementsOnNodeA->size[0];
      emxEnsureCapacity_real_T(sp, setAll, i2, &vi_emlrtRTEI);
      loop_ub = ElementsOnNodeA->size[0];
      for (i2 = 0; i2 < loop_ub; i2++) {
        setAll->data[i2] = ElementsOnNodeA->data[i2];
      }

      if (1.0 > d1) {
        i3 = 0;
      } else {
        i2 = (int32_T)ElementsOnNodeNum->data[i1 - 1];
        if (1 > i2) {
          emlrtDynamicBoundsCheckR2012b(1, 1, i2, &rf_emlrtBCI, sp);
        }

        i2 = (int32_T)ElementsOnNodeNum->data[i1 - 1];
        i3 = (int32_T)ElementsOnNodeNum->data[i1 - 1];
        if ((i3 < 1) || (i3 > i2)) {
          emlrtDynamicBoundsCheckR2012b(i3, 1, i2, &sf_emlrtBCI, sp);
        }
      }

      i2 = (int32_T)ElementsOnNodeNum->data[i1 - 1];
      if (1 > i2) {
        emlrtDynamicBoundsCheckR2012b(1, 1, i2, &qf_emlrtBCI, sp);
      }

      loop_ub = setAll->size[1];
      i2 = setPBC->size[0] * setPBC->size[1];
      setPBC->size[0] = 1;
      setPBC->size[1] = setAll->size[1];
      emxEnsureCapacity_real_T(sp, setPBC, i2, &bj_emlrtRTEI);
      for (i2 = 0; i2 < loop_ub; i2++) {
        setPBC->data[i2] = setAll->data[i2];
      }

      b_numel[0] = 1;
      b_numel[1] = i3;
      emlrtSubAssignSizeCheckR2012b(&b_numel[0], 2, &setPBC->size[0], 2,
        &g_emlrtECI, sp);
      loop_ub = setAll->size[1];
      for (i2 = 0; i2 < loop_ub; i2++) {
        interreg2->data[interreg2->size[0] * i2] = setAll->data[i2];
      }

      if (d1 != numA) {
        emlrtIntegerCheckR2012b(d1, &ee_emlrtDCI, sp);
      }

      i2 = setnPBC->size[0];
      setnPBC->size[0] = (int32_T)d1;
      emxEnsureCapacity_real_T(sp, setnPBC, i2, &fj_emlrtRTEI);
      if (d1 != numA) {
        emlrtIntegerCheckR2012b(d1, &ee_emlrtDCI, sp);
      }

      loop_ub = (int32_T)d1;
      for (i2 = 0; i2 < loop_ub; i2++) {
        setnPBC->data[i2] = 1.0;
      }

      i2 = (int32_T)d;
      emlrtForLoopVectorCheckR2012b(1.0, 1.0, FacetsOnNodeNum->data[(int32_T)
        NodesOnInterface->data[locN] - 1], mxDOUBLE_CLASS, (int32_T)
        FacetsOnNodeNum->data[(int32_T)NodesOnInterface->data[locN] - 1],
        &ac_emlrtRTEI, sp);
      for (locF = 0; locF < i2; locF++) {
        st.site = &hb_emlrtRSI;
        d_sparse_parenReference(&st, FacetsOnNodeInt->d, FacetsOnNodeInt->colidx,
          FacetsOnNodeInt->rowidx, FacetsOnNodeInt->m, FacetsOnNodeInt->n,
          (real_T)locF + 1.0, node, b_MorePBC, c_ii, indx);
        st.site = &gb_emlrtRSI;
        b_sparse_gt(&st, b_MorePBC, c_ii, h_expl_temp.d, h_expl_temp.colidx,
                    h_expl_temp.rowidx);
        st.site = &gb_emlrtRSI;
        if (c_sparse_full(&st, h_expl_temp.d, h_expl_temp.colidx)) {
          /*  exclude internal facs */
          /*                  intramattrue = ~isempty(find(matI==diag(intermat2),1)); I */
          /*                  found out that the code is only putting cut in */
          /*                  nodefaccut for intermaterials, not the diagonal of */
          /*                  intermat2, so no if-test is needed */
          st.site = &fb_emlrtRSI;
          d_sparse_parenReference(&st, FacetsOnNodeCut->d,
            FacetsOnNodeCut->colidx, FacetsOnNodeCut->rowidx, FacetsOnNodeCut->m,
            FacetsOnNodeCut->n, (real_T)locF + 1.0, node, b_MorePBC, c_ii, indx);

          /*                  if ~cuttrue || intramattrue % two elements should be joined into one sector, along with their neighbors currently in the sector */
          st.site = &eb_emlrtRSI;
          sparse_not(&st, b_MorePBC, c_ii, indx, h_expl_temp.d,
                     h_expl_temp.colidx, h_expl_temp.rowidx);
          st.site = &eb_emlrtRSI;
          if (c_sparse_full(&st, h_expl_temp.d, h_expl_temp.colidx)) {
            /*  two elements should be joined into one sector, along with their neighbors currently in the sector */
            st.site = &db_emlrtRSI;
            d_sparse_parenReference(&st, FacetsOnNode->d, FacetsOnNode->colidx,
              FacetsOnNode->rowidx, FacetsOnNode->m, FacetsOnNode->n, (real_T)
              locF + 1.0, node, b_MorePBC, c_ii, indx);
            st.site = &db_emlrtRSI;
            numA = sparse_full(&st, b_MorePBC, c_ii);
            if (numA != (int32_T)muDoubleScalarFloor(numA)) {
              emlrtIntegerCheckR2012b(numA, &ge_emlrtDCI, sp);
            }

            if (((int32_T)numA < 1) || ((int32_T)numA > ElementsOnFacet->size[0]))
            {
              emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1,
                ElementsOnFacet->size[0], &yn_emlrtBCI, sp);
            }

            nelA = (int32_T)ElementsOnFacet->data[(int32_T)numA - 1];
            if (((int32_T)numA < 1) || ((int32_T)numA > ElementsOnFacet->size[0]))
            {
              emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1,
                ElementsOnFacet->size[0], &bo_emlrtBCI, sp);
            }

            numPBC = (int32_T)ElementsOnFacet->data[((int32_T)numA +
              ElementsOnFacet->size[0]) - 1];

            /*  find the sectors for each element */
            nri = 0U;
            old = false;
            while ((nri < b_i1) && (!old)) {
              nri++;
              if (((int32_T)nri < 1) || ((int32_T)nri > setnPBC->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, setnPBC->size[0],
                  &go_emlrtBCI, sp);
              }

              if (setnPBC->data[(int32_T)nri - 1] > 0.0) {
                if (((int32_T)nri < 1) || ((int32_T)nri > setnPBC->size[0])) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, setnPBC->size[0],
                    &tf_emlrtBCI, sp);
                }

                d = setnPBC->data[(int32_T)nri - 1];
                if (1.0 > d) {
                  loop_ub = 0;
                } else {
                  if (1 > interreg2->size[0]) {
                    emlrtDynamicBoundsCheckR2012b(1, 1, interreg2->size[0],
                      &uf_emlrtBCI, sp);
                  }

                  if (((int32_T)d < 1) || ((int32_T)setnPBC->data[(int32_T)nri -
                       1] > interreg2->size[0])) {
                    emlrtDynamicBoundsCheckR2012b((int32_T)setnPBC->data
                      [(int32_T)nri - 1], 1, interreg2->size[0], &vf_emlrtBCI,
                      sp);
                  }

                  loop_ub = (int32_T)setnPBC->data[(int32_T)nri - 1];
                }

                if (((int32_T)nri < 1) || ((int32_T)nri > interreg2->size[1])) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, interreg2->
                    size[1], &wf_emlrtBCI, sp);
                }

                st.site = &cb_emlrtRSI;
                i3 = b_NodesOnPBCnum->size[0];
                b_NodesOnPBCnum->size[0] = loop_ub;
                emxEnsureCapacity_boolean_T(&st, b_NodesOnPBCnum, i3,
                  &jj_emlrtRTEI);
                for (i3 = 0; i3 < loop_ub; i3++) {
                  b_NodesOnPBCnum->data[i3] = (interreg2->data[i3 +
                    interreg2->size[0] * ((int32_T)nri - 1)] == nelA);
                }

                b_st.site = &ug_emlrtRSI;
                eml_find(&b_st, b_NodesOnPBCnum, ii);

                /*  find is MUCH faster than ismember */
                i3 = ElementsOnNodePBCNum->size[0];
                ElementsOnNodePBCNum->size[0] = ii->size[0];
                emxEnsureCapacity_real_T(sp, ElementsOnNodePBCNum, i3,
                  &gi_emlrtRTEI);
                loop_ub = ii->size[0];
                for (i3 = 0; i3 < loop_ub; i3++) {
                  ElementsOnNodePBCNum->data[i3] = ii->data[i3];
                }

                st.site = &bb_emlrtRSI;
                old = any(&st, ElementsOnNodePBCNum);

                /*                          if sec1>0 */
                /*                              break */
                /*                          end */
              }

              if (*emlrtBreakCheckR2012bFlagVar != 0) {
                emlrtBreakCheckR2012b(sp);
              }
            }

            iSec2 = 0U;
            old = false;
            while ((iSec2 < b_i1) && (!old)) {
              iSec2++;
              if (((int32_T)iSec2 < 1) || ((int32_T)iSec2 > setnPBC->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)iSec2, 1, setnPBC->size[0],
                  &ko_emlrtBCI, sp);
              }

              d = setnPBC->data[(int32_T)iSec2 - 1];
              if (d > 0.0) {
                if (((int32_T)iSec2 < 1) || ((int32_T)iSec2 > setnPBC->size[0]))
                {
                  emlrtDynamicBoundsCheckR2012b((int32_T)iSec2, 1, setnPBC->
                    size[0], &xf_emlrtBCI, sp);
                }

                if (1.0 > d) {
                  loop_ub = 0;
                } else {
                  if (1 > interreg2->size[0]) {
                    emlrtDynamicBoundsCheckR2012b(1, 1, interreg2->size[0],
                      &yf_emlrtBCI, sp);
                  }

                  loop_ub = (int32_T)setnPBC->data[(int32_T)iSec2 - 1];
                  if ((loop_ub < 1) || (loop_ub > interreg2->size[0])) {
                    emlrtDynamicBoundsCheckR2012b((int32_T)setnPBC->data
                      [(int32_T)iSec2 - 1], 1, interreg2->size[0], &ag_emlrtBCI,
                      sp);
                  }
                }

                if (((int32_T)iSec2 < 1) || ((int32_T)iSec2 > interreg2->size[1]))
                {
                  emlrtDynamicBoundsCheckR2012b((int32_T)iSec2, 1,
                    interreg2->size[1], &bg_emlrtBCI, sp);
                }

                st.site = &ab_emlrtRSI;
                i3 = b_NodesOnPBCnum->size[0];
                b_NodesOnPBCnum->size[0] = loop_ub;
                emxEnsureCapacity_boolean_T(&st, b_NodesOnPBCnum, i3,
                  &kj_emlrtRTEI);
                for (i3 = 0; i3 < loop_ub; i3++) {
                  b_NodesOnPBCnum->data[i3] = (numPBC == interreg2->data[i3 +
                    interreg2->size[0] * ((int32_T)iSec2 - 1)]);
                }

                b_st.site = &ug_emlrtRSI;
                eml_find(&b_st, b_NodesOnPBCnum, ii);
                i3 = ElementsOnNodePBCNum->size[0];
                ElementsOnNodePBCNum->size[0] = ii->size[0];
                emxEnsureCapacity_real_T(sp, ElementsOnNodePBCNum, i3,
                  &gi_emlrtRTEI);
                loop_ub = ii->size[0];
                for (i3 = 0; i3 < loop_ub; i3++) {
                  ElementsOnNodePBCNum->data[i3] = ii->data[i3];
                }

                st.site = &y_emlrtRSI;
                old = any(&st, ElementsOnNodePBCNum);

                /*                          if sec2>0 */
                /*                              break */
                /*                          end */
              }

              if (*emlrtBreakCheckR2012bFlagVar != 0) {
                emlrtBreakCheckR2012b(sp);
              }
            }

            /*  merge secs */
            if ((int32_T)iSec2 != (int32_T)nri) {
              if (((int32_T)iSec2 < 1) || ((int32_T)iSec2 > setnPBC->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)iSec2, 1, setnPBC->size[0],
                  &cg_emlrtBCI, sp);
              }

              d = setnPBC->data[(int32_T)iSec2 - 1];
              if (1.0 > d) {
                loop_ub = 0;
              } else {
                if (1 > interreg2->size[0]) {
                  emlrtDynamicBoundsCheckR2012b(1, 1, interreg2->size[0],
                    &dg_emlrtBCI, sp);
                }

                if (((int32_T)d < 1) || ((int32_T)setnPBC->data[(int32_T)iSec2 -
                     1] > interreg2->size[0])) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)setnPBC->data[(int32_T)
                    iSec2 - 1], 1, interreg2->size[0], &eg_emlrtBCI, sp);
                }

                loop_ub = (int32_T)setnPBC->data[(int32_T)iSec2 - 1];
              }

              if (((int32_T)nri < 1) || ((int32_T)nri > setnPBC->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, setnPBC->size[0],
                  &gg_emlrtBCI, sp);
              }

              d1 = setnPBC->data[(int32_T)nri - 1];
              if (1.0 > d1) {
                nel = 0;
              } else {
                if (1 > interreg2->size[0]) {
                  emlrtDynamicBoundsCheckR2012b(1, 1, interreg2->size[0],
                    &hg_emlrtBCI, sp);
                }

                nel = (int32_T)setnPBC->data[(int32_T)nri - 1];
                if ((nel < 1) || (nel > interreg2->size[0])) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)setnPBC->data[(int32_T)
                    nri - 1], 1, interreg2->size[0], &ig_emlrtBCI, sp);
                }
              }

              if (((int32_T)iSec2 < 1) || ((int32_T)iSec2 > setnPBC->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)iSec2, 1, setnPBC->size[0],
                  &no_emlrtBCI, sp);
              }

              if (((int32_T)nri < 1) || ((int32_T)nri > setnPBC->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, setnPBC->size[0],
                  &oo_emlrtBCI, sp);
              }

              d += d1;
              if (1.0 > d) {
                i3 = 0;
              } else {
                if (1 > interreg2->size[0]) {
                  emlrtDynamicBoundsCheckR2012b(1, 1, interreg2->size[0],
                    &kg_emlrtBCI, sp);
                }

                if (((int32_T)d < 1) || ((int32_T)d > interreg2->size[0])) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, interreg2->size[0],
                    &lg_emlrtBCI, sp);
                }

                i3 = (int32_T)d;
              }

              if (((int32_T)iSec2 < 1) || ((int32_T)iSec2 > interreg2->size[1]))
              {
                emlrtDynamicBoundsCheckR2012b((int32_T)iSec2, 1, interreg2->
                  size[1], &mg_emlrtBCI, sp);
              }

              st.site = &x_emlrtRSI;
              if (((int32_T)iSec2 < 1) || ((int32_T)iSec2 > interreg2->size[1]))
              {
                emlrtDynamicBoundsCheckR2012b((int32_T)iSec2, 1, interreg2->
                  size[1], &fg_emlrtBCI, &st);
              }

              if (((int32_T)nri < 1) || ((int32_T)nri > interreg2->size[1])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, interreg2->size[1],
                  &jg_emlrtBCI, &st);
              }

              b_i = MorePBC->size[0];
              MorePBC->size[0] = loop_ub + nel;
              emxEnsureCapacity_real_T(&st, MorePBC, b_i, &oj_emlrtRTEI);
              for (b_i = 0; b_i < loop_ub; b_i++) {
                MorePBC->data[b_i] = interreg2->data[b_i + interreg2->size[0] *
                  ((int32_T)iSec2 - 1)];
              }

              for (b_i = 0; b_i < nel; b_i++) {
                MorePBC->data[b_i + loop_ub] = interreg2->data[b_i +
                  interreg2->size[0] * ((int32_T)nri - 1)];
              }

              b_st.site = &uo_emlrtRSI;
              b_sort(&b_st, MorePBC);
              emlrtSubAssignSizeCheckR2012b(&i3, 1, &MorePBC->size[0], 1,
                &h_emlrtECI, sp);
              loop_ub = MorePBC->size[0];
              for (i3 = 0; i3 < loop_ub; i3++) {
                interreg2->data[i3 + interreg2->size[0] * ((int32_T)iSec2 - 1)] =
                  MorePBC->data[i3];
              }

              if (((int32_T)nri < 1) || ((int32_T)nri > interreg2->size[1])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, interreg2->size[1],
                  &ng_emlrtBCI, sp);
              }

              loop_ub = interreg2->size[0];
              for (i3 = 0; i3 < loop_ub; i3++) {
                interreg2->data[i3 + interreg2->size[0] * ((int32_T)nri - 1)] =
                  0.0;
              }

              if (((int32_T)iSec2 < 1) || ((int32_T)iSec2 > setnPBC->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)iSec2, 1, setnPBC->size[0],
                  &wo_emlrtBCI, sp);
              }

              if (((int32_T)nri < 1) || ((int32_T)nri > setnPBC->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, setnPBC->size[0],
                  &xo_emlrtBCI, sp);
              }

              if (((int32_T)iSec2 < 1) || ((int32_T)iSec2 > setnPBC->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)iSec2, 1, setnPBC->size[0],
                  &yo_emlrtBCI, sp);
              }

              setnPBC->data[(int32_T)iSec2 - 1] = d;
              if (((int32_T)nri < 1) || ((int32_T)nri > setnPBC->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)nri, 1, setnPBC->size[0],
                  &ap_emlrtBCI, sp);
              }

              setnPBC->data[(int32_T)nri - 1] = 0.0;
            }
          }
        }

        /*  if external fac */
        if (*emlrtBreakCheckR2012bFlagVar != 0) {
          emlrtBreakCheckR2012b(sp);
        }
      }

      /*  assign node IDs to each sector */
      st.site = &w_emlrtRSI;
      if (muDoubleScalarIsNaN(usePBC)) {
        emlrtErrorWithMessageIdR2018a(&st, &mb_emlrtRTEI, "MATLAB:nologicalnan",
          "MATLAB:nologicalnan", 0);
      }

      if (usePBC != 0.0) {
        if (i1 > NodesOnPBCnum->size[0]) {
          emlrtDynamicBoundsCheckR2012b(i1, 1, NodesOnPBCnum->size[0],
            &un_emlrtBCI, sp);
        }

        if (NodesOnPBCnum->data[i1 - 1] > 0.0) {
          /*  handle copies of PBC nodes specially */
          if (i1 > NodesOnPBCnum->size[0]) {
            emlrtDynamicBoundsCheckR2012b(i1, 1, NodesOnPBCnum->size[0],
              &vn_emlrtBCI, sp);
          }

          b_i1 = NodesOnPBCnum->data[i1 - 1] + 1.0;
          d = NodesOnPBCnum->data[i1 - 1] + 1.0;
          d1 = (int32_T)muDoubleScalarFloor(d);
          if (d != d1) {
            emlrtIntegerCheckR2012b(d, &fe_emlrtDCI, sp);
          }

          i2 = MorePBC->size[0];
          MorePBC->size[0] = (int32_T)d;
          emxEnsureCapacity_real_T(sp, MorePBC, i2, &ij_emlrtRTEI);
          if (d != d1) {
            emlrtIntegerCheckR2012b(d, &fe_emlrtDCI, sp);
          }

          loop_ub = (int32_T)d;
          for (i2 = 0; i2 < loop_ub; i2++) {
            MorePBC->data[i2] = 0.0;
          }

          i1 = (int32_T)ElementsOnNodeNum->data[i1 - 1];
          emlrtForLoopVectorCheckR2012b(1.0, 1.0, ElementsOnNodeNum->data
            [(int32_T)NodesOnInterface->data[locN] - 1], mxDOUBLE_CLASS,
            (int32_T)ElementsOnNodeNum->data[(int32_T)NodesOnInterface->
            data[locN] - 1], &bc_emlrtRTEI, sp);
          for (nelA = 0; nelA < i1; nelA++) {
            if (((int32_T)(nelA + 1U) < 1) || ((int32_T)(nelA + 1U) >
                 setnPBC->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)(nelA + 1U), 1,
                setnPBC->size[0], &xn_emlrtBCI, sp);
            }

            if (setnPBC->data[nelA] > 0.0) {
              if (1 > interreg2->size[0]) {
                emlrtDynamicBoundsCheckR2012b(1, 1, interreg2->size[0],
                  &og_emlrtBCI, sp);
              }

              i2 = (int32_T)setnPBC->data[nelA];
              if ((i2 < 1) || (i2 > interreg2->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)setnPBC->data[nelA], 1,
                  interreg2->size[0], &pg_emlrtBCI, sp);
              }

              i3 = nelA + 1;
              if ((i3 < 1) || (i3 > interreg2->size[1])) {
                emlrtDynamicBoundsCheckR2012b(i3, 1, interreg2->size[1],
                  &qg_emlrtBCI, sp);
              }

              if (1.0 > nen) {
                loop_ub = 0;
              } else {
                if (1 > NodesOnElementCG->size[1]) {
                  emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementCG->size[1],
                    &sg_emlrtBCI, sp);
                }

                if ((int32_T)nen > NodesOnElementCG->size[1]) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1,
                    NodesOnElementCG->size[1], &tg_emlrtBCI, sp);
                }

                loop_ub = (int32_T)nen;
              }

              st.site = &v_emlrtRSI;
              if (interreg2->data[interreg2->size[0] * nelA] != (int32_T)
                  muDoubleScalarFloor(interreg2->data[interreg2->size[0] * nelA]))
              {
                emlrtIntegerCheckR2012b(interreg2->data[interreg2->size[0] *
                  nelA], &ab_emlrtDCI, &st);
              }

              notdone = (int32_T)interreg2->data[interreg2->size[0] * nelA];
              if ((notdone < 1) || (notdone > NodesOnElementCG->size[0])) {
                emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnElementCG->
                  size[0], &rg_emlrtBCI, &st);
              }

              nodeC2_size[0] = 1;
              nodeC2_size[1] = loop_ub;
              for (b_i = 0; b_i < loop_ub; b_i++) {
                b_nodeC2_data[b_i] = (NodesOnElementCG->data[(notdone +
                  NodesOnElementCG->size[0] * b_i) - 1] == node);
              }

              b_st.site = &ug_emlrtRSI;
              f_eml_find(&b_st, b_nodeC2_data, nodeC2_size, ii_data, ii_size);
              loop_ub = ii_size[0] * ii_size[1];
              for (b_i = 0; b_i < loop_ub; b_i++) {
                nodeC2_data[b_i] = ii_data[b_i];
              }

              if (notdone > NodesOnElementPBC->size[0]) {
                emlrtDynamicBoundsCheckR2012b(notdone, 1,
                  NodesOnElementPBC->size[0], &ug_emlrtBCI, sp);
              }

              loop_ub = ii_size[1];
              for (b_i = 0; b_i < loop_ub; b_i++) {
                end = ii_data[b_i];
                if ((end < 1) || (end > NodesOnElementPBC->size[1])) {
                  emlrtDynamicBoundsCheckR2012b(end, 1, NodesOnElementPBC->size
                    [1], &mo_emlrtBCI, sp);
                }
              }

              if (1 > ii_size[1]) {
                emlrtDynamicBoundsCheckR2012b(1, 1, ii_size[1], &vg_emlrtBCI, sp);
              }

              numA = NodesOnElementPBC->data[(notdone + NodesOnElementPBC->size
                [0] * ((int32_T)nodeC2_data[0] - 1)) - 1];
              if (NodesOnElementPBC->data[(notdone + NodesOnElementPBC->size[0] *
                   (ii_data[0] - 1)) - 1] > node) {
                if (1.0 > b_i1 - 1.0) {
                  loop_ub = 0;
                } else {
                  if (((int32_T)(b_i1 - 1.0) < 1) || ((int32_T)(b_i1 - 1.0) > 4))
                  {
                    emlrtDynamicBoundsCheckR2012b((int32_T)(b_i1 - 1.0), 1, 4,
                      &wg_emlrtBCI, sp);
                  }

                  loop_ub = (int32_T)(b_i1 - 1.0);
                }

                st.site = &u_emlrtRSI;
                if (((int32_T)node < 1) || ((int32_T)node > NodesOnPBC->size[1]))
                {
                  emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
                    NodesOnPBC->size[1], &xg_emlrtBCI, &st);
                }

                NodesOnPBC_size[0] = loop_ub;
                for (b_i = 0; b_i < loop_ub; b_i++) {
                  b_NodesOnPBC_data[b_i] = (NodesOnPBC->data[b_i + 4 * ((int32_T)
                    node - 1)] == numA);
                }

                NodesOnPBC_data.data = &b_NodesOnPBC_data[0];
                NodesOnPBC_data.size = &NodesOnPBC_size[0];
                NodesOnPBC_data.allocatedSize = 4;
                NodesOnPBC_data.numDimensions = 1;
                NodesOnPBC_data.canFreeData = false;
                b_st.site = &ug_emlrtRSI;
                eml_find(&b_st, &NodesOnPBC_data, ii);
                nodeloc_size[0] = ii->size[0];
                loop_ub = ii->size[0];
                for (b_i = 0; b_i < loop_ub; b_i++) {
                  nodeloc_data[b_i] = (real_T)ii->data[b_i] + 1.0;
                }
              } else {
                nodeloc_size[0] = 1;
                nodeloc_data[0] = 1.0;
              }

              loop_ub = nodeloc_size[0];
              links2_size[0] = nodeloc_size[0];
              for (b_i = 0; b_i < loop_ub; b_i++) {
                d = nodeloc_data[b_i];
                if (((int32_T)d < 1) || ((int32_T)d > MorePBC->size[0])) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, MorePBC->size[0],
                    &qo_emlrtBCI, sp);
                }

                MorePBC_data[b_i] = MorePBC->data[(int32_T)d - 1];
              }

              st.site = &t_emlrtRSI;
              if (b_ifWhileCond(&st, MorePBC_data, links2_size)) {
                if (1 > interreg2->size[0]) {
                  emlrtDynamicBoundsCheckR2012b(1, 1, interreg2->size[0],
                    &yg_emlrtBCI, sp);
                }

                if (i2 > interreg2->size[0]) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)setnPBC->data[nelA], 1,
                    interreg2->size[0], &ah_emlrtBCI, sp);
                }

                if (i3 > interreg2->size[1]) {
                  emlrtDynamicBoundsCheckR2012b(i3, 1, interreg2->size[1],
                    &bh_emlrtBCI, sp);
                }

                b_i2++;
                d = NodesOnElementPBC->data[(notdone + NodesOnElementPBC->size[0]
                  * (ii_data[0] - 1)) - 1];
                if (d != (int32_T)muDoubleScalarFloor(d)) {
                  emlrtIntegerCheckR2012b(d, &bb_emlrtDCI, sp);
                }

                i3 = Coordinates->size[0];
                if (((int32_T)d < 1) || ((int32_T)d > i3)) {
                  emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, i3, &ch_emlrtBCI,
                    sp);
                }

                if (((int32_T)b_i2 < 1) || ((int32_T)b_i2 > Coordinates3->size[0]))
                {
                  emlrtDynamicBoundsCheckR2012b((int32_T)b_i2, 1,
                    Coordinates3->size[0], &li_emlrtBCI, sp);
                }

                Coordinates3->data[(int32_T)b_i2 - 1] = Coordinates->data
                  [(int32_T)d - 1];
                Coordinates3->data[((int32_T)b_i2 + Coordinates3->size[0]) - 1] =
                  Coordinates->data[((int32_T)d + Coordinates->size[0]) - 1];

                /*  Loop over all elements attached to node */
                emlrtForLoopVectorCheckR2012b(1.0, 1.0, setnPBC->data[nelA],
                  mxDOUBLE_CLASS, (int32_T)setnPBC->data[nelA], &cc_emlrtRTEI,
                  sp);
                if (0 <= (int32_T)setnPBC->data[nelA] - 1) {
                  i6 = i2;
                  d = ElementsOnNodeNum->data[(int32_T)node - 1];
                  if (d < 1.0) {
                    setAll->size[0] = 1;
                    setAll->size[1] = 0;
                  } else if (muDoubleScalarIsInf(d) && (1.0 == d)) {
                    i3 = setAll->size[0] * setAll->size[1];
                    setAll->size[0] = 1;
                    setAll->size[1] = 1;
                    emxEnsureCapacity_real_T(sp, setAll, i3, &nh_emlrtRTEI);
                    setAll->data[0] = rtNaN;
                  } else {
                    i3 = setAll->size[0] * setAll->size[1];
                    setAll->size[0] = 1;
                    setAll->size[1] = (int32_T)muDoubleScalarFloor
                      (ElementsOnNodeNum->data[(int32_T)node - 1] - 1.0) + 1;
                    emxEnsureCapacity_real_T(sp, setAll, i3, &nh_emlrtRTEI);
                    loop_ub = (int32_T)muDoubleScalarFloor
                      (ElementsOnNodeNum->data[(int32_T)node - 1] - 1.0);
                    for (i3 = 0; i3 <= loop_ub; i3++) {
                      setAll->data[i3] = (real_T)i3 + 1.0;
                    }
                  }

                  if (1.0 > nen) {
                    d_loop_ub = 0;
                  } else {
                    if (1 > NodesOnElementCG->size[1]) {
                      emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementCG->
                        size[1], &hh_emlrtBCI, sp);
                    }

                    if ((int32_T)nen > NodesOnElementCG->size[1]) {
                      emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1,
                        NodesOnElementCG->size[1], &ih_emlrtBCI, sp);
                    }

                    d_loop_ub = (int32_T)nen;
                  }
                }

                if (0 <= i2 - 1) {
                  nodeC2_size[0] = 1;
                  nodeC2_size[1] = d_loop_ub;
                }

                for (trueCount = 0; trueCount < i2; trueCount++) {
                  i3 = trueCount + 1;
                  if (i3 > i6) {
                    emlrtDynamicBoundsCheckR2012b(i3, 1, i6, &dh_emlrtBCI, sp);
                  }

                  if (interreg2->data[trueCount + interreg2->size[0] * nelA] !=
                      (int32_T)muDoubleScalarFloor(interreg2->data[trueCount +
                       interreg2->size[0] * nelA])) {
                    emlrtIntegerCheckR2012b(interreg2->data[trueCount +
                      interreg2->size[0] * nelA], &cb_emlrtDCI, sp);
                  }

                  i3 = RegionOnElement->size[0];
                  notdone = (int32_T)interreg2->data[trueCount + interreg2->
                    size[0] * nelA];
                  if ((notdone < 1) || (notdone > i3)) {
                    emlrtDynamicBoundsCheckR2012b(notdone, 1, i3, &eh_emlrtBCI,
                      sp);
                  }

                  st.site = &s_emlrtRSI;
                  b_st.site = &no_emlrtRSI;
                  sparse_parenAssign2D(&b_st, NodeReg, b_i2, numA,
                                       RegionOnElement->data[(int32_T)
                                       interreg2->data[trueCount +
                                       interreg2->size[0] * nelA] - 1]);
                  if (((int32_T)node < 1) || ((int32_T)node >
                       ElementsOnNodeNum->size[0])) {
                    emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
                      ElementsOnNodeNum->size[0], &fh_emlrtBCI, sp);
                  }

                  st.site = &r_emlrtRSI;
                  b_st.site = &r_emlrtRSI;
                  b_sparse_parenReference(&b_st, ElementsOnNode->d,
                    ElementsOnNode->colidx, ElementsOnNode->rowidx,
                    ElementsOnNode->m, ElementsOnNode->n, setAll, node,
                    NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                    &NodeRegSum.m);
                  b_st.site = &r_emlrtRSI;
                  c_sparse_eq(&b_st, interreg2->data[trueCount + interreg2->
                              size[0] * nelA], NodeRegSum.d, NodeRegSum.colidx,
                              NodeRegSum.rowidx, NodeRegSum.m, &d_expl_temp);
                  i3 = c_expl_temp.colidx->size[0];
                  c_expl_temp.colidx->size[0] = d_expl_temp.colidx->size[0];
                  emxEnsureCapacity_int32_T(&st, c_expl_temp.colidx, i3,
                    &qj_emlrtRTEI);
                  loop_ub = d_expl_temp.colidx->size[0];
                  for (i3 = 0; i3 < loop_ub; i3++) {
                    c_expl_temp.colidx->data[i3] = d_expl_temp.colidx->data[i3];
                  }

                  i3 = c_expl_temp.rowidx->size[0];
                  c_expl_temp.rowidx->size[0] = d_expl_temp.rowidx->size[0];
                  emxEnsureCapacity_int32_T(&st, c_expl_temp.rowidx, i3,
                    &qj_emlrtRTEI);
                  loop_ub = d_expl_temp.rowidx->size[0];
                  for (i3 = 0; i3 < loop_ub; i3++) {
                    c_expl_temp.rowidx->data[i3] = d_expl_temp.rowidx->data[i3];
                  }

                  b_st.site = &ug_emlrtRSI;
                  g_eml_find(&b_st, c_expl_temp.colidx, c_expl_temp.rowidx,
                             d_expl_temp.m, ii);
                  i3 = ElementsOnNodePBCNum->size[0];
                  ElementsOnNodePBCNum->size[0] = ii->size[0];
                  emxEnsureCapacity_real_T(sp, ElementsOnNodePBCNum, i3,
                    &gi_emlrtRTEI);
                  loop_ub = ii->size[0];
                  for (i3 = 0; i3 < loop_ub; i3++) {
                    ElementsOnNodePBCNum->data[i3] = ii->data[i3];
                  }

                  st.site = &d_emlrtRSI;
                  c_sparse_parenAssign(&st, ElementsOnNodeDup, b_i2,
                                       ElementsOnNodePBCNum, node);

                  /*  Reset nodal ID for element in higher material ID */
                  st.site = &q_emlrtRSI;
                  d = interreg2->data[trueCount + interreg2->size[0] * nelA];
                  if (((int32_T)d < 1) || ((int32_T)d > NodesOnElementCG->size[0]))
                  {
                    emlrtDynamicBoundsCheckR2012b((int32_T)d, 1,
                      NodesOnElementCG->size[0], &gh_emlrtBCI, &st);
                  }

                  for (i3 = 0; i3 < d_loop_ub; i3++) {
                    b_nodeC2_data[i3] = (NodesOnElementCG->data[((int32_T)d +
                      NodesOnElementCG->size[0] * i3) - 1] == node);
                  }

                  b_st.site = &ug_emlrtRSI;
                  f_eml_find(&b_st, b_nodeC2_data, nodeC2_size, ii_data, ii_size);
                  loop_ub = ii_size[1];
                  for (i3 = 0; i3 < loop_ub; i3++) {
                    b_i = ii_data[i3];
                    if (((int8_T)b_i < 1) || ((int8_T)b_i >
                         NodesOnElementDG->size[1])) {
                      emlrtDynamicBoundsCheckR2012b((int8_T)b_i, 1,
                        NodesOnElementDG->size[1], &dp_emlrtBCI, sp);
                    }

                    b_tmp_data[i3] = (int8_T)b_i;
                  }

                  if (notdone > NodesOnElementDG->size[0]) {
                    emlrtDynamicBoundsCheckR2012b(notdone, 1,
                      NodesOnElementDG->size[0], &ci_emlrtBCI, sp);
                  }

                  for (i3 = 0; i3 < loop_ub; i3++) {
                    NodesOnElementDG->data[(notdone + NodesOnElementDG->size[0] *
                      (b_tmp_data[i3] - 1)) - 1] = b_i2;
                  }

                  if (*emlrtBreakCheckR2012bFlagVar != 0) {
                    emlrtBreakCheckR2012b(sp);
                  }
                }
              } else {
                loop_ub = nodeloc_size[0];
                for (i3 = 0; i3 < loop_ub; i3++) {
                  d = nodeloc_data[i3];
                  if (((int32_T)d < 1) || ((int32_T)d > MorePBC->size[0])) {
                    emlrtDynamicBoundsCheckR2012b((int32_T)d, 1, MorePBC->size[0],
                      &vo_emlrtBCI, sp);
                  }

                  c_tmp_data[i3] = (int32_T)d;
                }

                for (i3 = 0; i3 < loop_ub; i3++) {
                  MorePBC->data[c_tmp_data[i3] - 1] = 1.0;
                }

                emlrtForLoopVectorCheckR2012b(1.0, 1.0, setnPBC->data[nelA],
                  mxDOUBLE_CLASS, (int32_T)setnPBC->data[nelA], &dc_emlrtRTEI,
                  sp);
                d = ElementsOnNodeNum->data[(int32_T)node - 1];
                if (d < 1.0) {
                  setAll->size[0] = 1;
                  setAll->size[1] = 0;
                } else if (muDoubleScalarIsInf(d) && (1.0 == d)) {
                  i3 = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  setAll->size[1] = 1;
                  emxEnsureCapacity_real_T(sp, setAll, i3, &nh_emlrtRTEI);
                  setAll->data[0] = rtNaN;
                } else {
                  i3 = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  setAll->size[1] = (int32_T)muDoubleScalarFloor
                    (ElementsOnNodeNum->data[(int32_T)node - 1] - 1.0) + 1;
                  emxEnsureCapacity_real_T(sp, setAll, i3, &nh_emlrtRTEI);
                  loop_ub = (int32_T)muDoubleScalarFloor(ElementsOnNodeNum->
                    data[(int32_T)node - 1] - 1.0);
                  for (i3 = 0; i3 <= loop_ub; i3++) {
                    setAll->data[i3] = (real_T)i3 + 1.0;
                  }
                }

                if (1.0 > nen) {
                  loop_ub = 0;
                } else {
                  if (1 > NodesOnElementCG->size[1]) {
                    emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementCG->size[1],
                      &nh_emlrtBCI, sp);
                  }

                  if ((int32_T)nen > NodesOnElementCG->size[1]) {
                    emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1,
                      NodesOnElementCG->size[1], &oh_emlrtBCI, sp);
                  }

                  loop_ub = (int32_T)nen;
                }

                if (0 <= i2 - 1) {
                  nodeC2_size[0] = 1;
                  nodeC2_size[1] = loop_ub;
                }

                for (trueCount = 0; trueCount < i2; trueCount++) {
                  i3 = trueCount + 1;
                  if (i3 > i2) {
                    emlrtDynamicBoundsCheckR2012b(i3, 1, i2, &jh_emlrtBCI, sp);
                  }

                  if (interreg2->data[trueCount + interreg2->size[0] * nelA] !=
                      (int32_T)muDoubleScalarFloor(interreg2->data[trueCount +
                       interreg2->size[0] * nelA])) {
                    emlrtIntegerCheckR2012b(interreg2->data[trueCount +
                      interreg2->size[0] * nelA], &db_emlrtDCI, sp);
                  }

                  i3 = RegionOnElement->size[0];
                  notdone = (int32_T)interreg2->data[trueCount + interreg2->
                    size[0] * nelA];
                  if ((notdone < 1) || (notdone > i3)) {
                    emlrtDynamicBoundsCheckR2012b(notdone, 1, i3, &kh_emlrtBCI,
                      sp);
                  }

                  st.site = &p_emlrtRSI;
                  b_st.site = &no_emlrtRSI;
                  sparse_parenAssign2D(&b_st, NodeReg, numA, numA,
                                       RegionOnElement->data[(int32_T)
                                       interreg2->data[trueCount +
                                       interreg2->size[0] * nelA] - 1]);
                  if (((int32_T)node < 1) || ((int32_T)node >
                       ElementsOnNodeNum->size[0])) {
                    emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
                      ElementsOnNodeNum->size[0], &lh_emlrtBCI, sp);
                  }

                  st.site = &o_emlrtRSI;
                  b_st.site = &o_emlrtRSI;
                  b_sparse_parenReference(&b_st, ElementsOnNode->d,
                    ElementsOnNode->colidx, ElementsOnNode->rowidx,
                    ElementsOnNode->m, ElementsOnNode->n, setAll, node,
                    NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                    &NodeRegSum.m);
                  b_st.site = &o_emlrtRSI;
                  c_sparse_eq(&b_st, interreg2->data[trueCount + interreg2->
                              size[0] * nelA], NodeRegSum.d, NodeRegSum.colidx,
                              NodeRegSum.rowidx, NodeRegSum.m, &d_expl_temp);
                  i3 = c_expl_temp.colidx->size[0];
                  c_expl_temp.colidx->size[0] = d_expl_temp.colidx->size[0];
                  emxEnsureCapacity_int32_T(&st, c_expl_temp.colidx, i3,
                    &pj_emlrtRTEI);
                  nel = d_expl_temp.colidx->size[0];
                  for (i3 = 0; i3 < nel; i3++) {
                    c_expl_temp.colidx->data[i3] = d_expl_temp.colidx->data[i3];
                  }

                  i3 = c_expl_temp.rowidx->size[0];
                  c_expl_temp.rowidx->size[0] = d_expl_temp.rowidx->size[0];
                  emxEnsureCapacity_int32_T(&st, c_expl_temp.rowidx, i3,
                    &pj_emlrtRTEI);
                  nel = d_expl_temp.rowidx->size[0];
                  for (i3 = 0; i3 < nel; i3++) {
                    c_expl_temp.rowidx->data[i3] = d_expl_temp.rowidx->data[i3];
                  }

                  b_st.site = &ug_emlrtRSI;
                  g_eml_find(&b_st, c_expl_temp.colidx, c_expl_temp.rowidx,
                             d_expl_temp.m, ii);
                  i3 = ElementsOnNodePBCNum->size[0];
                  ElementsOnNodePBCNum->size[0] = ii->size[0];
                  emxEnsureCapacity_real_T(sp, ElementsOnNodePBCNum, i3,
                    &gi_emlrtRTEI);
                  nel = ii->size[0];
                  for (i3 = 0; i3 < nel; i3++) {
                    ElementsOnNodePBCNum->data[i3] = ii->data[i3];
                  }

                  st.site = &c_emlrtRSI;
                  c_sparse_parenAssign(&st, ElementsOnNodeDup, numA,
                                       ElementsOnNodePBCNum, node);

                  /*  Reset nodal ID for element in higher material ID */
                  st.site = &n_emlrtRSI;
                  d = interreg2->data[trueCount + interreg2->size[0] * nelA];
                  if (((int32_T)d < 1) || ((int32_T)d > NodesOnElementCG->size[0]))
                  {
                    emlrtDynamicBoundsCheckR2012b((int32_T)d, 1,
                      NodesOnElementCG->size[0], &mh_emlrtBCI, &st);
                  }

                  for (i3 = 0; i3 < loop_ub; i3++) {
                    b_nodeC2_data[i3] = (NodesOnElementCG->data[((int32_T)d +
                      NodesOnElementCG->size[0] * i3) - 1] == node);
                  }

                  b_st.site = &ug_emlrtRSI;
                  f_eml_find(&b_st, b_nodeC2_data, nodeC2_size, ii_data, ii_size);
                  nel = ii_size[1];
                  for (i3 = 0; i3 < nel; i3++) {
                    b_i = ii_data[i3];
                    if (((int8_T)b_i < 1) || ((int8_T)b_i >
                         NodesOnElementDG->size[1])) {
                      emlrtDynamicBoundsCheckR2012b((int8_T)b_i, 1,
                        NodesOnElementDG->size[1], &cp_emlrtBCI, sp);
                    }

                    b_tmp_data[i3] = (int8_T)b_i;
                  }

                  if (notdone > NodesOnElementDG->size[0]) {
                    emlrtDynamicBoundsCheckR2012b(notdone, 1,
                      NodesOnElementDG->size[0], &di_emlrtBCI, sp);
                  }

                  for (i3 = 0; i3 < nel; i3++) {
                    NodesOnElementDG->data[(notdone + NodesOnElementDG->size[0] *
                      (b_tmp_data[i3] - 1)) - 1] = numA;
                  }

                  if (*emlrtBreakCheckR2012bFlagVar != 0) {
                    emlrtBreakCheckR2012b(sp);
                  }
                }
              }
            }

            if (*emlrtBreakCheckR2012bFlagVar != 0) {
              emlrtBreakCheckR2012b(sp);
            }
          }
        } else {
          guard1 = true;
        }
      } else {
        guard1 = true;
      }
    }

    if (guard2) {
      /*  regular midside node */
      i1 = (int32_T)(nummat + -1.0);
      emlrtForLoopVectorCheckR2012b(2.0, 1.0, nummat, mxDOUBLE_CLASS, (int32_T)
        (nummat + -1.0), &xb_emlrtRTEI, sp);
      for (nume = 0; nume < i1; nume++) {
        for (end = 0; end <= nume; end++) {
          /*  ID for material pair (row=mat2, col=mat1) */
          if (((int32_T)(nume + 2U) < 1) || ((int32_T)(nume + 2U) >
               InterTypes->size[0])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)(nume + 2U), 1,
              InterTypes->size[0], &jn_emlrtBCI, sp);
          }

          if (((int32_T)(end + 1U) < 1) || ((int32_T)(end + 1U) >
               InterTypes->size[1])) {
            emlrtDynamicBoundsCheckR2012b((int32_T)(end + 1U), 1,
              InterTypes->size[1], &jn_emlrtBCI, sp);
          }

          if (InterTypes->data[(nume + InterTypes->size[0] * end) + 1] > 0.0) {
            /*  Duplicate nodes along material interface */
            st.site = &nb_emlrtRSI;
            d_sparse_parenReference(&st, NodeReg->d, NodeReg->colidx,
              NodeReg->rowidx, NodeReg->m, NodeReg->n, node, (real_T)end + 1.0,
              b_MorePBC, c_ii, indx);
            st.site = &nb_emlrtRSI;
            b_sparse_gt(&st, b_MorePBC, c_ii, h_expl_temp.d, h_expl_temp.colidx,
                        h_expl_temp.rowidx);
            st.site = &nb_emlrtRSI;
            d_sparse_parenReference(&st, NodeReg->d, NodeReg->colidx,
              NodeReg->rowidx, NodeReg->m, NodeReg->n, node, (real_T)nume + 2.0,
              b_MorePBC, c_ii, indx);
            st.site = &nb_emlrtRSI;
            b_sparse_gt(&st, b_MorePBC, c_ii, f_expl_temp.d, f_expl_temp.colidx,
                        indx);
            st.site = &nb_emlrtRSI;
            b_sparse_and(&st, h_expl_temp.d, h_expl_temp.colidx, f_expl_temp.d,
                         f_expl_temp.colidx, indx, &g_expl_temp);
            st.site = &nb_emlrtRSI;
            d_sparse_parenReference(&st, NodeReg->d, NodeReg->colidx,
              NodeReg->rowidx, NodeReg->m, NodeReg->n, node, (real_T)nume + 2.0,
              b_MorePBC, c_ii, indx);
            st.site = &nb_emlrtRSI;
            d_sparse_parenReference(&st, NodeReg->d, NodeReg->colidx,
              NodeReg->rowidx, NodeReg->m, NodeReg->n, node, (real_T)end + 1.0,
              ElementsOnNodePBCNum, indx, ii);
            st.site = &nb_emlrtRSI;
            b_sparse_eq(&st, b_MorePBC, c_ii, ElementsOnNodePBCNum, indx, ii,
                        &h_expl_temp);
            st.site = &nb_emlrtRSI;
            b_sparse_and(&st, g_expl_temp.d, g_expl_temp.colidx, h_expl_temp.d,
                         h_expl_temp.colidx, h_expl_temp.rowidx, &f_expl_temp);

            /*  Namely, only find nodes that are part of materials mat1 and */
            /*  mat2, but ONLY if those nodal IDs have not been reset before, */
            /*  in which case the ID for mat1 will equal the ID for mat2. */
            /*  In this way, each new nodal ID is assigned to a single */
            /*  material. */
            st.site = &mb_emlrtRSI;
            if (c_sparse_full(&st, f_expl_temp.d, f_expl_temp.colidx)) {
              /*  Duplicate the nodal coordinates */
              b_i2++;
              st.site = &lb_emlrtRSI;
              b_st.site = &no_emlrtRSI;
              sparse_parenAssign2D(&b_st, NodeReg, b_i2, node, (real_T)nume +
                                   2.0);

              /*  Loop over all nodes on material interface that need */
              /*  duplicated */
              if (((int32_T)node < 1) || ((int32_T)node >
                   ElementsOnNodeNum->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
                  ElementsOnNodeNum->size[0], &jf_emlrtBCI, sp);
              }

              st.site = &kb_emlrtRSI;
              c_sparse_parenReference(&st, ElementsOnNode->d,
                ElementsOnNode->colidx, ElementsOnNode->rowidx,
                ElementsOnNode->m, ElementsOnNode->n, node, &NodeRegSum);
              st.site = &kb_emlrtRSI;
              b_sparse_full(&st, NodeRegSum.d, NodeRegSum.colidx,
                            NodeRegSum.rowidx, NodeRegSum.m, ElementsOnNodeA);

              /*  Loop over all elements attached to node */
              i2 = (int32_T)ElementsOnNodeNum->data[(int32_T)node - 1];
              emlrtForLoopVectorCheckR2012b(1.0, 1.0, ElementsOnNodeNum->data
                [(int32_T)node - 1], mxDOUBLE_CLASS, (int32_T)
                ElementsOnNodeNum->data[(int32_T)node - 1], &yb_emlrtRTEI, sp);
              for (trueCount = 0; trueCount < i2; trueCount++) {
                i3 = trueCount + 1;
                if ((i3 < 1) || (i3 > ElementsOnNodeA->size[0])) {
                  emlrtDynamicBoundsCheckR2012b(i3, 1, ElementsOnNodeA->size[0],
                    &kf_emlrtBCI, sp);
                }

                if (ElementsOnNodeA->data[trueCount] != (int32_T)
                    muDoubleScalarFloor(ElementsOnNodeA->data[trueCount])) {
                  emlrtIntegerCheckR2012b(ElementsOnNodeA->data[trueCount],
                    &x_emlrtDCI, sp);
                }

                i3 = RegionOnElement->size[0];
                notdone = (int32_T)ElementsOnNodeA->data[trueCount];
                if ((notdone < 1) || (notdone > i3)) {
                  emlrtDynamicBoundsCheckR2012b(notdone, 1, i3, &lf_emlrtBCI, sp);
                }

                if (RegionOnElement->data[notdone - 1] == (real_T)nume + 2.0) {
                  /*  Reset nodal ID for element in higher material ID */
                  if (1.0 > nen) {
                    loop_ub = 0;
                  } else {
                    if (1 > NodesOnElementCG->size[1]) {
                      emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementCG->
                        size[1], &nf_emlrtBCI, sp);
                    }

                    if ((int32_T)nen > NodesOnElementCG->size[1]) {
                      emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1,
                        NodesOnElementCG->size[1], &of_emlrtBCI, sp);
                    }

                    loop_ub = (int32_T)nen;
                  }

                  st.site = &jb_emlrtRSI;
                  if (notdone > NodesOnElementCG->size[0]) {
                    emlrtDynamicBoundsCheckR2012b(notdone, 1,
                      NodesOnElementCG->size[0], &mf_emlrtBCI, &st);
                  }

                  nodeC2_size[0] = 1;
                  nodeC2_size[1] = loop_ub;
                  for (i3 = 0; i3 < loop_ub; i3++) {
                    b_nodeC2_data[i3] = (NodesOnElementCG->data[(notdone +
                      NodesOnElementCG->size[0] * i3) - 1] == node);
                  }

                  b_st.site = &ug_emlrtRSI;
                  f_eml_find(&b_st, b_nodeC2_data, nodeC2_size, ii_data, ii_size);
                  loop_ub = ii_size[1];
                  for (i3 = 0; i3 < loop_ub; i3++) {
                    b_i = ii_data[i3];
                    if (((int8_T)b_i < 1) || ((int8_T)b_i >
                         NodesOnElementDG->size[1])) {
                      emlrtDynamicBoundsCheckR2012b((int8_T)b_i, 1,
                        NodesOnElementDG->size[1], &io_emlrtBCI, sp);
                    }

                    b_tmp_data[i3] = (int8_T)b_i;
                  }

                  if (notdone > NodesOnElementDG->size[0]) {
                    emlrtDynamicBoundsCheckR2012b(notdone, 1,
                      NodesOnElementDG->size[0], &bi_emlrtBCI, sp);
                  }

                  for (i3 = 0; i3 < loop_ub; i3++) {
                    NodesOnElementDG->data[(notdone + NodesOnElementDG->size[0] *
                      (b_tmp_data[i3] - 1)) - 1] = b_i2;
                  }

                  i3 = Coordinates->size[0];
                  if (((int32_T)node < 1) || ((int32_T)node > i3)) {
                    emlrtDynamicBoundsCheckR2012b((int32_T)node, 1, i3,
                      &pf_emlrtBCI, sp);
                  }

                  if (((int32_T)b_i2 < 1) || ((int32_T)b_i2 > Coordinates3->
                       size[0])) {
                    emlrtDynamicBoundsCheckR2012b((int32_T)b_i2, 1,
                      Coordinates3->size[0], &ki_emlrtBCI, sp);
                  }

                  Coordinates3->data[(int32_T)b_i2 - 1] = Coordinates->data
                    [(int32_T)node - 1];
                  Coordinates3->data[((int32_T)b_i2 + Coordinates3->size[0]) - 1]
                    = Coordinates->data[((int32_T)node + Coordinates->size[0]) -
                    1];
                }

                if (*emlrtBreakCheckR2012bFlagVar != 0) {
                  emlrtBreakCheckR2012b(sp);
                }
              }
            }
          }

          if (*emlrtBreakCheckR2012bFlagVar != 0) {
            emlrtBreakCheckR2012b(sp);
          }
        }

        if (*emlrtBreakCheckR2012bFlagVar != 0) {
          emlrtBreakCheckR2012b(sp);
        }
      }
    }

    if (guard1) {
      /*  regular node */
      numPBC = 0;
      i1 = (int32_T)ElementsOnNodeNum->data[i1 - 1];
      emlrtForLoopVectorCheckR2012b(1.0, 1.0, ElementsOnNodeNum->data[(int32_T)
        NodesOnInterface->data[locN] - 1], mxDOUBLE_CLASS, (int32_T)
        ElementsOnNodeNum->data[(int32_T)NodesOnInterface->data[locN] - 1],
        &ec_emlrtRTEI, sp);
      for (nelA = 0; nelA < i1; nelA++) {
        if (((int32_T)(nelA + 1U) < 1) || ((int32_T)(nelA + 1U) > setnPBC->size
             [0])) {
          emlrtDynamicBoundsCheckR2012b((int32_T)(nelA + 1U), 1, setnPBC->size[0],
            &wn_emlrtBCI, sp);
        }

        if (setnPBC->data[nelA] > 0.0) {
          st.site = &m_emlrtRSI;
          if (numPBC != 0) {
            if (1 > interreg2->size[0]) {
              emlrtDynamicBoundsCheckR2012b(1, 1, interreg2->size[0],
                &ph_emlrtBCI, sp);
            }

            i2 = (int32_T)setnPBC->data[nelA];
            if ((i2 < 1) || (i2 > interreg2->size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)setnPBC->data[nelA], 1,
                interreg2->size[0], &qh_emlrtBCI, sp);
            }

            i3 = nelA + 1;
            if ((i3 < 1) || (i3 > interreg2->size[1])) {
              emlrtDynamicBoundsCheckR2012b(i3, 1, interreg2->size[1],
                &rh_emlrtBCI, sp);
            }

            b_i2++;
            i3 = Coordinates->size[0];
            if (((int32_T)node < 1) || ((int32_T)node > i3)) {
              emlrtDynamicBoundsCheckR2012b((int32_T)node, 1, i3, &sh_emlrtBCI,
                sp);
            }

            if (((int32_T)b_i2 < 1) || ((int32_T)b_i2 > Coordinates3->size[0]))
            {
              emlrtDynamicBoundsCheckR2012b((int32_T)b_i2, 1, Coordinates3->
                size[0], &mi_emlrtBCI, sp);
            }

            Coordinates3->data[(int32_T)b_i2 - 1] = Coordinates->data[(int32_T)
              node - 1];
            Coordinates3->data[((int32_T)b_i2 + Coordinates3->size[0]) - 1] =
              Coordinates->data[((int32_T)node + Coordinates->size[0]) - 1];

            /*  Loop over all elements attached to node */
            emlrtForLoopVectorCheckR2012b(1.0, 1.0, setnPBC->data[nelA],
              mxDOUBLE_CLASS, (int32_T)setnPBC->data[nelA], &fc_emlrtRTEI, sp);
            d = ElementsOnNodeNum->data[(int32_T)node - 1];
            if (d < 1.0) {
              setAll->size[0] = 1;
              setAll->size[1] = 0;
            } else if (muDoubleScalarIsInf(d) && (1.0 == d)) {
              i3 = setAll->size[0] * setAll->size[1];
              setAll->size[0] = 1;
              setAll->size[1] = 1;
              emxEnsureCapacity_real_T(sp, setAll, i3, &nh_emlrtRTEI);
              setAll->data[0] = rtNaN;
            } else {
              i3 = setAll->size[0] * setAll->size[1];
              setAll->size[0] = 1;
              loop_ub = (int32_T)muDoubleScalarFloor(d - 1.0);
              setAll->size[1] = loop_ub + 1;
              emxEnsureCapacity_real_T(sp, setAll, i3, &nh_emlrtRTEI);
              for (i3 = 0; i3 <= loop_ub; i3++) {
                setAll->data[i3] = (real_T)i3 + 1.0;
              }
            }

            if (1.0 > nen) {
              loop_ub = 0;
            } else {
              if (1 > NodesOnElementCG->size[1]) {
                emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementCG->size[1],
                  &xh_emlrtBCI, sp);
              }

              if ((int32_T)nen > NodesOnElementCG->size[1]) {
                emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1,
                  NodesOnElementCG->size[1], &yh_emlrtBCI, sp);
              }

              loop_ub = (int32_T)nen;
            }

            if (0 <= i2 - 1) {
              nodeC2_size[0] = 1;
              nodeC2_size[1] = loop_ub;
            }

            for (trueCount = 0; trueCount < i2; trueCount++) {
              i3 = trueCount + 1;
              if (i3 > i2) {
                emlrtDynamicBoundsCheckR2012b(i3, 1, i2, &th_emlrtBCI, sp);
              }

              if (interreg2->data[trueCount + interreg2->size[0] * nelA] !=
                  (int32_T)muDoubleScalarFloor(interreg2->data[trueCount +
                   interreg2->size[0] * nelA])) {
                emlrtIntegerCheckR2012b(interreg2->data[trueCount +
                  interreg2->size[0] * nelA], &eb_emlrtDCI, sp);
              }

              i3 = RegionOnElement->size[0];
              notdone = (int32_T)interreg2->data[trueCount + interreg2->size[0] *
                nelA];
              if ((notdone < 1) || (notdone > i3)) {
                emlrtDynamicBoundsCheckR2012b(notdone, 1, i3, &uh_emlrtBCI, sp);
              }

              st.site = &l_emlrtRSI;
              b_st.site = &no_emlrtRSI;
              sparse_parenAssign2D(&b_st, NodeReg, b_i2, node,
                                   RegionOnElement->data[(int32_T)
                                   interreg2->data[trueCount + interreg2->size[0]
                                   * nelA] - 1]);
              if (((int32_T)node < 1) || ((int32_T)node >
                   ElementsOnNodeNum->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
                  ElementsOnNodeNum->size[0], &vh_emlrtBCI, sp);
              }

              st.site = &k_emlrtRSI;
              b_st.site = &k_emlrtRSI;
              b_sparse_parenReference(&b_st, ElementsOnNode->d,
                ElementsOnNode->colidx, ElementsOnNode->rowidx,
                ElementsOnNode->m, ElementsOnNode->n, setAll, node, NodeRegSum.d,
                NodeRegSum.colidx, NodeRegSum.rowidx, &NodeRegSum.m);
              b_st.site = &k_emlrtRSI;
              c_sparse_eq(&b_st, interreg2->data[trueCount + interreg2->size[0] *
                          nelA], NodeRegSum.d, NodeRegSum.colidx,
                          NodeRegSum.rowidx, NodeRegSum.m, &d_expl_temp);
              i3 = c_expl_temp.colidx->size[0];
              c_expl_temp.colidx->size[0] = d_expl_temp.colidx->size[0];
              emxEnsureCapacity_int32_T(&st, c_expl_temp.colidx, i3,
                &mj_emlrtRTEI);
              nel = d_expl_temp.colidx->size[0];
              for (i3 = 0; i3 < nel; i3++) {
                c_expl_temp.colidx->data[i3] = d_expl_temp.colidx->data[i3];
              }

              i3 = c_expl_temp.rowidx->size[0];
              c_expl_temp.rowidx->size[0] = d_expl_temp.rowidx->size[0];
              emxEnsureCapacity_int32_T(&st, c_expl_temp.rowidx, i3,
                &mj_emlrtRTEI);
              nel = d_expl_temp.rowidx->size[0];
              for (i3 = 0; i3 < nel; i3++) {
                c_expl_temp.rowidx->data[i3] = d_expl_temp.rowidx->data[i3];
              }

              b_st.site = &ug_emlrtRSI;
              g_eml_find(&b_st, c_expl_temp.colidx, c_expl_temp.rowidx,
                         d_expl_temp.m, ii);
              i3 = ElementsOnNodePBCNum->size[0];
              ElementsOnNodePBCNum->size[0] = ii->size[0];
              emxEnsureCapacity_real_T(sp, ElementsOnNodePBCNum, i3,
                &gi_emlrtRTEI);
              nel = ii->size[0];
              for (i3 = 0; i3 < nel; i3++) {
                ElementsOnNodePBCNum->data[i3] = ii->data[i3];
              }

              st.site = &b_emlrtRSI;
              c_sparse_parenAssign(&st, ElementsOnNodeDup, b_i2,
                                   ElementsOnNodePBCNum, node);

              /*  Reset nodal ID for element in higher material ID */
              st.site = &j_emlrtRSI;
              d = interreg2->data[trueCount + interreg2->size[0] * nelA];
              if (((int32_T)d < 1) || ((int32_T)d > NodesOnElementCG->size[0]))
              {
                emlrtDynamicBoundsCheckR2012b((int32_T)d, 1,
                  NodesOnElementCG->size[0], &wh_emlrtBCI, &st);
              }

              for (i3 = 0; i3 < loop_ub; i3++) {
                b_nodeC2_data[i3] = (NodesOnElementCG->data[((int32_T)d +
                  NodesOnElementCG->size[0] * i3) - 1] == node);
              }

              b_st.site = &ug_emlrtRSI;
              f_eml_find(&b_st, b_nodeC2_data, nodeC2_size, ii_data, ii_size);
              nel = ii_size[1];
              for (i3 = 0; i3 < nel; i3++) {
                b_i = ii_data[i3];
                if (((int8_T)b_i < 1) || ((int8_T)b_i > NodesOnElementDG->size[1]))
                {
                  emlrtDynamicBoundsCheckR2012b((int8_T)b_i, 1,
                    NodesOnElementDG->size[1], &bp_emlrtBCI, sp);
                }

                b_tmp_data[i3] = (int8_T)b_i;
              }

              if (notdone > NodesOnElementDG->size[0]) {
                emlrtDynamicBoundsCheckR2012b(notdone, 1, NodesOnElementDG->
                  size[0], &ei_emlrtBCI, sp);
              }

              for (i3 = 0; i3 < nel; i3++) {
                NodesOnElementDG->data[(notdone + NodesOnElementDG->size[0] *
                  (b_tmp_data[i3] - 1)) - 1] = b_i2;
              }

              if (*emlrtBreakCheckR2012bFlagVar != 0) {
                emlrtBreakCheckR2012b(sp);
              }
            }
          } else {
            numPBC = 1;
          }
        }

        if (*emlrtBreakCheckR2012bFlagVar != 0) {
          emlrtBreakCheckR2012b(sp);
        }
      }
    }

    /* if facnum */
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  emxFree_real_T(&b_MorePBC);
  emxFreeStruct_sparse2(&h_expl_temp);
  emxFreeStruct_sparse2(&g_expl_temp);
  emxFreeStruct_sparse2(&f_expl_temp);
  emxFree_int32_T(&c_ii);
  emxFree_int32_T(&indx);
  emxFree_real_T(&interreg2);
  emxFree_real_T(&setnPBC);
  emxFree_real_T(&setPBC);
  emxFree_real_T(&ElementsOnNodeA);

  /*  Form interfaces between all facs on interiors of specified material regions */
  st.site = &i_emlrtRSI;
  diag(&st, InterTypes, MorePBC);
  if (b_norm(MorePBC) > 0.0) {
    i = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
    NodesOnElementCG->size[0] = NodesOnElementDG->size[0];
    NodesOnElementCG->size[1] = NodesOnElementDG->size[1];
    emxEnsureCapacity_real_T(sp, NodesOnElementCG, i, &mi_emlrtRTEI);
    loop_ub = NodesOnElementDG->size[0] * NodesOnElementDG->size[1];
    for (i = 0; i < loop_ub; i++) {
      NodesOnElementCG->data[i] = NodesOnElementDG->data[i];
    }

    /*  Update connectivities to reflect presence of interfaces */
    i = (int32_T)muDoubleScalarFloor(b_i2);
    if (b_i2 != i) {
      emlrtIntegerCheckR2012b(b_i2, &ae_emlrtDCI, sp);
    }

    i1 = ElementsOnNodeNum2->size[0];
    loop_ub = (int32_T)b_i2;
    ElementsOnNodeNum2->size[0] = loop_ub;
    emxEnsureCapacity_real_T(sp, ElementsOnNodeNum2, i1, &pi_emlrtRTEI);
    if (loop_ub != i) {
      emlrtIntegerCheckR2012b(b_i2, &ae_emlrtDCI, sp);
    }

    for (i1 = 0; i1 < loop_ub; i1++) {
      ElementsOnNodeNum2->data[i1] = 0.0;
    }

    i1 = NodeCGDG->size[0] * NodeCGDG->size[1];
    NodeCGDG->size[0] = 12;
    emxEnsureCapacity_real_T(sp, NodeCGDG, i1, &ri_emlrtRTEI);
    if (loop_ub != i) {
      emlrtIntegerCheckR2012b(b_i2, &bc_emlrtDCI, sp);
    }

    i1 = NodeCGDG->size[0] * NodeCGDG->size[1];
    NodeCGDG->size[1] = loop_ub;
    emxEnsureCapacity_real_T(sp, NodeCGDG, i1, &ri_emlrtRTEI);
    if (loop_ub != i) {
      emlrtIntegerCheckR2012b(b_i2, &ce_emlrtDCI, sp);
    }

    nel = 12 * (int32_T)b_i2;
    for (i = 0; i < nel; i++) {
      NodeCGDG->data[i] = 0.0;
    }

    /*  Mapping of DG nodal IDs to CG nodal IDs */
    if (1.0 > b_i2) {
      loop_ub = 0;
      setAll->size[0] = 1;
      setAll->size[1] = 0;
    } else {
      i = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      setAll->size[1] = loop_ub;
      emxEnsureCapacity_real_T(sp, setAll, i, &xi_emlrtRTEI);
      nel = (int32_T)b_i2 - 1;
      for (i = 0; i <= nel; i++) {
        setAll->data[i] = (real_T)i + 1.0;
      }
    }

    b_numel[0] = 1;
    b_numel[1] = loop_ub;
    emlrtSubAssignSizeCheckR2012b(&b_numel[0], 2, &setAll->size[0], 2,
      &l_emlrtECI, sp);
    loop_ub = setAll->size[1];
    for (i = 0; i < loop_ub; i++) {
      NodeCGDG->data[12 * i] = setAll->data[i];
    }

    i = (int32_T)nummat;
    emlrtForLoopVectorCheckR2012b(1.0, 1.0, nummat, mxDOUBLE_CLASS, (int32_T)
      nummat, &gc_emlrtRTEI, sp);
    for (nume = 0; nume < i; nume++) {
      /*  ID for material pair (row=mat2, col=mat1) */
      if (((int32_T)(nume + 1U) < 1) || ((int32_T)(nume + 1U) > InterTypes->
           size[0])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(nume + 1U), 1, InterTypes->size
          [0], &pn_emlrtBCI, sp);
      }

      if (((int32_T)(nume + 1U) < 1) || ((int32_T)(nume + 1U) > InterTypes->
           size[1])) {
        emlrtDynamicBoundsCheckR2012b((int32_T)(nume + 1U), 1, InterTypes->size
          [1], &pn_emlrtBCI, sp);
      }

      if (InterTypes->data[nume + InterTypes->size[0] * nume] > 0.0) {
        /*  Find elements belonging to material mat2 */
        st.site = &h_emlrtRSI;
        i1 = b_NodesOnPBCnum->size[0];
        b_NodesOnPBCnum->size[0] = RegionOnElement->size[0];
        emxEnsureCapacity_boolean_T(&st, b_NodesOnPBCnum, i1, &ej_emlrtRTEI);
        loop_ub = RegionOnElement->size[0];
        for (i1 = 0; i1 < loop_ub; i1++) {
          b_NodesOnPBCnum->data[i1] = (RegionOnElement->data[i1] == (real_T)nume
            + 1.0);
        }

        b_st.site = &ug_emlrtRSI;
        eml_find(&b_st, b_NodesOnPBCnum, ii);
        i1 = MorePBC->size[0];
        MorePBC->size[0] = ii->size[0];
        emxEnsureCapacity_real_T(&st, MorePBC, i1, &gj_emlrtRTEI);
        loop_ub = ii->size[0];
        for (i1 = 0; i1 < loop_ub; i1++) {
          MorePBC->data[i1] = ii->data[i1];
        }

        /*  Loop over elements in material mat2, explode all the elements */
        i1 = MorePBC->size[0];
        if (0 <= i1 - 1) {
          b_nodeC2_size[0] = 1;
        }

        for (b_i = 0; b_i < i1; b_i++) {
          if ((b_i + 1 < 1) || (b_i + 1 > MorePBC->size[0])) {
            emlrtDynamicBoundsCheckR2012b(b_i + 1, 1, MorePBC->size[0],
              &tn_emlrtBCI, sp);
          }

          elem = (int32_T)MorePBC->data[b_i];
          if (1.0 > nen) {
            loop_ub = 0;
          } else {
            if (1 > NodesOnElementDG->size[1]) {
              emlrtDynamicBoundsCheckR2012b(1, 1, NodesOnElementDG->size[1],
                &gi_emlrtBCI, sp);
            }

            if ((int32_T)nen > NodesOnElementDG->size[1]) {
              emlrtDynamicBoundsCheckR2012b((int32_T)nen, 1,
                NodesOnElementDG->size[1], &hi_emlrtBCI, sp);
            }

            loop_ub = (int32_T)nen;
          }

          i2 = (int32_T)MorePBC->data[b_i];
          if ((i2 < 1) || (i2 > NodesOnElementDG->size[0])) {
            emlrtDynamicBoundsCheckR2012b(i2, 1, NodesOnElementDG->size[0],
              &fi_emlrtBCI, sp);
          }

          b_nodeC2_size[1] = loop_ub;
          for (i3 = 0; i3 < loop_ub; i3++) {
            nodeC2_data[i3] = NodesOnElementDG->data[(i2 +
              NodesOnElementDG->size[0] * i3) - 1];
          }

          i2 = intnnz(nodeC2_data, b_nodeC2_size);
          for (locN = 0; locN < i2; locN++) {
            /*  Loop over local Nodes */
            if ((elem < 1) || (elem > NodesOnElementCG->size[0])) {
              emlrtDynamicBoundsCheckR2012b(elem, 1, NodesOnElementCG->size[0],
                &ao_emlrtBCI, sp);
            }

            if (((int32_T)(locN + 1U) < 1) || ((int32_T)(locN + 1U) >
                 NodesOnElementCG->size[1])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)(locN + 1U), 1,
                NodesOnElementCG->size[1], &ao_emlrtBCI, sp);
            }

            node = NodesOnElementCG->data[(elem + NodesOnElementCG->size[0] *
              locN) - 1];
            if (node != (int32_T)muDoubleScalarFloor(node)) {
              emlrtIntegerCheckR2012b(node, &he_emlrtDCI, sp);
            }

            if (((int32_T)node < 1) || ((int32_T)node > ElementsOnNodeNum2->
                 size[0])) {
              emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
                ElementsOnNodeNum2->size[0], &co_emlrtBCI, sp);
            }

            if (ElementsOnNodeNum2->data[(int32_T)node - 1] == 0.0) {
              /*  only duplicate nodes after the first time encountered */
              if (((int32_T)node < 1) || ((int32_T)node >
                   ElementsOnNodeNum2->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
                  ElementsOnNodeNum2->size[0], &eo_emlrtBCI, sp);
              }

              ElementsOnNodeNum2->data[(int32_T)node - 1] = 1.0;
              if (((int32_T)node < 1) || ((int32_T)node > NodeCGDG->size[1])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)node, 1, NodeCGDG->size[1],
                  &fo_emlrtBCI, sp);
              }

              NodeCGDG->data[12 * ((int32_T)node - 1)] = node;

              /* node in DG mesh with same coordinates */
            } else {
              b_i2++;
              if (elem > NodesOnElementDG->size[0]) {
                emlrtDynamicBoundsCheckR2012b(elem, 1, NodesOnElementDG->size[0],
                  &do_emlrtBCI, sp);
              }

              if (((int32_T)(locN + 1U) < 1) || ((int32_T)(locN + 1U) >
                   NodesOnElementDG->size[1])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)(locN + 1U), 1,
                  NodesOnElementDG->size[1], &do_emlrtBCI, sp);
              }

              NodesOnElementDG->data[(elem + NodesOnElementDG->size[0] * locN) -
                1] = b_i2;
              if (((int32_T)node < 1) || ((int32_T)node > Coordinates3->size[0]))
              {
                emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
                  Coordinates3->size[0], &ni_emlrtBCI, sp);
              }

              if (((int32_T)b_i2 < 1) || ((int32_T)b_i2 > Coordinates3->size[0]))
              {
                emlrtDynamicBoundsCheckR2012b((int32_T)b_i2, 1,
                  Coordinates3->size[0], &oi_emlrtBCI, sp);
              }

              Coordinates3->data[(int32_T)b_i2 - 1] = Coordinates3->data
                [(int32_T)node - 1];
              Coordinates3->data[((int32_T)b_i2 + Coordinates3->size[0]) - 1] =
                Coordinates3->data[((int32_T)node + Coordinates3->size[0]) - 1];
              i3 = NodesOnElement->size[0];
              if (elem > i3) {
                emlrtDynamicBoundsCheckR2012b(elem, 1, i3, &ho_emlrtBCI, sp);
              }

              i3 = NodesOnElement->size[1];
              if (((int32_T)(locN + 1U) < 1) || ((int32_T)(locN + 1U) > i3)) {
                emlrtDynamicBoundsCheckR2012b((int32_T)(locN + 1U), 1, i3,
                  &ho_emlrtBCI, sp);
              }

              numA = NodesOnElement->data[(elem + NodesOnElement->size[0] * locN)
                - 1];
              if (numA != (int32_T)muDoubleScalarFloor(numA)) {
                emlrtIntegerCheckR2012b(numA, &fb_emlrtDCI, sp);
              }

              if (((int32_T)numA < 1) || ((int32_T)numA >
                   ElementsOnNodeNum->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1,
                  ElementsOnNodeNum->size[0], &pi_emlrtBCI, sp);
              }

              d = ElementsOnNodeNum->data[(int32_T)numA - 1];
              if (d < 1.0) {
                setAll->size[0] = 1;
                setAll->size[1] = 0;
              } else if (muDoubleScalarIsInf(d) && (1.0 == d)) {
                i3 = setAll->size[0] * setAll->size[1];
                setAll->size[0] = 1;
                setAll->size[1] = 1;
                emxEnsureCapacity_real_T(sp, setAll, i3, &nh_emlrtRTEI);
                setAll->data[0] = rtNaN;
              } else {
                i3 = setAll->size[0] * setAll->size[1];
                setAll->size[0] = 1;
                loop_ub = (int32_T)muDoubleScalarFloor(d - 1.0);
                setAll->size[1] = loop_ub + 1;
                emxEnsureCapacity_real_T(sp, setAll, i3, &nh_emlrtRTEI);
                for (i3 = 0; i3 <= loop_ub; i3++) {
                  setAll->data[i3] = (real_T)i3 + 1.0;
                }
              }

              st.site = &g_emlrtRSI;
              b_st.site = &g_emlrtRSI;
              b_sparse_parenReference(&b_st, ElementsOnNode->d,
                ElementsOnNode->colidx, ElementsOnNode->rowidx,
                ElementsOnNode->m, ElementsOnNode->n, setAll, numA, NodeRegSum.d,
                NodeRegSum.colidx, NodeRegSum.rowidx, &NodeRegSum.m);
              b_st.site = &g_emlrtRSI;
              c_sparse_eq(&b_st, elem, NodeRegSum.d, NodeRegSum.colidx,
                          NodeRegSum.rowidx, NodeRegSum.m, &d_expl_temp);
              i3 = c_expl_temp.colidx->size[0];
              c_expl_temp.colidx->size[0] = d_expl_temp.colidx->size[0];
              emxEnsureCapacity_int32_T(&st, c_expl_temp.colidx, i3,
                &nj_emlrtRTEI);
              loop_ub = d_expl_temp.colidx->size[0];
              for (i3 = 0; i3 < loop_ub; i3++) {
                c_expl_temp.colidx->data[i3] = d_expl_temp.colidx->data[i3];
              }

              i3 = c_expl_temp.rowidx->size[0];
              c_expl_temp.rowidx->size[0] = d_expl_temp.rowidx->size[0];
              emxEnsureCapacity_int32_T(&st, c_expl_temp.rowidx, i3,
                &nj_emlrtRTEI);
              loop_ub = d_expl_temp.rowidx->size[0];
              for (i3 = 0; i3 < loop_ub; i3++) {
                c_expl_temp.rowidx->data[i3] = d_expl_temp.rowidx->data[i3];
              }

              b_st.site = &ug_emlrtRSI;
              g_eml_find(&b_st, c_expl_temp.colidx, c_expl_temp.rowidx,
                         d_expl_temp.m, ii);
              i3 = ElementsOnNodePBCNum->size[0];
              ElementsOnNodePBCNum->size[0] = ii->size[0];
              emxEnsureCapacity_real_T(sp, ElementsOnNodePBCNum, i3,
                &gi_emlrtRTEI);
              loop_ub = ii->size[0];
              for (i3 = 0; i3 < loop_ub; i3++) {
                ElementsOnNodePBCNum->data[i3] = ii->data[i3];
              }

              st.site = &emlrtRSI;
              c_sparse_parenAssign(&st, ElementsOnNodeDup, b_i2,
                                   ElementsOnNodePBCNum, numA);

              /*    Add element to star list, increment number of elem in star */
              if (((int32_T)node < 1) || ((int32_T)node >
                   ElementsOnNodeNum2->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
                  ElementsOnNodeNum2->size[0], &ro_emlrtBCI, sp);
              }

              numA = ElementsOnNodeNum2->data[(int32_T)node - 1] + 1.0;
              if (((int32_T)node < 1) || ((int32_T)node >
                   ElementsOnNodeNum2->size[0])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)node, 1,
                  ElementsOnNodeNum2->size[0], &so_emlrtBCI, sp);
              }

              ElementsOnNodeNum2->data[(int32_T)node - 1] = numA;
              if (((int32_T)numA < 1) || ((int32_T)numA > 12)) {
                emlrtDynamicBoundsCheckR2012b((int32_T)numA, 1, 12, &to_emlrtBCI,
                  sp);
              }

              if (((int32_T)node < 1) || ((int32_T)node > NodeCGDG->size[1])) {
                emlrtDynamicBoundsCheckR2012b((int32_T)node, 1, NodeCGDG->size[1],
                  &uo_emlrtBCI, sp);
              }

              NodeCGDG->data[((int32_T)numA + 12 * ((int32_T)node - 1)) - 1] =
                b_i2;

              /* node in DG mesh with same coordinates */
            }

            if (*emlrtBreakCheckR2012bFlagVar != 0) {
              emlrtBreakCheckR2012b(sp);
            }
          }

          /* k */
          if (*emlrtBreakCheckR2012bFlagVar != 0) {
            emlrtBreakCheckR2012b(sp);
          }
        }
      }

      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    st.site = &f_emlrtRSI;
    i = b_NodesOnPBCnum->size[0];
    b_NodesOnPBCnum->size[0] = ElementsOnNodeNum2->size[0];
    emxEnsureCapacity_boolean_T(&st, b_NodesOnPBCnum, i, &cj_emlrtRTEI);
    loop_ub = ElementsOnNodeNum2->size[0];
    for (i = 0; i < loop_ub; i++) {
      b_NodesOnPBCnum->data[i] = (ElementsOnNodeNum2->data[i] == 0.0);
    }

    b_st.site = &ug_emlrtRSI;
    eml_find(&b_st, b_NodesOnPBCnum, ii);
    loop_ub = ii->size[0];
    for (i = 0; i < loop_ub; i++) {
      if ((ii->data[i] < 1) || (ii->data[i] > ElementsOnNodeNum2->size[0])) {
        emlrtDynamicBoundsCheckR2012b(ii->data[i], 1, ElementsOnNodeNum2->size[0],
          &qn_emlrtBCI, sp);
      }
    }

    loop_ub = ii->size[0];
    for (i = 0; i < loop_ub; i++) {
      ElementsOnNodeNum2->data[ii->data[i] - 1] = 1.0;
    }
  } else {
    /*  no DG in interior of any materials */
    i = (int32_T)muDoubleScalarFloor(b_i2);
    if (b_i2 != i) {
      emlrtIntegerCheckR2012b(b_i2, &yd_emlrtDCI, sp);
    }

    i1 = ElementsOnNodeNum2->size[0];
    loop_ub = (int32_T)b_i2;
    ElementsOnNodeNum2->size[0] = loop_ub;
    emxEnsureCapacity_real_T(sp, ElementsOnNodeNum2, i1, &ni_emlrtRTEI);
    if (loop_ub != i) {
      emlrtIntegerCheckR2012b(b_i2, &yd_emlrtDCI, sp);
    }

    for (i1 = 0; i1 < loop_ub; i1++) {
      ElementsOnNodeNum2->data[i1] = 1.0;
    }

    /*  set one copy of duplicated nodes per material ID so that BC expanding subroutine works properly. */
    i1 = NodeCGDG->size[0] * NodeCGDG->size[1];
    NodeCGDG->size[0] = 12;
    emxEnsureCapacity_real_T(sp, NodeCGDG, i1, &qi_emlrtRTEI);
    if (loop_ub != i) {
      emlrtIntegerCheckR2012b(b_i2, &ac_emlrtDCI, sp);
    }

    i1 = NodeCGDG->size[0] * NodeCGDG->size[1];
    NodeCGDG->size[1] = loop_ub;
    emxEnsureCapacity_real_T(sp, NodeCGDG, i1, &qi_emlrtRTEI);
    if (loop_ub != i) {
      emlrtIntegerCheckR2012b(b_i2, &be_emlrtDCI, sp);
    }

    nel = 12 * loop_ub;
    for (i = 0; i < nel; i++) {
      NodeCGDG->data[i] = 0.0;
    }

    /*  Mapping of DG nodal IDs to CG nodal IDs */
    if (1.0 > b_i2) {
      loop_ub = 0;
      setAll->size[0] = 1;
      setAll->size[1] = 0;
    } else {
      i = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      nel = loop_ub - 1;
      setAll->size[1] = loop_ub;
      emxEnsureCapacity_real_T(sp, setAll, i, &ui_emlrtRTEI);
      for (i = 0; i <= nel; i++) {
        setAll->data[i] = (real_T)i + 1.0;
      }
    }

    b_numel[0] = 1;
    b_numel[1] = loop_ub;
    emlrtSubAssignSizeCheckR2012b(&b_numel[0], 2, &setAll->size[0], 2,
      &m_emlrtECI, sp);
    loop_ub = setAll->size[1];
    for (i = 0; i < loop_ub; i++) {
      NodeCGDG->data[12 * i] = setAll->data[i];
    }
  }

  emxFree_boolean_T(&b_NodesOnPBCnum);
  emxFreeStruct_sparse3(&d_expl_temp);
  emxFreeStruct_sparse3(&c_expl_temp);
  emxFree_int32_T(&ii);
  emxFreeStruct_sparse(&NodeRegSum);
  emxFree_real_T(&setAll);
  emxFree_real_T(&MorePBC);
  emxFree_real_T(&ElementsOnNodePBCNum);

  /*  Form final lists of DG interfaces */
  *numnp = b_i2;
  if (1.0 > b_i2) {
    loop_ub = 0;
  } else {
    if (1 > Coordinates3->size[0]) {
      emlrtDynamicBoundsCheckR2012b(1, 1, Coordinates3->size[0], &qi_emlrtBCI,
        sp);
    }

    if (((int32_T)b_i2 < 1) || ((int32_T)b_i2 > Coordinates3->size[0])) {
      emlrtDynamicBoundsCheckR2012b((int32_T)b_i2, 1, Coordinates3->size[0],
        &ri_emlrtBCI, sp);
    }

    loop_ub = (int32_T)b_i2;
  }

  i = Coordinates->size[0] * Coordinates->size[1];
  Coordinates->size[0] = loop_ub;
  Coordinates->size[1] = 2;
  emxEnsureCapacity_real_T(sp, Coordinates, i, &aj_emlrtRTEI);
  for (i = 0; i < loop_ub; i++) {
    Coordinates->data[i] = Coordinates3->data[i];
  }

  for (i = 0; i < loop_ub; i++) {
    Coordinates->data[i + Coordinates->size[0]] = Coordinates3->data[i +
      Coordinates3->size[0]];
  }

  emxFree_real_T(&Coordinates3);
  if (27 < NodesOnElementDG->size[1]) {
    emlrtDimSizeGeqCheckR2012b(27, NodesOnElementDG->size[1], &k_emlrtECI, sp);
  }

  i = NodesOnElement->size[0] * NodesOnElement->size[1];
  NodesOnElement->size[0] = NodesOnElementDG->size[0];
  NodesOnElement->size[1] = NodesOnElementDG->size[1];
  emxEnsureCapacity_real_T(sp, NodesOnElement, i, &dj_emlrtRTEI);
  loop_ub = NodesOnElementDG->size[1];
  for (i = 0; i < loop_ub; i++) {
    nel = NodesOnElementDG->size[0];
    for (i1 = 0; i1 < nel; i1++) {
      NodesOnElement->data[i1 + NodesOnElement->size[0] * i] =
        NodesOnElementDG->data[i1 + NodesOnElementDG->size[0] * i];
    }
  }

  st.site = &e_emlrtRSI;
  if (muDoubleScalarIsNaN(usePBC)) {
    emlrtErrorWithMessageIdR2018a(&st, &mb_emlrtRTEI, "MATLAB:nologicalnan",
      "MATLAB:nologicalnan", 0);
  }

  if (usePBC != 0.0) {
    i = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
    NodesOnElementCG->size[0] = NodesOnElementPBC->size[0];
    NodesOnElementCG->size[1] = NodesOnElementPBC->size[1];
    emxEnsureCapacity_real_T(sp, NodesOnElementCG, i, &hj_emlrtRTEI);
    loop_ub = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1];
    for (i = 0; i < loop_ub; i++) {
      NodesOnElementCG->data[i] = NodesOnElementPBC->data[i];
    }
  }

  emxFree_real_T(&NodesOnElementPBC);

  /*  %% Clear out intermediate variables */
  /*  if clearinter */
  /*  KeyList = {'numEonB','numEonF','ElementsOnBoundary','numSI','ElementsOnFacet',... */
  /*  'ElementsOnNode','ElementsOnNodeA','ElementsOnNodeB','ElementsOnNodeDup',... */
  /*  'ElementsOnNodeNum','numfac','ElementsOnNodeNum2','numinttype','FacetsOnElement',... */
  /*  'FacetsOnElementInt','FacetsOnInterface','FacetsOnInterfaceNum','FacetsOnNode',... */
  /*  'FacetsOnNodeCut','FacetsOnNodeInt','FacetsOnNodeNum','NodeCGDG','NodeReg',... */
  /*  'NodesOnElementCG','NodesOnElementDG','NodesOnInterface','NodesOnInterfaceNum',... */
  /*  'numCL','maxel','numelPBC','RegionOnElementDG','numPBC','TieNodesNew','TieNodes',... */
  /*  'NodesOnPBC','NodesOnPBCnum','NodesOnLink','NodesOnLinknum','numEonPBC',... */
  /*  'FacetsOnPBC','FacetsOnPBCNum','FacetsOnIntMinusPBC','FacetsOnIntMinusPBCNum','MPCList'}; */
  /*  if isOctave */
  /*      wholething = [{'-x'},KeyList,currvariables']; */
  /*      clear(wholething{:}) */
  /*  else */
  /*      wholething = [{'-except'},KeyList,currvariables']; */
  /*      clearvars(wholething{:}) */
  /*  end */
  /*  end */
  *numSI = numSI_tmp;
  *NodesOnInterfaceNum = elemB;
  *numCL = 0.0;
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (DEIPFunc2.c) */
