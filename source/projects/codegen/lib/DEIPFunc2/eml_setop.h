/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * eml_setop.h
 *
 * Code generation for function 'eml_setop'
 *
 */

#ifndef EML_SETOP_H
#define EML_SETOP_H

/* Include files */
#include "DEIPFunc2_types.h"
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>
#ifdef __cplusplus

extern "C" {

#endif

  /* Function Declarations */
  void b_do_vectors(const emxArray_real_T *a, const emxArray_real_T *b,
                    emxArray_real_T *c, emxArray_int32_T *ia, int ib_size[1]);
  void c_do_vectors(const emxArray_real_T *a, const emxArray_real_T *b,
                    emxArray_real_T *c, emxArray_int32_T *ia, emxArray_int32_T
                    *ib);
  void do_vectors(const double a_data[], const int a_size[1], double b, double
                  c_data[], int c_size[1], int ia_data[], int ia_size[1], int
                  ib_size[1]);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (eml_setop.h) */
