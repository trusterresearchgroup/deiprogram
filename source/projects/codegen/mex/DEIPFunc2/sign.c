/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sign.c
 *
 * Code generation for function 'sign'
 *
 */

/* Include files */
#include "sign.h"
#include "DEIPFunc2_data.h"
#include "DEIPFunc2_types.h"
#include "eml_int_forloop_overflow_check.h"
#include "rt_nonfinite.h"
#include "mwmathutil.h"

/* Variable Definitions */
static emlrtRSInfo mk_emlrtRSI = { 18, /* lineNo */
  "sign",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elfun\\sign.m"/* pathName */
};

static emlrtRSInfo nk_emlrtRSI = { 22, /* lineNo */
  "applyScalarFunctionInPlace",        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\applyScalarFunctionInPlace.m"/* pathName */
};

static emlrtRSInfo ok_emlrtRSI = { 24, /* lineNo */
  "applyScalarFunctionInPlace",        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\applyScalarFunctionInPlace.m"/* pathName */
};

static emlrtRSInfo pk_emlrtRSI = { 10, /* lineNo */
  "sparse/applyScalarFunctionInPlace", /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\applyScalarFunctionInPlace.m"/* pathName */
};

static emlrtRSInfo qk_emlrtRSI = { 55, /* lineNo */
  "sparse/applyScalarFunctionInPlace", /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\applyScalarFunctionInPlace.m"/* pathName */
};

static emlrtRSInfo rk_emlrtRSI = { 33, /* lineNo */
  "applyScalarFunctionInPlace",        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\applyScalarFunctionInPlace.m"/* pathName */
};

/* Function Definitions */
void b_sign(const emlrtStack *sp, f_sparse *x)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack st;
  real_T val;
  int32_T c;
  int32_T currRowIdx;
  int32_T k;
  int32_T nx;
  int32_T ridx;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &mk_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  b_st.site = &nk_emlrtRSI;
  if ((x->m & 65535) > MAX_int32_T - ((x->m >> 16) << 16)) {
    emlrtErrorWithMessageIdR2018a(&b_st, &ic_emlrtRTEI,
      "Coder:toolbox:SparseNumelTooBig", "Coder:toolbox:SparseNumelTooBig", 0);
  }

  b_st.site = &ok_emlrtRSI;
  c_st.site = &pk_emlrtRSI;
  nx = x->d->size[0];
  d_st.site = &rk_emlrtRSI;
  if ((1 <= x->d->size[0]) && (x->d->size[0] > 2147483646)) {
    e_st.site = &ff_emlrtRSI;
    check_forloop_overflow_error(&e_st);
  }

  for (k = 0; k < nx; k++) {
    x->d->data[k] = muDoubleScalarSign(x->d->data[k]);
  }

  c_st.site = &qk_emlrtRSI;
  nx = 1;
  k = x->colidx->size[0];
  d_st.site = &jj_emlrtRSI;
  if ((1 <= x->colidx->size[0] - 1) && (x->colidx->size[0] - 1 > 2147483646)) {
    e_st.site = &ff_emlrtRSI;
    check_forloop_overflow_error(&e_st);
  }

  for (c = 0; c <= k - 2; c++) {
    ridx = x->colidx->data[c];
    x->colidx->data[c] = nx;
    while (ridx < x->colidx->data[c + 1]) {
      currRowIdx = x->rowidx->data[ridx - 1];
      val = x->d->data[ridx - 1];
      ridx++;
      if (val != 0.0) {
        x->d->data[nx - 1] = val;
        x->rowidx->data[nx - 1] = currRowIdx;
        nx++;
      }
    }
  }

  x->colidx->data[x->colidx->size[0] - 1] = nx;
}

/* End of code generation (sign.c) */
