/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * find.h
 *
 * Code generation for function 'find'
 *
 */

#pragma once

/* Include files */
#include "DEIPFunc3_types.h"
#include "rtwtypes.h"
#include "emlrt.h"
#include "mex.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Function Declarations */
void b_eml_find(const emlrtStack *sp, const emxArray_real_T *x, emxArray_int32_T
                *i);
void c_eml_find(const emlrtStack *sp, const emxArray_real_T *x, emxArray_int32_T
                *i);
void d_eml_find(const emlrtStack *sp, const emxArray_real_T *x, emxArray_int32_T
                *i);
void e_eml_find(const emlrtStack *sp, const emxArray_real_T *x_d, const
                emxArray_int32_T *x_colidx, const emxArray_int32_T *x_rowidx,
                int32_T x_m, int32_T x_n, emxArray_int32_T *i, emxArray_int32_T *
                j);
void eml_find(const emlrtStack *sp, const emxArray_boolean_T *x,
              emxArray_int32_T *i);
void f_eml_find(const emlrtStack *sp, const boolean_T x_data[], const int32_T
                x_size[2], int32_T i_data[], int32_T i_size[2]);
void g_eml_find(const emlrtStack *sp, const emxArray_int32_T *x_colidx, const
                emxArray_int32_T *x_rowidx, int32_T x_m, emxArray_int32_T *i);

/* End of code generation (find.h) */
