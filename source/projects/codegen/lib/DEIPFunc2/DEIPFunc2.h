/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc2.h
 *
 * Code generation for function 'DEIPFunc2'
 *
 */

#ifndef DEIPFUNC2_H
#define DEIPFUNC2_H

/* Include files */
#include "DEIPFunc2_types.h"
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>
#ifdef __cplusplus

extern "C" {

#endif

  /* Function Declarations */
  extern void DEIPFunc2(emxArray_real_T *InterTypes, emxArray_real_T
                        *NodesOnElement, const emxArray_real_T *RegionOnElement,
                        emxArray_real_T *Coordinates, double *numnp, double
                        numel, double nummat, double nen, double ndm, double
                        usePBC, const double *numMPC, const emxArray_real_T
                        *MPCList, double *numEonB, emxArray_real_T *numEonF,
                        emxArray_real_T *ElementsOnBoundary, double *numSI,
                        emxArray_real_T *ElementsOnFacet, d_sparse
                        *ElementsOnNode, d_sparse *ElementsOnNodeDup,
                        emxArray_real_T *ElementsOnNodeNum, double *numfac,
                        emxArray_real_T *ElementsOnNodeNum2, double *numinttype,
                        emxArray_real_T *FacetsOnElement, emxArray_real_T
                        *FacetsOnElementInt, emxArray_real_T *FacetsOnInterface,
                        emxArray_real_T *FacetsOnInterfaceNum, d_sparse
                        *FacetsOnNode, d_sparse *FacetsOnNodeCut, d_sparse
                        *FacetsOnNodeInt, emxArray_real_T *FacetsOnNodeNum,
                        emxArray_real_T *NodeCGDG, d_sparse *NodeReg,
                        emxArray_real_T *NodesOnElementCG, emxArray_real_T
                        *NodesOnElementDG, emxArray_real_T *NodesOnInterface,
                        double *NodesOnInterfaceNum, double *numCL,
                        emxArray_real_T *NodesOnPBC, emxArray_real_T
                        *NodesOnPBCnum, emxArray_real_T *NodesOnLink,
                        emxArray_real_T *NodesOnLinknum, emxArray_real_T
                        *numEonPBC, emxArray_real_T *FacetsOnPBC,
                        emxArray_real_T *FacetsOnPBCNum, emxArray_real_T
                        *FacetsOnIntMinusPBC, emxArray_real_T
                        *FacetsOnIntMinusPBCNum);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (DEIPFunc2.h) */
