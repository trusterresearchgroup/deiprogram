/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * locBsearch.c
 *
 * Code generation for function 'locBsearch'
 *
 */

/* Include files */
#include "locBsearch.h"
#include "DEIPFunc3_types.h"
#include "rt_nonfinite.h"

/* Function Definitions */
void b_sparse_locBsearch(const emxArray_int32_T *x, int32_T xi, int32_T xstart,
  int32_T xend, int32_T *n, boolean_T *found)
{
  int32_T high_i;
  int32_T low_ip1;
  int32_T mid_i;
  if (xstart < xend) {
    if (xi < x->data[xstart - 1]) {
      *n = xstart - 1;
      *found = false;
    } else {
      high_i = xend;
      *n = xstart;
      low_ip1 = xstart;
      while (high_i > low_ip1 + 1) {
        mid_i = (*n >> 1) + (high_i >> 1);
        if (((*n & 1) == 1) && ((high_i & 1) == 1)) {
          mid_i++;
        }

        if (xi >= x->data[mid_i - 1]) {
          *n = mid_i;
          low_ip1 = mid_i;
        } else {
          high_i = mid_i;
        }
      }

      *found = (x->data[*n - 1] == xi);
    }
  } else if (xstart == xend) {
    *n = xstart - 1;
    *found = false;
  } else {
    *n = 0;
    *found = false;
  }
}

void sparse_locBsearch(const emxArray_int32_T *x, real_T xi, int32_T xstart,
  int32_T xend, int32_T *n, boolean_T *found)
{
  int32_T high_i;
  int32_T low_ip1;
  int32_T mid_i;
  if (xstart < xend) {
    if (xi < x->data[xstart - 1]) {
      *n = xstart - 1;
      *found = false;
    } else {
      high_i = xend;
      *n = xstart;
      low_ip1 = xstart;
      while (high_i > low_ip1 + 1) {
        mid_i = (*n >> 1) + (high_i >> 1);
        if (((*n & 1) == 1) && ((high_i & 1) == 1)) {
          mid_i++;
        }

        if (xi >= x->data[mid_i - 1]) {
          *n = mid_i;
          low_ip1 = mid_i;
        } else {
          high_i = mid_i;
        }
      }

      *found = (x->data[*n - 1] == xi);
    }
  } else if (xstart == xend) {
    *n = xstart - 1;
    *found = false;
  } else {
    *n = 0;
    *found = false;
  }
}

/* End of code generation (locBsearch.c) */
