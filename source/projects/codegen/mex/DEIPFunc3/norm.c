/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * norm.c
 *
 * Code generation for function 'norm'
 *
 */

/* Include files */
#include "norm.h"
#include "DEIPFunc3_types.h"
#include "rt_nonfinite.h"
#include "blas.h"
#include <stddef.h>

/* Function Definitions */
real_T b_norm(const real_T x_data[], const int32_T x_size[2])
{
  ptrdiff_t incx_t;
  ptrdiff_t n_t;
  n_t = (ptrdiff_t)x_size[1];
  incx_t = (ptrdiff_t)1;
  return dnrm2(&n_t, &x_data[0], &incx_t);
}

real_T c_norm(const emxArray_real_T *x)
{
  ptrdiff_t incx_t;
  ptrdiff_t n_t;
  real_T y;
  if (x->size[0] == 0) {
    y = 0.0;
  } else {
    n_t = (ptrdiff_t)x->size[0];
    incx_t = (ptrdiff_t)1;
    y = dnrm2(&n_t, &x->data[0], &incx_t);
  }

  return y;
}

/* End of code generation (norm.c) */
