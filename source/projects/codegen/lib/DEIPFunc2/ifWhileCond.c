/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * ifWhileCond.c
 *
 * Code generation for function 'ifWhileCond'
 *
 */

/* Include files */
#include "ifWhileCond.h"
#include "rt_nonfinite.h"

/* Function Definitions */
boolean_T ifWhileCond(const double x_data[], const int x_size[1])
{
  int k;
  boolean_T exitg1;
  boolean_T y;
  y = (x_size[0] != 0);
  if (y) {
    k = 0;
    exitg1 = false;
    while ((!exitg1) && (k <= x_size[0] - 1)) {
      if (x_data[k] == 0.0) {
        y = false;
        exitg1 = true;
      } else {
        k++;
      }
    }
  }

  return y;
}

/* End of code generation (ifWhileCond.c) */
