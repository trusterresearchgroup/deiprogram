% Example demonstrating applying tractions to model
% Domain: 2x2 rectangle, combination of T3 and Q4 elements
% Loading: Pressure of 10 units in x direction, 20 in y direction.
%
% Last revision: 12/02/2019 TJT

%% Generate mesh
nen = 4;

% Block 1
xl = [1 0 0
      2 2 0
      4 0 2
      3 2 2];
type = 'cart';
rinc = 2;
sinc = 2;
node1 = 1;
elmt1 = 1;
mat = 1;
rskip = 0;
btype = 0;
[Coordinates,~,~,numnp,~] = block2d(type,rinc,sinc,node1,elmt1,mat,rskip,btype,xl,nen);
Coordinates = Coordinates';
NodesOnElement = [1 2 4 0
                  2 5 4 0
                  2 3 6 5
                  4 5 8 7
                  5 6 9 0
                  5 9 8 0];
RegionOnElement = ones(6,1);
numel = 6;

%% Boundary conditions; minimal for restraining RBM
NodeBC = [5 1 0
          5 2 0
          6 2 0];
numBC = 3;

%% Surface tractions representing pressures; determine edge numbers via 
% colors from plotMesh2
SurfacesL = [ 0 0 1 1 0 20 0
              0 0 1 3 10 0 0
              0 0 3 1 0 20 0
              0 0 3 2 -10 0 0
              0 0 4 4 10 0 0
              0 0 4 3 0 -20 0
              0 0 5 2 -10 0 0
              0 0 6 2 0 -20 0];
numSL = 8;

%% Material properties
PSPS = 's';
nummat = 1;
MatTypeTable = [1
                1];
MateT = [100e3 0.25 1];
% Output quantity flags
DHist = 1;
FHist = 1;
SHist = 1;
SEHist = 1;

%% Algorithm definitions for FEA_Program
ProbType = [numnp numel nummat 2 2 nen];


% Plot the regions in the mesh
plotMesh2(Coordinates,NodesOnElement,1,(1:numel),[1 1 1 1 1])
plotMesh2(Coordinates,NodesOnElement,4,(1:numel),[1 0 1 0 1])
plotBC(Coordinates, NodeBC, 4)

% % Plot the regions in the mesh
% plotElemCont2(Coordinates,StreListE(1,:)',NodesOnElement,2,(1:numel),[1 0 0],{'title'},{'String','Stress s_x_x'})
% plotElemCont2(Coordinates,StreListE(2,:)',NodesOnElement,3,(1:numel),[1 0 0],{'title'},{'String','Stress s_y_y'})