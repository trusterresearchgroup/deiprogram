/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * nchoosek.h
 *
 * Code generation for function 'nchoosek'
 *
 */

#pragma once

/* Include files */
#include "DEIPFunc3_types.h"
#include "rtwtypes.h"
#include "emlrt.h"
#include "mex.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Function Declarations */
void nchoosek(const emlrtStack *sp, const real_T x_data[], const int32_T x_size
              [2], emxArray_real_T *y);

/* End of code generation (nchoosek.h) */
