/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * ifWhileCond.c
 *
 * Code generation for function 'ifWhileCond'
 *
 */

/* Include files */
#include "ifWhileCond.h"
#include "rt_nonfinite.h"
#include "mwmathutil.h"

/* Variable Definitions */
static emlrtRSInfo di_emlrtRSI = { 18, /* lineNo */
  "ifWhileCond",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\ifWhileCond.m"/* pathName */
};

static emlrtRTEInfo kb_emlrtRTEI = { 32,/* lineNo */
  27,                                  /* colNo */
  "checkNoNaNs",                       /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\ifWhileCond.m"/* pName */
};

/* Function Definitions */
boolean_T b_ifWhileCond(const emlrtStack *sp, const real_T x_data[], const
  int32_T x_size[1])
{
  emlrtStack st;
  int32_T k;
  boolean_T exitg1;
  boolean_T y;
  st.prev = sp;
  st.tls = sp->tls;
  y = (x_size[0] != 0);
  if (y) {
    st.site = &di_emlrtRSI;
    k = 0;
    exitg1 = false;
    while ((!exitg1) && (k <= x_size[0] - 1)) {
      if (muDoubleScalarIsNaN(x_data[k])) {
        emlrtErrorWithMessageIdR2018a(&st, &kb_emlrtRTEI, "MATLAB:nologicalnan",
          "MATLAB:nologicalnan", 0);
      }

      if (x_data[k] == 0.0) {
        y = false;
        exitg1 = true;
      } else {
        k++;
      }
    }
  }

  return y;
}

boolean_T ifWhileCond(const boolean_T x_data[], const int32_T x_size[2])
{
  int32_T k;
  boolean_T exitg1;
  boolean_T y;
  y = (x_size[1] != 0);
  if (y) {
    k = 0;
    exitg1 = false;
    while ((!exitg1) && (k <= x_size[1] - 1)) {
      if (!x_data[k]) {
        y = false;
        exitg1 = true;
      } else {
        k++;
      }
    }
  }

  return y;
}

/* End of code generation (ifWhileCond.c) */
