/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * fillIn.h
 *
 * Code generation for function 'fillIn'
 *
 */

#pragma once

/* Include files */
#include "DEIPFunc3_types.h"
#include "rtwtypes.h"
#include "emlrt.h"
#include "mex.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Function Declarations */
void b_sparse_fillIn(const emlrtStack *sp, f_sparse *this);
void c_sparse_fillIn(const emlrtStack *sp, g_sparse *this);
void d_sparse_fillIn(const emlrtStack *sp, h_sparse *this);
void sparse_fillIn(const emlrtStack *sp, e_sparse *this);

/* End of code generation (fillIn.h) */
