/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sum.h
 *
 * Code generation for function 'sum'
 *
 */

#pragma once

/* Include files */
#include "DEIPFunc2_types.h"
#include "rtwtypes.h"
#include "emlrt.h"
#include "mex.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Function Declarations */
void sum(const emlrtStack *sp, const emxArray_real_T *x_d, const
         emxArray_int32_T *x_colidx, const emxArray_int32_T *x_rowidx, int32_T
         x_m, int32_T x_n, f_sparse *y);

/* End of code generation (sum.h) */
