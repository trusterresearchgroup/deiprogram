/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * parenAssign2D.c
 *
 * Code generation for function 'parenAssign2D'
 *
 */

/* Include files */
#include "parenAssign2D.h"
#include "DEIPFunc3_data.h"
#include "DEIPFunc3_emxutil.h"
#include "DEIPFunc3_types.h"
#include "bigProduct.h"
#include "eml_int_forloop_overflow_check.h"
#include "locBsearch.h"
#include "rt_nonfinite.h"
#include "validateNumericIndex.h"
#include <stddef.h>
#include <string.h>

/* Variable Definitions */
static emlrtRSInfo pn_emlrtRSI = { 329,/* lineNo */
  "realloc",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign2D.m"/* pathName */
};

static emlrtRSInfo qn_emlrtRSI = { 337,/* lineNo */
  "realloc",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign2D.m"/* pathName */
};

static emlrtRSInfo rn_emlrtRSI = { 342,/* lineNo */
  "realloc",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign2D.m"/* pathName */
};

static emlrtRSInfo un_emlrtRSI = { 264,/* lineNo */
  "incrColIdx",                        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign2D.m"/* pathName */
};

static emlrtRSInfo vn_emlrtRSI = { 271,/* lineNo */
  "decrColIdx",                        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign2D.m"/* pathName */
};

static emlrtRTEInfo gm_emlrtRTEI = { 325,/* lineNo */
  1,                                   /* colNo */
  "parenAssign2D",                     /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign2D.m"/* pName */
};

static emlrtRTEInfo hm_emlrtRTEI = { 326,/* lineNo */
  1,                                   /* colNo */
  "parenAssign2D",                     /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign2D.m"/* pName */
};

static emlrtRTEInfo im_emlrtRTEI = { 333,/* lineNo */
  1,                                   /* colNo */
  "parenAssign2D",                     /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign2D.m"/* pName */
};

static emlrtRTEInfo jm_emlrtRTEI = { 334,/* lineNo */
  1,                                   /* colNo */
  "parenAssign2D",                     /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign2D.m"/* pName */
};

/* Function Definitions */
void b_realloc(const emlrtStack *sp, e_sparse *this, int32_T numAllocRequested,
               int32_T ub1, int32_T lb2, int32_T ub2)
{
  emlrtStack b_st;
  emlrtStack st;
  emxArray_int32_T *rowidxt;
  emxArray_real_T *dt;
  int32_T numAlloc;
  int32_T overflow;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  emxInit_int32_T(sp, &rowidxt, 1, &gm_emlrtRTEI, true);
  overflow = rowidxt->size[0];
  rowidxt->size[0] = this->rowidx->size[0];
  emxEnsureCapacity_int32_T(sp, rowidxt, overflow, &gm_emlrtRTEI);
  numAlloc = this->rowidx->size[0];
  for (overflow = 0; overflow < numAlloc; overflow++) {
    rowidxt->data[overflow] = this->rowidx->data[overflow];
  }

  emxInit_real_T(sp, &dt, 1, &hm_emlrtRTEI, true);
  overflow = dt->size[0];
  dt->size[0] = this->d->size[0];
  emxEnsureCapacity_real_T(sp, dt, overflow, &hm_emlrtRTEI);
  numAlloc = this->d->size[0];
  for (overflow = 0; overflow < numAlloc; overflow++) {
    dt->data[overflow] = this->d->data[overflow];
  }

  bigProduct(this->m, this->n, &numAlloc, &overflow);
  if (overflow == 0) {
    st.site = &pn_emlrtRSI;
    bigProduct(this->m, this->n, &numAlloc, &overflow);
    if (overflow != 0) {
      emlrtErrorWithMessageIdR2018a(&st, &oc_emlrtRTEI,
        "Coder:toolbox:SparseNumelTooBig", "Coder:toolbox:SparseNumelTooBig", 0);
    }

    numAlloc = this->m * this->n;
    if (numAllocRequested <= numAlloc) {
      numAlloc = numAllocRequested;
    }

    if (1 >= numAlloc) {
      numAlloc = 1;
    }
  } else if (1 >= numAllocRequested) {
    numAlloc = 1;
  } else {
    numAlloc = numAllocRequested;
  }

  overflow = this->rowidx->size[0];
  this->rowidx->size[0] = numAlloc;
  emxEnsureCapacity_int32_T(sp, this->rowidx, overflow, &im_emlrtRTEI);
  for (overflow = 0; overflow < numAlloc; overflow++) {
    this->rowidx->data[overflow] = 0;
  }

  overflow = this->d->size[0];
  this->d->size[0] = numAlloc;
  emxEnsureCapacity_real_T(sp, this->d, overflow, &jm_emlrtRTEI);
  for (overflow = 0; overflow < numAlloc; overflow++) {
    this->d->data[overflow] = 0.0;
  }

  this->maxnz = numAlloc;
  st.site = &qn_emlrtRSI;
  if ((1 <= ub1) && (ub1 > 2147483646)) {
    b_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  for (numAlloc = 0; numAlloc < ub1; numAlloc++) {
    this->rowidx->data[numAlloc] = rowidxt->data[numAlloc];
    this->d->data[numAlloc] = dt->data[numAlloc];
  }

  st.site = &rn_emlrtRSI;
  if ((lb2 <= ub2) && (ub2 > 2147483646)) {
    b_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  for (numAlloc = lb2; numAlloc <= ub2; numAlloc++) {
    this->rowidx->data[numAlloc] = rowidxt->data[numAlloc - 1];
    this->d->data[numAlloc] = dt->data[numAlloc - 1];
  }

  emxFree_real_T(&dt);
  emxFree_int32_T(&rowidxt);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

void decrColIdx(const emlrtStack *sp, e_sparse *this, int32_T col)
{
  emlrtStack b_st;
  emlrtStack st;
  int32_T a;
  int32_T b;
  int32_T k;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  a = col + 1;
  b = this->n + 1;
  st.site = &vn_emlrtRSI;
  if ((col + 1 <= this->n + 1) && (this->n + 1 > 2147483646)) {
    b_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  for (k = a; k <= b; k++) {
    this->colidx->data[k - 1]--;
  }
}

void incrColIdx(const emlrtStack *sp, e_sparse *this, int32_T col)
{
  emlrtStack b_st;
  emlrtStack st;
  int32_T a;
  int32_T b;
  int32_T k;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  a = col + 1;
  b = this->n + 1;
  st.site = &un_emlrtRSI;
  if ((col + 1 <= this->n + 1) && (this->n + 1 > 2147483646)) {
    b_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  for (k = a; k <= b; k++) {
    this->colidx->data[k - 1]++;
  }
}

void sparse_parenAssign2D(const emlrtStack *sp, e_sparse *this, real_T rhs,
  real_T r, real_T c)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack st;
  real_T thisv;
  int32_T nelem;
  int32_T vidx;
  boolean_T found;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &gn_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  b_st.site = &hn_emlrtRSI;
  b_sparse_validateNumericIndex(&b_st, this->m, r);
  b_st.site = &in_emlrtRSI;
  b_sparse_validateNumericIndex(&b_st, this->n, c);
  b_st.site = &jn_emlrtRSI;
  b_sparse_locBsearch(this->rowidx, (int32_T)r, this->colidx->data[(int32_T)c -
                      1], this->colidx->data[(int32_T)c], &vidx, &found);
  if (found) {
    thisv = this->d->data[vidx - 1];
  } else {
    thisv = 0.0;
  }

  if ((!(thisv == 0.0)) || (!(rhs == 0.0))) {
    if ((thisv != 0.0) && (rhs != 0.0)) {
      this->d->data[vidx - 1] = rhs;
    } else if (thisv == 0.0) {
      if (this->colidx->data[this->colidx->size[0] - 1] - 1 == this->maxnz) {
        c_st.site = &kn_emlrtRSI;
        b_realloc(&c_st, this, this->colidx->data[this->colidx->size[0] - 1] + 9,
                  vidx, vidx + 1, this->colidx->data[this->colidx->size[0] - 1]
                  - 1);
        this->rowidx->data[vidx] = (int32_T)r;
        this->d->data[vidx] = rhs;
      } else {
        nelem = (this->colidx->data[this->colidx->size[0] - 1] - vidx) - 1;
        if (nelem > 0) {
          memmove((void *)&this->rowidx->data[vidx + 1], (void *)&this->
                  rowidx->data[vidx], (uint32_T)((size_t)nelem * sizeof(int32_T)));
          memmove((void *)&this->d->data[vidx + 1], (void *)&this->d->data[vidx],
                  (uint32_T)((size_t)nelem * sizeof(real_T)));
        }

        this->d->data[vidx] = rhs;
        this->rowidx->data[vidx] = (int32_T)r;
      }

      c_st.site = &mn_emlrtRSI;
      incrColIdx(&c_st, this, (int32_T)c);
    } else {
      nelem = (this->colidx->data[this->colidx->size[0] - 1] - vidx) - 1;
      if (nelem > 0) {
        memmove((void *)&this->rowidx->data[vidx - 1], (void *)&this->
                rowidx->data[vidx], (uint32_T)((size_t)nelem * sizeof(int32_T)));
        memmove((void *)&this->d->data[vidx - 1], (void *)&this->d->data[vidx],
                (uint32_T)((size_t)nelem * sizeof(real_T)));
      }

      c_st.site = &on_emlrtRSI;
      decrColIdx(&c_st, this, (int32_T)c);
    }
  }
}

/* End of code generation (parenAssign2D.c) */
