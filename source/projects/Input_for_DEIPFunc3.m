% 06/20/2017 TJT
% A self periodic micrstructure containing 10 grains
% 3D Domain: 1.355528516514000 X 1.271597984617000 X 1.289175242983000 
% Loading: Prescribed macro strain of 0.01 
% Last revision: 11/13/2019 Sunday Aduloju


clear
% clc

load('10grains.mat')
nen = 4;
% read periodic file
Perfile = 'n10-id1.per';
fid = fopen(Perfile);
FormatString=repmat('%f',1,5);  % Create format string based on parameter
InputText=textscan(fid,FormatString); % Read data block
NeperPBC=cell2mat(InputText); % Convert to numerical array from cell
fclose(fid);
nummat=10;

% %% Harvest RegionOnElement data
% nummatS=nummat;
% RegionOnElementS= RegionOnElement;
% nummat=1;
% RegionOnElement= ones(numel,1);
% %%


% SEHist = 1;
% SHist = 1;

MateT = ones(nummat,1)*[100 .25 1];
MatTypeTable = [(1:nummat); ones(1,nummat)];
     
MPCList = NeperPBC;
% [~,inds] = unique(MPCList(:,2));
% MPCList = MPCList(inds,:);
numMPC = length(MPCList);

logic_x = abs(NeperPBC(:,3))==1;
logic_y = abs(NeperPBC(:,4))==1;
logic_z = abs(NeperPBC(:,5))==1;
logic_xy = (abs(NeperPBC(:,3))==1)&(abs(NeperPBC(:,4))==1);
logic_yz = (abs(NeperPBC(:,4))==1)&(abs(NeperPBC(:,5))==1);
logic_zx = (abs(NeperPBC(:,5))==1)&(abs(NeperPBC(:,3))==1);
logic_xyz = (abs(NeperPBC(:,3))==1)&(abs(NeperPBC(:,4))==1)&(abs(NeperPBC(:,5))==1);
% cut out the multi-direction links
logic_x = logic_x&~logic_xy&~logic_zx&~logic_xyz;
logic_y = logic_y&~logic_yz&~logic_xy&~logic_xyz;
logic_z = logic_z&~logic_yz&~logic_zx&~logic_xyz;
PBCListx = NeperPBC(logic_x,:);
PBCListy = NeperPBC(logic_y,:);
PBCListz = NeperPBC(logic_z,:);

% Get rigid body modes
% or even just look for a node with 2 links
FIX = intersect(PBCListx(:,2),intersect(PBCListy(:,2),PBCListz(:,2)));
FIX = FIX(1);
% indx = find(PBCListx(:,2)==FIX);
% RIGHT = PBCListx(indx,1);
% indy = find(PBCListy(:,2)==FIX);
% TOP = PBCListy(indy,1);
% indz = find(PBCListz(:,2)==FIX);
% FRONT = PBCListz(indz,1);


NodeBC = [FIX 1 0
          FIX 2 0
          FIX 3 0
%           FRONT 1 0
%           FRONT 2 0
%           FRONT 3 0
%           RIGHT 2 0
%           RIGHT 3 0
%           TOP 1 0
          ];
% numBC = size(NodeBC,1);





% Output quantity flags
DHist = 1;
FHist = 1;
SHist = 1;
SEHist = 1;


% Generate DG interface: pairs of elements and faces, duplicate the nodes,
% update the connectivities
numnpCG = numnp;
usePBC = 2; % flag to turn on keeping PBC
ndm = 3;
% InterTypes = []; % generate DG elements on surface of domain; empty pair-wise array interpreted
InterTypes = zeros(nummat,nummat); % generate DG elements on surface of domain
% InterTypes = tril(ones(nummat),-1);% generate DG elements on all grain boundaries

