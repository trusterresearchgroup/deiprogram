/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * find.c
 *
 * Code generation for function 'find'
 *
 */

/* Include files */
#include "find.h"
#include "DEIPFunc2_data.h"
#include "DEIPFunc2_emxutil.h"
#include "DEIPFunc2_types.h"
#include "eml_int_forloop_overflow_check.h"
#include "indexShapeCheck.h"
#include "rt_nonfinite.h"
#include "warning.h"

/* Variable Definitions */
static emlrtRSInfo vg_emlrtRSI = { 144,/* lineNo */
  "eml_find",                          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pathName */
};

static emlrtRSInfo wg_emlrtRSI = { 382,/* lineNo */
  "find_first_indices",                /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pathName */
};

static emlrtRSInfo xg_emlrtRSI = { 402,/* lineNo */
  "find_first_indices",                /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pathName */
};

static emlrtRSInfo fi_emlrtRSI = { 158,/* lineNo */
  "eml_find",                          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pathName */
};

static emlrtRSInfo lj_emlrtRSI = { 138,/* lineNo */
  "eml_find",                          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pathName */
};

static emlrtRSInfo mj_emlrtRSI = { 202,/* lineNo */
  "find_first_nonempty_triples",       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pathName */
};

static emlrtRSInfo nj_emlrtRSI = { 205,/* lineNo */
  "find_first_nonempty_triples",       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pathName */
};

static emlrtRSInfo oj_emlrtRSI = { 253,/* lineNo */
  "find_first_nonempty_triples",       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pathName */
};

static emlrtRSInfo pj_emlrtRSI = { 254,/* lineNo */
  "find_first_nonempty_triples",       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pathName */
};

static emlrtRSInfo qj_emlrtRSI = { 255,/* lineNo */
  "find_first_nonempty_triples",       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pathName */
};

static emlrtRTEInfo f_emlrtRTEI = { 392,/* lineNo */
  1,                                   /* colNo */
  "find_first_indices",                /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo q_emlrtRTEI = { 239,/* lineNo */
  1,                                   /* colNo */
  "find_first_nonempty_triples",       /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtDCInfo b_emlrtDCI = { 195, /* lineNo */
  30,                                  /* colNo */
  "find_first_nonempty_triples",       /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo c_emlrtDCI = { 369, /* lineNo */
  30,                                  /* colNo */
  "find_first_indices",                /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m",/* pName */
  4                                    /* checkKind */
};

static emlrtRTEInfo vc_emlrtRTEI = { 369,/* lineNo */
  24,                                  /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo wc_emlrtRTEI = { 144,/* lineNo */
  9,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo gd_emlrtRTEI = { 364,/* lineNo */
  24,                                  /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo nd_emlrtRTEI = { 195,/* lineNo */
  24,                                  /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo od_emlrtRTEI = { 197,/* lineNo */
  24,                                  /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo pd_emlrtRTEI = { 199,/* lineNo */
  24,                                  /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo qd_emlrtRTEI = { 253,/* lineNo */
  5,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo rd_emlrtRTEI = { 254,/* lineNo */
  5,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo sd_emlrtRTEI = { 45,/* lineNo */
  20,                                  /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

/* Function Definitions */
void b_eml_find(const emlrtStack *sp, const emxArray_real_T *x, emxArray_int32_T
                *i)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack st;
  int32_T iv[2];
  int32_T b_i;
  int32_T idx;
  int32_T ii;
  int32_T nx;
  boolean_T exitg1;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  nx = x->size[0] * x->size[1];
  st.site = &vg_emlrtRSI;
  idx = 0;
  b_i = i->size[0];
  i->size[0] = nx;
  emxEnsureCapacity_int32_T(&st, i, b_i, &vc_emlrtRTEI);
  b_st.site = &wg_emlrtRSI;
  if ((1 <= nx) && (nx > 2147483646)) {
    c_st.site = &ff_emlrtRSI;
    check_forloop_overflow_error(&c_st);
  }

  ii = 0;
  exitg1 = false;
  while ((!exitg1) && (ii <= nx - 1)) {
    if (x->data[ii] != 0.0) {
      idx++;
      i->data[idx - 1] = ii + 1;
      if (idx >= nx) {
        exitg1 = true;
      } else {
        ii++;
      }
    } else {
      ii++;
    }
  }

  if (idx > nx) {
    emlrtErrorWithMessageIdR2018a(&st, &f_emlrtRTEI,
      "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
  }

  if (nx == 1) {
    if (idx == 0) {
      i->size[0] = 0;
    }
  } else {
    if (1 > idx) {
      b_i = 0;
    } else {
      b_i = idx;
    }

    iv[0] = 1;
    iv[1] = b_i;
    b_st.site = &xg_emlrtRSI;
    indexShapeCheck(&b_st, i->size[0], iv);
    ii = i->size[0];
    i->size[0] = b_i;
    emxEnsureCapacity_int32_T(&st, i, ii, &wc_emlrtRTEI);
  }

  if ((x->size[0] == 1) && (i->size[0] != 1) && (x->size[1] >= 2)) {
    st.site = &fi_emlrtRSI;
    warning(&st);
  }
}

void c_eml_find(const emlrtStack *sp, const emxArray_real_T *x, emxArray_int32_T
                *i)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack st;
  int32_T iv[2];
  int32_T b_i;
  int32_T idx;
  int32_T ii;
  int32_T nx;
  boolean_T exitg1;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  nx = x->size[0];
  st.site = &vg_emlrtRSI;
  idx = 0;
  b_i = i->size[0];
  i->size[0] = x->size[0];
  emxEnsureCapacity_int32_T(&st, i, b_i, &vc_emlrtRTEI);
  b_st.site = &wg_emlrtRSI;
  if ((1 <= x->size[0]) && (x->size[0] > 2147483646)) {
    c_st.site = &ff_emlrtRSI;
    check_forloop_overflow_error(&c_st);
  }

  ii = 0;
  exitg1 = false;
  while ((!exitg1) && (ii <= nx - 1)) {
    if (x->data[ii] != 0.0) {
      idx++;
      i->data[idx - 1] = ii + 1;
      if (idx >= nx) {
        exitg1 = true;
      } else {
        ii++;
      }
    } else {
      ii++;
    }
  }

  if (idx > x->size[0]) {
    emlrtErrorWithMessageIdR2018a(&st, &f_emlrtRTEI,
      "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
  }

  if (x->size[0] == 1) {
    if (idx == 0) {
      i->size[0] = 0;
    }
  } else {
    if (1 > idx) {
      b_i = 0;
    } else {
      b_i = idx;
    }

    iv[0] = 1;
    iv[1] = b_i;
    b_st.site = &xg_emlrtRSI;
    indexShapeCheck(&b_st, i->size[0], iv);
    ii = i->size[0];
    i->size[0] = b_i;
    emxEnsureCapacity_int32_T(&st, i, ii, &wc_emlrtRTEI);
  }
}

void d_eml_find(const emlrtStack *sp, const emxArray_real_T *x, emxArray_int32_T
                *i)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack st;
  int32_T idx;
  int32_T ii;
  int32_T nx;
  boolean_T exitg1;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  nx = x->size[1];
  st.site = &vg_emlrtRSI;
  idx = 0;
  ii = i->size[0] * i->size[1];
  i->size[0] = 1;
  i->size[1] = x->size[1];
  emxEnsureCapacity_int32_T(&st, i, ii, &gd_emlrtRTEI);
  b_st.site = &wg_emlrtRSI;
  if ((1 <= x->size[1]) && (x->size[1] > 2147483646)) {
    c_st.site = &ff_emlrtRSI;
    check_forloop_overflow_error(&c_st);
  }

  ii = 0;
  exitg1 = false;
  while ((!exitg1) && (ii <= nx - 1)) {
    if (x->data[ii] != 0.0) {
      idx++;
      i->data[idx - 1] = ii + 1;
      if (idx >= nx) {
        exitg1 = true;
      } else {
        ii++;
      }
    } else {
      ii++;
    }
  }

  if (idx > x->size[1]) {
    emlrtErrorWithMessageIdR2018a(&st, &f_emlrtRTEI,
      "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
  }

  if (x->size[1] == 1) {
    if (idx == 0) {
      i->size[0] = 1;
      i->size[1] = 0;
    }
  } else {
    ii = i->size[0] * i->size[1];
    if (1 > idx) {
      i->size[1] = 0;
    } else {
      i->size[1] = idx;
    }

    emxEnsureCapacity_int32_T(&st, i, ii, &wc_emlrtRTEI);
  }
}

void e_eml_find(const emlrtStack *sp, const emxArray_real_T *x_d, const
                emxArray_int32_T *x_colidx, const emxArray_int32_T *x_rowidx,
                int32_T x_m, int32_T x_n, emxArray_int32_T *i, emxArray_int32_T *
                j)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack st;
  emxArray_real_T *v;
  int32_T iv[2];
  int32_T b_i;
  int32_T col;
  int32_T idx;
  int32_T nx;
  boolean_T overflow;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  nx = x_colidx->data[x_colidx->size[0] - 1] - 2;
  if (x_colidx->data[x_colidx->size[0] - 1] - 1 == 0) {
    i->size[0] = 0;
    j->size[0] = 0;
  } else {
    st.site = &lj_emlrtRSI;
    col = x_colidx->data[x_colidx->size[0] - 1] - 1;
    if (col < 0) {
      emlrtNonNegativeCheckR2012b(col, &b_emlrtDCI, &st);
    }

    emxInit_real_T(&st, &v, 1, &sd_emlrtRTEI, true);
    b_i = i->size[0];
    i->size[0] = col;
    emxEnsureCapacity_int32_T(&st, i, b_i, &nd_emlrtRTEI);
    col = j->size[0];
    j->size[0] = x_colidx->data[x_colidx->size[0] - 1] - 1;
    emxEnsureCapacity_int32_T(&st, j, col, &od_emlrtRTEI);
    col = v->size[0];
    v->size[0] = x_colidx->data[x_colidx->size[0] - 1] - 1;
    emxEnsureCapacity_real_T(&st, v, col, &pd_emlrtRTEI);
    b_st.site = &mj_emlrtRSI;
    if (1 > x_colidx->data[x_colidx->size[0] - 1] - 1) {
      overflow = false;
    } else {
      overflow = (x_colidx->data[x_colidx->size[0] - 1] - 1 > 2147483646);
    }

    if (overflow) {
      c_st.site = &ff_emlrtRSI;
      check_forloop_overflow_error(&c_st);
    }

    for (idx = 0; idx <= nx; idx++) {
      i->data[idx] = x_rowidx->data[idx];
    }

    b_st.site = &nj_emlrtRSI;
    if (1 > x_colidx->data[x_colidx->size[0] - 1] - 1) {
      overflow = false;
    } else {
      overflow = (x_colidx->data[x_colidx->size[0] - 1] - 1 > 2147483646);
    }

    if (overflow) {
      c_st.site = &ff_emlrtRSI;
      check_forloop_overflow_error(&c_st);
    }

    for (col = 0; col <= nx; col++) {
      v->data[col] = x_d->data[col];
    }

    idx = 0;
    col = 1;
    while (idx < nx + 1) {
      if (idx == x_colidx->data[col] - 1) {
        col++;
      } else {
        idx++;
        j->data[idx - 1] = col;
      }
    }

    if (idx > x_colidx->data[x_colidx->size[0] - 1] - 1) {
      emlrtErrorWithMessageIdR2018a(&st, &q_emlrtRTEI,
        "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
    }

    if (x_colidx->data[x_colidx->size[0] - 1] - 1 == 1) {
      if (idx == 0) {
        i->size[0] = 0;
        j->size[0] = 0;
      }
    } else {
      if (1 > idx) {
        col = 0;
      } else {
        col = idx;
      }

      iv[0] = 1;
      iv[1] = col;
      b_st.site = &oj_emlrtRSI;
      indexShapeCheck(&b_st, i->size[0], iv);
      b_i = i->size[0];
      i->size[0] = col;
      emxEnsureCapacity_int32_T(&st, i, b_i, &qd_emlrtRTEI);
      if (1 > idx) {
        col = 0;
      } else {
        col = idx;
      }

      iv[0] = 1;
      iv[1] = col;
      b_st.site = &pj_emlrtRSI;
      indexShapeCheck(&b_st, j->size[0], iv);
      b_i = j->size[0];
      j->size[0] = col;
      emxEnsureCapacity_int32_T(&st, j, b_i, &rd_emlrtRTEI);
      iv[0] = 1;
      if (1 > idx) {
        iv[1] = 0;
      } else {
        iv[1] = idx;
      }

      b_st.site = &qj_emlrtRSI;
      indexShapeCheck(&b_st, v->size[0], iv);
    }

    emxFree_real_T(&v);
  }

  if ((x_m == 1) && (i->size[0] != 1) && (x_n >= 2)) {
    st.site = &fi_emlrtRSI;
    warning(&st);
  }

  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

void eml_find(const emlrtStack *sp, const emxArray_boolean_T *x,
              emxArray_int32_T *i)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack st;
  int32_T iv[2];
  int32_T b_i;
  int32_T idx;
  int32_T ii;
  int32_T nx;
  boolean_T exitg1;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  nx = x->size[0];
  st.site = &vg_emlrtRSI;
  idx = 0;
  b_i = i->size[0];
  i->size[0] = x->size[0];
  emxEnsureCapacity_int32_T(&st, i, b_i, &vc_emlrtRTEI);
  b_st.site = &wg_emlrtRSI;
  if ((1 <= x->size[0]) && (x->size[0] > 2147483646)) {
    c_st.site = &ff_emlrtRSI;
    check_forloop_overflow_error(&c_st);
  }

  ii = 0;
  exitg1 = false;
  while ((!exitg1) && (ii <= nx - 1)) {
    if (x->data[ii]) {
      idx++;
      i->data[idx - 1] = ii + 1;
      if (idx >= nx) {
        exitg1 = true;
      } else {
        ii++;
      }
    } else {
      ii++;
    }
  }

  if (idx > x->size[0]) {
    emlrtErrorWithMessageIdR2018a(&st, &f_emlrtRTEI,
      "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
  }

  if (x->size[0] == 1) {
    if (idx == 0) {
      i->size[0] = 0;
    }
  } else {
    if (1 > idx) {
      b_i = 0;
    } else {
      b_i = idx;
    }

    iv[0] = 1;
    iv[1] = b_i;
    b_st.site = &xg_emlrtRSI;
    indexShapeCheck(&b_st, i->size[0], iv);
    ii = i->size[0];
    i->size[0] = b_i;
    emxEnsureCapacity_int32_T(&st, i, ii, &wc_emlrtRTEI);
  }
}

void f_eml_find(const emlrtStack *sp, const boolean_T x_data[], const int32_T
                x_size[2], int32_T i_data[], int32_T i_size[2])
{
  emlrtStack st;
  int32_T idx;
  int32_T ii;
  int32_T nx;
  boolean_T exitg1;
  st.prev = sp;
  st.tls = sp->tls;
  nx = x_size[1];
  st.site = &vg_emlrtRSI;
  idx = 0;
  i_size[0] = 1;
  i_size[1] = x_size[1];
  ii = 0;
  exitg1 = false;
  while ((!exitg1) && (ii <= nx - 1)) {
    if (x_data[ii]) {
      idx++;
      i_data[idx - 1] = ii + 1;
      if (idx >= nx) {
        exitg1 = true;
      } else {
        ii++;
      }
    } else {
      ii++;
    }
  }

  if (idx > x_size[1]) {
    emlrtErrorWithMessageIdR2018a(&st, &f_emlrtRTEI,
      "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
  }

  if (x_size[1] == 1) {
    if (idx == 0) {
      i_size[0] = 1;
      i_size[1] = 0;
    }
  } else if (1 > idx) {
    i_size[1] = 0;
  } else {
    i_size[1] = idx;
  }
}

void g_eml_find(const emlrtStack *sp, const emxArray_int32_T *x_colidx, const
                emxArray_int32_T *x_rowidx, int32_T x_m, emxArray_int32_T *i)
{
  emlrtStack b_st;
  emlrtStack st;
  int32_T iv[2];
  int32_T b_i;
  int32_T col;
  int32_T idx;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &vg_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  idx = 0;
  col = x_colidx->data[x_colidx->size[0] - 1] - 1;
  if (col < 0) {
    emlrtNonNegativeCheckR2012b(col, &c_emlrtDCI, &st);
  }

  b_i = i->size[0];
  i->size[0] = col;
  emxEnsureCapacity_int32_T(&st, i, b_i, &vc_emlrtRTEI);
  col = 1;
  while (idx < x_colidx->data[x_colidx->size[0] - 1] - 1) {
    if (idx == x_colidx->data[col] - 1) {
      col++;
    } else {
      idx++;
      i->data[idx - 1] = (col - 1) * x_m + x_rowidx->data[idx - 1];
    }
  }

  if (idx > x_colidx->data[x_colidx->size[0] - 1] - 1) {
    emlrtErrorWithMessageIdR2018a(&st, &f_emlrtRTEI,
      "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
  }

  if (x_colidx->data[x_colidx->size[0] - 1] - 1 == 1) {
    if (idx == 0) {
      i->size[0] = 0;
    }
  } else {
    if (1 > idx) {
      col = 0;
    } else {
      col = idx;
    }

    iv[0] = 1;
    iv[1] = col;
    b_st.site = &xg_emlrtRSI;
    indexShapeCheck(&b_st, i->size[0], iv);
    b_i = i->size[0];
    i->size[0] = col;
    emxEnsureCapacity_int32_T(&st, i, b_i, &wc_emlrtRTEI);
  }
}

/* End of code generation (find.c) */
