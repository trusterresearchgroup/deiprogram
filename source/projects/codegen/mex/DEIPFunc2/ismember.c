/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * ismember.c
 *
 * Code generation for function 'ismember'
 *
 */

/* Include files */
#include "ismember.h"
#include "DEIPFunc2_data.h"
#include "DEIPFunc2_emxutil.h"
#include "DEIPFunc2_types.h"
#include "eml_int_forloop_overflow_check.h"
#include "issorted.h"
#include "rt_nonfinite.h"
#include "sort.h"
#include "mwmathutil.h"
#include <math.h>

/* Variable Definitions */
static emlrtRSInfo of_emlrtRSI = { 152,/* lineNo */
  "local_ismember",                    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\ismember.m"/* pathName */
};

static emlrtRSInfo pf_emlrtRSI = { 151,/* lineNo */
  "local_ismember",                    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\ismember.m"/* pathName */
};

static emlrtRSInfo jm_emlrtRSI = { 150,/* lineNo */
  "local_ismember",                    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\ismember.m"/* pathName */
};

static emlrtRSInfo km_emlrtRSI = { 161,/* lineNo */
  "local_ismember",                    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\ismember.m"/* pathName */
};

static emlrtRSInfo lm_emlrtRSI = { 168,/* lineNo */
  "local_ismember",                    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\ismember.m"/* pathName */
};

static emlrtRSInfo mm_emlrtRSI = { 171,/* lineNo */
  "local_ismember",                    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\ismember.m"/* pathName */
};

static emlrtRSInfo nm_emlrtRSI = { 190,/* lineNo */
  "local_ismember",                    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\ismember.m"/* pathName */
};

static emlrtRTEInfo ce_emlrtRTEI = { 111,/* lineNo */
  5,                                   /* colNo */
  "ismember",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\ismember.m"/* pName */
};

static emlrtRTEInfo ee_emlrtRTEI = { 43,/* lineNo */
  21,                                  /* colNo */
  "ismember",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\ismember.m"/* pName */
};

/* Function Declarations */
static int32_T bsearchni(int32_T k, const emxArray_real_T *x, const
  emxArray_real_T *s);

/* Function Definitions */
static int32_T bsearchni(int32_T k, const emxArray_real_T *x, const
  emxArray_real_T *s)
{
  real_T b_x;
  int32_T idx;
  int32_T ihi;
  int32_T ilo;
  int32_T imid;
  boolean_T exitg1;
  boolean_T p;
  b_x = x->data[k - 1];
  ihi = s->size[0];
  idx = 0;
  ilo = 1;
  exitg1 = false;
  while ((!exitg1) && (ihi >= ilo)) {
    imid = ((ilo >> 1) + (ihi >> 1)) - 1;
    if (((ilo & 1) == 1) && ((ihi & 1) == 1)) {
      imid++;
    }

    if (b_x == s->data[imid]) {
      idx = imid + 1;
      exitg1 = true;
    } else {
      if (muDoubleScalarIsNaN(s->data[imid])) {
        p = !muDoubleScalarIsNaN(b_x);
      } else if (muDoubleScalarIsNaN(b_x)) {
        p = false;
      } else {
        p = (b_x < s->data[imid]);
      }

      if (p) {
        ihi = imid;
      } else {
        ilo = imid + 2;
      }
    }
  }

  if (idx > 0) {
    idx--;
    while ((idx > 0) && (b_x == s->data[idx - 1])) {
      idx--;
    }

    idx++;
  }

  return idx;
}

void b_local_ismember(const emlrtStack *sp, const emxArray_real_T *a, const
                      emxArray_real_T *s, emxArray_boolean_T *tf)
{
  emlrtStack b_st;
  emlrtStack st;
  emxArray_int32_T *kp_emlrtRSI;
  emxArray_real_T b_s;
  emxArray_real_T *ss;
  real_T absx;
  int32_T c_s[1];
  int32_T exponent;
  int32_T na;
  int32_T ns;
  int32_T p;
  int32_T pmax;
  int32_T pmin;
  int32_T pow2p;
  boolean_T b_p;
  boolean_T exitg1;
  boolean_T guard1 = false;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  na = a->size[0] - 1;
  ns = s->size[0];
  pmin = tf->size[0] * tf->size[1];
  tf->size[0] = a->size[0];
  tf->size[1] = 1;
  emxEnsureCapacity_boolean_T(sp, tf, pmin, &ce_emlrtRTEI);
  pmax = a->size[0];
  for (pmin = 0; pmin < pmax; pmin++) {
    tf->data[pmin] = false;
  }

  emxInit_real_T(sp, &ss, 1, &ee_emlrtRTEI, true);
  emxInit_int32_T(sp, &kp_emlrtRSI, 1, &ee_emlrtRTEI, true);
  guard1 = false;
  if (s->size[0] <= 4) {
    guard1 = true;
  } else {
    pmax = 31;
    pmin = 0;
    exitg1 = false;
    while ((!exitg1) && (pmax - pmin > 1)) {
      p = (pmin + pmax) >> 1;
      pow2p = 1 << p;
      if (pow2p == ns) {
        pmax = p;
        exitg1 = true;
      } else if (pow2p > ns) {
        pmax = p;
      } else {
        pmin = p;
      }
    }

    if (a->size[0] <= pmax + 4) {
      guard1 = true;
    } else {
      pmax = s->size[0];
      b_s = *s;
      c_s[0] = pmax;
      b_s.size = &c_s[0];
      b_s.numDimensions = 1;
      st.site = &km_emlrtRSI;
      if (!issorted(&st, &b_s)) {
        st.site = &lm_emlrtRSI;
        pmin = ss->size[0];
        ss->size[0] = s->size[0];
        emxEnsureCapacity_real_T(&st, ss, pmin, &de_emlrtRTEI);
        pmax = s->size[0];
        for (pmin = 0; pmin < pmax; pmin++) {
          ss->data[pmin] = s->data[pmin];
        }

        b_st.site = &tf_emlrtRSI;
        sort(&b_st, ss, kp_emlrtRSI);
        st.site = &mm_emlrtRSI;
        if ((1 <= a->size[0]) && (a->size[0] > 2147483646)) {
          b_st.site = &ff_emlrtRSI;
          check_forloop_overflow_error(&b_st);
        }

        for (pmax = 0; pmax <= na; pmax++) {
          if (bsearchni(pmax + 1, a, ss) > 0) {
            tf->data[pmax] = true;
          }
        }
      } else {
        st.site = &nm_emlrtRSI;
        if ((1 <= a->size[0]) && (a->size[0] > 2147483646)) {
          b_st.site = &ff_emlrtRSI;
          check_forloop_overflow_error(&b_st);
        }

        for (pmax = 0; pmax <= na; pmax++) {
          if (bsearchni(pmax + 1, a, s) > 0) {
            tf->data[pmax] = true;
          }
        }
      }
    }
  }

  if (guard1) {
    st.site = &jm_emlrtRSI;
    if ((1 <= a->size[0]) && (a->size[0] > 2147483646)) {
      b_st.site = &ff_emlrtRSI;
      check_forloop_overflow_error(&b_st);
    }

    for (pmin = 0; pmin <= na; pmin++) {
      st.site = &pf_emlrtRSI;
      if ((1 <= ns) && (ns > 2147483646)) {
        b_st.site = &ff_emlrtRSI;
        check_forloop_overflow_error(&b_st);
      }

      pmax = 0;
      exitg1 = false;
      while ((!exitg1) && (pmax <= ns - 1)) {
        st.site = &of_emlrtRSI;
        absx = muDoubleScalarAbs(s->data[pmax] / 2.0);
        if ((!muDoubleScalarIsInf(absx)) && (!muDoubleScalarIsNaN(absx))) {
          if (absx <= 2.2250738585072014E-308) {
            absx = 4.94065645841247E-324;
          } else {
            frexp(absx, &exponent);
            absx = ldexp(1.0, exponent - 53);
          }
        } else {
          absx = rtNaN;
        }

        if ((muDoubleScalarAbs(s->data[pmax] - a->data[pmin]) < absx) ||
            (muDoubleScalarIsInf(a->data[pmin]) && muDoubleScalarIsInf(s->
              data[pmax]) && ((a->data[pmin] > 0.0) == (s->data[pmax] > 0.0))))
        {
          b_p = true;
        } else {
          b_p = false;
        }

        if (b_p) {
          tf->data[pmin] = true;
          exitg1 = true;
        } else {
          pmax++;
        }
      }
    }
  }

  emxFree_int32_T(&kp_emlrtRSI);
  emxFree_real_T(&ss);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

boolean_T local_ismember(real_T a, const real_T s_data[], const int32_T s_size[1])
{
  real_T absx;
  int32_T exponent;
  int32_T k;
  boolean_T exitg1;
  boolean_T p;
  boolean_T tf;
  tf = false;
  k = 0;
  exitg1 = false;
  while ((!exitg1) && (k <= s_size[0] - 1)) {
    absx = muDoubleScalarAbs(s_data[k] / 2.0);
    if ((!muDoubleScalarIsInf(absx)) && (!muDoubleScalarIsNaN(absx))) {
      if (absx <= 2.2250738585072014E-308) {
        absx = 4.94065645841247E-324;
      } else {
        frexp(absx, &exponent);
        absx = ldexp(1.0, exponent - 53);
      }
    } else {
      absx = rtNaN;
    }

    if ((muDoubleScalarAbs(s_data[k] - a) < absx) || (muDoubleScalarIsInf(a) &&
         muDoubleScalarIsInf(s_data[k]) && ((a > 0.0) == (s_data[k] > 0.0)))) {
      p = true;
    } else {
      p = false;
    }

    if (p) {
      tf = true;
      exitg1 = true;
    } else {
      k++;
    }
  }

  return tf;
}

/* End of code generation (ismember.c) */
