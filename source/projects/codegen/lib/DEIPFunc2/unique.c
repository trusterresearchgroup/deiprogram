/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * unique.c
 *
 * Code generation for function 'unique'
 *
 */

/* Include files */
#include "unique.h"
#include "DEIPFunc2_emxutil.h"
#include "DEIPFunc2_types.h"
#include "rt_nonfinite.h"
#include "sortLE.h"
#include "rt_nonfinite.h"
#include <math.h>

/* Function Definitions */
void unique_rows(const emxArray_real_T *a, emxArray_real_T *b, emxArray_int32_T *
                 ndx)
{
  emxArray_int32_T *idx;
  emxArray_int32_T *iwork;
  emxArray_real_T *b_b;
  emxArray_real_T *ycol;
  double absx;
  double b_a;
  double c_b;
  int b_i;
  int exitg1;
  int exponent;
  int i;
  int i1;
  int j;
  int k;
  int k0;
  int kEnd;
  int m;
  int n;
  int nb;
  int q;
  int qEnd;
  boolean_T exitg2;
  boolean_T p;
  if (a->size[0] == 0) {
    i = b->size[0] * b->size[1];
    b->size[0] = a->size[0];
    b->size[1] = 2;
    emxEnsureCapacity_real_T(b, i);
    m = a->size[0] * a->size[1];
    for (i = 0; i < m; i++) {
      b->data[i] = a->data[i];
    }

    ndx->size[0] = 0;
  } else {
    i = b->size[0] * b->size[1];
    b->size[0] = a->size[0];
    b->size[1] = 2;
    emxEnsureCapacity_real_T(b, i);
    m = a->size[0] * a->size[1];
    for (i = 0; i < m; i++) {
      b->data[i] = a->data[i];
    }

    emxInit_int32_T(&idx, 1);
    n = a->size[0] + 1;
    i = idx->size[0];
    idx->size[0] = a->size[0];
    emxEnsureCapacity_int32_T(idx, i);
    m = a->size[0];
    for (i = 0; i < m; i++) {
      idx->data[i] = 0;
    }

    emxInit_int32_T(&iwork, 1);
    i = iwork->size[0];
    iwork->size[0] = a->size[0];
    emxEnsureCapacity_int32_T(iwork, i);
    i = a->size[0] - 1;
    for (k = 1; k <= i; k += 2) {
      if (sortLE(a, k, k + 1)) {
        idx->data[k - 1] = k;
        idx->data[k] = k + 1;
      } else {
        idx->data[k - 1] = k + 1;
        idx->data[k] = k;
      }
    }

    if ((a->size[0] & 1) != 0) {
      idx->data[a->size[0] - 1] = a->size[0];
    }

    b_i = 2;
    while (b_i < n - 1) {
      m = b_i << 1;
      j = 1;
      for (k0 = b_i + 1; k0 < n; k0 = qEnd + b_i) {
        nb = j;
        q = k0;
        qEnd = j + m;
        if (qEnd > n) {
          qEnd = n;
        }

        k = 0;
        kEnd = qEnd - j;
        while (k + 1 <= kEnd) {
          i = idx->data[q - 1];
          i1 = idx->data[nb - 1];
          if (sortLE(a, i1, i)) {
            iwork->data[k] = i1;
            nb++;
            if (nb == k0) {
              while (q < qEnd) {
                k++;
                iwork->data[k] = idx->data[q - 1];
                q++;
              }
            }
          } else {
            iwork->data[k] = i;
            q++;
            if (q == qEnd) {
              while (nb < k0) {
                k++;
                iwork->data[k] = idx->data[nb - 1];
                nb++;
              }
            }
          }

          k++;
        }

        for (k = 0; k < kEnd; k++) {
          idx->data[(j + k) - 1] = iwork->data[k];
        }

        j = qEnd;
      }

      b_i = m;
    }

    emxFree_int32_T(&iwork);
    emxInit_real_T(&ycol, 1);
    m = a->size[0] - 1;
    i = ycol->size[0];
    ycol->size[0] = a->size[0];
    emxEnsureCapacity_real_T(ycol, i);
    for (b_i = 0; b_i <= m; b_i++) {
      ycol->data[b_i] = b->data[idx->data[b_i] - 1];
    }

    for (b_i = 0; b_i <= m; b_i++) {
      b->data[b_i] = ycol->data[b_i];
    }

    for (b_i = 0; b_i <= m; b_i++) {
      ycol->data[b_i] = b->data[(idx->data[b_i] + b->size[0]) - 1];
    }

    for (b_i = 0; b_i <= m; b_i++) {
      b->data[b_i + b->size[0]] = ycol->data[b_i];
    }

    i = ycol->size[0];
    ycol->size[0] = idx->size[0];
    emxEnsureCapacity_real_T(ycol, i);
    m = idx->size[0];
    for (i = 0; i < m; i++) {
      ycol->data[i] = idx->data[i];
    }

    emxFree_int32_T(&idx);
    nb = -1;
    m = a->size[0];
    k = 0;
    while (k + 1 <= m) {
      k0 = k;
      do {
        exitg1 = 0;
        k++;
        if (k + 1 > m) {
          exitg1 = 1;
        } else {
          p = false;
          j = 0;
          exitg2 = false;
          while ((!exitg2) && (j < 2)) {
            b_a = b->data[k0 + b->size[0] * j];
            c_b = b->data[k + b->size[0] * j];
            absx = fabs(c_b / 2.0);
            if ((!rtIsInf(absx)) && (!rtIsNaN(absx))) {
              if (absx <= 2.2250738585072014E-308) {
                absx = 4.94065645841247E-324;
              } else {
                frexp(absx, &exponent);
                absx = ldexp(1.0, exponent - 53);
              }
            } else {
              absx = rtNaN;
            }

            if ((fabs(c_b - b_a) < absx) || (rtIsInf(b_a) && rtIsInf(c_b) &&
                 ((b_a > 0.0) == (c_b > 0.0)))) {
              j++;
            } else {
              p = true;
              exitg2 = true;
            }
          }

          if (p) {
            exitg1 = 1;
          }
        }
      } while (exitg1 == 0);

      nb++;
      b->data[nb] = b->data[k0];
      b->data[nb + b->size[0]] = b->data[k0 + b->size[0]];
      ycol->data[nb] = ycol->data[k0];
    }

    emxInit_real_T(&b_b, 2);
    if (1 > nb + 1) {
      m = -1;
    } else {
      m = nb;
    }

    i = b_b->size[0] * b_b->size[1];
    b_b->size[0] = m + 1;
    b_b->size[1] = 2;
    emxEnsureCapacity_real_T(b_b, i);
    for (i = 0; i <= m; i++) {
      b_b->data[i] = b->data[i];
    }

    for (i = 0; i <= m; i++) {
      b_b->data[i + b_b->size[0]] = b->data[i + b->size[0]];
    }

    i = b->size[0] * b->size[1];
    b->size[0] = b_b->size[0];
    b->size[1] = 2;
    emxEnsureCapacity_real_T(b, i);
    m = b_b->size[0] * b_b->size[1];
    for (i = 0; i < m; i++) {
      b->data[i] = b_b->data[i];
    }

    emxFree_real_T(&b_b);
    i = ndx->size[0];
    ndx->size[0] = nb + 1;
    emxEnsureCapacity_int32_T(ndx, i);
    for (k = 0; k <= nb; k++) {
      ndx->data[k] = (int)ycol->data[k];
    }

    emxFree_real_T(&ycol);
  }
}

void unique_vector(const emxArray_real_T *a, emxArray_real_T *b)
{
  emxArray_int32_T *idx;
  emxArray_int32_T *iwork;
  double absx;
  double x;
  int b_i;
  int exitg2;
  int exponent;
  int i;
  int i2;
  int j;
  int k;
  int kEnd;
  int n;
  int na;
  int p;
  int pEnd;
  int q;
  int qEnd;
  boolean_T exitg1;
  emxInit_int32_T(&idx, 1);
  na = a->size[0];
  n = a->size[0] + 1;
  i = idx->size[0];
  idx->size[0] = a->size[0];
  emxEnsureCapacity_int32_T(idx, i);
  b_i = a->size[0];
  for (i = 0; i < b_i; i++) {
    idx->data[i] = 0;
  }

  if (a->size[0] != 0) {
    emxInit_int32_T(&iwork, 1);
    i = iwork->size[0];
    iwork->size[0] = a->size[0];
    emxEnsureCapacity_int32_T(iwork, i);
    i = a->size[0] - 1;
    for (k = 1; k <= i; k += 2) {
      if ((a->data[k - 1] <= a->data[k]) || rtIsNaN(a->data[k])) {
        idx->data[k - 1] = k;
        idx->data[k] = k + 1;
      } else {
        idx->data[k - 1] = k + 1;
        idx->data[k] = k;
      }
    }

    if ((a->size[0] & 1) != 0) {
      idx->data[a->size[0] - 1] = a->size[0];
    }

    b_i = 2;
    while (b_i < n - 1) {
      i2 = b_i << 1;
      j = 1;
      for (pEnd = b_i + 1; pEnd < n; pEnd = qEnd + b_i) {
        p = j;
        q = pEnd - 1;
        qEnd = j + i2;
        if (qEnd > n) {
          qEnd = n;
        }

        k = 0;
        kEnd = qEnd - j;
        while (k + 1 <= kEnd) {
          absx = a->data[idx->data[q] - 1];
          i = idx->data[p - 1];
          if ((a->data[i - 1] <= absx) || rtIsNaN(absx)) {
            iwork->data[k] = i;
            p++;
            if (p == pEnd) {
              while (q + 1 < qEnd) {
                k++;
                iwork->data[k] = idx->data[q];
                q++;
              }
            }
          } else {
            iwork->data[k] = idx->data[q];
            q++;
            if (q + 1 == qEnd) {
              while (p < pEnd) {
                k++;
                iwork->data[k] = idx->data[p - 1];
                p++;
              }
            }
          }

          k++;
        }

        for (k = 0; k < kEnd; k++) {
          idx->data[(j + k) - 1] = iwork->data[k];
        }

        j = qEnd;
      }

      b_i = i2;
    }

    emxFree_int32_T(&iwork);
  }

  i = b->size[0];
  b->size[0] = a->size[0];
  emxEnsureCapacity_real_T(b, i);
  for (k = 0; k < na; k++) {
    b->data[k] = a->data[idx->data[k] - 1];
  }

  emxFree_int32_T(&idx);
  k = 0;
  while ((k + 1 <= na) && rtIsInf(b->data[k]) && (b->data[k] < 0.0)) {
    k++;
  }

  i2 = k;
  k = a->size[0];
  while ((k >= 1) && rtIsNaN(b->data[k - 1])) {
    k--;
  }

  pEnd = a->size[0] - k;
  exitg1 = false;
  while ((!exitg1) && (k >= 1)) {
    absx = b->data[k - 1];
    if (rtIsInf(absx) && (absx > 0.0)) {
      k--;
    } else {
      exitg1 = true;
    }
  }

  b_i = (a->size[0] - k) - pEnd;
  p = -1;
  if (i2 > 0) {
    p = 0;
  }

  while (i2 + 1 <= k) {
    x = b->data[i2];
    do {
      exitg2 = 0;
      i2++;
      if (i2 + 1 > k) {
        exitg2 = 1;
      } else {
        absx = fabs(x / 2.0);
        if ((!rtIsInf(absx)) && (!rtIsNaN(absx))) {
          if (absx <= 2.2250738585072014E-308) {
            absx = 4.94065645841247E-324;
          } else {
            frexp(absx, &exponent);
            absx = ldexp(1.0, exponent - 53);
          }
        } else {
          absx = rtNaN;
        }

        if ((!(fabs(x - b->data[i2]) < absx)) && ((!rtIsInf(b->data[i2])) ||
             (!rtIsInf(x)) || ((b->data[i2] > 0.0) != (x > 0.0)))) {
          exitg2 = 1;
        }
      }
    } while (exitg2 == 0);

    p++;
    b->data[p] = x;
  }

  if (b_i > 0) {
    p++;
    b->data[p] = b->data[k];
  }

  i2 = k + b_i;
  for (j = 0; j < pEnd; j++) {
    p++;
    b->data[p] = b->data[i2 + j];
  }

  i = b->size[0];
  if (1 > p + 1) {
    b->size[0] = 0;
  } else {
    b->size[0] = p + 1;
  }

  emxEnsureCapacity_real_T(b, i);
}

/* End of code generation (unique.c) */
