/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sparse.h
 *
 * Code generation for function 'sparse'
 *
 */

#pragma once

/* Include files */
#include "DEIPFunc2_types.h"
#include "rtwtypes.h"
#include "emlrt.h"
#include "mex.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Function Declarations */
void b_sparse(const emlrtStack *sp, const emxArray_real_T *varargin_1, const
              emxArray_real_T *varargin_2, const emxArray_real_T *varargin_3,
              e_sparse *y);
void c_sparse(const emlrtStack *sp, const emxArray_real_T *varargin_1,
              emxArray_real_T *y_d, emxArray_int32_T *y_colidx, emxArray_int32_T
              *y_rowidx, int32_T *y_m, int32_T *y_n, int32_T *y_maxnz);
void d_sparse(const emlrtStack *sp, boolean_T varargin_1, emxArray_boolean_T
              *y_d, emxArray_int32_T *y_colidx, emxArray_int32_T *y_rowidx);
void sparse(const emlrtStack *sp, const emxArray_real_T *varargin_1, const
            emxArray_real_T *varargin_2, const emxArray_real_T *varargin_3,
            e_sparse *y);

/* End of code generation (sparse.h) */
