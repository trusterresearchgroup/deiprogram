/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc2_data.h
 *
 * Code generation for function 'DEIPFunc2_data'
 *
 */

#ifndef DEIPFUNC2_DATA_H
#define DEIPFUNC2_DATA_H

/* Include files */
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>

/* Variable Declarations */
extern boolean_T isInitialized_DEIPFunc2;

#endif

/* End of code generation (DEIPFunc2_data.h) */
