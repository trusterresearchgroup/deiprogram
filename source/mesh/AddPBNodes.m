function [Coordinates,NodesOnElement,RegionOnElement,MatTypeTable,MateT,numnp,numel,nummat,nen,NodeTypeNum] = AddPBNodes(MPCList,CornerXYZ,Coordinates,NodesOnElement,RegionOnElement,MatTypeTable,MateT,numnp,numel,nummat,nen)
%
% Tim Truster
% 12/07/2018
%
% Insert the Lagrange Multiplier nodes into a mesh for periodic BC modeling
%
% PBCmat = material type ID for PBC elements
% MPCList = list of P BC, which is expressed in the order that Abaqus
% usually gives:
% [u_-,1, -1.0, u_+,1, 1.0, u_master_+,1, 1.0, u_master_-,1, -1.0]
% [u_-,2, -1.0, u_+,2, 1.0, u_master_+,2, 1.0, u_master_-,2, -1.0] 
% [u_-,3, -1.0, u_+,3, 1.0, u_master_+,3, 1.0, u_master_-,3, -1.0]
% Simplified and reordered to: 
% [u_+, u_-, u_master_+, u_master_-]

PBCmat = nummat + 1;
ndm = size(Coordinates,2);
numMPC = size(MPCList,1);

% Node number for each group of nodes: solid, masterPBC, and Lagrange
% multipliers
NodeTypeNum = [1 numnp+1 numnp+ndm+1 numnp+ndm+numMPC+1]';

% expand materials
nummat = nummat + 1;
MatTypeTable = [MatTypeTable [nummat; 24; zeros(size(MatTypeTable,1)-2,1)]];
MateT = [MateT; zeros(1,size(MateT,2))];

% expand elements
nenPBC = 3+ndm;
if nen < nenPBC
    NodesOnElement = [NodesOnElement zeros(numel,nenPBC-nen)];
    nen = nenPBC;
end
RegionOnElement = [RegionOnElement; PBCmat*ones(numMPC,1)];
MPCnodes = (numnp+1+ndm:numnp+numMPC+ndm)';
Corners = numnp+1:numnp+ndm;
NodesOnElement = [NodesOnElement; [MPCList(:,1:2) ones(numMPC,1)*Corners MPCnodes zeros(numMPC,nen-nenPBC)]];
numel = numel + numMPC;

% expand nodes
numnp = numnp + numMPC + ndm;
Coordinates = [Coordinates; CornerXYZ; zeros(numMPC,ndm)];

