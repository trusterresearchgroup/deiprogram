/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc2_terminate.h
 *
 * Code generation for function 'DEIPFunc2_terminate'
 *
 */

#ifndef DEIPFUNC2_TERMINATE_H
#define DEIPFUNC2_TERMINATE_H

/* Include files */
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>
#ifdef __cplusplus

extern "C" {

#endif

  /* Function Declarations */
  extern void DEIPFunc2_terminate(void);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (DEIPFunc2_terminate.h) */
