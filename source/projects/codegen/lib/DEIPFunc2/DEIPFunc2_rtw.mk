###########################################################################
## Makefile generated for component 'DEIPFunc2'. 
## 
## Makefile     : DEIPFunc2_rtw.mk
## Generated on : Mon Feb 07 16:30:17 2022
## Final product: ./DEIPFunc2.lib
## Product type : static-library
## 
###########################################################################

###########################################################################
## MACROS
###########################################################################

# Macro Descriptions:
# PRODUCT_NAME            Name of the system to build
# MAKEFILE                Name of this makefile
# CMD_FILE                Command file
# MODELLIB                Static library target

PRODUCT_NAME              = DEIPFunc2
MAKEFILE                  = DEIPFunc2_rtw.mk
MATLAB_ROOT               = C:/PROGRA~1/MATLAB/R2020b
MATLAB_BIN                = C:/PROGRA~1/MATLAB/R2020b/bin
MATLAB_ARCH_BIN           = $(MATLAB_BIN)/win64
START_DIR                 = C:/Users/ttruster/_Prog_Repo/DEIP/DEIProgram_rel/source/projects/codegen/lib/DEIPFunc2
TGT_FCN_LIB               = None
SOLVER_OBJ                = 
CLASSIC_INTERFACE         = 0
MODEL_HAS_DYNAMICALLY_LOADED_SFCNS = 
RELATIVE_PATH_TO_ANCHOR   = .
CMD_FILE                  = DEIPFunc2_rtw.rsp
C_STANDARD_OPTS           = 
CPP_STANDARD_OPTS         = 
MODELLIB                  = DEIPFunc2.lib

###########################################################################
## TOOLCHAIN SPECIFICATIONS
###########################################################################

# Toolchain Name:          LCC-win64 v2.4.1 | gmake (64-bit Windows)
# Supported Version(s):    2.4.1
# ToolchainInfo Version:   2020b
# Specification Revision:  1.0
# 

#-----------
# MACROS
#-----------

SHELL              = cmd
LCC_ROOT           = $(MATLAB_ROOT)/sys/lcc64/lcc64
LCC_BUILDLIB       = $(LCC_ROOT)/bin/buildlib
LCC_LIB            = $(LCC_ROOT)/lib64
MW_EXTERNLIB_DIR   = $(MATLAB_ROOT)/extern/lib/win64/microsoft
MW_LIB_DIR         = $(MATLAB_ROOT)/lib/win64
TOOLCHAIN_INCLUDES = -I$(LCC_ROOT)/include64

TOOLCHAIN_SRCS = 
TOOLCHAIN_INCS = 
TOOLCHAIN_LIBS = 

#------------------------
# BUILD TOOL COMMANDS
#------------------------

# C Compiler: Lcc-win64 C Compiler
CC_PATH = $(LCC_ROOT)/bin
CC = "$(CC_PATH)/lcc64"

# Linker: Lcc-win64 Linker
LD_PATH = $(LCC_ROOT)/bin
LD = "$(LD_PATH)/lcclnk64"

# Archiver: Lcc-win64 Archiver
AR_PATH = $(LCC_ROOT)/bin
AR = "$(AR_PATH)/lcclib64"

# MEX Tool: MEX Tool
MEX_PATH = $(MATLAB_ARCH_BIN)
MEX = "$(MEX_PATH)/mex"

# Download: Download
DOWNLOAD =

# Execute: Execute
EXECUTE = $(PRODUCT)

# Builder: GMAKE Utility
MAKE_PATH = %MATLAB%\bin\win64
MAKE = "$(MAKE_PATH)/gmake"


#-------------------------
# Directives/Utilities
#-------------------------

CDEBUG              = -g
C_OUTPUT_FLAG       = -Fo
LDDEBUG             =
OUTPUT_FLAG         = -o
ARDEBUG             =
STATICLIB_OUTPUT_FLAG = /out:
MEX_DEBUG           = -g
RM                  = @del /F
ECHO                = @echo
MV                  = @move
RUN                 =

#--------------------------------------
# "Faster Runs" Build Configuration
#--------------------------------------

ARFLAGS              =
CFLAGS               = -c -w -noregistrylookup -nodeclspec -I$(LCC_ROOT)/include64
DOWNLOAD_FLAGS       =
EXECUTE_FLAGS        =
LDFLAGS              = -s -L$(LCC_LIB) $(LDFLAGS_ADDITIONAL)
MEX_CPPFLAGS         =
MEX_CPPLDFLAGS       =
MEX_CFLAGS           =
MEX_LDFLAGS          =
MAKE_FLAGS           = -f $(MAKEFILE)
SHAREDLIB_LDFLAGS    = -dll -entry LibMain -s -L$(LCC_LIB) $(LDFLAGS_ADDITIONAL) $(DEF_FILE)



###########################################################################
## OUTPUT INFO
###########################################################################

PRODUCT = ./DEIPFunc2.lib
PRODUCT_TYPE = "static-library"
BUILD_TYPE = "Static Library"

###########################################################################
## INCLUDE PATHS
###########################################################################

INCLUDES_BUILDINFO = -I$(START_DIR) -IC:/Users/ttruster/_PROG_~1/DEIP/DE0AFB~1/source/projects -I$(MATLAB_ROOT)/extern/include

INCLUDES = $(INCLUDES_BUILDINFO)

###########################################################################
## DEFINES
###########################################################################

DEFINES_CUSTOM = 
DEFINES_STANDARD = -DMODEL=DEIPFunc2

DEFINES = $(DEFINES_CUSTOM) $(DEFINES_STANDARD)

###########################################################################
## SOURCE FILES
###########################################################################

SRCS = $(START_DIR)/DEIPFunc2_data.c $(START_DIR)/rt_nonfinite.c $(START_DIR)/rtGetNaN.c $(START_DIR)/rtGetInf.c $(START_DIR)/DEIPFunc2_initialize.c $(START_DIR)/DEIPFunc2_terminate.c $(START_DIR)/unique.c $(START_DIR)/sortLE.c $(START_DIR)/ismember.c $(START_DIR)/find.c $(START_DIR)/eml_setop.c $(START_DIR)/nnz.c $(START_DIR)/minOrMax.c $(START_DIR)/sparse.c $(START_DIR)/sparse1.c $(START_DIR)/locBsearch.c $(START_DIR)/isequal.c $(START_DIR)/colon.c $(START_DIR)/sum.c $(START_DIR)/not.c $(START_DIR)/any.c $(START_DIR)/ifWhileCond.c $(START_DIR)/diag.c $(START_DIR)/norm.c $(START_DIR)/DEIPFunc2.c $(START_DIR)/sort.c $(START_DIR)/sortIdx.c $(START_DIR)/insertionsort.c $(START_DIR)/introsort.c $(START_DIR)/heapsort.c $(START_DIR)/fillIn.c $(START_DIR)/sign.c $(START_DIR)/parenAssign2D.c $(START_DIR)/DEIPFunc2_emxutil.c $(START_DIR)/DEIPFunc2_emxAPI.c

ALL_SRCS = $(SRCS)

###########################################################################
## OBJECTS
###########################################################################

OBJS = DEIPFunc2_data.obj rt_nonfinite.obj rtGetNaN.obj rtGetInf.obj DEIPFunc2_initialize.obj DEIPFunc2_terminate.obj unique.obj sortLE.obj ismember.obj find.obj eml_setop.obj nnz.obj minOrMax.obj sparse.obj sparse1.obj locBsearch.obj isequal.obj colon.obj sum.obj not.obj any.obj ifWhileCond.obj diag.obj norm.obj DEIPFunc2.obj sort.obj sortIdx.obj insertionsort.obj introsort.obj heapsort.obj fillIn.obj sign.obj parenAssign2D.obj DEIPFunc2_emxutil.obj DEIPFunc2_emxAPI.obj

ALL_OBJS = $(OBJS)

###########################################################################
## PREBUILT OBJECT FILES
###########################################################################

PREBUILT_OBJS = 

###########################################################################
## LIBRARIES
###########################################################################

LIBS = 

###########################################################################
## SYSTEM LIBRARIES
###########################################################################

SYSTEM_LIBS = 

###########################################################################
## ADDITIONAL TOOLCHAIN FLAGS
###########################################################################

#---------------
# C Compiler
#---------------

CFLAGS_BASIC = $(DEFINES) $(INCLUDES)

CFLAGS += $(CFLAGS_BASIC)

###########################################################################
## INLINED COMMANDS
###########################################################################

###########################################################################
## PHONY TARGETS
###########################################################################

.PHONY : all build clean info prebuild download execute


all : build
	@echo "### Successfully generated all binary outputs."


build : prebuild $(PRODUCT)


prebuild : 


download : $(PRODUCT)


execute : download


###########################################################################
## FINAL TARGET
###########################################################################

#---------------------------------
# Create a static library         
#---------------------------------

$(PRODUCT) : $(OBJS) $(PREBUILT_OBJS)
	@echo "### Creating static library "$(PRODUCT)" ..."
	$(AR) $(ARFLAGS) /out:$(PRODUCT) @$(CMD_FILE)
	@echo "### Created: $(PRODUCT)"


###########################################################################
## INTERMEDIATE TARGETS
###########################################################################

#---------------------
# SOURCE-TO-OBJECT
#---------------------

%.obj : %.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


%.obj : $(RELATIVE_PATH_TO_ANCHOR)/%.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


%.obj : $(START_DIR)/%.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


%.obj : C:/Users/ttruster/_Prog_Repo/DEIP/DEIProgram_rel/source/projects/%.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


DEIPFunc2_data.obj : $(START_DIR)/DEIPFunc2_data.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


rt_nonfinite.obj : $(START_DIR)/rt_nonfinite.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


rtGetNaN.obj : $(START_DIR)/rtGetNaN.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


rtGetInf.obj : $(START_DIR)/rtGetInf.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


DEIPFunc2_initialize.obj : $(START_DIR)/DEIPFunc2_initialize.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


DEIPFunc2_terminate.obj : $(START_DIR)/DEIPFunc2_terminate.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


unique.obj : $(START_DIR)/unique.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


sortLE.obj : $(START_DIR)/sortLE.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


ismember.obj : $(START_DIR)/ismember.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


find.obj : $(START_DIR)/find.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


eml_setop.obj : $(START_DIR)/eml_setop.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


nnz.obj : $(START_DIR)/nnz.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


minOrMax.obj : $(START_DIR)/minOrMax.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


sparse.obj : $(START_DIR)/sparse.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


sparse1.obj : $(START_DIR)/sparse1.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


locBsearch.obj : $(START_DIR)/locBsearch.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


isequal.obj : $(START_DIR)/isequal.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


colon.obj : $(START_DIR)/colon.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


sum.obj : $(START_DIR)/sum.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


not.obj : $(START_DIR)/not.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


any.obj : $(START_DIR)/any.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


ifWhileCond.obj : $(START_DIR)/ifWhileCond.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


diag.obj : $(START_DIR)/diag.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


norm.obj : $(START_DIR)/norm.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


DEIPFunc2.obj : $(START_DIR)/DEIPFunc2.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


sort.obj : $(START_DIR)/sort.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


sortIdx.obj : $(START_DIR)/sortIdx.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


insertionsort.obj : $(START_DIR)/insertionsort.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


introsort.obj : $(START_DIR)/introsort.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


heapsort.obj : $(START_DIR)/heapsort.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


fillIn.obj : $(START_DIR)/fillIn.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


sign.obj : $(START_DIR)/sign.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


parenAssign2D.obj : $(START_DIR)/parenAssign2D.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


DEIPFunc2_emxutil.obj : $(START_DIR)/DEIPFunc2_emxutil.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


DEIPFunc2_emxAPI.obj : $(START_DIR)/DEIPFunc2_emxAPI.c
	$(CC) $(CFLAGS) -Fo"$@" $(subst /,\,"$<")


###########################################################################
## DEPENDENCIES
###########################################################################

$(ALL_OBJS) : rtw_proj.tmw $(MAKEFILE)


###########################################################################
## MISCELLANEOUS TARGETS
###########################################################################

info : 
	@echo "### PRODUCT = $(PRODUCT)"
	@echo "### PRODUCT_TYPE = $(PRODUCT_TYPE)"
	@echo "### BUILD_TYPE = $(BUILD_TYPE)"
	@echo "### INCLUDES = $(INCLUDES)"
	@echo "### DEFINES = $(DEFINES)"
	@echo "### ALL_SRCS = $(ALL_SRCS)"
	@echo "### ALL_OBJS = $(ALL_OBJS)"
	@echo "### LIBS = $(LIBS)"
	@echo "### MODELREF_LIBS = $(MODELREF_LIBS)"
	@echo "### SYSTEM_LIBS = $(SYSTEM_LIBS)"
	@echo "### TOOLCHAIN_LIBS = $(TOOLCHAIN_LIBS)"
	@echo "### CFLAGS = $(CFLAGS)"
	@echo "### LDFLAGS = $(LDFLAGS)"
	@echo "### SHAREDLIB_LDFLAGS = $(SHAREDLIB_LDFLAGS)"
	@echo "### ARFLAGS = $(ARFLAGS)"
	@echo "### MEX_CFLAGS = $(MEX_CFLAGS)"
	@echo "### MEX_CPPFLAGS = $(MEX_CPPFLAGS)"
	@echo "### MEX_LDFLAGS = $(MEX_LDFLAGS)"
	@echo "### MEX_CPPLDFLAGS = $(MEX_CPPLDFLAGS)"
	@echo "### DOWNLOAD_FLAGS = $(DOWNLOAD_FLAGS)"
	@echo "### EXECUTE_FLAGS = $(EXECUTE_FLAGS)"
	@echo "### MAKE_FLAGS = $(MAKE_FLAGS)"


clean : 
	$(ECHO) "### Deleting all derived files..."
	$(RM) $(subst /,\,$(PRODUCT))
	$(RM) $(subst /,\,$(ALL_OBJS))
	$(ECHO) "### Deleted all derived files."


