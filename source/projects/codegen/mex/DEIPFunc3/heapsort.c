/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * heapsort.c
 *
 * Code generation for function 'heapsort'
 *
 */

/* Include files */
#include "heapsort.h"
#include "DEIPFunc3_data.h"
#include "DEIPFunc3_types.h"
#include "eml_int_forloop_overflow_check.h"
#include "rt_nonfinite.h"

/* Variable Definitions */
static emlrtRSInfo dl_emlrtRSI = { 20, /* lineNo */
  "heapsort",                          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\lib\\+coder\\+internal\\heapsort.m"/* pathName */
};

/* Function Declarations */
static void b_heapify(emxArray_int32_T *x, int32_T idx, int32_T xstart, int32_T
                      xend, const cell_wrap_6 cmp_tunableEnvironment[1]);
static void heapify(emxArray_int32_T *x, int32_T idx, int32_T xstart, int32_T
                    xend, const cell_wrap_6 cmp_tunableEnvironment[2]);

/* Function Definitions */
static void b_heapify(emxArray_int32_T *x, int32_T idx, int32_T xstart, int32_T
                      xend, const cell_wrap_6 cmp_tunableEnvironment[1])
{
  int32_T cmpIdx;
  int32_T extremum;
  int32_T extremumIdx;
  int32_T leftIdx;
  int32_T xcmp;
  boolean_T changed;
  boolean_T exitg1;
  changed = true;
  extremumIdx = (idx + xstart) - 2;
  leftIdx = ((idx << 1) + xstart) - 1;
  exitg1 = false;
  while ((!exitg1) && (leftIdx < xend)) {
    changed = false;
    extremum = x->data[extremumIdx];
    cmpIdx = leftIdx - 1;
    xcmp = x->data[leftIdx - 1];
    if (cmp_tunableEnvironment[0].f1->data[x->data[leftIdx - 1] - 1] <
        cmp_tunableEnvironment[0].f1->data[x->data[leftIdx] - 1]) {
      cmpIdx = leftIdx;
      xcmp = x->data[leftIdx];
    }

    if (cmp_tunableEnvironment[0].f1->data[x->data[extremumIdx] - 1] <
        cmp_tunableEnvironment[0].f1->data[xcmp - 1]) {
      x->data[extremumIdx] = xcmp;
      x->data[cmpIdx] = extremum;
      extremumIdx = cmpIdx;
      leftIdx = ((((cmpIdx - xstart) + 2) << 1) + xstart) - 1;
      changed = true;
    } else {
      exitg1 = true;
    }
  }

  if (changed && (leftIdx <= xend)) {
    extremum = x->data[extremumIdx];
    cmpIdx = x->data[leftIdx - 1];
    if (cmp_tunableEnvironment[0].f1->data[x->data[extremumIdx] - 1] <
        cmp_tunableEnvironment[0].f1->data[cmpIdx - 1]) {
      x->data[extremumIdx] = cmpIdx;
      x->data[leftIdx - 1] = extremum;
    }
  }
}

static void heapify(emxArray_int32_T *x, int32_T idx, int32_T xstart, int32_T
                    xend, const cell_wrap_6 cmp_tunableEnvironment[2])
{
  int32_T cmpIdx;
  int32_T extremum;
  int32_T extremumIdx;
  int32_T i;
  int32_T i1;
  int32_T i2;
  int32_T leftIdx;
  int32_T xcmp;
  boolean_T changed;
  boolean_T exitg1;
  boolean_T varargout_1;
  changed = true;
  extremumIdx = (idx + xstart) - 2;
  leftIdx = ((idx << 1) + xstart) - 2;
  exitg1 = false;
  while ((!exitg1) && (leftIdx + 1 < xend)) {
    changed = false;
    extremum = x->data[extremumIdx];
    cmpIdx = leftIdx;
    xcmp = x->data[leftIdx];
    i = x->data[leftIdx + 1] - 1;
    i1 = cmp_tunableEnvironment[0].f1->data[x->data[leftIdx] - 1];
    i2 = cmp_tunableEnvironment[0].f1->data[i];
    if (i1 < i2) {
      varargout_1 = true;
    } else if (i1 == i2) {
      varargout_1 = (cmp_tunableEnvironment[1].f1->data[x->data[leftIdx] - 1] <
                     cmp_tunableEnvironment[1].f1->data[i]);
    } else {
      varargout_1 = false;
    }

    if (varargout_1) {
      cmpIdx = leftIdx + 1;
      xcmp = x->data[leftIdx + 1];
    }

    i = cmp_tunableEnvironment[0].f1->data[x->data[extremumIdx] - 1];
    i1 = cmp_tunableEnvironment[0].f1->data[xcmp - 1];
    if (i < i1) {
      varargout_1 = true;
    } else if (i == i1) {
      varargout_1 = (cmp_tunableEnvironment[1].f1->data[x->data[extremumIdx] - 1]
                     < cmp_tunableEnvironment[1].f1->data[xcmp - 1]);
    } else {
      varargout_1 = false;
    }

    if (varargout_1) {
      x->data[extremumIdx] = xcmp;
      x->data[cmpIdx] = extremum;
      extremumIdx = cmpIdx;
      leftIdx = ((((cmpIdx - xstart) + 2) << 1) + xstart) - 2;
      changed = true;
    } else {
      exitg1 = true;
    }
  }

  if (changed && (leftIdx + 1 <= xend)) {
    extremum = x->data[extremumIdx];
    i = cmp_tunableEnvironment[0].f1->data[x->data[extremumIdx] - 1];
    i1 = cmp_tunableEnvironment[0].f1->data[x->data[leftIdx] - 1];
    if (i < i1) {
      varargout_1 = true;
    } else if (i == i1) {
      varargout_1 = (cmp_tunableEnvironment[1].f1->data[x->data[extremumIdx] - 1]
                     < cmp_tunableEnvironment[1].f1->data[x->data[leftIdx] - 1]);
    } else {
      varargout_1 = false;
    }

    if (varargout_1) {
      x->data[extremumIdx] = x->data[leftIdx];
      x->data[leftIdx] = extremum;
    }
  }
}

void b_heapsort(const emlrtStack *sp, emxArray_int32_T *x, int32_T xstart,
                int32_T xend, const cell_wrap_6 cmp_tunableEnvironment[2])
{
  emlrtStack b_st;
  emlrtStack st;
  int32_T k;
  int32_T n;
  int32_T t;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  n = xend - xstart;
  for (t = n + 1; t >= 1; t--) {
    heapify(x, t, xstart, xend, cmp_tunableEnvironment);
  }

  st.site = &dl_emlrtRSI;
  if ((1 <= n) && (n > 2147483646)) {
    b_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  for (k = 0; k < n; k++) {
    t = x->data[xend - 1];
    x->data[xend - 1] = x->data[xstart - 1];
    x->data[xstart - 1] = t;
    xend--;
    heapify(x, 1, xstart, xend, cmp_tunableEnvironment);
  }
}

void c_heapsort(const emlrtStack *sp, emxArray_int32_T *x, int32_T xstart,
                int32_T xend, const cell_wrap_6 cmp_tunableEnvironment[1])
{
  emlrtStack b_st;
  emlrtStack st;
  int32_T k;
  int32_T n;
  int32_T t;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  n = xend - xstart;
  for (t = n + 1; t >= 1; t--) {
    b_heapify(x, t, xstart, xend, cmp_tunableEnvironment);
  }

  st.site = &dl_emlrtRSI;
  if ((1 <= n) && (n > 2147483646)) {
    b_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  for (k = 0; k < n; k++) {
    t = x->data[xend - 1];
    x->data[xend - 1] = x->data[xstart - 1];
    x->data[xstart - 1] = t;
    xend--;
    b_heapify(x, 1, xstart, xend, cmp_tunableEnvironment);
  }
}

/* End of code generation (heapsort.c) */
