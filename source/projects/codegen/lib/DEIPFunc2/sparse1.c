/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sparse1.c
 *
 * Code generation for function 'sparse1'
 *
 */

/* Include files */
#include "sparse1.h"
#include "DEIPFunc2_emxutil.h"
#include "DEIPFunc2_types.h"
#include "fillIn.h"
#include "locBsearch.h"
#include "parenAssign2D.h"
#include "rt_nonfinite.h"
#include <stddef.h>
#include <string.h>

/* Function Declarations */
static int div_s32(int numerator, int denominator);
static void sparse_spallocLike(int m, int nzmax, emxArray_boolean_T *s_d,
  emxArray_int32_T *s_colidx, emxArray_int32_T *s_rowidx, int *s_m);

/* Function Definitions */
static int div_s32(int numerator, int denominator)
{
  unsigned int b_denominator;
  unsigned int b_numerator;
  int quotient;
  if (denominator == 0) {
    if (numerator >= 0) {
      quotient = MAX_int32_T;
    } else {
      quotient = MIN_int32_T;
    }
  } else {
    if (numerator < 0) {
      b_numerator = ~(unsigned int)numerator + 1U;
    } else {
      b_numerator = (unsigned int)numerator;
    }

    if (denominator < 0) {
      b_denominator = ~(unsigned int)denominator + 1U;
    } else {
      b_denominator = (unsigned int)denominator;
    }

    b_numerator /= b_denominator;
    if ((numerator < 0) != (denominator < 0)) {
      quotient = -(int)b_numerator;
    } else {
      quotient = (int)b_numerator;
    }
  }

  return quotient;
}

static void sparse_spallocLike(int m, int nzmax, emxArray_boolean_T *s_d,
  emxArray_int32_T *s_colidx, emxArray_int32_T *s_rowidx, int *s_m)
{
  int i;
  int numalloc;
  if (nzmax >= 1) {
    numalloc = nzmax;
  } else {
    numalloc = 1;
  }

  i = s_d->size[0];
  s_d->size[0] = numalloc;
  emxEnsureCapacity_boolean_T(s_d, i);
  for (i = 0; i < numalloc; i++) {
    s_d->data[i] = false;
  }

  i = s_colidx->size[0];
  s_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(s_colidx, i);
  i = s_rowidx->size[0];
  s_rowidx->size[0] = numalloc;
  emxEnsureCapacity_int32_T(s_rowidx, i);
  for (i = 0; i < numalloc; i++) {
    s_rowidx->data[i] = 0;
  }

  s_colidx->data[0] = 1;
  s_colidx->data[1] = 1;
  *s_m = m;
}

void b_sparse_and(const emxArray_boolean_T *a_d, const emxArray_int32_T
                  *a_colidx, const emxArray_boolean_T *b_d, const
                  emxArray_int32_T *b_colidx, const emxArray_int32_T *b_rowidx,
                  f_sparse *s)
{
  emxArray_boolean_T *tmpd;
  int i;
  int loop_ub;
  int nzs_tmp;
  boolean_T uniOp_tunableEnvironment_idx_0;
  if (a_colidx->data[a_colidx->size[0] - 1] - 1 > 0) {
    uniOp_tunableEnvironment_idx_0 = a_d->data[0];
  } else {
    uniOp_tunableEnvironment_idx_0 = false;
  }

  nzs_tmp = b_colidx->data[b_colidx->size[0] - 1];
  if (1 > b_colidx->data[b_colidx->size[0] - 1] - 1) {
    loop_ub = 0;
  } else {
    loop_ub = b_colidx->data[b_colidx->size[0] - 1] - 1;
  }

  emxInit_boolean_T(&tmpd, 1);
  i = tmpd->size[0];
  tmpd->size[0] = loop_ub;
  emxEnsureCapacity_boolean_T(tmpd, i);
  for (i = 0; i < loop_ub; i++) {
    tmpd->data[i] = (uniOp_tunableEnvironment_idx_0 && b_d->data[i]);
  }

  b_sparse_spallocLike(b_colidx->data[b_colidx->size[0] - 1] - 1, s->d,
                       s->colidx, s->rowidx);
  if (1 > b_colidx->data[b_colidx->size[0] - 1] - 1) {
    loop_ub = 1;
  } else {
    loop_ub = b_colidx->data[b_colidx->size[0] - 1];
  }

  for (i = 0; i <= loop_ub - 2; i++) {
    s->rowidx->data[i] = b_rowidx->data[i];
  }

  i = s->colidx->size[0];
  s->colidx->size[0] = b_colidx->size[0];
  emxEnsureCapacity_int32_T(s->colidx, i);
  loop_ub = b_colidx->size[0];
  for (i = 0; i < loop_ub; i++) {
    s->colidx->data[i] = b_colidx->data[i];
  }

  for (loop_ub = 0; loop_ub <= nzs_tmp - 2; loop_ub++) {
    s->d->data[loop_ub] = tmpd->data[loop_ub];
  }

  emxFree_boolean_T(&tmpd);
  c_sparse_fillIn(s);
}

void b_sparse_eq(const emxArray_real_T *a_d, const emxArray_int32_T *a_colidx,
                 const emxArray_real_T *b_d, const emxArray_int32_T *b_colidx,
                 const emxArray_int32_T *b_rowidx, f_sparse *s)
{
  emxArray_boolean_T *tmpd;
  double uniOp_tunableEnvironment_idx_0;
  int i;
  int idx;
  int loop_ub;
  boolean_T S;
  if (a_colidx->data[a_colidx->size[0] - 1] - 1 > 0) {
    uniOp_tunableEnvironment_idx_0 = a_d->data[0];
  } else {
    uniOp_tunableEnvironment_idx_0 = 0.0;
  }

  if (!(uniOp_tunableEnvironment_idx_0 == 0.0)) {
    idx = b_colidx->data[b_colidx->size[0] - 1];
    if (1 > b_colidx->data[b_colidx->size[0] - 1] - 1) {
      loop_ub = 0;
    } else {
      loop_ub = b_colidx->data[b_colidx->size[0] - 1] - 1;
    }

    emxInit_boolean_T(&tmpd, 1);
    i = tmpd->size[0];
    tmpd->size[0] = loop_ub;
    emxEnsureCapacity_boolean_T(tmpd, i);
    for (i = 0; i < loop_ub; i++) {
      tmpd->data[i] = (uniOp_tunableEnvironment_idx_0 == b_d->data[i]);
    }

    b_sparse_spallocLike(b_colidx->data[b_colidx->size[0] - 1] - 1, s->d,
                         s->colidx, s->rowidx);
    if (1 > b_colidx->data[b_colidx->size[0] - 1] - 1) {
      loop_ub = 1;
    } else {
      loop_ub = b_colidx->data[b_colidx->size[0] - 1];
    }

    for (i = 0; i <= loop_ub - 2; i++) {
      s->rowidx->data[i] = b_rowidx->data[i];
    }

    i = s->colidx->size[0];
    s->colidx->size[0] = b_colidx->size[0];
    emxEnsureCapacity_int32_T(s->colidx, i);
    loop_ub = b_colidx->size[0];
    for (i = 0; i < loop_ub; i++) {
      s->colidx->data[i] = b_colidx->data[i];
    }

    for (loop_ub = 0; loop_ub <= idx - 2; loop_ub++) {
      s->d->data[loop_ub] = tmpd->data[loop_ub];
    }

    emxFree_boolean_T(&tmpd);
    c_sparse_fillIn(s);
  } else {
    S = true;
    i = b_colidx->data[0];
    loop_ub = b_colidx->data[1] - 1;
    for (idx = i; idx <= loop_ub; idx++) {
      S = (uniOp_tunableEnvironment_idx_0 == b_d->data[0]);
    }

    i = s->d->size[0];
    s->d->size[0] = 1;
    emxEnsureCapacity_boolean_T(s->d, i);
    s->d->data[0] = false;
    i = s->colidx->size[0];
    s->colidx->size[0] = 2;
    emxEnsureCapacity_int32_T(s->colidx, i);
    s->colidx->data[0] = 1;
    i = s->rowidx->size[0];
    s->rowidx->size[0] = 1;
    emxEnsureCapacity_int32_T(s->rowidx, i);
    s->rowidx->data[0] = 1;
    s->colidx->data[1] = 1;
    if (S) {
      s->rowidx->data[0] = 1;
      s->d->data[0] = true;
      s->colidx->data[1] = 2;
    }
  }
}

void b_sparse_full(const emxArray_real_T *this_d, const emxArray_int32_T
                   *this_colidx, const emxArray_int32_T *this_rowidx, int this_m,
                   emxArray_real_T *y)
{
  int cend;
  int i;
  int idx;
  i = y->size[0] * y->size[1];
  y->size[0] = this_m;
  y->size[1] = 1;
  emxEnsureCapacity_real_T(y, i);
  for (i = 0; i < this_m; i++) {
    y->data[i] = 0.0;
  }

  cend = this_colidx->data[1] - 1;
  i = this_colidx->data[0];
  for (idx = i; idx <= cend; idx++) {
    y->data[this_rowidx->data[idx - 1] - 1] = this_d->data[idx - 1];
  }
}

void b_sparse_gt(const emxArray_real_T *a_d, const emxArray_int32_T *a_colidx,
                 emxArray_boolean_T *s_d, emxArray_int32_T *s_colidx,
                 emxArray_int32_T *s_rowidx)
{
  double uniOp_tunableEnvironment_idx_0;
  int i;
  if (a_colidx->data[a_colidx->size[0] - 1] - 1 > 0) {
    uniOp_tunableEnvironment_idx_0 = a_d->data[0];
  } else {
    uniOp_tunableEnvironment_idx_0 = 0.0;
  }

  i = s_d->size[0];
  s_d->size[0] = 1;
  emxEnsureCapacity_boolean_T(s_d, i);
  s_d->data[0] = false;
  i = s_colidx->size[0];
  s_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(s_colidx, i);
  s_colidx->data[0] = 1;
  i = s_rowidx->size[0];
  s_rowidx->size[0] = 1;
  emxEnsureCapacity_int32_T(s_rowidx, i);
  s_rowidx->data[0] = 1;
  s_colidx->data[1] = 1;
  if (uniOp_tunableEnvironment_idx_0 > 0.0) {
    s_rowidx->data[0] = 1;
    s_d->data[0] = true;
    s_colidx->data[1] = 2;
  }
}

void b_sparse_parenAssign(d_sparse *this, const emxArray_real_T *rhs, const
  emxArray_real_T *varargin_1)
{
  int k;
  int nidx;
  int siz_idx_0;
  int vk;
  nidx = varargin_1->size[0];
  for (k = 0; k < nidx; k++) {
    siz_idx_0 = this->m;
    vk = div_s32((int)varargin_1->data[k] - 1, siz_idx_0);
    sparse_parenAssign2D(this, rhs->data[k], (int)varargin_1->data[k] - vk *
                         siz_idx_0, vk + 1);
  }
}

void b_sparse_parenReference(const emxArray_real_T *this_d, const
  emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx, const
  emxArray_real_T *varargin_1, double varargin_2, emxArray_real_T *s_d,
  emxArray_int32_T *s_colidx, emxArray_int32_T *s_rowidx, int *s_m)
{
  double s_d_tmp;
  int colNnz;
  int i;
  int i1;
  int idx;
  int k;
  int ridx;
  int sm;
  boolean_T found;
  sm = varargin_1->size[1];
  s_d->size[0] = 0;
  s_rowidx->size[0] = 0;
  i = s_colidx->size[0];
  s_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(s_colidx, i);
  s_colidx->data[0] = 1;
  colNnz = 1;
  k = 0;
  for (ridx = 0; ridx < sm; ridx++) {
    b_sparse_locBsearch(this_rowidx, (int)varargin_1->data[ridx],
                        this_colidx->data[(int)varargin_2 - 1],
                        this_colidx->data[(int)varargin_2], &idx, &found);
    if (found) {
      i = s_d->size[0];
      i1 = s_d->size[0];
      s_d->size[0]++;
      emxEnsureCapacity_real_T(s_d, i1);
      s_d_tmp = this_d->data[idx - 1];
      s_d->data[i] = s_d_tmp;
      i = s_rowidx->size[0];
      i1 = s_rowidx->size[0];
      s_rowidx->size[0]++;
      emxEnsureCapacity_int32_T(s_rowidx, i1);
      s_rowidx->data[i] = ridx + 1;
      s_d->data[k] = s_d_tmp;
      s_rowidx->data[k] = ridx + 1;
      k++;
      colNnz++;
    }
  }

  s_colidx->data[1] = colNnz;
  if (s_colidx->data[1] - 1 == 0) {
    i = s_rowidx->size[0];
    s_rowidx->size[0] = 1;
    emxEnsureCapacity_int32_T(s_rowidx, i);
    s_rowidx->data[0] = 1;
    i = s_d->size[0];
    s_d->size[0] = 1;
    emxEnsureCapacity_real_T(s_d, i);
    s_d->data[0] = 0.0;
  }

  *s_m = varargin_1->size[1];
}

void b_sparse_spallocLike(int nzmax, emxArray_boolean_T *s_d, emxArray_int32_T
  *s_colidx, emxArray_int32_T *s_rowidx)
{
  int i;
  int numalloc;
  if (nzmax >= 1) {
    numalloc = nzmax;
  } else {
    numalloc = 1;
  }

  i = s_d->size[0];
  s_d->size[0] = numalloc;
  emxEnsureCapacity_boolean_T(s_d, i);
  for (i = 0; i < numalloc; i++) {
    s_d->data[i] = false;
  }

  i = s_colidx->size[0];
  s_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(s_colidx, i);
  i = s_rowidx->size[0];
  s_rowidx->size[0] = numalloc;
  emxEnsureCapacity_int32_T(s_rowidx, i);
  for (i = 0; i < numalloc; i++) {
    s_rowidx->data[i] = 0;
  }

  s_colidx->data[0] = 1;
  s_colidx->data[1] = 1;
}

void c_sparse_eq(double a, const emxArray_real_T *b_d, const emxArray_int32_T
                 *b_colidx, const emxArray_int32_T *b_rowidx, int b_m, e_sparse *
                 s)
{
  emxArray_boolean_T *tmpd;
  int i;
  int loop_ub;
  int nzs_tmp;
  nzs_tmp = b_colidx->data[b_colidx->size[0] - 1];
  if (1 > b_colidx->data[b_colidx->size[0] - 1] - 1) {
    loop_ub = 0;
  } else {
    loop_ub = b_colidx->data[b_colidx->size[0] - 1] - 1;
  }

  emxInit_boolean_T(&tmpd, 1);
  i = tmpd->size[0];
  tmpd->size[0] = loop_ub;
  emxEnsureCapacity_boolean_T(tmpd, i);
  for (i = 0; i < loop_ub; i++) {
    tmpd->data[i] = (a == b_d->data[i]);
  }

  sparse_spallocLike(b_m, b_colidx->data[b_colidx->size[0] - 1] - 1, s->d,
                     s->colidx, s->rowidx, &s->m);
  if (1 > b_colidx->data[b_colidx->size[0] - 1] - 1) {
    loop_ub = 1;
  } else {
    loop_ub = b_colidx->data[b_colidx->size[0] - 1];
  }

  for (i = 0; i <= loop_ub - 2; i++) {
    s->rowidx->data[i] = b_rowidx->data[i];
  }

  i = s->colidx->size[0];
  s->colidx->size[0] = b_colidx->size[0];
  emxEnsureCapacity_int32_T(s->colidx, i);
  loop_ub = b_colidx->size[0];
  for (i = 0; i < loop_ub; i++) {
    s->colidx->data[i] = b_colidx->data[i];
  }

  for (loop_ub = 0; loop_ub <= nzs_tmp - 2; loop_ub++) {
    s->d->data[loop_ub] = tmpd->data[loop_ub];
  }

  emxFree_boolean_T(&tmpd);
  b_sparse_fillIn(s);
}

void c_sparse_parenAssign(d_sparse *this, double rhs, const emxArray_real_T
  *varargin_1, double varargin_2)
{
  double thisv;
  int k;
  int nelem;
  int ridx;
  int sm;
  int vidx;
  boolean_T found;
  sm = varargin_1->size[0];
  for (ridx = 0; ridx < sm; ridx++) {
    b_sparse_locBsearch(this->rowidx, (int)varargin_1->data[ridx], this->
                        colidx->data[(int)varargin_2 - 1], this->colidx->data
                        [(int)varargin_2], &vidx, &found);
    if (found) {
      thisv = this->d->data[vidx - 1];
    } else {
      thisv = 0.0;
    }

    if ((!(thisv == 0.0)) || (!(rhs == 0.0))) {
      if ((thisv != 0.0) && (rhs != 0.0)) {
        this->d->data[vidx - 1] = rhs;
      } else if (thisv == 0.0) {
        if (this->colidx->data[this->colidx->size[0] - 1] - 1 == this->maxnz) {
          b_realloc(this, this->colidx->data[this->colidx->size[0] - 1] + 9,
                    vidx, vidx + 1, this->colidx->data[this->colidx->size[0] - 1]
                    - 1);
          this->rowidx->data[vidx] = (int)varargin_1->data[ridx];
          this->d->data[vidx] = rhs;
        } else {
          nelem = (this->colidx->data[this->colidx->size[0] - 1] - vidx) - 1;
          if (nelem > 0) {
            memmove((void *)&this->rowidx->data[vidx + 1], (void *)&this->
                    rowidx->data[vidx], (unsigned int)((size_t)nelem * sizeof
                     (int)));
            memmove((void *)&this->d->data[vidx + 1], (void *)&this->d->
                    data[vidx], (unsigned int)((size_t)nelem * sizeof(double)));
          }

          this->d->data[vidx] = rhs;
          this->rowidx->data[vidx] = (int)varargin_1->data[ridx];
        }

        vidx = (int)varargin_2 + 1;
        nelem = this->n + 1;
        for (k = vidx; k <= nelem; k++) {
          this->colidx->data[k - 1]++;
        }
      } else {
        nelem = (this->colidx->data[this->colidx->size[0] - 1] - vidx) - 1;
        if (nelem > 0) {
          memmove((void *)&this->rowidx->data[vidx - 1], (void *)&this->
                  rowidx->data[vidx], (unsigned int)((size_t)nelem * sizeof(int)));
          memmove((void *)&this->d->data[vidx - 1], (void *)&this->d->data[vidx],
                  (unsigned int)((size_t)nelem * sizeof(double)));
        }

        vidx = (int)varargin_2 + 1;
        nelem = this->n + 1;
        for (k = vidx; k <= nelem; k++) {
          this->colidx->data[k - 1]--;
        }
      }
    }
  }
}

void c_sparse_parenReference(const emxArray_real_T *this_d, const
  emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx, int this_m,
  double varargin_2, emxArray_real_T *s_d, emxArray_int32_T *s_colidx,
  emxArray_int32_T *s_rowidx, int *s_m)
{
  int b_nd_tmp;
  int k;
  int nd_tmp;
  int numalloc;
  int s_d_tmp;
  nd_tmp = this_colidx->data[(int)varargin_2 - 1];
  b_nd_tmp = this_colidx->data[(int)varargin_2] - nd_tmp;
  if (b_nd_tmp >= 1) {
    numalloc = b_nd_tmp;
  } else {
    numalloc = 1;
  }

  s_d_tmp = s_d->size[0];
  s_d->size[0] = numalloc;
  emxEnsureCapacity_real_T(s_d, s_d_tmp);
  for (s_d_tmp = 0; s_d_tmp < numalloc; s_d_tmp++) {
    s_d->data[s_d_tmp] = 0.0;
  }

  s_d_tmp = s_colidx->size[0];
  s_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(s_colidx, s_d_tmp);
  s_d_tmp = s_rowidx->size[0];
  s_rowidx->size[0] = numalloc;
  emxEnsureCapacity_int32_T(s_rowidx, s_d_tmp);
  for (s_d_tmp = 0; s_d_tmp < numalloc; s_d_tmp++) {
    s_rowidx->data[s_d_tmp] = 0;
  }

  s_colidx->data[0] = 1;
  s_colidx->data[1] = 1;
  if (b_nd_tmp != 0) {
    numalloc = 0;
    for (k = 0; k < b_nd_tmp; k++) {
      s_d_tmp = (nd_tmp + k) - 1;
      s_d->data[numalloc] = this_d->data[s_d_tmp];
      s_rowidx->data[numalloc] = this_rowidx->data[s_d_tmp];
      numalloc++;
    }

    s_colidx->data[1] = b_nd_tmp + 1;
  }

  *s_m = this_m;
}

void d_sparse_parenReference(const emxArray_real_T *this_d, const
  emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx, double
  varargin_1, double varargin_2, emxArray_real_T *s_d, emxArray_int32_T
  *s_colidx, emxArray_int32_T *s_rowidx)
{
  int i;
  int idx;
  boolean_T found;
  s_d->size[0] = 0;
  s_rowidx->size[0] = 0;
  i = s_colidx->size[0];
  s_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(s_colidx, i);
  s_colidx->data[0] = 1;
  s_colidx->data[1] = 1;
  b_sparse_locBsearch(this_rowidx, (int)varargin_1, this_colidx->data[(int)
                      varargin_2 - 1], this_colidx->data[(int)varargin_2], &idx,
                      &found);
  if (found) {
    i = s_d->size[0];
    s_d->size[0] = 1;
    emxEnsureCapacity_real_T(s_d, i);
    i = s_rowidx->size[0];
    s_rowidx->size[0] = 1;
    emxEnsureCapacity_int32_T(s_rowidx, i);
    s_d->data[0] = this_d->data[idx - 1];
    s_rowidx->data[0] = 1;
    s_colidx->data[1] = 2;
  }

  if (s_colidx->data[1] - 1 == 0) {
    i = s_rowidx->size[0];
    s_rowidx->size[0] = 1;
    emxEnsureCapacity_int32_T(s_rowidx, i);
    s_rowidx->data[0] = 1;
    i = s_d->size[0];
    s_d->size[0] = 1;
    emxEnsureCapacity_real_T(s_d, i);
    s_d->data[0] = 0.0;
  }
}

void sparse_and(const emxArray_boolean_T *a_d, const emxArray_int32_T *a_colidx,
                const emxArray_int32_T *a_rowidx, const emxArray_boolean_T *b_d,
                const emxArray_int32_T *b_colidx, const emxArray_int32_T
                *b_rowidx, int b_m, emxArray_boolean_T *s_d, emxArray_int32_T
                *s_colidx, emxArray_int32_T *s_rowidx, int *s_m)
{
  int aidx;
  int bidx;
  int numalloc;
  boolean_T moreAToDo;
  boolean_T moreBToDo;
  if (a_colidx->data[a_colidx->size[0] - 1] - 1 < b_colidx->data[b_colidx->size
      [0] - 1] - 1) {
    numalloc = a_colidx->data[a_colidx->size[0] - 1] - 1;
  } else {
    numalloc = b_colidx->data[b_colidx->size[0] - 1] - 1;
  }

  if (numalloc < 1) {
    numalloc = 1;
  }

  sparse_spallocLike(b_m, numalloc, s_d, s_colidx, s_rowidx, s_m);
  numalloc = 1;
  s_colidx->data[0] = 1;
  aidx = a_colidx->data[0];
  bidx = b_colidx->data[0] - 1;
  moreAToDo = (a_colidx->data[0] < a_colidx->data[1]);
  moreBToDo = (b_colidx->data[0] < b_colidx->data[1]);
  while (moreAToDo || moreBToDo) {
    while ((aidx < a_colidx->data[1]) && ((!moreBToDo) || (a_rowidx->data[aidx -
             1] < b_rowidx->data[bidx]))) {
      aidx++;
    }

    moreAToDo = (aidx < a_colidx->data[1]);
    while ((bidx + 1 < b_colidx->data[1]) && ((!moreAToDo) || (b_rowidx->
             data[bidx] < a_rowidx->data[aidx - 1]))) {
      bidx++;
    }

    while ((aidx < a_colidx->data[1]) && (bidx + 1 < b_colidx->data[1]) &&
           (a_rowidx->data[aidx - 1] == b_rowidx->data[bidx])) {
      if (a_d->data[aidx - 1] && b_d->data[bidx]) {
        s_d->data[numalloc - 1] = true;
        s_rowidx->data[numalloc - 1] = b_rowidx->data[bidx];
        numalloc++;
      }

      bidx++;
      aidx++;
    }

    moreAToDo = (aidx < a_colidx->data[1]);
    moreBToDo = (bidx + 1 < b_colidx->data[1]);
  }

  s_colidx->data[1] = numalloc;
}

void sparse_eq(const emxArray_real_T *a_d, const emxArray_int32_T *a_colidx,
               const emxArray_int32_T *a_rowidx, const emxArray_real_T *b_d,
               const emxArray_int32_T *b_colidx, const emxArray_int32_T
               *b_rowidx, int b_m, emxArray_boolean_T *s_d, emxArray_int32_T
               *s_colidx, emxArray_int32_T *s_rowidx, int *s_m)
{
  emxArray_boolean_T *S;
  int aidx;
  int bidx;
  int i;
  int k;
  boolean_T moreAToDo;
  boolean_T moreBToDo;
  emxInit_boolean_T(&S, 1);
  i = S->size[0];
  S->size[0] = b_m;
  emxEnsureCapacity_boolean_T(S, i);
  for (i = 0; i < b_m; i++) {
    S->data[i] = true;
  }

  aidx = a_colidx->data[0] - 1;
  bidx = b_colidx->data[0] - 1;
  moreAToDo = (a_colidx->data[0] < a_colidx->data[1]);
  moreBToDo = (b_colidx->data[0] < b_colidx->data[1]);
  while (moreAToDo || moreBToDo) {
    while ((aidx + 1 < a_colidx->data[1]) && ((!moreBToDo) || (a_rowidx->
             data[aidx] < b_rowidx->data[bidx]))) {
      S->data[a_rowidx->data[aidx] - 1] = (a_d->data[aidx] == 0.0);
      aidx++;
    }

    moreAToDo = (aidx + 1 < a_colidx->data[1]);
    while ((bidx + 1 < b_colidx->data[1]) && ((!moreAToDo) || (b_rowidx->
             data[bidx] < a_rowidx->data[aidx]))) {
      S->data[b_rowidx->data[bidx] - 1] = (0.0 == b_d->data[bidx]);
      bidx++;
    }

    while ((aidx + 1 < a_colidx->data[1]) && (bidx + 1 < b_colidx->data[1]) &&
           (a_rowidx->data[aidx] == b_rowidx->data[bidx])) {
      S->data[b_rowidx->data[bidx] - 1] = (a_d->data[aidx] == b_d->data[bidx]);
      bidx++;
      aidx++;
    }

    moreAToDo = (aidx + 1 < a_colidx->data[1]);
    moreBToDo = (bidx + 1 < b_colidx->data[1]);
  }

  bidx = S->size[0];
  aidx = 0;
  i = S->size[0];
  for (k = 0; k < i; k++) {
    if (S->data[k]) {
      aidx++;
    }
  }

  *s_m = S->size[0];
  if (aidx < 1) {
    aidx = 1;
  }

  i = s_d->size[0];
  s_d->size[0] = aidx;
  emxEnsureCapacity_boolean_T(s_d, i);
  for (i = 0; i < aidx; i++) {
    s_d->data[i] = false;
  }

  i = s_colidx->size[0];
  s_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(s_colidx, i);
  s_colidx->data[0] = 1;
  i = s_rowidx->size[0];
  s_rowidx->size[0] = aidx;
  emxEnsureCapacity_int32_T(s_rowidx, i);
  for (i = 0; i < aidx; i++) {
    s_rowidx->data[i] = 0;
  }

  s_rowidx->data[0] = 1;
  aidx = 0;
  for (k = 0; k < bidx; k++) {
    if (S->data[k]) {
      s_rowidx->data[aidx] = k + 1;
      s_d->data[aidx] = true;
      aidx++;
    }
  }

  emxFree_boolean_T(&S);
  s_colidx->data[1] = aidx + 1;
}

double sparse_full(const emxArray_real_T *this_d, const emxArray_int32_T
                   *this_colidx)
{
  double y;
  int cend;
  int i;
  int idx;
  y = 0.0;
  cend = this_colidx->data[1] - 1;
  i = this_colidx->data[0];
  for (idx = i; idx <= cend; idx++) {
    y = this_d->data[0];
  }

  return y;
}

void sparse_gt(const emxArray_real_T *a_d, const emxArray_int32_T *a_colidx,
               const emxArray_int32_T *a_rowidx, int a_m, e_sparse *s)
{
  emxArray_boolean_T *tmpd;
  int i;
  int loop_ub;
  int nzs_tmp;
  nzs_tmp = a_colidx->data[a_colidx->size[0] - 1];
  if (1 > a_colidx->data[a_colidx->size[0] - 1] - 1) {
    loop_ub = 0;
  } else {
    loop_ub = a_colidx->data[a_colidx->size[0] - 1] - 1;
  }

  emxInit_boolean_T(&tmpd, 1);
  i = tmpd->size[0];
  tmpd->size[0] = loop_ub;
  emxEnsureCapacity_boolean_T(tmpd, i);
  for (i = 0; i < loop_ub; i++) {
    tmpd->data[i] = (a_d->data[i] > 0.0);
  }

  sparse_spallocLike(a_m, a_colidx->data[a_colidx->size[0] - 1] - 1, s->d,
                     s->colidx, s->rowidx, &s->m);
  if (1 > a_colidx->data[a_colidx->size[0] - 1] - 1) {
    loop_ub = 1;
  } else {
    loop_ub = a_colidx->data[a_colidx->size[0] - 1];
  }

  for (i = 0; i <= loop_ub - 2; i++) {
    s->rowidx->data[i] = a_rowidx->data[i];
  }

  i = s->colidx->size[0];
  s->colidx->size[0] = a_colidx->size[0];
  emxEnsureCapacity_int32_T(s->colidx, i);
  loop_ub = a_colidx->size[0];
  for (i = 0; i < loop_ub; i++) {
    s->colidx->data[i] = a_colidx->data[i];
  }

  for (loop_ub = 0; loop_ub <= nzs_tmp - 2; loop_ub++) {
    s->d->data[loop_ub] = tmpd->data[loop_ub];
  }

  emxFree_boolean_T(&tmpd);
  b_sparse_fillIn(s);
}

void sparse_parenAssign(d_sparse *this, const emxArray_real_T *rhs_d, const
  emxArray_int32_T *rhs_colidx, const emxArray_int32_T *rhs_rowidx, int rhs_m,
  const emxArray_real_T *varargin_1)
{
  emxArray_int8_T *s_colidx;
  emxArray_real_T *s_d;
  double rhsv;
  int b_vk;
  int cend;
  int idx;
  int k;
  int nidx;
  int siz_idx_0;
  int vk;
  boolean_T found;
  nidx = varargin_1->size[0];
  emxInit_real_T(&s_d, 1);
  emxInit_int8_T(&s_colidx, 1);
  for (k = 0; k < nidx; k++) {
    siz_idx_0 = this->m;
    vk = div_s32((int)varargin_1->data[k] - 1, siz_idx_0);
    s_d->size[0] = 0;
    b_vk = s_colidx->size[0];
    s_colidx->size[0] = 2;
    emxEnsureCapacity_int8_T(s_colidx, b_vk);
    s_colidx->data[0] = 1;
    s_colidx->data[1] = 1;
    b_vk = div_s32(k, rhs_m);
    sparse_locBsearch(rhs_rowidx, (k - b_vk * rhs_m) + 1, rhs_colidx->data[b_vk],
                      rhs_colidx->data[b_vk + 1], &idx, &found);
    if (found) {
      b_vk = s_d->size[0];
      s_d->size[0] = 1;
      emxEnsureCapacity_real_T(s_d, b_vk);
      s_d->data[0] = rhs_d->data[idx - 1];
      s_colidx->data[1] = 2;
    }

    if (s_d->size[0] == 0) {
      b_vk = s_d->size[0];
      s_d->size[0] = 1;
      emxEnsureCapacity_real_T(s_d, b_vk);
      s_d->data[0] = 0.0;
    }

    rhsv = 0.0;
    cend = s_colidx->data[1] - 1;
    b_vk = s_colidx->data[0];
    for (idx = b_vk; idx <= cend; idx++) {
      rhsv = s_d->data[0];
    }

    sparse_parenAssign2D(this, rhsv, (int)varargin_1->data[k] - vk * siz_idx_0,
                         vk + 1);
  }

  emxFree_int8_T(&s_colidx);
  emxFree_real_T(&s_d);
}

void sparse_parenReference(const emxArray_real_T *this_d, const emxArray_int32_T
  *this_colidx, const emxArray_int32_T *this_rowidx, int this_m, const
  emxArray_real_T *varargin_1, emxArray_real_T *s_d, emxArray_int32_T *s_colidx,
  emxArray_int32_T *s_rowidx, int *s_m)
{
  int colNnz;
  int i;
  int idx;
  int k;
  int nrow;
  int ridx;
  int vk;
  boolean_T found;
  nrow = varargin_1->size[0];
  *s_m = varargin_1->size[0];
  s_d->size[0] = 0;
  s_rowidx->size[0] = 0;
  vk = s_colidx->size[0];
  s_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(s_colidx, vk);
  k = 0;
  s_colidx->data[0] = 1;
  colNnz = 1;
  for (ridx = 0; ridx < nrow; ridx++) {
    vk = div_s32((int)varargin_1->data[k] - 1, this_m);
    sparse_locBsearch(this_rowidx, (int)varargin_1->data[k] - vk * this_m,
                      this_colidx->data[vk], this_colidx->data[vk + 1], &idx,
                      &found);
    if (found) {
      vk = s_d->size[0];
      i = s_d->size[0];
      s_d->size[0]++;
      emxEnsureCapacity_real_T(s_d, i);
      s_d->data[vk] = this_d->data[idx - 1];
      vk = s_rowidx->size[0];
      i = s_rowidx->size[0];
      s_rowidx->size[0]++;
      emxEnsureCapacity_int32_T(s_rowidx, i);
      s_rowidx->data[vk] = ridx + 1;
      colNnz++;
    }

    k++;
  }

  s_colidx->data[1] = colNnz;
  if (s_d->size[0] == 0) {
    vk = s_d->size[0];
    s_d->size[0] = 1;
    emxEnsureCapacity_real_T(s_d, vk);
    s_d->data[0] = 0.0;
    vk = s_rowidx->size[0];
    s_rowidx->size[0] = 1;
    emxEnsureCapacity_int32_T(s_rowidx, vk);
    s_rowidx->data[0] = 0;
  }
}

void sparse_times(const emxArray_real_T *a, const emxArray_real_T *b_d, const
                  emxArray_int32_T *b_colidx, const emxArray_int32_T *b_rowidx,
                  int b_m, emxArray_real_T *s_d, emxArray_int32_T *s_colidx,
                  emxArray_int32_T *s_rowidx, int *s_m)
{
  double val;
  int idx;
  int numalloc;
  int row;
  numalloc = -1;
  row = a->size[0];
  for (idx = 0; idx < row; idx++) {
    if (a->data[idx] != 0.0) {
      numalloc++;
    }
  }

  numalloc += b_colidx->data[b_colidx->size[0] - 1];
  if (numalloc >= b_m) {
    numalloc = b_m;
  }

  if (numalloc < 1) {
    numalloc = 1;
  }

  row = s_d->size[0];
  s_d->size[0] = numalloc;
  emxEnsureCapacity_real_T(s_d, row);
  for (row = 0; row < numalloc; row++) {
    s_d->data[row] = 0.0;
  }

  row = s_colidx->size[0];
  s_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(s_colidx, row);
  row = s_rowidx->size[0];
  s_rowidx->size[0] = numalloc;
  emxEnsureCapacity_int32_T(s_rowidx, row);
  for (row = 0; row < numalloc; row++) {
    s_rowidx->data[row] = 0;
  }

  s_colidx->data[0] = 1;
  numalloc = 1;
  idx = b_colidx->data[0];
  for (row = 0; row < b_m; row++) {
    if ((idx < b_colidx->data[1]) && (row + 1 == b_rowidx->data[idx - 1])) {
      val = a->data[row] * b_d->data[idx - 1];
      idx++;
    } else {
      val = 0.0;
    }

    if (val != 0.0) {
      s_d->data[numalloc - 1] = val;
      s_rowidx->data[numalloc - 1] = row + 1;
      numalloc++;
    }
  }

  s_colidx->data[1] = numalloc;
  *s_m = b_m;
}

/* End of code generation (sparse1.c) */
