/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc2_data.c
 *
 * Code generation for function 'DEIPFunc2_data'
 *
 */

/* Include files */
#include "DEIPFunc2_data.h"
#include "rt_nonfinite.h"

/* Variable Definitions */
emlrtCTX emlrtRootTLSGlobal = NULL;
const volatile char_T *emlrtBreakCheckR2012bFlagVar = NULL;
emlrtContext emlrtContextGlobal = { true,/* bFirstTime */
  false,                               /* bInitialized */
  131595U,                             /* fVersionInfo */
  NULL,                                /* fErrorFunction */
  "DEIPFunc2",                         /* fFunctionName */
  NULL,                                /* fRTCallStack */
  false,                               /* bDebugMode */
  { 2045744189U, 2170104910U, 2743257031U, 4284093946U },/* fSigWrd */
  NULL                                 /* fSigMem */
};

emlrtRSInfo ff_emlrtRSI = { 21,        /* lineNo */
  "eml_int_forloop_overflow_check",    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\eml\\eml_int_forloop_overflow_check.m"/* pathName */
};

emlrtRSInfo tf_emlrtRSI = { 27,        /* lineNo */
  "sort",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sort.m"/* pathName */
};

emlrtRSInfo uh_emlrtRSI = { 14,        /* lineNo */
  "max",                               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\max.m"/* pathName */
};

emlrtRSInfo vh_emlrtRSI = { 44,        /* lineNo */
  "minOrMax",                          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\minOrMax.m"/* pathName */
};

emlrtRSInfo wh_emlrtRSI = { 79,        /* lineNo */
  "maximum",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\minOrMax.m"/* pathName */
};

emlrtRSInfo xh_emlrtRSI = { 169,       /* lineNo */
  "unaryMinOrMax",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pathName */
};

emlrtRSInfo ii_emlrtRSI = { 13,        /* lineNo */
  "sparse",                            /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\sparfun\\sparse.m"/* pathName */
};

emlrtRSInfo qi_emlrtRSI = { 221,       /* lineNo */
  "sparse/sparse",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

emlrtRSInfo jj_emlrtRSI = { 14,        /* lineNo */
  "sparse/fillIn",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\fillIn.m"/* pathName */
};

emlrtRSInfo il_emlrtRSI = { 66,        /* lineNo */
  "sparse/parenAssign2D",              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign2D.m"/* pathName */
};

emlrtRSInfo jl_emlrtRSI = { 77,        /* lineNo */
  "parenAssign2DNumeric",              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign2D.m"/* pathName */
};

emlrtRSInfo kl_emlrtRSI = { 78,        /* lineNo */
  "parenAssign2DNumeric",              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign2D.m"/* pathName */
};

emlrtRSInfo ll_emlrtRSI = { 81,        /* lineNo */
  "parenAssign2DNumeric",              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign2D.m"/* pathName */
};

emlrtRSInfo ml_emlrtRSI = { 110,       /* lineNo */
  "parenAssign2DNumericImpl",          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign2D.m"/* pathName */
};

emlrtRSInfo ol_emlrtRSI = { 119,       /* lineNo */
  "parenAssign2DNumericImpl",          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign2D.m"/* pathName */
};

emlrtRSInfo ql_emlrtRSI = { 124,       /* lineNo */
  "parenAssign2DNumericImpl",          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign2D.m"/* pathName */
};

emlrtRSInfo cm_emlrtRSI = { 51,        /* lineNo */
  "sparse/sparse",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

emlrtRSInfo qm_emlrtRSI = { 28,        /* lineNo */
  "colon",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\colon.m"/* pathName */
};

emlrtRSInfo no_emlrtRSI = { 266,       /* lineNo */
  "sparse/parenAssign",                /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

emlrtRTEInfo v_emlrtRTEI = { 1594,     /* lineNo */
  31,                                  /* colNo */
  "assertValidSize",                   /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

emlrtRTEInfo w_emlrtRTEI = { 11,       /* lineNo */
  27,                                  /* colNo */
  "sparse/sparse_validateNumericIndex",/* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\private\\validateNumericIndex.m"/* pName */
};

emlrtRTEInfo x_emlrtRTEI = { 12,       /* lineNo */
  27,                                  /* colNo */
  "sparse/sparse_validateNumericIndex",/* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\private\\validateNumericIndex.m"/* pName */
};

emlrtRTEInfo ic_emlrtRTEI = { 239,     /* lineNo */
  13,                                  /* colNo */
  "sparse/numel",                      /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

emlrtRTEInfo jd_emlrtRTEI = { 195,     /* lineNo */
  42,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

emlrtRTEInfo de_emlrtRTEI = { 27,      /* lineNo */
  6,                                   /* colNo */
  "sort",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sort.m"/* pName */
};

/* End of code generation (DEIPFunc2_data.c) */
