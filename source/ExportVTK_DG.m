% Default VTK export script for DG analyses; assumes all output is
% requested. Allows different element types.
% Run this AFTER an FEA_Program analysis
%
% Last revision: 1/18/2022 TJT

% remove Lagrange multiplier elements
if max(MatTypeTable(2,:),[],2) > 8
    if max(MatTypeTable(2,:),[],2) > 22 % Lagrange multipliers
        matSol = find(MatTypeTable(2,:)<=8,1,'last'); % Assuming sorted material types, last material that is solid or coupler
        [~,lastelems] = unique(RegionOnElement,'last'); % Assuming sorted element types, last element of each material
        numelPV = lastelems(matSol); % last element that is solid or coupler
        nenPV2 = 0;
    else % Periodic DG elements
        numelPV = numel;
        nenPV2 = ndm; %strip off the last ndm columns of macro nodes
    end
else
    numelPV = numel;
    nenPV2 = 0;
end
% handle whether couplers are present
if exist('RegionsOnInterface','var') % function version of DEIP
nodeelemcoup = [1 numnp 1 RegionsOnInterface(1,5)-1 RegionsOnInterface(1,5) numelPV];
exp_CZ = 1;
elseif exist('numelCG','var') && numelCG < numelPV % script version of DEIP
nodeelemcoup = [1 numnp 1 numelCG numelCG+1 numelPV];
exp_CZ = 1;
else % no couplers
nodeelemcoup = [1 numnp 1 numelPV 0 0];
exp_CZ = 0;
end
nenPV = 0; % automatically determine element type
otherflag = [ndm 8]; % 2d model with DG couplers
flagout = [1 exist('DispList','var') exist('StreList','var') exist('StreList','var') exist('StreListE','var') exist('StreListE','var') exp_CZ 0];
fileprop.dfoldername =  'ParaOut';
fileprop.filename = 'out'; %prefNodesOnElement filename (e.g. 'out' -> out01.vtk)
fileprop.header = 'Elasticity'; %text at top of VTK files

if exist('StreList','var') && exist('StreListE','var')
VTKOutputWriter([],[],nodeelemcoup,nenPV,otherflag,flagout,fileprop, ...
            Coordinates,NodesOnElement(:,1:end-nenPV2),RegionOnElement,DispList,StreList, ...
            StreListE)
elseif exist('StreList','var')
VTKOutputWriter([],[],nodeelemcoup,nenPV,otherflag,flagout,fileprop, ...
            Coordinates,NodesOnElement(:,1:end-nenPV2),RegionOnElement,DispList,StreList)
elseif exist('StreListE','var')
VTKOutputWriter([],[],nodeelemcoup,nenPV,otherflag,flagout,fileprop, ...
            Coordinates,NodesOnElement(:,1:end-nenPV2),RegionOnElement,DispList,[],StreListE)
elseif exist('DispList','var')
VTKOutputWriter([],[],nodeelemcoup,nenPV,otherflag,flagout,fileprop, ...
            Coordinates,NodesOnElement(:,1:end-nenPV2),RegionOnElement,DispList)
else
VTKOutputWriter([],[],nodeelemcoup,nenPV,otherflag,flagout,fileprop, ...
            Coordinates,NodesOnElement(:,1:end-nenPV2),RegionOnElement)
end