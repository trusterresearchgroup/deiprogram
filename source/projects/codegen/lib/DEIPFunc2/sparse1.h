/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sparse1.h
 *
 * Code generation for function 'sparse1'
 *
 */

#ifndef SPARSE1_H
#define SPARSE1_H

/* Include files */
#include "DEIPFunc2_types.h"
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>
#ifdef __cplusplus

extern "C" {

#endif

  /* Function Declarations */
  void b_sparse_and(const emxArray_boolean_T *a_d, const emxArray_int32_T
                    *a_colidx, const emxArray_boolean_T *b_d, const
                    emxArray_int32_T *b_colidx, const emxArray_int32_T *b_rowidx,
                    f_sparse *s);
  void b_sparse_eq(const emxArray_real_T *a_d, const emxArray_int32_T *a_colidx,
                   const emxArray_real_T *b_d, const emxArray_int32_T *b_colidx,
                   const emxArray_int32_T *b_rowidx, f_sparse *s);
  void b_sparse_full(const emxArray_real_T *this_d, const emxArray_int32_T
                     *this_colidx, const emxArray_int32_T *this_rowidx, int
                     this_m, emxArray_real_T *y);
  void b_sparse_gt(const emxArray_real_T *a_d, const emxArray_int32_T *a_colidx,
                   emxArray_boolean_T *s_d, emxArray_int32_T *s_colidx,
                   emxArray_int32_T *s_rowidx);
  void b_sparse_parenAssign(d_sparse *this, const emxArray_real_T *rhs, const
    emxArray_real_T *varargin_1);
  void b_sparse_parenReference(const emxArray_real_T *this_d, const
    emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx, const
    emxArray_real_T *varargin_1, double varargin_2, emxArray_real_T *s_d,
    emxArray_int32_T *s_colidx, emxArray_int32_T *s_rowidx, int *s_m);
  void b_sparse_spallocLike(int nzmax, emxArray_boolean_T *s_d, emxArray_int32_T
    *s_colidx, emxArray_int32_T *s_rowidx);
  void c_sparse_eq(double a, const emxArray_real_T *b_d, const emxArray_int32_T *
                   b_colidx, const emxArray_int32_T *b_rowidx, int b_m, e_sparse
                   *s);
  void c_sparse_parenAssign(d_sparse *this, double rhs, const emxArray_real_T
    *varargin_1, double varargin_2);
  void c_sparse_parenReference(const emxArray_real_T *this_d, const
    emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx, int
    this_m, double varargin_2, emxArray_real_T *s_d, emxArray_int32_T *s_colidx,
    emxArray_int32_T *s_rowidx, int *s_m);
  void d_sparse_parenReference(const emxArray_real_T *this_d, const
    emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx, double
    varargin_1, double varargin_2, emxArray_real_T *s_d, emxArray_int32_T
    *s_colidx, emxArray_int32_T *s_rowidx);
  void sparse_and(const emxArray_boolean_T *a_d, const emxArray_int32_T
                  *a_colidx, const emxArray_int32_T *a_rowidx, const
                  emxArray_boolean_T *b_d, const emxArray_int32_T *b_colidx,
                  const emxArray_int32_T *b_rowidx, int b_m, emxArray_boolean_T *
                  s_d, emxArray_int32_T *s_colidx, emxArray_int32_T *s_rowidx,
                  int *s_m);
  void sparse_eq(const emxArray_real_T *a_d, const emxArray_int32_T *a_colidx,
                 const emxArray_int32_T *a_rowidx, const emxArray_real_T *b_d,
                 const emxArray_int32_T *b_colidx, const emxArray_int32_T
                 *b_rowidx, int b_m, emxArray_boolean_T *s_d, emxArray_int32_T
                 *s_colidx, emxArray_int32_T *s_rowidx, int *s_m);
  double sparse_full(const emxArray_real_T *this_d, const emxArray_int32_T
                     *this_colidx);
  void sparse_gt(const emxArray_real_T *a_d, const emxArray_int32_T *a_colidx,
                 const emxArray_int32_T *a_rowidx, int a_m, e_sparse *s);
  void sparse_parenAssign(d_sparse *this, const emxArray_real_T *rhs_d, const
    emxArray_int32_T *rhs_colidx, const emxArray_int32_T *rhs_rowidx, int rhs_m,
    const emxArray_real_T *varargin_1);
  void sparse_parenReference(const emxArray_real_T *this_d, const
    emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx, int
    this_m, const emxArray_real_T *varargin_1, emxArray_real_T *s_d,
    emxArray_int32_T *s_colidx, emxArray_int32_T *s_rowidx, int *s_m);
  void sparse_times(const emxArray_real_T *a, const emxArray_real_T *b_d, const
                    emxArray_int32_T *b_colidx, const emxArray_int32_T *b_rowidx,
                    int b_m, emxArray_real_T *s_d, emxArray_int32_T *s_colidx,
                    emxArray_int32_T *s_rowidx, int *s_m);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (sparse1.h) */
