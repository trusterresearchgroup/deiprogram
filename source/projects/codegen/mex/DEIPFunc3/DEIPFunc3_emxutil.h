/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc3_emxutil.h
 *
 * Code generation for function 'DEIPFunc3_emxutil'
 *
 */

#pragma once

/* Include files */
#include "DEIPFunc3_types.h"
#include "rtwtypes.h"
#include "emlrt.h"
#include "mex.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Function Declarations */
void emxEnsureCapacity_boolean_T(const emlrtStack *sp, emxArray_boolean_T
  *emxArray, int32_T oldNumel, const emlrtRTEInfo *srcLocation);
void emxEnsureCapacity_int32_T(const emlrtStack *sp, emxArray_int32_T *emxArray,
  int32_T oldNumel, const emlrtRTEInfo *srcLocation);
void emxEnsureCapacity_int64_T(const emlrtStack *sp, emxArray_int64_T *emxArray,
  int32_T oldNumel, const emlrtRTEInfo *srcLocation);
void emxEnsureCapacity_real_T(const emlrtStack *sp, emxArray_real_T *emxArray,
  int32_T oldNumel, const emlrtRTEInfo *srcLocation);
void emxFreeMatrix_cell_wrap_6(cell_wrap_6 pMatrix[2]);
void emxFreeMatrix_cell_wrap_61(cell_wrap_6 pMatrix[1]);
void emxFreeStruct_cell_wrap_6(cell_wrap_6 *pStruct);
void emxFreeStruct_sparse(f_sparse *pStruct);
void emxFreeStruct_sparse1(e_sparse *pStruct);
void emxFreeStruct_sparse2(h_sparse *pStruct);
void emxFreeStruct_sparse3(g_sparse *pStruct);
void emxFree_boolean_T(emxArray_boolean_T **pEmxArray);
void emxFree_int32_T(emxArray_int32_T **pEmxArray);
void emxFree_int64_T(emxArray_int64_T **pEmxArray);
void emxFree_real_T(emxArray_real_T **pEmxArray);
void emxInitMatrix_cell_wrap_6(const emlrtStack *sp, cell_wrap_6 pMatrix[2],
  const emlrtRTEInfo *srcLocation, boolean_T doPush);
void emxInitMatrix_cell_wrap_61(const emlrtStack *sp, cell_wrap_6 pMatrix[1],
  const emlrtRTEInfo *srcLocation, boolean_T doPush);
void emxInitStruct_cell_wrap_6(const emlrtStack *sp, cell_wrap_6 *pStruct, const
  emlrtRTEInfo *srcLocation, boolean_T doPush);
void emxInitStruct_sparse(const emlrtStack *sp, f_sparse *pStruct, const
  emlrtRTEInfo *srcLocation, boolean_T doPush);
void emxInitStruct_sparse1(const emlrtStack *sp, e_sparse *pStruct, const
  emlrtRTEInfo *srcLocation, boolean_T doPush);
void emxInitStruct_sparse2(const emlrtStack *sp, h_sparse *pStruct, const
  emlrtRTEInfo *srcLocation, boolean_T doPush);
void emxInitStruct_sparse3(const emlrtStack *sp, g_sparse *pStruct, const
  emlrtRTEInfo *srcLocation, boolean_T doPush);
void emxInit_boolean_T(const emlrtStack *sp, emxArray_boolean_T **pEmxArray,
  int32_T numDimensions, const emlrtRTEInfo *srcLocation, boolean_T doPush);
void emxInit_int32_T(const emlrtStack *sp, emxArray_int32_T **pEmxArray, int32_T
                     numDimensions, const emlrtRTEInfo *srcLocation, boolean_T
                     doPush);
void emxInit_int64_T(const emlrtStack *sp, emxArray_int64_T **pEmxArray, int32_T
                     numDimensions, const emlrtRTEInfo *srcLocation, boolean_T
                     doPush);
void emxInit_real_T(const emlrtStack *sp, emxArray_real_T **pEmxArray, int32_T
                    numDimensions, const emlrtRTEInfo *srcLocation, boolean_T
                    doPush);

/* End of code generation (DEIPFunc3_emxutil.h) */
