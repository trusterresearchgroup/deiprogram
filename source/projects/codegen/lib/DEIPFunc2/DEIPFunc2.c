/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc2.c
 *
 * Code generation for function 'DEIPFunc2'
 *
 */

/* Include files */
#include "DEIPFunc2.h"
#include "DEIPFunc2_data.h"
#include "DEIPFunc2_emxutil.h"
#include "DEIPFunc2_initialize.h"
#include "DEIPFunc2_types.h"
#include "any.h"
#include "colon.h"
#include "diag.h"
#include "eml_setop.h"
#include "find.h"
#include "ifWhileCond.h"
#include "isequal.h"
#include "ismember.h"
#include "minOrMax.h"
#include "nnz.h"
#include "norm.h"
#include "not.h"
#include "parenAssign2D.h"
#include "rt_nonfinite.h"
#include "sign.h"
#include "sort.h"
#include "sparse.h"
#include "sparse1.h"
#include "sum.h"
#include "unique.h"
#include "rt_nonfinite.h"
#include <math.h>
#include <stdio.h>
#include <string.h>

/* Function Declarations */
static double rt_roundd_snf(double u);

/* Function Definitions */
static double rt_roundd_snf(double u)
{
  double y;
  if (fabs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = floor(u + 0.5);
    } else if (u > -0.5) {
      y = u * 0.0;
    } else {
      y = ceil(u - 0.5);
    }
  } else {
    y = u;
  }

  return y;
}

void DEIPFunc2(emxArray_real_T *InterTypes, emxArray_real_T *NodesOnElement,
               const emxArray_real_T *RegionOnElement, emxArray_real_T
               *Coordinates, double *numnp, double numel, double nummat, double
               nen, double ndm, double usePBC, const double *numMPC, const
               emxArray_real_T *MPCList, double *numEonB, emxArray_real_T
               *numEonF, emxArray_real_T *ElementsOnBoundary, double *numSI,
               emxArray_real_T *ElementsOnFacet, d_sparse *ElementsOnNode,
               d_sparse *ElementsOnNodeDup, emxArray_real_T *ElementsOnNodeNum,
               double *numfac, emxArray_real_T *ElementsOnNodeNum2, double
               *numinttype, emxArray_real_T *FacetsOnElement, emxArray_real_T
               *FacetsOnElementInt, emxArray_real_T *FacetsOnInterface,
               emxArray_real_T *FacetsOnInterfaceNum, d_sparse *FacetsOnNode,
               d_sparse *FacetsOnNodeCut, d_sparse *FacetsOnNodeInt,
               emxArray_real_T *FacetsOnNodeNum, emxArray_real_T *NodeCGDG,
               d_sparse *NodeReg, emxArray_real_T *NodesOnElementCG,
               emxArray_real_T *NodesOnElementDG, emxArray_real_T
               *NodesOnInterface, double *NodesOnInterfaceNum, double *numCL,
               emxArray_real_T *NodesOnPBC, emxArray_real_T *NodesOnPBCnum,
               emxArray_real_T *NodesOnLink, emxArray_real_T *NodesOnLinknum,
               emxArray_real_T *numEonPBC, emxArray_real_T *FacetsOnPBC,
               emxArray_real_T *FacetsOnPBCNum, emxArray_real_T
               *FacetsOnIntMinusPBC, emxArray_real_T *FacetsOnIntMinusPBCNum)
{
  d_sparse b_ElementsOnNodePBCRow;
  d_sparse expl_temp;
  e_sparse b_expl_temp;
  e_sparse c_expl_temp;
  e_sparse d_expl_temp;
  emxArray_boolean_T NodesOnPBC_data;
  emxArray_boolean_T *b_NodesOnPBCnum;
  emxArray_boolean_T *r4;
  emxArray_int32_T *ii;
  emxArray_int32_T *indx;
  emxArray_int32_T *jj;
  emxArray_int32_T *r;
  emxArray_int32_T *r1;
  emxArray_int32_T *r2;
  emxArray_int32_T *r3;
  emxArray_int32_T *r5;
  emxArray_int32_T *r6;
  emxArray_int32_T *t6_rowidx;
  emxArray_int64_T *inter;
  emxArray_real_T b_nodes_data;
  emxArray_real_T c_nodes_data;
  emxArray_real_T *Coordinates3;
  emxArray_real_T *ElementsOnNodeA;
  emxArray_real_T *ElementsOnNodePBCNum;
  emxArray_real_T *ElementsOnNodePBCRow;
  emxArray_real_T *ElementsOnNodeRow;
  emxArray_real_T *FacetsOnNodeInd;
  emxArray_real_T *MorePBC;
  emxArray_real_T *NodeRegInd;
  emxArray_real_T *NodesOnElementPBC;
  emxArray_real_T *b;
  emxArray_real_T *b_ElementsOnFacet;
  emxArray_real_T *b_MPCList;
  emxArray_real_T *b_MorePBC;
  emxArray_real_T *b_NodeRegInd;
  emxArray_real_T *c_NodeRegInd;
  emxArray_real_T *interreg2;
  emxArray_real_T *r7;
  emxArray_real_T *setAll;
  emxArray_real_T *setPBC;
  emxArray_real_T *setnPBC;
  emxArray_real_T *t4_d;
  f_sparse e_expl_temp;
  f_sparse f_expl_temp;
  f_sparse g_expl_temp;
  g_sparse NodeRegSum;
  double nodeC2_data[27];
  double nodeD2_data[27];
  double links[20];
  double links2_data[20];
  double nodes[20];
  double nodes_data[20];
  double MorePBC_data[4];
  double nodeloc_data[4];
  double b_node[2];
  double b_i1;
  double b_i2;
  double d;
  double d1;
  double elemA;
  double nextra;
  double node;
  double nodeA;
  double nodeB;
  double numA;
  double numFPBC;
  double numSI_tmp;
  long long b_qY;
  long long mat;
  long long q0;
  long long qY;
  int node_dup_tmp_data[27];
  int tmp_data[27];
  int ia_data[20];
  int c_tmp_data[4];
  int NodesOnElementCG_size[2];
  int nodeC2_size[2];
  int nodeD2_size[2];
  int tmp_size[2];
  int NodesOnPBC_size[1];
  int b_NodesOnElement[1];
  int ia_size[1];
  int ib_size[1];
  int links2_size[1];
  int nodeloc_size[1];
  int nodes_size[1];
  int b_i;
  int b_loop_ub;
  int c_loop_ub;
  int d_loop_ub;
  int e_loop_ub;
  int elem;
  int elemB;
  int f_loop_ub;
  int facI1;
  int i;
  int i1;
  int i2;
  int i3;
  unsigned int iSec2;
  int locF;
  int locFA;
  int locN;
  int loop_ub;
  int nel;
  int notdone;
  unsigned int nri;
  int numPBC;
  int nume;
  int trueCount;
  signed char b_tmp_data[27];
  signed char nloop4[8];
  signed char nloop3[6];
  boolean_T NodesOnElementCG_data[27];
  boolean_T b_NodesOnPBC_data[4];
  boolean_T guard1 = false;
  boolean_T old;
  (void)ndm;
  (void)numMPC;
  if (!isInitialized_DEIPFunc2) {
    DEIPFunc2_initialize();
  }

  /*  Program: DEIProgram2 */
  /*  Tim Truster */
  /*  08/15/2014 */
  /*  */
  /*  Script to convert CG mesh (e.g. from block program output) into DG mesh */
  /*  Addition of modules to insert interfaces selectively between materials */
  /*   */
  /*  Input: matrix InterTypes(nummat,nummat) for listing the interface */
  /*  material types to assign; only lower triangle is accessed; value 0 means */
  /*  interface is not inserted between those materials (including diagonal). */
  /*  */
  /*  Numbering of interface types (matI) is like: */
  /*  1 */
  /*  2 3 */
  /*  4 5 6 ... */
  /*  */
  /*  Output: */
  /*  Actual list of used interfaces is SurfacesI and numCL, new BC arrays and */
  /*  load arrays */
  /*  Total list of interfaces is in numIfac and Interfac, although there is */
  /*  no designation on whether the interfaces are active or not. These are */
  /*  useful for assigning different DG element types to different interfaces. */
  /*  Revisions: */
  /*  02/10/2017: adding periodic boundary conditions (PBC); uses both zipped mesh */
  /*  (collapsing linked PBC nodes into a single node and updating */
  /*  connectivity), and ghost elements (leaving original mesh, added elements */
  /*  along PBC edges that connect across the domain); both result in a 'torus' */
  /*  mesh. */
  /*  Revisions: */
  /*  04/04/2017: adding periodic BC in the form of multi-point constraints, */
  /*  which do not require the domain to be a cube. Still uses the zipped mesh, */
  /*  but does not add ghost elements. Instead, node duplication is performed */
  /*  directly on the zipped mesh, and MPC elements are applied outside of this */
  /*  subroutine. */
  /*  % flags for clearing out intermediate variables in this script; turned on */
  /*  % by default */
  /*  currvariables = who(); */
  /*  clearinter = 1; */
  numSI_tmp = 0.0;

  /*  % Test for Octave vs Matlab compatibility */
  /*  isOctave = exist('OCTAVE_VERSION', 'builtin') ~= 0; */
  /*  ElementsOnNode = zeros(maxel,numnp); % Array of elements connected to nodes */
  /*  ElementsOnNodeDup = zeros(maxel,numnp); % actual node ID given to element for an original node; general counterpart of NodeMat */
  i = ElementsOnNodeNum->size[0];
  ElementsOnNodeNum->size[0] = (int)*numnp;
  emxEnsureCapacity_real_T(ElementsOnNodeNum, i);
  loop_ub = (int)*numnp;
  for (i = 0; i < loop_ub; i++) {
    ElementsOnNodeNum->data[i] = 0.0;
  }

  /*  if ~exist('usePBC','var') */
  /*      usePBC = 0; */
  /*  end */
  emxInit_real_T(&ElementsOnNodePBCNum, 1);
  if (usePBC != 0.0) {
    /*  create extra arrays for zipped mesh */
    /*  ElementsOnNodePBC = zeros(maxel,numnp); % Array of elements connected to nodes */
    /*  ElementsOnNodePBCDup = zeros(maxel,numnp); % actual node ID given to element for an original node; general counterpart of NodeMat */
    i = ElementsOnNodePBCNum->size[0];
    ElementsOnNodePBCNum->size[0] = (int)*numnp;
    emxEnsureCapacity_real_T(ElementsOnNodePBCNum, i);
    loop_ub = (int)*numnp;
    for (i = 0; i < loop_ub; i++) {
      ElementsOnNodePBCNum->data[i] = 0.0;
    }
  } else {
    i = ElementsOnNodePBCNum->size[0];
    ElementsOnNodePBCNum->size[0] = 1;
    emxEnsureCapacity_real_T(ElementsOnNodePBCNum, i);
    ElementsOnNodePBCNum->data[0] = 0.0;
  }

  emxInit_real_T(&Coordinates3, 2);

  /*  NodeReg = zeros(numnp,nummat); */
  i = (int)(numel * nen);
  i1 = Coordinates3->size[0] * Coordinates3->size[1];
  Coordinates3->size[0] = i;
  Coordinates3->size[1] = 2;
  emxEnsureCapacity_real_T(Coordinates3, i1);
  loop_ub = i << 1;
  for (i = 0; i < loop_ub; i++) {
    Coordinates3->data[i] = 0.0;
  }

  i = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
  NodesOnElementCG->size[0] = NodesOnElement->size[0];
  NodesOnElementCG->size[1] = NodesOnElement->size[1];
  emxEnsureCapacity_real_T(NodesOnElementCG, i);
  loop_ub = NodesOnElement->size[0] * NodesOnElement->size[1];
  for (i = 0; i < loop_ub; i++) {
    NodesOnElementCG->data[i] = NodesOnElement->data[i];
  }

  /*  Set up PBC data structures */
  emxInit_real_T(&NodesOnElementPBC, 2);
  emxInit_real_T(&MorePBC, 1);
  emxInit_real_T(&setnPBC, 1);
  emxInit_int32_T(&indx, 1);
  emxInit_int32_T(&t6_rowidx, 1);
  emxInit_real_T(&b_MPCList, 2);
  emxInit_boolean_T(&b_NodesOnPBCnum, 1);
  if (usePBC != 0.0) {
    /*  Add space in connectivity for ghost elements */
    /*      if nen == 3 */
    /*          NodesOnElementCG = [NodesOnElementCG zeros(numel,4-nen) */
    /*                              zeros(numPBC*2,4)]; */
    /*          nen = 4; */
    /*      elseif nen == 6 */
    /*          NodesOnElementCG = [NodesOnElementCG zeros(numel,9-nen) */
    /*                              zeros(numPBC*2,9)]; */
    /*          nen = 9; */
    /*      end */
    i = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1];
    NodesOnElementPBC->size[0] = NodesOnElement->size[0];
    NodesOnElementPBC->size[1] = NodesOnElement->size[1];
    emxEnsureCapacity_real_T(NodesOnElementPBC, i);
    loop_ub = NodesOnElement->size[0] * NodesOnElement->size[1];
    for (i = 0; i < loop_ub; i++) {
      NodesOnElementPBC->data[i] = NodesOnElement->data[i];
    }

    /*  Connectivity that is zipped, having PBC nodes condensed together */
    /*      RegionOnElement = [RegionOnElement; zeros(numPBC*2,1)]; */
    /*  Lists of node pairs in convenient table format */
    i = NodesOnPBC->size[0] * NodesOnPBC->size[1];
    NodesOnPBC->size[0] = 4;
    NodesOnPBC->size[1] = (int)*numnp;
    emxEnsureCapacity_real_T(NodesOnPBC, i);
    notdone = (int)*numnp << 2;
    for (i = 0; i < notdone; i++) {
      NodesOnPBC->data[i] = 0.0;
    }

    /*  nodes that are paired up */
    i = NodesOnPBCnum->size[0];
    NodesOnPBCnum->size[0] = (int)*numnp;
    emxEnsureCapacity_real_T(NodesOnPBCnum, i);
    loop_ub = (int)*numnp;
    for (i = 0; i < loop_ub; i++) {
      NodesOnPBCnum->data[i] = 0.0;
    }

    /*  number of nodes with common pairing */
    i = NodesOnLink->size[0] * NodesOnLink->size[1];
    NodesOnLink->size[0] = 4;
    NodesOnLink->size[1] = (int)*numnp;
    emxEnsureCapacity_real_T(NodesOnLink, i);
    for (i = 0; i < notdone; i++) {
      NodesOnLink->data[i] = 0.0;
    }

    /*  IDs of PBC referring to node */
    i = NodesOnLinknum->size[0];
    NodesOnLinknum->size[0] = (int)*numnp;
    emxEnsureCapacity_real_T(NodesOnLinknum, i);
    loop_ub = (int)*numnp;
    for (i = 0; i < loop_ub; i++) {
      NodesOnLinknum->data[i] = 0.0;
    }

    /*  number of PBC referring to node */
    /*  Handle the corners first */
    if (usePBC == 2.0) {
      /*      PBCList = MPCList; */
      numPBC = MPCList->size[0];
    } else {
      /*      PBCList = MPCList; */
      numPBC = MPCList->size[0];
      loop_ub = MPCList->size[0];
      i = b_MPCList->size[0] * b_MPCList->size[1];
      b_MPCList->size[0] = loop_ub;
      b_MPCList->size[1] = 2;
      emxEnsureCapacity_real_T(b_MPCList, i);
      for (i = 0; i < loop_ub; i++) {
        b_MPCList->data[i] = MPCList->data[i + MPCList->size[0] * 2];
      }

      for (i = 0; i < loop_ub; i++) {
        b_MPCList->data[i + b_MPCList->size[0]] = MPCList->data[i +
          MPCList->size[0] * 3];
      }

      emxInit_real_T(&b, 2);
      unique_rows(b_MPCList, b, indx);
      i = MorePBC->size[0];
      MorePBC->size[0] = indx->size[0];
      emxEnsureCapacity_real_T(MorePBC, i);
      loop_ub = indx->size[0];
      for (i = 0; i < loop_ub; i++) {
        MorePBC->data[i] = indx->data[i];
      }

      if (b->size[0] == 2) {
        d = b->data[0];
        d1 = b->data[b->size[0]];
        i = 4 * ((int)d - 1);
        NodesOnPBC->data[i] = d1;
        NodesOnPBCnum->data[(int)d - 1] = 1.0;
        i1 = 4 * ((int)d1 - 1);
        NodesOnPBC->data[i1] = d;
        NodesOnPBCnum->data[(int)d1 - 1] = 1.0;
        NodesOnLink->data[i] = -MorePBC->data[0];
        NodesOnLinknum->data[(int)d - 1] = 1.0;
        NodesOnLink->data[i1] = -MorePBC->data[0];
        NodesOnLinknum->data[(int)d1 - 1] = 1.0;
        d = b->data[1];
        d1 = b->data[b->size[0] + 1];
        i = 4 * ((int)d - 1);
        NodesOnPBC->data[i] = d1;
        NodesOnPBCnum->data[(int)d - 1] = 1.0;
        i1 = 4 * ((int)d1 - 1) + 1;
        NodesOnPBC->data[i1] = d;
        NodesOnPBCnum->data[(int)d1 - 1] = 2.0;
        NodesOnLink->data[i] = -MorePBC->data[1];
        NodesOnLinknum->data[(int)d - 1] = 1.0;
        NodesOnLink->data[i1] = -MorePBC->data[1];
        NodesOnLinknum->data[(int)d1 - 1] = 2.0;
      }

      emxFree_real_T(&b);
    }

    /*  Now do the rest of the nodes */
    for (locFA = 0; locFA < numPBC; locFA++) {
      notdone = (int)MPCList->data[locFA];
      numA = NodesOnPBCnum->data[notdone - 1];
      if (1.0 > numA) {
        loop_ub = 0;
      } else {
        loop_ub = (int)numA;
      }

      /*  make sure pair isn't already in the list for nodeA */
      /*      if isOctave */
      nodeloc_size[0] = loop_ub;
      for (i = 0; i < loop_ub; i++) {
        nodeloc_data[i] = NodesOnPBC->data[i + 4 * (notdone - 1)];
      }

      old = local_ismember(MPCList->data[locFA + MPCList->size[0]], nodeloc_data,
                           nodeloc_size);

      /*      else */
      /*      old = ismembc(nodesAB(2),currPBC); */
      /*      end */
      if (!old) {
        /*  add the pair to nodeA, expand by one */
        i = 4 * (notdone - 1);
        NodesOnPBC->data[((int)(numA + 1.0) + i) - 1] = MPCList->data[locFA +
          MPCList->size[0]];
        NodesOnPBCnum->data[notdone - 1]++;

        /*  Record which PBC ID refers to these nodes */
        numA = NodesOnLinknum->data[notdone - 1] + 1.0;
        NodesOnLink->data[((int)numA + i) - 1] = (double)locFA + 1.0;
        NodesOnLinknum->data[notdone - 1]++;
      }

      notdone = (int)MPCList->data[locFA + MPCList->size[0]];
      d = NodesOnPBCnum->data[notdone - 1];
      if (1.0 > d) {
        loop_ub = 0;
      } else {
        loop_ub = (int)d;
      }

      /*  make sure pair isn't already in the list for nodeB */
      /*      if isOctave */
      nodeloc_size[0] = loop_ub;
      for (i = 0; i < loop_ub; i++) {
        nodeloc_data[i] = NodesOnPBC->data[i + 4 * (notdone - 1)];
      }

      old = local_ismember(MPCList->data[locFA], nodeloc_data, nodeloc_size);

      /*      else */
      /*      old = ismembc(nodesAB(1),currPBC); */
      /*      end */
      if (!old) {
        /*  add the pair to nodeB, expand by one */
        i = 4 * (notdone - 1);
        NodesOnPBC->data[((int)(d + 1.0) + i) - 1] = MPCList->data[locFA];
        NodesOnPBCnum->data[notdone - 1]++;

        /*  Record which PBC ID refers to these nodes */
        NodesOnLinknum->data[notdone - 1]++;
        NodesOnLink->data[((int)numA + i) - 1] = (double)locFA + 1.0;
      }
    }

    /*  Find additional connections for nodes with pairs of pairs; this WILL find */
    /*  the extra pairs that are usually deleted to remove linear dependence in */
    /*  the stiffness matrix, e.g. the repeated edges in 2d or 3d. */
    i = b_NodesOnPBCnum->size[0];
    b_NodesOnPBCnum->size[0] = NodesOnPBCnum->size[0];
    emxEnsureCapacity_boolean_T(b_NodesOnPBCnum, i);
    loop_ub = NodesOnPBCnum->size[0];
    for (i = 0; i < loop_ub; i++) {
      b_NodesOnPBCnum->data[i] = (NodesOnPBCnum->data[i] > 1.0);
    }

    eml_find(b_NodesOnPBCnum, t6_rowidx);
    i = MorePBC->size[0];
    MorePBC->size[0] = t6_rowidx->size[0];
    emxEnsureCapacity_real_T(MorePBC, i);
    loop_ub = t6_rowidx->size[0];
    for (i = 0; i < loop_ub; i++) {
      MorePBC->data[i] = t6_rowidx->data[i];
    }

    memset(&nodes[0], 0, 20U * sizeof(double));
    i = MorePBC->size[0];
    for (b_i = 0; b_i < i; b_i++) {
      nodes[0] = MorePBC->data[b_i];
      if (NodesOnPBCnum->data[(int)MorePBC->data[b_i] - 1] > 0.0) {
        /*  pairs not already condensed */
        notdone = 1;
        numPBC = 1;

        /*  use a tree of connectivity to get to all node pairs involving MorePBC(i) */
        while (notdone != 0) {
          /*              nodesS = nodes(1:lenn); */
          b_i1 = (double)numPBC + 1.0;
          b_i2 = (double)numPBC + 1.0;
          for (elemB = 0; elemB < numPBC; elemB++) {
            d = NodesOnPBCnum->data[(int)nodes[elemB] - 1];
            if (1.0 > d) {
              loop_ub = 0;
            } else {
              loop_ub = (int)d;
            }

            b_i2 = (b_i1 + (double)loop_ub) - 1.0;
            if (b_i1 > b_i2) {
              i1 = 1;
            } else {
              i1 = (int)b_i1;
            }

            numA = nodes[elemB];
            for (i2 = 0; i2 < loop_ub; i2++) {
              nodes[(i1 + i2) - 1] = NodesOnPBC->data[i2 + 4 * ((int)numA - 1)];
            }

            b_i1 = b_i2 + 1.0;
          }

          if (1.0 > b_i2) {
            loop_ub = 0;
          } else {
            loop_ub = (int)b_i2;
          }

          nodes_size[0] = loop_ub;
          if (0 <= loop_ub - 1) {
            memcpy(&nodes_data[0], &nodes[0], loop_ub * sizeof(double));
          }

          b_nodes_data.data = &nodes_data[0];
          b_nodes_data.size = &nodes_size[0];
          b_nodes_data.allocatedSize = 20;
          b_nodes_data.numDimensions = 1;
          b_nodes_data.canFreeData = false;
          unique_vector(&b_nodes_data, setnPBC);
          if (setnPBC->size[0] == numPBC) {
            /*  starting list of node pairs matches the unique short list */
            loop_ub = setnPBC->size[0];
            for (i1 = 0; i1 < loop_ub; i1++) {
              nodes[i1] = setnPBC->data[i1];
            }

            notdone = 0;
          } else {
            /*  found some new pairs, try searching again */
            loop_ub = setnPBC->size[0];
            for (i1 = 0; i1 < loop_ub; i1++) {
              nodes[i1] = setnPBC->data[i1];
            }
          }

          numPBC = setnPBC->size[0];
        }

        if (numPBC + 1U > 20U) {
          i1 = 0;
          i2 = -1;
        } else {
          i1 = numPBC;
          i2 = 19;
        }

        loop_ub = (i2 - i1) + 1;
        if (0 <= loop_ub - 1) {
          memset(&nodes[i1], 0, ((loop_ub + i1) - i1) * sizeof(double));
        }

        /* clean it out */
        /*  record the maximum connected pairs into the NodesOnPBC for all */
        /*  the connected nodes in the set; also combine together the */
        /*  NodesOnLink at the same time */
        /*          lenn = length(nodes); */
        memset(&links[0], 0, 20U * sizeof(double));
        b_i1 = 1.0;
        b_i2 = 1.0;
        for (elemB = 0; elemB < numPBC; elemB++) {
          locFA = (int)nodes[elemB] - 1;
          NodesOnPBCnum->data[locFA] = -((double)numPBC - 1.0);

          /*  flag this node as already condensed */
          if (1 > numPBC) {
            loop_ub = 0;
          } else {
            loop_ub = numPBC;
          }

          nodes_size[0] = loop_ub;
          if (0 <= loop_ub - 1) {
            memcpy(&nodes_data[0], &nodes[0], loop_ub * sizeof(double));
          }

          nodeloc_size[0] = nodes_size[0];
          do_vectors(nodes_data, nodeloc_size, nodes[elemB], links2_data,
                     links2_size, ia_data, ia_size, ib_size);
          loop_ub = links2_size[0];
          for (i1 = 0; i1 < loop_ub; i1++) {
            NodesOnPBC->data[i1 + 4 * locFA] = links2_data[i1];
          }

          numA = NodesOnLinknum->data[locFA];
          b_i2 = (b_i1 + numA) - 1.0;
          if (1.0 > numA) {
            loop_ub = 0;
          } else {
            loop_ub = (int)numA;
          }

          if (b_i1 > b_i2) {
            i1 = 1;
          } else {
            i1 = (int)b_i1;
          }

          for (i2 = 0; i2 < loop_ub; i2++) {
            links[(i1 + i2) - 1] = NodesOnLink->data[i2 + 4 * locFA];
          }

          b_i1 = b_i2 + 1.0;
        }

        if (1.0 > b_i2) {
          loop_ub = 0;
        } else {
          loop_ub = (int)b_i2;
        }

        nodes_size[0] = loop_ub;
        if (0 <= loop_ub - 1) {
          memcpy(&nodes_data[0], &links[0], loop_ub * sizeof(double));
        }

        c_nodes_data.data = &nodes_data[0];
        c_nodes_data.size = &nodes_size[0];
        c_nodes_data.allocatedSize = 20;
        c_nodes_data.numDimensions = 1;
        c_nodes_data.canFreeData = false;
        unique_vector(&c_nodes_data, setnPBC);
        loop_ub = setnPBC->size[0];
        elemB = setnPBC->size[0] - 1;
        trueCount = 0;
        for (notdone = 0; notdone < loop_ub; notdone++) {
          links2_data[notdone] = setnPBC->data[notdone];
          if (setnPBC->data[notdone] != 0.0) {
            trueCount++;
          }
        }

        locFA = 0;
        for (notdone = 0; notdone <= elemB; notdone++) {
          if (links2_data[notdone] != 0.0) {
            links2_data[locFA] = links2_data[notdone];
            locFA++;
          }
        }

        for (i1 = 0; i1 < numPBC; i1++) {
          for (i2 = 0; i2 < trueCount; i2++) {
            NodesOnLink->data[i2 + 4 * ((int)nodes[i1] - 1)] = links2_data[i2];
          }
        }

        if (1 > numPBC) {
          loop_ub = 0;
        } else {
          loop_ub = numPBC;
        }

        for (i1 = 0; i1 < loop_ub; i1++) {
          ia_data[i1] = (int)nodes[i1];
        }

        for (i1 = 0; i1 < loop_ub; i1++) {
          NodesOnLinknum->data[ia_data[i1] - 1] = (signed char)trueCount;
        }
      }
    }

    i = MorePBC->size[0];
    MorePBC->size[0] = NodesOnPBCnum->size[0];
    emxEnsureCapacity_real_T(MorePBC, i);
    loop_ub = NodesOnPBCnum->size[0];
    for (i = 0; i < loop_ub; i++) {
      MorePBC->data[i] = NodesOnPBCnum->data[i];
    }

    notdone = NodesOnPBCnum->size[0];
    i = NodesOnPBCnum->size[0];
    NodesOnPBCnum->size[0] = MorePBC->size[0];
    emxEnsureCapacity_real_T(NodesOnPBCnum, i);
    for (numPBC = 0; numPBC < notdone; numPBC++) {
      NodesOnPBCnum->data[numPBC] = fabs(MorePBC->data[numPBC]);
    }
  } else {
    i = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1];
    NodesOnElementPBC->size[0] = 1;
    NodesOnElementPBC->size[1] = 1;
    emxEnsureCapacity_real_T(NodesOnElementPBC, i);
    NodesOnElementPBC->data[0] = 0.0;

    /*  RegionOnElement = RegionOnElement; */
    NodesOnPBC->size[0] = 4;
    NodesOnPBC->size[1] = 0;

    /*  nodes that are paired up */
    NodesOnPBCnum->size[0] = 0;

    /*  number of nodes with common pairing */
    NodesOnLink->size[0] = 4;
    NodesOnLink->size[1] = 0;

    /*  IDs of PBC referring to node */
    NodesOnLinknum->size[0] = 0;

    /*  number of PBC referring to node */
  }

  /*  Mesh error checks */
  /*  Error checks for mesh arrays */
  /*  Nodes */
  /*  Elements */
  /*  Check that interfaces are assigned also when intraface couplers are requested */
  /*  reset to logical */
  b_eml_find(InterTypes, t6_rowidx);
  loop_ub = t6_rowidx->size[0] - 1;
  for (i = 0; i <= loop_ub; i++) {
    InterTypes->data[t6_rowidx->data[i] - 1] = 1.0;
  }

  d = rt_roundd_snf(nummat);
  if (d < 9.2233720368547758E+18) {
    if (d >= -9.2233720368547758E+18) {
      q0 = (long long)d;
    } else {
      q0 = MIN_int64_T;
    }
  } else if (d >= 9.2233720368547758E+18) {
    q0 = MAX_int64_T;
  } else {
    q0 = 0LL;
  }

  if (q0 < -9223372036854775807LL) {
    qY = MIN_int64_T;
  } else {
    qY = q0 - 1LL;
  }

  mat = 1LL;
  emxInit_int64_T(&inter, 1);
  emxInit_real_T(&setAll, 2);
  emxInit_int32_T(&r, 2);
  emxInit_real_T(&t4_d, 1);
  while (mat <= qY) {
    if (InterTypes->data[((int)mat + InterTypes->size[0] * ((int)mat - 1)) - 1]
        != 0.0) {
      if (mat > 9223372036854775806LL) {
        b_qY = MAX_int64_T;
      } else {
        b_qY = mat + 1LL;
      }

      if (b_qY > q0) {
        i = 0;
        i1 = 0;
      } else {
        i = (int)b_qY - 1;
        i1 = (int)q0;
      }

      loop_ub = i1 - i;
      i1 = t4_d->size[0];
      t4_d->size[0] = loop_ub;
      emxEnsureCapacity_real_T(t4_d, i1);
      for (i1 = 0; i1 < loop_ub; i1++) {
        t4_d->data[i1] = InterTypes->data[(i + i1) + InterTypes->size[0] * ((int)
          mat - 1)] - 1.0;
      }

      c_eml_find(t4_d, t6_rowidx);
      i = inter->size[0];
      inter->size[0] = t6_rowidx->size[0];
      emxEnsureCapacity_int64_T(inter, i);
      loop_ub = t6_rowidx->size[0];
      for (i = 0; i < loop_ub; i++) {
        inter->data[i] = t6_rowidx->data[i];
      }

      if (inter->size[0] != 0) {
        printf("intraface: %lli; additional interface flags have been added\n",
               mat);
        fflush(stdout);
        i = t6_rowidx->size[0];
        t6_rowidx->size[0] = inter->size[0];
        emxEnsureCapacity_int32_T(t6_rowidx, i);
        loop_ub = inter->size[0];
        for (i = 0; i < loop_ub; i++) {
          b_qY = inter->data[i];
          if ((b_qY < 0LL) && (mat < MIN_int64_T - b_qY)) {
            b_qY = MIN_int64_T;
          } else if ((b_qY > 0LL) && (mat > MAX_int64_T - b_qY)) {
            b_qY = MAX_int64_T;
          } else {
            b_qY += mat;
          }

          t6_rowidx->data[i] = (int)b_qY;
        }

        loop_ub = t6_rowidx->size[0];
        for (i = 0; i < loop_ub; i++) {
          InterTypes->data[(t6_rowidx->data[i] + InterTypes->size[0] * ((int)mat
            - 1)) - 1] = 1.0;
        }
      }

      if (mat < -9223372036854775807LL) {
        b_qY = MIN_int64_T;
      } else {
        b_qY = mat - 1LL;
      }

      if (1LL > b_qY) {
        loop_ub = 0;
      } else {
        loop_ub = (int)b_qY;
      }

      i = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      setAll->size[1] = loop_ub;
      emxEnsureCapacity_real_T(setAll, i);
      for (i = 0; i < loop_ub; i++) {
        setAll->data[i] = InterTypes->data[((int)mat + InterTypes->size[0] * i)
          - 1] - 1.0;
      }

      d_eml_find(setAll, r);
      i = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      setAll->size[1] = r->size[1];
      emxEnsureCapacity_real_T(setAll, i);
      loop_ub = r->size[0] * r->size[1];
      for (i = 0; i < loop_ub; i++) {
        setAll->data[i] = r->data[i];
      }

      if (setAll->size[1] != 0) {
        printf("intraface: %lli; additional interface flags have been added\n",
               mat);
        fflush(stdout);
        i = t6_rowidx->size[0];
        t6_rowidx->size[0] = setAll->size[1];
        emxEnsureCapacity_int32_T(t6_rowidx, i);
        loop_ub = setAll->size[1];
        for (i = 0; i < loop_ub; i++) {
          t6_rowidx->data[i] = (int)setAll->data[i];
        }

        loop_ub = t6_rowidx->size[0];
        for (i = 0; i < loop_ub; i++) {
          InterTypes->data[((int)mat + InterTypes->size[0] * (t6_rowidx->data[i]
            - 1)) - 1] = 1.0;
        }
      }
    }

    if (mat < -9223372036854775807LL) {
      b_qY = MIN_int64_T;
    } else {
      b_qY = mat - 1LL;
    }

    if (1LL > b_qY) {
      loop_ub = 0;
    } else {
      loop_ub = (int)b_qY;
    }

    i = t4_d->size[0];
    t4_d->size[0] = loop_ub;
    emxEnsureCapacity_real_T(t4_d, i);
    for (i = 0; i < loop_ub; i++) {
      t4_d->data[i] = InterTypes->data[i + InterTypes->size[0] * ((int)mat - 1)];
    }

    c_eml_find(t4_d, indx);
    if (indx->size[0] != 0) {
      printf("Warning: some entries found in the upper triangle of Intertypes(:,%lli)\n",
             mat);
      fflush(stdout);
    }

    mat++;
  }

  emxFree_int32_T(&r);
  emxFree_int64_T(&inter);

  /*  Form ElementsOnNode, ElementsOnNodeNum, DG nodes and connectivity */
  emxInit_real_T(&ElementsOnNodePBCRow, 2);
  emxInit_real_T(&ElementsOnNodeRow, 2);
  emxInit_real_T(&setPBC, 2);
  emxInitStruct_sparse1(&NodeRegSum);
  emxInit_int32_T(&r1, 1);
  emxInit_int32_T(&ii, 1);
  emxInit_int32_T(&jj, 1);
  emxInitStruct_sparse(&expl_temp);
  emxInit_real_T(&NodeRegInd, 2);
  if (usePBC != 0.0) {
    emxInit_real_T(&c_NodeRegInd, 2);

    /*  also form the zipped mesh at the same time */
    i = c_NodeRegInd->size[0] * c_NodeRegInd->size[1];
    c_NodeRegInd->size[0] = 3;
    i1 = (int)(numel * 8.0);
    c_NodeRegInd->size[1] = i1;
    emxEnsureCapacity_real_T(c_NodeRegInd, i);
    loop_ub = 3 * i1;
    for (i = 0; i < loop_ub; i++) {
      c_NodeRegInd->data[i] = 0.0;
    }

    /*  node, region attached, regular=1 or PBC=-1 */
    i = ElementsOnNodePBCRow->size[0] * ElementsOnNodePBCRow->size[1];
    ElementsOnNodePBCRow->size[0] = (int)numel;
    ElementsOnNodePBCRow->size[1] = (int)nen;
    emxEnsureCapacity_real_T(ElementsOnNodePBCRow, i);
    notdone = (int)numel * (int)nen;
    for (i = 0; i < notdone; i++) {
      ElementsOnNodePBCRow->data[i] = 0.0;
    }

    i = ElementsOnNodeRow->size[0] * ElementsOnNodeRow->size[1];
    ElementsOnNodeRow->size[0] = (int)numel;
    ElementsOnNodeRow->size[1] = (int)nen;
    emxEnsureCapacity_real_T(ElementsOnNodeRow, i);
    for (i = 0; i < notdone; i++) {
      ElementsOnNodeRow->data[i] = 0.0;
    }

    nri = 0U;
    nextra = 0.0;
    i = (int)numel;
    if (0 <= i - 1) {
      if (1.0 > nen) {
        nume = 0;
      } else {
        nume = (int)nen;
      }

      nodeC2_size[0] = 1;
      nodeC2_size[1] = nume;
    }

    for (elem = 0; elem < i; elem++) {
      for (i1 = 0; i1 < nume; i1++) {
        nodeC2_data[i1] = NodesOnElementPBC->data[elem + NodesOnElementPBC->
          size[0] * i1];
      }

      nel = intnnz(nodeC2_data, nodeC2_size);
      b_i2 = RegionOnElement->data[elem];
      for (locN = 0; locN < nel; locN++) {
        /*  Loop over local Nodes */
        node = NodesOnElementPBC->data[elem + NodesOnElementPBC->size[0] * locN];

        /*    Add element to star list, increment number of elem in star */
        numA = ElementsOnNodePBCNum->data[(int)node - 1] + 1.0;
        ElementsOnNodePBCRow->data[elem + ElementsOnNodePBCRow->size[0] * locN] =
          numA;
        ElementsOnNodePBCNum->data[(int)node - 1] = numA;

        /*              ElementsOnNodePBC(locE,node) = elem; */
        /*              ElementsOnNodePBCDup(locE,node) = node; */
        if (NodesOnPBCnum->data[(int)node - 1] > 0.0) {
          b_node[0] = node;
          b_node[1] = NodesOnPBC->data[4 * ((int)node - 1)];
          b_i1 = minimum(b_node);

          /*  use smallest node number as the zipped node */
          nri++;
          i1 = 3 * ((int)nri - 1);
          c_NodeRegInd->data[i1] = b_i1;
          c_NodeRegInd->data[i1 + 1] = b_i2;
          c_NodeRegInd->data[i1 + 2] = 1.0;

          /*  flag that node is in current material set */
          if (node != b_i1) {
            nri++;

            /*  also add for original edge */
            i1 = 3 * ((int)nri - 1);
            c_NodeRegInd->data[i1] = node;
            c_NodeRegInd->data[i1 + 1] = b_i2;
            c_NodeRegInd->data[i1 + 2] = -1.0;

            /*  flag that node is in current material set */
          }

          NodesOnElementCG->data[elem + NodesOnElementCG->size[0] * locN] = b_i1;

          /*    Add element to star list, increment number of elem in star */
          numA = ElementsOnNodeNum->data[(int)b_i1 - 1] + 1.0;
          ElementsOnNodeRow->data[elem + ElementsOnNodeRow->size[0] * locN] =
            numA;
          ElementsOnNodeNum->data[(int)b_i1 - 1] = numA;

          /*                  ElementsOnNode(locE,nodePBC) = elem; */
          /*                  ElementsOnNodeDup(locE,nodePBC) = nodePBC; */
        } else {
          nri++;
          i1 = 3 * ((int)nri - 1);
          c_NodeRegInd->data[i1] = node;
          c_NodeRegInd->data[i1 + 1] = b_i2;
          c_NodeRegInd->data[i1 + 2] = 1.0;

          /*  flag that node is in current material set */
          /*    Add element to star list, increment number of elem in star */
          numA = ElementsOnNodeNum->data[(int)node - 1] + 1.0;
          ElementsOnNodeRow->data[elem + ElementsOnNodeRow->size[0] * locN] =
            numA;
          ElementsOnNodeNum->data[(int)node - 1] = numA;

          /*                  ElementsOnNode(locE,nodePBC) = elem; */
          /*                  ElementsOnNodeDup(locE,nodePBC) = nodePBC; */
        }
      }

      i1 = (int)(nen + (1.0 - ((double)nel + 1.0)));
      for (locN = 0; locN < i1; locN++) {
        numA = ((double)nel + 1.0) + (double)locN;
        nextra++;
        ElementsOnNodeRow->data[elem + ElementsOnNodeRow->size[0] * ((int)numA -
          1)] = nextra;
        ElementsOnNodePBCRow->data[elem + ElementsOnNodePBCRow->size[0] * ((int)
          numA - 1)] = nextra;
      }
    }

    elemB = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1] - 1;
    trueCount = 0;
    for (b_i = 0; b_i <= elemB; b_i++) {
      if (NodesOnElementPBC->data[b_i] == 0.0) {
        trueCount++;
      }
    }

    i = jj->size[0];
    jj->size[0] = trueCount;
    emxEnsureCapacity_int32_T(jj, i);
    locFA = 0;
    for (b_i = 0; b_i <= elemB; b_i++) {
      if (NodesOnElementPBC->data[b_i] == 0.0) {
        jj->data[locFA] = b_i + 1;
        locFA++;
      }
    }

    loop_ub = jj->size[0] - 1;
    for (i = 0; i <= loop_ub; i++) {
      NodesOnElementPBC->data[jj->data[i] - 1] = *numnp + 1.0;
    }

    /* move empty numbers to end of array */
    elemB = NodesOnElementCG->size[0] * NodesOnElementCG->size[1] - 1;
    trueCount = 0;
    for (b_i = 0; b_i <= elemB; b_i++) {
      if (NodesOnElementCG->data[b_i] == 0.0) {
        trueCount++;
      }
    }

    i = r1->size[0];
    r1->size[0] = trueCount;
    emxEnsureCapacity_int32_T(r1, i);
    locFA = 0;
    for (b_i = 0; b_i <= elemB; b_i++) {
      if (NodesOnElementCG->data[b_i] == 0.0) {
        r1->data[locFA] = b_i + 1;
        locFA++;
      }
    }

    loop_ub = r1->size[0] - 1;
    for (i = 0; i <= loop_ub; i++) {
      NodesOnElementCG->data[r1->data[i] - 1] = *numnp + 1.0;
    }

    /* move empty numbers to end of array */
    if (numel < 1.0) {
      setAll->size[0] = 1;
      setAll->size[1] = 0;
    } else {
      i = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      setAll->size[1] = (int)floor(numel - 1.0) + 1;
      emxEnsureCapacity_real_T(setAll, i);
      loop_ub = (int)floor(numel - 1.0);
      for (i = 0; i <= loop_ub; i++) {
        setAll->data[i] = (double)i + 1.0;
      }
    }

    i = MorePBC->size[0];
    MorePBC->size[0] = setAll->size[1];
    emxEnsureCapacity_real_T(MorePBC, i);
    loop_ub = setAll->size[1];
    for (i = 0; i < loop_ub; i++) {
      MorePBC->data[i] = setAll->data[i];
    }

    emxInit_real_T(&b_MorePBC, 2);
    i = b_MorePBC->size[0] * b_MorePBC->size[1];
    b_MorePBC->size[0] = MorePBC->size[0];
    b_MorePBC->size[1] = (int)nen;
    emxEnsureCapacity_real_T(b_MorePBC, i);
    loop_ub = (int)nen;
    for (i = 0; i < loop_ub; i++) {
      nume = MorePBC->size[0];
      for (i1 = 0; i1 < nume; i1++) {
        b_MorePBC->data[i1 + b_MorePBC->size[0] * i] = MorePBC->data[i1];
      }
    }

    sparse(ElementsOnNodeRow, NodesOnElementCG, b_MorePBC, ElementsOnNode);

    /* zeros(maxel,numnp); % Array of elements connected to nodes */
    sparse(ElementsOnNodeRow, NodesOnElementCG, NodesOnElementCG,
           ElementsOnNodeDup);

    /*  actual node ID given to element for an original node; general counterpart of NodeMat */
    i = b_MorePBC->size[0] * b_MorePBC->size[1];
    b_MorePBC->size[0] = MorePBC->size[0];
    b_MorePBC->size[1] = (int)nen;
    emxEnsureCapacity_real_T(b_MorePBC, i);
    loop_ub = (int)nen;
    for (i = 0; i < loop_ub; i++) {
      nume = MorePBC->size[0];
      for (i1 = 0; i1 < nume; i1++) {
        b_MorePBC->data[i1 + b_MorePBC->size[0] * i] = MorePBC->data[i1];
      }
    }

    emxInitStruct_sparse(&b_ElementsOnNodePBCRow);
    sparse(ElementsOnNodePBCRow, NodesOnElementPBC, b_MorePBC, &expl_temp);

    /* zeros(maxel,numnp); % Array of elements connected to nodes */
    sparse(ElementsOnNodePBCRow, NodesOnElementPBC, NodesOnElementPBC,
           &b_ElementsOnNodePBCRow);

    /*  actual node ID given to element for an original node; general counterpart of NodeMat */
    if (1 > (int)nri) {
      loop_ub = 0;
      nume = 0;
      numPBC = 0;
    } else {
      loop_ub = (int)nri;
      nume = (int)nri;
      numPBC = (int)nri;
    }

    i = setAll->size[0] * setAll->size[1];
    setAll->size[0] = 1;
    setAll->size[1] = loop_ub;
    emxEnsureCapacity_real_T(setAll, i);
    emxFreeStruct_sparse(&b_ElementsOnNodePBCRow);
    emxFree_real_T(&b_MorePBC);
    for (i = 0; i < loop_ub; i++) {
      setAll->data[i] = c_NodeRegInd->data[3 * i];
    }

    i = setPBC->size[0] * setPBC->size[1];
    setPBC->size[0] = 1;
    setPBC->size[1] = nume;
    emxEnsureCapacity_real_T(setPBC, i);
    for (i = 0; i < nume; i++) {
      setPBC->data[i] = c_NodeRegInd->data[3 * i + 1];
    }

    i = NodeRegInd->size[0] * NodeRegInd->size[1];
    NodeRegInd->size[0] = 1;
    NodeRegInd->size[1] = numPBC;
    emxEnsureCapacity_real_T(NodeRegInd, i);
    for (i = 0; i < numPBC; i++) {
      NodeRegInd->data[i] = c_NodeRegInd->data[3 * i + 2];
    }

    emxFree_real_T(&c_NodeRegInd);
    b_sparse(setAll, setPBC, NodeRegInd, NodeReg);

    /*  allow allocation of PBC nodes too, but with negatives so as not to be in facet counts */
    e_eml_find(NodeReg->colidx, NodeReg->rowidx, ii, jj);
    i = setnPBC->size[0];
    setnPBC->size[0] = ii->size[0];
    emxEnsureCapacity_real_T(setnPBC, i);
    loop_ub = ii->size[0];
    for (i = 0; i < loop_ub; i++) {
      setnPBC->data[i] = ii->data[i];
    }

    i = MorePBC->size[0];
    MorePBC->size[0] = jj->size[0];
    emxEnsureCapacity_real_T(MorePBC, i);
    loop_ub = jj->size[0];
    for (i = 0; i < loop_ub; i++) {
      MorePBC->data[i] = jj->data[i];
    }

    /*  sparse adds together entries when duplicated, so reduce back to original magnitude */
    numPBC = (int)maximum(setnPBC);
    i = MorePBC->size[0];
    MorePBC->size[0] = setnPBC->size[0];
    emxEnsureCapacity_real_T(MorePBC, i);
    loop_ub = setnPBC->size[0];
    for (i = 0; i < loop_ub; i++) {
      MorePBC->data[i] = (int)setnPBC->data[i] + numPBC * ((int)MorePBC->data[i]
        - 1);
    }

    sparse_parenReference(NodeReg->d, NodeReg->colidx, NodeReg->rowidx,
                          NodeReg->m, MorePBC, NodeRegSum.d, NodeRegSum.colidx,
                          NodeRegSum.rowidx, &NodeRegSum.m);
    b_sign(&NodeRegSum);
    sparse_times(setnPBC, NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                 NodeRegSum.m, t4_d, ii, jj, &notdone);
    sparse_parenAssign(NodeReg, t4_d, ii, jj, notdone, MorePBC);

    /* keep the negative sign for PBC nodes to ignore below */
    elemB = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1] - 1;
    trueCount = 0;
    for (b_i = 0; b_i <= elemB; b_i++) {
      if (NodesOnElementPBC->data[b_i] == *numnp + 1.0) {
        trueCount++;
      }
    }

    emxInit_int32_T(&r2, 1);
    i = r2->size[0];
    r2->size[0] = trueCount;
    emxEnsureCapacity_int32_T(r2, i);
    locFA = 0;
    for (b_i = 0; b_i <= elemB; b_i++) {
      if (NodesOnElementPBC->data[b_i] == *numnp + 1.0) {
        r2->data[locFA] = b_i + 1;
        locFA++;
      }
    }

    loop_ub = r2->size[0] - 1;
    for (i = 0; i <= loop_ub; i++) {
      NodesOnElementPBC->data[r2->data[i] - 1] = 0.0;
    }

    emxFree_int32_T(&r2);

    /* move empty numbers to end of array */
    elemB = NodesOnElementCG->size[0] * NodesOnElementCG->size[1] - 1;
    trueCount = 0;
    for (b_i = 0; b_i <= elemB; b_i++) {
      if (NodesOnElementCG->data[b_i] == *numnp + 1.0) {
        trueCount++;
      }
    }

    emxInit_int32_T(&r3, 1);
    i = r3->size[0];
    r3->size[0] = trueCount;
    emxEnsureCapacity_int32_T(r3, i);
    locFA = 0;
    for (b_i = 0; b_i <= elemB; b_i++) {
      if (NodesOnElementCG->data[b_i] == *numnp + 1.0) {
        r3->data[locFA] = b_i + 1;
        locFA++;
      }
    }

    loop_ub = r3->size[0] - 1;
    for (i = 0; i <= loop_ub; i++) {
      NodesOnElementCG->data[r3->data[i] - 1] = 0.0;
    }

    emxFree_int32_T(&r3);

    /* move empty numbers to end of array */
    /*      % get memory back */
    /*      clear NodeRegInd */
    /*      clear ElementsOnNodeRow */
    /*      clear ElementsOnNodePBCRow */
  } else {
    emxInit_real_T(&b_NodeRegInd, 2);
    i = b_NodeRegInd->size[0] * b_NodeRegInd->size[1];
    b_NodeRegInd->size[0] = 2;
    i1 = (int)(numel * 8.0);
    b_NodeRegInd->size[1] = i1;
    emxEnsureCapacity_real_T(b_NodeRegInd, i);
    loop_ub = i1 << 1;
    for (i = 0; i < loop_ub; i++) {
      b_NodeRegInd->data[i] = 0.0;
    }

    i = ElementsOnNodeRow->size[0] * ElementsOnNodeRow->size[1];
    ElementsOnNodeRow->size[0] = (int)numel;
    ElementsOnNodeRow->size[1] = (int)nen;
    emxEnsureCapacity_real_T(ElementsOnNodeRow, i);
    notdone = (int)numel * (int)nen;
    for (i = 0; i < notdone; i++) {
      ElementsOnNodeRow->data[i] = 0.0;
    }

    /*      ElementsOnNodeCol = zeros(nen,numel); */
    nri = 0U;
    nextra = 0.0;
    i = (int)numel;
    if (0 <= i - 1) {
      if (1.0 > nen) {
        nume = 0;
      } else {
        nume = (int)nen;
      }

      nodeC2_size[0] = 1;
      nodeC2_size[1] = nume;
    }

    for (elem = 0; elem < i; elem++) {
      for (i1 = 0; i1 < nume; i1++) {
        nodeC2_data[i1] = NodesOnElement->data[elem + NodesOnElement->size[0] *
          i1];
      }

      nel = intnnz(nodeC2_data, nodeC2_size);
      for (locN = 0; locN < nel; locN++) {
        /*  Loop over local Nodes */
        node = NodesOnElement->data[elem + NodesOnElement->size[0] * locN];
        nri++;
        i1 = 2 * ((int)nri - 1);
        b_NodeRegInd->data[i1] = node;
        b_NodeRegInd->data[i1 + 1] = RegionOnElement->data[elem];

        /*              NodeReg(node,reg) = node; % flag that node is in current material set */
        /*    Add element to star list, increment number of elem in star */
        numA = ElementsOnNodeNum->data[(int)node - 1] + 1.0;
        ElementsOnNodeRow->data[elem + ElementsOnNodeRow->size[0] * locN] = numA;

        /*              ElementsOnNode(locE,node) = elem; */
        ElementsOnNodeNum->data[(int)node - 1] = numA;

        /*              ElementsOnNodeCol(locN,elem) = node; */
        /*              ElementsOnNodeDup(locE,node) = node; */
      }

      i1 = (int)(nen + (1.0 - ((double)nel + 1.0)));
      for (locN = 0; locN < i1; locN++) {
        nextra++;
        ElementsOnNodeRow->data[elem + ElementsOnNodeRow->size[0] * ((int)
          (((double)nel + 1.0) + (double)locN) - 1)] = nextra;
      }
    }

    elemB = NodesOnElement->size[0] * NodesOnElement->size[1] - 1;
    trueCount = 0;
    for (b_i = 0; b_i <= elemB; b_i++) {
      if (NodesOnElement->data[b_i] == 0.0) {
        trueCount++;
      }
    }

    i = jj->size[0];
    jj->size[0] = trueCount;
    emxEnsureCapacity_int32_T(jj, i);
    locFA = 0;
    for (b_i = 0; b_i <= elemB; b_i++) {
      if (NodesOnElement->data[b_i] == 0.0) {
        jj->data[locFA] = b_i + 1;
        locFA++;
      }
    }

    loop_ub = jj->size[0];
    for (i = 0; i < loop_ub; i++) {
      NodesOnElement->data[jj->data[i] - 1] = *numnp + 1.0;
    }

    /* move empty numbers to end of array */
    if (numel < 1.0) {
      setAll->size[0] = 1;
      setAll->size[1] = 0;
    } else {
      i = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      loop_ub = (int)floor(numel - 1.0);
      setAll->size[1] = loop_ub + 1;
      emxEnsureCapacity_real_T(setAll, i);
      for (i = 0; i <= loop_ub; i++) {
        setAll->data[i] = (double)i + 1.0;
      }
    }

    i = ElementsOnNodePBCRow->size[0] * ElementsOnNodePBCRow->size[1];
    ElementsOnNodePBCRow->size[0] = setAll->size[1];
    ElementsOnNodePBCRow->size[1] = (int)nen;
    emxEnsureCapacity_real_T(ElementsOnNodePBCRow, i);
    loop_ub = (int)nen;
    for (i = 0; i < loop_ub; i++) {
      nume = setAll->size[1];
      for (i1 = 0; i1 < nume; i1++) {
        ElementsOnNodePBCRow->data[i1 + ElementsOnNodePBCRow->size[0] * i] =
          setAll->data[i1];
      }
    }

    sparse(ElementsOnNodeRow, NodesOnElement, ElementsOnNodePBCRow,
           ElementsOnNode);

    /* zeros(maxel,numnp); % Array of elements connected to nodes */
    sparse(ElementsOnNodeRow, NodesOnElement, NodesOnElement, ElementsOnNodeDup);

    /*  actual node ID given to element for an original node; general counterpart of NodeMat */
    if (1 > (int)nri) {
      loop_ub = 0;
      nume = 0;
      numPBC = 0;
    } else {
      loop_ub = (int)nri;
      nume = (int)nri;
      numPBC = (int)nri;
    }

    i = setAll->size[0] * setAll->size[1];
    setAll->size[0] = 1;
    setAll->size[1] = loop_ub;
    emxEnsureCapacity_real_T(setAll, i);
    for (i = 0; i < loop_ub; i++) {
      setAll->data[i] = b_NodeRegInd->data[2 * i];
    }

    i = setPBC->size[0] * setPBC->size[1];
    setPBC->size[0] = 1;
    setPBC->size[1] = nume;
    emxEnsureCapacity_real_T(setPBC, i);
    for (i = 0; i < nume; i++) {
      setPBC->data[i] = b_NodeRegInd->data[2 * i + 1];
    }

    i = NodeRegInd->size[0] * NodeRegInd->size[1];
    NodeRegInd->size[0] = 1;
    NodeRegInd->size[1] = numPBC;
    emxEnsureCapacity_real_T(NodeRegInd, i);
    for (i = 0; i < numPBC; i++) {
      NodeRegInd->data[i] = b_NodeRegInd->data[2 * i];
    }

    emxFree_real_T(&b_NodeRegInd);
    b_sparse(setAll, setPBC, NodeRegInd, NodeReg);
    e_eml_find(NodeReg->colidx, NodeReg->rowidx, ii, jj);
    i = setnPBC->size[0];
    setnPBC->size[0] = ii->size[0];
    emxEnsureCapacity_real_T(setnPBC, i);
    loop_ub = ii->size[0];
    for (i = 0; i < loop_ub; i++) {
      setnPBC->data[i] = ii->data[i];
    }

    i = MorePBC->size[0];
    MorePBC->size[0] = jj->size[0];
    emxEnsureCapacity_real_T(MorePBC, i);
    loop_ub = jj->size[0];
    for (i = 0; i < loop_ub; i++) {
      MorePBC->data[i] = jj->data[i];
    }

    numPBC = (int)*numnp;
    i = MorePBC->size[0];
    MorePBC->size[0] = setnPBC->size[0];
    emxEnsureCapacity_real_T(MorePBC, i);
    loop_ub = setnPBC->size[0];
    for (i = 0; i < loop_ub; i++) {
      MorePBC->data[i] = (int)setnPBC->data[i] + numPBC * ((int)MorePBC->data[i]
        - 1);
    }

    b_sparse_parenAssign(NodeReg, setnPBC, MorePBC);
    elemB = NodesOnElement->size[0] * NodesOnElement->size[1] - 1;
    trueCount = 0;
    for (b_i = 0; b_i <= elemB; b_i++) {
      if (NodesOnElement->data[b_i] == *numnp + 1.0) {
        trueCount++;
      }
    }

    i = r1->size[0];
    r1->size[0] = trueCount;
    emxEnsureCapacity_int32_T(r1, i);
    locFA = 0;
    for (b_i = 0; b_i <= elemB; b_i++) {
      if (NodesOnElement->data[b_i] == *numnp + 1.0) {
        r1->data[locFA] = b_i + 1;
        locFA++;
      }
    }

    loop_ub = r1->size[0];
    for (i = 0; i < loop_ub; i++) {
      NodesOnElement->data[r1->data[i] - 1] = 0.0;
    }

    /* move empty numbers to end of array */
    i = ElementsOnNodePBCRow->size[0] * ElementsOnNodePBCRow->size[1];
    ElementsOnNodePBCRow->size[0] = (int)numel;
    ElementsOnNodePBCRow->size[1] = (int)nen;
    emxEnsureCapacity_real_T(ElementsOnNodePBCRow, i);
    for (i = 0; i < notdone; i++) {
      ElementsOnNodePBCRow->data[i] = 0.0;
    }

    c_sparse(ElementsOnNodePBCRow, expl_temp.d, expl_temp.colidx,
             expl_temp.rowidx, &locFA, &notdone, &numPBC);

    /*      % get memory back */
    /*      clear NodeRegInd */
    /*      clear ElementsOnNodeRow */
  }

  emxFree_int32_T(&r1);
  emxFree_real_T(&ElementsOnNodeRow);
  emxFree_real_T(&ElementsOnNodePBCRow);
  i = NodesOnElementDG->size[0] * NodesOnElementDG->size[1];
  NodesOnElementDG->size[0] = NodesOnElementCG->size[0];
  NodesOnElementDG->size[1] = NodesOnElementCG->size[1];
  emxEnsureCapacity_real_T(NodesOnElementDG, i);
  loop_ub = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
  for (i = 0; i < loop_ub; i++) {
    NodesOnElementDG->data[i] = NodesOnElementCG->data[i];
  }

  if (1.0 > *numnp) {
    loop_ub = 0;
  } else {
    loop_ub = (int)*numnp;
  }

  for (i = 0; i < loop_ub; i++) {
    Coordinates3->data[i] = Coordinates->data[i];
  }

  for (i = 0; i < loop_ub; i++) {
    Coordinates3->data[i + Coordinates3->size[0]] = Coordinates->data[i +
      Coordinates->size[0]];
  }

  *numinttype = nummat * (nummat + 1.0) / 2.0;
  i = numEonF->size[0];
  numEonF->size[0] = (int)*numinttype;
  emxEnsureCapacity_real_T(numEonF, i);
  loop_ub = (int)*numinttype;
  for (i = 0; i < loop_ub; i++) {
    numEonF->data[i] = 0.0;
  }

  *numEonB = 0.0;
  loop_ub = (int)(4.0 * numel);
  i = ElementsOnFacet->size[0] * ElementsOnFacet->size[1];
  ElementsOnFacet->size[0] = loop_ub;
  ElementsOnFacet->size[1] = 4;
  emxEnsureCapacity_real_T(ElementsOnFacet, i);
  nume = loop_ub << 2;
  for (i = 0; i < nume; i++) {
    ElementsOnFacet->data[i] = 0.0;
  }

  i = FacetsOnInterface->size[0];
  FacetsOnInterface->size[0] = loop_ub;
  emxEnsureCapacity_real_T(FacetsOnInterface, i);
  for (i = 0; i < loop_ub; i++) {
    FacetsOnInterface->data[i] = 0.0;
  }

  i = ElementsOnBoundary->size[0] * ElementsOnBoundary->size[1];
  ElementsOnBoundary->size[0] = loop_ub;
  ElementsOnBoundary->size[1] = 2;
  emxEnsureCapacity_real_T(ElementsOnBoundary, i);
  nume = loop_ub << 1;
  for (i = 0; i < nume; i++) {
    ElementsOnBoundary->data[i] = 0.0;
  }

  /*  All exposed faces */
  if (usePBC != 0.0) {
    i = FacetsOnPBC->size[0];
    FacetsOnPBC->size[0] = loop_ub;
    emxEnsureCapacity_real_T(FacetsOnPBC, i);
    for (i = 0; i < loop_ub; i++) {
      FacetsOnPBC->data[i] = 0.0;
    }

    /*  separate list for PBC facets that are found; grouped by interface type */
    i = numEonPBC->size[0];
    numEonPBC->size[0] = (int)*numinttype;
    emxEnsureCapacity_real_T(numEonPBC, i);
    loop_ub = (int)*numinttype;
    for (i = 0; i < loop_ub; i++) {
      numEonPBC->data[i] = 0.0;
    }

    numFPBC = 0.0;

    /*  record how many PBC facets are found */
  } else {
    i = FacetsOnPBC->size[0];
    FacetsOnPBC->size[0] = 4;
    emxEnsureCapacity_real_T(FacetsOnPBC, i);
    FacetsOnPBC->data[0] = 0.0;
    FacetsOnPBC->data[1] = 0.0;
    FacetsOnPBC->data[2] = 0.0;
    FacetsOnPBC->data[3] = 0.0;

    /*  separate list for PBC facets that are found; grouped by interface type */
    i = numEonPBC->size[0];
    numEonPBC->size[0] = 2;
    emxEnsureCapacity_real_T(numEonPBC, i);
    numEonPBC->data[0] = 0.0;
    numEonPBC->data[1] = 0.0;
    numFPBC = 0.0;

    /*  record how many PBC facets are found */
  }

  i = FacetsOnElement->size[0] * FacetsOnElement->size[1];
  FacetsOnElement->size[0] = (int)numel;
  FacetsOnElement->size[1] = 4;
  emxEnsureCapacity_real_T(FacetsOnElement, i);
  notdone = (int)numel << 2;
  for (i = 0; i < notdone; i++) {
    FacetsOnElement->data[i] = 0.0;
  }

  /*  each fac in mesh, gets assigned an fac ID based on its material (1:numSI for that material) */
  i = FacetsOnElementInt->size[0] * FacetsOnElementInt->size[1];
  FacetsOnElementInt->size[0] = (int)numel;
  FacetsOnElementInt->size[1] = 4;
  emxEnsureCapacity_real_T(FacetsOnElementInt, i);
  for (i = 0; i < notdone; i++) {
    FacetsOnElementInt->data[i] = 0.0;
  }

  /*  materialI ID for each fac in mesh, according to element */
  i = FacetsOnNodeNum->size[0];
  FacetsOnNodeNum->size[0] = (int)*numnp;
  emxEnsureCapacity_real_T(FacetsOnNodeNum, i);
  loop_ub = (int)*numnp;
  for (i = 0; i < loop_ub; i++) {
    FacetsOnNodeNum->data[i] = 0.0;
  }

  emxInit_real_T(&FacetsOnNodeInd, 2);

  /*  number of facs attached to a node. */
  i = FacetsOnNodeInd->size[0] * FacetsOnNodeInd->size[1];
  FacetsOnNodeInd->size[0] = 5;
  i1 = (int)(20.0 * numel);
  FacetsOnNodeInd->size[1] = i1;
  emxEnsureCapacity_real_T(FacetsOnNodeInd, i);
  loop_ub = 5 * i1;
  for (i = 0; i < loop_ub; i++) {
    FacetsOnNodeInd->data[i] = 0.0;
  }

  nri = 0U;
  nloop3[0] = 1;
  nloop3[3] = 2;
  nloop3[1] = 2;
  nloop3[4] = 3;
  nloop3[2] = 3;
  nloop3[5] = 1;
  nloop4[0] = 1;
  nloop4[4] = 2;
  nloop4[1] = 2;
  nloop4[5] = 3;
  nloop4[2] = 3;
  nloop4[6] = 4;
  nloop4[3] = 4;
  nloop4[7] = 1;
  *numfac = 0.0;

  /*  Find all facets in mesh */
  i = (int)numel;
  emxInit_real_T(&ElementsOnNodeA, 2);
  emxInit_boolean_T(&r4, 2);
  emxInit_int32_T(&r5, 1);
  emxInit_int32_T(&r6, 1);
  emxInit_real_T(&r7, 2);
  for (elem = 0; elem < i; elem++) {
    if (1.0 > nen) {
      loop_ub = 0;
    } else {
      loop_ub = (int)nen;
    }

    nodeC2_size[0] = 1;
    nodeC2_size[1] = loop_ub;
    for (i1 = 0; i1 < loop_ub; i1++) {
      nodeC2_data[i1] = NodesOnElementCG->data[elem + NodesOnElementCG->size[0] *
        i1];
    }

    nel = intnnz(nodeC2_data, nodeC2_size);
    if ((nel == 3) || (nel == 6)) {
      nume = 2;
    } else {
      nume = 3;
    }

    /*      Loop over facs of element */
    for (locF = 0; locF <= nume; locF++) {
      if (!(FacetsOnElement->data[elem + FacetsOnElement->size[0] * locF] != 0.0))
      {
        /*  reg1 is overwritten below, so it must be re-evaluated */
        if ((nel == 3) || (nel == 6)) {
          nodeA = NodesOnElementCG->data[elem + NodesOnElementCG->size[0] *
            (nloop3[locF] - 1)];
          nodeB = NodesOnElementCG->data[elem + NodesOnElementCG->size[0] *
            (nloop3[locF + 3] - 1)];
        } else {
          nodeA = NodesOnElementCG->data[elem + NodesOnElementCG->size[0] *
            (nloop4[locF] - 1)];
          nodeB = NodesOnElementCG->data[elem + NodesOnElementCG->size[0] *
            (nloop4[locF + 4] - 1)];
        }

        d = ElementsOnNodeNum->data[(int)nodeA - 1];
        if (d < 1.0) {
          setAll->size[0] = 1;
          setAll->size[1] = 0;
        } else if (rtIsInf(d) && (1.0 == d)) {
          i1 = setAll->size[0] * setAll->size[1];
          setAll->size[0] = 1;
          setAll->size[1] = 1;
          emxEnsureCapacity_real_T(setAll, i1);
          setAll->data[0] = rtNaN;
        } else {
          i1 = setAll->size[0] * setAll->size[1];
          setAll->size[0] = 1;
          loop_ub = (int)floor(d - 1.0);
          setAll->size[1] = loop_ub + 1;
          emxEnsureCapacity_real_T(setAll, i1);
          for (i1 = 0; i1 <= loop_ub; i1++) {
            setAll->data[i1] = (double)i1 + 1.0;
          }
        }

        b_sparse_parenReference(ElementsOnNode->d, ElementsOnNode->colidx,
          ElementsOnNode->rowidx, setAll, nodeA, NodeRegSum.d, NodeRegSum.colidx,
          NodeRegSum.rowidx, &NodeRegSum.m);
        b_sparse_full(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                      NodeRegSum.m, ElementsOnNodeA);
        d1 = ElementsOnNodeNum->data[(int)nodeB - 1];
        if (d1 < 1.0) {
          setAll->size[0] = 1;
          setAll->size[1] = 0;
        } else if (rtIsInf(d1) && (1.0 == d1)) {
          i1 = setAll->size[0] * setAll->size[1];
          setAll->size[0] = 1;
          setAll->size[1] = 1;
          emxEnsureCapacity_real_T(setAll, i1);
          setAll->data[0] = rtNaN;
        } else {
          i1 = setAll->size[0] * setAll->size[1];
          setAll->size[0] = 1;
          loop_ub = (int)floor(d1 - 1.0);
          setAll->size[1] = loop_ub + 1;
          emxEnsureCapacity_real_T(setAll, i1);
          for (i1 = 0; i1 <= loop_ub; i1++) {
            setAll->data[i1] = (double)i1 + 1.0;
          }
        }

        /*  Determine if fac is on domain interior or boundary */
        if ((d > 1.0) && (d1 > 1.0)) {
          /*  Clean and fast way to intersect the 3 sets of elements, using */
          /*  built-in Matlab functions; change ismembc to ismember if the */
          /*  function is not in the standard package */
          /*              if isOctave */
          b_sparse_parenReference(ElementsOnNode->d, ElementsOnNode->colidx,
            ElementsOnNode->rowidx, setAll, nodeB, NodeRegSum.d,
            NodeRegSum.colidx, NodeRegSum.rowidx, &NodeRegSum.m);
          b_sparse_full(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                        NodeRegSum.m, r7);
          b_local_ismember(ElementsOnNodeA, r7, r4);
          elemB = r4->size[0] - 1;
          trueCount = 0;
          for (b_i = 0; b_i <= elemB; b_i++) {
            if (r4->data[b_i]) {
              trueCount++;
            }
          }

          i1 = r5->size[0];
          r5->size[0] = trueCount;
          emxEnsureCapacity_int32_T(r5, i1);
          locFA = 0;
          for (b_i = 0; b_i <= elemB; b_i++) {
            if (r4->data[b_i]) {
              r5->data[locFA] = b_i + 1;
              locFA++;
            }
          }

          /*              else */
          /*              twoelem = ElementsOnNodeA(ismembc(ElementsOnNodeA,ElementsOnNodeB)); */
          /*              end */
          if (r5->size[0] == 2) {
            /*  element interface */
            facI1 = 1;
            elemA = ElementsOnNodeA->data[r5->data[0] - 1];
            if (elemA == (double)elem + 1.0) {
              elemA = ElementsOnNodeA->data[r5->data[1] - 1];
            }
          } else if (r5->size[0] <= 2) {
            /*  domain boundary */
            facI1 = 0;
            elemA = 0.0;
          } else {
            /*  likely a PBC degenerating case */
          }
        } else {
          /*  domain boundary */
          facI1 = 0;
          elemA = 0.0;
        }

        if (facI1 == 1) {
          /* element interface, add to SurfacesI */
          /*  Find which slots nodeA and nodeB occupy on elemA */
          b_i = 1;
          b_i1 = NodesOnElementCG->data[(int)elemA - 1];
          while ((b_i < 5) && (b_i1 != nodeA)) {
            b_i++;
            b_i1 = NodesOnElementCG->data[((int)elemA + NodesOnElementCG->size[0]
              * (b_i - 1)) - 1];
          }

          b_i1 = b_i;
          b_i = 1;
          numA = NodesOnElementCG->data[(int)elemA - 1];
          while ((b_i < 5) && (numA != nodeB)) {
            b_i++;
            numA = NodesOnElementCG->data[((int)elemA + NodesOnElementCG->size[0]
              * (b_i - 1)) - 1];
          }

          i1 = (int)b_i1 * b_i;
          if (i1 == 2) {
            locFA = 0;
          } else if (i1 == 6) {
            locFA = 1;
          } else if (i1 == 12) {
            locFA = 2;
          } else if (i1 == 3) {
            locFA = 2;
          } else {
            /*  nodeC*nodeD==4 */
            locFA = 3;
          }

          if (FacetsOnElement->data[((int)elemA + FacetsOnElement->size[0] *
               locFA) - 1] == 0.0) {
            /*  New fac, add to list */
            if (1.0 > nen) {
              loop_ub = 0;
            } else {
              loop_ub = (int)nen;
            }

            nodeC2_size[0] = 1;
            nodeC2_size[1] = loop_ub;
            for (i1 = 0; i1 < loop_ub; i1++) {
              nodeC2_data[i1] = NodesOnElementCG->data[((int)elemA +
                NodesOnElementCG->size[0] * i1) - 1];
            }

            notdone = intnnz(nodeC2_data, nodeC2_size);
            elemB = elem;
            numPBC = locF;
            nextra = RegionOnElement->data[(int)elemA - 1];
            if (nextra > RegionOnElement->data[elem]) {
              /* swap the order of L and R so that L is always larger material ID */
              b_i2 = RegionOnElement->data[elem];
              elemB = (int)elemA - 1;
              elemA = (double)elem + 1.0;
              numPBC = locFA;
              locFA = locF;
              notdone = nel;
            } else {
              b_i2 = nextra;
              nextra = RegionOnElement->data[elem];
            }

            node = nextra * (nextra - 1.0) / 2.0 + b_i2;

            /*  ID for material pair (row=mat2, col=mat1) */
            numSI_tmp = numEonF->data[(int)node - 1] + 1.0;
            (*numfac)++;
            numEonF->data[(int)node - 1] = numSI_tmp;
            FacetsOnElement->data[elemB + FacetsOnElement->size[0] * numPBC] =
              *numfac;
            FacetsOnElement->data[((int)elemA + FacetsOnElement->size[0] * locFA)
              - 1] = *numfac;
            FacetsOnElementInt->data[elemB + FacetsOnElementInt->size[0] *
              numPBC] = node;
            FacetsOnElementInt->data[((int)elemA + FacetsOnElementInt->size[0] *
              locFA) - 1] = node;
            if ((notdone == 3) || (notdone == 6)) {
              nodeA = NodesOnElementCG->data[((int)elemA +
                NodesOnElementCG->size[0] * (nloop3[locFA] - 1)) - 1];
              nodeB = NodesOnElementCG->data[((int)elemA +
                NodesOnElementCG->size[0] * (nloop3[locFA + 3] - 1)) - 1];
            } else {
              nodeA = NodesOnElementCG->data[((int)elemA +
                NodesOnElementCG->size[0] * (nloop4[locFA] - 1)) - 1];
              nodeB = NodesOnElementCG->data[((int)elemA +
                NodesOnElementCG->size[0] * (nloop4[locFA + 4] - 1)) - 1];
            }

            ElementsOnFacet->data[(int)*numfac - 1] = elemB + 1;

            /* elemL */
            ElementsOnFacet->data[((int)*numfac + ElementsOnFacet->size[0]) - 1]
              = elemA;

            /* elemR */
            ElementsOnFacet->data[((int)*numfac + ElementsOnFacet->size[0] * 2)
              - 1] = (double)numPBC + 1.0;

            /* facL */
            ElementsOnFacet->data[((int)*numfac + ElementsOnFacet->size[0] * 3)
              - 1] = (double)locFA + 1.0;

            /* facR */
            FacetsOnInterface->data[(int)*numfac - 1] = node;

            /*  Assign nodal fac pairs */
            numA = FacetsOnNodeNum->data[(int)nodeA - 1] + 1.0;
            FacetsOnNodeNum->data[(int)nodeA - 1] = numA;
            nri += 2U;
            i1 = 5 * ((int)nri - 2);
            FacetsOnNodeInd->data[i1] = numA;
            FacetsOnNodeInd->data[i1 + 1] = nodeA;
            FacetsOnNodeInd->data[i1 + 2] = *numfac;
            FacetsOnNodeInd->data[i1 + 3] = node;

            /*                  FacetsOnNode(facnumA,nodeA) = numfac; */
            /*                  FacetsOnNodeInt(facnumA,nodeA) = regI; */
            numA = FacetsOnNodeNum->data[(int)nodeB - 1] + 1.0;
            FacetsOnNodeNum->data[(int)nodeB - 1] = numA;
            i2 = 5 * ((int)nri - 1);
            FacetsOnNodeInd->data[i2] = numA;
            FacetsOnNodeInd->data[i2 + 1] = nodeB;
            FacetsOnNodeInd->data[i2 + 2] = *numfac;
            FacetsOnNodeInd->data[i2 + 3] = node;

            /*                  FacetsOnNode(facnumB,nodeB) = numfac; */
            /*                  FacetsOnNodeInt(facnumB,nodeB) = regI; */
            if (usePBC != 0.0) {
              /*  check if facet is on the PBC, if so then CUT it */
              if ((notdone == 3) || (notdone == 6)) {
                numA = NodesOnElementPBC->data[((int)elemA +
                  NodesOnElementPBC->size[0] * (nloop3[locFA] - 1)) - 1];
                b_i1 = NodesOnElementPBC->data[((int)elemA +
                  NodesOnElementPBC->size[0] * (nloop3[locFA + 3] - 1)) - 1];
              } else {
                numA = NodesOnElementPBC->data[((int)elemA +
                  NodesOnElementPBC->size[0] * (nloop4[locFA] - 1)) - 1];
                b_i1 = NodesOnElementPBC->data[((int)elemA +
                  NodesOnElementPBC->size[0] * (nloop4[locFA + 4] - 1)) - 1];
              }

              /*                      if (nelB==3||nelB==6) */
              loop_ub = NodesOnElementCG->size[1];
              NodesOnElementCG_size[0] = 1;
              NodesOnElementCG_size[1] = NodesOnElementCG->size[1];
              for (i3 = 0; i3 < loop_ub; i3++) {
                NodesOnElementCG_data[i3] = (NodesOnElementCG->data[elemB +
                  NodesOnElementCG->size[0] * i3] == nodeA);
              }

              f_eml_find(NodesOnElementCG_data, NodesOnElementCG_size, tmp_data,
                         tmp_size);
              nodeC2_size[0] = 1;
              loop_ub = tmp_size[1];
              nodeC2_size[1] = tmp_size[1];
              for (i3 = 0; i3 < loop_ub; i3++) {
                nodeC2_data[i3] = NodesOnElementPBC->data[elemB +
                  NodesOnElementPBC->size[0] * (tmp_data[i3] - 1)];
              }

              loop_ub = NodesOnElementCG->size[1];
              NodesOnElementCG_size[0] = 1;
              NodesOnElementCG_size[1] = NodesOnElementCG->size[1];
              for (i3 = 0; i3 < loop_ub; i3++) {
                NodesOnElementCG_data[i3] = (NodesOnElementCG->data[elemB +
                  NodesOnElementCG->size[0] * i3] == nodeB);
              }

              f_eml_find(NodesOnElementCG_data, NodesOnElementCG_size, tmp_data,
                         tmp_size);
              nodeD2_size[0] = 1;
              loop_ub = tmp_size[1];
              nodeD2_size[1] = tmp_size[1];
              for (i3 = 0; i3 < loop_ub; i3++) {
                nodeD2_data[i3] = NodesOnElementPBC->data[elemB +
                  NodesOnElementPBC->size[0] * (tmp_data[i3] - 1)];
              }

              /*  Condition: if the nodes opposite each other on the */
              /*  two elements adjoining the facet from the unzipped */
              /*  model (NodesOnElementPBC) are all different, then the */
              /*  two elements are on opposite sides of the domain and */
              /*  the facet needs to be cut */
              if ((!isequal(numA, nodeC2_data, nodeC2_size)) && (!isequal(b_i1,
                    nodeD2_data, nodeD2_size))) {
                /*  Make sure facet is NOT an interface in the */
                /*  unzipped mesh, because for triangles both nodes */
                /*  might be in PBCList but they are on different */
                /*  boundary surfaces */
                d = ElementsOnNodePBCNum->data[(int)numA - 1];
                if (d < 1.0) {
                  setAll->size[0] = 1;
                  setAll->size[1] = 0;
                } else if (rtIsInf(d) && (1.0 == d)) {
                  i3 = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  setAll->size[1] = 1;
                  emxEnsureCapacity_real_T(setAll, i3);
                  setAll->data[0] = rtNaN;
                } else {
                  i3 = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  loop_ub = (int)floor(d - 1.0);
                  setAll->size[1] = loop_ub + 1;
                  emxEnsureCapacity_real_T(setAll, i3);
                  for (i3 = 0; i3 <= loop_ub; i3++) {
                    setAll->data[i3] = (double)i3 + 1.0;
                  }
                }

                b_sparse_parenReference(expl_temp.d, expl_temp.colidx,
                  expl_temp.rowidx, setAll, numA, NodeRegSum.d,
                  NodeRegSum.colidx, NodeRegSum.rowidx, &NodeRegSum.m);
                b_sparse_full(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                              NodeRegSum.m, ElementsOnNodeA);
                d1 = ElementsOnNodePBCNum->data[(int)b_i1 - 1];
                if (d1 < 1.0) {
                  setAll->size[0] = 1;
                  setAll->size[1] = 0;
                } else if (rtIsInf(d1) && (1.0 == d1)) {
                  i3 = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  setAll->size[1] = 1;
                  emxEnsureCapacity_real_T(setAll, i3);
                  setAll->data[0] = rtNaN;
                } else {
                  i3 = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  loop_ub = (int)floor(d1 - 1.0);
                  setAll->size[1] = loop_ub + 1;
                  emxEnsureCapacity_real_T(setAll, i3);
                  for (i3 = 0; i3 <= loop_ub; i3++) {
                    setAll->data[i3] = (double)i3 + 1.0;
                  }
                }

                /*  Determine if fac is on domain interior or boundary */
                guard1 = false;
                if ((d > 1.0) && (d1 > 1.0)) {
                  /*  Clean and fast way to intersect the 3 sets of elements, using */
                  /*  built-in Matlab functions; change ismembc to ismember if the */
                  /*  function is not in the standard package */
                  /*                            if isOctave */
                  b_sparse_parenReference(expl_temp.d, expl_temp.colidx,
                    expl_temp.rowidx, setAll, b_i1, NodeRegSum.d,
                    NodeRegSum.colidx, NodeRegSum.rowidx, &NodeRegSum.m);
                  b_sparse_full(NodeRegSum.d, NodeRegSum.colidx,
                                NodeRegSum.rowidx, NodeRegSum.m, r7);
                  b_local_ismember(ElementsOnNodeA, r7, r4);
                  elemB = r4->size[0] - 1;
                  trueCount = 0;
                  for (b_i = 0; b_i <= elemB; b_i++) {
                    if (r4->data[b_i]) {
                      trueCount++;
                    }
                  }

                  i3 = r6->size[0];
                  r6->size[0] = trueCount;
                  emxEnsureCapacity_int32_T(r6, i3);
                  locFA = 0;
                  for (b_i = 0; b_i <= elemB; b_i++) {
                    if (r4->data[b_i]) {
                      r6->data[locFA] = b_i + 1;
                      locFA++;
                    }
                  }

                  /*                            else */
                  /*                            twoelem = ElementsOnNodeA(ismembc(ElementsOnNodeA,ElementsOnNodeB)); */
                  /*                            end */
                  if ((r6->size[0] == 2) || (r6->size[0] > 2)) {
                    /*  element interface */
                    /*  likely a PBC degenerating case */
                    notdone = 0;
                  } else {
                    /*  domain boundary */
                    facI1 = 0;
                    guard1 = true;
                  }
                } else {
                  /*  domain boundary */
                  facI1 = 0;
                  guard1 = true;
                }

                if (guard1) {
                  notdone = 1;
                  numSI_tmp = numEonPBC->data[(int)node - 1] + 1.0;
                  numEonPBC->data[(int)node - 1] = numSI_tmp;
                  numFPBC++;
                  FacetsOnPBC->data[(int)*numfac - 1] = node;
                }
              } else {
                notdone = 0;
              }
            } else {
              notdone = 0;
            }

            if ((InterTypes->data[((int)nextra + InterTypes->size[0] * ((int)
                   b_i2 - 1)) - 1] > 0.0) || (notdone != 0)) {
              /*  Mark facs as being cut */
              FacetsOnNodeInd->data[i1 + 4] = 1.0;
              FacetsOnNodeInd->data[i2 + 4] = 1.0;

              /*                      FacetsOnNodeCut(facnumA,nodeA) = 1; */
              /*                      FacetsOnNodeCut(facnumB,nodeB) = 1; */
            }
          }
        } else {
          /* domain boundary, add to SurfaceF */
          numSI_tmp = *numEonB + 1.0;
          (*numEonB)++;
          FacetsOnElement->data[elem + FacetsOnElement->size[0] * locF] =
            -numSI_tmp;
          ElementsOnBoundary->data[(int)numSI_tmp - 1] = (double)elem + 1.0;
          ElementsOnBoundary->data[((int)numSI_tmp + ElementsOnBoundary->size[0])
            - 1] = (double)locF + 1.0;

          /*  Assign nodal fac pairs */
          numA = FacetsOnNodeNum->data[(int)nodeA - 1] + 1.0;
          FacetsOnNodeNum->data[(int)nodeA - 1] = numA;
          nri += 2U;
          i1 = 5 * ((int)nri - 2);
          FacetsOnNodeInd->data[i1] = numA;
          FacetsOnNodeInd->data[i1 + 1] = nodeA;
          FacetsOnNodeInd->data[i1 + 2] = numSI_tmp;
          FacetsOnNodeInd->data[i1 + 3] = -1.0;

          /*              FacetsOnNode(facnum,nodeA) = numSI; */
          /*              FacetsOnNodeInt(facnum,nodeA) = -1; */
          numA = FacetsOnNodeNum->data[(int)nodeB - 1] + 1.0;
          FacetsOnNodeNum->data[(int)nodeB - 1] = numA;
          i1 = 5 * ((int)nri - 1);
          FacetsOnNodeInd->data[i1] = numA;
          FacetsOnNodeInd->data[i1 + 1] = nodeB;
          FacetsOnNodeInd->data[i1 + 2] = numSI_tmp;
          FacetsOnNodeInd->data[i1 + 3] = -1.0;

          /*              FacetsOnNode(facnum,nodeB) = numSI; */
          /*              FacetsOnNodeInt(facnum,nodeB) = -1; */
        }
      }

      /* facet not identified */
    }
  }

  emxFreeStruct_sparse(&expl_temp);
  emxFree_int32_T(&r6);
  emxFree_int32_T(&r5);
  emxFree_boolean_T(&r4);
  emxInit_real_T(&b_ElementsOnFacet, 2);
  if (1.0 > *numfac) {
    loop_ub = 0;
  } else {
    loop_ub = (int)*numfac;
  }

  i = b_ElementsOnFacet->size[0] * b_ElementsOnFacet->size[1];
  b_ElementsOnFacet->size[0] = loop_ub;
  b_ElementsOnFacet->size[1] = 4;
  emxEnsureCapacity_real_T(b_ElementsOnFacet, i);
  for (i = 0; i < loop_ub; i++) {
    b_ElementsOnFacet->data[i] = ElementsOnFacet->data[i];
  }

  for (i = 0; i < loop_ub; i++) {
    b_ElementsOnFacet->data[i + b_ElementsOnFacet->size[0]] =
      ElementsOnFacet->data[i + ElementsOnFacet->size[0]];
  }

  for (i = 0; i < loop_ub; i++) {
    b_ElementsOnFacet->data[i + b_ElementsOnFacet->size[0] * 2] =
      ElementsOnFacet->data[i + ElementsOnFacet->size[0] * 2];
  }

  for (i = 0; i < loop_ub; i++) {
    b_ElementsOnFacet->data[i + b_ElementsOnFacet->size[0] * 3] =
      ElementsOnFacet->data[i + ElementsOnFacet->size[0] * 3];
  }

  i = ElementsOnFacet->size[0] * ElementsOnFacet->size[1];
  ElementsOnFacet->size[0] = b_ElementsOnFacet->size[0];
  ElementsOnFacet->size[1] = 4;
  emxEnsureCapacity_real_T(ElementsOnFacet, i);
  loop_ub = b_ElementsOnFacet->size[0] * b_ElementsOnFacet->size[1];
  for (i = 0; i < loop_ub; i++) {
    ElementsOnFacet->data[i] = b_ElementsOnFacet->data[i];
  }

  emxFree_real_T(&b_ElementsOnFacet);
  if (1.0 > *numEonB) {
    loop_ub = 0;
  } else {
    loop_ub = (int)*numEonB;
  }

  i = b_MPCList->size[0] * b_MPCList->size[1];
  b_MPCList->size[0] = loop_ub;
  b_MPCList->size[1] = 2;
  emxEnsureCapacity_real_T(b_MPCList, i);
  for (i = 0; i < loop_ub; i++) {
    b_MPCList->data[i] = ElementsOnBoundary->data[i];
  }

  for (i = 0; i < loop_ub; i++) {
    b_MPCList->data[i + b_MPCList->size[0]] = ElementsOnBoundary->data[i +
      ElementsOnBoundary->size[0]];
  }

  i = ElementsOnBoundary->size[0] * ElementsOnBoundary->size[1];
  ElementsOnBoundary->size[0] = b_MPCList->size[0];
  ElementsOnBoundary->size[1] = 2;
  emxEnsureCapacity_real_T(ElementsOnBoundary, i);
  loop_ub = b_MPCList->size[0] * b_MPCList->size[1];
  for (i = 0; i < loop_ub; i++) {
    ElementsOnBoundary->data[i] = b_MPCList->data[i];
  }

  emxFree_real_T(&b_MPCList);
  if (1 > (int)nri) {
    loop_ub = 0;
    nume = 0;
    numPBC = 0;
  } else {
    loop_ub = (int)nri;
    nume = (int)nri;
    numPBC = (int)nri;
  }

  i = setAll->size[0] * setAll->size[1];
  setAll->size[0] = 1;
  setAll->size[1] = loop_ub;
  emxEnsureCapacity_real_T(setAll, i);
  for (i = 0; i < loop_ub; i++) {
    setAll->data[i] = FacetsOnNodeInd->data[5 * i];
  }

  i = setPBC->size[0] * setPBC->size[1];
  setPBC->size[0] = 1;
  setPBC->size[1] = nume;
  emxEnsureCapacity_real_T(setPBC, i);
  for (i = 0; i < nume; i++) {
    setPBC->data[i] = FacetsOnNodeInd->data[5 * i + 1];
  }

  i = NodeRegInd->size[0] * NodeRegInd->size[1];
  NodeRegInd->size[0] = 1;
  NodeRegInd->size[1] = numPBC;
  emxEnsureCapacity_real_T(NodeRegInd, i);
  for (i = 0; i < numPBC; i++) {
    NodeRegInd->data[i] = FacetsOnNodeInd->data[5 * i + 2];
  }

  b_sparse(setAll, setPBC, NodeRegInd, FacetsOnNode);

  /*  for each node, which other node is connected to it by an edge; value is the nodal ID of the connecting node */
  if (1 > (int)nri) {
    loop_ub = 0;
    nume = 0;
    numPBC = 0;
  } else {
    loop_ub = (int)nri;
    nume = (int)nri;
    numPBC = (int)nri;
  }

  i = setAll->size[0] * setAll->size[1];
  setAll->size[0] = 1;
  setAll->size[1] = loop_ub;
  emxEnsureCapacity_real_T(setAll, i);
  for (i = 0; i < loop_ub; i++) {
    setAll->data[i] = FacetsOnNodeInd->data[5 * i];
  }

  i = setPBC->size[0] * setPBC->size[1];
  setPBC->size[0] = 1;
  setPBC->size[1] = nume;
  emxEnsureCapacity_real_T(setPBC, i);
  for (i = 0; i < nume; i++) {
    setPBC->data[i] = FacetsOnNodeInd->data[5 * i + 1];
  }

  i = NodeRegInd->size[0] * NodeRegInd->size[1];
  NodeRegInd->size[0] = 1;
  NodeRegInd->size[1] = numPBC;
  emxEnsureCapacity_real_T(NodeRegInd, i);
  for (i = 0; i < numPBC; i++) {
    NodeRegInd->data[i] = FacetsOnNodeInd->data[5 * i + 4];
  }

  b_sparse(setAll, setPBC, NodeRegInd, FacetsOnNodeCut);

  /*  flag for whether that edge is being cut between two nodes; 1 for cut, 0 for retain. */
  if (1 > (int)nri) {
    loop_ub = 0;
    nume = 0;
    numPBC = 0;
  } else {
    loop_ub = (int)nri;
    nume = (int)nri;
    numPBC = (int)nri;
  }

  i = setAll->size[0] * setAll->size[1];
  setAll->size[0] = 1;
  setAll->size[1] = loop_ub;
  emxEnsureCapacity_real_T(setAll, i);
  for (i = 0; i < loop_ub; i++) {
    setAll->data[i] = FacetsOnNodeInd->data[5 * i];
  }

  i = setPBC->size[0] * setPBC->size[1];
  setPBC->size[0] = 1;
  setPBC->size[1] = nume;
  emxEnsureCapacity_real_T(setPBC, i);
  for (i = 0; i < nume; i++) {
    setPBC->data[i] = FacetsOnNodeInd->data[5 * i + 1];
  }

  i = NodeRegInd->size[0] * NodeRegInd->size[1];
  NodeRegInd->size[0] = 1;
  NodeRegInd->size[1] = numPBC;
  emxEnsureCapacity_real_T(NodeRegInd, i);
  for (i = 0; i < numPBC; i++) {
    NodeRegInd->data[i] = FacetsOnNodeInd->data[5 * i + 3];
  }

  emxFree_real_T(&FacetsOnNodeInd);
  b_sparse(setAll, setPBC, NodeRegInd, FacetsOnNodeInt);

  /*  flag for materialI ID for that edge; -1 means domain edge. */
  /*  clear FacetsOnNodeInd */
  /*  Group facets according to interface type */
  if (1.0 > *numfac) {
    loop_ub = 0;
  } else {
    loop_ub = (int)*numfac;
  }

  i = MorePBC->size[0];
  MorePBC->size[0] = loop_ub;
  emxEnsureCapacity_real_T(MorePBC, i);
  emxFree_real_T(&NodeRegInd);
  for (i = 0; i < loop_ub; i++) {
    MorePBC->data[i] = FacetsOnInterface->data[i];
  }

  sort(MorePBC, indx);
  i = FacetsOnInterface->size[0];
  FacetsOnInterface->size[0] = indx->size[0];
  emxEnsureCapacity_real_T(FacetsOnInterface, i);
  loop_ub = indx->size[0];
  for (i = 0; i < loop_ub; i++) {
    FacetsOnInterface->data[i] = indx->data[i];
  }

  i = FacetsOnIntMinusPBCNum->size[0];
  FacetsOnIntMinusPBCNum->size[0] = numEonF->size[0] + 1;
  emxEnsureCapacity_real_T(FacetsOnIntMinusPBCNum, i);
  FacetsOnIntMinusPBCNum->data[0] = 1.0;
  loop_ub = numEonF->size[0];
  for (i = 0; i < loop_ub; i++) {
    FacetsOnIntMinusPBCNum->data[i + 1] = numEonF->data[i];
  }

  i = FacetsOnInterfaceNum->size[0];
  FacetsOnInterfaceNum->size[0] = FacetsOnIntMinusPBCNum->size[0];
  emxEnsureCapacity_real_T(FacetsOnInterfaceNum, i);
  loop_ub = FacetsOnIntMinusPBCNum->size[0];
  for (i = 0; i < loop_ub; i++) {
    FacetsOnInterfaceNum->data[i] = FacetsOnIntMinusPBCNum->data[i];
  }

  i = (int)*numinttype;
  for (numPBC = 0; numPBC < i; numPBC++) {
    FacetsOnInterfaceNum->data[numPBC + 1] += FacetsOnInterfaceNum->data[numPBC];
  }

  /*  The actual facet identifiers for interface type regI are then: */
  /*  locF = FacetsOnInterfaceNum(regI):(FacetsOnInterfaceNum(regI+1)-1); */
  /*  facs = FacetsOnInterface(locF); */
  /*  true = all(ElementsOnFacet2(facs,1:4) == ElementsOnFacet(1:numEonF(regI),1:4,regI)); */
  if (usePBC != 0.0) {
    /*  sort to find the list of PBC facets grouped by interface type */
    if (1.0 > *numfac) {
      loop_ub = 0;
    } else {
      loop_ub = (int)*numfac;
    }

    i = MorePBC->size[0];
    MorePBC->size[0] = loop_ub;
    emxEnsureCapacity_real_T(MorePBC, i);
    for (i = 0; i < loop_ub; i++) {
      MorePBC->data[i] = FacetsOnPBC->data[i];
    }

    sort(MorePBC, indx);
    i = MorePBC->size[0];
    MorePBC->size[0] = indx->size[0];
    emxEnsureCapacity_real_T(MorePBC, i);
    loop_ub = indx->size[0];
    for (i = 0; i < loop_ub; i++) {
      MorePBC->data[i] = indx->data[i];
    }

    d = *numfac - numFPBC;
    if (d + 1.0 > *numfac) {
      i = -1;
      i1 = -2;
    } else {
      i = (int)(d + 1.0) - 2;
      i1 = (int)*numfac - 2;
    }

    loop_ub = i1 - i;
    i1 = FacetsOnPBC->size[0];
    FacetsOnPBC->size[0] = loop_ub + 1;
    emxEnsureCapacity_real_T(FacetsOnPBC, i1);
    for (i1 = 0; i1 <= loop_ub; i1++) {
      FacetsOnPBC->data[i1] = MorePBC->data[(i + i1) + 1];
    }

    /*  delete the zeros */
    i1 = FacetsOnPBCNum->size[0];
    FacetsOnPBCNum->size[0] = numEonPBC->size[0] + 1;
    emxEnsureCapacity_real_T(FacetsOnPBCNum, i1);
    FacetsOnPBCNum->data[0] = 1.0;
    loop_ub = numEonPBC->size[0];
    for (i1 = 0; i1 < loop_ub; i1++) {
      FacetsOnPBCNum->data[i1 + 1] = numEonPBC->data[i1];
    }

    i1 = (int)*numinttype;
    for (numPBC = 0; numPBC < i1; numPBC++) {
      FacetsOnPBCNum->data[numPBC + 1] += FacetsOnPBCNum->data[numPBC];
    }

    i1 = FacetsOnIntMinusPBC->size[0];
    FacetsOnIntMinusPBC->size[0] = (int)d;
    emxEnsureCapacity_real_T(FacetsOnIntMinusPBC, i1);
    loop_ub = (int)(*numfac - numFPBC);
    for (i1 = 0; i1 < loop_ub; i1++) {
      FacetsOnIntMinusPBC->data[i1] = 0.0;
    }

    i1 = (int)*numinttype;
    for (numPBC = 0; numPBC < i1; numPBC++) {
      d = FacetsOnInterfaceNum->data[numPBC + 1] - 1.0;
      if (d < FacetsOnInterfaceNum->data[numPBC]) {
        setAll->size[0] = 1;
        setAll->size[1] = 0;
      } else if ((rtIsInf(FacetsOnInterfaceNum->data[numPBC]) || rtIsInf(d)) &&
                 (FacetsOnInterfaceNum->data[numPBC] == d)) {
        i2 = setAll->size[0] * setAll->size[1];
        setAll->size[0] = 1;
        setAll->size[1] = 1;
        emxEnsureCapacity_real_T(setAll, i2);
        setAll->data[0] = rtNaN;
      } else if (FacetsOnInterfaceNum->data[numPBC] ==
                 FacetsOnInterfaceNum->data[numPBC]) {
        i2 = setAll->size[0] * setAll->size[1];
        setAll->size[0] = 1;
        loop_ub = (int)floor(d - FacetsOnInterfaceNum->data[numPBC]);
        setAll->size[1] = loop_ub + 1;
        emxEnsureCapacity_real_T(setAll, i2);
        for (i2 = 0; i2 <= loop_ub; i2++) {
          setAll->data[i2] = FacetsOnInterfaceNum->data[numPBC] + (double)i2;
        }
      } else {
        eml_float_colon(FacetsOnInterfaceNum->data[numPBC], d, setAll);
      }

      d = FacetsOnPBCNum->data[numPBC + 1] - 1.0;
      if (d < FacetsOnPBCNum->data[numPBC]) {
        setPBC->size[0] = 1;
        setPBC->size[1] = 0;
      } else if ((rtIsInf(FacetsOnPBCNum->data[numPBC]) || rtIsInf(d)) &&
                 (FacetsOnPBCNum->data[numPBC] == d)) {
        i2 = setPBC->size[0] * setPBC->size[1];
        setPBC->size[0] = 1;
        setPBC->size[1] = 1;
        emxEnsureCapacity_real_T(setPBC, i2);
        setPBC->data[0] = rtNaN;
      } else if (FacetsOnPBCNum->data[numPBC] == FacetsOnPBCNum->data[numPBC]) {
        i2 = setPBC->size[0] * setPBC->size[1];
        setPBC->size[0] = 1;
        loop_ub = (int)floor(d - FacetsOnPBCNum->data[numPBC]);
        setPBC->size[1] = loop_ub + 1;
        emxEnsureCapacity_real_T(setPBC, i2);
        for (i2 = 0; i2 <= loop_ub; i2++) {
          setPBC->data[i2] = FacetsOnPBCNum->data[numPBC] + (double)i2;
        }
      } else {
        eml_float_colon(FacetsOnPBCNum->data[numPBC], d, setPBC);
      }

      i2 = ElementsOnNodePBCNum->size[0];
      ElementsOnNodePBCNum->size[0] = setAll->size[1];
      emxEnsureCapacity_real_T(ElementsOnNodePBCNum, i2);
      loop_ub = setAll->size[1];
      for (i2 = 0; i2 < loop_ub; i2++) {
        ElementsOnNodePBCNum->data[i2] = FacetsOnInterface->data[(int)
          setAll->data[i2] - 1];
      }

      i2 = t4_d->size[0];
      t4_d->size[0] = setPBC->size[1];
      emxEnsureCapacity_real_T(t4_d, i2);
      loop_ub = setPBC->size[1];
      for (i2 = 0; i2 < loop_ub; i2++) {
        t4_d->data[i2] = MorePBC->data[i + (int)setPBC->data[i2]];
      }

      b_do_vectors(ElementsOnNodePBCNum, t4_d, setnPBC, indx, ib_size);
      numSI_tmp = setnPBC->size[0];
      d = FacetsOnIntMinusPBCNum->data[numPBC];
      d1 = FacetsOnIntMinusPBCNum->data[numPBC] + (double)setnPBC->size[0];
      FacetsOnIntMinusPBCNum->data[numPBC + 1] = d1;
      if (setnPBC->size[0] > 0) {
        if (d > d1 - 1.0) {
          i2 = -1;
          i3 = 0;
        } else {
          i2 = (int)d - 2;
          i3 = (int)(d1 - 1.0);
        }

        loop_ub = (i3 - i2) - 1;
        for (i3 = 0; i3 < loop_ub; i3++) {
          FacetsOnIntMinusPBC->data[(i2 + i3) + 1] = setnPBC->data[i3];
        }
      }
    }
  } else {
    i = FacetsOnPBCNum->size[0];
    FacetsOnPBCNum->size[0] = 1;
    emxEnsureCapacity_real_T(FacetsOnPBCNum, i);
    FacetsOnPBCNum->data[0] = 0.0;
    i = FacetsOnIntMinusPBC->size[0];
    FacetsOnIntMinusPBC->size[0] = 1;
    emxEnsureCapacity_real_T(FacetsOnIntMinusPBC, i);
    FacetsOnIntMinusPBC->data[0] = 0.0;
    i = FacetsOnIntMinusPBCNum->size[0];
    FacetsOnIntMinusPBCNum->size[0] = 1;
    emxEnsureCapacity_real_T(FacetsOnIntMinusPBCNum, i);
    FacetsOnIntMinusPBCNum->data[0] = 0.0;
  }

  emxFree_real_T(&setPBC);

  /*  Find material interfaces and duplicate nodes */
  ElementsOnNodeA->size[0] = 0;
  ElementsOnNodeA->size[1] = 1;
  i = (int)(nummat + -1.0);
  emxInitStruct_sparse3(&b_expl_temp);
  emxInitStruct_sparse3(&c_expl_temp);
  emxInitStruct_sparse3(&d_expl_temp);
  for (nume = 0; nume < i; nume++) {
    for (elemB = 0; elemB <= nume; elemB++) {
      /*  ID for material pair (row=mat2, col=mat1) */
      if (InterTypes->data[(nume + InterTypes->size[0] * elemB) + 1] > 0.0) {
        /*  Mark nodes on the interfaces */
        c_sparse_parenReference(NodeReg->d, NodeReg->colidx, NodeReg->rowidx,
          NodeReg->m, (double)elemB + 1.0, NodeRegSum.d, NodeRegSum.colidx,
          NodeRegSum.rowidx, &NodeRegSum.m);
        sparse_gt(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  NodeRegSum.m, &c_expl_temp);
        c_sparse_parenReference(NodeReg->d, NodeReg->colidx, NodeReg->rowidx,
          NodeReg->m, (double)nume + 2.0, NodeRegSum.d, NodeRegSum.colidx,
          NodeRegSum.rowidx, &NodeRegSum.m);
        sparse_gt(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  NodeRegSum.m, &d_expl_temp);
        i1 = b_expl_temp.colidx->size[0];
        b_expl_temp.colidx->size[0] = d_expl_temp.colidx->size[0];
        emxEnsureCapacity_int32_T(b_expl_temp.colidx, i1);
        loop_ub = d_expl_temp.colidx->size[0];
        for (i1 = 0; i1 < loop_ub; i1++) {
          b_expl_temp.colidx->data[i1] = d_expl_temp.colidx->data[i1];
        }

        i1 = b_expl_temp.rowidx->size[0];
        b_expl_temp.rowidx->size[0] = d_expl_temp.rowidx->size[0];
        emxEnsureCapacity_int32_T(b_expl_temp.rowidx, i1);
        loop_ub = d_expl_temp.rowidx->size[0];
        for (i1 = 0; i1 < loop_ub; i1++) {
          b_expl_temp.rowidx->data[i1] = d_expl_temp.rowidx->data[i1];
        }

        sparse_and(c_expl_temp.d, c_expl_temp.colidx, c_expl_temp.rowidx,
                   d_expl_temp.d, b_expl_temp.colidx, b_expl_temp.rowidx,
                   d_expl_temp.m, b_NodesOnPBCnum, indx, t6_rowidx, &locFA);
        c_sparse_parenReference(NodeReg->d, NodeReg->colidx, NodeReg->rowidx,
          NodeReg->m, (double)nume + 2.0, NodeRegSum.d, NodeRegSum.colidx,
          NodeRegSum.rowidx, &NodeRegSum.m);
        c_sparse_parenReference(NodeReg->d, NodeReg->colidx, NodeReg->rowidx,
          NodeReg->m, (double)elemB + 1.0, t4_d, ii, jj, &notdone);
        sparse_eq(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx, t4_d, ii,
                  jj, notdone, c_expl_temp.d, c_expl_temp.colidx,
                  c_expl_temp.rowidx, &numPBC);
        sparse_and(b_NodesOnPBCnum, indx, t6_rowidx, c_expl_temp.d,
                   c_expl_temp.colidx, c_expl_temp.rowidx, numPBC, d_expl_temp.d,
                   b_expl_temp.colidx, b_expl_temp.rowidx, &notdone);
        g_eml_find(b_expl_temp.colidx, b_expl_temp.rowidx, notdone, ii);
        loop_ub = ElementsOnNodeA->size[0];
        i1 = NodesOnInterface->size[0];
        NodesOnInterface->size[0] = ElementsOnNodeA->size[0] + ii->size[0];
        emxEnsureCapacity_real_T(NodesOnInterface, i1);
        for (i1 = 0; i1 < loop_ub; i1++) {
          NodesOnInterface->data[i1] = ElementsOnNodeA->data[i1];
        }

        loop_ub = ii->size[0];
        for (i1 = 0; i1 < loop_ub; i1++) {
          NodesOnInterface->data[i1 + ElementsOnNodeA->size[0]] = ii->data[i1];
        }

        i1 = ElementsOnNodeA->size[0] * ElementsOnNodeA->size[1];
        ElementsOnNodeA->size[0] = NodesOnInterface->size[0];
        ElementsOnNodeA->size[1] = 1;
        emxEnsureCapacity_real_T(ElementsOnNodeA, i1);
        loop_ub = NodesOnInterface->size[0];
        for (i1 = 0; i1 < loop_ub; i1++) {
          ElementsOnNodeA->data[i1] = NodesOnInterface->data[i1];
        }
      }
    }
  }

  emxFreeStruct_sparse3(&d_expl_temp);
  if (usePBC != 0.0) {
    /*  Add PBC nodes into duplicating list */
    sum(NodeReg->d, NodeReg->colidx, NodeReg->rowidx, NodeReg->m, NodeReg->n,
        NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx, &NodeRegSum.m);

    /*  from zipped nodes, ONLY the one with regions attached is in the zipped connectivity */
    sparse_gt(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx, NodeRegSum.m,
              &b_expl_temp);
    g_eml_find(b_expl_temp.colidx, b_expl_temp.rowidx, b_expl_temp.m, ii);
    i = b_NodesOnPBCnum->size[0];
    b_NodesOnPBCnum->size[0] = NodesOnPBCnum->size[0];
    emxEnsureCapacity_boolean_T(b_NodesOnPBCnum, i);
    loop_ub = NodesOnPBCnum->size[0];
    for (i = 0; i < loop_ub; i++) {
      b_NodesOnPBCnum->data[i] = (NodesOnPBCnum->data[i] > 0.0);
    }

    eml_find(b_NodesOnPBCnum, t6_rowidx);
    i = ElementsOnNodePBCNum->size[0];
    ElementsOnNodePBCNum->size[0] = t6_rowidx->size[0];
    emxEnsureCapacity_real_T(ElementsOnNodePBCNum, i);
    loop_ub = t6_rowidx->size[0];
    for (i = 0; i < loop_ub; i++) {
      ElementsOnNodePBCNum->data[i] = t6_rowidx->data[i];
    }

    i = t4_d->size[0];
    t4_d->size[0] = ii->size[0];
    emxEnsureCapacity_real_T(t4_d, i);
    loop_ub = ii->size[0];
    for (i = 0; i < loop_ub; i++) {
      t4_d->data[i] = ii->data[i];
    }

    c_do_vectors(ElementsOnNodePBCNum, t4_d, MorePBC, indx, jj);
    i = NodesOnInterface->size[0];
    NodesOnInterface->size[0] = ElementsOnNodeA->size[0] + MorePBC->size[0];
    emxEnsureCapacity_real_T(NodesOnInterface, i);
    loop_ub = ElementsOnNodeA->size[0];
    for (i = 0; i < loop_ub; i++) {
      NodesOnInterface->data[i] = ElementsOnNodeA->data[i];
    }

    loop_ub = MorePBC->size[0];
    for (i = 0; i < loop_ub; i++) {
      NodesOnInterface->data[i + ElementsOnNodeA->size[0]] = MorePBC->data[i];
    }

    i = ElementsOnNodeA->size[0] * ElementsOnNodeA->size[1];
    ElementsOnNodeA->size[0] = NodesOnInterface->size[0];
    ElementsOnNodeA->size[1] = 1;
    emxEnsureCapacity_real_T(ElementsOnNodeA, i);
    loop_ub = NodesOnInterface->size[0];
    for (i = 0; i < loop_ub; i++) {
      ElementsOnNodeA->data[i] = NodesOnInterface->data[i];
    }

    /*  add these zipped PBC nodes into the list for duplicates too */
  }

  emxFree_int32_T(&jj);
  notdone = ElementsOnNodeA->size[0];
  c_nodes_data = *ElementsOnNodeA;
  b_NodesOnElement[0] = notdone;
  c_nodes_data.size = &b_NodesOnElement[0];
  c_nodes_data.numDimensions = 1;
  unique_vector(&c_nodes_data, NodesOnInterface);
  locFA = NodesOnInterface->size[0];

  /*  Now actually duplicate the nodes */
  /*  Criteria: only nodes for which ALL inter-material facs involving a given */
  /*  material are being cut, then they are duplicated. */
  nextra = *numnp;
  i = NodesOnInterface->size[0];
  emxInit_real_T(&interreg2, 2);
  emxInitStruct_sparse2(&e_expl_temp);
  emxInitStruct_sparse2(&f_expl_temp);
  emxInitStruct_sparse2(&g_expl_temp);
  for (locN = 0; locN < i; locN++) {
    d = NodesOnInterface->data[locN];

    /*      matnode = sum(NodeMat(node,:)>0); */
    i1 = (int)NodesOnInterface->data[locN] - 1;
    if (FacetsOnNodeNum->data[i1] == 0.0) {
      /*  midside node */
      if ((usePBC != 0.0) && (NodesOnPBCnum->data[i1] > 0.0)) {
        /*  handle copies of PBC nodes specially */
        /*  Just get the old node numbers back and copy them into place */
        c_sparse_parenReference(ElementsOnNode->d, ElementsOnNode->colidx,
          ElementsOnNode->rowidx, ElementsOnNode->m, NodesOnInterface->data[locN],
          NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx, &NodeRegSum.m);
        b_sparse_full(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                      NodeRegSum.m, ElementsOnNodeA);

        /*  Loop over all elements attached to node */
        i1 = (int)ElementsOnNodeNum->data[i1];
        if (0 <= (int)ElementsOnNodeNum->data[(int)NodesOnInterface->data[locN]
            - 1] - 1) {
          if (1.0 > nen) {
            b_loop_ub = 0;
          } else {
            b_loop_ub = (int)nen;
          }
        }

        if (0 <= i1 - 1) {
          NodesOnElementCG_size[0] = 1;
          NodesOnElementCG_size[1] = b_loop_ub;
        }

        for (trueCount = 0; trueCount < i1; trueCount++) {
          /*  Reset nodal ID for element in higher material ID */
          numPBC = (int)ElementsOnNodeA->data[trueCount];
          for (i2 = 0; i2 < b_loop_ub; i2++) {
            NodesOnElementCG_data[i2] = (NodesOnElementCG->data[(numPBC +
              NodesOnElementCG->size[0] * i2) - 1] == d);
          }

          f_eml_find(NodesOnElementCG_data, NodesOnElementCG_size, tmp_data,
                     tmp_size);
          loop_ub = tmp_size[1];
          if (0 <= loop_ub - 1) {
            memcpy(&node_dup_tmp_data[0], &tmp_data[0], loop_ub * sizeof(int));
          }

          d1 = ElementsOnNodeA->data[trueCount];
          for (i2 = 0; i2 < loop_ub; i2++) {
            NodesOnElementDG->data[((int)d1 + NodesOnElementDG->size[0] *
              (node_dup_tmp_data[i2] - 1)) - 1] = NodesOnElementPBC->data[((int)
              d1 + NodesOnElementPBC->size[0] * (node_dup_tmp_data[i2] - 1)) - 1];
          }
        }
      } else {
        /*  regular midside node */
        i2 = (int)(nummat + -1.0);
        for (nume = 0; nume < i2; nume++) {
          for (elemB = 0; elemB <= nume; elemB++) {
            /*  ID for material pair (row=mat2, col=mat1) */
            if (InterTypes->data[(nume + InterTypes->size[0] * elemB) + 1] > 0.0)
            {
              /*  Duplicate nodes along material interface */
              d_sparse_parenReference(NodeReg->d, NodeReg->colidx,
                NodeReg->rowidx, d, (double)elemB + 1.0, ElementsOnNodePBCNum,
                ii, indx);
              b_sparse_gt(ElementsOnNodePBCNum, ii, g_expl_temp.d,
                          g_expl_temp.colidx, g_expl_temp.rowidx);
              d_sparse_parenReference(NodeReg->d, NodeReg->colidx,
                NodeReg->rowidx, d, (double)nume + 2.0, ElementsOnNodePBCNum, ii,
                indx);
              b_sparse_gt(ElementsOnNodePBCNum, ii, e_expl_temp.d,
                          e_expl_temp.colidx, indx);
              b_sparse_and(g_expl_temp.d, g_expl_temp.colidx, e_expl_temp.d,
                           e_expl_temp.colidx, indx, &f_expl_temp);
              d_sparse_parenReference(NodeReg->d, NodeReg->colidx,
                NodeReg->rowidx, d, (double)nume + 2.0, ElementsOnNodePBCNum, ii,
                indx);
              d_sparse_parenReference(NodeReg->d, NodeReg->colidx,
                NodeReg->rowidx, d, (double)elemB + 1.0, t4_d, indx, t6_rowidx);
              b_sparse_eq(ElementsOnNodePBCNum, ii, t4_d, indx, t6_rowidx,
                          &g_expl_temp);
              b_sparse_and(f_expl_temp.d, f_expl_temp.colidx, g_expl_temp.d,
                           g_expl_temp.colidx, g_expl_temp.rowidx, &e_expl_temp);

              /*  Namely, only find nodes that are part of materials mat1 and */
              /*  mat2, but ONLY if those nodal IDs have not been reset before, */
              /*  in which case the ID for mat1 will equal the ID for mat2. */
              /*  In this way, each new nodal ID is assigned to a single */
              /*  material. */
              old = false;
              numPBC = e_expl_temp.colidx->data[1] - 1;
              i3 = e_expl_temp.colidx->data[0];
              for (notdone = i3; notdone <= numPBC; notdone++) {
                old = e_expl_temp.d->data[notdone - 1];
              }

              if (old) {
                /*  Duplicate the nodal coordinates */
                nextra++;
                sparse_parenAssign2D(NodeReg, nextra, d, (double)nume + 2.0);

                /*  Loop over all nodes on material interface that need */
                /*  duplicated */
                c_sparse_parenReference(ElementsOnNode->d,
                  ElementsOnNode->colidx, ElementsOnNode->rowidx,
                  ElementsOnNode->m, d, NodeRegSum.d, NodeRegSum.colidx,
                  NodeRegSum.rowidx, &NodeRegSum.m);
                b_sparse_full(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                              NodeRegSum.m, ElementsOnNodeA);

                /*  Loop over all elements attached to node */
                i3 = (int)ElementsOnNodeNum->data[i1];
                for (trueCount = 0; trueCount < i3; trueCount++) {
                  numPBC = (int)ElementsOnNodeA->data[trueCount];
                  if (RegionOnElement->data[numPBC - 1] == (double)nume + 2.0) {
                    /*  Reset nodal ID for element in higher material ID */
                    if (1.0 > nen) {
                      loop_ub = 0;
                    } else {
                      loop_ub = (int)nen;
                    }

                    NodesOnElementCG_size[0] = 1;
                    NodesOnElementCG_size[1] = loop_ub;
                    for (nel = 0; nel < loop_ub; nel++) {
                      NodesOnElementCG_data[nel] = (NodesOnElementCG->data
                        [(numPBC + NodesOnElementCG->size[0] * nel) - 1] == d);
                    }

                    f_eml_find(NodesOnElementCG_data, NodesOnElementCG_size,
                               tmp_data, tmp_size);
                    loop_ub = tmp_size[1];
                    for (nel = 0; nel < loop_ub; nel++) {
                      b_tmp_data[nel] = (signed char)tmp_data[nel];
                    }

                    for (nel = 0; nel < loop_ub; nel++) {
                      NodesOnElementDG->data[(numPBC + NodesOnElementDG->size[0]
                        * (b_tmp_data[nel] - 1)) - 1] = nextra;
                    }

                    Coordinates3->data[(int)nextra - 1] = Coordinates->data[i1];
                    Coordinates3->data[((int)nextra + Coordinates3->size[0]) - 1]
                      = Coordinates->data[i1 + Coordinates->size[0]];
                  }
                }
              }
            }
          }
        }
      }
    } else {
      /*  All corner nodes */
      /*  form secs; a sec is a contiguous region of elements that is not */
      /*  cut apart by any CZM facs */
      /*  start by assuming all elements are separated */
      b_i1 = ElementsOnNodeNum->data[i1];
      i2 = interreg2->size[0] * interreg2->size[1];
      interreg2->size[0] = (int)b_i1;
      interreg2->size[1] = (int)ElementsOnNodeNum->data[i1];
      emxEnsureCapacity_real_T(interreg2, i2);
      loop_ub = (int)ElementsOnNodeNum->data[i1] * (int)ElementsOnNodeNum->
        data[i1];
      for (i2 = 0; i2 < loop_ub; i2++) {
        interreg2->data[i2] = 0.0;
      }

      d1 = ElementsOnNodeNum->data[(int)NodesOnInterface->data[locN] - 1];
      if (d1 < 1.0) {
        setAll->size[0] = 1;
        setAll->size[1] = 0;
      } else if (rtIsInf(d1) && (1.0 == ElementsOnNodeNum->data[(int)
                  NodesOnInterface->data[locN] - 1])) {
        i2 = setAll->size[0] * setAll->size[1];
        setAll->size[0] = 1;
        setAll->size[1] = 1;
        emxEnsureCapacity_real_T(setAll, i2);
        setAll->data[0] = rtNaN;
      } else {
        i2 = setAll->size[0] * setAll->size[1];
        setAll->size[0] = 1;
        loop_ub = (int)floor(ElementsOnNodeNum->data[(int)NodesOnInterface->
                             data[locN] - 1] - 1.0);
        setAll->size[1] = loop_ub + 1;
        emxEnsureCapacity_real_T(setAll, i2);
        for (i2 = 0; i2 <= loop_ub; i2++) {
          setAll->data[i2] = (double)i2 + 1.0;
        }
      }

      b_sparse_parenReference(ElementsOnNode->d, ElementsOnNode->colidx,
        ElementsOnNode->rowidx, setAll, NodesOnInterface->data[locN],
        NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx, &NodeRegSum.m);
      b_sparse_full(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                    NodeRegSum.m, r7);
      i2 = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      setAll->size[1] = r7->size[0];
      emxEnsureCapacity_real_T(setAll, i2);
      loop_ub = r7->size[0];
      for (i2 = 0; i2 < loop_ub; i2++) {
        setAll->data[i2] = r7->data[i2];
      }

      loop_ub = setAll->size[1];
      for (i2 = 0; i2 < loop_ub; i2++) {
        interreg2->data[interreg2->size[0] * i2] = setAll->data[i2];
      }

      i2 = (int)NodesOnInterface->data[locN] - 1;
      loop_ub = (int)ElementsOnNodeNum->data[i2];
      i3 = setnPBC->size[0];
      setnPBC->size[0] = loop_ub;
      emxEnsureCapacity_real_T(setnPBC, i3);
      for (i3 = 0; i3 < loop_ub; i3++) {
        setnPBC->data[i3] = 1.0;
      }

      i3 = (int)FacetsOnNodeNum->data[i2];
      for (locF = 0; locF < i3; locF++) {
        d_sparse_parenReference(FacetsOnNodeInt->d, FacetsOnNodeInt->colidx,
          FacetsOnNodeInt->rowidx, (double)locF + 1.0, d, ElementsOnNodePBCNum,
          ii, indx);
        b_sparse_gt(ElementsOnNodePBCNum, ii, g_expl_temp.d, g_expl_temp.colidx,
                    g_expl_temp.rowidx);
        old = false;
        numPBC = g_expl_temp.colidx->data[1] - 1;
        nel = g_expl_temp.colidx->data[0];
        for (notdone = nel; notdone <= numPBC; notdone++) {
          old = g_expl_temp.d->data[notdone - 1];
        }

        if (old) {
          /*  exclude internal facs */
          /*                  intramattrue = ~isempty(find(matI==diag(intermat2),1)); I */
          /*                  found out that the code is only putting cut in */
          /*                  nodefaccut for intermaterials, not the diagonal of */
          /*                  intermat2, so no if-test is needed */
          /*                  if ~cuttrue || intramattrue % two elements should be joined into one sector, along with their neighbors currently in the sector */
          d_sparse_parenReference(FacetsOnNodeCut->d, FacetsOnNodeCut->colidx,
            FacetsOnNodeCut->rowidx, (double)locF + 1.0, d, ElementsOnNodePBCNum,
            ii, indx);
          sparse_not(ii, indx, g_expl_temp.d, g_expl_temp.colidx,
                     g_expl_temp.rowidx);
          old = false;
          numPBC = g_expl_temp.colidx->data[1] - 1;
          nel = g_expl_temp.colidx->data[0];
          for (notdone = nel; notdone <= numPBC; notdone++) {
            old = g_expl_temp.d->data[notdone - 1];
          }

          if (old) {
            /*  two elements should be joined into one sector, along with their neighbors currently in the sector */
            d_sparse_parenReference(FacetsOnNode->d, FacetsOnNode->colidx,
              FacetsOnNode->rowidx, (double)locF + 1.0, d, ElementsOnNodePBCNum,
              ii, indx);
            numA = sparse_full(ElementsOnNodePBCNum, ii);
            numPBC = (int)ElementsOnFacet->data[(int)numA - 1];
            notdone = (int)ElementsOnFacet->data[((int)numA +
              ElementsOnFacet->size[0]) - 1];

            /*  find the sectors for each element */
            nri = 0U;
            old = false;
            while ((nri < b_i1) && (!old)) {
              nri++;
              d1 = setnPBC->data[(int)nri - 1];
              if (d1 > 0.0) {
                if (1.0 > d1) {
                  nume = 0;
                } else {
                  nume = (int)d1;
                }

                /*  find is MUCH faster than ismember */
                nel = b_NodesOnPBCnum->size[0];
                b_NodesOnPBCnum->size[0] = nume;
                emxEnsureCapacity_boolean_T(b_NodesOnPBCnum, nel);
                for (nel = 0; nel < nume; nel++) {
                  b_NodesOnPBCnum->data[nel] = (interreg2->data[nel +
                    interreg2->size[0] * ((int)nri - 1)] == numPBC);
                }

                eml_find(b_NodesOnPBCnum, t6_rowidx);
                nel = ElementsOnNodePBCNum->size[0];
                ElementsOnNodePBCNum->size[0] = t6_rowidx->size[0];
                emxEnsureCapacity_real_T(ElementsOnNodePBCNum, nel);
                nume = t6_rowidx->size[0];
                for (nel = 0; nel < nume; nel++) {
                  ElementsOnNodePBCNum->data[nel] = t6_rowidx->data[nel];
                }

                old = any(ElementsOnNodePBCNum);

                /*                          if sec1>0 */
                /*                              break */
                /*                          end */
              }
            }

            iSec2 = 0U;
            old = false;
            while ((iSec2 < b_i1) && (!old)) {
              iSec2++;
              d1 = setnPBC->data[(int)iSec2 - 1];
              if (d1 > 0.0) {
                if (1.0 > d1) {
                  nume = 0;
                } else {
                  nume = (int)setnPBC->data[(int)iSec2 - 1];
                }

                nel = b_NodesOnPBCnum->size[0];
                b_NodesOnPBCnum->size[0] = nume;
                emxEnsureCapacity_boolean_T(b_NodesOnPBCnum, nel);
                for (nel = 0; nel < nume; nel++) {
                  b_NodesOnPBCnum->data[nel] = (notdone == interreg2->data[nel +
                    interreg2->size[0] * ((int)iSec2 - 1)]);
                }

                eml_find(b_NodesOnPBCnum, t6_rowidx);
                nel = ElementsOnNodePBCNum->size[0];
                ElementsOnNodePBCNum->size[0] = t6_rowidx->size[0];
                emxEnsureCapacity_real_T(ElementsOnNodePBCNum, nel);
                nume = t6_rowidx->size[0];
                for (nel = 0; nel < nume; nel++) {
                  ElementsOnNodePBCNum->data[nel] = t6_rowidx->data[nel];
                }

                old = any(ElementsOnNodePBCNum);

                /*                          if sec2>0 */
                /*                              break */
                /*                          end */
              }
            }

            /*  merge secs */
            if ((int)iSec2 != (int)nri) {
              d1 = setnPBC->data[(int)iSec2 - 1];
              if (1.0 > d1) {
                nume = 0;
              } else {
                nume = (int)d1;
              }

              b_i2 = setnPBC->data[(int)nri - 1];
              if (1.0 > b_i2) {
                numPBC = 0;
              } else {
                numPBC = (int)b_i2;
              }

              nel = MorePBC->size[0];
              MorePBC->size[0] = nume + numPBC;
              emxEnsureCapacity_real_T(MorePBC, nel);
              for (nel = 0; nel < nume; nel++) {
                MorePBC->data[nel] = interreg2->data[nel + interreg2->size[0] *
                  ((int)iSec2 - 1)];
              }

              for (nel = 0; nel < numPBC; nel++) {
                MorePBC->data[nel + nume] = interreg2->data[nel +
                  interreg2->size[0] * ((int)nri - 1)];
              }

              b_sort(MorePBC);
              nume = MorePBC->size[0];
              for (nel = 0; nel < nume; nel++) {
                interreg2->data[nel + interreg2->size[0] * ((int)iSec2 - 1)] =
                  MorePBC->data[nel];
              }

              nume = interreg2->size[0];
              for (nel = 0; nel < nume; nel++) {
                interreg2->data[nel + interreg2->size[0] * ((int)nri - 1)] = 0.0;
              }

              setnPBC->data[(int)iSec2 - 1] = d1 + b_i2;
              setnPBC->data[(int)nri - 1] = 0.0;
            }
          }
        }

        /*  if external fac */
      }

      /*  assign node IDs to each sector */
      guard1 = false;
      if (usePBC != 0.0) {
        d1 = NodesOnPBCnum->data[i2];
        if (d1 > 0.0) {
          /*  handle copies of PBC nodes specially */
          nume = (int)(NodesOnPBCnum->data[i2] + 1.0);
          i2 = MorePBC->size[0];
          MorePBC->size[0] = nume;
          emxEnsureCapacity_real_T(MorePBC, i2);
          for (i2 = 0; i2 < nume; i2++) {
            MorePBC->data[i2] = 0.0;
          }

          for (elemB = 0; elemB < loop_ub; elemB++) {
            if (setnPBC->data[elemB] > 0.0) {
              if (1.0 > nen) {
                nume = 0;
              } else {
                nume = (int)nen;
              }

              notdone = (int)interreg2->data[interreg2->size[0] * elemB];
              NodesOnElementCG_size[0] = 1;
              NodesOnElementCG_size[1] = nume;
              for (i2 = 0; i2 < nume; i2++) {
                NodesOnElementCG_data[i2] = (NodesOnElementCG->data[(notdone +
                  NodesOnElementCG->size[0] * i2) - 1] == d);
              }

              f_eml_find(NodesOnElementCG_data, NodesOnElementCG_size, tmp_data,
                         tmp_size);
              numPBC = tmp_size[0] * tmp_size[1];
              for (i2 = 0; i2 < numPBC; i2++) {
                nodeC2_data[i2] = tmp_data[i2];
              }

              b_i2 = interreg2->data[interreg2->size[0] * elemB];
              numA = NodesOnElementPBC->data[((int)b_i2 +
                NodesOnElementPBC->size[0] * ((int)nodeC2_data[0] - 1)) - 1];
              NodesOnElementCG_size[0] = 1;
              NodesOnElementCG_size[1] = nume;
              for (i2 = 0; i2 < nume; i2++) {
                NodesOnElementCG_data[i2] = (NodesOnElementCG->data[((int)b_i2 +
                  NodesOnElementCG->size[0] * i2) - 1] == d);
              }

              f_eml_find(NodesOnElementCG_data, NodesOnElementCG_size, tmp_data,
                         tmp_size);
              if (NodesOnElementPBC->data[((int)interreg2->data[interreg2->size
                   [0] * elemB] + NodesOnElementPBC->size[0] * (tmp_data[0] - 1))
                  - 1] > d) {
                if (1.0 > (d1 + 1.0) - 1.0) {
                  numPBC = 0;
                } else {
                  numPBC = (int)((d1 + 1.0) - 1.0);
                }

                NodesOnPBC_size[0] = numPBC;
                for (i2 = 0; i2 < numPBC; i2++) {
                  b_NodesOnPBC_data[i2] = (NodesOnPBC->data[i2 + 4 * i1] == numA);
                }

                NodesOnPBC_data.data = &b_NodesOnPBC_data[0];
                NodesOnPBC_data.size = &NodesOnPBC_size[0];
                NodesOnPBC_data.allocatedSize = 4;
                NodesOnPBC_data.numDimensions = 1;
                NodesOnPBC_data.canFreeData = false;
                eml_find(&NodesOnPBC_data, t6_rowidx);
                nodeloc_size[0] = t6_rowidx->size[0];
                numPBC = t6_rowidx->size[0];
                for (i2 = 0; i2 < numPBC; i2++) {
                  nodeloc_data[i2] = (double)t6_rowidx->data[i2] + 1.0;
                }
              } else {
                nodeloc_size[0] = 1;
                nodeloc_data[0] = 1.0;
              }

              numPBC = nodeloc_size[0];
              links2_size[0] = nodeloc_size[0];
              for (i2 = 0; i2 < numPBC; i2++) {
                MorePBC_data[i2] = MorePBC->data[(int)nodeloc_data[i2] - 1];
              }

              if (ifWhileCond(MorePBC_data, links2_size)) {
                nextra++;
                NodesOnElementCG_size[0] = 1;
                NodesOnElementCG_size[1] = nume;
                for (i2 = 0; i2 < nume; i2++) {
                  NodesOnElementCG_data[i2] = (NodesOnElementCG->data[(notdone +
                    NodesOnElementCG->size[0] * i2) - 1] == d);
                }

                f_eml_find(NodesOnElementCG_data, NodesOnElementCG_size,
                           tmp_data, tmp_size);
                notdone = (int)NodesOnElementPBC->data[((int)interreg2->
                  data[interreg2->size[0] * elemB] + NodesOnElementPBC->size[0] *
                  (tmp_data[0] - 1)) - 1];
                Coordinates3->data[(int)nextra - 1] = Coordinates->data[notdone
                  - 1];
                Coordinates3->data[((int)nextra + Coordinates3->size[0]) - 1] =
                  Coordinates->data[(notdone + Coordinates->size[0]) - 1];

                /*  Loop over all elements attached to node */
                i2 = (int)setnPBC->data[elemB];
                if (0 <= (int)setnPBC->data[elemB] - 1) {
                  if (b_i1 < 1.0) {
                    setAll->size[0] = 1;
                    setAll->size[1] = 0;
                  } else if (rtIsInf(b_i1) && (1.0 == b_i1)) {
                    i3 = setAll->size[0] * setAll->size[1];
                    setAll->size[0] = 1;
                    setAll->size[1] = 1;
                    emxEnsureCapacity_real_T(setAll, i3);
                    setAll->data[0] = rtNaN;
                  } else {
                    i3 = setAll->size[0] * setAll->size[1];
                    setAll->size[0] = 1;
                    setAll->size[1] = (int)floor(ElementsOnNodeNum->data[i1] -
                      1.0) + 1;
                    emxEnsureCapacity_real_T(setAll, i3);
                    nume = (int)floor(ElementsOnNodeNum->data[i1] - 1.0);
                    for (i3 = 0; i3 <= nume; i3++) {
                      setAll->data[i3] = (double)i3 + 1.0;
                    }
                  }

                  b_sparse_parenReference(ElementsOnNode->d,
                    ElementsOnNode->colidx, ElementsOnNode->rowidx, setAll, d,
                    NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                    &NodeRegSum.m);
                  if (1.0 > nen) {
                    f_loop_ub = 0;
                  } else {
                    f_loop_ub = (int)nen;
                  }
                }

                if (0 <= i2 - 1) {
                  NodesOnElementCG_size[0] = 1;
                  NodesOnElementCG_size[1] = f_loop_ub;
                }

                for (trueCount = 0; trueCount < i2; trueCount++) {
                  sparse_parenAssign2D(NodeReg, nextra, numA,
                                       RegionOnElement->data[(int)
                                       interreg2->data[trueCount +
                                       interreg2->size[0] * elemB] - 1]);
                  c_sparse_eq(interreg2->data[trueCount + interreg2->size[0] *
                              elemB], NodeRegSum.d, NodeRegSum.colidx,
                              NodeRegSum.rowidx, NodeRegSum.m, &c_expl_temp);
                  i3 = b_expl_temp.colidx->size[0];
                  b_expl_temp.colidx->size[0] = c_expl_temp.colidx->size[0];
                  emxEnsureCapacity_int32_T(b_expl_temp.colidx, i3);
                  nume = c_expl_temp.colidx->size[0];
                  for (i3 = 0; i3 < nume; i3++) {
                    b_expl_temp.colidx->data[i3] = c_expl_temp.colidx->data[i3];
                  }

                  i3 = b_expl_temp.rowidx->size[0];
                  b_expl_temp.rowidx->size[0] = c_expl_temp.rowidx->size[0];
                  emxEnsureCapacity_int32_T(b_expl_temp.rowidx, i3);
                  nume = c_expl_temp.rowidx->size[0];
                  for (i3 = 0; i3 < nume; i3++) {
                    b_expl_temp.rowidx->data[i3] = c_expl_temp.rowidx->data[i3];
                  }

                  g_eml_find(b_expl_temp.colidx, b_expl_temp.rowidx,
                             c_expl_temp.m, ii);
                  i3 = t4_d->size[0];
                  t4_d->size[0] = ii->size[0];
                  emxEnsureCapacity_real_T(t4_d, i3);
                  nume = ii->size[0];
                  for (i3 = 0; i3 < nume; i3++) {
                    t4_d->data[i3] = ii->data[i3];
                  }

                  c_sparse_parenAssign(ElementsOnNodeDup, nextra, t4_d, d);

                  /*  Reset nodal ID for element in higher material ID */
                  notdone = (int)interreg2->data[trueCount + interreg2->size[0] *
                    elemB];
                  for (i3 = 0; i3 < f_loop_ub; i3++) {
                    NodesOnElementCG_data[i3] = (NodesOnElementCG->data[(notdone
                      + NodesOnElementCG->size[0] * i3) - 1] == d);
                  }

                  f_eml_find(NodesOnElementCG_data, NodesOnElementCG_size,
                             tmp_data, tmp_size);
                  nume = tmp_size[1];
                  for (i3 = 0; i3 < nume; i3++) {
                    b_tmp_data[i3] = (signed char)tmp_data[i3];
                  }

                  for (i3 = 0; i3 < nume; i3++) {
                    NodesOnElementDG->data[(notdone + NodesOnElementDG->size[0] *
                      (b_tmp_data[i3] - 1)) - 1] = nextra;
                  }
                }
              } else {
                nume = nodeloc_size[0];
                for (i2 = 0; i2 < nume; i2++) {
                  c_tmp_data[i2] = (int)nodeloc_data[i2];
                }

                for (i2 = 0; i2 < nume; i2++) {
                  MorePBC->data[c_tmp_data[i2] - 1] = 1.0;
                }

                i2 = (int)setnPBC->data[elemB];
                if (0 <= i2 - 1) {
                  if (b_i1 < 1.0) {
                    setAll->size[0] = 1;
                    setAll->size[1] = 0;
                  } else if (rtIsInf(b_i1) && (1.0 == b_i1)) {
                    i3 = setAll->size[0] * setAll->size[1];
                    setAll->size[0] = 1;
                    setAll->size[1] = 1;
                    emxEnsureCapacity_real_T(setAll, i3);
                    setAll->data[0] = rtNaN;
                  } else {
                    i3 = setAll->size[0] * setAll->size[1];
                    setAll->size[0] = 1;
                    setAll->size[1] = (int)floor(ElementsOnNodeNum->data[i1] -
                      1.0) + 1;
                    emxEnsureCapacity_real_T(setAll, i3);
                    nume = (int)floor(ElementsOnNodeNum->data[i1] - 1.0);
                    for (i3 = 0; i3 <= nume; i3++) {
                      setAll->data[i3] = (double)i3 + 1.0;
                    }
                  }

                  b_sparse_parenReference(ElementsOnNode->d,
                    ElementsOnNode->colidx, ElementsOnNode->rowidx, setAll, d,
                    NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                    &NodeRegSum.m);
                  if (1.0 > nen) {
                    e_loop_ub = 0;
                  } else {
                    e_loop_ub = (int)nen;
                  }

                  NodesOnElementCG_size[0] = 1;
                  NodesOnElementCG_size[1] = e_loop_ub;
                }

                for (trueCount = 0; trueCount < i2; trueCount++) {
                  sparse_parenAssign2D(NodeReg, numA, numA,
                                       RegionOnElement->data[(int)
                                       interreg2->data[trueCount +
                                       interreg2->size[0] * elemB] - 1]);
                  c_sparse_eq(interreg2->data[trueCount + interreg2->size[0] *
                              elemB], NodeRegSum.d, NodeRegSum.colidx,
                              NodeRegSum.rowidx, NodeRegSum.m, &c_expl_temp);
                  i3 = b_expl_temp.colidx->size[0];
                  b_expl_temp.colidx->size[0] = c_expl_temp.colidx->size[0];
                  emxEnsureCapacity_int32_T(b_expl_temp.colidx, i3);
                  nume = c_expl_temp.colidx->size[0];
                  for (i3 = 0; i3 < nume; i3++) {
                    b_expl_temp.colidx->data[i3] = c_expl_temp.colidx->data[i3];
                  }

                  i3 = b_expl_temp.rowidx->size[0];
                  b_expl_temp.rowidx->size[0] = c_expl_temp.rowidx->size[0];
                  emxEnsureCapacity_int32_T(b_expl_temp.rowidx, i3);
                  nume = c_expl_temp.rowidx->size[0];
                  for (i3 = 0; i3 < nume; i3++) {
                    b_expl_temp.rowidx->data[i3] = c_expl_temp.rowidx->data[i3];
                  }

                  g_eml_find(b_expl_temp.colidx, b_expl_temp.rowidx,
                             c_expl_temp.m, ii);
                  i3 = t4_d->size[0];
                  t4_d->size[0] = ii->size[0];
                  emxEnsureCapacity_real_T(t4_d, i3);
                  nume = ii->size[0];
                  for (i3 = 0; i3 < nume; i3++) {
                    t4_d->data[i3] = ii->data[i3];
                  }

                  c_sparse_parenAssign(ElementsOnNodeDup, numA, t4_d, d);

                  /*  Reset nodal ID for element in higher material ID */
                  notdone = (int)interreg2->data[trueCount + interreg2->size[0] *
                    elemB];
                  for (i3 = 0; i3 < e_loop_ub; i3++) {
                    NodesOnElementCG_data[i3] = (NodesOnElementCG->data[(notdone
                      + NodesOnElementCG->size[0] * i3) - 1] == d);
                  }

                  f_eml_find(NodesOnElementCG_data, NodesOnElementCG_size,
                             tmp_data, tmp_size);
                  nume = tmp_size[1];
                  for (i3 = 0; i3 < nume; i3++) {
                    b_tmp_data[i3] = (signed char)tmp_data[i3];
                  }

                  for (i3 = 0; i3 < nume; i3++) {
                    NodesOnElementDG->data[(notdone + NodesOnElementDG->size[0] *
                      (b_tmp_data[i3] - 1)) - 1] = numA;
                  }
                }
              }
            }
          }
        } else {
          guard1 = true;
        }
      } else {
        guard1 = true;
      }

      if (guard1) {
        /*  regular node */
        numPBC = 0;
        for (elemB = 0; elemB < loop_ub; elemB++) {
          if (setnPBC->data[elemB] > 0.0) {
            if (numPBC != 0) {
              nextra++;
              Coordinates3->data[(int)nextra - 1] = Coordinates->data[i1];
              Coordinates3->data[((int)nextra + Coordinates3->size[0]) - 1] =
                Coordinates->data[i1 + Coordinates->size[0]];

              /*  Loop over all elements attached to node */
              i2 = (int)setnPBC->data[elemB];
              if (0 <= i2 - 1) {
                if (b_i1 < 1.0) {
                  setAll->size[0] = 1;
                  setAll->size[1] = 0;
                } else if (rtIsInf(b_i1) && (1.0 == b_i1)) {
                  i3 = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  setAll->size[1] = 1;
                  emxEnsureCapacity_real_T(setAll, i3);
                  setAll->data[0] = rtNaN;
                } else {
                  i3 = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  nume = (int)floor(b_i1 - 1.0);
                  setAll->size[1] = nume + 1;
                  emxEnsureCapacity_real_T(setAll, i3);
                  for (i3 = 0; i3 <= nume; i3++) {
                    setAll->data[i3] = (double)i3 + 1.0;
                  }
                }

                b_sparse_parenReference(ElementsOnNode->d,
                  ElementsOnNode->colidx, ElementsOnNode->rowidx, setAll, d,
                  NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  &NodeRegSum.m);
                if (1.0 > nen) {
                  d_loop_ub = 0;
                } else {
                  d_loop_ub = (int)nen;
                }

                NodesOnElementCG_size[0] = 1;
                NodesOnElementCG_size[1] = d_loop_ub;
              }

              for (trueCount = 0; trueCount < i2; trueCount++) {
                sparse_parenAssign2D(NodeReg, nextra, d, RegionOnElement->data
                                     [(int)interreg2->data[trueCount +
                                     interreg2->size[0] * elemB] - 1]);
                c_sparse_eq(interreg2->data[trueCount + interreg2->size[0] *
                            elemB], NodeRegSum.d, NodeRegSum.colidx,
                            NodeRegSum.rowidx, NodeRegSum.m, &c_expl_temp);
                i3 = b_expl_temp.colidx->size[0];
                b_expl_temp.colidx->size[0] = c_expl_temp.colidx->size[0];
                emxEnsureCapacity_int32_T(b_expl_temp.colidx, i3);
                nume = c_expl_temp.colidx->size[0];
                for (i3 = 0; i3 < nume; i3++) {
                  b_expl_temp.colidx->data[i3] = c_expl_temp.colidx->data[i3];
                }

                i3 = b_expl_temp.rowidx->size[0];
                b_expl_temp.rowidx->size[0] = c_expl_temp.rowidx->size[0];
                emxEnsureCapacity_int32_T(b_expl_temp.rowidx, i3);
                nume = c_expl_temp.rowidx->size[0];
                for (i3 = 0; i3 < nume; i3++) {
                  b_expl_temp.rowidx->data[i3] = c_expl_temp.rowidx->data[i3];
                }

                g_eml_find(b_expl_temp.colidx, b_expl_temp.rowidx, c_expl_temp.m,
                           ii);
                i3 = t4_d->size[0];
                t4_d->size[0] = ii->size[0];
                emxEnsureCapacity_real_T(t4_d, i3);
                nume = ii->size[0];
                for (i3 = 0; i3 < nume; i3++) {
                  t4_d->data[i3] = ii->data[i3];
                }

                c_sparse_parenAssign(ElementsOnNodeDup, nextra, t4_d, d);

                /*  Reset nodal ID for element in higher material ID */
                notdone = (int)interreg2->data[trueCount + interreg2->size[0] *
                  elemB];
                for (i3 = 0; i3 < d_loop_ub; i3++) {
                  NodesOnElementCG_data[i3] = (NodesOnElementCG->data[(notdone +
                    NodesOnElementCG->size[0] * i3) - 1] == d);
                }

                f_eml_find(NodesOnElementCG_data, NodesOnElementCG_size,
                           tmp_data, tmp_size);
                nume = tmp_size[1];
                for (i3 = 0; i3 < nume; i3++) {
                  b_tmp_data[i3] = (signed char)tmp_data[i3];
                }

                for (i3 = 0; i3 < nume; i3++) {
                  NodesOnElementDG->data[(notdone + NodesOnElementDG->size[0] *
                    (b_tmp_data[i3] - 1)) - 1] = nextra;
                }
              }
            } else {
              numPBC = 1;
            }
          }
        }
      }
    }

    /* if facnum */
  }

  emxFree_real_T(&r7);
  emxFreeStruct_sparse2(&g_expl_temp);
  emxFreeStruct_sparse2(&f_expl_temp);
  emxFreeStruct_sparse2(&e_expl_temp);
  emxFree_int32_T(&indx);
  emxFree_real_T(&interreg2);
  emxFree_real_T(&setnPBC);
  emxFree_real_T(&ElementsOnNodeA);

  /*  Form interfaces between all facs on interiors of specified material regions */
  diag(InterTypes, ElementsOnNodePBCNum);
  if (b_norm(ElementsOnNodePBCNum) > 0.0) {
    i = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
    NodesOnElementCG->size[0] = NodesOnElementDG->size[0];
    NodesOnElementCG->size[1] = NodesOnElementDG->size[1];
    emxEnsureCapacity_real_T(NodesOnElementCG, i);
    loop_ub = NodesOnElementDG->size[0] * NodesOnElementDG->size[1];
    for (i = 0; i < loop_ub; i++) {
      NodesOnElementCG->data[i] = NodesOnElementDG->data[i];
    }

    /*  Update connectivities to reflect presence of interfaces */
    i = ElementsOnNodeNum2->size[0];
    ElementsOnNodeNum2->size[0] = (int)nextra;
    emxEnsureCapacity_real_T(ElementsOnNodeNum2, i);
    loop_ub = (int)nextra;
    for (i = 0; i < loop_ub; i++) {
      ElementsOnNodeNum2->data[i] = 0.0;
    }

    i = NodeCGDG->size[0] * NodeCGDG->size[1];
    NodeCGDG->size[0] = 12;
    NodeCGDG->size[1] = (int)nextra;
    emxEnsureCapacity_real_T(NodeCGDG, i);
    loop_ub = 12 * (int)nextra;
    for (i = 0; i < loop_ub; i++) {
      NodeCGDG->data[i] = 0.0;
    }

    /*  Mapping of DG nodal IDs to CG nodal IDs */
    if (!(nextra < 1.0)) {
      loop_ub = (int)floor(nextra - 1.0);
      for (i = 0; i <= loop_ub; i++) {
        NodeCGDG->data[12 * i] = (double)i + 1.0;
      }
    }

    i = (int)nummat;
    for (nume = 0; nume < i; nume++) {
      /*  ID for material pair (row=mat2, col=mat1) */
      if (InterTypes->data[nume + InterTypes->size[0] * nume] > 0.0) {
        /*  Find elements belonging to material mat2 */
        i1 = b_NodesOnPBCnum->size[0];
        b_NodesOnPBCnum->size[0] = RegionOnElement->size[0];
        emxEnsureCapacity_boolean_T(b_NodesOnPBCnum, i1);
        loop_ub = RegionOnElement->size[0];
        for (i1 = 0; i1 < loop_ub; i1++) {
          b_NodesOnPBCnum->data[i1] = (RegionOnElement->data[i1] == (double)nume
            + 1.0);
        }

        eml_find(b_NodesOnPBCnum, t6_rowidx);
        i1 = MorePBC->size[0];
        MorePBC->size[0] = t6_rowidx->size[0];
        emxEnsureCapacity_real_T(MorePBC, i1);
        loop_ub = t6_rowidx->size[0];
        for (i1 = 0; i1 < loop_ub; i1++) {
          MorePBC->data[i1] = t6_rowidx->data[i1];
        }

        /*  Loop over elements in material mat2, explode all the elements */
        i1 = MorePBC->size[0];
        if (0 <= i1 - 1) {
          if (1.0 > nen) {
            c_loop_ub = 0;
          } else {
            c_loop_ub = (int)nen;
          }

          nodeC2_size[0] = 1;
          nodeC2_size[1] = c_loop_ub;
        }

        for (b_i = 0; b_i < i1; b_i++) {
          d = MorePBC->data[b_i];
          for (i2 = 0; i2 < c_loop_ub; i2++) {
            nodeC2_data[i2] = NodesOnElementDG->data[((int)d +
              NodesOnElementDG->size[0] * i2) - 1];
          }

          i2 = intnnz(nodeC2_data, nodeC2_size);
          for (locN = 0; locN < i2; locN++) {
            /*  Loop over local Nodes */
            node = NodesOnElementCG->data[((int)d + NodesOnElementCG->size[0] *
              locN) - 1];
            d1 = ElementsOnNodeNum2->data[(int)node - 1];
            if (d1 == 0.0) {
              /*  only duplicate nodes after the first time encountered */
              ElementsOnNodeNum2->data[(int)node - 1] = 1.0;
              NodeCGDG->data[12 * ((int)node - 1)] = node;

              /* node in DG mesh with same coordinates */
            } else {
              nextra++;
              NodesOnElementDG->data[((int)d + NodesOnElementDG->size[0] * locN)
                - 1] = nextra;
              Coordinates3->data[(int)nextra - 1] = Coordinates3->data[(int)node
                - 1];
              Coordinates3->data[((int)nextra + Coordinates3->size[0]) - 1] =
                Coordinates3->data[((int)node + Coordinates3->size[0]) - 1];
              numA = NodesOnElement->data[((int)d + NodesOnElement->size[0] *
                locN) - 1];
              b_i2 = ElementsOnNodeNum->data[(int)numA - 1];
              if (b_i2 < 1.0) {
                setAll->size[0] = 1;
                setAll->size[1] = 0;
              } else if (rtIsInf(b_i2) && (1.0 == b_i2)) {
                i3 = setAll->size[0] * setAll->size[1];
                setAll->size[0] = 1;
                setAll->size[1] = 1;
                emxEnsureCapacity_real_T(setAll, i3);
                setAll->data[0] = rtNaN;
              } else {
                i3 = setAll->size[0] * setAll->size[1];
                setAll->size[0] = 1;
                loop_ub = (int)floor(b_i2 - 1.0);
                setAll->size[1] = loop_ub + 1;
                emxEnsureCapacity_real_T(setAll, i3);
                for (i3 = 0; i3 <= loop_ub; i3++) {
                  setAll->data[i3] = (double)i3 + 1.0;
                }
              }

              b_sparse_parenReference(ElementsOnNode->d, ElementsOnNode->colidx,
                ElementsOnNode->rowidx, setAll, numA, NodeRegSum.d,
                NodeRegSum.colidx, NodeRegSum.rowidx, &NodeRegSum.m);
              c_sparse_eq((int)d, NodeRegSum.d, NodeRegSum.colidx,
                          NodeRegSum.rowidx, NodeRegSum.m, &c_expl_temp);
              i3 = b_expl_temp.colidx->size[0];
              b_expl_temp.colidx->size[0] = c_expl_temp.colidx->size[0];
              emxEnsureCapacity_int32_T(b_expl_temp.colidx, i3);
              loop_ub = c_expl_temp.colidx->size[0];
              for (i3 = 0; i3 < loop_ub; i3++) {
                b_expl_temp.colidx->data[i3] = c_expl_temp.colidx->data[i3];
              }

              i3 = b_expl_temp.rowidx->size[0];
              b_expl_temp.rowidx->size[0] = c_expl_temp.rowidx->size[0];
              emxEnsureCapacity_int32_T(b_expl_temp.rowidx, i3);
              loop_ub = c_expl_temp.rowidx->size[0];
              for (i3 = 0; i3 < loop_ub; i3++) {
                b_expl_temp.rowidx->data[i3] = c_expl_temp.rowidx->data[i3];
              }

              g_eml_find(b_expl_temp.colidx, b_expl_temp.rowidx, c_expl_temp.m,
                         ii);
              i3 = t4_d->size[0];
              t4_d->size[0] = ii->size[0];
              emxEnsureCapacity_real_T(t4_d, i3);
              loop_ub = ii->size[0];
              for (i3 = 0; i3 < loop_ub; i3++) {
                t4_d->data[i3] = ii->data[i3];
              }

              c_sparse_parenAssign(ElementsOnNodeDup, nextra, t4_d, numA);

              /*    Add element to star list, increment number of elem in star */
              ElementsOnNodeNum2->data[(int)node - 1]++;
              NodeCGDG->data[((int)(d1 + 1.0) + 12 * ((int)node - 1)) - 1] =
                nextra;

              /* node in DG mesh with same coordinates */
            }
          }

          /* k */
        }
      }
    }

    i = b_NodesOnPBCnum->size[0];
    b_NodesOnPBCnum->size[0] = ElementsOnNodeNum2->size[0];
    emxEnsureCapacity_boolean_T(b_NodesOnPBCnum, i);
    loop_ub = ElementsOnNodeNum2->size[0];
    for (i = 0; i < loop_ub; i++) {
      b_NodesOnPBCnum->data[i] = (ElementsOnNodeNum2->data[i] == 0.0);
    }

    eml_find(b_NodesOnPBCnum, t6_rowidx);
    loop_ub = t6_rowidx->size[0];
    for (i = 0; i < loop_ub; i++) {
      ElementsOnNodeNum2->data[t6_rowidx->data[i] - 1] = 1.0;
    }
  } else {
    /*  no DG in interior of any materials */
    i = ElementsOnNodeNum2->size[0];
    ElementsOnNodeNum2->size[0] = (int)nextra;
    emxEnsureCapacity_real_T(ElementsOnNodeNum2, i);
    loop_ub = (int)nextra;
    for (i = 0; i < loop_ub; i++) {
      ElementsOnNodeNum2->data[i] = 1.0;
    }

    /*  set one copy of duplicated nodes per material ID so that BC expanding subroutine works properly. */
    i = NodeCGDG->size[0] * NodeCGDG->size[1];
    NodeCGDG->size[0] = 12;
    NodeCGDG->size[1] = (int)nextra;
    emxEnsureCapacity_real_T(NodeCGDG, i);
    loop_ub = 12 * (int)nextra;
    for (i = 0; i < loop_ub; i++) {
      NodeCGDG->data[i] = 0.0;
    }

    /*  Mapping of DG nodal IDs to CG nodal IDs */
    if (!(nextra < 1.0)) {
      loop_ub = (int)floor(nextra - 1.0);
      for (i = 0; i <= loop_ub; i++) {
        NodeCGDG->data[12 * i] = (double)i + 1.0;
      }
    }
  }

  emxFree_boolean_T(&b_NodesOnPBCnum);
  emxFreeStruct_sparse3(&c_expl_temp);
  emxFreeStruct_sparse3(&b_expl_temp);
  emxFree_int32_T(&t6_rowidx);
  emxFree_real_T(&t4_d);
  emxFree_int32_T(&ii);
  emxFreeStruct_sparse1(&NodeRegSum);
  emxFree_real_T(&setAll);
  emxFree_real_T(&MorePBC);
  emxFree_real_T(&ElementsOnNodePBCNum);

  /*  Form final lists of DG interfaces */
  *numnp = nextra;
  if (1.0 > nextra) {
    loop_ub = 0;
  } else {
    loop_ub = (int)nextra;
  }

  i = Coordinates->size[0] * Coordinates->size[1];
  Coordinates->size[0] = loop_ub;
  Coordinates->size[1] = 2;
  emxEnsureCapacity_real_T(Coordinates, i);
  for (i = 0; i < loop_ub; i++) {
    Coordinates->data[i] = Coordinates3->data[i];
  }

  for (i = 0; i < loop_ub; i++) {
    Coordinates->data[i + Coordinates->size[0]] = Coordinates3->data[i +
      Coordinates3->size[0]];
  }

  emxFree_real_T(&Coordinates3);
  i = NodesOnElement->size[0] * NodesOnElement->size[1];
  NodesOnElement->size[0] = NodesOnElementDG->size[0];
  NodesOnElement->size[1] = NodesOnElementDG->size[1];
  emxEnsureCapacity_real_T(NodesOnElement, i);
  loop_ub = NodesOnElementDG->size[1];
  for (i = 0; i < loop_ub; i++) {
    nume = NodesOnElementDG->size[0];
    for (i1 = 0; i1 < nume; i1++) {
      NodesOnElement->data[i1 + NodesOnElement->size[0] * i] =
        NodesOnElementDG->data[i1 + NodesOnElementDG->size[0] * i];
    }
  }

  if (usePBC != 0.0) {
    i = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
    NodesOnElementCG->size[0] = NodesOnElementPBC->size[0];
    NodesOnElementCG->size[1] = NodesOnElementPBC->size[1];
    emxEnsureCapacity_real_T(NodesOnElementCG, i);
    loop_ub = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1];
    for (i = 0; i < loop_ub; i++) {
      NodesOnElementCG->data[i] = NodesOnElementPBC->data[i];
    }
  }

  emxFree_real_T(&NodesOnElementPBC);

  /*  %% Clear out intermediate variables */
  /*  if clearinter */
  /*  KeyList = {'numEonB','numEonF','ElementsOnBoundary','numSI','ElementsOnFacet',... */
  /*  'ElementsOnNode','ElementsOnNodeA','ElementsOnNodeB','ElementsOnNodeDup',... */
  /*  'ElementsOnNodeNum','numfac','ElementsOnNodeNum2','numinttype','FacetsOnElement',... */
  /*  'FacetsOnElementInt','FacetsOnInterface','FacetsOnInterfaceNum','FacetsOnNode',... */
  /*  'FacetsOnNodeCut','FacetsOnNodeInt','FacetsOnNodeNum','NodeCGDG','NodeReg',... */
  /*  'NodesOnElementCG','NodesOnElementDG','NodesOnInterface','NodesOnInterfaceNum',... */
  /*  'numCL','maxel','numelPBC','RegionOnElementDG','numPBC','TieNodesNew','TieNodes',... */
  /*  'NodesOnPBC','NodesOnPBCnum','NodesOnLink','NodesOnLinknum','numEonPBC',... */
  /*  'FacetsOnPBC','FacetsOnPBCNum','FacetsOnIntMinusPBC','FacetsOnIntMinusPBCNum','MPCList'}; */
  /*  if isOctave */
  /*      wholething = [{'-x'},KeyList,currvariables']; */
  /*      clear(wholething{:}) */
  /*  else */
  /*      wholething = [{'-except'},KeyList,currvariables']; */
  /*      clearvars(wholething{:}) */
  /*  end */
  /*  end */
  *numSI = numSI_tmp;
  *NodesOnInterfaceNum = locFA;
  *numCL = 0.0;
}

/* End of code generation (DEIPFunc2.c) */
