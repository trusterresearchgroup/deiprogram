/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sortrows.h
 *
 * Code generation for function 'sortrows'
 *
 */

#pragma once

/* Include files */
#include "DEIPFunc3_types.h"
#include "rtwtypes.h"
#include "emlrt.h"
#include "mex.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Function Declarations */
void apply_row_permutation(const emlrtStack *sp, emxArray_real_T *y, const
  emxArray_int32_T *idx);

/* End of code generation (sortrows.h) */
