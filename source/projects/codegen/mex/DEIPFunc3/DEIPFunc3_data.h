/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc3_data.h
 *
 * Code generation for function 'DEIPFunc3_data'
 *
 */

#pragma once

/* Include files */
#include "rtwtypes.h"
#include "emlrt.h"
#include "mex.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern const volatile char_T *emlrtBreakCheckR2012bFlagVar;
extern emlrtContext emlrtContextGlobal;
extern emlrtRSInfo bg_emlrtRSI;
extern emlrtRSInfo cg_emlrtRSI;
extern emlrtRSInfo fg_emlrtRSI;
extern emlrtRSInfo gg_emlrtRSI;
extern emlrtRSInfo hg_emlrtRSI;
extern emlrtRSInfo tg_emlrtRSI;
extern emlrtRSInfo ch_emlrtRSI;
extern emlrtRSInfo dh_emlrtRSI;
extern emlrtRSInfo hh_emlrtRSI;
extern emlrtRSInfo ih_emlrtRSI;
extern emlrtRSInfo jh_emlrtRSI;
extern emlrtRSInfo kh_emlrtRSI;
extern emlrtRSInfo oh_emlrtRSI;
extern emlrtRSInfo qh_emlrtRSI;
extern emlrtRSInfo rh_emlrtRSI;
extern emlrtRSInfo sj_emlrtRSI;
extern emlrtRSInfo tj_emlrtRSI;
extern emlrtRSInfo uj_emlrtRSI;
extern emlrtRSInfo vj_emlrtRSI;
extern emlrtRSInfo gk_emlrtRSI;
extern emlrtRSInfo ok_emlrtRSI;
extern emlrtRSInfo hl_emlrtRSI;
extern emlrtRSInfo gn_emlrtRSI;
extern emlrtRSInfo hn_emlrtRSI;
extern emlrtRSInfo in_emlrtRSI;
extern emlrtRSInfo jn_emlrtRSI;
extern emlrtRSInfo kn_emlrtRSI;
extern emlrtRSInfo mn_emlrtRSI;
extern emlrtRSInfo on_emlrtRSI;
extern emlrtRSInfo ao_emlrtRSI;
extern emlrtRSInfo lo_emlrtRSI;
extern emlrtRSInfo iq_emlrtRSI;
extern emlrtRTEInfo ab_emlrtRTEI;
extern emlrtRTEInfo cb_emlrtRTEI;
extern emlrtRTEInfo oc_emlrtRTEI;
extern emlrtRTEInfo rc_emlrtRTEI;
extern emlrtRTEInfo vc_emlrtRTEI;
extern emlrtRTEInfo xc_emlrtRTEI;
extern emlrtRTEInfo yc_emlrtRTEI;
extern emlrtRTEInfo ld_emlrtRTEI;
extern emlrtRTEInfo wd_emlrtRTEI;

/* End of code generation (DEIPFunc3_data.h) */
