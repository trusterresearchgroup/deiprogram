% 03/28/2021 TJT
% A self periodic microstructure containing 30 grains, 2D
% PBC imposed through Lagrange multipliers
% Loading: Prescribed macro strain of 0.01 

clear
% clc

%% Load Neper .msh file
perplan = 1;
innamePBC = 'n30-id1';
Gmshfile = [innamePBC '.msh'];
GmshInputReader

nen = 3;
% read periodic file
Perfile = [innamePBC '.per'];
fid = fopen(Perfile);
FormatString=repmat('%f',1,5);  % Create format string based on parameter
InputText=textscan(fid,FormatString); % Read data block
NeperPBC=cell2mat(InputText); % Convert to numerical array from cell
fclose(fid);
nummat=max(RegionOnElement);
numelCG = numel;

MateT = ones(nummat,1)*[100 .25 1];
MatTypeTable = [(1:nummat); ones(1,nummat)];
     
MPCList = NeperPBC;
% [~,inds] = unique(MPCList(:,2));
% MPCList = MPCList(inds,:);
numMPC = length(MPCList);

% Set rigid body modes
FIX = 1; % just pick one; do the strain BC...
NodeBC = [FIX 1 0
          FIX 2 0
          ];

%% Add the Lagrange multiplier nodes
CornerXYZ = [-1 0.000000
             0.000000 -1];
[Coordinates,NodesOnElement,RegionOnElement,MatTypeTable,MateT,...
    numnp,numel,nummat,nen,NodeTypeNum] = AddPBNodes(MPCList,CornerXYZ,Coordinates,...
    NodesOnElement,RegionOnElement,MatTypeTable,MateT,numnp,numel,nummat,nen);


% Output quantity flags
DHist = 1;
FHist = 1;
SHist = 1;
SEHist = 1;   

%% Apply macrostrain
PbcStra=0.01;

NodeLoad2 = [NodeTypeNum(2)   1 PbcStra
             NodeTypeNum(2)   2 0
             NodeTypeNum(2)+1 1 0
             NodeTypeNum(2)+1 2 0];
NodeBC = [NodeBC; NodeLoad2];
numBC = length(NodeBC);


ProbType = [numnp numel nummat ndm ndm nen];

 AlgoType = [0; 1; 0];


%% Plots for after FEA_Program is called

% % Plot the u_y displacement, takes a long time
% plotNodeCont2(Coordinates+Node_U_V*20, Node_U_V(:,1), NodesOnElement,100, 1:numelCG,1:numnp, [1 0 0])
% % Extract the macroscale strain and stress*volume
% [RVE_E,RVE_F] = GetMacroSS(DispList,ForcList,NodeTypeNum,ndm)
