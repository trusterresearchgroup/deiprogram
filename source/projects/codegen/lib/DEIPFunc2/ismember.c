/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * ismember.c
 *
 * Code generation for function 'ismember'
 *
 */

/* Include files */
#include "ismember.h"
#include "DEIPFunc2_emxutil.h"
#include "DEIPFunc2_types.h"
#include "rt_nonfinite.h"
#include "sort.h"
#include "rt_nonfinite.h"
#include <math.h>

/* Function Declarations */
static int bsearchni(int k, const emxArray_real_T *x, const emxArray_real_T *s);

/* Function Definitions */
static int bsearchni(int k, const emxArray_real_T *x, const emxArray_real_T *s)
{
  double b_x;
  int idx;
  int ihi;
  int ilo;
  int imid;
  boolean_T exitg1;
  boolean_T p;
  b_x = x->data[k - 1];
  ihi = s->size[0];
  idx = 0;
  ilo = 1;
  exitg1 = false;
  while ((!exitg1) && (ihi >= ilo)) {
    imid = ((ilo >> 1) + (ihi >> 1)) - 1;
    if (((ilo & 1) == 1) && ((ihi & 1) == 1)) {
      imid++;
    }

    if (b_x == s->data[imid]) {
      idx = imid + 1;
      exitg1 = true;
    } else {
      if (rtIsNaN(s->data[imid])) {
        p = !rtIsNaN(b_x);
      } else if (rtIsNaN(b_x)) {
        p = false;
      } else {
        p = (b_x < s->data[imid]);
      }

      if (p) {
        ihi = imid;
      } else {
        ilo = imid + 2;
      }
    }
  }

  if (idx > 0) {
    idx--;
    while ((idx > 0) && (b_x == s->data[idx - 1])) {
      idx--;
    }

    idx++;
  }

  return idx;
}

void b_local_ismember(const emxArray_real_T *a, const emxArray_real_T *s,
                      emxArray_boolean_T *tf)
{
  emxArray_int32_T *b_ss;
  emxArray_real_T *ss;
  double absx;
  double b;
  int exponent;
  int na;
  int ns;
  int p;
  int pmax;
  int pmin;
  int pow2p;
  boolean_T exitg1;
  boolean_T guard1 = false;
  boolean_T y;
  na = a->size[0] - 1;
  ns = s->size[0];
  pmin = tf->size[0] * tf->size[1];
  tf->size[0] = a->size[0];
  tf->size[1] = 1;
  emxEnsureCapacity_boolean_T(tf, pmin);
  pmax = a->size[0];
  for (pmin = 0; pmin < pmax; pmin++) {
    tf->data[pmin] = false;
  }

  emxInit_real_T(&ss, 1);
  emxInit_int32_T(&b_ss, 1);
  guard1 = false;
  if (s->size[0] <= 4) {
    guard1 = true;
  } else {
    pmax = 31;
    pmin = 0;
    exitg1 = false;
    while ((!exitg1) && (pmax - pmin > 1)) {
      p = (pmin + pmax) >> 1;
      pow2p = 1 << p;
      if (pow2p == ns) {
        pmax = p;
        exitg1 = true;
      } else if (pow2p > ns) {
        pmax = p;
      } else {
        pmin = p;
      }
    }

    if (a->size[0] <= pmax + 4) {
      guard1 = true;
    } else {
      y = true;
      if (s->size[0] != 1) {
        pmax = 0;
        exitg1 = false;
        while ((!exitg1) && (pmax <= s->size[0] - 2)) {
          absx = s->data[pmax + 1];
          if ((!(s->data[pmax] <= absx)) && (!rtIsNaN(absx))) {
            y = false;
          }

          if (!y) {
            exitg1 = true;
          } else {
            pmax++;
          }
        }
      }

      if (!y) {
        pmin = ss->size[0];
        ss->size[0] = s->size[0];
        emxEnsureCapacity_real_T(ss, pmin);
        pmax = s->size[0];
        for (pmin = 0; pmin < pmax; pmin++) {
          ss->data[pmin] = s->data[pmin];
        }

        sort(ss, b_ss);
        for (pmax = 0; pmax <= na; pmax++) {
          if (bsearchni(pmax + 1, a, ss) > 0) {
            tf->data[pmax] = true;
          }
        }
      } else {
        for (pmax = 0; pmax <= na; pmax++) {
          if (bsearchni(pmax + 1, a, s) > 0) {
            tf->data[pmax] = true;
          }
        }
      }
    }
  }

  if (guard1) {
    for (pmin = 0; pmin <= na; pmin++) {
      pmax = 0;
      exitg1 = false;
      while ((!exitg1) && (pmax <= ns - 1)) {
        b = s->data[pmax];
        absx = fabs(b / 2.0);
        if ((!rtIsInf(absx)) && (!rtIsNaN(absx))) {
          if (absx <= 2.2250738585072014E-308) {
            absx = 4.94065645841247E-324;
          } else {
            frexp(absx, &exponent);
            absx = ldexp(1.0, exponent - 53);
          }
        } else {
          absx = rtNaN;
        }

        if ((fabs(s->data[pmax] - a->data[pmin]) < absx) || (rtIsInf(a->
              data[pmin]) && rtIsInf(b) && ((a->data[pmin] > 0.0) == (s->
               data[pmax] > 0.0)))) {
          tf->data[pmin] = true;
          exitg1 = true;
        } else {
          pmax++;
        }
      }
    }
  }

  emxFree_int32_T(&b_ss);
  emxFree_real_T(&ss);
}

boolean_T local_ismember(double a, const double s_data[], const int s_size[1])
{
  double absx;
  int exponent;
  int k;
  boolean_T exitg1;
  boolean_T tf;
  tf = false;
  k = 0;
  exitg1 = false;
  while ((!exitg1) && (k <= s_size[0] - 1)) {
    absx = fabs(s_data[k] / 2.0);
    if ((!rtIsInf(absx)) && (!rtIsNaN(absx))) {
      if (absx <= 2.2250738585072014E-308) {
        absx = 4.94065645841247E-324;
      } else {
        frexp(absx, &exponent);
        absx = ldexp(1.0, exponent - 53);
      }
    } else {
      absx = rtNaN;
    }

    if ((fabs(s_data[k] - a) < absx) || (rtIsInf(a) && rtIsInf(s_data[k]) && ((a
           > 0.0) == (s_data[k] > 0.0)))) {
      tf = true;
      exitg1 = true;
    } else {
      k++;
    }
  }

  return tf;
}

/* End of code generation (ismember.c) */
