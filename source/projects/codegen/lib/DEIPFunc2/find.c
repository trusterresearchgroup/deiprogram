/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * find.c
 *
 * Code generation for function 'find'
 *
 */

/* Include files */
#include "find.h"
#include "DEIPFunc2_emxutil.h"
#include "DEIPFunc2_types.h"
#include "rt_nonfinite.h"

/* Function Definitions */
void b_eml_find(const emxArray_real_T *x, emxArray_int32_T *i)
{
  int idx;
  int ii;
  int nx;
  boolean_T exitg1;
  nx = x->size[0] * x->size[1];
  idx = 0;
  ii = i->size[0];
  i->size[0] = nx;
  emxEnsureCapacity_int32_T(i, ii);
  ii = 0;
  exitg1 = false;
  while ((!exitg1) && (ii <= nx - 1)) {
    if (x->data[ii] != 0.0) {
      idx++;
      i->data[idx - 1] = ii + 1;
      if (idx >= nx) {
        exitg1 = true;
      } else {
        ii++;
      }
    } else {
      ii++;
    }
  }

  if (nx == 1) {
    if (idx == 0) {
      i->size[0] = 0;
    }
  } else {
    ii = i->size[0];
    if (1 > idx) {
      i->size[0] = 0;
    } else {
      i->size[0] = idx;
    }

    emxEnsureCapacity_int32_T(i, ii);
  }
}

void c_eml_find(const emxArray_real_T *x, emxArray_int32_T *i)
{
  int idx;
  int ii;
  int nx;
  boolean_T exitg1;
  nx = x->size[0];
  idx = 0;
  ii = i->size[0];
  i->size[0] = x->size[0];
  emxEnsureCapacity_int32_T(i, ii);
  ii = 0;
  exitg1 = false;
  while ((!exitg1) && (ii <= nx - 1)) {
    if (x->data[ii] != 0.0) {
      idx++;
      i->data[idx - 1] = ii + 1;
      if (idx >= nx) {
        exitg1 = true;
      } else {
        ii++;
      }
    } else {
      ii++;
    }
  }

  if (x->size[0] == 1) {
    if (idx == 0) {
      i->size[0] = 0;
    }
  } else {
    ii = i->size[0];
    if (1 > idx) {
      i->size[0] = 0;
    } else {
      i->size[0] = idx;
    }

    emxEnsureCapacity_int32_T(i, ii);
  }
}

void d_eml_find(const emxArray_real_T *x, emxArray_int32_T *i)
{
  int idx;
  int ii;
  int nx;
  boolean_T exitg1;
  nx = x->size[1];
  idx = 0;
  ii = i->size[0] * i->size[1];
  i->size[0] = 1;
  i->size[1] = x->size[1];
  emxEnsureCapacity_int32_T(i, ii);
  ii = 0;
  exitg1 = false;
  while ((!exitg1) && (ii <= nx - 1)) {
    if (x->data[ii] != 0.0) {
      idx++;
      i->data[idx - 1] = ii + 1;
      if (idx >= nx) {
        exitg1 = true;
      } else {
        ii++;
      }
    } else {
      ii++;
    }
  }

  if (x->size[1] == 1) {
    if (idx == 0) {
      i->size[0] = 1;
      i->size[1] = 0;
    }
  } else {
    ii = i->size[0] * i->size[1];
    if (1 > idx) {
      i->size[1] = 0;
    } else {
      i->size[1] = idx;
    }

    emxEnsureCapacity_int32_T(i, ii);
  }
}

void e_eml_find(const emxArray_int32_T *x_colidx, const emxArray_int32_T
                *x_rowidx, emxArray_int32_T *i, emxArray_int32_T *j)
{
  int col;
  int idx;
  int nx;
  nx = x_colidx->data[x_colidx->size[0] - 1] - 1;
  if (x_colidx->data[x_colidx->size[0] - 1] - 1 == 0) {
    i->size[0] = 0;
    j->size[0] = 0;
  } else {
    col = i->size[0];
    i->size[0] = x_colidx->data[x_colidx->size[0] - 1] - 1;
    emxEnsureCapacity_int32_T(i, col);
    col = j->size[0];
    j->size[0] = x_colidx->data[x_colidx->size[0] - 1] - 1;
    emxEnsureCapacity_int32_T(j, col);
    for (idx = 0; idx < nx; idx++) {
      i->data[idx] = x_rowidx->data[idx];
    }

    idx = 0;
    col = 1;
    while (idx < nx) {
      if (idx == x_colidx->data[col] - 1) {
        col++;
      } else {
        idx++;
        j->data[idx - 1] = col;
      }
    }

    if (x_colidx->data[x_colidx->size[0] - 1] - 1 == 1) {
      if (idx == 0) {
        i->size[0] = 0;
        j->size[0] = 0;
      }
    } else {
      col = i->size[0];
      if (1 > idx) {
        i->size[0] = 0;
      } else {
        i->size[0] = idx;
      }

      emxEnsureCapacity_int32_T(i, col);
      col = j->size[0];
      if (1 > idx) {
        j->size[0] = 0;
      } else {
        j->size[0] = idx;
      }

      emxEnsureCapacity_int32_T(j, col);
    }
  }
}

void eml_find(const emxArray_boolean_T *x, emxArray_int32_T *i)
{
  int idx;
  int ii;
  int nx;
  boolean_T exitg1;
  nx = x->size[0];
  idx = 0;
  ii = i->size[0];
  i->size[0] = x->size[0];
  emxEnsureCapacity_int32_T(i, ii);
  ii = 0;
  exitg1 = false;
  while ((!exitg1) && (ii <= nx - 1)) {
    if (x->data[ii]) {
      idx++;
      i->data[idx - 1] = ii + 1;
      if (idx >= nx) {
        exitg1 = true;
      } else {
        ii++;
      }
    } else {
      ii++;
    }
  }

  if (x->size[0] == 1) {
    if (idx == 0) {
      i->size[0] = 0;
    }
  } else {
    ii = i->size[0];
    if (1 > idx) {
      i->size[0] = 0;
    } else {
      i->size[0] = idx;
    }

    emxEnsureCapacity_int32_T(i, ii);
  }
}

void f_eml_find(const boolean_T x_data[], const int x_size[2], int i_data[], int
                i_size[2])
{
  int idx;
  int ii;
  int nx;
  boolean_T exitg1;
  nx = x_size[1];
  idx = 0;
  i_size[0] = 1;
  i_size[1] = x_size[1];
  ii = 0;
  exitg1 = false;
  while ((!exitg1) && (ii <= nx - 1)) {
    if (x_data[ii]) {
      idx++;
      i_data[idx - 1] = ii + 1;
      if (idx >= nx) {
        exitg1 = true;
      } else {
        ii++;
      }
    } else {
      ii++;
    }
  }

  if (x_size[1] == 1) {
    if (idx == 0) {
      i_size[0] = 1;
      i_size[1] = 0;
    }
  } else if (1 > idx) {
    i_size[1] = 0;
  } else {
    i_size[1] = idx;
  }
}

void g_eml_find(const emxArray_int32_T *x_colidx, const emxArray_int32_T
                *x_rowidx, int x_m, emxArray_int32_T *i)
{
  int col;
  int idx;
  idx = 0;
  col = i->size[0];
  i->size[0] = x_colidx->data[x_colidx->size[0] - 1] - 1;
  emxEnsureCapacity_int32_T(i, col);
  col = 1;
  while (idx < x_colidx->data[x_colidx->size[0] - 1] - 1) {
    if (idx == x_colidx->data[col] - 1) {
      col++;
    } else {
      idx++;
      i->data[idx - 1] = (col - 1) * x_m + x_rowidx->data[idx - 1];
    }
  }

  if (x_colidx->data[x_colidx->size[0] - 1] - 1 == 1) {
    if (idx == 0) {
      i->size[0] = 0;
    }
  } else {
    col = i->size[0];
    if (1 > idx) {
      i->size[0] = 0;
    } else {
      i->size[0] = idx;
    }

    emxEnsureCapacity_int32_T(i, col);
  }
}

/* End of code generation (find.c) */
