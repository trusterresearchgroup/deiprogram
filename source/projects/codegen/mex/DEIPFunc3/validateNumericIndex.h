/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * validateNumericIndex.h
 *
 * Code generation for function 'validateNumericIndex'
 *
 */

#pragma once

/* Include files */
#include "DEIPFunc3_types.h"
#include "rtwtypes.h"
#include "emlrt.h"
#include "mex.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Function Declarations */
void b_sparse_validateNumericIndex(const emlrtStack *sp, int32_T upperBound,
  real_T idx);
void c_sparse_validateNumericIndex(const emlrtStack *sp, int32_T upperBound,
  const emxArray_real_T *idx);
void sparse_validateNumericIndex(const emlrtStack *sp, int32_T upperBound, const
  emxArray_real_T *idx);

/* End of code generation (validateNumericIndex.h) */
