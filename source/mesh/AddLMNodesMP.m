function [Coordinates,NodesOnElement,RegionOnElement,MatTypeTable,MateT,numnp,numel,nummat,nen] = AddLMNodesMP(MPListN,Coordinates,NodesOnElement,...
    RegionOnElement,MatTypeTable,MateT,numnp,numel,nummat,nen,maxMPC,numMPC)
%
% Tim Truster
% 02/11/2017
%
% Insert the Lagrange Multiplier nodes into a mesh for periodic BC modeling
%
% PBCmat = material type ID for PBC elements
% PBCList = list of P BC, which is expressed in the order that Abaqus
% usually gives:
% [u_-,1, -1.0, u_+,1, 1.0, u_master_+,1, 1.0, u_master_-,1, -1.0]
% [u_-,2, -1.0, u_+,2, 1.0, u_master_+,2, 1.0, u_master_-,2, -1.0] 
% [u_-,3, -1.0, u_+,3, 1.0, u_master_+,3, 1.0, u_master_-,3, -1.0]
% Simplified and reordered to: 
% [u_+, u_-, u_master_+, u_master_-]

PBCmat = nummat + 1;
ndm = size(Coordinates,2);
numPBC = size(MPListN,1);

% expand materials
nummat = nummat + 1;
MatTypeTable = [MatTypeTable [nummat; 29; zeros(size(MatTypeTable,1)-2,1)]];
MateT = [MateT; zeros(1,size(MateT,2))];

% expand elements
nenPBC = maxMPC+1;
if nen < nenPBC
    NodesOnElement = [NodesOnElement zeros(numel,nenPBC-nen)];
    nen = nenPBC;
end
RegionOnElement = [RegionOnElement; PBCmat*ones(numPBC,1)];
PBCnodes = (numnp+1:numnp+numPBC)';
NodesOnElement = [NodesOnElement; [MPListN PBCnodes zeros(numPBC,nen-nenPBC)]];
% Move the LM node over to the left, so that there are no intermediate zero
% nodes
for j = 1:numPBC
    numN = nnz(MPListN(j,:)); % number of nodes in the constraint
    if numN < maxMPC
    NodesOnElement(j+numel,numN+1) = NodesOnElement(j+numel,maxMPC+1);
    NodesOnElement(j+numel,maxMPC+1) = 0; % Delete the entry at the maxMPC location, caused by line 35 above
    end
end
numel = numel + numPBC;

% expand nodes
numnp = numnp + numPBC;
Coordinates = [Coordinates; zeros(numPBC,ndm)];

