/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * insertionsort.c
 *
 * Code generation for function 'insertionsort'
 *
 */

/* Include files */
#include "insertionsort.h"
#include "DEIPFunc3_data.h"
#include "DEIPFunc3_types.h"
#include "eml_int_forloop_overflow_check.h"
#include "rt_nonfinite.h"

/* Variable Definitions */
static emlrtRSInfo tk_emlrtRSI = { 18, /* lineNo */
  "insertionsort",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\lib\\+coder\\+internal\\insertionsort.m"/* pathName */
};

/* Function Definitions */
void b_insertionsort(const emlrtStack *sp, emxArray_int32_T *x, int32_T xstart,
                     int32_T xend, const cell_wrap_6 cmp_tunableEnvironment[1])
{
  emlrtStack b_st;
  emlrtStack st;
  int32_T a;
  int32_T idx;
  int32_T k;
  int32_T xc;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  a = xstart + 1;
  st.site = &tk_emlrtRSI;
  if ((xstart + 1 <= xend) && (xend > 2147483646)) {
    b_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  for (k = a; k <= xend; k++) {
    xc = x->data[k - 1];
    idx = k - 1;
    while ((idx >= xstart) && (cmp_tunableEnvironment[0].f1->data[xc - 1] <
            cmp_tunableEnvironment[0].f1->data[x->data[idx - 1] - 1])) {
      x->data[idx] = x->data[idx - 1];
      idx--;
    }

    x->data[idx] = xc;
  }
}

void insertionsort(const emlrtStack *sp, emxArray_int32_T *x, int32_T xstart,
                   int32_T xend, const cell_wrap_6 cmp_tunableEnvironment[2])
{
  emlrtStack b_st;
  emlrtStack st;
  int32_T a;
  int32_T idx;
  int32_T k;
  int32_T xc;
  boolean_T exitg1;
  boolean_T varargout_1;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  a = xstart + 1;
  st.site = &tk_emlrtRSI;
  if ((xstart + 1 <= xend) && (xend > 2147483646)) {
    b_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  for (k = a; k <= xend; k++) {
    xc = x->data[k - 1] - 1;
    idx = k - 2;
    exitg1 = false;
    while ((!exitg1) && (idx + 1 >= xstart)) {
      if (cmp_tunableEnvironment[0].f1->data[xc] < cmp_tunableEnvironment[0].
          f1->data[x->data[idx] - 1]) {
        varargout_1 = true;
      } else if (cmp_tunableEnvironment[0].f1->data[xc] ==
                 cmp_tunableEnvironment[0].f1->data[x->data[idx] - 1]) {
        varargout_1 = (cmp_tunableEnvironment[1].f1->data[xc] <
                       cmp_tunableEnvironment[1].f1->data[x->data[idx] - 1]);
      } else {
        varargout_1 = false;
      }

      if (varargout_1) {
        x->data[idx + 1] = x->data[idx];
        idx--;
      } else {
        exitg1 = true;
      }
    }

    x->data[idx + 1] = xc + 1;
  }
}

/* End of code generation (insertionsort.c) */
