/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc2_data.c
 *
 * Code generation for function 'DEIPFunc2_data'
 *
 */

/* Include files */
#include "DEIPFunc2_data.h"
#include "rt_nonfinite.h"

/* Variable Definitions */
boolean_T isInitialized_DEIPFunc2 = false;

/* End of code generation (DEIPFunc2_data.c) */
