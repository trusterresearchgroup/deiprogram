/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_DEIPFunc3_api.c
 *
 * Code generation for function '_coder_DEIPFunc3_api'
 *
 */

/* Include files */
#include "_coder_DEIPFunc3_api.h"
#include "DEIPFunc3.h"
#include "DEIPFunc3_data.h"
#include "DEIPFunc3_emxutil.h"
#include "DEIPFunc3_mexutil.h"
#include "DEIPFunc3_types.h"
#include "rt_nonfinite.h"

/* Variable Definitions */
static emlrtRTEInfo if_emlrtRTEI = { 1,/* lineNo */
  1,                                   /* colNo */
  "_coder_DEIPFunc3_api",              /* fName */
  ""                                   /* pName */
};

/* Function Declarations */
static void c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *InterTypes,
  const char_T *identifier, emxArray_real_T *y);
static void d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y);
static void e_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *NodesOnElement, const char_T *identifier, emxArray_real_T *y);
static void f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y);
static void g_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *RegionOnElement, const char_T *identifier, emxArray_real_T *y);
static void h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y);
static void i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *Coordinates,
  const char_T *identifier, emxArray_real_T *y);
static void j_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y);
static void k_emlrt_marshallIn(const emlrtStack *sp, const mxArray *MPCList,
  const char_T *identifier, emxArray_real_T *y);
static void k_emlrt_marshallOut(const emxArray_real_T *u, const mxArray *y);
static void l_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y);
static void l_emlrt_marshallOut(const emxArray_real_T *u, const mxArray *y);
static const mxArray *m_emlrt_marshallOut(const emxArray_real_T *u);
static void n_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret);
static const mxArray *n_emlrt_marshallOut(const emxArray_real_T *u);
static void o_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret);
static const mxArray *o_emlrt_marshallOut(const e_sparse u);
static void p_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret);
static void q_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret);
static void r_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret);

/* Function Definitions */
static void c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *InterTypes,
  const char_T *identifier, emxArray_real_T *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char_T *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  d_emlrt_marshallIn(sp, emlrtAlias(InterTypes), &thisId, y);
  emlrtDestroyArray(&InterTypes);
}

static void d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y)
{
  n_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void e_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *NodesOnElement, const char_T *identifier, emxArray_real_T *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char_T *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  f_emlrt_marshallIn(sp, emlrtAlias(NodesOnElement), &thisId, y);
  emlrtDestroyArray(&NodesOnElement);
}

static void f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y)
{
  o_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void g_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *RegionOnElement, const char_T *identifier, emxArray_real_T *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char_T *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  h_emlrt_marshallIn(sp, emlrtAlias(RegionOnElement), &thisId, y);
  emlrtDestroyArray(&RegionOnElement);
}

static void h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y)
{
  p_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *Coordinates,
  const char_T *identifier, emxArray_real_T *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char_T *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  j_emlrt_marshallIn(sp, emlrtAlias(Coordinates), &thisId, y);
  emlrtDestroyArray(&Coordinates);
}

static void j_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y)
{
  q_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void k_emlrt_marshallIn(const emlrtStack *sp, const mxArray *MPCList,
  const char_T *identifier, emxArray_real_T *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char_T *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  l_emlrt_marshallIn(sp, emlrtAlias(MPCList), &thisId, y);
  emlrtDestroyArray(&MPCList);
}

static void k_emlrt_marshallOut(const emxArray_real_T *u, const mxArray *y)
{
  emlrtMxSetData((mxArray *)y, &u->data[0]);
  emlrtSetDimensions((mxArray *)y, u->size, 2);
}

static void l_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y)
{
  r_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void l_emlrt_marshallOut(const emxArray_real_T *u, const mxArray *y)
{
  emlrtMxSetData((mxArray *)y, &u->data[0]);
  emlrtSetDimensions((mxArray *)y, u->size, 1);
}

static const mxArray *m_emlrt_marshallOut(const emxArray_real_T *u)
{
  static const int32_T iv[1] = { 0 };

  const mxArray *m;
  const mxArray *y;
  y = NULL;
  m = emlrtCreateNumericArray(1, &iv[0], mxDOUBLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m, &u->data[0]);
  emlrtSetDimensions((mxArray *)m, u->size, 1);
  emlrtAssign(&y, m);
  return y;
}

static void n_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret)
{
  static const int32_T dims[2] = { -1, -1 };

  int32_T iv[2];
  int32_T i;
  const boolean_T bv[2] = { true, true };

  emlrtCheckVsBuiltInR2012b(sp, msgId, src, "double", false, 2U, dims, &bv[0],
    iv);
  ret->allocatedSize = iv[0] * iv[1];
  i = ret->size[0] * ret->size[1];
  ret->size[0] = iv[0];
  ret->size[1] = iv[1];
  emxEnsureCapacity_real_T(sp, ret, i, (emlrtRTEInfo *)NULL);
  ret->data = (real_T *)emlrtMxGetData(src);
  ret->canFreeData = false;
  emlrtDestroyArray(&src);
}

static const mxArray *n_emlrt_marshallOut(const emxArray_real_T *u)
{
  static const int32_T iv[2] = { 0, 0 };

  const mxArray *m;
  const mxArray *y;
  y = NULL;
  m = emlrtCreateNumericArray(2, &iv[0], mxDOUBLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m, &u->data[0]);
  emlrtSetDimensions((mxArray *)m, u->size, 2);
  emlrtAssign(&y, m);
  return y;
}

static void o_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret)
{
  static const int32_T dims[2] = { -1, 27 };

  int32_T iv[2];
  int32_T i;
  const boolean_T bv[2] = { true, true };

  emlrtCheckVsBuiltInR2012b(sp, msgId, src, "double", false, 2U, dims, &bv[0],
    iv);
  ret->allocatedSize = iv[0] * iv[1];
  i = ret->size[0] * ret->size[1];
  ret->size[0] = iv[0];
  ret->size[1] = iv[1];
  emxEnsureCapacity_real_T(sp, ret, i, (emlrtRTEInfo *)NULL);
  ret->data = (real_T *)emlrtMxGetData(src);
  ret->canFreeData = false;
  emlrtDestroyArray(&src);
}

static const mxArray *o_emlrt_marshallOut(const e_sparse u)
{
  const mxArray *y;
  y = NULL;
  emlrtAssign(&y, emlrtCreateSparse(&u.d->data[0], &u.colidx->data[0],
    &u.rowidx->data[0], u.m, u.n, u.maxnz, mxDOUBLE_CLASS, mxREAL));
  return y;
}

static void p_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret)
{
  static const int32_T dims[1] = { -1 };

  int32_T iv[1];
  int32_T i;
  const boolean_T bv[1] = { true };

  emlrtCheckVsBuiltInR2012b(sp, msgId, src, "double", false, 1U, dims, &bv[0],
    iv);
  ret->allocatedSize = iv[0];
  i = ret->size[0];
  ret->size[0] = iv[0];
  emxEnsureCapacity_real_T(sp, ret, i, (emlrtRTEInfo *)NULL);
  ret->data = (real_T *)emlrtMxGetData(src);
  ret->canFreeData = false;
  emlrtDestroyArray(&src);
}

static void q_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret)
{
  static const int32_T dims[2] = { -1, 3 };

  int32_T iv[2];
  int32_T i;
  const boolean_T bv[2] = { true, false };

  emlrtCheckVsBuiltInR2012b(sp, msgId, src, "double", false, 2U, dims, &bv[0],
    iv);
  ret->allocatedSize = iv[0] * iv[1];
  i = ret->size[0] * ret->size[1];
  ret->size[0] = iv[0];
  ret->size[1] = iv[1];
  emxEnsureCapacity_real_T(sp, ret, i, (emlrtRTEInfo *)NULL);
  ret->data = (real_T *)emlrtMxGetData(src);
  ret->canFreeData = false;
  emlrtDestroyArray(&src);
}

static void r_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret)
{
  static const int32_T dims[2] = { -1, 5 };

  int32_T iv[2];
  int32_T i;
  const boolean_T bv[2] = { true, false };

  emlrtCheckVsBuiltInR2012b(sp, msgId, src, "double", false, 2U, dims, &bv[0],
    iv);
  ret->allocatedSize = iv[0] * iv[1];
  i = ret->size[0] * ret->size[1];
  ret->size[0] = iv[0];
  ret->size[1] = iv[1];
  emxEnsureCapacity_real_T(sp, ret, i, (emlrtRTEInfo *)NULL);
  ret->data = (real_T *)emlrtMxGetData(src);
  ret->canFreeData = false;
  emlrtDestroyArray(&src);
}

void DEIPFunc3_api(const mxArray * const prhs[12], int32_T nlhs, const mxArray
                   *plhs[41])
{
  e_sparse ElementsOnNode;
  e_sparse ElementsOnNodeDup;
  e_sparse FacetsOnNode;
  e_sparse FacetsOnNodeCut;
  e_sparse FacetsOnNodeInt;
  e_sparse NodeReg;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  emxArray_real_T *Coordinates;
  emxArray_real_T *ElementsOnBoundary;
  emxArray_real_T *ElementsOnFacet;
  emxArray_real_T *ElementsOnNodeNum;
  emxArray_real_T *ElementsOnNodeNum2;
  emxArray_real_T *FacetsOnElement;
  emxArray_real_T *FacetsOnElementInt;
  emxArray_real_T *FacetsOnIntMinusPBC;
  emxArray_real_T *FacetsOnIntMinusPBCNum;
  emxArray_real_T *FacetsOnInterface;
  emxArray_real_T *FacetsOnInterfaceNum;
  emxArray_real_T *FacetsOnNodeNum;
  emxArray_real_T *FacetsOnPBC;
  emxArray_real_T *FacetsOnPBCNum;
  emxArray_real_T *InterTypes;
  emxArray_real_T *MPCList;
  emxArray_real_T *NodeCGDG;
  emxArray_real_T *NodesOnElement;
  emxArray_real_T *NodesOnElementCG;
  emxArray_real_T *NodesOnElementDG;
  emxArray_real_T *NodesOnInterface;
  emxArray_real_T *NodesOnLink;
  emxArray_real_T *NodesOnLinknum;
  emxArray_real_T *NodesOnPBC;
  emxArray_real_T *NodesOnPBCnum;
  emxArray_real_T *RegionOnElement;
  emxArray_real_T *numEonF;
  emxArray_real_T *numEonPBC;
  const mxArray *prhs_copy_idx_0;
  const mxArray *prhs_copy_idx_1;
  const mxArray *prhs_copy_idx_11;
  const mxArray *prhs_copy_idx_2;
  const mxArray *prhs_copy_idx_3;
  real_T NodesOnInterfaceNum;
  real_T ndm;
  real_T nen;
  real_T numCL;
  real_T numEonB;
  real_T numMPC;
  real_T numSI;
  real_T numel;
  real_T numfac;
  real_T numinttype;
  real_T nummat;
  real_T numnp;
  real_T usePBC;
  st.tls = emlrtRootTLSGlobal;
  emlrtHeapReferenceStackEnterFcnR2012b(&st);
  emxInit_real_T(&st, &InterTypes, 2, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &NodesOnElement, 2, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &RegionOnElement, 1, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &Coordinates, 2, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &MPCList, 2, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &numEonF, 1, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &ElementsOnBoundary, 2, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &ElementsOnFacet, 2, &if_emlrtRTEI, true);
  emxInitStruct_sparse1(&st, &ElementsOnNode, &if_emlrtRTEI, true);
  emxInitStruct_sparse1(&st, &ElementsOnNodeDup, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &ElementsOnNodeNum, 1, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &ElementsOnNodeNum2, 1, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &FacetsOnElement, 2, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &FacetsOnElementInt, 2, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &FacetsOnInterface, 1, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &FacetsOnInterfaceNum, 1, &if_emlrtRTEI, true);
  emxInitStruct_sparse1(&st, &FacetsOnNode, &if_emlrtRTEI, true);
  emxInitStruct_sparse1(&st, &FacetsOnNodeCut, &if_emlrtRTEI, true);
  emxInitStruct_sparse1(&st, &FacetsOnNodeInt, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &FacetsOnNodeNum, 1, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &NodeCGDG, 2, &if_emlrtRTEI, true);
  emxInitStruct_sparse1(&st, &NodeReg, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &NodesOnElementCG, 2, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &NodesOnElementDG, 2, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &NodesOnInterface, 1, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &NodesOnPBC, 2, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &NodesOnPBCnum, 1, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &NodesOnLink, 2, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &NodesOnLinknum, 1, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &numEonPBC, 1, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &FacetsOnPBC, 1, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &FacetsOnPBCNum, 1, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &FacetsOnIntMinusPBC, 1, &if_emlrtRTEI, true);
  emxInit_real_T(&st, &FacetsOnIntMinusPBCNum, 1, &if_emlrtRTEI, true);
  prhs_copy_idx_0 = emlrtProtectR2012b(prhs[0], 0, false, -1);
  prhs_copy_idx_1 = emlrtProtectR2012b(prhs[1], 1, true, -1);
  prhs_copy_idx_2 = emlrtProtectR2012b(prhs[2], 2, true, -1);
  prhs_copy_idx_3 = emlrtProtectR2012b(prhs[3], 3, true, -1);
  prhs_copy_idx_11 = emlrtProtectR2012b(prhs[11], 11, true, -1);

  /* Marshall function inputs */
  InterTypes->canFreeData = false;
  c_emlrt_marshallIn(&st, emlrtAlias(prhs_copy_idx_0), "InterTypes", InterTypes);
  NodesOnElement->canFreeData = false;
  e_emlrt_marshallIn(&st, emlrtAlias(prhs_copy_idx_1), "NodesOnElement",
                     NodesOnElement);
  RegionOnElement->canFreeData = false;
  g_emlrt_marshallIn(&st, emlrtAlias(prhs_copy_idx_2), "RegionOnElement",
                     RegionOnElement);
  Coordinates->canFreeData = false;
  i_emlrt_marshallIn(&st, emlrtAlias(prhs_copy_idx_3), "Coordinates",
                     Coordinates);
  numnp = emlrt_marshallIn(&st, emlrtAliasP(prhs[4]), "numnp");
  numel = emlrt_marshallIn(&st, emlrtAliasP(prhs[5]), "numel");
  nummat = emlrt_marshallIn(&st, emlrtAliasP(prhs[6]), "nummat");
  nen = emlrt_marshallIn(&st, emlrtAliasP(prhs[7]), "nen");
  ndm = emlrt_marshallIn(&st, emlrtAliasP(prhs[8]), "ndm");
  usePBC = emlrt_marshallIn(&st, emlrtAliasP(prhs[9]), "usePBC");
  numMPC = emlrt_marshallIn(&st, emlrtAliasP(prhs[10]), "numMPC");
  MPCList->canFreeData = false;
  k_emlrt_marshallIn(&st, emlrtAlias(prhs_copy_idx_11), "MPCList", MPCList);

  /* Invoke the target function */
  DEIPFunc3(&st, InterTypes, NodesOnElement, RegionOnElement, Coordinates,
            &numnp, numel, nummat, nen, ndm, usePBC, &numMPC, MPCList, &numEonB,
            numEonF, ElementsOnBoundary, &numSI, ElementsOnFacet,
            &ElementsOnNode, &ElementsOnNodeDup, ElementsOnNodeNum, &numfac,
            ElementsOnNodeNum2, &numinttype, FacetsOnElement, FacetsOnElementInt,
            FacetsOnInterface, FacetsOnInterfaceNum, &FacetsOnNode,
            &FacetsOnNodeCut, &FacetsOnNodeInt, FacetsOnNodeNum, NodeCGDG,
            &NodeReg, NodesOnElementCG, NodesOnElementDG, NodesOnInterface,
            &NodesOnInterfaceNum, &numCL, NodesOnPBC, NodesOnPBCnum, NodesOnLink,
            NodesOnLinknum, numEonPBC, FacetsOnPBC, FacetsOnPBCNum,
            FacetsOnIntMinusPBC, FacetsOnIntMinusPBCNum);

  /* Marshall function outputs */
  NodesOnElement->canFreeData = false;
  k_emlrt_marshallOut(NodesOnElement, prhs_copy_idx_1);
  plhs[0] = prhs_copy_idx_1;
  emxFree_real_T(&NodesOnElement);
  emxFree_real_T(&InterTypes);
  if (nlhs > 1) {
    RegionOnElement->canFreeData = false;
    l_emlrt_marshallOut(RegionOnElement, prhs_copy_idx_2);
    plhs[1] = prhs_copy_idx_2;
  }

  emxFree_real_T(&RegionOnElement);
  if (nlhs > 2) {
    Coordinates->canFreeData = false;
    k_emlrt_marshallOut(Coordinates, prhs_copy_idx_3);
    plhs[2] = prhs_copy_idx_3;
  }

  emxFree_real_T(&Coordinates);
  if (nlhs > 3) {
    plhs[3] = f_emlrt_marshallOut(numnp);
  }

  if (nlhs > 4) {
    MPCList->canFreeData = false;
    k_emlrt_marshallOut(MPCList, prhs_copy_idx_11);
    plhs[4] = prhs_copy_idx_11;
  }

  emxFree_real_T(&MPCList);
  if (nlhs > 5) {
    plhs[5] = f_emlrt_marshallOut(numMPC);
  }

  if (nlhs > 6) {
    plhs[6] = f_emlrt_marshallOut(numEonB);
  }

  if (nlhs > 7) {
    numEonF->canFreeData = false;
    plhs[7] = m_emlrt_marshallOut(numEonF);
  }

  emxFree_real_T(&numEonF);
  if (nlhs > 8) {
    ElementsOnBoundary->canFreeData = false;
    plhs[8] = n_emlrt_marshallOut(ElementsOnBoundary);
  }

  emxFree_real_T(&ElementsOnBoundary);
  if (nlhs > 9) {
    plhs[9] = f_emlrt_marshallOut(numSI);
  }

  if (nlhs > 10) {
    ElementsOnFacet->canFreeData = false;
    plhs[10] = n_emlrt_marshallOut(ElementsOnFacet);
  }

  emxFree_real_T(&ElementsOnFacet);
  if (nlhs > 11) {
    plhs[11] = o_emlrt_marshallOut(ElementsOnNode);
  }

  emxFreeStruct_sparse1(&ElementsOnNode);
  if (nlhs > 12) {
    plhs[12] = o_emlrt_marshallOut(ElementsOnNodeDup);
  }

  emxFreeStruct_sparse1(&ElementsOnNodeDup);
  if (nlhs > 13) {
    ElementsOnNodeNum->canFreeData = false;
    plhs[13] = m_emlrt_marshallOut(ElementsOnNodeNum);
  }

  emxFree_real_T(&ElementsOnNodeNum);
  if (nlhs > 14) {
    plhs[14] = f_emlrt_marshallOut(numfac);
  }

  if (nlhs > 15) {
    ElementsOnNodeNum2->canFreeData = false;
    plhs[15] = m_emlrt_marshallOut(ElementsOnNodeNum2);
  }

  emxFree_real_T(&ElementsOnNodeNum2);
  if (nlhs > 16) {
    plhs[16] = f_emlrt_marshallOut(numinttype);
  }

  if (nlhs > 17) {
    FacetsOnElement->canFreeData = false;
    plhs[17] = n_emlrt_marshallOut(FacetsOnElement);
  }

  emxFree_real_T(&FacetsOnElement);
  if (nlhs > 18) {
    FacetsOnElementInt->canFreeData = false;
    plhs[18] = n_emlrt_marshallOut(FacetsOnElementInt);
  }

  emxFree_real_T(&FacetsOnElementInt);
  if (nlhs > 19) {
    FacetsOnInterface->canFreeData = false;
    plhs[19] = m_emlrt_marshallOut(FacetsOnInterface);
  }

  emxFree_real_T(&FacetsOnInterface);
  if (nlhs > 20) {
    FacetsOnInterfaceNum->canFreeData = false;
    plhs[20] = m_emlrt_marshallOut(FacetsOnInterfaceNum);
  }

  emxFree_real_T(&FacetsOnInterfaceNum);
  if (nlhs > 21) {
    plhs[21] = o_emlrt_marshallOut(FacetsOnNode);
  }

  emxFreeStruct_sparse1(&FacetsOnNode);
  if (nlhs > 22) {
    plhs[22] = o_emlrt_marshallOut(FacetsOnNodeCut);
  }

  emxFreeStruct_sparse1(&FacetsOnNodeCut);
  if (nlhs > 23) {
    plhs[23] = o_emlrt_marshallOut(FacetsOnNodeInt);
  }

  emxFreeStruct_sparse1(&FacetsOnNodeInt);
  if (nlhs > 24) {
    FacetsOnNodeNum->canFreeData = false;
    plhs[24] = m_emlrt_marshallOut(FacetsOnNodeNum);
  }

  emxFree_real_T(&FacetsOnNodeNum);
  if (nlhs > 25) {
    NodeCGDG->canFreeData = false;
    plhs[25] = n_emlrt_marshallOut(NodeCGDG);
  }

  emxFree_real_T(&NodeCGDG);
  if (nlhs > 26) {
    plhs[26] = o_emlrt_marshallOut(NodeReg);
  }

  emxFreeStruct_sparse1(&NodeReg);
  if (nlhs > 27) {
    NodesOnElementCG->canFreeData = false;
    plhs[27] = n_emlrt_marshallOut(NodesOnElementCG);
  }

  emxFree_real_T(&NodesOnElementCG);
  if (nlhs > 28) {
    NodesOnElementDG->canFreeData = false;
    plhs[28] = n_emlrt_marshallOut(NodesOnElementDG);
  }

  emxFree_real_T(&NodesOnElementDG);
  if (nlhs > 29) {
    NodesOnInterface->canFreeData = false;
    plhs[29] = m_emlrt_marshallOut(NodesOnInterface);
  }

  emxFree_real_T(&NodesOnInterface);
  if (nlhs > 30) {
    plhs[30] = f_emlrt_marshallOut(NodesOnInterfaceNum);
  }

  if (nlhs > 31) {
    plhs[31] = f_emlrt_marshallOut(numCL);
  }

  if (nlhs > 32) {
    NodesOnPBC->canFreeData = false;
    plhs[32] = n_emlrt_marshallOut(NodesOnPBC);
  }

  emxFree_real_T(&NodesOnPBC);
  if (nlhs > 33) {
    NodesOnPBCnum->canFreeData = false;
    plhs[33] = m_emlrt_marshallOut(NodesOnPBCnum);
  }

  emxFree_real_T(&NodesOnPBCnum);
  if (nlhs > 34) {
    NodesOnLink->canFreeData = false;
    plhs[34] = n_emlrt_marshallOut(NodesOnLink);
  }

  emxFree_real_T(&NodesOnLink);
  if (nlhs > 35) {
    NodesOnLinknum->canFreeData = false;
    plhs[35] = m_emlrt_marshallOut(NodesOnLinknum);
  }

  emxFree_real_T(&NodesOnLinknum);
  if (nlhs > 36) {
    numEonPBC->canFreeData = false;
    plhs[36] = m_emlrt_marshallOut(numEonPBC);
  }

  emxFree_real_T(&numEonPBC);
  if (nlhs > 37) {
    FacetsOnPBC->canFreeData = false;
    plhs[37] = m_emlrt_marshallOut(FacetsOnPBC);
  }

  emxFree_real_T(&FacetsOnPBC);
  if (nlhs > 38) {
    FacetsOnPBCNum->canFreeData = false;
    plhs[38] = m_emlrt_marshallOut(FacetsOnPBCNum);
  }

  emxFree_real_T(&FacetsOnPBCNum);
  if (nlhs > 39) {
    FacetsOnIntMinusPBC->canFreeData = false;
    plhs[39] = m_emlrt_marshallOut(FacetsOnIntMinusPBC);
  }

  emxFree_real_T(&FacetsOnIntMinusPBC);
  if (nlhs > 40) {
    FacetsOnIntMinusPBCNum->canFreeData = false;
    plhs[40] = m_emlrt_marshallOut(FacetsOnIntMinusPBCNum);
  }

  emxFree_real_T(&FacetsOnIntMinusPBCNum);
  emlrtHeapReferenceStackLeaveFcnR2012b(&st);
}

/* End of code generation (_coder_DEIPFunc3_api.c) */
