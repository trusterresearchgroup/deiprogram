/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * parenAssign2D.h
 *
 * Code generation for function 'parenAssign2D'
 *
 */

#ifndef PARENASSIGN2D_H
#define PARENASSIGN2D_H

/* Include files */
#include "DEIPFunc2_types.h"
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>
#ifdef __cplusplus

extern "C" {

#endif

  /* Function Declarations */
  void b_realloc(d_sparse *this, int numAllocRequested, int ub1, int lb2, int
                 ub2);
  void sparse_parenAssign2D(d_sparse *this, double rhs, double r, double c);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (parenAssign2D.h) */
