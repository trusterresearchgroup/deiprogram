% Interactive example that exports solutions from tutorial DEIPex2 into a 
% VTK file and demonstrate how to execute analyses in batch mode.
%
% Moderate example of insertion of couplers using DEIProgram2. Corresponds
% to Figure 5 in "Discontinuous Element Insertion Algorithm" manuscript
% Domain: 4x3 rectangle, combination of T3 and Q4 elements; insertion of CZ
% couplers on region interfaces; demonstrates separation of mesh
% Loading: Displacement of 0.1 on right edge and top edge
%
% Last revision: 12/29/2021 TJT

%% Input/mesh only
DEIPex2Func %load the mesh

disp(' Export the regions in the mesh to in0001.vtk; press any key to continue')
pause

stepFE = [1 1 1];
stepPV = [1 1 1];
% nodeelemcoup = [1 numnp 1 numelCG numelCG+1 numel];
nodeelemcoup = [1 numnp 1 RegionsOnInterface(1,5)-1 RegionsOnInterface(1,5) numel];
nenPV = 0;
otherflag = [2 8];
flagout = [1 0 0 0 0 0 1 0];
fileprop.dfoldername =  'ParaOut';
fileprop.filename = 'in'; %prefNodesOnElement filename (e.g. 'out' -> out01.vtk)
fileprop.header = 'Mesh Only'; %text at top of VTK files
fileprop.foldername =  'DEIPex2VTK';

VTKOutputWriter(stepFE,stepPV,nodeelemcoup,nenPV,otherflag,flagout,fileprop, ...
            Coordinates,NodesOnElement,RegionOnElement,[],[], ...
            [])

%% Analysis results
batchinter = 'batch';
batchname = 'DEIPex2Func.m';
fprintf('starting file %s\n',batchname)
FEA_Program

disp(' Export the displacements and stresses; press any key to continue')
pause

stepFE = [1 1 1];
stepPV = [1 1 1];
% nodeelemcoup = [1 numnp 1 numelCG numelCG+1 numel];
nodeelemcoup = [1 numnp 1 RegionsOnInterface(1,5)-1 RegionsOnInterface(1,5) numel];
nenPV = 0;
otherflag = [2 8];
flagout = [1 1 1 1 1 1 0 0];
fileprop.dfoldername =  'ParaOut';
fileprop.filename = 'out'; %prefNodesOnElement filename (e.g. 'out' -> out01.vtk)
fileprop.header = 'Elasticity'; %text at top of VTK files
fileprop.foldername =  'DEIPex2VTK';

VTKOutputWriter(stepFE,stepPV,nodeelemcoup,nenPV,otherflag,flagout,fileprop, ...
            Coordinates,NodesOnElement,RegionOnElement,DispList,StreList, ...
            StreListE)
