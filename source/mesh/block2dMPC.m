function [MPCList,numMPC,NodeBC,numBC] = block2dMPC(rinc,sinc)

% Boundary conditions
FIX = 1;
RIGHT = (rinc+1); %x
TOP = (rinc+1)*(sinc)+1; %y
TOPRIGHT = TOP + RIGHT - 1; %xy
NodeBC = [FIX 1 0
          FIX 2 0
          RIGHT 2 0 % prevent rigid body rotation
          ];
numBC = size(NodeBC,1);

% Create periodic boundary conditions
dx = 1;
dy = rinc+1;
dxy = sinc+1;
PBCListx = [(RIGHT:dy:TOPRIGHT)' (FIX:dy:TOP)' RIGHT*ones(dxy,1) FIX*ones(dxy,1)]; %y
% remove repeats
RIGHTface = 2:dxy;
PBCListx = PBCListx(RIGHTface,:);
PBCListy = [(TOP:dx:TOPRIGHT)' (FIX:dx:RIGHT)' TOP*ones(dy,1) FIX*ones(dy,1)]; %x
% remove repeats
TOPface = 1:dy;
TOPface = setdiff(TOPface,dy);
TOPface = setdiff(TOPface,1);
PBCListy = PBCListy(TOPface,:);
MPCListx = [PBCListx(:,1:2) ones(size(PBCListx,1),1)*[1 0]
            PBCListx(1,3:4) [1 0]];
MPCListy = [PBCListy(:,1:2) ones(size(PBCListy,1),1)*[0 1]
            PBCListy(1,3:4) [0 1]];
MPCList = [MPCListx; MPCListy];
numMPC = length(MPCList);