/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * validateNumericIndex.c
 *
 * Code generation for function 'validateNumericIndex'
 *
 */

/* Include files */
#include "validateNumericIndex.h"
#include "DEIPFunc2_data.h"
#include "DEIPFunc2_types.h"
#include "rt_nonfinite.h"
#include "mwmathutil.h"

/* Function Definitions */
void b_sparse_validateNumericIndex(const emlrtStack *sp, int32_T upperBound,
  real_T idx)
{
  if ((!(muDoubleScalarFloor(idx) == idx)) || muDoubleScalarIsInf(idx) || (!(idx
        > 0.0))) {
    emlrtErrorWithMessageIdR2018a(sp, &w_emlrtRTEI, "Coder:MATLAB:badsubscript",
      "Coder:MATLAB:badsubscript", 0);
  }

  if (!(idx <= upperBound)) {
    emlrtErrorWithMessageIdR2018a(sp, &x_emlrtRTEI,
      "Coder:builtins:IndexOutOfBounds", "Coder:builtins:IndexOutOfBounds", 6, 6,
      idx, 12, 1, 12, upperBound);
  }
}

void sparse_validateNumericIndex(const emlrtStack *sp, int32_T upperBound, const
  emxArray_real_T *idx)
{
  int32_T i;
  int32_T k;
  i = idx->size[0];
  for (k = 0; k < i; k++) {
    if ((!(muDoubleScalarFloor(idx->data[k]) == idx->data[k])) ||
        muDoubleScalarIsInf(idx->data[k]) || (!(idx->data[k] > 0.0))) {
      emlrtErrorWithMessageIdR2018a(sp, &w_emlrtRTEI,
        "Coder:MATLAB:badsubscript", "Coder:MATLAB:badsubscript", 0);
    }

    if (!(idx->data[k] <= upperBound)) {
      emlrtErrorWithMessageIdR2018a(sp, &x_emlrtRTEI,
        "Coder:builtins:IndexOutOfBounds", "Coder:builtins:IndexOutOfBounds", 6,
        6, idx->data[k], 12, 1, 12, upperBound);
    }
  }
}

/* End of code generation (validateNumericIndex.c) */
