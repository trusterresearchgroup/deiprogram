/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * isequal.c
 *
 * Code generation for function 'isequal'
 *
 */

/* Include files */
#include "isequal.h"
#include "rt_nonfinite.h"

/* Function Definitions */
boolean_T isequal(real_T varargin_1, const real_T varargin_2_data[], const
                  int32_T varargin_2_size[2])
{
  boolean_T b_p;
  boolean_T p;
  p = false;
  b_p = (1 == varargin_2_size[1]);
  if (b_p && (varargin_2_size[1] != 0) && (!(varargin_1 == varargin_2_data[0])))
  {
    b_p = false;
  }

  return b_p || p;
}

/* End of code generation (isequal.c) */
