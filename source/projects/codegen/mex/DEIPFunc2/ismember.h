/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * ismember.h
 *
 * Code generation for function 'ismember'
 *
 */

#pragma once

/* Include files */
#include "DEIPFunc2_types.h"
#include "rtwtypes.h"
#include "emlrt.h"
#include "mex.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Function Declarations */
void b_local_ismember(const emlrtStack *sp, const emxArray_real_T *a, const
                      emxArray_real_T *s, emxArray_boolean_T *tf);
boolean_T local_ismember(real_T a, const real_T s_data[], const int32_T s_size[1]);

/* End of code generation (ismember.h) */
