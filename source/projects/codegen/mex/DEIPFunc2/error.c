/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * error.c
 *
 * Code generation for function 'error'
 *
 */

/* Include files */
#include "error.h"
#include "rt_nonfinite.h"

/* Variable Definitions */
static emlrtMCInfo i_emlrtMCI = { 27,  /* lineNo */
  5,                                   /* colNo */
  "error",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\lang\\error.m"/* pName */
};

static emlrtRSInfo hp_emlrtRSI = { 27, /* lineNo */
  "error",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\lang\\error.m"/* pathName */
};

/* Function Declarations */
static void i_error(const emlrtStack *sp, const mxArray *b, emlrtMCInfo
                    *location);

/* Function Definitions */
static void i_error(const emlrtStack *sp, const mxArray *b, emlrtMCInfo
                    *location)
{
  const mxArray *pArray;
  pArray = b;
  emlrtCallMATLABR2012b(sp, 0, NULL, 1, &pArray, "error", true, location);
}

void b_error(const emlrtStack *sp)
{
  static const int32_T iv[2] = { 1, 51 };

  static const char_T varargin_1[51] = { 'E', 'r', 'r', 'o', 'r', ',', ' ', 'n',
    'u', 'm', 'b', 'e', 'r', ' ', 'o', 'f', ' ', 'n', 'o', 'd', 'e', 's', ':',
    ' ', 'n', 'u', 'm', 'n', 'p', ' ', '>', ' ', 's', 'i', 'z', 'e', '(', 'C',
    'o', 'o', 'r', 'd', 'i', 'n', 'a', 't', 'e', 's', ',', '1', ')' };

  emlrtStack st;
  const mxArray *m;
  const mxArray *y;
  st.prev = sp;
  st.tls = sp->tls;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 51, m, &varargin_1[0]);
  emlrtAssign(&y, m);
  st.site = &hp_emlrtRSI;
  i_error(&st, y, &i_emlrtMCI);
}

void c_error(const emlrtStack *sp)
{
  static const int32_T iv[2] = { 1, 54 };

  static const char_T varargin_1[54] = { 'E', 'r', 'r', 'o', 'r', ',', ' ', 'n',
    'u', 'm', 'b', 'e', 'r', ' ', 'o', 'f', ' ', 'n', 'o', 'd', 'e', 's', ':',
    ' ', 'n', 'u', 'm', 'e', 'l', ' ', '>', ' ', 's', 'i', 'z', 'e', '(', 'N',
    'o', 'd', 'e', 's', 'O', 'n', 'E', 'l', 'e', 'm', 'e', 'n', 't', ',', '1',
    ')' };

  emlrtStack st;
  const mxArray *m;
  const mxArray *y;
  st.prev = sp;
  st.tls = sp->tls;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 54, m, &varargin_1[0]);
  emlrtAssign(&y, m);
  st.site = &hp_emlrtRSI;
  i_error(&st, y, &i_emlrtMCI);
}

void d_error(const emlrtStack *sp)
{
  static const int32_T iv[2] = { 1, 74 };

  static const char_T varargin_1[74] = { 'N', 'o', 't', 'e', ' ', 't', 'h', 'a',
    't', ' ', 'F', 'E', 'A', '_', 'P', 'r', 'o', 'g', 'r', 'a', 'm', ' ', 'd',
    'o', 'e', 's', ' ', 'n', 'o', 't', ' ', 'u', 's', 'e', ' ', 't', 'h', 'e',
    ' ', 'e', 'l', 'e', 'm', 'e', 'n', 't', ' ', 'I', 'D', ' ', 'a', 's', ' ',
    'c', 'o', 'l', 'u', 'm', 'n', ' ', '1', ' ', 'o', 'f', ' ', 't', 'h', 'e',
    ' ', 'a', 'r', 'r', 'a', 'y' };

  emlrtStack st;
  const mxArray *m;
  const mxArray *y;
  st.prev = sp;
  st.tls = sp->tls;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 74, m, &varargin_1[0]);
  emlrtAssign(&y, m);
  st.site = &hp_emlrtRSI;
  i_error(&st, y, &i_emlrtMCI);
}

void e_error(const emlrtStack *sp)
{
  static const int32_T iv[2] = { 1, 117 };

  static const char_T varargin_1[117] = { 'E', 'r', 'r', 'o', 'r', ',', ' ', 'c',
    'o', 'n', 'n', 'e', 'c', 't', 'i', 'v', 'i', 't', 'y', ':', ' ', 'a', ' ',
    'n', 'o', 'd', 'e', ' ', 'i', 'n', ' ', 'N', 'o', 'd', 'e', 's', 'O', 'n',
    'E', 'l', 'e', 'm', 'e', 'n', 't', ' ', 'e', 'x', 'c', 'e', 'e', 'd', 's',
    ' ', 't', 'h', 'e', ' ', 'n', 'u', 'm', 'b', 'e', 'r', ' ', 'o', 'f', ' ',
    'n', 'o', 'd', 'e', 's', ':', ' ', 'm', 'a', 'x', '(', 'N', 'o', 'd', 'e',
    's', 'O', 'n', 'E', 'l', 'e', 'm', 'e', 'n', 't', '(', '1', ':', 'n', 'u',
    'm', 'e', 'l', ',', '1', ':', 'n', 'e', 'n', ')', ')', ' ', '>', ' ', 'n',
    'u', 'm', 'n', 'p' };

  emlrtStack st;
  const mxArray *m;
  const mxArray *y;
  st.prev = sp;
  st.tls = sp->tls;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 117, m, &varargin_1[0]);
  emlrtAssign(&y, m);
  st.site = &hp_emlrtRSI;
  i_error(&st, y, &i_emlrtMCI);
}

void error(const emlrtStack *sp)
{
  static const int32_T iv[2] = { 1, 28 };

  static const char_T varargin_1[28] = { 'W', 'r', 'o', 'n', 'g', ' ', 'n', 'u',
    'm', 'b', 'e', 'r', ' ', 'o', 'f', ' ', 'm', 'a', 's', 't', 'e', 'r', ' ',
    'p', 'a', 'i', 'r', 's' };

  emlrtStack st;
  const mxArray *m;
  const mxArray *y;
  st.prev = sp;
  st.tls = sp->tls;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 28, m, &varargin_1[0]);
  emlrtAssign(&y, m);
  st.site = &hp_emlrtRSI;
  i_error(&st, y, &i_emlrtMCI);
}

void f_error(const emlrtStack *sp)
{
  static const int32_T iv[2] = { 1, 100 };

  static const char_T varargin_1[100] = { 'D', 'e', 'g', 'e', 'n', 'e', 'r', 'a',
    't', 'e', ' ', 'P', 'B', 'C', ' ', 'z', 'i', 'p', 'p', 'e', 'd', ' ', 'm',
    'e', 's', 'h', ' ', 'f', 'a', 'c', 'e', 't', ' ', 'e', 'n', 'c', 'o', 'u',
    'n', 't', 'e', 'r', 'e', 'd', ';', ' ', 'a', 'd', 'd', ' ', 'm', 'o', 'r',
    'e', ' ', 'l', 'a', 'y', 'e', 'r', 's', ' ', 'o', 'f', ' ', 'e', 'l', 'e',
    'm', 'e', 'n', 't', 's', ' ', 'b', 'e', 't', 'w', 'e', 'e', 'n', ' ', 'o',
    'p', 'p', 'o', 's', 'i', 't', 'e', ' ', 'P', 'B', 'C', ' ', 'f', 'a', 'c',
    'e', 's' };

  emlrtStack st;
  const mxArray *m;
  const mxArray *y;
  st.prev = sp;
  st.tls = sp->tls;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 100, m, &varargin_1[0]);
  emlrtAssign(&y, m);
  st.site = &hp_emlrtRSI;
  i_error(&st, y, &i_emlrtMCI);
}

void g_error(const emlrtStack *sp)
{
  static const int32_T iv[2] = { 1, 14 };

  static const char_T varargin_1[14] = { 'd', 'e', 'g', 'e', 'n', 'e', 'r', 'a',
    't', 'e', ' ', 'P', 'B', 'C' };

  emlrtStack st;
  const mxArray *m;
  const mxArray *y;
  st.prev = sp;
  st.tls = sp->tls;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 14, m, &varargin_1[0]);
  emlrtAssign(&y, m);
  st.site = &hp_emlrtRSI;
  i_error(&st, y, &i_emlrtMCI);
}

void h_error(const emlrtStack *sp)
{
  static const int32_T iv[2] = { 1, 46 };

  static const char_T varargin_1[46] = { 'd', 'o', 'm', 'a', 'i', 'n', ' ', 'b',
    'o', 'u', 'n', 'd', 'a', 'r', 'i', 'e', 's', ' ', 'n', 'o', 't', ' ', 'a',
    'l', 'l', 'o', 'w', 'e', 'd', ' ', 'i', 'n', ' ', 'p', 'e', 'r', 'i', 'o',
    'd', 'i', 'c', ' ', 'm', 'e', 's', 'h' };

  emlrtStack st;
  const mxArray *m;
  const mxArray *y;
  st.prev = sp;
  st.tls = sp->tls;
  y = NULL;
  m = emlrtCreateCharArray(2, &iv[0]);
  emlrtInitCharArrayR2013a(sp, 46, m, &varargin_1[0]);
  emlrtAssign(&y, m);
  st.site = &hp_emlrtRSI;
  i_error(&st, y, &i_emlrtMCI);
}

/* End of code generation (error.c) */
