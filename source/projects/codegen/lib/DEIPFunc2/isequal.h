/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * isequal.h
 *
 * Code generation for function 'isequal'
 *
 */

#ifndef ISEQUAL_H
#define ISEQUAL_H

/* Include files */
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>
#ifdef __cplusplus

extern "C" {

#endif

  /* Function Declarations */
  boolean_T isequal(double varargin_1, const double varargin_2_data[], const int
                    varargin_2_size[2]);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (isequal.h) */
