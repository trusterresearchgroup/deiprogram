/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * nnz.h
 *
 * Code generation for function 'nnz'
 *
 */

#ifndef NNZ_H
#define NNZ_H

/* Include files */
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>
#ifdef __cplusplus

extern "C" {

#endif

  /* Function Declarations */
  int intnnz(const double s_data[], const int s_size[2]);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (nnz.h) */
