/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * not.h
 *
 * Code generation for function 'not'
 *
 */

#pragma once

/* Include files */
#include "DEIPFunc2_types.h"
#include "rtwtypes.h"
#include "emlrt.h"
#include "mex.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Function Declarations */
void sparse_not(const emlrtStack *sp, const emxArray_real_T *S_d, const
                emxArray_int32_T *S_colidx, const emxArray_int32_T *S_rowidx,
                emxArray_boolean_T *out_d, emxArray_int32_T *out_colidx,
                emxArray_int32_T *out_rowidx);

/* End of code generation (not.h) */
