/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * introsort.c
 *
 * Code generation for function 'introsort'
 *
 */

/* Include files */
#include "introsort.h"
#include "DEIPFunc3_types.h"
#include "heapsort.h"
#include "insertionsort.h"
#include "rt_nonfinite.h"

/* Type Definitions */
#ifndef typedef_struct_T
#define typedef_struct_T

typedef struct {
  int32_T xstart;
  int32_T xend;
  int32_T depth;
} struct_T;

#endif                                 /*typedef_struct_T*/

/* Variable Definitions */
static emlrtRSInfo uk_emlrtRSI = { 34, /* lineNo */
  "introsort",                         /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\lib\\+coder\\+internal\\introsort.m"/* pathName */
};

static emlrtRSInfo vk_emlrtRSI = { 42, /* lineNo */
  "introsort",                         /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\lib\\+coder\\+internal\\introsort.m"/* pathName */
};

static emlrtRSInfo xk_emlrtRSI = { 47, /* lineNo */
  "introsort",                         /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\lib\\+coder\\+internal\\introsort.m"/* pathName */
};

static emlrtRSInfo yk_emlrtRSI = { 49, /* lineNo */
  "introsort",                         /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\lib\\+coder\\+internal\\introsort.m"/* pathName */
};

static emlrtRSInfo al_emlrtRSI = { 55, /* lineNo */
  "introsort",                         /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\lib\\+coder\\+internal\\introsort.m"/* pathName */
};

static emlrtRSInfo bl_emlrtRSI = { 58, /* lineNo */
  "introsort",                         /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\lib\\+coder\\+internal\\introsort.m"/* pathName */
};

static emlrtRSInfo cl_emlrtRSI = { 41, /* lineNo */
  "introsort",                         /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\lib\\+coder\\+internal\\introsort.m"/* pathName */
};

static emlrtRTEInfo nc_emlrtRTEI = { 62,/* lineNo */
  39,                                  /* colNo */
  "stack/push",                        /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\lib\\+coder\\+internal\\stack.m"/* pName */
};

static emlrtDCInfo df_emlrtDCI = { 48, /* lineNo */
  48,                                  /* colNo */
  "stack/stack",                       /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\lib\\+coder\\+internal\\stack.m",/* pName */
  4                                    /* checkKind */
};

/* Function Definitions */
void b_introsort(const emlrtStack *sp, emxArray_int32_T *x, int32_T xend, const
                 cell_wrap_6 cmp_tunableEnvironment[1])
{
  emlrtStack st;
  struct_T st_d_data[120];
  struct_T frame;
  int32_T exitg2;
  int32_T exitg3;
  int32_T j;
  int32_T pivot;
  int32_T pmax;
  int32_T pmin;
  int32_T pow2p;
  int32_T st_n;
  int32_T unnamed_idx_0;
  boolean_T exitg1;
  st.prev = sp;
  st.tls = sp->tls;
  if (1 < xend) {
    if (xend <= 32) {
      st.site = &uk_emlrtRSI;
      b_insertionsort(&st, x, 1, xend, cmp_tunableEnvironment);
    } else {
      pmax = 31;
      pmin = 0;
      exitg1 = false;
      while ((!exitg1) && (pmax - pmin > 1)) {
        j = (pmin + pmax) >> 1;
        pow2p = 1 << j;
        if (pow2p == xend) {
          pmax = j;
          exitg1 = true;
        } else if (pow2p > xend) {
          pmax = j;
        } else {
          pmin = j;
        }
      }

      pow2p = (pmax - 1) << 1;
      frame.xstart = 1;
      frame.xend = xend;
      frame.depth = 0;
      st.site = &cl_emlrtRSI;
      unnamed_idx_0 = pow2p << 1;
      if (unnamed_idx_0 < 0) {
        emlrtNonNegativeCheckR2012b(unnamed_idx_0, &df_emlrtDCI, &st);
      }

      for (pmin = 0; pmin < unnamed_idx_0; pmin++) {
        st_d_data[pmin] = frame;
      }

      st.site = &vk_emlrtRSI;
      if (0 >= unnamed_idx_0) {
        emlrtErrorWithMessageIdR2018a(&st, &nc_emlrtRTEI,
          "Coder:toolbox:StackPushLimit", "Coder:toolbox:StackPushLimit", 0);
      }

      st_d_data[0] = frame;
      st_n = 1;
      while (st_n > 0) {
        frame = st_d_data[st_n - 1];
        st_n--;
        pmin = frame.xend - frame.xstart;
        if (pmin + 1 <= 32) {
          st.site = &xk_emlrtRSI;
          b_insertionsort(&st, x, frame.xstart, frame.xend,
                          cmp_tunableEnvironment);
        } else if (frame.depth == pow2p) {
          st.site = &yk_emlrtRSI;
          c_heapsort(&st, x, frame.xstart, frame.xend, cmp_tunableEnvironment);
        } else {
          pmax = (frame.xstart + pmin / 2) - 1;
          pmin = x->data[frame.xstart - 1];
          if (cmp_tunableEnvironment[0].f1->data[x->data[pmax] - 1] <
              cmp_tunableEnvironment[0].f1->data[pmin - 1]) {
            x->data[frame.xstart - 1] = x->data[pmax];
            x->data[pmax] = pmin;
          }

          if (cmp_tunableEnvironment[0].f1->data[x->data[frame.xend - 1] - 1] <
              cmp_tunableEnvironment[0].f1->data[x->data[frame.xstart - 1] - 1])
          {
            pmin = x->data[frame.xstart - 1];
            x->data[frame.xstart - 1] = x->data[frame.xend - 1];
            x->data[frame.xend - 1] = pmin;
          }

          if (cmp_tunableEnvironment[0].f1->data[x->data[frame.xend - 1] - 1] <
              cmp_tunableEnvironment[0].f1->data[x->data[pmax] - 1]) {
            pmin = x->data[pmax];
            x->data[pmax] = x->data[frame.xend - 1];
            x->data[frame.xend - 1] = pmin;
          }

          pivot = x->data[pmax];
          x->data[pmax] = x->data[frame.xend - 2];
          x->data[frame.xend - 2] = pivot;
          pmax = frame.xstart - 1;
          j = frame.xend - 2;
          do {
            exitg2 = 0;
            pmax++;
            do {
              exitg3 = 0;
              pmin = cmp_tunableEnvironment[0].f1->data[pivot - 1];
              if (cmp_tunableEnvironment[0].f1->data[x->data[pmax] - 1] < pmin)
              {
                pmax++;
              } else {
                exitg3 = 1;
              }
            } while (exitg3 == 0);

            for (j--; pmin < cmp_tunableEnvironment[0].f1->data[x->data[j] - 1];
                 j--) {
            }

            if (pmax + 1 >= j + 1) {
              exitg2 = 1;
            } else {
              pmin = x->data[pmax];
              x->data[pmax] = x->data[j];
              x->data[j] = pmin;
            }
          } while (exitg2 == 0);

          x->data[frame.xend - 2] = x->data[pmax];
          x->data[pmax] = pivot;
          if (pmax + 2 < frame.xend) {
            st.site = &al_emlrtRSI;
            if (st_n >= unnamed_idx_0) {
              emlrtErrorWithMessageIdR2018a(&st, &nc_emlrtRTEI,
                "Coder:toolbox:StackPushLimit", "Coder:toolbox:StackPushLimit",
                0);
            }

            st_d_data[st_n].xstart = pmax + 2;
            st_d_data[st_n].xend = frame.xend;
            st_d_data[st_n].depth = frame.depth + 1;
            st_n++;
          }

          if (frame.xstart < pmax + 1) {
            st.site = &bl_emlrtRSI;
            if (st_n >= unnamed_idx_0) {
              emlrtErrorWithMessageIdR2018a(&st, &nc_emlrtRTEI,
                "Coder:toolbox:StackPushLimit", "Coder:toolbox:StackPushLimit",
                0);
            }

            st_d_data[st_n].xstart = frame.xstart;
            st_d_data[st_n].xend = pmax + 1;
            st_d_data[st_n].depth = frame.depth + 1;
            st_n++;
          }
        }
      }
    }
  }
}

void introsort(const emlrtStack *sp, emxArray_int32_T *x, int32_T xend, const
               cell_wrap_6 cmp_tunableEnvironment[2])
{
  emlrtStack st;
  struct_T st_d_data[120];
  struct_T frame;
  int32_T MAXDEPTH;
  int32_T exitg2;
  int32_T exitg3;
  int32_T i;
  int32_T j;
  int32_T pivot;
  int32_T pmax;
  int32_T pmin;
  int32_T pow2p;
  int32_T st_n;
  int32_T unnamed_idx_0;
  boolean_T exitg1;
  boolean_T varargout_1;
  st.prev = sp;
  st.tls = sp->tls;
  if (1 < xend) {
    if (xend <= 32) {
      st.site = &uk_emlrtRSI;
      insertionsort(&st, x, 1, xend, cmp_tunableEnvironment);
    } else {
      pmax = 31;
      pmin = 0;
      exitg1 = false;
      while ((!exitg1) && (pmax - pmin > 1)) {
        j = (pmin + pmax) >> 1;
        pow2p = 1 << j;
        if (pow2p == xend) {
          pmax = j;
          exitg1 = true;
        } else if (pow2p > xend) {
          pmax = j;
        } else {
          pmin = j;
        }
      }

      MAXDEPTH = (pmax - 1) << 1;
      frame.xstart = 1;
      frame.xend = xend;
      frame.depth = 0;
      st.site = &cl_emlrtRSI;
      unnamed_idx_0 = MAXDEPTH << 1;
      if (unnamed_idx_0 < 0) {
        emlrtNonNegativeCheckR2012b(unnamed_idx_0, &df_emlrtDCI, &st);
      }

      for (i = 0; i < unnamed_idx_0; i++) {
        st_d_data[i] = frame;
      }

      st.site = &vk_emlrtRSI;
      if (0 >= unnamed_idx_0) {
        emlrtErrorWithMessageIdR2018a(&st, &nc_emlrtRTEI,
          "Coder:toolbox:StackPushLimit", "Coder:toolbox:StackPushLimit", 0);
      }

      st_d_data[0] = frame;
      st_n = 1;
      while (st_n > 0) {
        frame = st_d_data[st_n - 1];
        st_n--;
        i = frame.xend - frame.xstart;
        if (i + 1 <= 32) {
          st.site = &xk_emlrtRSI;
          insertionsort(&st, x, frame.xstart, frame.xend, cmp_tunableEnvironment);
        } else if (frame.depth == MAXDEPTH) {
          st.site = &yk_emlrtRSI;
          b_heapsort(&st, x, frame.xstart, frame.xend, cmp_tunableEnvironment);
        } else {
          pow2p = (frame.xstart + i / 2) - 1;
          i = x->data[frame.xstart - 1];
          pmax = cmp_tunableEnvironment[0].f1->data[x->data[pow2p] - 1];
          pmin = cmp_tunableEnvironment[0].f1->data[i - 1];
          if (pmax < pmin) {
            varargout_1 = true;
          } else if (pmax == pmin) {
            varargout_1 = (cmp_tunableEnvironment[1].f1->data[x->data[pow2p] - 1]
                           < cmp_tunableEnvironment[1].f1->data[i - 1]);
          } else {
            varargout_1 = false;
          }

          if (varargout_1) {
            x->data[frame.xstart - 1] = x->data[pow2p];
            x->data[pow2p] = i;
          }

          if (cmp_tunableEnvironment[0].f1->data[x->data[frame.xend - 1] - 1] <
              cmp_tunableEnvironment[0].f1->data[x->data[frame.xstart - 1] - 1])
          {
            varargout_1 = true;
          } else if (cmp_tunableEnvironment[0].f1->data[x->data[frame.xend - 1]
                     - 1] == cmp_tunableEnvironment[0].f1->data[x->
                     data[frame.xstart - 1] - 1]) {
            varargout_1 = (cmp_tunableEnvironment[1].f1->data[x->data[frame.xend
                           - 1] - 1] < cmp_tunableEnvironment[1].f1->data
                           [x->data[frame.xstart - 1] - 1]);
          } else {
            varargout_1 = false;
          }

          if (varargout_1) {
            pmax = x->data[frame.xstart - 1];
            x->data[frame.xstart - 1] = x->data[frame.xend - 1];
            x->data[frame.xend - 1] = pmax;
          }

          if (cmp_tunableEnvironment[0].f1->data[x->data[frame.xend - 1] - 1] <
              cmp_tunableEnvironment[0].f1->data[x->data[pow2p] - 1]) {
            varargout_1 = true;
          } else if (cmp_tunableEnvironment[0].f1->data[x->data[frame.xend - 1]
                     - 1] == cmp_tunableEnvironment[0].f1->data[x->data[pow2p] -
                     1]) {
            varargout_1 = (cmp_tunableEnvironment[1].f1->data[x->data[frame.xend
                           - 1] - 1] < cmp_tunableEnvironment[1].f1->data
                           [x->data[pow2p] - 1]);
          } else {
            varargout_1 = false;
          }

          if (varargout_1) {
            pmax = x->data[pow2p];
            x->data[pow2p] = x->data[frame.xend - 1];
            x->data[frame.xend - 1] = pmax;
          }

          pivot = x->data[pow2p] - 1;
          x->data[pow2p] = x->data[frame.xend - 2];
          x->data[frame.xend - 2] = pivot + 1;
          pmin = frame.xstart - 1;
          j = frame.xend - 2;
          do {
            exitg2 = 0;
            pmin++;
            do {
              exitg3 = 0;
              i = cmp_tunableEnvironment[0].f1->data[x->data[pmin] - 1];
              if (i < cmp_tunableEnvironment[0].f1->data[pivot]) {
                varargout_1 = true;
              } else if (i == cmp_tunableEnvironment[0].f1->data[pivot]) {
                varargout_1 = (cmp_tunableEnvironment[1].f1->data[x->data[pmin]
                               - 1] < cmp_tunableEnvironment[1].f1->data[pivot]);
              } else {
                varargout_1 = false;
              }

              if (varargout_1) {
                pmin++;
              } else {
                exitg3 = 1;
              }
            } while (exitg3 == 0);

            j--;
            do {
              exitg3 = 0;
              i = cmp_tunableEnvironment[0].f1->data[x->data[j] - 1];
              if (cmp_tunableEnvironment[0].f1->data[pivot] < i) {
                varargout_1 = true;
              } else if (cmp_tunableEnvironment[0].f1->data[pivot] == i) {
                varargout_1 = (cmp_tunableEnvironment[1].f1->data[pivot] <
                               cmp_tunableEnvironment[1].f1->data[x->data[j] - 1]);
              } else {
                varargout_1 = false;
              }

              if (varargout_1) {
                j--;
              } else {
                exitg3 = 1;
              }
            } while (exitg3 == 0);

            if (pmin + 1 >= j + 1) {
              exitg2 = 1;
            } else {
              pmax = x->data[pmin];
              x->data[pmin] = x->data[j];
              x->data[j] = pmax;
            }
          } while (exitg2 == 0);

          x->data[frame.xend - 2] = x->data[pmin];
          x->data[pmin] = pivot + 1;
          if (pmin + 2 < frame.xend) {
            st.site = &al_emlrtRSI;
            if (st_n >= unnamed_idx_0) {
              emlrtErrorWithMessageIdR2018a(&st, &nc_emlrtRTEI,
                "Coder:toolbox:StackPushLimit", "Coder:toolbox:StackPushLimit",
                0);
            }

            st_d_data[st_n].xstart = pmin + 2;
            st_d_data[st_n].xend = frame.xend;
            st_d_data[st_n].depth = frame.depth + 1;
            st_n++;
          }

          if (frame.xstart < pmin + 1) {
            st.site = &bl_emlrtRSI;
            if (st_n >= unnamed_idx_0) {
              emlrtErrorWithMessageIdR2018a(&st, &nc_emlrtRTEI,
                "Coder:toolbox:StackPushLimit", "Coder:toolbox:StackPushLimit",
                0);
            }

            st_d_data[st_n].xstart = frame.xstart;
            st_d_data[st_n].xend = pmin + 1;
            st_d_data[st_n].depth = frame.depth + 1;
            st_n++;
          }
        }
      }
    }
  }
}

/* End of code generation (introsort.c) */
