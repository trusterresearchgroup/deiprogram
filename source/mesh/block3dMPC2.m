function [MPCList,numMPC,NodeBC,numBC] = block3dMPC2(nx, ny, nz, dx, dy, dz)
     
numMPC = (nz-2)*(ny-2) + (nz-2)*(nx-2) + (ny-2)*(nx-2) ...
       + 3*(nx-2) + 3*(ny-2) + 3*(nz-2) + 7;
MPCList = zeros(numMPC,5);
nind = 0;
% fprintf(fid,'\nc     Now writing surface\n');
% x direction
for kk = 1:(nz-2)
    for jj = 1:(ny-2)
        node1 = nx*jj + nx*ny*kk + 1;                          % (1 ,jj,kk)
        node2 = node1 + nx - 1;                                % (nx,jj,kk)
        nind = nind + 1;
        MPCList(nind,:) = [node2,node1,dx,0,0];
    end
end

% y direction
for kk = 1:(nz-2)
    for ii = 1:(nx-2)
        node1 = ii + nx*ny*kk + 1;                             % (ii,1 ,kk)
        node2 = node1 + nx*(ny-1);                             % (ii,ny,kk)
        nind = nind + 1;
        MPCList(nind,:) = [node2,node1,0,dy,0];
    end
end

% z direction
for jj = 1:(ny-2)
    for ii = 1:(nx-2)
        node1 = ii + nx*jj + 1;                                % (ii,jj,1 )
        node2 = node1 + nx*ny*(nz-1);                          % (ii,jj,nz)
        nind = nind + 1;
        MPCList(nind,:) = [node2,node1,0,0,dz];
    end
end

% ============================== write edge ===============================
% fprintf(fid,'\nc     Now writing edge\n');
% edge x
for ii = 2:(nx-1)
    % edge pair 1
    node1 = ii;                                                % (ii,1 ,1 )
    node2 = ii + (ny-1)*nx;                                    % (ii,ny,1 )
    nind = nind + 1;
    MPCList(nind,:) = [node2,node1,0,dy,0];
    % edge pair 2
    node1 = ii;                                                % (ii,1 ,1 )
    node2 = ii + nx*ny*nz - nx;                                % (ii,ny,nz)
    nind = nind + 1;
    MPCList(nind,:) = [node2,node1,0,dy,dz];
    % edge pair 3
    node1 = ii;                                                % (ii,1 ,1 )
    node2 = ii + (nz-1)*nx*ny;                                 % (ii,1 ,nz)
    nind = nind + 1;
    MPCList(nind,:) = [node2,node1,0,0,dz];
end

% edge y
for jj = 2:(ny-1)
    % edge pair 1
    node1 = 1 + (jj-1)*nx;                                     % ( 1,jj, 1)
    node2 = jj*nx;                                             % (nx,jj, 1)
    nind = nind + 1;
    MPCList(nind,:) = [node2,node1,dx,0,0];
    % edge pair 2
    node1 = 1 + (jj-1)*nx;                                     % ( 1,jj, 1)
    node2 = jj*nx + (nz-1)*nx*ny;                              % (nx,jj,nz)
    nind = nind + 1;
    MPCList(nind,:) = [node2,node1,dx,0,dz];
    % edge pair 3
    node1 = 1 + (jj-1)*nx;                                     % ( 1,jj, 1)
    node2 = 1 + (jj-1)*nx + (nz-1)*nx*ny;                      % ( 1,jj,nz)
    nind = nind + 1;
    MPCList(nind,:) = [node2,node1,0,0,dz];
end

% edge x
for kk = 2:(nz-1)
    % edge pair 1
    node1 = 1 + (kk-1)*nx*ny;                                  % ( 1, 1,kk)
    node2 = kk*nx*ny - nx + 1;                                 % ( 1,ny,kk)
    nind = nind + 1;
    MPCList(nind,:) = [node2,node1,0,dy,0];
    % edge pair 2
    node1 = 1 + (kk-1)*nx*ny;                                  % ( 1, 1,kk)
    node2 = kk*nx*ny;                                          % (nx,ny,kk)
    nind = nind + 1;
    MPCList(nind,:) = [node2,node1,dx,dy,0];
    % edge pair 3
    node1 = 1 + (kk-1)*nx*ny;                                  % ( 1, 1,kk)
    node2 = nx + (kk-1)*nx*ny;                                 % (nx, 1,kk)
    nind = nind + 1;
    MPCList(nind,:) = [node2,node1,dx,0,0];
end

% ============================= write corner ==============================
% fprintf(fid,'\nc     Now writing corner\n');
%                      1  1  1    =>    1
%                     nx  1  1    =>    nx
%                     nx ny  1    =>    nx*ny
%                      1 ny  1    =>    (ny-1)*nx+1
%                      1  1 nz    =>    (nz-1)*nx*ny+1
%                     nx  1 nz    =>    (nz-1)*nx*ny+nx
%                     nx ny nz    =>    nx*ny*nz
%                      1 ny nz    =>    nx*ny*nz-nx+1
node1 = 1;                                                     % ( 1, 1, 1)
node2 = (nz-1)*nx*ny + 1;                                      % ( 1, 1,nz)
nind = nind + 1;
MPCList(nind,:) = [node2,node1,0,0,dz];

node1 = 1;                                                    % ( 1, 1, 1)
node2 = (nz-1)*nx*ny + nx;                                     % (nx, 1,nz)
nind = nind + 1;
MPCList(nind,:) = [node2,node1,dx,0,dz];

node1 = 1;                                         % ( 1, 1, 1)
node2 = nx*ny*nz-nx+1;                                         % ( 1,ny,nz)
nind = nind + 1;
MPCList(nind,:) = [node2,node1,0,dy,dz];

node1 = 1;                                                 % ( 1, 1, 1)
node2 = nx*ny*nz;                                              % (nx,ny,nz)
nind = nind + 1;
MPCList(nind,:) = [node2,node1,dz,dy,dz];

node1 = 1;                                                     % ( 1, 1, 1)
node2 = nx;                                                    % (nx, 1, 1)
nind = nind + 1;
MPCList(nind,:) = [node2,node1,dx,0,0];

node1 = 1;                                                     % ( 1, 1, 1)
node2 = (ny-1)*nx+1;                                           % ( 1,ny, 1)
nind = nind + 1;
MPCList(nind,:) = [node2,node1,0,dy,0];

node1 = 1;                                                    % ( 1, 1, 1)
node2 = nx*ny;                                                 % (nx,ny, 1)
nind = nind + 1;
MPCList(nind,:) = [node2,node1,dx,dy,0];

MPCList = MPCList(1:nind,:);
numMPC = nind;

% Boundary conditions
numnp = (nx)*(ny)*(nz);
FIX = (numnp-1)/2+1;
NodeBC = [FIX 1 0
          FIX 2 0
          FIX 3 0
          ];
numBC = size(NodeBC,1);