/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc2_terminate.c
 *
 * Code generation for function 'DEIPFunc2_terminate'
 *
 */

/* Include files */
#include "DEIPFunc2_terminate.h"
#include "DEIPFunc2_data.h"
#include "rt_nonfinite.h"

/* Function Definitions */
void DEIPFunc2_terminate(void)
{
  /* (no terminate code required) */
  isInitialized_DEIPFunc2 = false;
}

/* End of code generation (DEIPFunc2_terminate.c) */
