/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_DEIPFunc2_info.h
 *
 * Code generation for function 'DEIPFunc2'
 *
 */

#ifndef _CODER_DEIPFUNC2_INFO_H
#define _CODER_DEIPFUNC2_INFO_H

/* Include files */
#include "mex.h"
#ifdef __cplusplus

extern "C" {

#endif

  /* Function Declarations */
  MEXFUNCTION_LINKAGE mxArray *emlrtMexFcnProperties(void);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (_coder_DEIPFunc2_info.h) */
