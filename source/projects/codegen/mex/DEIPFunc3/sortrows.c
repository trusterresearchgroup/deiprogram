/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sortrows.c
 *
 * Code generation for function 'sortrows'
 *
 */

/* Include files */
#include "sortrows.h"
#include "DEIPFunc3_data.h"
#include "DEIPFunc3_emxutil.h"
#include "DEIPFunc3_types.h"
#include "eml_int_forloop_overflow_check.h"
#include "rt_nonfinite.h"

/* Variable Definitions */
static emlrtRSInfo ig_emlrtRSI = { 40, /* lineNo */
  "apply_row_permutation",             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pathName */
};

static emlrtRSInfo jg_emlrtRSI = { 43, /* lineNo */
  "apply_row_permutation",             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pathName */
};

static emlrtRTEInfo sl_emlrtRTEI = { 38,/* lineNo */
  23,                                  /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

static emlrtRTEInfo tl_emlrtRTEI = { 38,/* lineNo */
  1,                                   /* colNo */
  "sortrows",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\datafun\\sortrows.m"/* pName */
};

/* Function Definitions */
void apply_row_permutation(const emlrtStack *sp, emxArray_real_T *y, const
  emxArray_int32_T *idx)
{
  emlrtStack b_st;
  emlrtStack st;
  emxArray_real_T *ycol;
  int32_T i;
  int32_T m;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  emxInit_real_T(sp, &ycol, 1, &tl_emlrtRTEI, true);
  m = y->size[0];
  i = ycol->size[0];
  ycol->size[0] = y->size[0];
  emxEnsureCapacity_real_T(sp, ycol, i, &sl_emlrtRTEI);
  st.site = &ig_emlrtRSI;
  if ((1 <= m) && (m > 2147483646)) {
    b_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  for (i = 0; i < m; i++) {
    ycol->data[i] = y->data[idx->data[i] - 1];
  }

  st.site = &jg_emlrtRSI;
  if ((1 <= m) && (m > 2147483646)) {
    b_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  for (i = 0; i < m; i++) {
    y->data[i] = ycol->data[i];
  }

  st.site = &ig_emlrtRSI;
  for (i = 0; i < m; i++) {
    ycol->data[i] = y->data[(idx->data[i] + y->size[0]) - 1];
  }

  st.site = &jg_emlrtRSI;
  for (i = 0; i < m; i++) {
    y->data[i + y->size[0]] = ycol->data[i];
  }

  emxFree_real_T(&ycol);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (sortrows.c) */
