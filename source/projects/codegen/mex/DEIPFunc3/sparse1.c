/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sparse1.c
 *
 * Code generation for function 'sparse1'
 *
 */

/* Include files */
#include "sparse1.h"
#include "DEIPFunc3_data.h"
#include "DEIPFunc3_emxutil.h"
#include "DEIPFunc3_types.h"
#include "bigProduct.h"
#include "eml_int_forloop_overflow_check.h"
#include "fillIn.h"
#include "ind2sub.h"
#include "indexShapeCheck.h"
#include "locBsearch.h"
#include "parenAssign2D.h"
#include "rt_nonfinite.h"
#include "sparse.h"
#include "validateNumericIndex.h"
#include "mwmathutil.h"
#include <stddef.h>
#include <string.h>

/* Variable Definitions */
static emlrtRSInfo pk_emlrtRSI = { 1618,/* lineNo */
  "assertValidIndexArg",               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo el_emlrtRSI = { 1645,/* lineNo */
  "permuteVector",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo wl_emlrtRSI = { 250,/* lineNo */
  "sparse/parenReference",             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo xl_emlrtRSI = { 17, /* lineNo */
  "sparse/parenReference1D",           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenReference1D.m"/* pathName */
};

static emlrtRSInfo yl_emlrtRSI = { 53, /* lineNo */
  "parenReference1DNumeric",           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenReference1D.m"/* pathName */
};

static emlrtRSInfo am_emlrtRSI = { 55, /* lineNo */
  "parenReference1DNumeric",           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenReference1D.m"/* pathName */
};

static emlrtRSInfo bm_emlrtRSI = { 76, /* lineNo */
  "parenReference1DNumeric",           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenReference1D.m"/* pathName */
};

static emlrtRSInfo cm_emlrtRSI = { 82, /* lineNo */
  "parenReference1DNumeric",           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenReference1D.m"/* pathName */
};

static emlrtRSInfo dm_emlrtRSI = { 93, /* lineNo */
  "parenReference1DNumeric",           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenReference1D.m"/* pathName */
};

static emlrtRSInfo em_emlrtRSI = { 94, /* lineNo */
  "parenReference1DNumeric",           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenReference1D.m"/* pathName */
};

static emlrtRSInfo gm_emlrtRSI = { 1461,/* lineNo */
  "sparse/spallocLike",                /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo hm_emlrtRSI = { 144,/* lineNo */
  "sparse/sparse",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo jm_emlrtRSI = { 19, /* lineNo */
  "ind2sub",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\elmat\\ind2sub.m"/* pathName */
};

static emlrtRSInfo qm_emlrtRSI = { 299,/* lineNo */
  "sparse/times",                      /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo rm_emlrtRSI = { 95, /* lineNo */
  "sparse/binOp",                      /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\binOp.m"/* pathName */
};

static emlrtRSInfo sm_emlrtRSI = { 110,/* lineNo */
  "sparse/binOp",                      /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\binOp.m"/* pathName */
};

static emlrtRSInfo tm_emlrtRSI = { 292,/* lineNo */
  "allocEqsizeBinop",                  /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\binOp.m"/* pathName */
};

static emlrtRSInfo um_emlrtRSI = { 178,/* lineNo */
  "sparse/sparse",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo vm_emlrtRSI = { 219,/* lineNo */
  "sparseFullEqsizeBinOp",             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\binOp.m"/* pathName */
};

static emlrtRSInfo wm_emlrtRSI = { 264,/* lineNo */
  "sparse/parenAssign",                /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo xm_emlrtRSI = { 22, /* lineNo */
  "sparse/parenAssign1D",              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign1D.m"/* pathName */
};

static emlrtRSInfo ym_emlrtRSI = { 36, /* lineNo */
  "parenAssign1DNumeric",              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign1D.m"/* pathName */
};

static emlrtRSInfo an_emlrtRSI = { 38, /* lineNo */
  "parenAssign1DNumeric",              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign1D.m"/* pathName */
};

static emlrtRSInfo bn_emlrtRSI = { 42, /* lineNo */
  "parenAssign1DNumeric",              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign1D.m"/* pathName */
};

static emlrtRSInfo cn_emlrtRSI = { 44, /* lineNo */
  "parenAssign1DNumeric",              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign1D.m"/* pathName */
};

static emlrtRSInfo dn_emlrtRSI = { 48, /* lineNo */
  "parenAssign1DNumeric",              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign1D.m"/* pathName */
};

static emlrtRSInfo en_emlrtRSI = { 50, /* lineNo */
  "parenAssign1DNumeric",              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign1D.m"/* pathName */
};

static emlrtRSInfo fn_emlrtRSI = { 277,/* lineNo */
  "sparse/full",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo ln_emlrtRSI = { 115,/* lineNo */
  "parenAssign2DNumericImpl",          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign2D.m"/* pathName */
};

static emlrtRSInfo nn_emlrtRSI = { 123,/* lineNo */
  "parenAssign2DNumericImpl",          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign2D.m"/* pathName */
};

static emlrtRSInfo bo_emlrtRSI = { 252,/* lineNo */
  "sparse/parenReference",             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo co_emlrtRSI = { 25, /* lineNo */
  "sparse/parenReference2D",           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenReference2D.m"/* pathName */
};

static emlrtRSInfo do_emlrtRSI = { 32, /* lineNo */
  "parenReference2DNumeric",           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenReference2D.m"/* pathName */
};

static emlrtRSInfo eo_emlrtRSI = { 33, /* lineNo */
  "parenReference2DNumeric",           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenReference2D.m"/* pathName */
};

static emlrtRSInfo fo_emlrtRSI = { 36, /* lineNo */
  "parenReference2DNumeric",           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenReference2D.m"/* pathName */
};

static emlrtRSInfo go_emlrtRSI = { 51, /* lineNo */
  "parenReference2DNumericImpl",       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenReference2D.m"/* pathName */
};

static emlrtRSInfo ho_emlrtRSI = { 53, /* lineNo */
  "parenReference2DNumericImpl",       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenReference2D.m"/* pathName */
};

static emlrtRSInfo io_emlrtRSI = { 275,/* lineNo */
  "sparse/full",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo ro_emlrtRSI = { 20, /* lineNo */
  "sparse/parenReference2D",           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenReference2D.m"/* pathName */
};

static emlrtRSInfo so_emlrtRSI = { 81, /* lineNo */
  "parenReference2DColumns",           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenReference2D.m"/* pathName */
};

static emlrtRSInfo to_emlrtRSI = { 93, /* lineNo */
  "parenReference2DColumns",           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenReference2D.m"/* pathName */
};

static emlrtRSInfo uo_emlrtRSI = { 104,/* lineNo */
  "parenReference2DColumns",           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenReference2D.m"/* pathName */
};

static emlrtRSInfo vo_emlrtRSI = { 315,/* lineNo */
  "sparse/gt",                         /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo wo_emlrtRSI = { 143,/* lineNo */
  "sparse/binOp",                      /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\binOp.m"/* pathName */
};

static emlrtRSInfo xo_emlrtRSI = { 18, /* lineNo */
  "spfun",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\sparfun\\spfun.m"/* pathName */
};

static emlrtRSInfo yo_emlrtRSI = { 443,/* lineNo */
  "sparse/spfunImpl",                  /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo ap_emlrtRSI = { 446,/* lineNo */
  "sparse/spfunImpl",                  /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo bp_emlrtRSI = { 449,/* lineNo */
  "sparse/spfunImpl",                  /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo cp_emlrtRSI = { 343,/* lineNo */
  "sparse/and",                        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo dp_emlrtRSI = { 331,/* lineNo */
  "sparse/eq",                         /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo ep_emlrtRSI = { 115,/* lineNo */
  "sparse/binOp",                      /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\binOp.m"/* pathName */
};

static emlrtRSInfo eq_emlrtRSI = { 138,/* lineNo */
  "sparse/binOp",                      /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\binOp.m"/* pathName */
};

static emlrtRSInfo fq_emlrtRSI = { 239,/* lineNo */
  "scalarBinOp",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\binOp.m"/* pathName */
};

static emlrtRSInfo gq_emlrtRSI = { 147,/* lineNo */
  "sparse/binOp",                      /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\binOp.m"/* pathName */
};

static emlrtRSInfo hq_emlrtRSI = { 149,/* lineNo */
  "sparse/binOp",                      /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\binOp.m"/* pathName */
};

static emlrtRSInfo oq_emlrtRSI = { 89, /* lineNo */
  "parenAssign2DNumericImpl",          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign2D.m"/* pathName */
};

static emlrtRTEInfo r_emlrtRTEI = { 1620,/* lineNo */
  31,                                  /* colNo */
  "assertValidIndexArg",               /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo s_emlrtRTEI = { 1622,/* lineNo */
  31,                                  /* colNo */
  "assertValidIndexArg",               /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo t_emlrtRTEI = { 1624,/* lineNo */
  31,                                  /* colNo */
  "assertValidIndexArg",               /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo x_emlrtRTEI = { 85,/* lineNo */
  1,                                   /* colNo */
  "parenReference1DNumeric",           /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenReference1D.m"/* pName */
};

static emlrtRTEInfo y_emlrtRTEI = { 1591,/* lineNo */
  9,                                   /* colNo */
  "assertValidSize",                   /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo eb_emlrtRTEI = { 53,/* lineNo */
  27,                                  /* colNo */
  "sparse/binOp",                      /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\binOp.m"/* pName */
};

static emlrtRTEInfo fb_emlrtRTEI = { 286,/* lineNo */
  9,                                   /* colNo */
  "allocEqsizeBinop",                  /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\binOp.m"/* pName */
};

static emlrtRTEInfo gb_emlrtRTEI = { 44,/* lineNo */
  1,                                   /* colNo */
  "parenReference2DNumericImpl",       /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenReference2D.m"/* pName */
};

static emlrtRTEInfo jb_emlrtRTEI = { 92,/* lineNo */
  1,                                   /* colNo */
  "parenReference2DColumns",           /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenReference2D.m"/* pName */
};

static emlrtRTEInfo kb_emlrtRTEI = { 441,/* lineNo */
  34,                                  /* colNo */
  "sparse/spfunImpl",                  /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo pc_emlrtRTEI = { 11,/* lineNo */
  23,                                  /* colNo */
  "sparse/parenAssign1D",              /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign1D.m"/* pName */
};

static emlrtRTEInfo ae_emlrtRTEI = { 1615,/* lineNo */
  27,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo ie_emlrtRTEI = { 250,/* lineNo */
  21,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo je_emlrtRTEI = { 250,/* lineNo */
  17,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo ke_emlrtRTEI = { 242,/* lineNo */
  22,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo le_emlrtRTEI = { 95,/* lineNo */
  5,                                   /* colNo */
  "binOp",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\binOp.m"/* pName */
};

static emlrtRTEInfo me_emlrtRTEI = { 1461,/* lineNo */
  13,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo ne_emlrtRTEI = { 252,/* lineNo */
  17,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo oe_emlrtRTEI = { 274,/* lineNo */
  13,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo qe_emlrtRTEI = { 440,/* lineNo */
  12,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo re_emlrtRTEI = { 315,/* lineNo */
  13,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo se_emlrtRTEI = { 331,/* lineNo */
  13,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo ff_emlrtRTEI = { 343,/* lineNo */
  13,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo em_emlrtRTEI = { 1644,/* lineNo */
  5,                                   /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo fm_emlrtRTEI = { 48,/* lineNo */
  21,                                  /* colNo */
  "parenAssign1D",                     /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\parenAssign1D.m"/* pName */
};

/* Function Declarations */
static void b_sparse_spallocLike(const emlrtStack *sp, int32_T m, int32_T nzmax,
  emxArray_boolean_T *s_d, emxArray_int32_T *s_colidx, emxArray_int32_T
  *s_rowidx, int32_T *s_m);

/* Function Definitions */
static void b_sparse_spallocLike(const emlrtStack *sp, int32_T m, int32_T nzmax,
  emxArray_boolean_T *s_d, emxArray_int32_T *s_colidx, emxArray_int32_T
  *s_rowidx, int32_T *s_m)
{
  emlrtStack b_st;
  emlrtStack st;
  int32_T i;
  int32_T numalloc;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &gm_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  b_st.site = &hm_emlrtRSI;
  if (m < 0) {
    emlrtErrorWithMessageIdR2018a(&b_st, &y_emlrtRTEI,
      "Coder:toolbox:SparseNegativeSize", "Coder:toolbox:SparseNegativeSize", 0);
  }

  if (m >= MAX_int32_T) {
    emlrtErrorWithMessageIdR2018a(&b_st, &ab_emlrtRTEI,
      "Coder:toolbox:SparseMaxSize", "Coder:toolbox:SparseMaxSize", 2, 12,
      MAX_int32_T);
  }

  b_st.site = &um_emlrtRSI;
  if (nzmax < 0) {
    emlrtErrorWithMessageIdR2018a(&b_st, &y_emlrtRTEI,
      "Coder:toolbox:SparseNegativeSize", "Coder:toolbox:SparseNegativeSize", 0);
  }

  if (nzmax >= MAX_int32_T) {
    emlrtErrorWithMessageIdR2018a(&b_st, &ab_emlrtRTEI,
      "Coder:toolbox:SparseMaxSize", "Coder:toolbox:SparseMaxSize", 2, 12,
      MAX_int32_T);
  }

  if (nzmax >= 1) {
    numalloc = nzmax;
  } else {
    numalloc = 1;
  }

  i = s_d->size[0];
  s_d->size[0] = numalloc;
  emxEnsureCapacity_boolean_T(&st, s_d, i, &me_emlrtRTEI);
  for (i = 0; i < numalloc; i++) {
    s_d->data[i] = false;
  }

  i = s_colidx->size[0];
  s_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(&st, s_colidx, i, &wd_emlrtRTEI);
  i = s_rowidx->size[0];
  s_rowidx->size[0] = numalloc;
  emxEnsureCapacity_int32_T(&st, s_rowidx, i, &me_emlrtRTEI);
  for (i = 0; i < numalloc; i++) {
    s_rowidx->data[i] = 0;
  }

  s_colidx->data[0] = 1;
  s_colidx->data[1] = 1;
  *s_m = m;
}

void assertValidIndexArg(const emlrtStack *sp, const emxArray_real_T *s,
  emxArray_int32_T *sint)
{
  emlrtStack b_st;
  emlrtStack st;
  int32_T k;
  int32_T ns;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  ns = s->size[0] * s->size[1];
  k = sint->size[0];
  sint->size[0] = ns;
  emxEnsureCapacity_int32_T(sp, sint, k, &ae_emlrtRTEI);
  st.site = &pk_emlrtRSI;
  if ((1 <= ns) && (ns > 2147483646)) {
    b_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  for (k = 0; k < ns; k++) {
    if (!(muDoubleScalarFloor(s->data[k]) == s->data[k])) {
      emlrtErrorWithMessageIdR2018a(sp, &r_emlrtRTEI,
        "MATLAB:sparsfcn:nonIntegerIndex", "MATLAB:sparsfcn:nonIntegerIndex", 0);
    }

    if (!(s->data[k] < 2.147483647E+9)) {
      emlrtErrorWithMessageIdR2018a(sp, &s_emlrtRTEI,
        "MATLAB:sparsfcn:largeIndex", "MATLAB:sparsfcn:largeIndex", 0);
    }

    if (!(0.0 < s->data[k])) {
      emlrtErrorWithMessageIdR2018a(sp, &t_emlrtRTEI,
        "MATLAB:sparsfcn:nonPosIndex", "MATLAB:sparsfcn:nonPosIndex", 0);
    }

    sint->data[k] = (int32_T)s->data[k];
  }
}

void b_assertValidIndexArg(const emlrtStack *sp, const emxArray_real_T *s,
  emxArray_int32_T *sint)
{
  emlrtStack b_st;
  emlrtStack st;
  real_T d;
  int32_T k;
  int32_T ns;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  ns = s->size[1];
  k = sint->size[0];
  sint->size[0] = s->size[1];
  emxEnsureCapacity_int32_T(sp, sint, k, &ae_emlrtRTEI);
  st.site = &pk_emlrtRSI;
  if ((1 <= s->size[1]) && (s->size[1] > 2147483646)) {
    b_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  for (k = 0; k < ns; k++) {
    d = s->data[k];
    if (!(muDoubleScalarFloor(d) == d)) {
      emlrtErrorWithMessageIdR2018a(sp, &r_emlrtRTEI,
        "MATLAB:sparsfcn:nonIntegerIndex", "MATLAB:sparsfcn:nonIntegerIndex", 0);
    }

    if (!(d < 2.147483647E+9)) {
      emlrtErrorWithMessageIdR2018a(sp, &s_emlrtRTEI,
        "MATLAB:sparsfcn:largeIndex", "MATLAB:sparsfcn:largeIndex", 0);
    }

    if (!(0.0 < d)) {
      emlrtErrorWithMessageIdR2018a(sp, &t_emlrtRTEI,
        "MATLAB:sparsfcn:nonPosIndex", "MATLAB:sparsfcn:nonPosIndex", 0);
    }

    sint->data[k] = (int32_T)d;
  }
}

void b_sparse_and(const emlrtStack *sp, const emxArray_boolean_T *a_d, const
                  emxArray_int32_T *a_colidx, const emxArray_boolean_T *b_d,
                  const emxArray_int32_T *b_colidx, const emxArray_int32_T
                  *b_rowidx, h_sparse *s)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack st;
  emxArray_boolean_T *tmpd;
  int32_T i;
  int32_T loop_ub;
  int32_T nzs_tmp;
  boolean_T overflow;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  st.site = &cp_emlrtRSI;
  if (a_colidx->data[a_colidx->size[0] - 1] - 1 > 0) {
    overflow = a_d->data[0];
  } else {
    overflow = false;
  }

  b_st.site = &wo_emlrtRSI;
  c_st.site = &xo_emlrtRSI;
  nzs_tmp = b_colidx->data[b_colidx->size[0] - 1];
  if (1 > b_colidx->data[b_colidx->size[0] - 1] - 1) {
    loop_ub = 0;
  } else {
    loop_ub = b_colidx->data[b_colidx->size[0] - 1] - 1;
  }

  emxInit_boolean_T(&c_st, &tmpd, 1, &qe_emlrtRTEI, true);
  i = tmpd->size[0];
  tmpd->size[0] = loop_ub;
  emxEnsureCapacity_boolean_T(&c_st, tmpd, i, &qe_emlrtRTEI);
  for (i = 0; i < loop_ub; i++) {
    tmpd->data[i] = (overflow && b_d->data[i]);
  }

  if (tmpd->size[0] != b_colidx->data[b_colidx->size[0] - 1] - 1) {
    emlrtErrorWithMessageIdR2018a(&c_st, &kb_emlrtRTEI, "MATLAB:samelen",
      "MATLAB:samelen", 0);
  }

  d_st.site = &yo_emlrtRSI;
  c_sparse_spallocLike(&d_st, b_colidx->data[b_colidx->size[0] - 1] - 1, s->d,
                       s->colidx, s->rowidx);
  if (1 > b_colidx->data[b_colidx->size[0] - 1] - 1) {
    loop_ub = 1;
  } else {
    loop_ub = b_colidx->data[b_colidx->size[0] - 1];
  }

  for (i = 0; i <= loop_ub - 2; i++) {
    s->rowidx->data[i] = b_rowidx->data[i];
  }

  i = s->colidx->size[0];
  s->colidx->size[0] = b_colidx->size[0];
  emxEnsureCapacity_int32_T(&c_st, s->colidx, i, &ff_emlrtRTEI);
  loop_ub = b_colidx->size[0];
  for (i = 0; i < loop_ub; i++) {
    s->colidx->data[i] = b_colidx->data[i];
  }

  d_st.site = &ap_emlrtRSI;
  if (1 > b_colidx->data[b_colidx->size[0] - 1] - 1) {
    overflow = false;
  } else {
    overflow = (b_colidx->data[b_colidx->size[0] - 1] - 1 > 2147483646);
  }

  if (overflow) {
    e_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&e_st);
  }

  for (loop_ub = 0; loop_ub <= nzs_tmp - 2; loop_ub++) {
    s->d->data[loop_ub] = tmpd->data[loop_ub];
  }

  emxFree_boolean_T(&tmpd);
  d_st.site = &bp_emlrtRSI;
  d_sparse_fillIn(&d_st, s);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

void b_sparse_eq(const emlrtStack *sp, const emxArray_real_T *a_d, const
                 emxArray_int32_T *a_colidx, const emxArray_real_T *b_d, const
                 emxArray_int32_T *b_colidx, const emxArray_int32_T *b_rowidx,
                 h_sparse *s)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack st;
  emxArray_boolean_T *tmpd;
  real_T uniOp_tunableEnvironment_idx_0;
  int32_T a;
  int32_T b;
  int32_T idx;
  boolean_T S;
  boolean_T overflow;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  st.site = &dp_emlrtRSI;
  if (a_colidx->data[a_colidx->size[0] - 1] - 1 > 0) {
    uniOp_tunableEnvironment_idx_0 = a_d->data[0];
  } else {
    uniOp_tunableEnvironment_idx_0 = 0.0;
  }

  if (!(uniOp_tunableEnvironment_idx_0 == 0.0)) {
    b_st.site = &wo_emlrtRSI;
    c_st.site = &xo_emlrtRSI;
    b = b_colidx->data[b_colidx->size[0] - 1];
    if (1 > b_colidx->data[b_colidx->size[0] - 1] - 1) {
      a = 0;
    } else {
      a = b_colidx->data[b_colidx->size[0] - 1] - 1;
    }

    emxInit_boolean_T(&c_st, &tmpd, 1, &qe_emlrtRTEI, true);
    idx = tmpd->size[0];
    tmpd->size[0] = a;
    emxEnsureCapacity_boolean_T(&c_st, tmpd, idx, &qe_emlrtRTEI);
    for (idx = 0; idx < a; idx++) {
      tmpd->data[idx] = (uniOp_tunableEnvironment_idx_0 == b_d->data[idx]);
    }

    if (tmpd->size[0] != b_colidx->data[b_colidx->size[0] - 1] - 1) {
      emlrtErrorWithMessageIdR2018a(&c_st, &kb_emlrtRTEI, "MATLAB:samelen",
        "MATLAB:samelen", 0);
    }

    d_st.site = &yo_emlrtRSI;
    c_sparse_spallocLike(&d_st, b_colidx->data[b_colidx->size[0] - 1] - 1, s->d,
                         s->colidx, s->rowidx);
    if (1 > b_colidx->data[b_colidx->size[0] - 1] - 1) {
      a = 1;
    } else {
      a = b_colidx->data[b_colidx->size[0] - 1];
    }

    for (idx = 0; idx <= a - 2; idx++) {
      s->rowidx->data[idx] = b_rowidx->data[idx];
    }

    idx = s->colidx->size[0];
    s->colidx->size[0] = b_colidx->size[0];
    emxEnsureCapacity_int32_T(&c_st, s->colidx, idx, &se_emlrtRTEI);
    a = b_colidx->size[0];
    for (idx = 0; idx < a; idx++) {
      s->colidx->data[idx] = b_colidx->data[idx];
    }

    d_st.site = &ap_emlrtRSI;
    if (1 > b_colidx->data[b_colidx->size[0] - 1] - 1) {
      overflow = false;
    } else {
      overflow = (b_colidx->data[b_colidx->size[0] - 1] - 1 > 2147483646);
    }

    if (overflow) {
      e_st.site = &fg_emlrtRSI;
      check_forloop_overflow_error(&e_st);
    }

    for (a = 0; a <= b - 2; a++) {
      s->d->data[a] = tmpd->data[a];
    }

    emxFree_boolean_T(&tmpd);
    d_st.site = &bp_emlrtRSI;
    d_sparse_fillIn(&d_st, s);
  } else {
    b_st.site = &gq_emlrtRSI;
    S = true;
    a = b_colidx->data[0];
    b = b_colidx->data[1] - 1;
    c_st.site = &fq_emlrtRSI;
    if (b_colidx->data[0] > b_colidx->data[1] - 1) {
      overflow = false;
    } else {
      overflow = (b_colidx->data[1] - 1 > 2147483646);
    }

    if (overflow) {
      d_st.site = &fg_emlrtRSI;
      check_forloop_overflow_error(&d_st);
    }

    for (idx = a; idx <= b; idx++) {
      S = (uniOp_tunableEnvironment_idx_0 == b_d->data[0]);
    }

    b_st.site = &hq_emlrtRSI;
    d_sparse(&b_st, S, s->d, s->colidx, s->rowidx);
  }

  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

void b_sparse_full(const emlrtStack *sp, const emxArray_real_T *this_d, const
                   emxArray_int32_T *this_colidx, const emxArray_int32_T
                   *this_rowidx, int32_T this_m, int32_T this_n, emxArray_real_T
                   *y)
{
  emlrtStack b_st;
  emlrtStack st;
  int32_T a;
  int32_T c;
  int32_T idx;
  int32_T loop_ub;
  boolean_T overflow;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  a = y->size[0] * y->size[1];
  y->size[0] = this_m;
  y->size[1] = this_n;
  emxEnsureCapacity_real_T(sp, y, a, &oe_emlrtRTEI);
  loop_ub = this_m * this_n;
  for (a = 0; a < loop_ub; a++) {
    y->data[a] = 0.0;
  }

  st.site = &io_emlrtRSI;
  if ((1 <= this_n) && (this_n > 2147483646)) {
    b_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  for (c = 0; c < this_n; c++) {
    loop_ub = this_colidx->data[c + 1] - 1;
    a = this_colidx->data[c];
    st.site = &fn_emlrtRSI;
    if (this_colidx->data[c] > loop_ub) {
      overflow = false;
    } else {
      overflow = (loop_ub > 2147483646);
    }

    if (overflow) {
      b_st.site = &fg_emlrtRSI;
      check_forloop_overflow_error(&b_st);
    }

    for (idx = a; idx <= loop_ub; idx++) {
      y->data[(this_rowidx->data[idx - 1] + y->size[0] * c) - 1] = this_d->
        data[idx - 1];
    }
  }
}

void b_sparse_gt(const emlrtStack *sp, const emxArray_real_T *a_d, const
                 emxArray_int32_T *a_colidx, emxArray_boolean_T *s_d,
                 emxArray_int32_T *s_colidx, emxArray_int32_T *s_rowidx)
{
  emlrtStack b_st;
  emlrtStack st;
  real_T uniOp_tunableEnvironment_idx_0;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &vo_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  if (a_colidx->data[a_colidx->size[0] - 1] - 1 > 0) {
    uniOp_tunableEnvironment_idx_0 = a_d->data[0];
  } else {
    uniOp_tunableEnvironment_idx_0 = 0.0;
  }

  b_st.site = &eq_emlrtRSI;
  d_sparse(&b_st, uniOp_tunableEnvironment_idx_0 > 0.0, s_d, s_colidx, s_rowidx);
}

void b_sparse_parenAssign(const emlrtStack *sp, e_sparse *this, const
  emxArray_real_T *rhs, const emxArray_real_T *varargin_1)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack st;
  real_T b_this[2];
  int32_T k;
  int32_T nidx;
  int32_T overflow;
  int32_T varargout_4;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &wm_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  b_bigProduct(this->m, this->n, &nidx, &overflow);
  if (rhs->size[0] != varargin_1->size[0]) {
    emlrtErrorWithMessageIdR2018a(&st, &pc_emlrtRTEI,
      "MATLAB:subsassignnumelmismatch", "MATLAB:subsassignnumelmismatch", 0);
  }

  b_st.site = &xm_emlrtRSI;
  if (overflow == 0) {
    c_st.site = &ym_emlrtRSI;
    sparse_validateNumericIndex(&c_st, this->m * this->n, varargin_1);
  } else {
    c_st.site = &an_emlrtRSI;
    sparse_validateNumericIndex(&c_st, MAX_int32_T, varargin_1);
  }

  nidx = varargin_1->size[0];
  c_st.site = &bn_emlrtRSI;
  if ((1 <= varargin_1->size[0]) && (varargin_1->size[0] > 2147483646)) {
    d_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&d_st);
  }

  for (k = 0; k < nidx; k++) {
    c_st.site = &cn_emlrtRSI;
    b_this[0] = this->m;
    b_this[1] = this->n;
    d_st.site = &jm_emlrtRSI;
    b_ind2sub_indexClass(&d_st, b_this, varargin_1->data[k], &overflow,
                         &varargout_4);
    c_st.site = &en_emlrtRSI;
    sparse_parenAssign2D(&c_st, this, rhs->data[k], overflow, varargout_4);
  }
}

void b_sparse_parenReference(const emlrtStack *sp, const emxArray_real_T *this_d,
  const emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx,
  int32_T this_m, int32_T this_n, const emxArray_real_T *varargin_1, const
  real_T varargin_2_data[], const int32_T varargin_2_size[2], emxArray_real_T
  *s_d, emxArray_int32_T *s_colidx, emxArray_int32_T *s_rowidx, int32_T *s_m,
  int32_T *s_n, int32_T *s_maxnz)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack st;
  emxArray_real_T b_varargin_2_data;
  real_T nt;
  real_T s_d_tmp;
  int32_T cidx;
  int32_T colNnz;
  int32_T i;
  int32_T i1;
  int32_T idx;
  int32_T k;
  int32_T ridx;
  int32_T sm;
  int32_T sn;
  boolean_T found;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &bo_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  b_st.site = &co_emlrtRSI;
  c_st.site = &do_emlrtRSI;
  c_sparse_validateNumericIndex(&c_st, this_m, varargin_1);
  b_varargin_2_data.data = (real_T *)&varargin_2_data[0];
  b_varargin_2_data.size = (int32_T *)&varargin_2_size[0];
  b_varargin_2_data.allocatedSize = -1;
  b_varargin_2_data.numDimensions = 2;
  b_varargin_2_data.canFreeData = false;
  c_st.site = &eo_emlrtRSI;
  c_sparse_validateNumericIndex(&c_st, this_n, &b_varargin_2_data);
  sm = varargin_1->size[1];
  sn = varargin_2_size[1];
  c_st.site = &fo_emlrtRSI;
  if (0 > varargin_1->size[1] * varargin_2_size[1]) {
    emlrtErrorWithMessageIdR2018a(&c_st, &gb_emlrtRTEI,
      "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
  }

  s_d->size[0] = 0;
  s_rowidx->size[0] = 0;
  i = s_colidx->size[0];
  s_colidx->size[0] = varargin_2_size[1] + 1;
  emxEnsureCapacity_int32_T(&c_st, s_colidx, i, &ne_emlrtRTEI);
  colNnz = varargin_2_size[1];
  for (i = 0; i <= colNnz; i++) {
    s_colidx->data[i] = 0;
  }

  s_colidx->data[0] = 1;
  colNnz = 1;
  k = 0;
  d_st.site = &go_emlrtRSI;
  for (cidx = 0; cidx < sn; cidx++) {
    nt = varargin_2_data[cidx];
    d_st.site = &ho_emlrtRSI;
    if ((1 <= sm) && (sm > 2147483646)) {
      e_st.site = &fg_emlrtRSI;
      check_forloop_overflow_error(&e_st);
    }

    for (ridx = 0; ridx < sm; ridx++) {
      b_sparse_locBsearch(this_rowidx, (int32_T)varargin_1->data[ridx],
                          this_colidx->data[(int32_T)nt - 1], this_colidx->data
                          [(int32_T)nt], &idx, &found);
      if (found) {
        i = s_d->size[0];
        i1 = s_d->size[0];
        s_d->size[0]++;
        emxEnsureCapacity_real_T(&c_st, s_d, i1, &ke_emlrtRTEI);
        s_d_tmp = this_d->data[idx - 1];
        s_d->data[i] = s_d_tmp;
        i = s_rowidx->size[0];
        i1 = s_rowidx->size[0];
        s_rowidx->size[0]++;
        emxEnsureCapacity_int32_T(&c_st, s_rowidx, i1, &ke_emlrtRTEI);
        s_rowidx->data[i] = ridx + 1;
        s_d->data[k] = s_d_tmp;
        s_rowidx->data[k] = ridx + 1;
        k++;
        colNnz++;
      }
    }

    s_colidx->data[cidx + 1] = colNnz;
  }

  if (s_colidx->data[s_colidx->size[0] - 1] - 1 == 0) {
    i = s_rowidx->size[0];
    s_rowidx->size[0] = 1;
    emxEnsureCapacity_int32_T(&c_st, s_rowidx, i, &ne_emlrtRTEI);
    s_rowidx->data[0] = 1;
    i = s_d->size[0];
    s_d->size[0] = 1;
    emxEnsureCapacity_real_T(&c_st, s_d, i, &ne_emlrtRTEI);
    s_d->data[0] = 0.0;
  }

  *s_m = varargin_1->size[1];
  *s_n = varargin_2_size[1];
  if (s_colidx->data[s_colidx->size[0] - 1] - 1 >= 1) {
    *s_maxnz = s_colidx->data[s_colidx->size[0] - 1] - 1;
  } else {
    *s_maxnz = 1;
  }
}

void c_sparse_eq(const emlrtStack *sp, real_T a, const emxArray_real_T *b_d,
                 const emxArray_int32_T *b_colidx, const emxArray_int32_T
                 *b_rowidx, int32_T b_m, g_sparse *s)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack st;
  emxArray_boolean_T *tmpd;
  int32_T i;
  int32_T loop_ub;
  int32_T nzs_tmp;
  boolean_T overflow;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  st.site = &dp_emlrtRSI;
  b_st.site = &wo_emlrtRSI;
  c_st.site = &xo_emlrtRSI;
  nzs_tmp = b_colidx->data[b_colidx->size[0] - 1];
  if (1 > b_colidx->data[b_colidx->size[0] - 1] - 1) {
    loop_ub = 0;
  } else {
    loop_ub = b_colidx->data[b_colidx->size[0] - 1] - 1;
  }

  emxInit_boolean_T(&c_st, &tmpd, 1, &qe_emlrtRTEI, true);
  i = tmpd->size[0];
  tmpd->size[0] = loop_ub;
  emxEnsureCapacity_boolean_T(&c_st, tmpd, i, &qe_emlrtRTEI);
  for (i = 0; i < loop_ub; i++) {
    tmpd->data[i] = (a == b_d->data[i]);
  }

  if (tmpd->size[0] != b_colidx->data[b_colidx->size[0] - 1] - 1) {
    emlrtErrorWithMessageIdR2018a(&c_st, &kb_emlrtRTEI, "MATLAB:samelen",
      "MATLAB:samelen", 0);
  }

  d_st.site = &yo_emlrtRSI;
  b_sparse_spallocLike(&d_st, b_m, b_colidx->data[b_colidx->size[0] - 1] - 1,
                       s->d, s->colidx, s->rowidx, &s->m);
  if (1 > b_colidx->data[b_colidx->size[0] - 1] - 1) {
    loop_ub = 1;
  } else {
    loop_ub = b_colidx->data[b_colidx->size[0] - 1];
  }

  for (i = 0; i <= loop_ub - 2; i++) {
    s->rowidx->data[i] = b_rowidx->data[i];
  }

  i = s->colidx->size[0];
  s->colidx->size[0] = b_colidx->size[0];
  emxEnsureCapacity_int32_T(&c_st, s->colidx, i, &se_emlrtRTEI);
  loop_ub = b_colidx->size[0];
  for (i = 0; i < loop_ub; i++) {
    s->colidx->data[i] = b_colidx->data[i];
  }

  d_st.site = &ap_emlrtRSI;
  if (1 > b_colidx->data[b_colidx->size[0] - 1] - 1) {
    overflow = false;
  } else {
    overflow = (b_colidx->data[b_colidx->size[0] - 1] - 1 > 2147483646);
  }

  if (overflow) {
    e_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&e_st);
  }

  for (loop_ub = 0; loop_ub <= nzs_tmp - 2; loop_ub++) {
    s->d->data[loop_ub] = tmpd->data[loop_ub];
  }

  emxFree_boolean_T(&tmpd);
  d_st.site = &bp_emlrtRSI;
  c_sparse_fillIn(&d_st, s);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

void c_sparse_full(const emlrtStack *sp, const emxArray_real_T *this_d, const
                   emxArray_int32_T *this_colidx, const emxArray_int32_T
                   *this_rowidx, int32_T this_m, emxArray_real_T *y)
{
  emlrtStack b_st;
  emlrtStack st;
  int32_T a;
  int32_T cend;
  int32_T idx;
  boolean_T overflow;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  cend = y->size[0] * y->size[1];
  y->size[0] = this_m;
  y->size[1] = 1;
  emxEnsureCapacity_real_T(sp, y, cend, &oe_emlrtRTEI);
  for (cend = 0; cend < this_m; cend++) {
    y->data[cend] = 0.0;
  }

  cend = this_colidx->data[1] - 1;
  a = this_colidx->data[0];
  st.site = &fn_emlrtRSI;
  if (this_colidx->data[0] > this_colidx->data[1] - 1) {
    overflow = false;
  } else {
    overflow = (this_colidx->data[1] - 1 > 2147483646);
  }

  if (overflow) {
    b_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  for (idx = a; idx <= cend; idx++) {
    y->data[this_rowidx->data[idx - 1] - 1] = this_d->data[idx - 1];
  }
}

void c_sparse_parenAssign(const emlrtStack *sp, e_sparse *this, real_T rhs,
  const emxArray_real_T *varargin_1, real_T varargin_2)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack st;
  real_T thisv;
  int32_T nelem;
  int32_T ridx;
  int32_T sm;
  int32_T vidx;
  boolean_T found;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &iq_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  b_st.site = &gn_emlrtRSI;
  c_st.site = &hn_emlrtRSI;
  sparse_validateNumericIndex(&c_st, this->m, varargin_1);
  c_st.site = &in_emlrtRSI;
  b_sparse_validateNumericIndex(&c_st, this->n, varargin_2);
  sm = varargin_1->size[0];
  c_st.site = &jn_emlrtRSI;
  d_st.site = &oq_emlrtRSI;
  if ((1 <= varargin_1->size[0]) && (varargin_1->size[0] > 2147483646)) {
    e_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&e_st);
  }

  for (ridx = 0; ridx < sm; ridx++) {
    b_sparse_locBsearch(this->rowidx, (int32_T)varargin_1->data[ridx],
                        this->colidx->data[(int32_T)varargin_2 - 1],
                        this->colidx->data[(int32_T)varargin_2], &vidx, &found);
    if (found) {
      thisv = this->d->data[vidx - 1];
    } else {
      thisv = 0.0;
    }

    if ((!(thisv == 0.0)) || (!(rhs == 0.0))) {
      if ((thisv != 0.0) && (rhs != 0.0)) {
        this->d->data[vidx - 1] = rhs;
      } else if (thisv == 0.0) {
        if (this->colidx->data[this->colidx->size[0] - 1] - 1 == this->maxnz) {
          d_st.site = &kn_emlrtRSI;
          b_realloc(&d_st, this, this->colidx->data[this->colidx->size[0] - 1] +
                    9, vidx, vidx + 1, this->colidx->data[this->colidx->size[0]
                    - 1] - 1);
          this->rowidx->data[vidx] = (int32_T)varargin_1->data[ridx];
          this->d->data[vidx] = rhs;
        } else {
          d_st.site = &ln_emlrtRSI;
          nelem = (this->colidx->data[this->colidx->size[0] - 1] - vidx) - 1;
          if (nelem > 0) {
            memmove((void *)&this->rowidx->data[vidx + 1], (void *)&this->
                    rowidx->data[vidx], (uint32_T)((size_t)nelem * sizeof
                     (int32_T)));
            memmove((void *)&this->d->data[vidx + 1], (void *)&this->d->
                    data[vidx], (uint32_T)((size_t)nelem * sizeof(real_T)));
          }

          this->d->data[vidx] = rhs;
          this->rowidx->data[vidx] = (int32_T)varargin_1->data[ridx];
        }

        d_st.site = &mn_emlrtRSI;
        incrColIdx(&d_st, this, (int32_T)varargin_2);
      } else {
        d_st.site = &nn_emlrtRSI;
        nelem = (this->colidx->data[this->colidx->size[0] - 1] - vidx) - 1;
        if (nelem > 0) {
          memmove((void *)&this->rowidx->data[vidx - 1], (void *)&this->
                  rowidx->data[vidx], (uint32_T)((size_t)nelem * sizeof(int32_T)));
          memmove((void *)&this->d->data[vidx - 1], (void *)&this->d->data[vidx],
                  (uint32_T)((size_t)nelem * sizeof(real_T)));
        }

        d_st.site = &on_emlrtRSI;
        decrColIdx(&d_st, this, (int32_T)varargin_2);
      }
    }
  }
}

void c_sparse_parenReference(const emlrtStack *sp, const emxArray_real_T *this_d,
  const emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx,
  int32_T this_m, int32_T this_n, real_T varargin_2, f_sparse *s)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack st;
  int32_T b_nd_tmp;
  int32_T i;
  int32_T k;
  int32_T nd_tmp;
  int32_T outIdx;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &bo_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  b_st.site = &ro_emlrtRSI;
  c_st.site = &so_emlrtRSI;
  b_sparse_validateNumericIndex(&c_st, this_n, varargin_2);
  nd_tmp = this_colidx->data[(int32_T)varargin_2 - 1];
  b_nd_tmp = this_colidx->data[(int32_T)varargin_2] - nd_tmp;
  if ((b_nd_tmp > this_m) && (this_m != 0)) {
    emlrtErrorWithMessageIdR2018a(&b_st, &jb_emlrtRTEI,
      "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
  }

  c_st.site = &to_emlrtRSI;
  sparse_spallocLike(&c_st, this_m, b_nd_tmp, s);
  if (b_nd_tmp != 0) {
    outIdx = 0;
    c_st.site = &uo_emlrtRSI;
    if ((1 <= b_nd_tmp) && (b_nd_tmp > 2147483646)) {
      d_st.site = &fg_emlrtRSI;
      check_forloop_overflow_error(&d_st);
    }

    for (k = 0; k < b_nd_tmp; k++) {
      i = (nd_tmp + k) - 1;
      s->d->data[outIdx] = this_d->data[i];
      s->rowidx->data[outIdx] = this_rowidx->data[i];
      outIdx++;
    }

    s->colidx->data[1] = s->colidx->data[0] + b_nd_tmp;
  }
}

void c_sparse_spallocLike(const emlrtStack *sp, int32_T nzmax,
  emxArray_boolean_T *s_d, emxArray_int32_T *s_colidx, emxArray_int32_T
  *s_rowidx)
{
  emlrtStack b_st;
  emlrtStack st;
  int32_T i;
  int32_T numalloc;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &gm_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  b_st.site = &um_emlrtRSI;
  if (nzmax < 0) {
    emlrtErrorWithMessageIdR2018a(&b_st, &y_emlrtRTEI,
      "Coder:toolbox:SparseNegativeSize", "Coder:toolbox:SparseNegativeSize", 0);
  }

  if (nzmax >= MAX_int32_T) {
    emlrtErrorWithMessageIdR2018a(&b_st, &ab_emlrtRTEI,
      "Coder:toolbox:SparseMaxSize", "Coder:toolbox:SparseMaxSize", 2, 12,
      MAX_int32_T);
  }

  if (nzmax >= 1) {
    numalloc = nzmax;
  } else {
    numalloc = 1;
  }

  i = s_d->size[0];
  s_d->size[0] = numalloc;
  emxEnsureCapacity_boolean_T(&st, s_d, i, &me_emlrtRTEI);
  for (i = 0; i < numalloc; i++) {
    s_d->data[i] = false;
  }

  i = s_colidx->size[0];
  s_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(&st, s_colidx, i, &wd_emlrtRTEI);
  i = s_rowidx->size[0];
  s_rowidx->size[0] = numalloc;
  emxEnsureCapacity_int32_T(&st, s_rowidx, i, &me_emlrtRTEI);
  for (i = 0; i < numalloc; i++) {
    s_rowidx->data[i] = 0;
  }

  s_colidx->data[0] = 1;
  s_colidx->data[1] = 1;
}

boolean_T d_sparse_full(const emlrtStack *sp, const emxArray_boolean_T *this_d,
  const emxArray_int32_T *this_colidx)
{
  emlrtStack b_st;
  emlrtStack st;
  int32_T a;
  int32_T cend;
  int32_T idx;
  boolean_T overflow;
  boolean_T y;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  y = false;
  cend = this_colidx->data[1] - 1;
  a = this_colidx->data[0];
  st.site = &fn_emlrtRSI;
  if (this_colidx->data[0] > this_colidx->data[1] - 1) {
    overflow = false;
  } else {
    overflow = (this_colidx->data[1] - 1 > 2147483646);
  }

  if (overflow) {
    b_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  for (idx = a; idx <= cend; idx++) {
    y = this_d->data[idx - 1];
  }

  return y;
}

void d_sparse_parenReference(const emlrtStack *sp, const emxArray_real_T *this_d,
  const emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx,
  int32_T this_m, int32_T this_n, real_T varargin_2, emxArray_real_T *s_d,
  emxArray_int32_T *s_colidx, emxArray_int32_T *s_rowidx)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack st;
  int32_T i;
  int32_T idx;
  boolean_T found;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &bo_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  b_st.site = &co_emlrtRSI;
  c_st.site = &do_emlrtRSI;
  b_sparse_validateNumericIndex(&c_st, this_m, 1.0);
  c_st.site = &eo_emlrtRSI;
  b_sparse_validateNumericIndex(&c_st, this_n, varargin_2);
  c_st.site = &fo_emlrtRSI;
  s_d->size[0] = 0;
  s_rowidx->size[0] = 0;
  i = s_colidx->size[0];
  s_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(&c_st, s_colidx, i, &ne_emlrtRTEI);
  s_colidx->data[0] = 1;
  s_colidx->data[1] = 1;
  b_sparse_locBsearch(this_rowidx, 1, this_colidx->data[(int32_T)varargin_2 - 1],
                      this_colidx->data[(int32_T)varargin_2], &idx, &found);
  if (found) {
    i = s_d->size[0];
    s_d->size[0] = 1;
    emxEnsureCapacity_real_T(&c_st, s_d, i, &ne_emlrtRTEI);
    i = s_rowidx->size[0];
    s_rowidx->size[0] = 1;
    emxEnsureCapacity_int32_T(&c_st, s_rowidx, i, &ne_emlrtRTEI);
    s_d->data[0] = this_d->data[idx - 1];
    s_rowidx->data[0] = 1;
    s_colidx->data[1] = 2;
  }

  if (s_colidx->data[1] - 1 == 0) {
    i = s_rowidx->size[0];
    s_rowidx->size[0] = 1;
    emxEnsureCapacity_int32_T(&c_st, s_rowidx, i, &ne_emlrtRTEI);
    s_rowidx->data[0] = 1;
    i = s_d->size[0];
    s_d->size[0] = 1;
    emxEnsureCapacity_real_T(&c_st, s_d, i, &ne_emlrtRTEI);
    s_d->data[0] = 0.0;
  }
}

void e_sparse_parenReference(const emlrtStack *sp, const emxArray_real_T *this_d,
  const emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx,
  int32_T this_m, int32_T this_n, real_T varargin_1, real_T varargin_2,
  emxArray_real_T *s_d, emxArray_int32_T *s_colidx, emxArray_int32_T *s_rowidx)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack st;
  int32_T i;
  int32_T idx;
  boolean_T found;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &bo_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  b_st.site = &co_emlrtRSI;
  c_st.site = &do_emlrtRSI;
  b_sparse_validateNumericIndex(&c_st, this_m, varargin_1);
  c_st.site = &eo_emlrtRSI;
  b_sparse_validateNumericIndex(&c_st, this_n, varargin_2);
  c_st.site = &fo_emlrtRSI;
  s_d->size[0] = 0;
  s_rowidx->size[0] = 0;
  i = s_colidx->size[0];
  s_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(&c_st, s_colidx, i, &ne_emlrtRTEI);
  s_colidx->data[0] = 1;
  s_colidx->data[1] = 1;
  b_sparse_locBsearch(this_rowidx, (int32_T)varargin_1, this_colidx->data
                      [(int32_T)varargin_2 - 1], this_colidx->data[(int32_T)
                      varargin_2], &idx, &found);
  if (found) {
    i = s_d->size[0];
    s_d->size[0] = 1;
    emxEnsureCapacity_real_T(&c_st, s_d, i, &ne_emlrtRTEI);
    i = s_rowidx->size[0];
    s_rowidx->size[0] = 1;
    emxEnsureCapacity_int32_T(&c_st, s_rowidx, i, &ne_emlrtRTEI);
    s_d->data[0] = this_d->data[idx - 1];
    s_rowidx->data[0] = 1;
    s_colidx->data[1] = 2;
  }

  if (s_colidx->data[1] - 1 == 0) {
    i = s_rowidx->size[0];
    s_rowidx->size[0] = 1;
    emxEnsureCapacity_int32_T(&c_st, s_rowidx, i, &ne_emlrtRTEI);
    s_rowidx->data[0] = 1;
    i = s_d->size[0];
    s_d->size[0] = 1;
    emxEnsureCapacity_real_T(&c_st, s_d, i, &ne_emlrtRTEI);
    s_d->data[0] = 0.0;
  }
}

void f_sparse_parenReference(const emlrtStack *sp, const emxArray_real_T *this_d,
  const emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx,
  int32_T this_m, int32_T this_n, const emxArray_real_T *varargin_1, real_T
  varargin_2, emxArray_real_T *s_d, emxArray_int32_T *s_colidx, emxArray_int32_T
  *s_rowidx, int32_T *s_m)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack st;
  real_T s_d_tmp;
  int32_T colNnz;
  int32_T i;
  int32_T i1;
  int32_T idx;
  int32_T k;
  int32_T ridx;
  int32_T sm;
  boolean_T found;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &bo_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  b_st.site = &co_emlrtRSI;
  c_st.site = &do_emlrtRSI;
  c_sparse_validateNumericIndex(&c_st, this_m, varargin_1);
  c_st.site = &eo_emlrtRSI;
  b_sparse_validateNumericIndex(&c_st, this_n, varargin_2);
  sm = varargin_1->size[1];
  c_st.site = &fo_emlrtRSI;
  s_d->size[0] = 0;
  s_rowidx->size[0] = 0;
  i = s_colidx->size[0];
  s_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(&c_st, s_colidx, i, &ne_emlrtRTEI);
  s_colidx->data[0] = 1;
  colNnz = 1;
  k = 0;
  d_st.site = &ho_emlrtRSI;
  if ((1 <= varargin_1->size[1]) && (varargin_1->size[1] > 2147483646)) {
    e_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&e_st);
  }

  for (ridx = 0; ridx < sm; ridx++) {
    b_sparse_locBsearch(this_rowidx, (int32_T)varargin_1->data[ridx],
                        this_colidx->data[(int32_T)varargin_2 - 1],
                        this_colidx->data[(int32_T)varargin_2], &idx, &found);
    if (found) {
      i = s_d->size[0];
      i1 = s_d->size[0];
      s_d->size[0]++;
      emxEnsureCapacity_real_T(&c_st, s_d, i1, &ke_emlrtRTEI);
      s_d_tmp = this_d->data[idx - 1];
      s_d->data[i] = s_d_tmp;
      i = s_rowidx->size[0];
      i1 = s_rowidx->size[0];
      s_rowidx->size[0]++;
      emxEnsureCapacity_int32_T(&c_st, s_rowidx, i1, &ke_emlrtRTEI);
      s_rowidx->data[i] = ridx + 1;
      s_d->data[k] = s_d_tmp;
      s_rowidx->data[k] = ridx + 1;
      k++;
      colNnz++;
    }
  }

  s_colidx->data[1] = colNnz;
  if (s_colidx->data[1] - 1 == 0) {
    i = s_rowidx->size[0];
    s_rowidx->size[0] = 1;
    emxEnsureCapacity_int32_T(&c_st, s_rowidx, i, &ne_emlrtRTEI);
    s_rowidx->data[0] = 1;
    i = s_d->size[0];
    s_d->size[0] = 1;
    emxEnsureCapacity_real_T(&c_st, s_d, i, &ne_emlrtRTEI);
    s_d->data[0] = 0.0;
  }

  *s_m = varargin_1->size[1];
}

void permuteVector(const emlrtStack *sp, const emxArray_int32_T *idx,
                   emxArray_int32_T *y)
{
  emlrtStack b_st;
  emlrtStack st;
  emxArray_int32_T *t;
  int32_T i;
  int32_T loop_ub;
  int32_T ny;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  emxInit_int32_T(sp, &t, 1, &em_emlrtRTEI, true);
  ny = y->size[0];
  i = t->size[0];
  t->size[0] = y->size[0];
  emxEnsureCapacity_int32_T(sp, t, i, &em_emlrtRTEI);
  loop_ub = y->size[0];
  for (i = 0; i < loop_ub; i++) {
    t->data[i] = y->data[i];
  }

  st.site = &el_emlrtRSI;
  if ((1 <= y->size[0]) && (y->size[0] > 2147483646)) {
    b_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  for (loop_ub = 0; loop_ub < ny; loop_ub++) {
    y->data[loop_ub] = t->data[idx->data[loop_ub] - 1];
  }

  emxFree_int32_T(&t);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

void sparse_and(const emlrtStack *sp, const emxArray_boolean_T *a_d, const
                emxArray_int32_T *a_colidx, const emxArray_int32_T *a_rowidx,
                int32_T a_m, const emxArray_boolean_T *b_d, const
                emxArray_int32_T *b_colidx, const emxArray_int32_T *b_rowidx,
                int32_T b_m, emxArray_boolean_T *s_d, emxArray_int32_T *s_colidx,
                emxArray_int32_T *s_rowidx, int32_T *s_m)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack st;
  int32_T bidx;
  int32_T didx;
  int32_T numalloc;
  boolean_T moreAToDo;
  boolean_T moreBToDo;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &cp_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  if (a_m != b_m) {
    emlrtErrorWithMessageIdR2018a(&st, &eb_emlrtRTEI, "MATLAB:dimagree",
      "MATLAB:dimagree", 0);
  }

  b_st.site = &rm_emlrtRSI;
  numalloc = a_colidx->data[a_colidx->size[0] - 1] - 1;
  didx = b_colidx->data[b_colidx->size[0] - 1] - 1;
  numalloc = muIntScalarMin_sint32(numalloc, didx);
  if (numalloc < 1) {
    numalloc = 1;
  }

  c_st.site = &tm_emlrtRSI;
  b_sparse_spallocLike(&c_st, b_m, numalloc, s_d, s_colidx, s_rowidx, s_m);
  didx = 1;
  s_colidx->data[0] = 1;
  numalloc = a_colidx->data[0];
  bidx = b_colidx->data[0] - 1;
  moreAToDo = (a_colidx->data[0] < a_colidx->data[1]);
  moreBToDo = (b_colidx->data[0] < b_colidx->data[1]);
  while (moreAToDo || moreBToDo) {
    while ((numalloc < a_colidx->data[1]) && ((!moreBToDo) || (a_rowidx->
             data[numalloc - 1] < b_rowidx->data[bidx]))) {
      numalloc++;
    }

    moreAToDo = (numalloc < a_colidx->data[1]);
    while ((bidx + 1 < b_colidx->data[1]) && ((!moreAToDo) || (b_rowidx->
             data[bidx] < a_rowidx->data[numalloc - 1]))) {
      bidx++;
    }

    while ((numalloc < a_colidx->data[1]) && (bidx + 1 < b_colidx->data[1]) &&
           (a_rowidx->data[numalloc - 1] == b_rowidx->data[bidx])) {
      if (a_d->data[numalloc - 1] && b_d->data[bidx]) {
        s_d->data[didx - 1] = true;
        s_rowidx->data[didx - 1] = b_rowidx->data[bidx];
        didx++;
      }

      bidx++;
      numalloc++;
    }

    moreAToDo = (numalloc < a_colidx->data[1]);
    moreBToDo = (bidx + 1 < b_colidx->data[1]);
  }

  s_colidx->data[1] = didx;
}

void sparse_eq(const emlrtStack *sp, const emxArray_real_T *a_d, const
               emxArray_int32_T *a_colidx, const emxArray_int32_T *a_rowidx,
               int32_T a_m, const emxArray_real_T *b_d, const emxArray_int32_T
               *b_colidx, const emxArray_int32_T *b_rowidx, int32_T b_m,
               emxArray_boolean_T *s_d, emxArray_int32_T *s_colidx,
               emxArray_int32_T *s_rowidx, int32_T *s_m)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack st;
  emxArray_boolean_T *S;
  int32_T aidx;
  int32_T bidx;
  int32_T i;
  int32_T k;
  boolean_T moreAToDo;
  boolean_T moreBToDo;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  st.site = &dp_emlrtRSI;
  if (a_m != b_m) {
    emlrtErrorWithMessageIdR2018a(&st, &eb_emlrtRTEI, "MATLAB:dimagree",
      "MATLAB:dimagree", 0);
  }

  emxInit_boolean_T(&st, &S, 1, &le_emlrtRTEI, true);
  i = S->size[0];
  S->size[0] = b_m;
  emxEnsureCapacity_boolean_T(&st, S, i, &le_emlrtRTEI);
  for (i = 0; i < b_m; i++) {
    S->data[i] = true;
  }

  aidx = a_colidx->data[0] - 1;
  bidx = b_colidx->data[0] - 1;
  moreAToDo = (a_colidx->data[0] < a_colidx->data[1]);
  moreBToDo = (b_colidx->data[0] < b_colidx->data[1]);
  while (moreAToDo || moreBToDo) {
    while ((aidx + 1 < a_colidx->data[1]) && ((!moreBToDo) || (a_rowidx->
             data[aidx] < b_rowidx->data[bidx]))) {
      S->data[a_rowidx->data[aidx] - 1] = (a_d->data[aidx] == 0.0);
      aidx++;
    }

    moreAToDo = (aidx + 1 < a_colidx->data[1]);
    while ((bidx + 1 < b_colidx->data[1]) && ((!moreAToDo) || (b_rowidx->
             data[bidx] < a_rowidx->data[aidx]))) {
      S->data[b_rowidx->data[bidx] - 1] = (0.0 == b_d->data[bidx]);
      bidx++;
    }

    while ((aidx + 1 < a_colidx->data[1]) && (bidx + 1 < b_colidx->data[1]) &&
           (a_rowidx->data[aidx] == b_rowidx->data[bidx])) {
      S->data[b_rowidx->data[bidx] - 1] = (a_d->data[aidx] == b_d->data[bidx]);
      bidx++;
      aidx++;
    }

    moreAToDo = (aidx + 1 < a_colidx->data[1]);
    moreBToDo = (bidx + 1 < b_colidx->data[1]);
  }

  b_st.site = &ep_emlrtRSI;
  c_st.site = &gk_emlrtRSI;
  bidx = S->size[0];
  d_st.site = &ao_emlrtRSI;
  if (S->size[0] >= MAX_int32_T) {
    emlrtErrorWithMessageIdR2018a(&d_st, &ab_emlrtRTEI,
      "Coder:toolbox:SparseMaxSize", "Coder:toolbox:SparseMaxSize", 2, 12,
      MAX_int32_T);
  }

  aidx = 0;
  i = S->size[0];
  for (k = 0; k < i; k++) {
    if (S->data[k]) {
      aidx++;
    }
  }

  *s_m = S->size[0];
  if (aidx < 1) {
    aidx = 1;
  }

  i = s_d->size[0];
  s_d->size[0] = aidx;
  emxEnsureCapacity_boolean_T(&c_st, s_d, i, &se_emlrtRTEI);
  for (i = 0; i < aidx; i++) {
    s_d->data[i] = false;
  }

  i = s_colidx->size[0];
  s_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(&c_st, s_colidx, i, &se_emlrtRTEI);
  s_colidx->data[0] = 1;
  i = s_rowidx->size[0];
  s_rowidx->size[0] = aidx;
  emxEnsureCapacity_int32_T(&c_st, s_rowidx, i, &se_emlrtRTEI);
  for (i = 0; i < aidx; i++) {
    s_rowidx->data[i] = 0;
  }

  s_rowidx->data[0] = 1;
  aidx = 0;
  for (k = 0; k < bidx; k++) {
    if (S->data[k]) {
      s_rowidx->data[aidx] = k + 1;
      s_d->data[aidx] = true;
      aidx++;
    }
  }

  emxFree_boolean_T(&S);
  s_colidx->data[1] = aidx + 1;
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

real_T sparse_full(const emlrtStack *sp, const emxArray_real_T *this_d, const
                   emxArray_int32_T *this_colidx)
{
  emlrtStack b_st;
  emlrtStack st;
  real_T y;
  int32_T a;
  int32_T cend;
  int32_T idx;
  boolean_T overflow;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  y = 0.0;
  cend = this_colidx->data[1] - 1;
  a = this_colidx->data[0];
  st.site = &fn_emlrtRSI;
  if (this_colidx->data[0] > this_colidx->data[1] - 1) {
    overflow = false;
  } else {
    overflow = (this_colidx->data[1] - 1 > 2147483646);
  }

  if (overflow) {
    b_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  for (idx = a; idx <= cend; idx++) {
    y = this_d->data[0];
  }

  return y;
}

void sparse_gt(const emlrtStack *sp, const emxArray_real_T *a_d, const
               emxArray_int32_T *a_colidx, const emxArray_int32_T *a_rowidx,
               int32_T a_m, g_sparse *s)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack st;
  emxArray_boolean_T *tmpd;
  int32_T i;
  int32_T loop_ub;
  int32_T nzs_tmp;
  boolean_T overflow;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  st.site = &vo_emlrtRSI;
  b_st.site = &wo_emlrtRSI;
  c_st.site = &xo_emlrtRSI;
  nzs_tmp = a_colidx->data[a_colidx->size[0] - 1];
  if (1 > a_colidx->data[a_colidx->size[0] - 1] - 1) {
    loop_ub = 0;
  } else {
    loop_ub = a_colidx->data[a_colidx->size[0] - 1] - 1;
  }

  emxInit_boolean_T(&c_st, &tmpd, 1, &qe_emlrtRTEI, true);
  i = tmpd->size[0];
  tmpd->size[0] = loop_ub;
  emxEnsureCapacity_boolean_T(&c_st, tmpd, i, &qe_emlrtRTEI);
  for (i = 0; i < loop_ub; i++) {
    tmpd->data[i] = (a_d->data[i] > 0.0);
  }

  if (tmpd->size[0] != a_colidx->data[a_colidx->size[0] - 1] - 1) {
    emlrtErrorWithMessageIdR2018a(&c_st, &kb_emlrtRTEI, "MATLAB:samelen",
      "MATLAB:samelen", 0);
  }

  d_st.site = &yo_emlrtRSI;
  b_sparse_spallocLike(&d_st, a_m, a_colidx->data[a_colidx->size[0] - 1] - 1,
                       s->d, s->colidx, s->rowidx, &s->m);
  if (1 > a_colidx->data[a_colidx->size[0] - 1] - 1) {
    loop_ub = 1;
  } else {
    loop_ub = a_colidx->data[a_colidx->size[0] - 1];
  }

  for (i = 0; i <= loop_ub - 2; i++) {
    s->rowidx->data[i] = a_rowidx->data[i];
  }

  i = s->colidx->size[0];
  s->colidx->size[0] = a_colidx->size[0];
  emxEnsureCapacity_int32_T(&c_st, s->colidx, i, &re_emlrtRTEI);
  loop_ub = a_colidx->size[0];
  for (i = 0; i < loop_ub; i++) {
    s->colidx->data[i] = a_colidx->data[i];
  }

  d_st.site = &ap_emlrtRSI;
  if (1 > a_colidx->data[a_colidx->size[0] - 1] - 1) {
    overflow = false;
  } else {
    overflow = (a_colidx->data[a_colidx->size[0] - 1] - 1 > 2147483646);
  }

  if (overflow) {
    e_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&e_st);
  }

  for (loop_ub = 0; loop_ub <= nzs_tmp - 2; loop_ub++) {
    s->d->data[loop_ub] = tmpd->data[loop_ub];
  }

  emxFree_boolean_T(&tmpd);
  d_st.site = &bp_emlrtRSI;
  c_sparse_fillIn(&d_st, s);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

void sparse_parenAssign(const emlrtStack *sp, e_sparse *this, const
  emxArray_real_T *rhs_d, const emxArray_int32_T *rhs_colidx, const
  emxArray_int32_T *rhs_rowidx, int32_T rhs_m, const emxArray_real_T *varargin_1)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack f_st;
  emlrtStack g_st;
  emlrtStack st;
  emxArray_int32_T *s_colidx;
  emxArray_real_T *s_d;
  real_T b_this[2];
  real_T varargout_1_tmp[2];
  real_T rhsv;
  int32_T highOrderLHS;
  int32_T highOrderRHS;
  int32_T idx;
  int32_T k;
  int32_T lowOrderSize;
  int32_T nidx;
  int32_T overflow;
  boolean_T found;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  f_st.prev = &e_st;
  f_st.tls = e_st.tls;
  g_st.prev = &f_st;
  g_st.tls = f_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  st.site = &wm_emlrtRSI;
  b_bigProduct(this->m, this->n, &lowOrderSize, &overflow);
  varargout_1_tmp[0] = rhs_m;
  varargout_1_tmp[1] = 1.0;
  b_bigProduct(rhs_m, 1, &lowOrderSize, &highOrderRHS);
  b_bigProduct(varargin_1->size[0], 1, &nidx, &highOrderLHS);
  if ((nidx != lowOrderSize) || (highOrderLHS != highOrderRHS)) {
    emlrtErrorWithMessageIdR2018a(&st, &pc_emlrtRTEI,
      "MATLAB:subsassignnumelmismatch", "MATLAB:subsassignnumelmismatch", 0);
  }

  b_st.site = &xm_emlrtRSI;
  if (overflow == 0) {
    c_st.site = &ym_emlrtRSI;
    sparse_validateNumericIndex(&c_st, this->m * this->n, varargin_1);
  } else {
    c_st.site = &an_emlrtRSI;
    sparse_validateNumericIndex(&c_st, MAX_int32_T, varargin_1);
  }

  nidx = varargin_1->size[0];
  c_st.site = &bn_emlrtRSI;
  if ((1 <= varargin_1->size[0]) && (varargin_1->size[0] > 2147483646)) {
    d_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&d_st);
  }

  emxInit_real_T(&b_st, &s_d, 1, &fm_emlrtRTEI, true);
  emxInit_int32_T(&b_st, &s_colidx, 1, &fm_emlrtRTEI, true);
  for (k = 0; k < nidx; k++) {
    c_st.site = &cn_emlrtRSI;
    b_this[0] = this->m;
    b_this[1] = this->n;
    d_st.site = &jm_emlrtRSI;
    b_ind2sub_indexClass(&d_st, b_this, varargin_1->data[k], &highOrderLHS,
                         &overflow);
    c_st.site = &dn_emlrtRSI;
    d_st.site = &wl_emlrtRSI;
    e_st.site = &xl_emlrtRSI;
    if ((rhs_m & 65535) > MAX_int32_T - ((rhs_m >> 16) << 16)) {
      f_st.site = &am_emlrtRSI;
    } else {
      f_st.site = &yl_emlrtRSI;
      if (k + 1 > rhs_m) {
        emlrtErrorWithMessageIdR2018a(&f_st, &cb_emlrtRTEI,
          "Coder:builtins:IndexOutOfBounds", "Coder:builtins:IndexOutOfBounds",
          6, 12, k + 1, 12, 1, 12, rhs_m);
      }
    }

    f_st.site = &cm_emlrtRSI;
    s_d->size[0] = 0;
    lowOrderSize = s_colidx->size[0];
    s_colidx->size[0] = 2;
    emxEnsureCapacity_int32_T(&e_st, s_colidx, lowOrderSize, &je_emlrtRTEI);
    s_colidx->data[0] = 1;
    s_colidx->data[1] = 1;
    f_st.site = &em_emlrtRSI;
    g_st.site = &jm_emlrtRSI;
    ind2sub_indexClass(&g_st, varargout_1_tmp, k + 1, &lowOrderSize,
                       &highOrderRHS);
    sparse_locBsearch(rhs_rowidx, lowOrderSize, rhs_colidx->data[highOrderRHS -
                      1], rhs_colidx->data[highOrderRHS], &idx, &found);
    if (found) {
      lowOrderSize = s_d->size[0];
      s_d->size[0] = 1;
      emxEnsureCapacity_real_T(&e_st, s_d, lowOrderSize, &je_emlrtRTEI);
      s_d->data[0] = rhs_d->data[idx - 1];
      s_colidx->data[1] = 2;
    }

    if (s_d->size[0] == 0) {
      lowOrderSize = s_d->size[0];
      s_d->size[0] = 1;
      emxEnsureCapacity_real_T(&e_st, s_d, lowOrderSize, &je_emlrtRTEI);
      s_d->data[0] = 0.0;
    }

    c_st.site = &dn_emlrtRSI;
    rhsv = sparse_full(&c_st, s_d, s_colidx);
    c_st.site = &en_emlrtRSI;
    sparse_parenAssign2D(&c_st, this, rhsv, highOrderLHS, overflow);
  }

  emxFree_int32_T(&s_colidx);
  emxFree_real_T(&s_d);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

void sparse_parenReference(const emlrtStack *sp, const emxArray_real_T *this_d,
  const emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx,
  int32_T this_m, int32_T this_n, const emxArray_real_T *varargin_1, f_sparse *s)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack st;
  emxArray_real_T *b_varargin_1;
  real_T c_varargin_1[2];
  real_T varargout_1_tmp[2];
  int32_T colNnz;
  int32_T idx;
  int32_T k;
  int32_T nrow;
  int32_T overflow;
  int32_T ridx;
  int32_T unusedU0;
  boolean_T found;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  st.site = &wl_emlrtRSI;
  b_st.site = &xl_emlrtRSI;
  bigProduct(this_m, this_n, &unusedU0, &overflow);
  emxInit_real_T(&b_st, &b_varargin_1, 1, &ie_emlrtRTEI, true);
  if (overflow == 0) {
    unusedU0 = varargin_1->size[0];
    overflow = b_varargin_1->size[0];
    b_varargin_1->size[0] = varargin_1->size[0];
    emxEnsureCapacity_real_T(&b_st, b_varargin_1, overflow, &ie_emlrtRTEI);
    for (overflow = 0; overflow < unusedU0; overflow++) {
      b_varargin_1->data[overflow] = varargin_1->data[overflow];
    }

    c_st.site = &yl_emlrtRSI;
    sparse_validateNumericIndex(&c_st, this_m * this_n, b_varargin_1);
  } else {
    unusedU0 = varargin_1->size[0];
    overflow = b_varargin_1->size[0];
    b_varargin_1->size[0] = varargin_1->size[0];
    emxEnsureCapacity_real_T(&b_st, b_varargin_1, overflow, &ie_emlrtRTEI);
    for (overflow = 0; overflow < unusedU0; overflow++) {
      b_varargin_1->data[overflow] = varargin_1->data[overflow];
    }

    c_st.site = &am_emlrtRSI;
    sparse_validateNumericIndex(&c_st, MAX_int32_T, b_varargin_1);
  }

  emxFree_real_T(&b_varargin_1);
  nrow = varargin_1->size[0];
  varargout_1_tmp[0] = this_m;
  varargout_1_tmp[1] = this_n;
  c_varargin_1[0] = varargin_1->size[0];
  c_varargin_1[1] = 1.0;
  c_st.site = &bm_emlrtRSI;
  b_indexShapeCheck(&c_st, varargout_1_tmp, c_varargin_1);
  c_st.site = &cm_emlrtRSI;
  d_st.site = &gm_emlrtRSI;
  e_st.site = &hm_emlrtRSI;
  if (varargin_1->size[0] < 0) {
    emlrtErrorWithMessageIdR2018a(&e_st, &y_emlrtRTEI,
      "Coder:toolbox:SparseNegativeSize", "Coder:toolbox:SparseNegativeSize", 0);
  }

  if (varargin_1->size[0] >= MAX_int32_T) {
    emlrtErrorWithMessageIdR2018a(&e_st, &ab_emlrtRTEI,
      "Coder:toolbox:SparseMaxSize", "Coder:toolbox:SparseMaxSize", 2, 12,
      MAX_int32_T);
  }

  s->m = varargin_1->size[0];
  overflow = s->d->size[0];
  s->d->size[0] = 1;
  emxEnsureCapacity_real_T(&d_st, s->d, overflow, &je_emlrtRTEI);
  s->d->data[0] = 0.0;
  overflow = s->colidx->size[0];
  s->colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(&d_st, s->colidx, overflow, &wd_emlrtRTEI);
  s->colidx->data[0] = 1;
  overflow = s->rowidx->size[0];
  s->rowidx->size[0] = 1;
  emxEnsureCapacity_int32_T(&d_st, s->rowidx, overflow, &je_emlrtRTEI);
  s->rowidx->data[0] = 0;
  s->colidx->data[1] = 1;
  e_st.site = &ok_emlrtRSI;
  b_sparse_fillIn(&e_st, s);
  if ((0 > varargin_1->size[0]) && (varargin_1->size[0] != 0)) {
    emlrtErrorWithMessageIdR2018a(&b_st, &x_emlrtRTEI,
      "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
  }

  s->d->size[0] = 0;
  s->rowidx->size[0] = 0;
  overflow = s->colidx->size[0];
  s->colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(&b_st, s->colidx, overflow, &je_emlrtRTEI);
  k = 0;
  s->colidx->data[0] = 1;
  colNnz = 1;
  c_st.site = &dm_emlrtRSI;
  if ((1 <= varargin_1->size[0]) && (varargin_1->size[0] > 2147483646)) {
    d_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&d_st);
  }

  for (ridx = 0; ridx < nrow; ridx++) {
    c_st.site = &em_emlrtRSI;
    d_st.site = &jm_emlrtRSI;
    ind2sub_indexClass(&d_st, varargout_1_tmp, (int32_T)varargin_1->data[k],
                       &unusedU0, &overflow);
    sparse_locBsearch(this_rowidx, unusedU0, this_colidx->data[overflow - 1],
                      this_colidx->data[overflow], &idx, &found);
    if (found) {
      overflow = s->d->size[0];
      unusedU0 = s->d->size[0];
      s->d->size[0]++;
      emxEnsureCapacity_real_T(&b_st, s->d, unusedU0, &ke_emlrtRTEI);
      s->d->data[overflow] = this_d->data[idx - 1];
      overflow = s->rowidx->size[0];
      unusedU0 = s->rowidx->size[0];
      s->rowidx->size[0]++;
      emxEnsureCapacity_int32_T(&b_st, s->rowidx, unusedU0, &ke_emlrtRTEI);
      s->rowidx->data[overflow] = ridx + 1;
      colNnz++;
    }

    k++;
  }

  s->colidx->data[1] = colNnz;
  if (s->d->size[0] == 0) {
    overflow = s->d->size[0];
    s->d->size[0] = 1;
    emxEnsureCapacity_real_T(&b_st, s->d, overflow, &je_emlrtRTEI);
    s->d->data[0] = 0.0;
    overflow = s->rowidx->size[0];
    s->rowidx->size[0] = 1;
    emxEnsureCapacity_int32_T(&b_st, s->rowidx, overflow, &je_emlrtRTEI);
    s->rowidx->data[0] = 0;
  }

  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

void sparse_spallocLike(const emlrtStack *sp, int32_T m, int32_T nzmax, f_sparse
  *s)
{
  emlrtStack b_st;
  emlrtStack st;
  int32_T i;
  int32_T numalloc;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &gm_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  b_st.site = &hm_emlrtRSI;
  if (m < 0) {
    emlrtErrorWithMessageIdR2018a(&b_st, &y_emlrtRTEI,
      "Coder:toolbox:SparseNegativeSize", "Coder:toolbox:SparseNegativeSize", 0);
  }

  if (m >= MAX_int32_T) {
    emlrtErrorWithMessageIdR2018a(&b_st, &ab_emlrtRTEI,
      "Coder:toolbox:SparseMaxSize", "Coder:toolbox:SparseMaxSize", 2, 12,
      MAX_int32_T);
  }

  s->m = m;
  b_st.site = &um_emlrtRSI;
  if (nzmax < 0) {
    emlrtErrorWithMessageIdR2018a(&b_st, &y_emlrtRTEI,
      "Coder:toolbox:SparseNegativeSize", "Coder:toolbox:SparseNegativeSize", 0);
  }

  if (nzmax >= MAX_int32_T) {
    emlrtErrorWithMessageIdR2018a(&b_st, &ab_emlrtRTEI,
      "Coder:toolbox:SparseMaxSize", "Coder:toolbox:SparseMaxSize", 2, 12,
      MAX_int32_T);
  }

  if (nzmax >= 1) {
    numalloc = nzmax;
  } else {
    numalloc = 1;
  }

  i = s->d->size[0];
  s->d->size[0] = numalloc;
  emxEnsureCapacity_real_T(&st, s->d, i, &me_emlrtRTEI);
  for (i = 0; i < numalloc; i++) {
    s->d->data[i] = 0.0;
  }

  i = s->colidx->size[0];
  s->colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(&st, s->colidx, i, &wd_emlrtRTEI);
  s->colidx->data[0] = 1;
  i = s->rowidx->size[0];
  s->rowidx->size[0] = numalloc;
  emxEnsureCapacity_int32_T(&st, s->rowidx, i, &me_emlrtRTEI);
  for (i = 0; i < numalloc; i++) {
    s->rowidx->data[i] = 0;
  }

  s->colidx->data[1] = 1;
  b_st.site = &ok_emlrtRSI;
  b_sparse_fillIn(&b_st, s);
}

void sparse_times(const emlrtStack *sp, const emxArray_real_T *a, const
                  emxArray_real_T *b_d, const emxArray_int32_T *b_colidx, const
                  emxArray_int32_T *b_rowidx, int32_T b_m, emxArray_real_T *s_d,
                  emxArray_int32_T *s_colidx, emxArray_int32_T *s_rowidx,
                  int32_T *s_m)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack st;
  f_sparse expl_temp;
  real_T val;
  int32_T idx;
  int32_T n;
  int32_T numalloc;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  st.site = &qm_emlrtRSI;
  if (a->size[0] != b_m) {
    emlrtErrorWithMessageIdR2018a(&st, &eb_emlrtRTEI, "MATLAB:dimagree",
      "MATLAB:dimagree", 0);
  }

  b_st.site = &rm_emlrtRSI;
  n = 0;
  idx = a->size[0];
  for (numalloc = 0; numalloc < idx; numalloc++) {
    if (a->data[numalloc] != 0.0) {
      n++;
    }
  }

  if (n > -(b_colidx->data[b_colidx->size[0] - 1] + MIN_int32_T)) {
    bigProduct(1, b_m, &numalloc, &idx);
    if (idx != 0) {
      emlrtErrorWithMessageIdR2018a(&b_st, &fb_emlrtRTEI,
        "Coder:toolbox:SparseFuncAlmostFull",
        "Coder:toolbox:SparseFuncAlmostFull", 0);
    }
  }

  numalloc = (n + b_colidx->data[b_colidx->size[0] - 1]) - 1;
  numalloc = muIntScalarMin_sint32(numalloc, b_m);
  if (numalloc < 1) {
    numalloc = 1;
  }

  emxInitStruct_sparse(&b_st, &expl_temp, &le_emlrtRTEI, true);
  c_st.site = &tm_emlrtRSI;
  sparse_spallocLike(&c_st, b_m, numalloc, &expl_temp);
  idx = s_d->size[0];
  s_d->size[0] = expl_temp.d->size[0];
  emxEnsureCapacity_real_T(&b_st, s_d, idx, &le_emlrtRTEI);
  numalloc = expl_temp.d->size[0];
  for (idx = 0; idx < numalloc; idx++) {
    s_d->data[idx] = expl_temp.d->data[idx];
  }

  idx = s_colidx->size[0];
  s_colidx->size[0] = expl_temp.colidx->size[0];
  emxEnsureCapacity_int32_T(&b_st, s_colidx, idx, &le_emlrtRTEI);
  numalloc = expl_temp.colidx->size[0];
  for (idx = 0; idx < numalloc; idx++) {
    s_colidx->data[idx] = expl_temp.colidx->data[idx];
  }

  idx = s_rowidx->size[0];
  s_rowidx->size[0] = expl_temp.rowidx->size[0];
  emxEnsureCapacity_int32_T(&b_st, s_rowidx, idx, &le_emlrtRTEI);
  numalloc = expl_temp.rowidx->size[0];
  for (idx = 0; idx < numalloc; idx++) {
    s_rowidx->data[idx] = expl_temp.rowidx->data[idx];
  }

  *s_m = expl_temp.m;
  b_st.site = &sm_emlrtRSI;
  s_colidx->data[0] = 1;
  numalloc = 1;
  idx = b_colidx->data[0];
  c_st.site = &vm_emlrtRSI;
  if ((1 <= expl_temp.m) && (expl_temp.m > 2147483646)) {
    d_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&d_st);
  }

  emxFreeStruct_sparse(&expl_temp);
  for (n = 0; n < *s_m; n++) {
    if ((idx < b_colidx->data[1]) && (n + 1 == b_rowidx->data[idx - 1])) {
      val = a->data[n] * b_d->data[idx - 1];
      idx++;
    } else {
      val = 0.0;
    }

    if (val != 0.0) {
      s_d->data[numalloc - 1] = val;
      s_rowidx->data[numalloc - 1] = n + 1;
      numalloc++;
    }
  }

  s_colidx->data[1] = numalloc;
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (sparse1.c) */
