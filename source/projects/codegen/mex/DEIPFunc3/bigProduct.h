/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * bigProduct.h
 *
 * Code generation for function 'bigProduct'
 *
 */

#pragma once

/* Include files */
#include "rtwtypes.h"
#include "emlrt.h"
#include "mex.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Function Declarations */
void b_bigProduct(int32_T a, int32_T b, int32_T *loworderbits, int32_T
                  *highorderbits);
void bigProduct(int32_T a, int32_T b, int32_T *loworderbits, int32_T
                *highorderbits);

/* End of code generation (bigProduct.h) */
