/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * ismember.c
 *
 * Code generation for function 'ismember'
 *
 */

/* Include files */
#include "ismember.h"
#include "DEIPFunc3_data.h"
#include "DEIPFunc3_emxutil.h"
#include "DEIPFunc3_types.h"
#include "eml_int_forloop_overflow_check.h"
#include "issorted.h"
#include "rt_nonfinite.h"
#include "sort.h"
#include "mwmathutil.h"
#include <math.h>

/* Variable Definitions */
static emlrtRSInfo og_emlrtRSI = { 152,/* lineNo */
  "local_ismember",                    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\ismember.m"/* pathName */
};

static emlrtRSInfo pg_emlrtRSI = { 151,/* lineNo */
  "local_ismember",                    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\ismember.m"/* pathName */
};

static emlrtRSInfo ui_emlrtRSI = { 150,/* lineNo */
  "local_ismember",                    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\ismember.m"/* pathName */
};

static emlrtRSInfo vi_emlrtRSI = { 161,/* lineNo */
  "local_ismember",                    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\ismember.m"/* pathName */
};

static emlrtRSInfo wi_emlrtRSI = { 168,/* lineNo */
  "local_ismember",                    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\ismember.m"/* pathName */
};

static emlrtRSInfo xi_emlrtRSI = { 171,/* lineNo */
  "local_ismember",                    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\ismember.m"/* pathName */
};

static emlrtRSInfo yi_emlrtRSI = { 190,/* lineNo */
  "local_ismember",                    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\ismember.m"/* pathName */
};

static emlrtRTEInfo kd_emlrtRTEI = { 111,/* lineNo */
  5,                                   /* colNo */
  "ismember",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\ismember.m"/* pName */
};

static emlrtRTEInfo md_emlrtRTEI = { 43,/* lineNo */
  21,                                  /* colNo */
  "ismember",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\ops\\ismember.m"/* pName */
};

/* Function Declarations */
static int32_T bsearchni(int32_T k, const emxArray_real_T *x, const
  emxArray_real_T *s);

/* Function Definitions */
static int32_T bsearchni(int32_T k, const emxArray_real_T *x, const
  emxArray_real_T *s)
{
  real_T b_x;
  int32_T idx;
  int32_T ihi;
  int32_T ilo;
  int32_T imid;
  boolean_T exitg1;
  boolean_T p;
  b_x = x->data[k - 1];
  ihi = s->size[0];
  idx = 0;
  ilo = 1;
  exitg1 = false;
  while ((!exitg1) && (ihi >= ilo)) {
    imid = ((ilo >> 1) + (ihi >> 1)) - 1;
    if (((ilo & 1) == 1) && ((ihi & 1) == 1)) {
      imid++;
    }

    if (b_x == s->data[imid]) {
      idx = imid + 1;
      exitg1 = true;
    } else {
      if (muDoubleScalarIsNaN(s->data[imid])) {
        p = !muDoubleScalarIsNaN(b_x);
      } else if (muDoubleScalarIsNaN(b_x)) {
        p = false;
      } else {
        p = (b_x < s->data[imid]);
      }

      if (p) {
        ihi = imid;
      } else {
        ilo = imid + 2;
      }
    }
  }

  if (idx > 0) {
    idx--;
    while ((idx > 0) && (b_x == s->data[idx - 1])) {
      idx--;
    }

    idx++;
  }

  return idx;
}

void b_local_ismember(const emlrtStack *sp, const emxArray_real_T *a, const
                      emxArray_real_T *s, emxArray_boolean_T *tf)
{
  emlrtStack b_st;
  emlrtStack st;
  emxArray_int32_T *dr_emlrtRSI;
  emxArray_real_T *ss;
  real_T absx;
  int32_T exponent;
  int32_T na;
  int32_T ns;
  int32_T p;
  int32_T pmax;
  int32_T pmin;
  int32_T pow2p;
  boolean_T b_p;
  boolean_T exitg1;
  boolean_T guard1 = false;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  na = a->size[0] - 1;
  ns = s->size[0];
  pmin = tf->size[0];
  tf->size[0] = a->size[0];
  emxEnsureCapacity_boolean_T(sp, tf, pmin, &kd_emlrtRTEI);
  pmax = a->size[0];
  for (pmin = 0; pmin < pmax; pmin++) {
    tf->data[pmin] = false;
  }

  emxInit_real_T(sp, &ss, 1, &md_emlrtRTEI, true);
  emxInit_int32_T(sp, &dr_emlrtRSI, 1, &md_emlrtRTEI, true);
  guard1 = false;
  if (s->size[0] <= 4) {
    guard1 = true;
  } else {
    pmax = 31;
    pmin = 0;
    exitg1 = false;
    while ((!exitg1) && (pmax - pmin > 1)) {
      p = (pmin + pmax) >> 1;
      pow2p = 1 << p;
      if (pow2p == ns) {
        pmax = p;
        exitg1 = true;
      } else if (pow2p > ns) {
        pmax = p;
      } else {
        pmin = p;
      }
    }

    if (a->size[0] <= pmax + 4) {
      guard1 = true;
    } else {
      st.site = &vi_emlrtRSI;
      if (!issorted(&st, s)) {
        st.site = &wi_emlrtRSI;
        pmin = ss->size[0];
        ss->size[0] = s->size[0];
        emxEnsureCapacity_real_T(&st, ss, pmin, &ld_emlrtRTEI);
        pmax = s->size[0];
        for (pmin = 0; pmin < pmax; pmin++) {
          ss->data[pmin] = s->data[pmin];
        }

        b_st.site = &tg_emlrtRSI;
        sort(&b_st, ss, dr_emlrtRSI);
        st.site = &xi_emlrtRSI;
        if ((1 <= a->size[0]) && (a->size[0] > 2147483646)) {
          b_st.site = &fg_emlrtRSI;
          check_forloop_overflow_error(&b_st);
        }

        for (pmax = 0; pmax <= na; pmax++) {
          if (bsearchni(pmax + 1, a, ss) > 0) {
            tf->data[pmax] = true;
          }
        }
      } else {
        st.site = &yi_emlrtRSI;
        if ((1 <= a->size[0]) && (a->size[0] > 2147483646)) {
          b_st.site = &fg_emlrtRSI;
          check_forloop_overflow_error(&b_st);
        }

        for (pmax = 0; pmax <= na; pmax++) {
          if (bsearchni(pmax + 1, a, s) > 0) {
            tf->data[pmax] = true;
          }
        }
      }
    }
  }

  if (guard1) {
    st.site = &ui_emlrtRSI;
    if ((1 <= a->size[0]) && (a->size[0] > 2147483646)) {
      b_st.site = &fg_emlrtRSI;
      check_forloop_overflow_error(&b_st);
    }

    for (pmin = 0; pmin <= na; pmin++) {
      st.site = &pg_emlrtRSI;
      if ((1 <= ns) && (ns > 2147483646)) {
        b_st.site = &fg_emlrtRSI;
        check_forloop_overflow_error(&b_st);
      }

      pmax = 0;
      exitg1 = false;
      while ((!exitg1) && (pmax <= ns - 1)) {
        st.site = &og_emlrtRSI;
        absx = muDoubleScalarAbs(s->data[pmax] / 2.0);
        if ((!muDoubleScalarIsInf(absx)) && (!muDoubleScalarIsNaN(absx))) {
          if (absx <= 2.2250738585072014E-308) {
            absx = 4.94065645841247E-324;
          } else {
            frexp(absx, &exponent);
            absx = ldexp(1.0, exponent - 53);
          }
        } else {
          absx = rtNaN;
        }

        if ((muDoubleScalarAbs(s->data[pmax] - a->data[pmin]) < absx) ||
            (muDoubleScalarIsInf(a->data[pmin]) && muDoubleScalarIsInf(s->
              data[pmax]) && ((a->data[pmin] > 0.0) == (s->data[pmax] > 0.0))))
        {
          b_p = true;
        } else {
          b_p = false;
        }

        if (b_p) {
          tf->data[pmin] = true;
          exitg1 = true;
        } else {
          pmax++;
        }
      }
    }
  }

  emxFree_int32_T(&dr_emlrtRSI);
  emxFree_real_T(&ss);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

boolean_T local_ismember(real_T a, const real_T s_data[], const int32_T s_size[1])
{
  real_T absx;
  int32_T exponent;
  int32_T k;
  boolean_T exitg1;
  boolean_T p;
  boolean_T tf;
  tf = false;
  k = 0;
  exitg1 = false;
  while ((!exitg1) && (k <= s_size[0] - 1)) {
    absx = muDoubleScalarAbs(s_data[k] / 2.0);
    if ((!muDoubleScalarIsInf(absx)) && (!muDoubleScalarIsNaN(absx))) {
      if (absx <= 2.2250738585072014E-308) {
        absx = 4.94065645841247E-324;
      } else {
        frexp(absx, &exponent);
        absx = ldexp(1.0, exponent - 53);
      }
    } else {
      absx = rtNaN;
    }

    if ((muDoubleScalarAbs(s_data[k] - a) < absx) || (muDoubleScalarIsInf(a) &&
         muDoubleScalarIsInf(s_data[k]) && ((a > 0.0) == (s_data[k] > 0.0)))) {
      p = true;
    } else {
      p = false;
    }

    if (p) {
      tf = true;
      exitg1 = true;
    } else {
      k++;
    }
  }

  return tf;
}

/* End of code generation (ismember.c) */
