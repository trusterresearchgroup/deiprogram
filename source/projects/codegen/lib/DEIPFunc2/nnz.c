/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * nnz.c
 *
 * Code generation for function 'nnz'
 *
 */

/* Include files */
#include "nnz.h"
#include "rt_nonfinite.h"

/* Function Definitions */
int intnnz(const double s_data[], const int s_size[2])
{
  int i;
  int k;
  int n;
  n = 0;
  i = s_size[1];
  for (k = 0; k < i; k++) {
    if (s_data[k] != 0.0) {
      n++;
    }
  }

  return n;
}

/* End of code generation (nnz.c) */
