/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * nchoosek.c
 *
 * Code generation for function 'nchoosek'
 *
 */

/* Include files */
#include "nchoosek.h"
#include "DEIPFunc3_data.h"
#include "DEIPFunc3_emxutil.h"
#include "DEIPFunc3_types.h"
#include "eml_int_forloop_overflow_check.h"
#include "rt_nonfinite.h"
#include "warning.h"
#include "mwmathutil.h"

/* Variable Definitions */
static emlrtRSInfo ej_emlrtRSI = { 58, /* lineNo */
  "nchoosek",                          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\specfun\\nchoosek.m"/* pathName */
};

static emlrtRSInfo fj_emlrtRSI = { 53, /* lineNo */
  "nchoosek",                          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\specfun\\nchoosek.m"/* pathName */
};

static emlrtRSInfo gj_emlrtRSI = { 116,/* lineNo */
  "nCk",                               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\specfun\\nchoosek.m"/* pathName */
};

static emlrtDCInfo emlrtDCI = { 54,    /* lineNo */
  62,                                  /* colNo */
  "nchoosek",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\specfun\\nchoosek.m",/* pName */
  4                                    /* checkKind */
};

static emlrtRTEInfo nd_emlrtRTEI = { 54,/* lineNo */
  5,                                   /* colNo */
  "nchoosek",                          /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\specfun\\nchoosek.m"/* pName */
};

/* Function Definitions */
void nchoosek(const emlrtStack *sp, const real_T x_data[], const int32_T x_size
              [2], emxArray_real_T *y)
{
  emlrtStack b_st;
  emlrtStack st;
  real_T b_y;
  int32_T comb[2];
  int32_T combj;
  int32_T icomb;
  int32_T j;
  int32_T nmk;
  int32_T nrows;
  int32_T row;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &fj_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  icomb = 1;
  if (2.0 > (real_T)x_size[1] / 2.0) {
    icomb = x_size[1] - 3;
  }

  b_y = 1.0;
  nmk = x_size[1] - icomb;
  for (j = 0; j <= icomb; j++) {
    b_y *= (real_T)(j + nmk) / ((real_T)j + 1.0);
  }

  b_y = muDoubleScalarRound(b_y);
  if (!(b_y <= 9.007199254740992E+15)) {
    b_st.site = &gj_emlrtRSI;
    warning(&b_st);
  }

  nrows = (int32_T)b_y;
  if ((int32_T)b_y < 0) {
    emlrtNonNegativeCheckR2012b((int32_T)b_y, &emlrtDCI, sp);
  }

  icomb = y->size[0] * y->size[1];
  y->size[0] = (int32_T)b_y;
  y->size[1] = 2;
  emxEnsureCapacity_real_T(sp, y, icomb, &nd_emlrtRTEI);
  comb[0] = 1;
  comb[1] = 2;
  icomb = 1;
  nmk = x_size[1];
  st.site = &ej_emlrtRSI;
  if ((1 <= (int32_T)b_y) && ((int32_T)b_y > 2147483646)) {
    b_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  for (row = 0; row < nrows; row++) {
    y->data[row] = x_data[comb[0] - 1];
    y->data[row + y->size[0]] = x_data[comb[1] - 1];
    if (icomb + 1 > 0) {
      j = comb[icomb];
      combj = comb[icomb] + 1;
      comb[icomb]++;
      if (j + 1 < nmk) {
        icomb += 2;
        for (j = icomb; j < 3; j++) {
          combj++;
          comb[1] = combj;
        }

        icomb = 1;
        nmk = x_size[1];
      } else {
        icomb--;
        nmk--;
      }
    }
  }
}

/* End of code generation (nchoosek.c) */
