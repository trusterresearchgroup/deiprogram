% Example demonstrating applying tractions to model
% Domain: 2x2x2 cube, HEX8 elements
% Loading: Pressure of 10 units in x direction, shear of 20 in yz direction.
%
% Last revision: 12/02/2019 TJT

%% Generate mesh
clear
clc
nen = 8;

% Block 1
xl = [1 0 0 0
      2 2 0 0
      4 0 2 0
      3 2 2 0
      5 0 0 2
      6 2 0 2
      8 0 2 2
      7 2 2 2];
[Coordinates,NodesOnElement,RegionOnElement,numnp,numel] = block3d('cart',2,2,2,1,1,1,10,xl,nen);
Coordinates = Coordinates';
NodesOnElement = NodesOnElement';

%% Boundary conditions; minimal for restraining RBM
NodeBC = [14 1 0
          14 2 0
          14 3 0
          15 3 0
          15 2 0
          17 3 0];
numBC = 6;

%% Surface tractions representing pressures; determine face numbers via 
% colors from plotMesh3
SurfacesL = [zeros(4,2) (1:2:7)' ones(4,1)*1 ones(4,1)*[ 10 0 0]  % minus x
             zeros(4,2) (2:2:8)' ones(4,1)*2 ones(4,1)*[-10 0 0]  % plus  x
             zeros(4,2) (1:1:4)' ones(4,1)*5 ones(4,1)*[0 -20 0]  % minus z
             zeros(4,2) (5:1:8)' ones(4,1)*6 ones(4,1)*[0  20 0]  % plus  z
             zeros(2,2) (1:1:2)' ones(2,1)*3 ones(2,1)*[0 0 -20]  % minus y
             zeros(2,2) (5:1:6)' ones(2,1)*3 ones(2,1)*[0 0 -20]  % minus y
             zeros(2,2) (3:1:4)' ones(2,1)*4 ones(2,1)*[0 0  20]  % plus  y
             zeros(2,2) (7:1:8)' ones(2,1)*4 ones(2,1)*[0 0  20]];% plus  y
numSL = size(SurfacesL,1);

%% Material properties
nummat = 1;
MatTypeTable = [1
                1];
MateT = [100e3 0.25 1];
% Output quantity flags
DHist = 1;
FHist = 1;
SHist = 1;
SEHist = 1;

%% Algorithm definitions for FEA_Program
ProbType = [numnp numel nummat 3 3 nen];


% Plot the regions in the mesh
plotMesh3(Coordinates,NodesOnElement,1,(1:numel),(1:numnp),[1 1 1 1 1])
view(3)
plotMesh3(Coordinates,NodesOnElement,4,(1:numel),(1:numnp),[1 0 1 0 1])
plotBC(Coordinates, NodeBC, 4)
view(3)

% % Plot the regions in the mesh
% plotElemCont3(Coordinates,StreListE(1,:)',NodesOnElement,2,(1:numel),(1:numnp),[1 0 0],{'title'},{'String','Stress s_x_x'})
% plotElemCont3(Coordinates,StreListE(5,:)',NodesOnElement,3,(1:numel),(1:numnp),[1 0 0],{'title'},{'String','Stress s_y_z'})