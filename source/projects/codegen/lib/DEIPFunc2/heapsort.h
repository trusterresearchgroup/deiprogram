/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * heapsort.h
 *
 * Code generation for function 'heapsort'
 *
 */

#ifndef HEAPSORT_H
#define HEAPSORT_H

/* Include files */
#include "DEIPFunc2_types.h"
#include "rtwtypes.h"
#include <stddef.h>
#include <stdlib.h>
#ifdef __cplusplus

extern "C" {

#endif

  /* Function Declarations */
  void b_heapsort(emxArray_int32_T *x, int xstart, int xend, const cell_wrap_4
                  cmp_tunableEnvironment[2]);
  void c_heapsort(emxArray_int32_T *x, int xstart, int xend, const cell_wrap_4
                  cmp_tunableEnvironment[1]);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (heapsort.h) */
