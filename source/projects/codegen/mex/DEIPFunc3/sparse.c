/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sparse.c
 *
 * Code generation for function 'sparse'
 *
 */

/* Include files */
#include "sparse.h"
#include "DEIPFunc3_data.h"
#include "DEIPFunc3_emxutil.h"
#include "DEIPFunc3_types.h"
#include "eml_int_forloop_overflow_check.h"
#include "fillIn.h"
#include "introsort.h"
#include "rt_nonfinite.h"
#include "sparse1.h"

/* Variable Definitions */
static emlrtRSInfo hk_emlrtRSI = { 121,/* lineNo */
  "sparse/sparse",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo ik_emlrtRSI = { 122,/* lineNo */
  "sparse/sparse",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo jk_emlrtRSI = { 128,/* lineNo */
  "sparse/sparse",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo kk_emlrtRSI = { 140,/* lineNo */
  "sparse/sparse",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo lk_emlrtRSI = { 166,/* lineNo */
  "sparse/sparse",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo mk_emlrtRSI = { 199,/* lineNo */
  "sparse/sparse",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo nk_emlrtRSI = { 211,/* lineNo */
  "sparse/sparse",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo qk_emlrtRSI = { 1634,/* lineNo */
  "locSortrows",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo rk_emlrtRSI = { 1636,/* lineNo */
  "locSortrows",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo sk_emlrtRSI = { 1637,/* lineNo */
  "locSortrows",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo fl_emlrtRSI = { 322,/* lineNo */
  "unaryMinOrMaxDispatch",             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pathName */
};

static emlrtRSInfo gl_emlrtRSI = { 363,/* lineNo */
  "minOrMax1D",                        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\eml\\+coder\\+internal\\unaryMinOrMax.m"/* pathName */
};

static emlrtRSInfo wn_emlrtRSI = { 68, /* lineNo */
  "sparse/sparse",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo xn_emlrtRSI = { 67, /* lineNo */
  "sparse/sparse",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRSInfo yn_emlrtRSI = { 52, /* lineNo */
  "sparse/sparse",                     /* fcnName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pathName */
};

static emlrtRTEInfo p_emlrtRTEI = { 113,/* lineNo */
  35,                                  /* colNo */
  "sparse/sparse",                     /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo q_emlrtRTEI = { 118,/* lineNo */
  35,                                  /* colNo */
  "sparse/sparse",                     /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtDCInfo b_emlrtDCI = { 195, /* lineNo */
  48,                                  /* colNo */
  "sparse/sparse",                     /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m",/* pName */
  4                                    /* checkKind */
};

static emlrtRTEInfo ud_emlrtRTEI = { 127,/* lineNo */
  44,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo vd_emlrtRTEI = { 13,/* lineNo */
  1,                                   /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\eml\\lib\\matlab\\sparfun\\sparse.m"/* pName */
};

static emlrtRTEInfo xd_emlrtRTEI = { 127,/* lineNo */
  13,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

static emlrtRTEInfo yd_emlrtRTEI = { 1635,/* lineNo */
  36,                                  /* colNo */
  "sparse",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2020b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\@sparse\\sparse.m"/* pName */
};

/* Function Definitions */
void b_sparse(const emlrtStack *sp, const emxArray_real_T *varargin_1, const
              emxArray_real_T *varargin_2, const emxArray_real_T *varargin_3,
              e_sparse *y)
{
  cell_wrap_6 this_tunableEnvironment[2];
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack f_st;
  emlrtStack g_st;
  emlrtStack h_st;
  emlrtStack i_st;
  emlrtStack st;
  emxArray_int32_T *sortedIndices;
  int32_T i;
  int32_T k;
  int32_T nc;
  int32_T numalloc;
  int32_T thism;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  f_st.prev = &e_st;
  f_st.tls = e_st.tls;
  g_st.prev = &f_st;
  g_st.tls = f_st.tls;
  h_st.prev = &g_st;
  h_st.tls = g_st.tls;
  i_st.prev = &h_st;
  i_st.tls = h_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  st.site = &gk_emlrtRSI;
  nc = varargin_2->size[1];
  if ((varargin_1->size[1] != varargin_2->size[1]) || (varargin_3->size[1] !=
       varargin_2->size[1])) {
    emlrtErrorWithMessageIdR2018a(&st, &p_emlrtRTEI, "MATLAB:samelen",
      "MATLAB:samelen", 0);
  }

  if ((varargin_3->size[1] != varargin_2->size[1]) && (varargin_3->size[1] !=
       varargin_1->size[1])) {
    emlrtErrorWithMessageIdR2018a(&st, &q_emlrtRTEI, "MATLAB:samelen",
      "MATLAB:samelen", 0);
  }

  emxInit_int32_T(&st, &sortedIndices, 1, &xd_emlrtRTEI, true);
  emxInitMatrix_cell_wrap_6(&st, this_tunableEnvironment, &yd_emlrtRTEI, true);
  b_st.site = &hk_emlrtRSI;
  b_assertValidIndexArg(&b_st, varargin_1, this_tunableEnvironment[1].f1);
  b_st.site = &ik_emlrtRSI;
  b_assertValidIndexArg(&b_st, varargin_2, this_tunableEnvironment[0].f1);
  i = sortedIndices->size[0];
  sortedIndices->size[0] = varargin_2->size[1];
  emxEnsureCapacity_int32_T(&st, sortedIndices, i, &ud_emlrtRTEI);
  b_st.site = &jk_emlrtRSI;
  if ((1 <= varargin_2->size[1]) && (varargin_2->size[1] > 2147483646)) {
    c_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&c_st);
  }

  for (k = 0; k < nc; k++) {
    sortedIndices->data[k] = k + 1;
  }

  b_st.site = &kk_emlrtRSI;
  c_st.site = &qk_emlrtRSI;
  introsort(&c_st, sortedIndices, this_tunableEnvironment[0].f1->size[0],
            this_tunableEnvironment);
  c_st.site = &rk_emlrtRSI;
  permuteVector(&c_st, sortedIndices, this_tunableEnvironment[0].f1);
  c_st.site = &sk_emlrtRSI;
  permuteVector(&c_st, sortedIndices, this_tunableEnvironment[1].f1);
  if ((this_tunableEnvironment[1].f1->size[0] == 0) || (this_tunableEnvironment
       [0].f1->size[0] == 0)) {
    thism = 0;
    y->n = 0;
  } else {
    b_st.site = &lk_emlrtRSI;
    c_st.site = &sj_emlrtRSI;
    d_st.site = &tj_emlrtRSI;
    e_st.site = &uj_emlrtRSI;
    f_st.site = &vj_emlrtRSI;
    numalloc = this_tunableEnvironment[1].f1->size[0];
    g_st.site = &fl_emlrtRSI;
    thism = this_tunableEnvironment[1].f1->data[0];
    h_st.site = &gl_emlrtRSI;
    if ((2 <= this_tunableEnvironment[1].f1->size[0]) &&
        (this_tunableEnvironment[1].f1->size[0] > 2147483646)) {
      i_st.site = &fg_emlrtRSI;
      check_forloop_overflow_error(&i_st);
    }

    for (k = 2; k <= numalloc; k++) {
      i = this_tunableEnvironment[1].f1->data[k - 1];
      if (thism < i) {
        thism = i;
      }
    }

    y->n = this_tunableEnvironment[0].f1->data[this_tunableEnvironment[0]
      .f1->size[0] - 1];
  }

  y->m = thism;
  if (varargin_2->size[1] >= 1) {
    numalloc = varargin_2->size[1];
  } else {
    numalloc = 1;
  }

  i = y->d->size[0];
  y->d->size[0] = numalloc;
  emxEnsureCapacity_real_T(&st, y->d, i, &vd_emlrtRTEI);
  for (i = 0; i < numalloc; i++) {
    y->d->data[i] = 0.0;
  }

  y->maxnz = numalloc;
  if (y->n + 1 < 0) {
    emlrtNonNegativeCheckR2012b(y->n + 1, &b_emlrtDCI, &st);
  }

  i = y->colidx->size[0];
  y->colidx->size[0] = y->n + 1;
  emxEnsureCapacity_int32_T(&st, y->colidx, i, &wd_emlrtRTEI);
  y->colidx->data[0] = 1;
  i = y->rowidx->size[0];
  y->rowidx->size[0] = numalloc;
  emxEnsureCapacity_int32_T(&st, y->rowidx, i, &vd_emlrtRTEI);
  for (i = 0; i < numalloc; i++) {
    y->rowidx->data[i] = 0;
  }

  numalloc = 0;
  thism = y->n;
  b_st.site = &mk_emlrtRSI;
  if ((1 <= y->n) && (y->n > 2147483646)) {
    c_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&c_st);
  }

  for (k = 0; k < thism; k++) {
    while ((numalloc + 1 <= nc) && (this_tunableEnvironment[0].f1->data[numalloc]
            == k + 1)) {
      y->rowidx->data[numalloc] = this_tunableEnvironment[1].f1->data[numalloc];
      numalloc++;
    }

    y->colidx->data[k + 1] = numalloc + 1;
  }

  emxFreeMatrix_cell_wrap_6(this_tunableEnvironment);
  b_st.site = &nk_emlrtRSI;
  if ((1 <= varargin_2->size[1]) && (varargin_2->size[1] > 2147483646)) {
    c_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&c_st);
  }

  for (k = 0; k < nc; k++) {
    y->d->data[k] = varargin_3->data[sortedIndices->data[k] - 1];
  }

  emxFree_int32_T(&sortedIndices);
  b_st.site = &ok_emlrtRSI;
  sparse_fillIn(&b_st, y);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

void c_sparse(const emlrtStack *sp, const emxArray_real_T *varargin_1,
              emxArray_real_T *y_d, emxArray_int32_T *y_colidx, emxArray_int32_T
              *y_rowidx, int32_T *y_m, int32_T *y_n, int32_T *y_maxnz)
{
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack st;
  int32_T i;
  int32_T loop_ub;
  int32_T mInt;
  int32_T nInt;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &gk_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  mInt = varargin_1->size[0];
  nInt = varargin_1->size[1];
  b_st.site = &ao_emlrtRSI;
  if (varargin_1->size[0] >= MAX_int32_T) {
    emlrtErrorWithMessageIdR2018a(&b_st, &ab_emlrtRTEI,
      "Coder:toolbox:SparseMaxSize", "Coder:toolbox:SparseMaxSize", 2, 12,
      MAX_int32_T);
  }

  b_st.site = &yn_emlrtRSI;
  *y_m = varargin_1->size[0];
  *y_n = varargin_1->size[1];
  i = y_d->size[0];
  y_d->size[0] = 1;
  emxEnsureCapacity_real_T(&st, y_d, i, &vd_emlrtRTEI);
  y_d->data[0] = 0.0;
  i = y_colidx->size[0];
  y_colidx->size[0] = varargin_1->size[1] + 1;
  emxEnsureCapacity_int32_T(&st, y_colidx, i, &vd_emlrtRTEI);
  loop_ub = varargin_1->size[1];
  for (i = 0; i <= loop_ub; i++) {
    y_colidx->data[i] = 0;
  }

  y_colidx->data[0] = 1;
  i = y_rowidx->size[0];
  y_rowidx->size[0] = 1;
  emxEnsureCapacity_int32_T(&st, y_rowidx, i, &vd_emlrtRTEI);
  y_rowidx->data[0] = 1;
  b_st.site = &xn_emlrtRSI;
  for (loop_ub = 0; loop_ub < nInt; loop_ub++) {
    b_st.site = &wn_emlrtRSI;
    if ((1 <= mInt) && (mInt > 2147483646)) {
      c_st.site = &fg_emlrtRSI;
      check_forloop_overflow_error(&c_st);
    }

    y_colidx->data[loop_ub + 1] = 1;
  }

  *y_maxnz = 1;
}

void d_sparse(const emlrtStack *sp, boolean_T varargin_1, emxArray_boolean_T
              *y_d, emxArray_int32_T *y_colidx, emxArray_int32_T *y_rowidx)
{
  emlrtStack st;
  int32_T i;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &gk_emlrtRSI;
  i = y_d->size[0];
  y_d->size[0] = 1;
  emxEnsureCapacity_boolean_T(&st, y_d, i, &vd_emlrtRTEI);
  y_d->data[0] = false;
  i = y_colidx->size[0];
  y_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(&st, y_colidx, i, &vd_emlrtRTEI);
  y_colidx->data[0] = 1;
  i = y_rowidx->size[0];
  y_rowidx->size[0] = 1;
  emxEnsureCapacity_int32_T(&st, y_rowidx, i, &vd_emlrtRTEI);
  y_rowidx->data[0] = 1;
  y_colidx->data[1] = 1;
  if (varargin_1) {
    y_rowidx->data[0] = 1;
    y_d->data[0] = true;
    y_colidx->data[1] = 2;
  }
}

void sparse(const emlrtStack *sp, const emxArray_real_T *varargin_1, const
            emxArray_real_T *varargin_2, const emxArray_real_T *varargin_3,
            e_sparse *y)
{
  cell_wrap_6 this_tunableEnvironment[2];
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack f_st;
  emlrtStack g_st;
  emlrtStack h_st;
  emlrtStack i_st;
  emlrtStack st;
  emxArray_int32_T *sortedIndices;
  int32_T i;
  int32_T k;
  int32_T nc;
  int32_T nr;
  int32_T ny;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  f_st.prev = &e_st;
  f_st.tls = e_st.tls;
  g_st.prev = &f_st;
  g_st.tls = f_st.tls;
  h_st.prev = &g_st;
  h_st.tls = g_st.tls;
  i_st.prev = &h_st;
  i_st.tls = h_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  st.site = &gk_emlrtRSI;
  nc = varargin_2->size[0] * varargin_2->size[1];
  nr = varargin_1->size[0] * varargin_1->size[1];
  ny = varargin_3->size[0] * varargin_3->size[1];
  if ((nr != nc) || (ny != nc)) {
    emlrtErrorWithMessageIdR2018a(&st, &p_emlrtRTEI, "MATLAB:samelen",
      "MATLAB:samelen", 0);
  }

  if ((ny != nc) && (ny != nr)) {
    emlrtErrorWithMessageIdR2018a(&st, &q_emlrtRTEI, "MATLAB:samelen",
      "MATLAB:samelen", 0);
  }

  emxInit_int32_T(&st, &sortedIndices, 1, &xd_emlrtRTEI, true);
  emxInitMatrix_cell_wrap_6(&st, this_tunableEnvironment, &yd_emlrtRTEI, true);
  b_st.site = &hk_emlrtRSI;
  assertValidIndexArg(&b_st, varargin_1, this_tunableEnvironment[1].f1);
  b_st.site = &ik_emlrtRSI;
  assertValidIndexArg(&b_st, varargin_2, this_tunableEnvironment[0].f1);
  i = sortedIndices->size[0];
  sortedIndices->size[0] = nc;
  emxEnsureCapacity_int32_T(&st, sortedIndices, i, &ud_emlrtRTEI);
  b_st.site = &jk_emlrtRSI;
  if ((1 <= nc) && (nc > 2147483646)) {
    c_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&c_st);
  }

  for (k = 0; k < nc; k++) {
    sortedIndices->data[k] = k + 1;
  }

  b_st.site = &kk_emlrtRSI;
  c_st.site = &qk_emlrtRSI;
  introsort(&c_st, sortedIndices, this_tunableEnvironment[0].f1->size[0],
            this_tunableEnvironment);
  c_st.site = &rk_emlrtRSI;
  permuteVector(&c_st, sortedIndices, this_tunableEnvironment[0].f1);
  c_st.site = &sk_emlrtRSI;
  permuteVector(&c_st, sortedIndices, this_tunableEnvironment[1].f1);
  if ((this_tunableEnvironment[1].f1->size[0] == 0) || (this_tunableEnvironment
       [0].f1->size[0] == 0)) {
    ny = 0;
    y->n = 0;
  } else {
    b_st.site = &lk_emlrtRSI;
    c_st.site = &sj_emlrtRSI;
    d_st.site = &tj_emlrtRSI;
    e_st.site = &uj_emlrtRSI;
    f_st.site = &vj_emlrtRSI;
    nr = this_tunableEnvironment[1].f1->size[0];
    g_st.site = &fl_emlrtRSI;
    ny = this_tunableEnvironment[1].f1->data[0];
    h_st.site = &gl_emlrtRSI;
    if ((2 <= this_tunableEnvironment[1].f1->size[0]) &&
        (this_tunableEnvironment[1].f1->size[0] > 2147483646)) {
      i_st.site = &fg_emlrtRSI;
      check_forloop_overflow_error(&i_st);
    }

    for (k = 2; k <= nr; k++) {
      i = this_tunableEnvironment[1].f1->data[k - 1];
      if (ny < i) {
        ny = i;
      }
    }

    y->n = this_tunableEnvironment[0].f1->data[this_tunableEnvironment[0]
      .f1->size[0] - 1];
  }

  y->m = ny;
  if (nc >= 1) {
    nr = nc;
  } else {
    nr = 1;
  }

  i = y->d->size[0];
  y->d->size[0] = nr;
  emxEnsureCapacity_real_T(&st, y->d, i, &vd_emlrtRTEI);
  for (i = 0; i < nr; i++) {
    y->d->data[i] = 0.0;
  }

  y->maxnz = nr;
  if (y->n + 1 < 0) {
    emlrtNonNegativeCheckR2012b(y->n + 1, &b_emlrtDCI, &st);
  }

  i = y->colidx->size[0];
  y->colidx->size[0] = y->n + 1;
  emxEnsureCapacity_int32_T(&st, y->colidx, i, &wd_emlrtRTEI);
  y->colidx->data[0] = 1;
  i = y->rowidx->size[0];
  y->rowidx->size[0] = nr;
  emxEnsureCapacity_int32_T(&st, y->rowidx, i, &vd_emlrtRTEI);
  for (i = 0; i < nr; i++) {
    y->rowidx->data[i] = 0;
  }

  nr = 0;
  ny = y->n;
  b_st.site = &mk_emlrtRSI;
  if ((1 <= y->n) && (y->n > 2147483646)) {
    c_st.site = &fg_emlrtRSI;
    check_forloop_overflow_error(&c_st);
  }

  for (k = 0; k < ny; k++) {
    while ((nr + 1 <= nc) && (this_tunableEnvironment[0].f1->data[nr] == k + 1))
    {
      y->rowidx->data[nr] = this_tunableEnvironment[1].f1->data[nr];
      nr++;
    }

    y->colidx->data[k + 1] = nr + 1;
  }

  emxFreeMatrix_cell_wrap_6(this_tunableEnvironment);
  b_st.site = &nk_emlrtRSI;
  for (k = 0; k < nc; k++) {
    y->d->data[k] = varargin_3->data[sortedIndices->data[k] - 1];
  }

  emxFree_int32_T(&sortedIndices);
  b_st.site = &ok_emlrtRSI;
  sparse_fillIn(&b_st, y);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (sparse.c) */
