/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sparse1.h
 *
 * Code generation for function 'sparse1'
 *
 */

#pragma once

/* Include files */
#include "DEIPFunc3_types.h"
#include "rtwtypes.h"
#include "emlrt.h"
#include "mex.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Function Declarations */
void assertValidIndexArg(const emlrtStack *sp, const emxArray_real_T *s,
  emxArray_int32_T *sint);
void b_assertValidIndexArg(const emlrtStack *sp, const emxArray_real_T *s,
  emxArray_int32_T *sint);
void b_sparse_and(const emlrtStack *sp, const emxArray_boolean_T *a_d, const
                  emxArray_int32_T *a_colidx, const emxArray_boolean_T *b_d,
                  const emxArray_int32_T *b_colidx, const emxArray_int32_T
                  *b_rowidx, h_sparse *s);
void b_sparse_eq(const emlrtStack *sp, const emxArray_real_T *a_d, const
                 emxArray_int32_T *a_colidx, const emxArray_real_T *b_d, const
                 emxArray_int32_T *b_colidx, const emxArray_int32_T *b_rowidx,
                 h_sparse *s);
void b_sparse_full(const emlrtStack *sp, const emxArray_real_T *this_d, const
                   emxArray_int32_T *this_colidx, const emxArray_int32_T
                   *this_rowidx, int32_T this_m, int32_T this_n, emxArray_real_T
                   *y);
void b_sparse_gt(const emlrtStack *sp, const emxArray_real_T *a_d, const
                 emxArray_int32_T *a_colidx, emxArray_boolean_T *s_d,
                 emxArray_int32_T *s_colidx, emxArray_int32_T *s_rowidx);
void b_sparse_parenAssign(const emlrtStack *sp, e_sparse *this, const
  emxArray_real_T *rhs, const emxArray_real_T *varargin_1);
void b_sparse_parenReference(const emlrtStack *sp, const emxArray_real_T *this_d,
  const emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx,
  int32_T this_m, int32_T this_n, const emxArray_real_T *varargin_1, const
  real_T varargin_2_data[], const int32_T varargin_2_size[2], emxArray_real_T
  *s_d, emxArray_int32_T *s_colidx, emxArray_int32_T *s_rowidx, int32_T *s_m,
  int32_T *s_n, int32_T *s_maxnz);
void c_sparse_eq(const emlrtStack *sp, real_T a, const emxArray_real_T *b_d,
                 const emxArray_int32_T *b_colidx, const emxArray_int32_T
                 *b_rowidx, int32_T b_m, g_sparse *s);
void c_sparse_full(const emlrtStack *sp, const emxArray_real_T *this_d, const
                   emxArray_int32_T *this_colidx, const emxArray_int32_T
                   *this_rowidx, int32_T this_m, emxArray_real_T *y);
void c_sparse_parenAssign(const emlrtStack *sp, e_sparse *this, real_T rhs,
  const emxArray_real_T *varargin_1, real_T varargin_2);
void c_sparse_parenReference(const emlrtStack *sp, const emxArray_real_T *this_d,
  const emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx,
  int32_T this_m, int32_T this_n, real_T varargin_2, f_sparse *s);
void c_sparse_spallocLike(const emlrtStack *sp, int32_T nzmax,
  emxArray_boolean_T *s_d, emxArray_int32_T *s_colidx, emxArray_int32_T
  *s_rowidx);
boolean_T d_sparse_full(const emlrtStack *sp, const emxArray_boolean_T *this_d,
  const emxArray_int32_T *this_colidx);
void d_sparse_parenReference(const emlrtStack *sp, const emxArray_real_T *this_d,
  const emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx,
  int32_T this_m, int32_T this_n, real_T varargin_2, emxArray_real_T *s_d,
  emxArray_int32_T *s_colidx, emxArray_int32_T *s_rowidx);
void e_sparse_parenReference(const emlrtStack *sp, const emxArray_real_T *this_d,
  const emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx,
  int32_T this_m, int32_T this_n, real_T varargin_1, real_T varargin_2,
  emxArray_real_T *s_d, emxArray_int32_T *s_colidx, emxArray_int32_T *s_rowidx);
void f_sparse_parenReference(const emlrtStack *sp, const emxArray_real_T *this_d,
  const emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx,
  int32_T this_m, int32_T this_n, const emxArray_real_T *varargin_1, real_T
  varargin_2, emxArray_real_T *s_d, emxArray_int32_T *s_colidx, emxArray_int32_T
  *s_rowidx, int32_T *s_m);
void permuteVector(const emlrtStack *sp, const emxArray_int32_T *idx,
                   emxArray_int32_T *y);
void sparse_and(const emlrtStack *sp, const emxArray_boolean_T *a_d, const
                emxArray_int32_T *a_colidx, const emxArray_int32_T *a_rowidx,
                int32_T a_m, const emxArray_boolean_T *b_d, const
                emxArray_int32_T *b_colidx, const emxArray_int32_T *b_rowidx,
                int32_T b_m, emxArray_boolean_T *s_d, emxArray_int32_T *s_colidx,
                emxArray_int32_T *s_rowidx, int32_T *s_m);
void sparse_eq(const emlrtStack *sp, const emxArray_real_T *a_d, const
               emxArray_int32_T *a_colidx, const emxArray_int32_T *a_rowidx,
               int32_T a_m, const emxArray_real_T *b_d, const emxArray_int32_T
               *b_colidx, const emxArray_int32_T *b_rowidx, int32_T b_m,
               emxArray_boolean_T *s_d, emxArray_int32_T *s_colidx,
               emxArray_int32_T *s_rowidx, int32_T *s_m);
real_T sparse_full(const emlrtStack *sp, const emxArray_real_T *this_d, const
                   emxArray_int32_T *this_colidx);
void sparse_gt(const emlrtStack *sp, const emxArray_real_T *a_d, const
               emxArray_int32_T *a_colidx, const emxArray_int32_T *a_rowidx,
               int32_T a_m, g_sparse *s);
void sparse_parenAssign(const emlrtStack *sp, e_sparse *this, const
  emxArray_real_T *rhs_d, const emxArray_int32_T *rhs_colidx, const
  emxArray_int32_T *rhs_rowidx, int32_T rhs_m, const emxArray_real_T *varargin_1);
void sparse_parenReference(const emlrtStack *sp, const emxArray_real_T *this_d,
  const emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx,
  int32_T this_m, int32_T this_n, const emxArray_real_T *varargin_1, f_sparse *s);
void sparse_spallocLike(const emlrtStack *sp, int32_T m, int32_T nzmax, f_sparse
  *s);
void sparse_times(const emlrtStack *sp, const emxArray_real_T *a, const
                  emxArray_real_T *b_d, const emxArray_int32_T *b_colidx, const
                  emxArray_int32_T *b_rowidx, int32_T b_m, emxArray_real_T *s_d,
                  emxArray_int32_T *s_colidx, emxArray_int32_T *s_rowidx,
                  int32_T *s_m);

/* End of code generation (sparse1.h) */
